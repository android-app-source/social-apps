.class public Lcom/twitter/android/initialization/AppStyleInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/app/ActivityManager$TaskDescription;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 79
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 81
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 82
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 83
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 84
    return-object v0
.end method

.method private a(Landroid/app/Activity;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 49
    invoke-static {}, Lcom/twitter/android/initialization/AppStyleInitializer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-direct {p0, p1}, Lcom/twitter/android/initialization/AppStyleInitializer;->a(Landroid/content/Context;)V

    .line 51
    iget-object v0, p0, Lcom/twitter/android/initialization/AppStyleInitializer;->a:Landroid/app/ActivityManager$TaskDescription;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 53
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Lcom/twitter/android/initialization/AppStyleInitializer;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/twitter/android/initialization/AppStyleInitializer;->a:Landroid/app/ActivityManager$TaskDescription;

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/initialization/AppStyleInitializer;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->labelRes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    const v2, 0x7f02067b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 63
    invoke-static {p1}, Lcnh;->a(Landroid/content/Context;)I

    move-result v3

    invoke-static {v2, v3}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 62
    invoke-static {v2}, Lcom/twitter/android/initialization/AppStyleInitializer;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 64
    const v3, 0x7f110015

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 65
    new-instance v3, Landroid/app/ActivityManager$TaskDescription;

    invoke-direct {v3, v1, v2, v0}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    iput-object v3, p0, Lcom/twitter/android/initialization/AppStyleInitializer;->a:Landroid/app/ActivityManager$TaskDescription;

    .line 68
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/initialization/AppStyleInitializer;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/android/initialization/AppStyleInitializer;->a(Landroid/app/Activity;)V

    return-void
.end method

.method private a(Landroid/content/res/Resources;)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 73
    invoke-static {}, Lcom/twitter/android/initialization/AppStyleInitializer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/initialization/AppStyleInitializer;->a:Landroid/app/ActivityManager$TaskDescription;

    .line 74
    invoke-virtual {v0}, Landroid/app/ActivityManager$TaskDescription;->getPrimaryColor()I

    move-result v0

    const v1, 0x7f110015

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/AppStyleInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/initialization/AppStyleInitializer$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/initialization/AppStyleInitializer$1;-><init>(Lcom/twitter/android/initialization/AppStyleInitializer;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/util/b;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 45
    return-void
.end method
