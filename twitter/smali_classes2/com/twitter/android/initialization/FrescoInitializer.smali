.class public Lcom/twitter/android/initialization/FrescoInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/FrescoInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lbyz;

    invoke-direct {v0}, Lbyz;-><init>()V

    .line 45
    new-instance v1, Lbrg;

    sget-object v2, Lcom/twitter/library/media/manager/j$a;->i:Lcom/twitter/library/media/manager/i$b;

    invoke-direct {v1, p1, v2}, Lbrg;-><init>(Landroid/content/Context;Lcom/twitter/library/media/manager/i$b;)V

    .line 47
    invoke-static {p1}, Ldh;->a(Landroid/content/Context;)Ldh$a;

    move-result-object v2

    new-instance v3, Lbyw;

    invoke-direct {v3, p1}, Lbyw;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {v2, v3}, Ldh$a;->a(Laz;)Ldh$a;

    move-result-object v2

    new-instance v3, Lbyy;

    invoke-direct {v3}, Lbyy;-><init>()V

    .line 49
    invoke-virtual {v2, v3}, Ldh$a;->b(Laz;)Ldh$a;

    move-result-object v2

    .line 50
    invoke-static {p1}, Lbyx;->a(Landroid/content/Context;)Lcom/facebook/cache/disk/b;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldh$a;->a(Lcom/facebook/cache/disk/b;)Ldh$a;

    move-result-object v2

    .line 51
    invoke-static {}, Lbza;->a()Lbza;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldh$a;->a(Lcom/facebook/common/memory/b;)Ldh$a;

    move-result-object v2

    .line 52
    invoke-virtual {v2, v0}, Ldh$a;->a(Ldf;)Ldh$a;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v1}, Ldh$a;->a(Lcom/facebook/imagepipeline/producers/ac;)Ldh$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Ldh$a;->a()Ldh;

    move-result-object v0

    .line 55
    invoke-static {p1, v0}, Lbq;->a(Landroid/content/Context;Ldh;)V

    .line 56
    const-class v0, Lcom/twitter/android/initialization/FrescoInitializer;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 58
    new-instance v0, Lcom/twitter/android/initialization/FrescoInitializer$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/initialization/FrescoInitializer$1;-><init>(Lcom/twitter/android/initialization/FrescoInitializer;)V

    invoke-static {v0}, Lcpg;->a(Lcpg$b;)V

    .line 64
    return-void
.end method
