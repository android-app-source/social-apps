.class public Lcom/twitter/android/initialization/ErrorReporterInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/initialization/ErrorReporterInitializer$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lanb;-><init>()V

    .line 54
    iput-boolean p1, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer;->a:Z

    .line 55
    iput-boolean p2, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer;->b:Z

    .line 56
    return-void
.end method

.method static synthetic a(Lcpa;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 47
    invoke-static {p0, p1}, Lcom/twitter/android/initialization/ErrorReporterInitializer;->b(Lcpa;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/twitter/android/initialization/ErrorReporterInitializer;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcpa;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 157
    const-string/jumbo v1, "verified_user"

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcpa;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    const-string/jumbo v0, "user_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpa;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    return-void

    .line 157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    :try_start_0
    invoke-static {}, Lcoo;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Git SHA: 61f5fde5b56a1de7b614d674afeb2199d4296ed5 Display:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Fingerprint:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Brand:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Device:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Product:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/ErrorReporterInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 60
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    check-cast v0, Lcph;

    .line 64
    new-instance v1, Lcom/twitter/fabric/CrashlyticsErrorLogger;

    iget-boolean v2, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer;->a:Z

    invoke-direct {v1, p1, v2}, Lcom/twitter/fabric/CrashlyticsErrorLogger;-><init>(Landroid/content/Context;Z)V

    .line 65
    iget-boolean v2, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer;->a:Z

    if-eqz v2, :cond_0

    .line 66
    new-instance v2, Lcom/twitter/android/initialization/ErrorReporterInitializer$1;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/initialization/ErrorReporterInitializer$1;-><init>(Lcom/twitter/android/initialization/ErrorReporterInitializer;Lcom/twitter/fabric/CrashlyticsErrorLogger;)V

    invoke-static {v2}, Lcoj;->a(Lcoj$a;)V

    .line 73
    invoke-virtual {v0, v1}, Lcph;->a(Lcpc;)V

    .line 76
    :cond_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->a()Z

    move-result v1

    .line 77
    if-nez v1, :cond_1

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    invoke-virtual {v2}, Lcof;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    :cond_1
    new-instance v2, Lcpe;

    invoke-direct {v2, p1, v1}, Lcpe;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v0, v2}, Lcph;->a(Lcpc;)V

    .line 81
    :cond_2
    invoke-static {}, Lcom/twitter/android/dogfood/a;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 82
    invoke-static {p1}, Lcom/twitter/android/dogfood/a;->a(Landroid/content/Context;)Lcom/twitter/android/dogfood/a;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Lcom/twitter/android/dogfood/a;->f()Lrx/a;

    .line 84
    invoke-virtual {v0, v1}, Lcph;->a(Lcpc;)V

    .line 87
    :cond_3
    new-instance v1, Lcom/twitter/library/network/narc/g;

    invoke-direct {v1}, Lcom/twitter/library/network/narc/g;-><init>()V

    invoke-virtual {v0, v1}, Lcph;->a(Lcpc;)V

    .line 89
    new-instance v1, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;

    .line 90
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer;->b:Z

    invoke-direct {v1, p1, v2, v3}, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;-><init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;Z)V

    .line 89
    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 92
    invoke-virtual {v0}, Lcph;->b()Lcpa;

    move-result-object v1

    .line 93
    const-string/jumbo v2, "build_info"

    invoke-static {}, Lcom/twitter/android/initialization/ErrorReporterInitializer;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcpa;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    invoke-virtual {v1}, Lcpa;->a()I

    .line 97
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/twitter/android/initialization/ErrorReporterInitializer;->b(Lcpa;Lcom/twitter/library/client/Session;)V

    .line 99
    new-instance v3, Lcom/twitter/android/initialization/ErrorReporterInitializer$2;

    invoke-direct {v3, p0, v1}, Lcom/twitter/android/initialization/ErrorReporterInitializer$2;-><init>(Lcom/twitter/android/initialization/ErrorReporterInitializer;Lcpa;)V

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 106
    new-instance v1, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;-><init>(Lcom/twitter/android/initialization/ErrorReporterInitializer;)V

    invoke-virtual {v0, v1}, Lcph;->a(Lcph$a;)V

    .line 133
    return-void
.end method
