.class public Lcom/twitter/android/initialization/TypefaceInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/TypefaceInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 24
    const-string/jumbo v0, "typefaces_android_in_twitter_edit_text_enable"

    .line 25
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    invoke-static {v0}, Lcom/twitter/ui/widget/TwitterEditText;->setUseCustomFont(Z)V

    .line 30
    :cond_0
    const-string/jumbo v0, "android_system_font_5282"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    .line 31
    invoke-static {v0}, Lcom/twitter/ui/widget/i;->a(Z)V

    .line 32
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/initialization/TypefaceInitializer$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/initialization/TypefaceInitializer$1;-><init>(Lcom/twitter/android/initialization/TypefaceInitializer;Z)V

    invoke-virtual {v1, v2}, Lcom/twitter/app/common/util/b;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 41
    return-void
.end method
