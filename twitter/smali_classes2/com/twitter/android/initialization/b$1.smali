.class Lcom/twitter/android/initialization/b$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/initialization/b;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/initialization/b;


# direct methods
.method constructor <init>(Lcom/twitter/android/initialization/b;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/twitter/android/initialization/b$1;->a:Lcom/twitter/android/initialization/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    .line 55
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v2

    .line 56
    sget-object v3, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 57
    iget-object v0, p0, Lcom/twitter/android/initialization/b$1;->a:Lcom/twitter/android/initialization/b;

    invoke-static {v0}, Lcom/twitter/android/initialization/b;->a(Lcom/twitter/android/initialization/b;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 58
    new-instance v5, Lcom/twitter/metrics/g;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v5, v1, v3, v6, v7}, Lcom/twitter/metrics/g;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;J)V

    invoke-virtual {v2, v5}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/g;)V

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method
