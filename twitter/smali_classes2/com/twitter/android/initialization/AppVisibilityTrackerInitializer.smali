.class public Lcom/twitter/android/initialization/AppVisibilityTrackerInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/AppVisibilityTrackerInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 20
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    .line 21
    new-instance v1, Lcom/twitter/android/client/d;

    invoke-direct {v1, p1}, Lcom/twitter/android/client/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 22
    new-instance v1, Lcom/twitter/library/util/d$a;

    invoke-direct {v1}, Lcom/twitter/library/util/d$a;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 23
    new-instance v1, Lbpz;

    invoke-direct {v1}, Lbpz;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 24
    new-instance v1, Lcom/twitter/android/ae;

    invoke-direct {v1}, Lcom/twitter/android/ae;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 25
    new-instance v1, Lcom/twitter/library/client/x;

    invoke-direct {v1, p1}, Lcom/twitter/library/client/x;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 26
    new-instance v1, Lcom/twitter/badge/a;

    invoke-direct {v1}, Lcom/twitter/badge/a;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 27
    return-void
.end method
