.class Lcom/twitter/android/TweetActivity$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/TweetActivity;->a(Ljava/lang/String;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/TweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 545
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$4;->c:Lcom/twitter/android/TweetActivity;

    iput-object p2, p0, Lcom/twitter/android/TweetActivity$4;->a:Ljava/lang/String;

    iput p3, p0, Lcom/twitter/android/TweetActivity$4;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 550
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity$4;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v1}, Lcom/twitter/android/TweetActivity;->c(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "tweet:notification_landing"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/TweetActivity$4;->a:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x2

    const-string/jumbo v4, ":click"

    aput-object v4, v1, v3

    .line 551
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 550
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 552
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$4;->c:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity$4;->c:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-interface {v0}, Lata;->f()Lcom/twitter/library/api/ActivitySummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 554
    iget v0, p0, Lcom/twitter/android/TweetActivity$4;->b:I

    packed-switch v0, :pswitch_data_0

    move-object v1, v2

    .line 568
    :goto_0
    if-eqz v1, :cond_0

    .line 569
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$4;->c:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lata;

    .line 570
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetDetailView$b;

    .line 571
    iget v3, p0, Lcom/twitter/android/TweetActivity$4;->b:I

    invoke-interface {v0, v2, v3, v1}, Lcom/twitter/android/widget/TweetDetailView$b;->a(Landroid/view/View;I[J)V

    .line 574
    :cond_0
    return-void

    .line 556
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$4;->c:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-interface {v0}, Lata;->f()Lcom/twitter/library/api/ActivitySummary;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/ActivitySummary;->d:[J

    move-object v1, v0

    .line 557
    goto :goto_0

    .line 560
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$4;->c:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-interface {v0}, Lata;->f()Lcom/twitter/library/api/ActivitySummary;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/ActivitySummary;->c:[J

    move-object v1, v0

    .line 561
    goto :goto_0

    .line 554
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
