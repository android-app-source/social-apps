.class public Lcom/twitter/android/bj;
.super Lcom/twitter/android/UsersAdapter;
.source "Twttr"


# instance fields
.field protected final a:Z

.field protected b:Z

.field private d:Z

.field private final e:Z

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Z)V
    .locals 8
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 56
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/bj;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;ZLcom/twitter/android/UsersAdapter$CheckboxConfig;Z)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;ZLcom/twitter/android/UsersAdapter$CheckboxConfig;Z)V
    .locals 6
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Z",
            "Lcom/twitter/android/UsersAdapter$CheckboxConfig;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 48
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/UsersAdapter;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/bj;->b:Z

    .line 49
    iput-boolean p5, p0, Lcom/twitter/android/bj;->a:Z

    .line 50
    iput-boolean p7, p0, Lcom/twitter/android/bj;->e:Z

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-boolean v0, p0, Lcom/twitter/android/bj;->a:Z

    if-eqz v0, :cond_2

    .line 63
    iget-boolean v0, p0, Lcom/twitter/android/bj;->e:Z

    if-nez v0, :cond_1

    .line 64
    invoke-virtual {p0}, Lcom/twitter/android/bj;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f040063

    .line 66
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 67
    invoke-virtual {v1, v0, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserView;

    .line 66
    invoke-virtual {p0, v0}, Lcom/twitter/android/bj;->a(Lcom/twitter/ui/user/UserView;)Lcom/twitter/ui/user/UserView;

    move-result-object v0

    .line 76
    :goto_1
    return-object v0

    .line 64
    :cond_0
    const v0, 0x7f04042f

    goto :goto_0

    .line 69
    :cond_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04013b

    .line 70
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 71
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    .line 72
    invoke-virtual {p0, v0}, Lcom/twitter/android/bj;->a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/bj;->a(Lcom/twitter/ui/user/UserView;)Lcom/twitter/ui/user/UserView;

    goto :goto_1

    .line 76
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/UsersAdapter;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/bj;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/twitter/android/bj;->e:Z

    if-eqz v0, :cond_0

    .line 155
    check-cast p1, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserView;

    .line 157
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/UsersAdapter;->a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/bj;->d:Z

    .line 167
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/16 v6, 0xd

    const/4 v2, 0x0

    .line 82
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/UsersAdapter;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 83
    const/16 v0, 0xe

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 85
    iget-boolean v0, p0, Lcom/twitter/android/bj;->a:Z

    if-eqz v0, :cond_4

    .line 86
    invoke-virtual {p0, p1}, Lcom/twitter/android/bj;->a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    .line 87
    sget v1, Lcni;->a:F

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setContentSize(F)V

    .line 88
    iget-boolean v1, p0, Lcom/twitter/android/bj;->f:Z

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->c(Z)V

    .line 89
    iget-boolean v1, p0, Lcom/twitter/android/bj;->c:Z

    if-eqz v1, :cond_2

    .line 90
    const/16 v1, 0x8

    .line 91
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v5, 0x9

    .line 92
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/model/core/v;->a([B)Lcom/twitter/model/core/v;

    move-result-object v5

    .line 90
    invoke-virtual {v0, v1, v5}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 93
    iget-boolean v1, p0, Lcom/twitter/android/bj;->g:Z

    if-eqz v1, :cond_0

    .line 94
    invoke-static {}, Lcom/twitter/android/revenue/k;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setProfileDescriptionMaxLines(I)V

    .line 100
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/bj;->d:Z

    if-eqz v1, :cond_1

    .line 101
    invoke-virtual {v0}, Lcom/twitter/ui/user/UserSocialView;->g()V

    .line 102
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0e004a

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setContentSize(F)V

    .line 103
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f110177

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setScreenNameColor(I)V

    .line 106
    :cond_1
    iget-boolean v1, p0, Lcom/twitter/android/bj;->b:Z

    if-eqz v1, :cond_3

    .line 107
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 108
    :goto_1
    sparse-switch v1, :sswitch_data_0

    .line 117
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v5

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/user/UserSocialView;->a(IILjava/lang/String;IZ)V

    .line 149
    :goto_2
    return-void

    .line 97
    :cond_2
    invoke-virtual {v0, v3, v3}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    goto :goto_0

    .line 107
    :cond_3
    const/4 v1, -0x1

    goto :goto_1

    .line 111
    :sswitch_0
    const v2, 0x7f020835

    .line 112
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 113
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v5

    .line 111
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/user/UserSocialView;->a(IILjava/lang/String;IZ)V

    goto :goto_2

    .line 124
    :cond_4
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 147
    :goto_3
    check-cast p1, Lcom/twitter/ui/user/BaseUserView;

    invoke-virtual {p1, v3}, Lcom/twitter/ui/user/BaseUserView;->setExtraInfo(Ljava/lang/String;)V

    goto :goto_2

    .line 126
    :sswitch_1
    if-nez v4, :cond_5

    .line 127
    const v0, 0x7f0a03a9

    new-array v1, v7, [Ljava/lang/Object;

    .line 128
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 127
    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 130
    :cond_5
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0009

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 132
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    .line 133
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v7

    .line 130
    invoke-virtual {v0, v1, v4, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 138
    :sswitch_2
    const v0, 0x7f0a08eb

    new-array v1, v7, [Ljava/lang/Object;

    .line 139
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 138
    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 108
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x28 -> :sswitch_0
    .end sparse-switch

    .line 124
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x28 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 25
    check-cast p3, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/bj;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/twitter/android/bj;->b:Z

    .line 163
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/twitter/android/bj;->g:Z

    .line 171
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/twitter/android/bj;->f:Z

    .line 175
    return-void
.end method
