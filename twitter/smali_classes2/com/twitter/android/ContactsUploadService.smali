.class public Lcom/twitter/android/ContactsUploadService;
.super Landroid/app/IntentService;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/k;


# static fields
.field private static a:Z

.field private static b:Z


# instance fields
.field private c:Lcom/twitter/library/util/g;

.field private d:Lcom/twitter/library/client/Session;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:I

.field private i:J

.field private j:I

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    sput-boolean v0, Lcom/twitter/android/ContactsUploadService;->a:Z

    .line 62
    sput-boolean v0, Lcom/twitter/android/ContactsUploadService;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    const-string/jumbo v0, "ab_upload_service"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 68
    iput v1, p0, Lcom/twitter/android/ContactsUploadService;->g:I

    .line 69
    iput v1, p0, Lcom/twitter/android/ContactsUploadService;->h:I

    .line 71
    iput v1, p0, Lcom/twitter/android/ContactsUploadService;->j:I

    .line 76
    return-void
.end method

.method public static a()J
    .locals 4

    .prologue
    .line 225
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v0

    const-string/jumbo v1, "fft"

    const-wide/16 v2, 0x0

    .line 226
    invoke-interface {v0, v1, v2, v3}, Lcqs;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 225
    return-wide v0
.end method

.method private static a(Ljava/lang/String;J)Lcom/twitter/metrics/n;
    .locals 3

    .prologue
    .line 180
    .line 181
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    sget-object v1, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 180
    invoke-static {p0, v0, p1, p2, v1}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)V
    .locals 2

    .prologue
    .line 230
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Lcqs;->a()Lcqs$b;

    move-result-object v0

    const-string/jumbo v1, "fft"

    .line 232
    invoke-interface {v0, v1, p0, p1}, Lcqs$b;->a(Ljava/lang/String;J)Lcqs$b;

    move-result-object v0

    .line 233
    invoke-interface {v0}, Lcqs$b;->a()V

    .line 234
    return-void
.end method

.method private a(Landroid/content/Intent;ZZ)V
    .locals 1

    .prologue
    .line 185
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 186
    invoke-static {p2}, Lcom/twitter/android/ContactsUploadService;->b(Z)V

    .line 187
    invoke-static {p3}, Lcom/twitter/android/ContactsUploadService;->a(Z)V

    .line 188
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 116
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ContactsUploadService;->c:Lcom/twitter/library/util/g;

    .line 117
    invoke-interface {v0, p1}, Lcom/twitter/library/util/g;->a(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v0

    move-object v1, v0

    .line 119
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/ContactsUploadService;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/ContactsUploadService;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":follow_friends:::resolvable"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 120
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 121
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 119
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 123
    iget-object v0, p0, Lcom/twitter/android/ContactsUploadService;->c:Lcom/twitter/library/util/g;

    invoke-interface {v0}, Lcom/twitter/library/util/g;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/twitter/android/ContactsUploadService;->c:Lcom/twitter/library/util/g;

    invoke-interface {v0}, Lcom/twitter/library/util/g;->c()V

    .line 126
    :cond_0
    invoke-direct {p0, v1}, Lcom/twitter/android/ContactsUploadService;->a(Ljava/util/Map;)V

    .line 127
    return-void

    .line 118
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method private a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 130
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v2

    .line 131
    const/16 v3, 0x32

    invoke-static {v2, v3}, Lcom/twitter/library/network/ab;->a(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/ContactsUploadService;->f:I

    .line 132
    iget v2, p0, Lcom/twitter/android/ContactsUploadService;->f:I

    if-lez v2, :cond_0

    .line 133
    const-string/jumbo v2, "contacts:timing:total:upload_contacts"

    iget-object v3, p0, Lcom/twitter/android/ContactsUploadService;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/twitter/android/ContactsUploadService;->a(Ljava/lang/String;J)Lcom/twitter/metrics/n;

    move-result-object v2

    .line 134
    invoke-virtual {v2}, Lcom/twitter/metrics/n;->i()V

    .line 136
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ContactsUploadService;->c:Lcom/twitter/library/util/g;

    iget-boolean v3, p0, Lcom/twitter/android/ContactsUploadService;->k:Z

    invoke-interface {v2, p1, p0, v3}, Lcom/twitter/library/util/g;->a(Ljava/util/Map;Lcom/twitter/library/util/k;Z)V

    .line 138
    new-instance v2, Lcom/twitter/util/a;

    iget-object v3, p0, Lcom/twitter/android/ContactsUploadService;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, p0, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v3, "addressbook_import_done"

    .line 139
    invoke-virtual {v2, v3, v0}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v2

    .line 140
    invoke-virtual {v2}, Lcom/twitter/util/a$a;->apply()V

    .line 141
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "upload_success_broadcast"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "lookup_complete"

    .line 142
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "pages"

    iget v4, p0, Lcom/twitter/android/ContactsUploadService;->f:I

    .line 143
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    .line 145
    iget v3, p0, Lcom/twitter/android/ContactsUploadService;->f:I

    if-lez v3, :cond_1

    .line 146
    const-string/jumbo v3, "page"

    iget v4, p0, Lcom/twitter/android/ContactsUploadService;->f:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 148
    :cond_1
    iget v3, p0, Lcom/twitter/android/ContactsUploadService;->j:I

    if-nez v3, :cond_2

    .line 149
    :goto_0
    invoke-direct {p0, v2, v1, v0}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/content/Intent;ZZ)V

    .line 150
    invoke-direct {p0}, Lcom/twitter/android/ContactsUploadService;->d()V

    .line 151
    return-void

    :cond_2
    move v0, v1

    .line 148
    goto :goto_0
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 237
    sput-boolean p0, Lcom/twitter/android/ContactsUploadService;->a:Z

    .line 238
    const-class v0, Lcom/twitter/android/ContactsUploadService;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 239
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 213
    invoke-static {p0, p2}, Lbmp;->a(Landroid/content/Context;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    const/4 v0, 0x0

    .line 221
    :goto_0
    return v0

    .line 217
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ContactsUploadService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "scribe_page_term"

    .line 218
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "extra_is_live_sync_experience"

    .line 219
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 217
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 220
    invoke-static {v0}, Lcom/twitter/android/ContactsUploadService;->b(Z)V

    goto :goto_0
.end method

.method public static b(Z)V
    .locals 1

    .prologue
    .line 242
    sput-boolean p0, Lcom/twitter/android/ContactsUploadService;->b:Z

    .line 243
    const-class v0, Lcom/twitter/android/ContactsUploadService;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 244
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 247
    sget-boolean v0, Lcom/twitter/android/ContactsUploadService;->a:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 251
    sget-boolean v0, Lcom/twitter/android/ContactsUploadService;->b:Z

    return v0
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 193
    iget-object v0, p0, Lcom/twitter/android/ContactsUploadService;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 194
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/ContactsUploadService;->e:Ljava/lang/String;

    aput-object v4, v1, v6

    const-string/jumbo v4, "follow_friends::forward_lookup:request"

    aput-object v4, v1, v7

    .line 195
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget v1, p0, Lcom/twitter/android/ContactsUploadService;->f:I

    int-to-long v4, v1

    .line 196
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 194
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 197
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/ContactsUploadService;->e:Ljava/lang/String;

    aput-object v4, v1, v6

    const-string/jumbo v4, "follow_friends::forward_lookup:failure"

    aput-object v4, v1, v7

    .line 198
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget v1, p0, Lcom/twitter/android/ContactsUploadService;->j:I

    int-to-long v4, v1

    .line 199
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 197
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 200
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/ContactsUploadService;->e:Ljava/lang/String;

    aput-object v4, v1, v6

    const-string/jumbo v4, "follow_friends::forward_lookup:count"

    aput-object v4, v1, v7

    .line 201
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget v1, p0, Lcom/twitter/android/ContactsUploadService;->g:I

    int-to-long v4, v1

    .line 202
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 200
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 203
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/ContactsUploadService;->e:Ljava/lang/String;

    aput-object v4, v1, v6

    const-string/jumbo v4, "import_addressbook::import:done"

    aput-object v4, v1, v7

    .line 204
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 205
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/twitter/android/ContactsUploadService;->i:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 203
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 206
    const-string/jumbo v0, "contacts:timing:total:upload_contacts"

    invoke-static {v0, v2, v3}, Lcom/twitter/android/ContactsUploadService;->a(Ljava/lang/String;J)Lcom/twitter/metrics/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/metrics/n;->j()V

    .line 208
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/android/ContactsUploadService;->a(J)V

    .line 209
    return-void
.end method


# virtual methods
.method public a(Lbbw;Lcom/twitter/library/service/u;)V
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 155
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    iget v0, p0, Lcom/twitter/android/ContactsUploadService;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/ContactsUploadService;->j:I

    .line 159
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p1, Lbbw;->o:Landroid/os/Bundle;

    .line 161
    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 162
    const-string/jumbo v2, "pages"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 163
    iget v3, p0, Lcom/twitter/android/ContactsUploadService;->g:I

    const-string/jumbo v4, "num_users"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v3

    iput v0, p0, Lcom/twitter/android/ContactsUploadService;->g:I

    .line 164
    iget v0, p0, Lcom/twitter/android/ContactsUploadService;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/ContactsUploadService;->h:I

    .line 165
    iget v0, p0, Lcom/twitter/android/ContactsUploadService;->h:I

    if-eq v0, v2, :cond_1

    .line 166
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "upload_success_broadcast"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "page"

    .line 167
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pages"

    .line 168
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 169
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/content/Intent;ZZ)V

    .line 172
    :cond_1
    return-void
.end method

.method public a(Lbbz;Lcom/twitter/library/service/u;)V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 81
    if-nez p1, :cond_1

    .line 83
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "ContactsUploadService intent is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const-string/jumbo v0, "scribe_page_term"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ContactsUploadService;->e:Ljava/lang/String;

    .line 87
    invoke-static {v5}, Lcom/twitter/android/ContactsUploadService;->a(Z)V

    .line 88
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ContactsUploadService;->d:Lcom/twitter/library/client/Session;

    .line 89
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ContactsUploadService;->i:J

    .line 90
    const-string/jumbo v0, "ab_upload"

    const-string/jumbo v1, "Starting AB Upload.."

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/twitter/android/ContactsUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/h;->a(Landroid/content/Context;)Lcom/twitter/library/util/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ContactsUploadService;->c:Lcom/twitter/library/util/g;

    .line 93
    iget-object v0, p0, Lcom/twitter/android/ContactsUploadService;->c:Lcom/twitter/library/util/g;

    invoke-interface {v0}, Lcom/twitter/library/util/g;->a()Landroid/database/Cursor;

    move-result-object v1

    .line 95
    const-string/jumbo v0, "extra_is_live_sync_experience"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ContactsUploadService;->k:Z

    .line 97
    iget-boolean v0, p0, Lcom/twitter/android/ContactsUploadService;->k:Z

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 98
    :cond_2
    invoke-direct {p0, v1}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/database/Cursor;)V

    .line 110
    :goto_1
    if-eqz v1, :cond_0

    .line 111
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 102
    :cond_3
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/ContactsUploadService;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/ContactsUploadService;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":follow_friends:::resolvable"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 103
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v2, 0x0

    .line 104
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 102
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 105
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "upload_success_broadcast"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "lookup_complete"

    .line 106
    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 105
    invoke-direct {p0, v0, v5, v6}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/content/Intent;ZZ)V

    .line 107
    invoke-direct {p0}, Lcom/twitter/android/ContactsUploadService;->d()V

    goto :goto_1
.end method
