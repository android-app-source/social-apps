.class public Lcom/twitter/android/NameEntryFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/app/onboarding/flowstep/common/a;
.implements Lcom/twitter/app/onboarding/flowstep/common/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/NameEntryFragment$a;,
        Lcom/twitter/android/NameEntryFragment$NameLoaderState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/abs/AbsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/text/TextWatcher;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/twitter/app/onboarding/flowstep/common/a;",
        "Lcom/twitter/app/onboarding/flowstep/common/h",
        "<",
        "Lcom/twitter/model/onboarding/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[I

.field private static final c:[I


# instance fields
.field a:Lcom/twitter/ui/widget/TwitterEditText;

.field private d:Lcom/twitter/android/NameEntryFragment$a;

.field private e:Lcom/twitter/android/w;

.field private f:Lcom/twitter/android/NameEntryFragment$NameLoaderState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/NameEntryFragment;->b:[I

    .line 46
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010471

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/android/NameEntryFragment;->c:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 53
    sget-object v0, Lcom/twitter/android/NameEntryFragment$NameLoaderState;->a:Lcom/twitter/android/NameEntryFragment$NameLoaderState;

    iput-object v0, p0, Lcom/twitter/android/NameEntryFragment;->f:Lcom/twitter/android/NameEntryFragment$NameLoaderState;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/NameEntryFragment;)Lcom/twitter/android/ValidationState$a;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 178
    if-eqz p0, :cond_0

    .line 179
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 180
    invoke-virtual {p0}, Landroid/widget/EditText;->requestFocus()Z

    .line 182
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/NameEntryFragment;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/twitter/android/NameEntryFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 174
    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/twitter/android/NameEntryFragment;->c:[I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    .line 175
    return-void

    .line 174
    :cond_0
    sget-object v0, Lcom/twitter/android/NameEntryFragment;->b:[I

    goto :goto_0
.end method

.method private b(I)Lcom/twitter/android/ValidationState$State;
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/NameEntryFragment;->a(Z)V

    .line 162
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 163
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    .line 165
    if-lt v0, p1, :cond_0

    .line 166
    sget-object v0, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    .line 170
    :goto_0
    return-object v0

    .line 168
    :cond_0
    sget-object v0, Lcom/twitter/android/ValidationState$State;->a:Lcom/twitter/android/ValidationState$State;

    goto :goto_0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 122
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->T:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "android.permission.READ_CONTACTS"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/twitter/android/NameEntryFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 125
    :cond_0
    return-void
.end method

.method private h()Lcom/twitter/android/ValidationState$a;
    .locals 1

    .prologue
    .line 245
    const-class v0, Lcom/twitter/android/ValidationState$a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/NameEntryFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ValidationState$a;

    return-object v0
.end method

.method private i()Lars;
    .locals 1

    .prologue
    .line 249
    const-class v0, Lars;

    invoke-virtual {p0, v0}, Lcom/twitter/android/NameEntryFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lars;

    return-object v0
.end method

.method private j()Lcom/twitter/android/au;
    .locals 1

    .prologue
    .line 253
    const-class v0, Lcom/twitter/android/au;

    invoke-virtual {p0, v0}, Lcom/twitter/android/NameEntryFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/au;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 63
    const v0, 0x7f040227

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 65
    const v0, 0x7f1303a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 66
    const v2, 0x7f0a05c3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 68
    const v0, 0x7f13055e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    .line 70
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 72
    if-nez p2, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/twitter/android/NameEntryFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/NameEntryFragment;->a(Lcom/twitter/app/common/base/b;Landroid/widget/EditText;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-static {v0}, Lcom/twitter/android/NameEntryFragment;->a(Landroid/widget/EditText;)V

    .line 76
    return-object v1
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const-string/jumbo v0, "display_name"

    .line 195
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 194
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 198
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ValidationState;

    sget-object v2, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->d:Lcom/twitter/android/NameEntryFragment$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/NameEntryFragment$a;->a(I)V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-static {v0}, Lcom/twitter/android/NameEntryFragment;->a(Landroid/widget/EditText;)V

    .line 204
    :cond_0
    sget-object v0, Lcom/twitter/android/NameEntryFragment$NameLoaderState;->c:Lcom/twitter/android/NameEntryFragment$NameLoaderState;

    iput-object v0, p0, Lcom/twitter/android/NameEntryFragment;->f:Lcom/twitter/android/NameEntryFragment$NameLoaderState;

    .line 205
    return-void
.end method

.method public a(Lcom/twitter/android/w;)V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p1, Lcom/twitter/android/w;->a:Ljava/lang/String;

    .line 236
    invoke-virtual {p0}, Lcom/twitter/android/NameEntryFragment;->Y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setError(Ljava/lang/CharSequence;)V

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/NameEntryFragment;->e:Lcom/twitter/android/w;

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/NameEntryFragment;->e:Lcom/twitter/android/w;

    goto :goto_0
.end method

.method a(Lcom/twitter/app/common/base/b;Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 107
    const-string/jumbo v0, "extra_flow_data"

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    const-string/jumbo v0, "extra_flow_data"

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FlowData;

    .line 109
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 110
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 111
    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 118
    :goto_1
    return-void

    .line 109
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 113
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->g()V

    goto :goto_1

    .line 116
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->g()V

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 148
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 149
    invoke-direct {p0, v2}, Lcom/twitter/android/NameEntryFragment;->b(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    .line 150
    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->d:Lcom/twitter/android/NameEntryFragment$a;

    invoke-virtual {v1, v2}, Lcom/twitter/android/NameEntryFragment$a;->a(I)V

    .line 155
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ValidationState;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v1, v2}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    .line 158
    :cond_0
    return-void

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->d:Lcom/twitter/android/NameEntryFragment$a;

    invoke-virtual {v1, v2}, Lcom/twitter/android/NameEntryFragment$a;->removeMessages(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 88
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 90
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/ValidationState$a;->e()Lcom/twitter/android/ValidationState;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_3

    .line 92
    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    invoke-direct {p0, v2}, Lcom/twitter/android/NameEntryFragment;->a(Z)V

    .line 101
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->e:Lcom/twitter/android/w;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->e:Lcom/twitter/android/w;

    invoke-virtual {p0, v0}, Lcom/twitter/android/NameEntryFragment;->a(Lcom/twitter/android/w;)V

    .line 104
    :cond_1
    return-void

    .line 94
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/twitter/android/NameEntryFragment;->b(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->d:Lcom/twitter/android/NameEntryFragment$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/NameEntryFragment$a;->a(I)V

    goto :goto_0

    .line 98
    :cond_3
    invoke-direct {p0, v2}, Lcom/twitter/android/NameEntryFragment;->b(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->d:Lcom/twitter/android/NameEntryFragment$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/NameEntryFragment$a;->a(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/twitter/android/NameEntryFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->i()Lars;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lars;->c(Ljava/lang/String;)V

    .line 214
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->j()Lcom/twitter/android/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/au;->bl_()V

    .line 216
    :cond_0
    return-void
.end method

.method public e()Lcom/twitter/model/onboarding/a;
    .locals 2

    .prologue
    .line 231
    new-instance v0, Lcom/twitter/model/onboarding/g;

    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/g;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    .line 224
    :goto_0
    return v0

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const v1, 0x7f0a08bf

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 224
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    new-instance v0, Lcom/twitter/android/NameEntryFragment$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/NameEntryFragment$a;-><init>(Lcom/twitter/android/NameEntryFragment;)V

    iput-object v0, p0, Lcom/twitter/android/NameEntryFragment;->d:Lcom/twitter/android/NameEntryFragment$a;

    .line 59
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    sget-object v0, Lcom/twitter/android/NameEntryFragment$NameLoaderState;->b:Lcom/twitter/android/NameEntryFragment$NameLoaderState;

    iput-object v0, p0, Lcom/twitter/android/NameEntryFragment;->f:Lcom/twitter/android/NameEntryFragment$NameLoaderState;

    .line 187
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/ac;->a(Landroid/content/Context;)Lcom/twitter/android/util/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/ab;->c()Landroid/support/v4/content/Loader;

    move-result-object v0

    return-object v0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 129
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    .line 130
    invoke-direct {p0}, Lcom/twitter/android/NameEntryFragment;->j()Lcom/twitter/android/au;

    move-result-object v1

    .line 131
    const v2, 0x7f13055e

    if-ne v0, v2, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    invoke-interface {v1}, Lcom/twitter/android/au;->n_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-interface {v1}, Lcom/twitter/android/au;->b()V

    .line 133
    const/4 v0, 0x1

    .line 135
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/NameEntryFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 144
    return-void
.end method

.method public q_()V
    .locals 3

    .prologue
    .line 81
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 83
    iget-object v0, p0, Lcom/twitter/android/NameEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 84
    return-void
.end method
