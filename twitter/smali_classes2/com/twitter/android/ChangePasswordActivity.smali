.class public Lcom/twitter/android/ChangePasswordActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public a:Z

.field public b:Lcom/twitter/library/service/s;

.field private c:Lcom/twitter/ui/widget/TwitterEditText;

.field private d:Lcom/twitter/ui/widget/TwitterEditText;

.field private e:Lcom/twitter/ui/widget/TwitterEditText;

.field private f:Landroid/widget/Button;

.field private g:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Z

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 271
    new-instance v0, Lbbb;

    iget-object v2, p0, Lcom/twitter/android/ChangePasswordActivity;->g:Lcom/twitter/library/client/Session;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lbbb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbbb;->g(I)Lcom/twitter/library/service/s;

    .line 273
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ChangePasswordActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 274
    iget-boolean v1, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Z

    if-eqz v1, :cond_0

    .line 275
    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Lcom/twitter/library/service/s;

    .line 277
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 280
    invoke-direct {p0, p1}, Lcom/twitter/android/ChangePasswordActivity;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    const v2, 0x7f0a08c1

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 290
    :goto_0
    return v0

    .line 283
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 284
    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    const v2, 0x7f0a0632

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0

    .line 286
    :cond_1
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 287
    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    const v2, 0x7f0a05c6

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0

    .line 290
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 264
    const-string/jumbo v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    .line 257
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    .line 258
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    .line 259
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    .line 260
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 256
    :goto_0
    return v0

    .line 260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 297
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 299
    iget-object v2, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 300
    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 301
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    const v0, 0x7f040061

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 145
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 146
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 147
    return-object p2
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 84
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->g:Lcom/twitter/library/client/Session;

    .line 85
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    iget-wide v4, v3, Lcom/twitter/library/service/v;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 92
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v3

    .line 93
    const/4 v1, 0x0

    .line 94
    if-eqz v3, :cond_1

    .line 95
    iget-object v1, v3, Lcom/twitter/network/l;->b:Ljava/lang/String;

    .line 97
    :cond_1
    iget-object v3, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    invoke-static {v3}, Lcom/twitter/library/network/ab;->a(Landroid/os/Bundle;)[I

    move-result-object v3

    .line 99
    const-string/jumbo v4, "OK"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 100
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    iget v0, v0, Lcom/twitter/network/l;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_3

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Lcom/twitter/ui/widget/TwitterEditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 104
    const-string/jumbo v0, "settings:change_password::change_password:success"

    .line 105
    const v1, 0x7f0a062f

    .line 137
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 138
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    iget-wide v4, v3, Lcom/twitter/library/service/v;->c:J

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v2

    invoke-virtual {v1, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 107
    :cond_3
    if-eqz v3, :cond_4

    array-length v0, v3

    if-nez v0, :cond_5

    :cond_4
    move v0, v2

    .line 109
    :goto_2
    sparse-switch v0, :sswitch_data_0

    .line 131
    const-string/jumbo v0, "settings:change_password::change_password:failure"

    .line 132
    const v1, 0x7f0a062b

    goto :goto_1

    .line 107
    :cond_5
    aget v0, v3, v2

    goto :goto_2

    .line 111
    :sswitch_0
    const-string/jumbo v0, "settings:change_password::change_password:wrong_old"

    .line 112
    const v1, 0x7f0a062e

    .line 113
    goto :goto_1

    .line 116
    :sswitch_1
    const-string/jumbo v0, "settings:change_password::change_password:minimum_length"

    .line 117
    const v1, 0x7f0a08c1

    .line 118
    goto :goto_1

    .line 121
    :sswitch_2
    const-string/jumbo v0, "settings:change_password::change_password:weak"

    .line 122
    const v1, 0x7f0a062c

    .line 123
    goto :goto_1

    .line 126
    :sswitch_3
    const-string/jumbo v0, "settings:change_password::change_password:mismatch"

    .line 127
    const v1, 0x7f0a0632

    .line 128
    goto :goto_1

    .line 109
    nop

    :sswitch_data_0
    .sparse-switch
        0x3c -> :sswitch_3
        0x3e -> :sswitch_1
        0x72 -> :sswitch_0
        0xee -> :sswitch_2
    .end sparse-switch
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    const v1, 0x7f0a08c1

    .line 219
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 220
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 232
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/ChangePasswordActivity;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 233
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 227
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0

    .line 229
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 153
    new-instance v0, Lcom/twitter/android/j;

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/j;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0}, Lcom/twitter/android/j;->a()Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "ChangePasswordActivity requires a target account"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->g:Lcom/twitter/library/client/Session;

    .line 161
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->g:Lcom/twitter/library/client/Session;

    .line 162
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "settings:change_password:::impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 164
    const v0, 0x7f1301df

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Lcom/twitter/ui/widget/TwitterEditText;

    .line 165
    const v0, 0x7f1301e0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    .line 166
    const v0, 0x7f1301e1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    .line 167
    const v0, 0x7f1301e2

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/Button;

    .line 168
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 171
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 172
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 175
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 176
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 178
    const v0, 0x7f1301e3

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 179
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 186
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 187
    const v1, 0x7f1301e2

    if-ne v0, v1, :cond_1

    .line 188
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "settings:change_password::change_password:click"

    aput-object v2, v1, v4

    .line 189
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 188
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 190
    invoke-direct {p0}, Lcom/twitter/android/ChangePasswordActivity;->j()V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    const v1, 0x7f1301e3

    if-ne v0, v1, :cond_0

    .line 192
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "settings:change_password::forgot_password:click"

    aput-object v2, v1, v4

    .line 193
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 192
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 194
    const-string/jumbo v0, "native_pw_reset_from_change_pw_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 195
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PasswordResetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 196
    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    .line 197
    if-eqz v1, :cond_2

    .line 198
    const-string/jumbo v2, "account_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    :cond_2
    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->startActivity(Landroid/content/Intent;)V

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->finish()V

    goto :goto_0

    .line 203
    :cond_3
    const-string/jumbo v0, ""

    const v1, 0x7f0a0be9

    invoke-static {p0, v0, v1}, Lcom/twitter/android/util/o;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const v3, 0x7f0a08c1

    const/4 v2, 0x6

    .line 237
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 238
    const v1, 0x7f1301e1

    if-ne v0, v1, :cond_2

    .line 239
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    .line 241
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    if-ge v0, v2, :cond_1

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 253
    :cond_1
    :goto_0
    return-void

    .line 245
    :cond_2
    const v1, 0x7f1301e0

    if-ne v0, v1, :cond_1

    .line 246
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    .line 248
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    if-ge v0, v2, :cond_1

    .line 249
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method
