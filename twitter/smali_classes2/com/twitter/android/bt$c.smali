.class final Lcom/twitter/android/bt$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/cx$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/bt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 1173
    sget v0, Lbtv;->f:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/database/Cursor;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 1261
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1178
    sget v0, Lbtv;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1183
    sget v0, Lbtv;->h:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1188
    sget v0, Lbtv;->i:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1193
    sget v0, Lbtv;->p:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1194
    if-nez v0, :cond_0

    .line 1195
    sget v0, Lbtv;->o:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1197
    :cond_0
    return-object v0
.end method

.method public f(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 1202
    sget v0, Lbtv;->j:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 1208
    sget v0, Lbtv;->j:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(Landroid/database/Cursor;)Lcgi;
    .locals 2

    .prologue
    .line 1214
    sget v0, Lbtv;->l:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    return-object v0
.end method

.method public i(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 1220
    sget v0, Lbtv;->k:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1225
    sget v0, Lbtv;->n:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 1230
    sget v0, Lbtv;->m:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public l(Landroid/database/Cursor;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1236
    sget v0, Lbtv;->e:I

    .line 1237
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    .line 1240
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1256
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    return v0

    .line 1242
    :cond_0
    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 1243
    check-cast v0, Ljava/lang/Boolean;

    goto :goto_0

    .line 1245
    :cond_1
    new-instance v2, Lcpb;

    invoke-direct {v2}, Lcpb;-><init>()V

    .line 1246
    check-cast p1, Lcom/twitter/library/provider/p;

    .line 1247
    const-string/jumbo v3, "start index: "

    invoke-virtual {p1}, Lcom/twitter/library/provider/p;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 1248
    const-string/jumbo v3, "end index: "

    invoke-virtual {p1}, Lcom/twitter/library/provider/p;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 1249
    const-string/jumbo v3, "current position: "

    invoke-virtual {p1}, Lcom/twitter/library/provider/p;->getPosition()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 1250
    const-string/jumbo v3, "top is a type of: "

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 1251
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v3, "TLNA-1259: Cannot be cast to Boolean."

    invoke-direct {v0, v3}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    .line 1253
    invoke-static {v2}, Lcpd;->c(Lcpb;)V

    move-object v0, v1

    .line 1254
    goto :goto_0

    .line 1256
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
