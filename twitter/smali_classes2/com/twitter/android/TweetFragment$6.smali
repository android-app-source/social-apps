.class Lcom/twitter/android/TweetFragment$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/TweetFragment;->a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Landroid/app/Activity;

.field final synthetic c:Lcom/twitter/model/core/Tweet;

.field final synthetic d:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;JLandroid/app/Activity;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 1894
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$6;->d:Lcom/twitter/android/TweetFragment;

    iput-wide p2, p0, Lcom/twitter/android/TweetFragment$6;->a:J

    iput-object p4, p0, Lcom/twitter/android/TweetFragment$6;->b:Landroid/app/Activity;

    iput-object p5, p0, Lcom/twitter/android/TweetFragment$6;->c:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1897
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$6;->d:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$6;->d:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->f(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$a;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/TweetFragment$a;->b(Lcom/twitter/android/TweetFragment$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1898
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$6;->d:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->f(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/TweetFragment$6;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/TweetFragment$a;->a(J)I

    move-result v0

    .line 1899
    if-lez v0, :cond_0

    .line 1900
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$6;->b:Landroid/app/Activity;

    const v1, 0x7f0a0764

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1901
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$6;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$6;->b:Landroid/app/Activity;

    const v3, 0x7f1302e4

    .line 1903
    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    .line 1902
    invoke-static {v1, v2, v0, v3}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    const v1, 0x7f0a0763

    new-instance v2, Lcom/twitter/android/TweetFragment$6$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/TweetFragment$6$1;-><init>(Lcom/twitter/android/TweetFragment$6;)V

    .line 1906
    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 1912
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 1915
    :cond_0
    return-void
.end method
