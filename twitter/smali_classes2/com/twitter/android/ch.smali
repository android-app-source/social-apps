.class public Lcom/twitter/android/ch;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/TimelineMessageView$a;


# instance fields
.field private final a:Landroid/view/animation/Interpolator;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Lcom/twitter/library/client/v;

.field private final e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/p;Lcom/twitter/library/client/v;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/twitter/ui/anim/h;->b()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ch;->a:Landroid/view/animation/Interpolator;

    .line 51
    iput-object p1, p0, Lcom/twitter/android/ch;->b:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/twitter/android/ch;->c:Lcom/twitter/library/client/p;

    .line 53
    iput-object p3, p0, Lcom/twitter/android/ch;->d:Lcom/twitter/library/client/v;

    .line 54
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    invoke-static {p4, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/ch;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ch;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/ch;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/View;Ljava/lang/String;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/r;)V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0, p1}, Lcom/twitter/android/ch;->a(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    .line 101
    new-instance v1, Lcom/twitter/android/ch$1;

    invoke-direct {v1, p0, p4, p2, p3}, Lcom/twitter/android/ch$1;-><init>(Lcom/twitter/android/ch;Lcom/twitter/model/timeline/r;Ljava/lang/String;Lcom/twitter/android/timeline/bk;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 111
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ch;Lcom/twitter/model/timeline/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/ch;->a(Lcom/twitter/model/timeline/r;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ch;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/ch;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/library/widget/TimelineMessageView;Lcom/twitter/model/timeline/l;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 71
    const v0, 0x7f13007e

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TimelineMessageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 72
    if-eqz v0, :cond_0

    .line 73
    iget-object v1, v0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    const-string/jumbo v2, "click"

    invoke-direct {p0, v1, p3, v2}, Lcom/twitter/android/ch;->a(Lcom/twitter/model/timeline/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-boolean v1, p2, Lcom/twitter/model/timeline/l;->c:Z

    if-eqz v1, :cond_1

    .line 75
    iget-object v1, p2, Lcom/twitter/model/timeline/l;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/twitter/android/ch;->a(Landroid/view/View;Ljava/lang/String;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/r;)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v0, p2, Lcom/twitter/model/timeline/l;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/ch;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/timeline/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 121
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 122
    iput-object p1, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 123
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v0, p0, Lcom/twitter/android/ch;->d:Lcom/twitter/library/client/v;

    .line 124
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/twitter/android/ch;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 125
    invoke-virtual {v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/twitter/android/ch;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x3

    aput-object p2, v3, v0

    const/4 v0, 0x4

    aput-object p3, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 127
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 128
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 129
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 92
    if-eqz p1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/twitter/android/ch;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/ch;->d:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v8, p0, Lcom/twitter/android/ch;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/twitter/android/ch;->b:Landroid/content/Context;

    .line 94
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v10

    move-object v3, p1

    move-object v6, v2

    move-object v7, v2

    .line 93
    invoke-static/range {v1 .. v10}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLjava/lang/String;)V

    .line 96
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/ch;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/ch;->d:Lcom/twitter/library/client/v;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/ch;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/ch;->c:Lcom/twitter/library/client/p;

    return-object v0
.end method


# virtual methods
.method a(Landroid/view/View;)Landroid/animation/Animator;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 116
    const-wide/16 v0, 0x190

    iget-object v2, p0, Lcom/twitter/android/ch;->a:Landroid/view/animation/Interpolator;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/e;->a(Landroid/view/View;JLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/widget/TimelineMessageView;)V
    .locals 4

    .prologue
    .line 86
    const v0, 0x7f13007e

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TimelineMessageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 87
    iget-object v1, v0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    const-string/jumbo v2, "dismiss"

    const-string/jumbo v3, "click"

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/ch;->a(Lcom/twitter/model/timeline/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const/4 v2, 0x0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/bk;

    iget-object v0, v0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    invoke-direct {p0, p1, v2, v1, v0}, Lcom/twitter/android/ch;->a(Landroid/view/View;Ljava/lang/String;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/r;)V

    .line 89
    return-void
.end method

.method public a(Lcom/twitter/library/widget/TimelineMessageView;Lcom/twitter/model/timeline/l;)V
    .locals 1

    .prologue
    .line 60
    const-string/jumbo v0, "primary_action"

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/ch;->a(Lcom/twitter/library/widget/TimelineMessageView;Lcom/twitter/model/timeline/l;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public b(Lcom/twitter/library/widget/TimelineMessageView;Lcom/twitter/model/timeline/l;)V
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "secondary_action"

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/ch;->a(Lcom/twitter/library/widget/TimelineMessageView;Lcom/twitter/model/timeline/l;Ljava/lang/String;)V

    .line 67
    return-void
.end method
