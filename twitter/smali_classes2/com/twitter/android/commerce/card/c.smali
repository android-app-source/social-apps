.class public Lcom/twitter/android/commerce/card/c;
.super Lcom/twitter/android/revenue/card/ae;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/q$a;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field protected c:Lcom/twitter/media/ui/image/MediaImageView;

.field protected d:Landroid/widget/TextView;

.field protected e:Landroid/widget/TextView;

.field protected f:Landroid/view/View;

.field protected g:Landroid/widget/TextView;

.field protected h:Landroid/view/View;

.field private final m:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

.field private final n:Lcom/twitter/library/util/s;

.field private o:Lcar;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/card/m;Lcom/twitter/android/commerce/card/CommerceCardActionHandler;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/twitter/android/revenue/card/ae;-><init>(Lcom/twitter/android/revenue/card/m;)V

    .line 55
    new-instance v0, Lcom/twitter/android/commerce/card/c$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/commerce/card/c$1;-><init>(Lcom/twitter/android/commerce/card/c;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    .line 61
    iput-object p2, p0, Lcom/twitter/android/commerce/card/c;->m:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    .line 62
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 7

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->l()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->f()Lcom/twitter/library/card/CardContext;

    move-result-object v0

    if-nez v0, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/c;->f()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v1

    .line 184
    const/4 v4, 0x0

    .line 186
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 212
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->m:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    iget-object v2, p0, Lcom/twitter/android/commerce/card/c;->o:Lcar;

    iget-object v3, p0, Lcom/twitter/android/commerce/card/c;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v3}, Lcom/twitter/android/revenue/card/m;->l()Landroid/app/Activity;

    move-result-object v3

    sget-object v5, Lcom/twitter/android/commerce/card/CommerceCardActionHandler$CommerceCardType;->a:Lcom/twitter/android/commerce/card/CommerceCardActionHandler$CommerceCardType;

    iget-object v6, p0, Lcom/twitter/android/commerce/card/c;->l:Lcom/twitter/android/revenue/card/m;

    .line 213
    invoke-virtual {v6}, Lcom/twitter/android/revenue/card/m;->f()Lcom/twitter/library/card/CardContext;

    move-result-object v6

    .line 212
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;Lcar;Landroid/app/Activity;Ljava/lang/String;Lcom/twitter/android/commerce/card/CommerceCardActionHandler$CommerceCardType;Lcom/twitter/library/card/CardContext;)V

    goto :goto_0

    .line 188
    :pswitch_1
    const-string/jumbo v4, "buynow_card_cta_click"

    goto :goto_1

    .line 192
    :pswitch_2
    const-string/jumbo v4, "buynow_card_primary_image_click"

    goto :goto_1

    .line 196
    :pswitch_3
    const-string/jumbo v4, "buynow_card_item_title_click"

    goto :goto_1

    .line 200
    :pswitch_4
    const-string/jumbo v4, "buynow_card_item_title_click"

    goto :goto_1

    .line 204
    :pswitch_5
    const-string/jumbo v4, "buynow_card_container_click"

    goto :goto_1

    .line 186
    :pswitch_data_0
    .packed-switch 0x7f13021a
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lcar;)V
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->d:Landroid/widget/TextView;

    sget-object v1, Lcom/twitter/android/revenue/card/h;->b:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 118
    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->d:Landroid/widget/TextView;

    const-string/jumbo v0, "item_title"

    const-class v2, Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 121
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/commerce/card/c;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/commerce/card/c;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method private b(Lcar;)V
    .locals 5

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->e:Landroid/widget/TextView;

    sget-object v1, Lcom/twitter/android/revenue/card/h;->b:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 127
    const-string/jumbo v0, "variant1_price"

    const-class v1, Ljava/lang/String;

    .line 128
    invoke-virtual {p1, v0, v1}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 127
    invoke-static {v0}, Lcom/twitter/android/commerce/util/b;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 130
    const-string/jumbo v0, "merchant_name"

    const-class v2, Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    .line 134
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/commerce/card/c;->a:Landroid/content/Context;

    const v4, 0x7f0a0115

    .line 135
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    .line 136
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 137
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 142
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->f:Landroid/view/View;

    const-string/jumbo v1, "button"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 163
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 164
    return-void
.end method

.method private c(Lcar;)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 146
    const-string/jumbo v0, "item_image"

    invoke-static {v0, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    const v2, 0x3ff47ae1    # 1.91f

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 149
    if-eqz v0, :cond_0

    .line 150
    iget v1, v0, Lcas;->b:I

    iget v2, v0, Lcas;->c:I

    if-gt v1, v2, :cond_1

    .line 151
    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    sget-object v2, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->a:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 155
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 156
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 159
    :cond_0
    return-void

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    sget-object v2, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 168
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->g:Landroid/widget/TextView;

    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 175
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 94
    invoke-super {p0}, Lcom/twitter/android/revenue/card/ae;->a()V

    .line 95
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/commerce/card/c;->j:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 99
    :cond_0
    return-void
.end method

.method public a(JLcar;)V
    .locals 3

    .prologue
    .line 103
    iput-object p3, p0, Lcom/twitter/android/commerce/card/c;->o:Lcar;

    .line 104
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->h()Lcom/twitter/android/card/d;

    move-result-object v1

    const-string/jumbo v0, "_card_data"

    const-class v2, Ljava/lang/String;

    invoke-virtual {p3, v0, v2}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 107
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/c;->a(Lcar;)V

    .line 108
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/c;->b(Lcar;)V

    .line 109
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/c;->c(Lcar;)V

    .line 110
    invoke-direct {p0}, Lcom/twitter/android/commerce/card/c;->c()V

    .line 111
    invoke-direct {p0}, Lcom/twitter/android/commerce/card/c;->d()V

    .line 112
    invoke-direct {p0}, Lcom/twitter/android/commerce/card/c;->e()V

    .line 113
    return-void
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 4

    .prologue
    .line 66
    iput-object p1, p0, Lcom/twitter/android/commerce/card/c;->a:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/twitter/android/commerce/card/c;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 69
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04022e

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    const v1, 0x7f130569

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 73
    const v1, 0x7f040071

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 74
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 76
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    const v1, 0x7f13056a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->h:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->n:Lcom/twitter/library/util/s;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/c;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    const v1, 0x7f13021f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->d:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    const v1, 0x7f130220

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->e:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    const v1, 0x7f13021c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->c:Lcom/twitter/media/ui/image/MediaImageView;

    .line 82
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    const v1, 0x7f130215

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->g:Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lcom/twitter/android/commerce/card/c;->i:Landroid/view/View;

    const v1, 0x7f13021e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/c;->f:Landroid/view/View;

    .line 84
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/ae;->a(Lcom/twitter/library/card/z$a;)V

    .line 89
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/commerce/card/c;->j:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 90
    return-void
.end method
