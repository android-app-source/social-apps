.class public Lcom/twitter/android/commerce/card/a;
.super Lcom/twitter/android/revenue/card/ae;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/q$a;


# instance fields
.field protected final a:I

.field protected b:Lcom/twitter/media/ui/image/MediaImageView;

.field protected c:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field private final d:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

.field private final e:Lcom/twitter/library/util/s;

.field private f:Lcar;

.field private g:Lcom/twitter/ui/widget/TwitterButton;

.field private h:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/view/View;

.field private p:Ljava/lang/String;

.field private q:Landroid/view/View;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/revenue/card/m;)V
    .locals 5

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/twitter/android/revenue/card/ae;-><init>(Lcom/twitter/android/revenue/card/m;)V

    .line 58
    new-instance v0, Lcom/twitter/android/commerce/card/a$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/commerce/card/a$1;-><init>(Lcom/twitter/android/commerce/card/a;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->e:Lcom/twitter/library/util/s;

    .line 64
    const v0, 0x7f040070

    iput v0, p0, Lcom/twitter/android/commerce/card/a;->a:I

    .line 65
    new-instance v0, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->l:Lcom/twitter/android/revenue/card/m;

    .line 66
    invoke-virtual {v1}, Lcom/twitter/android/revenue/card/m;->g()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/commerce/card/a;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v2}, Lcom/twitter/android/revenue/card/m;->i()Lcom/twitter/android/card/b;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/commerce/card/a;->l:Lcom/twitter/android/revenue/card/m;

    .line 67
    invoke-virtual {v3}, Lcom/twitter/android/revenue/card/m;->h()Lcom/twitter/android/card/d;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/commerce/card/a;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v4}, Lcom/twitter/android/revenue/card/m;->m()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;-><init>(Landroid/content/Context;Lcom/twitter/android/card/b;Lcom/twitter/android/card/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->d:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    .line 68
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 78
    invoke-super {p0}, Lcom/twitter/android/revenue/card/ae;->a()V

    .line 80
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 84
    :cond_0
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/commerce/card/a;->j:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 85
    return-void
.end method

.method public a(JLcar;)V
    .locals 3

    .prologue
    .line 104
    iput-object p3, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    .line 105
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/a;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->p:Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->h()Lcom/twitter/android/card/d;

    move-result-object v1

    const-string/jumbo v0, "_card_data"

    const-class v2, Ljava/lang/String;

    invoke-virtual {p3, v0, v2}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/commerce/card/a;->a(Lcar;)V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/commerce/card/a;->b(Lcar;)V

    .line 111
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/commerce/card/a;->c(Lcar;)V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/commerce/card/a;->d(Lcar;)V

    .line 113
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/commerce/card/a;->e(Lcar;)V

    .line 114
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/a;->e()V

    .line 115
    return-void
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 4

    .prologue
    .line 185
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04022e

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    .line 188
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f130569

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 189
    iget v1, p0, Lcom/twitter/android/commerce/card/a;->a:I

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 190
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 192
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f13056a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->q:Landroid/view/View;

    .line 193
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->e:Lcom/twitter/library/util/s;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 195
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f130214

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    .line 196
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v1, 0x40200000    # 2.5f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 198
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "card"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setImageType(Ljava/lang/String;)V

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f130217

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->g:Lcom/twitter/ui/widget/TwitterButton;

    .line 202
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f130218

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->h:Landroid/widget/TextView;

    .line 203
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f130219

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->m:Landroid/widget/TextView;

    .line 204
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f130215

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->n:Landroid/widget/TextView;

    .line 205
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->i:Landroid/view/View;

    const v1, 0x7f130216

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/a;->o:Landroid/view/View;

    .line 207
    iput-object p2, p0, Lcom/twitter/android/commerce/card/a;->c:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 208
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 9

    .prologue
    .line 211
    .line 212
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/a;->f()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    .line 211
    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v2

    .line 213
    const-string/jumbo v0, "webview_url"

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v3

    .line 214
    const-string/jumbo v0, "webview_title"

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->f:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v4

    .line 215
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->f()Lcom/twitter/library/card/CardContext;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/card/m;->l()Landroid/app/Activity;

    move-result-object v5

    .line 217
    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->i()Lcgi;

    move-result-object v1

    .line 219
    if-eqz v1, :cond_1

    iget-object v8, v1, Lcgi;->c:Ljava/lang/String;

    .line 220
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->d:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    .line 221
    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->c()J

    move-result-wide v6

    .line 220
    invoke-virtual/range {v1 .. v8}, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;JLjava/lang/String;)V

    .line 223
    :cond_0
    return-void

    .line 219
    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method a(Lcar;)V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_0

    .line 121
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 122
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 123
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "image"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->e:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 128
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/ae;->a(Lcom/twitter/library/card/z$a;)V

    .line 73
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/commerce/card/a;->j:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 74
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 92
    :cond_0
    return-void
.end method

.method b(Lcar;)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 132
    const-string/jumbo v0, "title"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_0

    .line 134
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->h:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->e:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 139
    :cond_0
    return-void
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const-string/jumbo v0, "image"

    return-object v0
.end method

.method c(Lcar;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 143
    const-string/jumbo v0, "description"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_0

    .line 145
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->m:Landroid/widget/TextView;

    const-string/jumbo v1, "description"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 147
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->e:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 150
    :cond_0
    return-void
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string/jumbo v0, "webview_url"

    return-object v0
.end method

.method d(Lcar;)V
    .locals 3

    .prologue
    .line 153
    const-string/jumbo v0, "cta"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->g:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->g:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->g:Lcom/twitter/ui/widget/TwitterButton;

    const-string/jumbo v1, "button"

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setTag(Ljava/lang/Object;)V

    .line 157
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->g:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lcom/twitter/android/commerce/card/a$2;

    iget-object v2, p0, Lcom/twitter/android/commerce/card/a;->g:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/commerce/card/a$2;-><init>(Lcom/twitter/android/commerce/card/a;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 164
    :cond_0
    return-void
.end method

.method e()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->o:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->o:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->e:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    :cond_0
    return-void
.end method

.method e(Lcar;)V
    .locals 2

    .prologue
    .line 167
    const-string/jumbo v0, "badge"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->n:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->n:Landroid/widget/TextView;

    const-string/jumbo v1, "badge"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 171
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/a;->e:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->n:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/twitter/android/commerce/card/a;->n:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
