.class public Lcom/twitter/android/commerce/card/i;
.super Lcom/twitter/android/revenue/card/a;
.source "Twttr"


# instance fields
.field protected a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

.field protected b:Landroid/widget/TextView;

.field protected c:Landroid/widget/TextView;

.field protected d:Landroid/widget/TextView;

.field protected e:Lcom/twitter/ui/widget/TwitterButton;

.field protected f:Landroid/view/View;

.field protected final g:Lcom/twitter/library/util/s;

.field protected h:Landroid/view/View;

.field private final r:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

.field private s:Lcom/twitter/media/ui/image/MediaImageView;

.field private t:Lcar;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/f;Lcom/twitter/android/card/c;)V
    .locals 4

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/revenue/card/a;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 61
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04022e

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    .line 63
    new-instance v0, Lcom/twitter/android/commerce/card/i$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/commerce/card/i$1;-><init>(Lcom/twitter/android/commerce/card/i;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->g:Lcom/twitter/library/util/s;

    .line 69
    new-instance v0, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    .line 70
    invoke-static {p2}, Lcom/twitter/android/card/g;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, p4, p3, v1}, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;-><init>(Landroid/content/Context;Lcom/twitter/android/card/b;Lcom/twitter/android/card/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->r:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    .line 71
    return-void
.end method

.method private d(Lcar;)V
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 156
    const-string/jumbo v0, "original_image"

    invoke-static {v0, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    const v2, 0x3ff47ae1    # 1.91f

    invoke-virtual {v0, v2}, Lcas;->a(F)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 158
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 159
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 161
    :cond_0
    return-void
.end method

.method private e(Lcar;)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->e:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->e:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 212
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Lcom/twitter/android/revenue/card/a;->a()V

    .line 77
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/widget/media/MultiImageContainer;->b()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 84
    :cond_1
    return-void
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130569

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 228
    if-eqz v0, :cond_0

    .line 229
    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 230
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 232
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f13056a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->h:Landroid/view/View;

    .line 233
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->g:Lcom/twitter/library/util/s;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 235
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130226

    .line 236
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    .line 238
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130218

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->b:Landroid/widget/TextView;

    .line 239
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130228

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    .line 240
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130229

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    .line 241
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130227

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->e:Lcom/twitter/ui/widget/TwitterButton;

    .line 242
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130216

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->f:Landroid/view/View;

    .line 244
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->q:Landroid/view/View;

    const v1, 0x7f130214

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    .line 245
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    const v1, 0x3ff47ae1    # 1.91f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 249
    :cond_0
    return-void
.end method

.method public a(JLcar;)V
    .locals 6

    .prologue
    const v0, 0x7f040250

    .line 99
    iput-object p3, p0, Lcom/twitter/android/commerce/card/i;->t:Lcar;

    .line 100
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->d_:Lcom/twitter/android/card/d;

    const-string/jumbo v2, "_card_data"

    invoke-static {v2, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 103
    sget-object v1, Lcom/twitter/android/revenue/card/q;->a:[Ljava/lang/String;

    invoke-static {p3, v1}, Lcom/twitter/android/commerce/util/b;->a(Lcar;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 106
    const/4 v1, 0x0

    .line 108
    iget-object v3, p0, Lcom/twitter/android/commerce/card/i;->i:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/commerce/card/i;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {v3, v4}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v3

    .line 109
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    .line 110
    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 123
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/commerce/card/i;->a(I)V

    .line 125
    invoke-virtual {p0, v2, v3, v1}, Lcom/twitter/android/commerce/card/i;->a(Ljava/util/List;ZZ)V

    .line 126
    invoke-virtual {p0, p3}, Lcom/twitter/android/commerce/card/i;->a(Lcar;)V

    .line 127
    invoke-virtual {p0, p3}, Lcom/twitter/android/commerce/card/i;->c(Lcar;)V

    .line 128
    invoke-virtual {p0, p3}, Lcom/twitter/android/commerce/card/i;->b(Lcar;)V

    .line 129
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/i;->e(Lcar;)V

    .line 130
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/i;->f()V

    .line 131
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/i;->d(Lcar;)V

    .line 132
    return-void

    .line 110
    :cond_1
    const v0, 0x7f040074

    goto :goto_0

    .line 113
    :cond_2
    const-string/jumbo v1, "original_image"

    invoke-static {v1, p3}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v1

    .line 114
    invoke-static {v1}, Lcom/twitter/android/revenue/j;->a(Lcas;)Z

    move-result v1

    .line 115
    if-nez v3, :cond_0

    .line 118
    if-eqz v1, :cond_3

    const v0, 0x7f040251

    goto :goto_0

    :cond_3
    const v0, 0x7f040075

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/i;->e()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->r:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    iget-object v2, p0, Lcom/twitter/android/commerce/card/i;->t:Lcar;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;Lcar;)V

    .line 224
    return-void
.end method

.method a(Lcar;)V
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 165
    const-string/jumbo v0, "product_display_text"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_0

    .line 167
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    sget v3, Lcni;->a:F

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 168
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->b:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->g:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 173
    :cond_0
    return-void
.end method

.method a(Ljava/util/List;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    if-eqz v0, :cond_0

    .line 136
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 138
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 139
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    const v0, 0x3ff47ae1    # 1.91f

    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v3, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    if-eqz p2, :cond_2

    const v0, 0x7f04034a

    :goto_1
    invoke-virtual {v3, p1, v2, v0}, Lcom/twitter/android/revenue/widget/media/MultiImageContainer;->a(Ljava/util/List;Ljava/util/List;I)V

    .line 149
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    sget-object v1, Lcom/twitter/android/revenue/card/q;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/widget/media/MultiImageContainer;->setTag(Ljava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->g:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/widget/media/MultiImageContainer;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 152
    :cond_0
    return-void

    .line 139
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_2
    move v0, v1

    .line 142
    goto :goto_1

    .line 145
    :cond_3
    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    const v1, 0x7f04034b

    .line 146
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, p1, v2, v1}, Lcom/twitter/android/revenue/widget/media/MultiImageContainer;->a(Ljava/util/List;Ljava/util/List;I)V

    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->a:Lcom/twitter/android/revenue/widget/media/MultiImageContainer;

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/widget/media/MultiImageContainer;->setFromMemoryOnly(Z)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->s:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 95
    :cond_1
    return-void
.end method

.method b(Lcar;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 176
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 177
    const-string/jumbo v0, "price"

    .line 178
    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "currency"

    .line 179
    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 177
    invoke-static {v0, v1}, Lcom/twitter/android/commerce/util/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_1

    .line 181
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    sget v2, Lcni;->a:F

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 183
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    const-string/jumbo v1, "price"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 185
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->g:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method c(Lcar;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 193
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 194
    const-string/jumbo v0, "product_domain"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    const-string/jumbo v1, "www."

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 198
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    sget v2, Lcni;->a:F

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 199
    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    const-string/jumbo v1, "product_domain"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->g:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method f()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/twitter/android/commerce/card/i;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/i;->g:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 218
    :cond_0
    return-void
.end method
