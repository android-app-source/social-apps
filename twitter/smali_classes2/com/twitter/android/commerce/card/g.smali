.class public Lcom/twitter/android/commerce/card/g;
.super Lcom/twitter/android/revenue/card/ae;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/q$a;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field protected c:Lcom/twitter/media/ui/image/MediaImageView;

.field protected d:Landroid/widget/TextView;

.field protected e:Landroid/widget/TextView;

.field protected f:Landroid/view/View;

.field private final g:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

.field private final h:Lcom/twitter/library/util/s;

.field private m:Lcar;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/card/m;Lcom/twitter/android/commerce/card/CommerceCardActionHandler;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/twitter/android/revenue/card/ae;-><init>(Lcom/twitter/android/revenue/card/m;)V

    .line 49
    new-instance v0, Lcom/twitter/android/commerce/card/g$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/commerce/card/g$1;-><init>(Lcom/twitter/android/commerce/card/g;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/card/g;->h:Lcom/twitter/library/util/s;

    .line 55
    iput-object p2, p0, Lcom/twitter/android/commerce/card/g;->g:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    .line 56
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 7

    .prologue
    .line 157
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->l()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->f()Lcom/twitter/library/card/CardContext;

    move-result-object v0

    if-nez v0, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/commerce/card/g;->f()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v1

    .line 164
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 186
    const/4 v4, 0x0

    .line 191
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->g:Lcom/twitter/android/commerce/card/CommerceCardActionHandler;

    iget-object v2, p0, Lcom/twitter/android/commerce/card/g;->m:Lcar;

    iget-object v3, p0, Lcom/twitter/android/commerce/card/g;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v3}, Lcom/twitter/android/revenue/card/m;->l()Landroid/app/Activity;

    move-result-object v3

    sget-object v5, Lcom/twitter/android/commerce/card/CommerceCardActionHandler$CommerceCardType;->b:Lcom/twitter/android/commerce/card/CommerceCardActionHandler$CommerceCardType;

    iget-object v6, p0, Lcom/twitter/android/commerce/card/g;->l:Lcom/twitter/android/revenue/card/m;

    .line 192
    invoke-virtual {v6}, Lcom/twitter/android/revenue/card/m;->f()Lcom/twitter/library/card/CardContext;

    move-result-object v6

    .line 191
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/commerce/card/CommerceCardActionHandler;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;Lcar;Landroid/app/Activity;Ljava/lang/String;Lcom/twitter/android/commerce/card/CommerceCardActionHandler$CommerceCardType;Lcom/twitter/library/card/CardContext;)V

    goto :goto_0

    .line 166
    :pswitch_0
    const-string/jumbo v4, "cloffer_card_cta_click"

    goto :goto_1

    .line 170
    :pswitch_1
    const-string/jumbo v4, "cloffer_card_primary_image_click"

    goto :goto_1

    .line 174
    :pswitch_2
    const-string/jumbo v4, "cloffer_card_item_title_click"

    goto :goto_1

    .line 178
    :pswitch_3
    const-string/jumbo v4, "cloffer_card_item_title_click"

    goto :goto_1

    .line 182
    :pswitch_4
    const-string/jumbo v4, "cloffer_card_container_click"

    goto :goto_1

    .line 164
    :pswitch_data_0
    .packed-switch 0x7f130221
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcar;)V
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->d:Landroid/widget/TextView;

    sget-object v1, Lcom/twitter/android/revenue/card/h;->b:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 106
    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->d:Landroid/widget/TextView;

    const-string/jumbo v0, "offer_title"

    const-class v2, Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 109
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/commerce/card/g;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/commerce/card/g;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method private b(Lcar;)V
    .locals 5

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->e:Landroid/widget/TextView;

    sget-object v1, Lcom/twitter/android/revenue/card/h;->b:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 115
    const-string/jumbo v0, "offer_redeem_type"

    const-class v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    const-string/jumbo v1, "offer_merchant_name"

    const-class v2, Ljava/lang/String;

    .line 118
    invoke-virtual {p1, v1, v2}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " "

    .line 122
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/commerce/card/g;->a:Landroid/content/Context;

    const v4, 0x7f0a0115

    .line 123
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " "

    .line 124
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 129
    :cond_0
    return-void
.end method

.method private c(Lcar;)V
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 133
    const-string/jumbo v0, "offer_image"

    invoke-static {v0, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v2, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v1, v2, :cond_1

    .line 135
    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    const v2, 0x40533333    # 3.3f

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 140
    :goto_0
    if-eqz v0, :cond_0

    .line 141
    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 142
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 145
    :cond_0
    return-void

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x40200000    # 2.5f

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    goto :goto_0
.end method

.method private d(Lcar;)V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->f:Landroid/view/View;

    const-string/jumbo v1, "button"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 150
    return-void
.end method

.method private e(Lcar;)V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/commerce/card/g;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 154
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 83
    invoke-super {p0}, Lcom/twitter/android/revenue/card/ae;->a()V

    .line 84
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/commerce/card/g;->j:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 88
    :cond_0
    return-void
.end method

.method public a(JLcar;)V
    .locals 3

    .prologue
    .line 92
    iput-object p3, p0, Lcom/twitter/android/commerce/card/g;->m:Lcar;

    .line 93
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->l:Lcom/twitter/android/revenue/card/m;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/m;->h()Lcom/twitter/android/card/d;

    move-result-object v1

    const-string/jumbo v0, "_card_data"

    const-class v2, Ljava/lang/String;

    invoke-virtual {p3, v0, v2}, Lcar;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 96
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/g;->a(Lcar;)V

    .line 97
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/g;->b(Lcar;)V

    .line 98
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/g;->c(Lcar;)V

    .line 99
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/g;->d(Lcar;)V

    .line 100
    invoke-direct {p0, p3}, Lcom/twitter/android/commerce/card/g;->e(Lcar;)V

    .line 101
    return-void
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 3

    .prologue
    .line 60
    iput-object p1, p0, Lcom/twitter/android/commerce/card/g;->a:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/twitter/android/commerce/card/g;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 63
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, p2, :cond_0

    const v0, 0x7f040072

    .line 67
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/g;->i:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->i:Landroid/view/View;

    const v1, 0x7f130224

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/g;->d:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->i:Landroid/view/View;

    const v1, 0x7f130225

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/g;->e:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->i:Landroid/view/View;

    const v1, 0x7f130222

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/commerce/card/g;->c:Lcom/twitter/media/ui/image/MediaImageView;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/commerce/card/g;->i:Landroid/view/View;

    const v1, 0x7f130223

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/commerce/card/g;->f:Landroid/view/View;

    .line 73
    return-void

    .line 63
    :cond_0
    const v0, 0x7f040073

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/ae;->a(Lcom/twitter/library/card/z$a;)V

    .line 78
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/commerce/card/g;->j:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 79
    return-void
.end method
