.class public Lcom/twitter/android/commerce/view/OrderHistoryFragment$b;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/commerce/view/OrderHistoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/commerce/view/OrderHistoryFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/commerce/view/OrderHistoryFragment;)V
    .locals 1

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 247
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/view/OrderHistoryFragment$b;->a:Ljava/lang/ref/WeakReference;

    .line 248
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 242
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/commerce/view/OrderHistoryFragment$b;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/android/commerce/view/OrderHistoryFragment$b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/commerce/view/OrderHistoryFragment;

    .line 254
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/commerce/view/OrderHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 255
    invoke-virtual {v0}, Lcom/twitter/android/commerce/view/OrderHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    instance-of v1, p1, Lbmb;

    if-eqz v1, :cond_0

    .line 257
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    .line 258
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 259
    iget-object v1, v1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "order_history_list"

    sget-object v3, Lcom/twitter/library/commerce/model/i;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/commerce/model/i;

    .line 262
    invoke-virtual {v0, v1}, Lcom/twitter/android/commerce/view/OrderHistoryFragment;->a(Lcom/twitter/library/commerce/model/i;)V

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v1, v1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "commerce_error_list_bundle"

    .line 265
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 266
    invoke-virtual {v0, v1}, Lcom/twitter/android/commerce/view/OrderHistoryFragment;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method
