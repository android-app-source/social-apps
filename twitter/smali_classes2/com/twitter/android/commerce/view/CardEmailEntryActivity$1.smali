.class Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;
.super Lcom/twitter/android/commerce/util/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/commerce/view/CardEmailEntryActivity;->j()Lcom/twitter/android/commerce/util/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

.field final synthetic b:Lcom/twitter/android/commerce/view/CardEmailEntryActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/commerce/view/CardEmailEntryActivity;Landroid/content/Context;Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;)V
    .locals 3

    .prologue
    const v2, 0x7f0a0164

    .line 229
    iput-object p1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->b:Lcom/twitter/android/commerce/view/CardEmailEntryActivity;

    iput-object p3, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    invoke-direct {p0, p2}, Lcom/twitter/android/commerce/util/c;-><init>(Landroid/content/Context;)V

    .line 230
    const v0, 0x7f0a0166

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->b:Lcom/twitter/android/commerce/widget/creditcard/CardNumber;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 231
    const v0, 0x7f0a0167

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->b:Lcom/twitter/android/commerce/widget/creditcard/CardNumber;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 232
    const v0, 0x7f0a016a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->b:Lcom/twitter/android/commerce/widget/creditcard/CardNumber;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 233
    const v0, 0x7f0a0169

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->b:Lcom/twitter/android/commerce/widget/creditcard/CardNumber;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 234
    const v0, 0x7f0a016c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->c:Lcom/twitter/android/commerce/widget/creditcard/CardExpiration;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 235
    const v0, 0x7f0a016b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->c:Lcom/twitter/android/commerce/widget/creditcard/CardExpiration;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 236
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->c:Lcom/twitter/android/commerce/widget/creditcard/CardExpiration;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 237
    const v0, 0x7f0a0160

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->c:Lcom/twitter/android/commerce/widget/creditcard/CardExpiration;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 238
    const v0, 0x7f0a015f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->d:Lcom/twitter/android/commerce/widget/creditcard/CardCCV;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 239
    const v0, 0x7f0a015e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->d:Lcom/twitter/android/commerce/widget/creditcard/CardCCV;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 240
    const v0, 0x7f0a0165

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->c:Lcom/twitter/android/commerce/widget/creditcard/CardExpiration;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 241
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->c:Lcom/twitter/android/commerce/widget/creditcard/CardExpiration;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 242
    const v0, 0x7f0a016d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a:Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;

    iget-object v1, v1, Lcom/twitter/android/commerce/widget/creditcard/CardEntryContainer;->e:Lcom/twitter/android/commerce/widget/creditcard/InternationalCardZip;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 243
    const v0, 0x7f0a0150

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->b:Lcom/twitter/android/commerce/view/CardEmailEntryActivity;

    invoke-static {v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity;->a(Lcom/twitter/android/commerce/view/CardEmailEntryActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 244
    const v0, 0x7f0a016f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->b:Lcom/twitter/android/commerce/view/CardEmailEntryActivity;

    invoke-static {v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity;->a(Lcom/twitter/android/commerce/view/CardEmailEntryActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/commerce/view/CardEmailEntryActivity$1;->a(Ljava/lang/Integer;Landroid/widget/TextView;)Lcom/twitter/android/commerce/util/c;

    .line 245
    return-void
.end method
