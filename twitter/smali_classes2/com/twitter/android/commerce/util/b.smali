.class public Lcom/twitter/android/commerce/util/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/CreditCard;
    .locals 1

    .prologue
    .line 224
    if-eqz p0, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/m;->a()Lcom/twitter/library/commerce/model/CreditCard;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/commerce/model/CreditCard;Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/a;
    .locals 6

    .prologue
    .line 258
    if-eqz p1, :cond_1

    .line 259
    invoke-virtual {p1}, Lcom/twitter/library/commerce/model/m;->h()[Lcom/twitter/library/commerce/model/a;

    move-result-object v2

    .line 260
    if-eqz v2, :cond_1

    .line 261
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 262
    invoke-virtual {v0}, Lcom/twitter/library/commerce/model/a;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/CreditCard;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 268
    :goto_1
    return-object v0

    .line 261
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/commerce/model/CreditCard$Type;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 273
    const-string/jumbo v0, "/"

    .line 274
    if-eqz p1, :cond_2

    .line 275
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 276
    const/4 v0, 0x0

    .line 277
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/CreditCard$Type;

    .line 278
    if-lez v1, :cond_0

    .line 279
    const-string/jumbo v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_0
    invoke-static {p0, v0}, Lcom/twitter/library/commerce/model/CreditCard$Type;->a(Landroid/content/Context;Lcom/twitter/library/commerce/model/CreditCard$Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 283
    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 286
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    const-string/jumbo v0, "USD"

    invoke-static {p0, v0}, Lcom/twitter/android/commerce/util/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 454
    const/4 v0, 0x0

    .line 455
    instance-of v1, p0, Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 456
    check-cast p0, Ljava/lang/String;

    .line 458
    :try_start_0
    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 459
    invoke-static {v1, p1}, Lcom/twitter/android/commerce/util/b;->a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 463
    :cond_0
    :goto_0
    return-object v0

    .line 460
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Ljava/math/BigDecimal;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 483
    const-string/jumbo v0, "USD"

    invoke-static {p0, v0}, Lcom/twitter/android/commerce/util/b;->a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 490
    new-instance v0, Ljava/math/BigDecimal;

    const-wide/32 v2, 0xf4240

    invoke-direct {v0, v2, v3}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {p0, v0}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 491
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 493
    :try_start_0
    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 494
    :catch_0
    move-exception v0

    .line 495
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 155
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 157
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "MMM dd, yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 159
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/HashMap;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 399
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    :goto_0
    return-object v0

    .line 400
    :catch_0
    move-exception v0

    .line 402
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(ZLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 290
    if-eqz p0, :cond_0

    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "settings:payment_settings:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 293
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "buy_now::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcar;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcar;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 384
    invoke-virtual {p0}, Lcar;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 385
    invoke-virtual {p0, v0}, Lcar;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 386
    instance-of v4, v1, Lcas;

    if-eqz v4, :cond_0

    .line 387
    check-cast v1, Lcas;

    .line 388
    iget-object v1, v1, Lcas;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 390
    :cond_0
    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 393
    :cond_1
    return-object v2
.end method

.method public static a(Lcar;[Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcar;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 468
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 470
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 471
    invoke-static {v3, p0}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v3

    .line 472
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcas;->a:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 473
    iget-object v3, v3, Lcas;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 476
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 373
    if-eqz p0, :cond_0

    .line 374
    const-string/jumbo v0, " "

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 375
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 379
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Ljava/lang/String;Lcom/twitter/library/commerce/model/a;Lcom/twitter/library/commerce/model/g;Ljava/lang/String;Lcom/twitter/library/commerce/model/CreditCard;Ljava/util/List;IZZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/library/card/CardContext;",
            "Ljava/lang/String;",
            "Lcom/twitter/library/commerce/model/a;",
            "Lcom/twitter/library/commerce/model/g;",
            "Ljava/lang/String;",
            "Lcom/twitter/library/commerce/model/CreditCard;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZZZ)V"
        }
    .end annotation

    .prologue
    .line 326
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 327
    const-string/jumbo v0, "store_profile:payment_method:start"

    .line 328
    invoke-static {p9, v0}, Lcom/twitter/android/commerce/util/b;->a(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    invoke-static {p0, p1, v0, p2}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;Lcom/twitter/library/card/CardContext;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->V:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {p0, p1, v0}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/api/PromotedEvent;)V

    .line 332
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/commerce/view/ProfileEntryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 334
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 335
    const-string/jumbo v2, "commerce_address_object"

    sget-object v3, Lcom/twitter/library/commerce/model/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v2, p3, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 337
    const-string/jumbo v2, "commerce_profile_email"

    sget-object v3, Lcom/twitter/library/commerce/model/g;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v2, p4, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 338
    const-string/jumbo v2, "commerce_buynow_card_context"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 339
    const-string/jumbo v2, "commerce_launched_from_settings"

    invoke-virtual {v1, v2, p9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 340
    const-string/jumbo v2, "commerce_phone_required"

    invoke-virtual {v1, v2, p10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 341
    const-string/jumbo v2, "commerce_billing_required"

    invoke-virtual {v1, v2, p11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 342
    if-eqz p7, :cond_1

    instance-of v2, p7, Ljava/io/Serializable;

    if-eqz v2, :cond_1

    .line 343
    const-string/jumbo v2, "commerce_allowed_states_for_item"

    check-cast p7, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 347
    :cond_1
    if-eqz p6, :cond_2

    invoke-static {}, Lcom/twitter/android/commerce/util/b;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 348
    const-string/jumbo v2, "commerce_partial_card_object"

    sget-object v3, Lcom/twitter/library/commerce/model/CreditCard;->d:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v2, p6, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 351
    :cond_2
    invoke-static {v1, p5}, Lcom/twitter/android/commerce/network/e;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 354
    invoke-virtual {p0, v0, p8}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 355
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 76
    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 77
    if-eqz p1, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 80
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/Integer;IZ)V
    .locals 6

    .prologue
    .line 110
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/Integer;IZZ)V

    .line 111
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/Integer;IZZ)V
    .locals 6

    .prologue
    .line 120
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/Integer;Ljava/lang/String;ZZ)V

    .line 122
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/Integer;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 115
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/Integer;Ljava/lang/String;ZZ)V

    .line 116
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/Integer;Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 126
    const v0, 0x7f130211

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 128
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    const v0, 0x7f130210

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 131
    if-eqz p2, :cond_2

    .line 132
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 136
    :goto_0
    if-nez p4, :cond_0

    .line 137
    const v0, 0x7f13020e

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 138
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 141
    :cond_0
    if-eqz p5, :cond_1

    .line 142
    const v0, 0x7f13020f

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 144
    :cond_1
    return-void

    .line 134
    :cond_2
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    .prologue
    .line 167
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->af:Lcom/twitter/library/api/PromotedEvent;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->ab:Lcom/twitter/library/api/PromotedEvent;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->aj:Lcom/twitter/library/api/PromotedEvent;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->al:Lcom/twitter/library/api/PromotedEvent;

    if-eq p2, v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/card/CardContext;->i()Lcgi;

    move-result-object v0

    .line 175
    :goto_1
    if-eqz v0, :cond_0

    .line 176
    invoke-static {p2, v0}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 174
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/card/CardContext;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;Lcom/twitter/library/card/CardContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/card/CardContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 94
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    move-object v2, p0

    move-object v3, p1

    move-object v5, v4

    .line 98
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/card/g;->a(JLandroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 99
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 100
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 101
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    invoke-virtual {v0, p3, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 104
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 106
    :cond_1
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 181
    const-string/jumbo v0, "commerce_enabled"

    invoke-static {v0}, Lcom/twitter/android/commerce/util/b;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Z)Z
    .locals 1

    .prologue
    .line 358
    const-string/jumbo v0, "debug_always_require_phone"

    invoke-static {p0, p1, v0}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;ZLjava/lang/String;)Z
    .locals 2

    .prologue
    .line 366
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 367
    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 368
    if-eqz v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    return p1
.end method

.method public static a(Ljava/util/HashMap;Ljava/lang/String;Z)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 407
    .line 408
    invoke-static {p0, p1}, Lcom/twitter/android/commerce/util/b;->a(Ljava/util/HashMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 409
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 410
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "false"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 411
    const/4 p2, 0x0

    .line 422
    :cond_0
    :goto_0
    return p2

    .line 412
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    const/4 p2, 0x1

    goto :goto_0

    .line 416
    :cond_2
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 417
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 418
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0
.end method

.method public static b(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/CreditCard;
    .locals 5

    .prologue
    .line 231
    if-eqz p0, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/m;->e()[Lcom/twitter/library/commerce/model/CreditCard;

    move-result-object v2

    .line 233
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 234
    invoke-virtual {v0}, Lcom/twitter/library/commerce/model/CreditCard;->o()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 239
    :goto_1
    return-object v0

    .line 233
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 239
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcar;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 501
    const-string/jumbo v0, "product_url"

    invoke-static {v0, p0}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v2

    .line 502
    const-string/jumbo v0, "mobile_url_params"

    invoke-static {v0, p0}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 503
    const/4 v0, 0x0

    .line 504
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 506
    :try_start_0
    const-string/jumbo v3, "UTF-8"

    invoke-static {v1, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 510
    :cond_0
    :goto_0
    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 511
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    .line 512
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x3f

    .line 513
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 515
    :goto_2
    return-object v0

    .line 512
    :cond_1
    const/16 v1, 0x26

    goto :goto_1

    .line 507
    :catch_0
    move-exception v1

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 185
    const-string/jumbo v0, "commerce_allow_unverified_email_address"

    invoke-static {v0}, Lcom/twitter/android/commerce/util/b;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;Z)Z
    .locals 1

    .prologue
    .line 362
    const-string/jumbo v0, "debug_always_require_billing"

    invoke-static {p0, p1, v0}, Lcom/twitter/android/commerce/util/b;->a(Landroid/content/Context;ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 193
    invoke-static {p0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v3

    .line 194
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    .line 195
    invoke-virtual {v2}, Lcof;->b()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lcof;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v1

    .line 196
    :goto_0
    if-nez v2, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    :cond_3
    move v2, v0

    .line 195
    goto :goto_0
.end method

.method public static c(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/g;
    .locals 1

    .prologue
    .line 243
    if-eqz p0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/m;->c()Lcom/twitter/library/commerce/model/g;

    move-result-object v0

    .line 246
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 189
    const-string/jumbo v0, "commerce_order_history_enabled"

    invoke-static {v0}, Lcom/twitter/android/commerce/util/b;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/l;
    .locals 1

    .prologue
    .line 250
    if-eqz p0, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/m;->d()Lcom/twitter/library/commerce/model/l;

    move-result-object v0

    .line 253
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 200
    const-string/jumbo v0, "commerce_offers_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static e()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 204
    const-string/jumbo v2, "commerce_upgrade_profile"

    .line 205
    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v3

    .line 206
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    .line 207
    invoke-virtual {v2}, Lcof;->b()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lcof;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v1

    .line 209
    :goto_0
    if-nez v2, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    :cond_3
    move v2, v0

    .line 207
    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 213
    const-string/jumbo v0, "commerce_use_v2_api"

    .line 214
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 216
    return v0
.end method
