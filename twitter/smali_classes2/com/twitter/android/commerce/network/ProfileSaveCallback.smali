.class public Lcom/twitter/android/commerce/network/ProfileSaveCallback;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/commerce/network/ProfileSaveCallback$RequestType;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/commerce/network/j;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/commerce/network/ProfileSaveCallback$RequestType;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/commerce/network/j;Lcom/twitter/android/commerce/network/ProfileSaveCallback$RequestType;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/network/ProfileSaveCallback;->a:Ljava/lang/ref/WeakReference;

    .line 28
    iput-object p2, p0, Lcom/twitter/android/commerce/network/ProfileSaveCallback;->b:Lcom/twitter/android/commerce/network/ProfileSaveCallback$RequestType;

    .line 29
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/commerce/network/ProfileSaveCallback;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    iget-boolean v0, p0, Lcom/twitter/android/commerce/network/ProfileSaveCallback;->c:Z

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 35
    iget-object v1, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "commerce_error_list_bundle"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 36
    iget-object v1, p0, Lcom/twitter/android/commerce/network/ProfileSaveCallback;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/commerce/network/j;

    .line 38
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/twitter/android/commerce/network/j;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    .line 40
    iget-object v5, p0, Lcom/twitter/android/commerce/network/ProfileSaveCallback;->b:Lcom/twitter/android/commerce/network/ProfileSaveCallback$RequestType;

    sget-object v6, Lcom/twitter/android/commerce/network/ProfileSaveCallback$RequestType;->a:Lcom/twitter/android/commerce/network/ProfileSaveCallback$RequestType;

    if-ne v5, v6, :cond_3

    .line 41
    if-eqz v4, :cond_1

    .line 42
    :goto_0
    if-eqz v3, :cond_2

    .line 43
    invoke-interface {v1, v4}, Lcom/twitter/android/commerce/network/j;->e(Landroid/os/Bundle;)V

    .line 63
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v3, v2

    .line 41
    goto :goto_0

    .line 45
    :cond_2
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "address_signature_bundle"

    .line 46
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 47
    invoke-interface {v1, v0}, Lcom/twitter/android/commerce/network/j;->c(Landroid/os/Bundle;)V

    goto :goto_1

    .line 50
    :cond_3
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "store_profile_bundle"

    .line 51
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 52
    if-eqz v5, :cond_4

    if-eqz v5, :cond_5

    const-string/jumbo v0, "storeprofile_bundle_success"

    .line 54
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v3

    .line 55
    :goto_2
    if-eqz v0, :cond_6

    .line 56
    invoke-interface {v1, v4}, Lcom/twitter/android/commerce/network/j;->f(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 54
    goto :goto_2

    .line 58
    :cond_6
    invoke-interface {v1, v5}, Lcom/twitter/android/commerce/network/j;->d(Landroid/os/Bundle;)V

    goto :goto_1
.end method
