.class public Lcom/twitter/android/commerce/network/c;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/commerce/network/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/commerce/network/d;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/network/c;->a:Ljava/lang/ref/WeakReference;

    .line 28
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/commerce/network/c;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/commerce/network/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/commerce/network/d;

    .line 33
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/android/commerce/network/d;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    instance-of v1, p1, Lbln;

    if-eqz v1, :cond_0

    .line 35
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    .line 36
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    invoke-interface {v0}, Lcom/twitter/android/commerce/network/d;->a()V

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    iget-object v1, v1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "commerce_error_list_bundle"

    .line 40
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 41
    invoke-interface {v0, v1}, Lcom/twitter/android/commerce/network/d;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method
