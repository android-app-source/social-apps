.class public Lcom/twitter/android/commerce/network/h;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/commerce/network/i;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/commerce/network/i;Z)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/commerce/network/h;->a:Ljava/lang/ref/WeakReference;

    .line 31
    iput-boolean p2, p0, Lcom/twitter/android/commerce/network/h;->b:Z

    .line 32
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/commerce/network/h;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/commerce/network/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/commerce/network/i;

    .line 38
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/android/commerce/network/i;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 39
    instance-of v1, p1, Lbme;

    if-eqz v1, :cond_0

    .line 40
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    .line 41
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 42
    iget-object v1, v1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "profile_bundle"

    sget-object v3, Lcom/twitter/library/commerce/model/m;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/commerce/model/m;

    .line 44
    iget-boolean v2, p0, Lcom/twitter/android/commerce/network/h;->b:Z

    invoke-interface {v0, v2, v1}, Lcom/twitter/android/commerce/network/i;->a(ZLcom/twitter/library/commerce/model/m;)V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    iget-object v1, v1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "commerce_error_list_bundle"

    .line 47
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 48
    invoke-interface {v0, v1}, Lcom/twitter/android/commerce/network/i;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method
