.class public Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/app/Activity;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private f:Lcom/twitter/analytics/model/ScribeLog;


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->f:Lcom/twitter/analytics/model/ScribeLog;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->f:Lcom/twitter/analytics/model/ScribeLog;

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->b:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->b:Landroid/app/Activity;

    const-class v3, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    iget-object v3, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->c:Ljava/lang/String;

    .line 51
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "query_name"

    iget-object v3, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->d:Ljava/lang/String;

    .line 52
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "search_type"

    iget v3, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->e:I

    .line 53
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "terminal"

    const/4 v3, 0x0

    .line 54
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 55
    return-void
.end method

.method public setClickScribeLog(Lcom/twitter/analytics/model/ScribeLog;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->f:Lcom/twitter/analytics/model/ScribeLog;

    .line 59
    return-void
.end method

.method public setDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/commerce/widget/form/ViewMoreContainer;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    return-void
.end method
