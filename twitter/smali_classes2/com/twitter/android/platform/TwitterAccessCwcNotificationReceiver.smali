.class public Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a()J
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 190
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 191
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 192
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 193
    const/16 v1, 0xa

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 194
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 195
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 196
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 185
    const-string/jumbo v1, "key_event_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    const/4 v1, 0x0

    invoke-static {p0, p2, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 57
    const-string/jumbo v3, "twitter_access_cwc_notifications_enabled"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 58
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v3

    .line 59
    if-eqz v2, :cond_0

    invoke-virtual {v3}, Lbaa;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "twitter_access_cwc_notifications_enabled"

    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 64
    :cond_1
    invoke-virtual {v3}, Lbaa;->c()Ljava/lang/String;

    move-result-object v2

    .line 65
    if-eqz v2, :cond_0

    .line 68
    const-string/jumbo v3, "twitter_access_cwc_notifications_carriers_keys"

    invoke-static {v3}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 70
    invoke-interface {v3, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 164
    const-string/jumbo v0, "alarm"

    .line 165
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 167
    invoke-static {}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xf731400

    .line 168
    invoke-static {p0}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->f(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v6

    .line 166
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 169
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "twitter_access_cwc_notifications_schedule_started"

    .line 170
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 171
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 114
    const-string/jumbo v0, "twitter_access_cwc_notifications_carriers_keys"

    invoke-static {v0}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 116
    const-string/jumbo v1, "twitter_access_cwc_notifications_carriers_names"

    invoke-static {v1}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 118
    const-string/jumbo v2, "twitter_access_cwc_notifications_headlines"

    invoke-static {v2}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 120
    const-string/jumbo v3, "twitter_access_cwc_notifications_bodies"

    invoke-static {v3}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 122
    invoke-static {p1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v4

    .line 123
    invoke-virtual {v4}, Lbaa;->c()Ljava/lang/String;

    move-result-object v4

    .line 122
    invoke-interface {v0, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 124
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 125
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 126
    invoke-static {p1}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v4

    .line 127
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {v4, v1, v2, v0}, Lcom/twitter/android/client/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "app:twitter_access:cricket:notification:impression"

    aput-object v2, v1, v5

    .line 131
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 132
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Landroid/content/Context;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 130
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 133
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 134
    const-string/jumbo v1, "twitter_access_notifications_shown_count"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 136
    const/16 v2, 0xe

    if-ne v1, v2, :cond_1

    .line 137
    invoke-direct {p0, p1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->e(Landroid/content/Context;)V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "twitter_access_notifications_shown_count"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 140
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private d(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 147
    invoke-static {p1}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/l;->b()V

    .line 148
    invoke-direct {p0, p1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->e(Landroid/content/Context;)V

    .line 149
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app:twitter_access:cricket:notification:disabled"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 150
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Landroid/content/Context;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 151
    return-void
.end method

.method private e(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 154
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "twitter_access_cwc_notifications_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 155
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 156
    const-string/jumbo v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 157
    invoke-static {p1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->f(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 156
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 158
    return-void
.end method

.method private static f(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 203
    const-string/jumbo v0, "type_event_show"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 76
    const-string/jumbo v0, "key_event_type"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 77
    if-nez v3, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 82
    :pswitch_0
    const-string/jumbo v0, "twitter_access_cwc_notifications_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 84
    invoke-direct {p0, p1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->d(Landroid/content/Context;)V

    goto :goto_0

    .line 80
    :sswitch_0
    const-string/jumbo v4, "type_event_show"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string/jumbo v4, "type_event_disable"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string/jumbo v4, "type_event_start_cricket_activity"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    .line 85
    :cond_3
    invoke-static {p1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-direct {p0, p1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->c(Landroid/content/Context;)V

    goto :goto_0

    .line 91
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->d(Landroid/content/Context;)V

    goto :goto_0

    .line 95
    :pswitch_2
    const-string/jumbo v0, "cricket_experience_tournament_hashtag_takeover"

    invoke-static {v0}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/SearchActivity;

    invoke-direct {v3, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "query"

    .line 98
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v3, 0x14000000

    .line 99
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 100
    invoke-static {v2, v0}, Lcom/twitter/android/al;->a(ZLandroid/content/Intent;)V

    .line 101
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 102
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "app:twitter_access:cricket:notification:open"

    aput-object v3, v2, v1

    .line 103
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Landroid/content/Context;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 102
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 80
    :sswitch_data_0
    .sparse-switch
        -0x5e5eca02 -> :sswitch_1
        -0x467653fc -> :sswitch_2
        0x57338fa7 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
