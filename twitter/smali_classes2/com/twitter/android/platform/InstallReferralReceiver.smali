.class public Lcom/twitter/android/platform/InstallReferralReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/platform/InstallReferralReceiver$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v4, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 30
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v10, [Ljava/lang/String;

    const-string/jumbo v3, "external::::referred"

    aput-object v3, v2, v9

    .line 34
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v2, "4"

    .line 35
    invoke-virtual {v0, v2, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 37
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/api/c;->a(Landroid/content/Context;)Lcom/twitter/library/api/c;

    move-result-object v1

    .line 38
    sget-object v2, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v2, v1}, Lcom/twitter/library/client/b;->a(Lcom/twitter/library/api/c;)V

    .line 39
    if-eqz v1, :cond_0

    .line 40
    const-string/jumbo v2, "6"

    .line 41
    invoke-virtual {v1}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v3

    .line 40
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 42
    invoke-virtual {v1}, Lcom/twitter/library/api/c;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 47
    :cond_0
    const-string/jumbo v1, "DispatchActivity"

    invoke-virtual {p0, v1, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 49
    const-string/jumbo v2, "version_code_for_app_update"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v4, :cond_3

    .line 51
    if-eqz p1, :cond_2

    .line 52
    sget-object v1, Lcom/twitter/android/util/AppEventTrack$EventType;->f:Lcom/twitter/android/util/AppEventTrack$EventType;

    new-array v2, v10, [Ljava/lang/String;

    aput-object p1, v2, v9

    invoke-static {p0, v1, v2}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/android/util/AppEventTrack$EventType;[Ljava/lang/String;)V

    .line 71
    :cond_1
    :goto_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 72
    return-void

    .line 54
    :cond_2
    sget-object v1, Lcom/twitter/android/util/AppEventTrack$EventType;->f:Lcom/twitter/android/util/AppEventTrack$EventType;

    new-array v2, v9, [Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/android/util/AppEventTrack$EventType;[Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_3
    if-eqz p1, :cond_4

    .line 58
    invoke-static {p0, p1}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 60
    new-instance v7, Lcom/twitter/android/util/AppEventTrack$b;

    .line 61
    invoke-static {p1}, Lcom/twitter/android/util/AppEventTrack;->a(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    invoke-direct {v7, v1}, Lcom/twitter/android/util/AppEventTrack$b;-><init>(Ljava/util/HashMap;)V

    .line 63
    const/4 v1, 0x3

    iget-object v2, v7, Lcom/twitter/android/util/AppEventTrack$b;->c:Ljava/lang/String;

    iget-object v3, v7, Lcom/twitter/android/util/AppEventTrack$b;->d:Ljava/lang/String;

    iget-object v4, v7, Lcom/twitter/android/util/AppEventTrack$b;->a:Ljava/lang/String;

    iget-object v5, v7, Lcom/twitter/android/util/AppEventTrack$b;->e:Ljava/lang/String;

    iget-object v6, v7, Lcom/twitter/android/util/AppEventTrack$b;->b:Ljava/lang/String;

    iget-object v7, v7, Lcom/twitter/android/util/AppEventTrack$b;->f:Ljava/lang/String;

    move-object v8, p1

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 65
    sget-object v1, Lcom/twitter/android/util/AppEventTrack$EventType;->a:Lcom/twitter/android/util/AppEventTrack$EventType;

    new-array v2, v10, [Ljava/lang/String;

    aput-object p1, v2, v9

    invoke-static {p0, v1, v2}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/android/util/AppEventTrack$EventType;[Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_4
    const-string/jumbo v1, "app_event_track_non_referred_install_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    sget-object v1, Lcom/twitter/android/util/AppEventTrack$EventType;->h:Lcom/twitter/android/util/AppEventTrack$EventType;

    new-array v2, v9, [Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/android/util/AppEventTrack$EventType;[Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/android/platform/InstallReferralReceiver$a;

    invoke-direct {v0, p1}, Lcom/twitter/android/platform/InstallReferralReceiver$a;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "referrer"

    .line 92
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 91
    invoke-virtual {v0, v1}, Lcom/twitter/android/platform/InstallReferralReceiver$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 93
    return-void
.end method
