.class public Lcom/twitter/android/az;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/az;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lbmp$a;

.field private final d:Lcom/twitter/util/android/f;


# direct methods
.method constructor <init>(Landroid/content/Context;Lbmp$a;Lcom/twitter/util/android/f;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/az;->b:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/twitter/android/az;->c:Lbmp$a;

    .line 42
    iput-object p3, p0, Lcom/twitter/android/az;->d:Lcom/twitter/util/android/f;

    .line 43
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/az;
    .locals 3

    .prologue
    .line 46
    sget-object v0, Lcom/twitter/android/az;->a:Lcom/twitter/android/az;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/twitter/android/az;

    new-instance v1, Lbmp$a;

    invoke-direct {v1}, Lbmp$a;-><init>()V

    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/az;-><init>(Landroid/content/Context;Lbmp$a;Lcom/twitter/util/android/f;)V

    sput-object v0, Lcom/twitter/android/az;->a:Lcom/twitter/android/az;

    .line 48
    const-class v0, Lcom/twitter/android/az;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 50
    :cond_0
    sget-object v0, Lcom/twitter/android/az;->a:Lcom/twitter/android/az;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 68
    new-instance v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 69
    const-string/jumbo v0, "addressBookPermissionStatus"

    iput-object v0, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 70
    new-array v5, v10, [Z

    iget-object v0, p0, Lcom/twitter/android/az;->b:Landroid/content/Context;

    .line 71
    invoke-static {v0}, Lcom/twitter/library/util/h;->a(Landroid/content/Context;)Lcom/twitter/library/util/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/util/g;->b()Z

    move-result v0

    aput-boolean v0, v5, v2

    iget-object v0, p0, Lcom/twitter/android/az;->c:Lbmp$a;

    iget-object v6, p0, Lcom/twitter/android/az;->b:Landroid/content/Context;

    .line 72
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-virtual {v0, v6, v8, v9}, Lbmp$a;->a(Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    aput-boolean v0, v5, v1

    iget-object v0, p0, Lcom/twitter/android/az;->c:Lbmp$a;

    iget-object v6, p0, Lcom/twitter/android/az;->b:Landroid/content/Context;

    .line 73
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-virtual {v0, v6, v8, v9}, Lbmp$a;->b(Landroid/content/Context;J)Z

    move-result v0

    aput-boolean v0, v5, v7

    .line 70
    invoke-static {v5}, Lcom/twitter/library/util/af;->a([Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 74
    invoke-virtual {p1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 76
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 77
    const-string/jumbo v4, "geoPermissionStatus"

    iput-object v4, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 78
    const/4 v4, 0x4

    new-array v4, v4, [Z

    .line 79
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v5

    invoke-virtual {v5}, Lbqg;->e()Z

    move-result v5

    aput-boolean v5, v4, v2

    .line 80
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v5

    invoke-virtual {v5, v3}, Lbqg;->c(Lcom/twitter/library/client/Session;)Z

    move-result v3

    aput-boolean v3, v4, v1

    .line 81
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v3

    invoke-virtual {v3}, Lbqg;->f()Z

    move-result v3

    aput-boolean v3, v4, v7

    .line 82
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v3

    invoke-virtual {v3}, Lbqg;->g()Z

    move-result v3

    aput-boolean v3, v4, v10

    .line 78
    invoke-static {v4}, Lcom/twitter/library/util/af;->a([Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 84
    invoke-virtual {p1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 86
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 87
    const-string/jumbo v3, "notificationPermissionSettings"

    iput-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 88
    new-array v3, v1, [Z

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/az;->a()Z

    move-result v4

    aput-boolean v4, v3, v2

    .line 88
    invoke-static {v3}, Lcom/twitter/library/util/af;->a([Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 90
    invoke-virtual {p1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 92
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 93
    const-string/jumbo v3, "androidMPermissionsActive"

    iput-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 94
    new-array v1, v1, [Z

    iget-object v3, p0, Lcom/twitter/android/az;->d:Lcom/twitter/util/android/f;

    .line 95
    invoke-virtual {v3}, Lcom/twitter/util/android/f;->b()Z

    move-result v3

    aput-boolean v3, v1, v2

    .line 94
    invoke-static {v1}, Lcom/twitter/library/util/af;->a([Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 96
    invoke-virtual {p1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 97
    return-void

    :cond_0
    move v0, v2

    .line 72
    goto/16 :goto_0
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/az;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
