.class public Lcom/twitter/android/bh;
.super Lcom/twitter/android/widget/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/widget/p",
        "<",
        "Lcom/twitter/android/bj;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/android/ca;

.field private final d:I

.field private final e:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private final f:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private final g:I

.field private final h:F

.field private i:Lcom/twitter/model/core/TwitterUser;

.field private j:Z

.field private k:Z

.field private l:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/bj;IIZ)V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0, p2, p4}, Lcom/twitter/android/widget/p;-><init>(Landroid/widget/ListAdapter;I)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/bh;->j:Z

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/bh;->l:Landroid/view/View$OnClickListener;

    .line 45
    invoke-virtual {p2, p5}, Lcom/twitter/android/bj;->e(Z)V

    .line 46
    invoke-virtual {p2, p5}, Lcom/twitter/android/bj;->b(Z)V

    .line 47
    invoke-virtual {p2, p5}, Lcom/twitter/android/bj;->c(Z)V

    .line 49
    iput-object p1, p0, Lcom/twitter/android/bh;->b:Landroid/content/Context;

    .line 50
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    .line 51
    invoke-virtual {v0, p3}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p1}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/twitter/android/ca;

    const v2, 0x7f0a06f2

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/ca;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    iput-object v1, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/ca;

    .line 57
    iput p3, p0, Lcom/twitter/android/bh;->d:I

    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f04035f

    :goto_0
    iput v0, p0, Lcom/twitter/android/bh;->e:I

    .line 67
    :goto_1
    const v0, 0x7f040064

    iput v0, p0, Lcom/twitter/android/bh;->g:I

    .line 68
    iget-object v0, p0, Lcom/twitter/android/bh;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/bh;->h:F

    .line 69
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f040377

    :goto_2
    iput v0, p0, Lcom/twitter/android/bh;->f:I

    .line 71
    iget-object v0, p0, Lcom/twitter/android/bh;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/bj;

    invoke-virtual {v0}, Lcom/twitter/android/bj;->a()V

    .line 72
    return-void

    .line 60
    :cond_0
    const v0, 0x7f04012d

    goto :goto_0

    .line 64
    :cond_1
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f040375

    :goto_3
    iput v0, p0, Lcom/twitter/android/bh;->e:I

    goto :goto_1

    :cond_2
    const v0, 0x7f0402df

    goto :goto_3

    .line 69
    :cond_3
    const v0, 0x7f0402e3

    goto :goto_2
.end method

.method public static a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;ILandroid/view/View$OnClickListener;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 77
    invoke-static {p0, p1, p2, p3}, Lcom/twitter/android/bh;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    .line 78
    if-eqz p4, :cond_0

    .line 79
    invoke-virtual {v0, p5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 80
    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {v1, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/android/bh;->k:Z

    invoke-static {v1}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 155
    iget v0, p0, Lcom/twitter/android/bh;->d:I

    sparse-switch v0, :sswitch_data_0

    .line 171
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/twitter/android/bh;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    .line 157
    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/bh;->b:Landroid/content/Context;

    const v1, 0x7f0a0a3e

    .line 158
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/bh;->e:I

    .line 157
    invoke-static {p1, p2, v0, v1}, Lcom/twitter/android/bh;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v1

    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 160
    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 161
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 166
    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/bh;->b:Landroid/content/Context;

    const v1, 0x7f0a06c7

    .line 167
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/bh;->g:I

    iget-object v4, p0, Lcom/twitter/android/bh;->l:Landroid/view/View$OnClickListener;

    const v5, 0x7f13001f

    move-object v0, p1

    move-object v1, p2

    .line 166
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/bh;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;ILandroid/view/View$OnClickListener;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 155
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/bh;->i:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/twitter/android/bh;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/bj;

    invoke-virtual {v0, p1}, Lcom/twitter/android/bj;->getItemId(I)J

    move-result-wide v2

    .line 132
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/bh;->b:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    .line 133
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "type"

    const/16 v4, 0xa

    .line 134
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v4, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v5, 0x5

    .line 138
    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v5, p0, Lcom/twitter/android/bh;->i:Lcom/twitter/model/core/TwitterUser;

    iget-wide v6, v5, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 139
    invoke-virtual {v0, v6, v7}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-boolean v5, p0, Lcom/twitter/android/bh;->k:Z

    .line 140
    invoke-static {v5}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v5, "similar_to"

    .line 141
    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 136
    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 142
    iget-object v0, p0, Lcom/twitter/android/bh;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/bj;

    invoke-virtual {v0}, Lcom/twitter/android/bj;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    .line 143
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_0

    .line 145
    const-string/jumbo v2, "friendship"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    move-object v0, v1

    .line 149
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/twitter/android/bh;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bj;

    invoke-virtual {v0}, Lcom/twitter/android/bj;->k()Lcjt;

    move-result-object v0

    new-instance v1, Lcbe;

    invoke-direct {v1, p1}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/bh;->notifyDataSetChanged()V

    .line 215
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/twitter/android/bh;->l:Landroid/view/View$OnClickListener;

    .line 210
    return-void
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;Z)V
    .locals 4

    .prologue
    .line 193
    iput-boolean p2, p0, Lcom/twitter/android/bh;->k:Z

    .line 194
    iget-object v0, p0, Lcom/twitter/android/bh;->i:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/bh;->i:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/TwitterUser;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/ca;

    iget-object v0, v0, Lcom/twitter/android/ca;->c:Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/ca;

    iget-object v1, v1, Lcom/twitter/android/ca;->c:Landroid/content/Intent;

    invoke-static {v1}, Lcom/twitter/app/users/f;->a(Landroid/content/Intent;)Lcom/twitter/app/users/f;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/app/users/f;->b(Ljava/lang/String;)Lcom/twitter/app/users/f;

    move-result-object v1

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 199
    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/users/f;->a(J)Lcom/twitter/app/users/f;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/bh;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 198
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 200
    iput-object p1, p0, Lcom/twitter/android/bh;->i:Lcom/twitter/model/core/TwitterUser;

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/bh;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method protected b(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 178
    iget v0, p0, Lcom/twitter/android/bh;->d:I

    packed-switch v0, :pswitch_data_0

    .line 186
    iget v0, p0, Lcom/twitter/android/bh;->f:I

    iget-object v1, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/ca;

    iget v2, p0, Lcom/twitter/android/bh;->h:F

    invoke-static {v0, p1, p2, v1, v2}, Lcom/twitter/android/cb;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/ca;F)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 180
    :pswitch_0
    new-instance v0, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 181
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
    .end packed-switch
.end method

.method protected b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/ca;

    iget-object v0, v0, Lcom/twitter/android/ca;->c:Landroid/content/Intent;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 92
    iget-boolean v0, p0, Lcom/twitter/android/bh;->j:Z

    if-nez v0, :cond_0

    .line 94
    iget v0, p0, Lcom/twitter/android/bh;->d:I

    sparse-switch v0, :sswitch_data_0

    .line 104
    const/4 v0, 0x0

    .line 108
    :goto_0
    if-eqz v0, :cond_0

    .line 109
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 110
    invoke-direct {p0, v0}, Lcom/twitter/android/bh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 111
    iput-boolean v4, p0, Lcom/twitter/android/bh;->j:Z

    .line 115
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/widget/p;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 96
    :sswitch_0
    const-string/jumbo v0, "similar_to:::impression"

    goto :goto_0

    .line 100
    :sswitch_1
    const-string/jumbo v0, "user_similarities_list:::impression"

    goto :goto_0

    .line 94
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method
