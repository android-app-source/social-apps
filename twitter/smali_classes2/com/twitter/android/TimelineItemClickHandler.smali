.class public Lcom/twitter/android/TimelineItemClickHandler;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/TimelineItemClickHandler$TimelineItemClickException;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final c:Landroid/support/v4/app/FragmentManager;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/twitter/android/revenue/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/revenue/c;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/twitter/android/TimelineItemClickHandler;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 66
    iput-object p5, p0, Lcom/twitter/android/TimelineItemClickHandler;->c:Landroid/support/v4/app/FragmentManager;

    .line 67
    iput-object p6, p0, Lcom/twitter/android/TimelineItemClickHandler;->f:Lcom/twitter/android/revenue/c;

    .line 68
    iput-object p3, p0, Lcom/twitter/android/TimelineItemClickHandler;->d:Ljava/lang/String;

    .line 69
    iput-object p4, p0, Lcom/twitter/android/TimelineItemClickHandler;->e:Ljava/lang/String;

    .line 70
    return-void
.end method

.method private a(Lcom/twitter/library/api/PromotedEvent;JLcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 249
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lbel;

    invoke-direct {v2, v0, p4, p1}, Lbel;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/PromotedEvent;)V

    .line 251
    invoke-virtual {v2, p2, p3}, Lbel;->a(J)Lbel;

    move-result-object v0

    .line 250
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 252
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/twitter/android/timeline/bk;IILcom/twitter/library/client/Session;Lcom/twitter/android/TweetActivity$a;)V
    .locals 13

    .prologue
    .line 78
    invoke-virtual/range {p5 .. p5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 79
    instance-of v2, p2, Lcom/twitter/android/timeline/ak;

    if-eqz v2, :cond_1

    .line 80
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/bg;

    .line 81
    iget-object v2, v2, Lcom/twitter/android/bg;->d:Lcom/twitter/library/widget/TweetView;

    .line 82
    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    .line 83
    iget-object v3, p0, Lcom/twitter/android/TimelineItemClickHandler;->c:Landroid/support/v4/app/FragmentManager;

    .line 84
    invoke-static {v3, v2}, Lcom/twitter/android/widget/ConfirmCancelPendingTweetDialog;->a(Landroid/support/v4/app/FragmentManager;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/widget/ConfirmCancelPendingTweetDialog;

    .line 244
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 85
    :cond_1
    instance-of v2, p2, Lcom/twitter/android/timeline/ca;

    if-eqz v2, :cond_a

    .line 86
    check-cast p2, Lcom/twitter/android/timeline/ca;

    .line 87
    new-instance v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 92
    iget-object v2, p2, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    if-eqz v2, :cond_7

    iget-object v2, p2, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget v2, v2, Lcom/twitter/model/topic/e;->k:I

    move v3, v2

    .line 94
    :goto_1
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/TimelineItemClickHandler;->d:Ljava/lang/String;

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "trend_row"

    aput-object v6, v2, v5

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput-object v6, v2, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "trend"

    aput-object v6, v2, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "click"

    aput-object v6, v2, v5

    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 95
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/TimelineItemClickHandler;->d:Ljava/lang/String;

    aput-object v7, v2, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "trend_row"

    aput-object v7, v2, v6

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput-object v7, v2, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "trend"

    aput-object v7, v2, v6

    const/4 v6, 0x4

    const-string/jumbo v7, "search"

    aput-object v7, v2, v6

    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 96
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/twitter/android/TimelineItemClickHandler;->d:Ljava/lang/String;

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "trend_row"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    const/4 v8, 0x0

    aput-object v8, v2, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "promoted_trend"

    aput-object v8, v2, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "click"

    aput-object v8, v2, v7

    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 97
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/twitter/android/TimelineItemClickHandler;->d:Ljava/lang/String;

    aput-object v9, v2, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "trend_row"

    aput-object v9, v2, v8

    const/4 v8, 0x2

    const/4 v9, 0x0

    aput-object v9, v2, v8

    const/4 v8, 0x3

    const-string/jumbo v9, "promoted_trend"

    aput-object v9, v2, v8

    const/4 v8, 0x4

    const-string/jumbo v9, "search"

    aput-object v9, v2, v8

    .line 98
    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 99
    iget-object v2, p2, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    if-eqz v2, :cond_2

    .line 100
    iget-object v2, p2, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-boolean v2, v2, Lcom/twitter/model/topic/e;->j:Z

    .line 101
    invoke-static {v2}, Lcom/twitter/model/topic/e;->a(Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 104
    :cond_2
    const/16 v2, 0x8

    iput v2, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 105
    iget-object v2, p2, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    iput-object v2, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 108
    iget-object v2, p2, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    if-eqz v2, :cond_8

    .line 109
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->h:Lcom/twitter/library/api/PromotedEvent;

    iget-object v9, p2, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    iget-wide v10, v9, Lcgi;->e:J

    move-object/from16 v0, p5

    invoke-direct {p0, v2, v10, v11, v0}, Lcom/twitter/android/TimelineItemClickHandler;->a(Lcom/twitter/library/api/PromotedEvent;JLcom/twitter/library/client/Session;)V

    .line 111
    iget-object v2, p2, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    iget-wide v10, v2, Lcgi;->e:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 112
    if-eqz v7, :cond_3

    .line 113
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    iget-object v9, p2, Lcom/twitter/android/timeline/ca;->l:Ljava/lang/String;

    .line 115
    invoke-virtual {v2, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    .line 116
    invoke-virtual {v2, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 117
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 118
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 119
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 121
    :cond_3
    if-eqz v8, :cond_4

    .line 122
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    iget-object v7, p2, Lcom/twitter/android/timeline/ca;->l:Ljava/lang/String;

    .line 124
    invoke-virtual {v2, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v8, v7, v9

    .line 125
    invoke-virtual {v2, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 126
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 127
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 128
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 134
    :cond_4
    :goto_2
    if-eqz v5, :cond_5

    .line 135
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    iget-object v7, p2, Lcom/twitter/android/timeline/ca;->l:Ljava/lang/String;

    .line 136
    invoke-virtual {v2, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    .line 137
    invoke-virtual {v2, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 138
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 139
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 135
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 141
    :cond_5
    if-eqz v6, :cond_6

    .line 142
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    iget-object v5, p2, Lcom/twitter/android/timeline/ca;->l:Ljava/lang/String;

    .line 143
    invoke-virtual {v2, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    .line 144
    invoke-virtual {v2, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 145
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 146
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 142
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 149
    :cond_6
    iget-object v2, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    iget-object v3, p2, Lcom/twitter/android/timeline/ca;->a:Ljava/lang/String;

    iget v4, p2, Lcom/twitter/android/timeline/ca;->n:I

    iget-object v5, p2, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    iget-object v6, p2, Lcom/twitter/android/timeline/ca;->l:Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p2, Lcom/twitter/android/timeline/ca;->m:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget-object v11, p2, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    if-eqz v11, :cond_9

    iget-object v11, p2, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-object v11, v11, Lcom/twitter/model/topic/e;->d:Lcom/twitter/model/topic/trends/d;

    iget-object v11, v11, Lcom/twitter/model/topic/trends/d;->c:Ljava/util/ArrayList;

    .line 150
    :goto_3
    invoke-static/range {v2 .. v11}, Lcom/twitter/android/cj;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/android/widget/TopicView$TopicData;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v2

    .line 153
    iget-object v3, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 92
    :cond_7
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_1

    .line 131
    :cond_8
    iget v2, p2, Lcom/twitter/android/timeline/ca;->o:I

    iput v2, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->y:I

    goto :goto_2

    .line 149
    :cond_9
    const/4 v11, 0x0

    goto :goto_3

    .line 156
    :cond_a
    sparse-switch p4, :sswitch_data_0

    .line 210
    :goto_4
    const/4 v2, 0x0

    .line 211
    instance-of v3, p2, Lcom/twitter/android/timeline/cd;

    if-eqz v3, :cond_e

    .line 212
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/timeline/cd;

    iget-object v2, v2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 220
    :cond_b
    :goto_5
    if-eqz v2, :cond_11

    .line 221
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 223
    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Lcom/twitter/android/TweetActivity$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/TweetActivity$a;

    move-result-object v3

    .line 224
    invoke-virtual {v3, p2}, Lcom/twitter/android/TweetActivity$a;->a(Lcom/twitter/android/timeline/bk;)Lcom/twitter/android/TweetActivity$a;

    move-result-object v3

    .line 225
    invoke-virtual {v3}, Lcom/twitter/android/TweetActivity$a;->b()V

    .line 227
    invoke-static {v2}, Lcom/twitter/android/av/h;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 228
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 229
    iget-object v4, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    .line 230
    invoke-static {v2}, Lcom/twitter/android/cp;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v5

    .line 229
    invoke-static {v3, v4, v2, v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 231
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/TimelineItemClickHandler;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 232
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v7, "tweet"

    const-string/jumbo v8, "click"

    .line 231
    invoke-static {v6, v2, v7, v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/TimelineItemClickHandler;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 233
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 231
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 160
    :sswitch_1
    invoke-virtual {p2}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v2

    .line 162
    iget v2, v2, Lcom/twitter/android/timeline/bg;->e:I

    .line 164
    sparse-switch v2, :sswitch_data_1

    goto :goto_4

    .line 180
    :sswitch_2
    check-cast p2, Lcom/twitter/android/timeline/x;

    .line 182
    new-instance v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 183
    const/4 v12, 0x0

    .line 184
    iget-object v2, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_c

    .line 185
    iget-object v2, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v12

    .line 187
    :cond_c
    iget-object v2, p2, Lcom/twitter/android/timeline/x;->e:Lcom/twitter/model/timeline/r;

    iput-object v2, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 188
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v2, 0x5

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v8, p0, Lcom/twitter/android/TimelineItemClickHandler;->d:Ljava/lang/String;

    aput-object v8, v5, v2

    const/4 v2, 0x1

    iget-object v8, p0, Lcom/twitter/android/TimelineItemClickHandler;->e:Ljava/lang/String;

    aput-object v8, v5, v2

    const/4 v8, 0x2

    iget-object v2, p2, Lcom/twitter/android/timeline/x;->e:Lcom/twitter/model/timeline/r;

    if-eqz v2, :cond_d

    iget-object v2, p2, Lcom/twitter/android/timeline/x;->e:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    :goto_6
    aput-object v2, v5, v8

    const/4 v2, 0x3

    const-string/jumbo v8, "footer"

    aput-object v8, v5, v2

    const/4 v2, 0x4

    const-string/jumbo v8, "click"

    aput-object v8, v5, v2

    .line 189
    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 191
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 188
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 192
    iget-object v3, p0, Lcom/twitter/android/TimelineItemClickHandler;->a:Landroid/content/Context;

    const/4 v4, 0x0

    iget-object v2, p2, Lcom/twitter/android/timeline/x;->a:Lcom/twitter/model/timeline/m;

    iget-object v5, v2, Lcom/twitter/model/timeline/m;->c:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/twitter/android/TimelineItemClickHandler;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v11, 0x0

    invoke-static/range {v3 .. v12}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 167
    :sswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/cf$g;

    .line 168
    iget-object v2, v2, Lcom/twitter/android/cf$g;->a:Lcom/twitter/android/cf$b;

    .line 169
    if-eqz v2, :cond_0

    .line 170
    iget-wide v2, v2, Lcom/twitter/android/cf$b;->c:J

    move-object/from16 v0, p6

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/TweetActivity$a;->a(J)Lcom/twitter/android/TweetActivity$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/TweetActivity$a;->b()V

    goto/16 :goto_0

    .line 188
    :cond_d
    const/4 v2, 0x0

    goto :goto_6

    .line 213
    :cond_e
    instance-of v3, p2, Lcom/twitter/android/timeline/ap;

    if-eqz v3, :cond_f

    move-object v2, p2

    .line 214
    check-cast v2, Lcom/twitter/android/timeline/ap;

    iget-object v2, v2, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    iget-object v2, v2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    goto/16 :goto_5

    .line 215
    :cond_f
    instance-of v3, p2, Lcom/twitter/android/timeline/a;

    if-eqz v3, :cond_b

    .line 216
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/timeline/a;

    .line 217
    iget-object v3, p0, Lcom/twitter/android/TimelineItemClickHandler;->f:Lcom/twitter/android/revenue/c;

    iget-object v2, v2, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/twitter/android/revenue/c;->a(Ljava/lang/String;)Lcom/twitter/android/revenue/a;

    move-result-object v2

    .line 218
    instance-of v3, v2, Lcom/twitter/android/revenue/l;

    if-eqz v3, :cond_10

    check-cast v2, Lcom/twitter/android/revenue/l;

    iget-object v2, v2, Lcom/twitter/android/revenue/l;->f:Lcom/twitter/model/core/Tweet;

    goto/16 :goto_5

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 237
    :cond_11
    new-instance v2, Lcpb;

    new-instance v3, Lcom/twitter/android/TimelineItemClickHandler$TimelineItemClickException;

    invoke-direct {v3}, Lcom/twitter/android/TimelineItemClickHandler$TimelineItemClickException;-><init>()V

    invoke-direct {v2, v3}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v3, "class"

    .line 238
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v3, "entityId"

    .line 239
    invoke-virtual {p2}, Lcom/twitter/android/timeline/bk;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v3, "entityDataFlags"

    .line 240
    invoke-virtual {p2}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v4

    iget v4, v4, Lcom/twitter/android/timeline/bg;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v3, "entityDataType"

    .line 241
    invoke-virtual {p2}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v4

    iget v4, v4, Lcom/twitter/android/timeline/bg;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    .line 237
    invoke-static {v2}, Lcpd;->c(Lcpb;)V

    goto/16 :goto_0

    .line 156
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1b -> :sswitch_1
        0x1c -> :sswitch_1
    .end sparse-switch

    .line 164
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xd -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch
.end method
