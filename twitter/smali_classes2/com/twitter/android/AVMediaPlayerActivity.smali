.class public Lcom/twitter/android/AVMediaPlayerActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# instance fields
.field protected a:Lcom/twitter/library/av/playback/AVPlayer;

.field protected b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field protected c:Lcom/twitter/library/av/VideoPlayerView;

.field protected d:Lcom/twitter/library/av/playback/u;

.field protected e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final f:Lcom/twitter/library/av/playback/q;

.field private g:Lcom/twitter/library/av/playback/AVDataSource;

.field private h:Z

.field private i:Z

.field private j:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 44
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->f:Lcom/twitter/library/av/playback/q;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->i:Z

    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->j:Landroid/view/ViewGroup;

    .line 59
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Landroid/view/ViewGroup;)V

    .line 60
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 69
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 71
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 73
    :cond_0
    return-object v0
.end method

.method protected a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->i()V

    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method protected a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 112
    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected b()Lcom/twitter/library/av/VideoPlayerView;
    .locals 3

    .prologue
    .line 176
    new-instance v0, Lcom/twitter/library/av/VideoPlayerView;

    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->b:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 79
    const-string/jumbo v0, "is_from_dock"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->i:Z

    .line 80
    const-string/jumbo v0, "is_from_inline"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->h:Z

    .line 81
    iget-boolean v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->i:Z

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    const v0, 0x7f050030

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/AVMediaPlayerActivity;->overridePendingTransition(II)V

    .line 85
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 87
    const-string/jumbo v0, "association"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 88
    if-nez p1, :cond_2

    .line 89
    const-string/jumbo v0, "ms"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVDataSource;

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->g:Lcom/twitter/library/av/playback/AVDataSource;

    .line 94
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->g:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    .line 95
    invoke-virtual {p0, v0}, Lcom/twitter/android/AVMediaPlayerActivity;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 97
    const v0, 0x7f0a051f

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->finish()V

    .line 106
    :goto_1
    return-void

    .line 91
    :cond_2
    const-string/jumbo v0, "ms"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVDataSource;

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->g:Lcom/twitter/library/av/playback/AVDataSource;

    goto :goto_0

    .line 102
    :cond_3
    new-instance v0, Lcom/twitter/library/av/playback/v;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/v;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->g:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->d:Lcom/twitter/library/av/playback/u;

    .line 104
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AVMediaPlayerActivity;->b(Landroid/view/ViewGroup;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AVMediaPlayerActivity;->setContentView(Landroid/view/View;)V

    goto :goto_1
.end method

.method protected b(Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 129
    new-instance v0, Lcom/twitter/library/av/playback/r;

    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/r;-><init>(Lcom/twitter/library/av/playback/q;)V

    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->d:Lcom/twitter/library/av/playback/u;

    .line 130
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 131
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Landroid/content/Context;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    sget-object v1, Lbyo;->b:Lbyf;

    .line 133
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lbyf;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 134
    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->b(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 135
    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/r;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 137
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->b()Lcom/twitter/library/av/VideoPlayerView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->c:Lcom/twitter/library/av/VideoPlayerView;

    .line 140
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->c:Lcom/twitter/library/av/VideoPlayerView;

    const v1, 0x7f13000f

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoPlayerView;->setId(I)V

    .line 142
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->g:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->c:Lcom/twitter/library/av/VideoPlayerView;

    new-instance v1, Lcom/twitter/library/av/y;

    iget-object v2, p0, Lcom/twitter/android/AVMediaPlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    iget-object v3, p0, Lcom/twitter/android/AVMediaPlayerActivity;->c:Lcom/twitter/library/av/VideoPlayerView;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/av/y;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoPlayerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->Q()Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 148
    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    if-eq v0, v1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->c:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->b()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/av/VideoPlayerView;->a(II)V

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->c:Lcom/twitter/library/av/VideoPlayerView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    invoke-virtual {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->c()Lcom/twitter/library/av/control/e;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_2

    .line 157
    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 158
    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->c:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/VideoPlayerView;->setExternalChromeView(Lcom/twitter/library/av/control/e;)V

    .line 159
    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-interface {v0, v1}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 162
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->d_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 165
    :cond_3
    return-void
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/twitter/library/av/control/e;
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    return-object v0
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 214
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 217
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->f:Lcom/twitter/library/av/playback/q;

    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 219
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->f:Lcom/twitter/library/av/playback/q;

    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->b(Lcom/twitter/library/av/playback/u;)V

    .line 221
    :cond_0
    return-void
.end method

.method protected d_()Z
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 226
    iget-boolean v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->h:Z

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x0

    const v1, 0x7f050032

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/AVMediaPlayerActivity;->overridePendingTransition(II)V

    .line 229
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 199
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 202
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 193
    const-string/jumbo v0, "ms"

    iget-object v1, p0, Lcom/twitter/android/AVMediaPlayerActivity;->g:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 194
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/twitter/android/AVMediaPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->j()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 210
    :cond_0
    return-void
.end method
