.class public Lcom/twitter/android/cs;
.super Lcom/twitter/library/view/a;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/library/client/Session;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/twitter/library/view/a;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/cs;->a:Ljava/lang/ref/WeakReference;

    .line 52
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cs;->b:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lcom/twitter/android/cs;->c:Lcom/twitter/library/client/Session;

    .line 54
    iput-object p3, p0, Lcom/twitter/android/cs;->d:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Lcom/twitter/android/cs;->e:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Lcom/twitter/android/cs;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 57
    return-void
.end method

.method private a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 4

    .prologue
    .line 65
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v1

    .line 66
    sget-object v0, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v0}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v2

    .line 67
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 68
    const-string/jumbo v3, "app_download_client_event"

    invoke-virtual {p1, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 69
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 70
    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/util/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    const-string/jumbo v3, "3"

    invoke-virtual {p1, v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 74
    const-string/jumbo v0, "4"

    invoke-virtual {p1, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 77
    :cond_0
    if-eqz v2, :cond_1

    .line 78
    const-string/jumbo v0, "6"

    .line 79
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 78
    invoke-virtual {p1, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 80
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 82
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i()Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 83
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;ZLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 6

    .prologue
    .line 91
    if-nez p3, :cond_0

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->G:J

    iget-object v1, p0, Lcom/twitter/android/cs;->c:Lcom/twitter/library/client/Session;

    .line 95
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 94
    invoke-static {v2, v3, v4, v5}, Lcom/twitter/database/schema/a;->b(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    .line 96
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x23c1

    .line 93
    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 108
    :goto_0
    return-void

    .line 100
    :cond_0
    new-instance v0, Lcom/twitter/android/av/ad;

    invoke-direct {v0}, Lcom/twitter/android/av/ad;-><init>()V

    .line 101
    invoke-virtual {v0, p2}, Lcom/twitter/android/av/ad;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/ab;

    move-result-object v0

    .line 102
    invoke-virtual {v0, p4}, Lcom/twitter/library/av/ab;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/ab;

    move-result-object v0

    const/4 v1, 0x0

    .line 103
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->c(Z)Lcom/twitter/library/av/ab;

    move-result-object v0

    .line 105
    invoke-static {p1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    invoke-virtual {v1}, Lbaa;->k()Z

    move-result v1

    .line 104
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->e(Z)Lcom/twitter/library/av/ab;

    move-result-object v0

    .line 106
    invoke-virtual {v0, p1}, Lcom/twitter/library/av/ab;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/cs;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/cs;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/twitter/android/cs;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 62
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/cs;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 114
    if-eqz v1, :cond_2

    .line 115
    if-eqz p1, :cond_6

    .line 116
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 118
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 117
    invoke-static {v4}, Lcom/twitter/model/util/c;->c(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v4

    .line 119
    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {v0}, Lcax;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v4, :cond_3

    iget-object v0, v4, Lcom/twitter/model/core/MediaEntity;->E:Ljava/lang/String;

    iget-object v4, p2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    .line 121
    invoke-static {v0, v4}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 122
    :goto_0
    if-eqz v0, :cond_4

    .line 123
    invoke-virtual {p0, v1, p1, v3, p4}, Lcom/twitter/android/cs;->a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;ZLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 147
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v3

    .line 121
    goto :goto_0

    .line 125
    :cond_4
    invoke-static {p1}, Lbwr;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 126
    iget-object v0, p0, Lcom/twitter/android/cs;->b:Landroid/content/Context;

    invoke-static {v0, p1, v7}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 128
    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 129
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v4, p0, Lcom/twitter/android/cs;->c:Lcom/twitter/library/client/Session;

    .line 130
    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 131
    new-array v2, v2, [Ljava/lang/String;

    aput-object p3, v2, v3

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 132
    iget-object v2, p0, Lcom/twitter/android/cs;->b:Landroid/content/Context;

    invoke-static {v0, v2, p1, v7}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    .line 134
    iget-object v2, p0, Lcom/twitter/android/cs;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 135
    invoke-direct {p0, v0}, Lcom/twitter/android/cs;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 139
    :cond_5
    invoke-static {p1}, Lcom/twitter/library/client/BrowserDataSourceFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/client/BrowserDataSource;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/cs;->c:Lcom/twitter/library/client/Session;

    .line 140
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v9, p0, Lcom/twitter/android/cs;->e:Ljava/lang/String;

    move-object v3, p2

    move-object v6, p3

    move-object v8, p4

    .line 139
    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    goto :goto_1

    .line 143
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/cs;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v9, p0, Lcom/twitter/android/cs;->e:Ljava/lang/String;

    move-object v2, v7

    move-object v3, p2

    move-object v6, p3

    move-object v8, p4

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    goto :goto_1
.end method
