.class public Lcom/twitter/android/util/m;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/util/m;->a:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/util/m;->b:Ljava/util/List;

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/util/m;->a(J)V

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/util/m;)J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/twitter/android/util/m;->c:J

    return-wide v0
.end method

.method private a(ZZII)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 146
    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/util/m;->g:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 147
    :goto_0
    if-eqz p1, :cond_2

    .line 148
    if-nez v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/twitter/android/util/m;->a:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Lcom/twitter/android/util/m;->b:Ljava/util/List;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    :goto_1
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/util/m;->a:Ljava/util/List;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lcom/twitter/android/util/m;->b:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    iput-boolean v1, p0, Lcom/twitter/android/util/m;->g:Z

    goto :goto_1

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/util/m;->a:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lcom/twitter/android/util/m;->a:Ljava/util/List;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/twitter/android/util/m;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/util/m;->e:Z

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    .line 58
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/util/m;->f:Z

    if-eqz v0, :cond_2

    .line 59
    const/4 v0, 0x2

    goto :goto_0

    .line 61
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/twitter/android/util/m;->c:J

    .line 51
    invoke-virtual {p0}, Lcom/twitter/android/util/m;->b()V

    .line 52
    return-void
.end method

.method public a(Lcmr;)V
    .locals 6

    .prologue
    const v5, 0x7f1308a1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 96
    iput-boolean v4, p0, Lcom/twitter/android/util/m;->g:Z

    .line 97
    iget-boolean v0, p0, Lcom/twitter/android/util/m;->d:Z

    const v1, 0x7f1307a8

    invoke-direct {p0, v0, v3, v1, v4}, Lcom/twitter/android/util/m;->a(ZZII)V

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/util/m;->a()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 99
    iput-boolean v3, p0, Lcom/twitter/android/util/m;->g:Z

    .line 101
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/util/m;->f:Z

    const v1, 0x7f1307a9

    const v2, 0x7f13089e

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/twitter/android/util/m;->a(ZZII)V

    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/util/m;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 103
    iput-boolean v3, p0, Lcom/twitter/android/util/m;->g:Z

    .line 107
    :cond_1
    iput-boolean v3, p0, Lcom/twitter/android/util/m;->g:Z

    .line 109
    invoke-direct {p0, v3, v3, v5, v5}, Lcom/twitter/android/util/m;->a(ZZII)V

    .line 111
    iget-object v0, p0, Lcom/twitter/android/util/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 112
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_2

    .line 113
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_2

    .line 115
    invoke-interface {v0, v4}, Lcmm;->f(Z)Lcmm;

    goto :goto_0

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/util/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 120
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_4

    .line 123
    invoke-interface {v0, v3}, Lcmm;->f(Z)Lcmm;

    goto :goto_1

    .line 127
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/util/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/util/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 129
    return-void
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/android/client/i;)V
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/twitter/android/util/m$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/util/m$1;-><init>(Lcom/twitter/android/util/m;Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/android/client/i;)V

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->post(Ljava/lang/Runnable;)Z

    .line 203
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lbsd;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/util/m;->d:Z

    .line 66
    invoke-static {}, Lwk;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/util/m;->e:Z

    .line 67
    iget-wide v0, p0, Lcom/twitter/android/util/m;->c:J

    invoke-static {v0, v1}, Lbsi;->g(J)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/util/m;->f:Z

    .line 68
    return-void
.end method
