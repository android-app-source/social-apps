.class public Lcom/twitter/android/util/AppRatingPromptHelper;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/util/AppRatingPromptHelper$a;,
        Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/android/util/AppRatingPromptHelper$a;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-static {p0}, Lcom/twitter/android/util/AppRatingPromptHelper;->c(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 106
    const-string/jumbo v1, "donotshow"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    const/4 v0, -0x1

    .line 128
    :goto_0
    return v0

    .line 110
    :cond_0
    const-string/jumbo v1, "lastuse"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 111
    const-string/jumbo v1, "consecutivedays"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 113
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 114
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 115
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 116
    invoke-virtual {v6, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 118
    invoke-virtual {p1, v6}, Lcom/twitter/android/util/AppRatingPromptHelper$a;->a(Ljava/util/Calendar;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 119
    add-int/lit8 v0, v1, 0x1

    .line 120
    const-string/jumbo v1, "consecutivedays"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 126
    :goto_1
    const-string/jumbo v1, "lastuse"

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v2, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 127
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {p1, v6, v3}, Lcom/twitter/android/util/AppRatingPromptHelper$a;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 123
    const-string/jumbo v1, "consecutivedays"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;J)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 28
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 30
    const-string/jumbo v0, "rate_us_prompt"

    .line 33
    sget-object v0, Lcom/twitter/android/util/AppRatingPromptHelper$1;->a:[I

    invoke-virtual {p0}, Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    move-object v2, v1

    .line 90
    :goto_0
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "rate_us_prompt"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    aput-object v1, v4, v5

    const/4 v1, 0x3

    aput-object v2, v4, v1

    const/4 v1, 0x4

    aput-object v0, v4, v1

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 92
    return-object v3

    .line 36
    :pswitch_0
    const-string/jumbo v0, "impression"

    move-object v2, v1

    .line 37
    goto :goto_0

    .line 40
    :pswitch_1
    const-string/jumbo v2, "rate_us_yes"

    .line 41
    const-string/jumbo v0, "click"

    goto :goto_0

    .line 45
    :pswitch_2
    const-string/jumbo v2, "rate_us_no"

    .line 46
    const-string/jumbo v0, "click"

    goto :goto_0

    .line 50
    :pswitch_3
    const-string/jumbo v2, "rate_us_later"

    .line 51
    const-string/jumbo v0, "click"

    goto :goto_0

    .line 55
    :pswitch_4
    const-string/jumbo v2, "feedback"

    .line 56
    const-string/jumbo v0, "click"

    goto :goto_0

    .line 60
    :pswitch_5
    const-string/jumbo v2, "rating"

    .line 61
    const-string/jumbo v0, "1star"

    goto :goto_0

    .line 65
    :pswitch_6
    const-string/jumbo v2, "rating"

    .line 66
    const-string/jumbo v0, "2star"

    goto :goto_0

    .line 70
    :pswitch_7
    const-string/jumbo v2, "rating"

    .line 71
    const-string/jumbo v0, "3star"

    goto :goto_0

    .line 75
    :pswitch_8
    const-string/jumbo v2, "rating"

    .line 76
    const-string/jumbo v0, "4star"

    goto :goto_0

    .line 80
    :pswitch_9
    const-string/jumbo v2, "rating"

    .line 81
    const-string/jumbo v0, "5star"

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 132
    invoke-static {p0}, Lcom/twitter/android/util/AppRatingPromptHelper;->c(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 133
    const-string/jumbo v1, "donotshow"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 134
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 135
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 138
    invoke-static {p0}, Lcom/twitter/android/util/AppRatingPromptHelper;->c(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 139
    const-string/jumbo v1, "consecutivedays"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 140
    const-string/jumbo v1, "lastuse"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 141
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 142
    return-void
.end method

.method public static b(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;J)V
    .locals 1

    .prologue
    .line 101
    invoke-static {p0, p1, p2}, Lcom/twitter/android/util/AppRatingPromptHelper;->a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;J)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 102
    return-void
.end method

.method private static c(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 145
    const-string/jumbo v0, "appratingusage"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
