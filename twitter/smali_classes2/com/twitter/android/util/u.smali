.class public Lcom/twitter/android/util/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/util/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/util/u$b;,
        Lcom/twitter/android/util/u$a;,
        Lcom/twitter/android/util/u$c;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field private c:Lcom/twitter/android/util/f;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/util/u;->b:Z

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/util/u;Lcom/twitter/android/util/f;)Lcom/twitter/android/util/f;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    return-object p1
.end method

.method static synthetic a(JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    invoke-static {p0, p1, p2}, Lcom/twitter/android/util/u;->b(JLjava/lang/String;)V

    return-void
.end method

.method private static b(JLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 232
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "unavailable"

    move-object v1, v0

    .line 233
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 234
    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "app"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const-string/jumbo v3, "phone_number"

    aput-object v3, v2, v5

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    aput-object v1, v2, v3

    .line 235
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 236
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 237
    return-void

    .line 232
    :cond_0
    const-string/jumbo v0, "available"

    move-object v1, v0

    goto :goto_0
.end method

.method private s()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    iget-object v0, v0, Lcom/twitter/android/util/f;->b:Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/u;->b(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method private u()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 274
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v2, "data"

    .line 275
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/util/u$c;->a:[Ljava/lang/String;

    const-string/jumbo v3, "mimetype= ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "vnd.android.cursor.item/phone_v2"

    aput-object v6, v4, v5

    const-string/jumbo v5, "is_primary"

    .line 274
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    :goto_1
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 282
    const/4 v0, 0x0

    goto :goto_0

    .line 280
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private v()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/u;->b(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->a:Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/util/u;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 350
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v0

    .line 351
    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->b(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    invoke-virtual {v0, p1, p2}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 354
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 361
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {p0, p1}, Lcom/twitter/android/util/u;->b(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v0

    sget-object v1, Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->b:Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/util/u;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 364
    :goto_0
    if-eqz v0, :cond_1

    const-string/jumbo v1, "\\s"

    const-string/jumbo v2, "\u00a0"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 362
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 364
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/util/s$a;)V
    .locals 4

    .prologue
    .line 434
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 435
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 436
    new-instance v2, Lcom/twitter/library/api/g;

    iget-object v3, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/g;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 437
    new-instance v0, Lcom/twitter/android/util/u$a;

    invoke-direct {v0, p1}, Lcom/twitter/android/util/u$a;-><init>(Lcom/twitter/android/util/s$a;)V

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 438
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/android/util/s$b;)V
    .locals 5

    .prologue
    .line 448
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 449
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 450
    new-instance v2, Lbch;

    iget-object v3, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    const-class v4, Lbch;

    .line 451
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, p1}, Lbch;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 452
    new-instance v0, Lcom/twitter/android/util/u$b;

    invoke-direct {v0, p2}, Lcom/twitter/android/util/u$b;-><init>(Lcom/twitter/android/util/s$b;)V

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 454
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/twitter/library/util/aa;->a()Lcom/twitter/library/util/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/aa;->g()Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 375
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-object v0

    .line 380
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v2

    .line 381
    invoke-static {}, Lcom/twitter/library/util/aa;->a()Lcom/twitter/library/util/aa;

    move-result-object v1

    .line 383
    invoke-virtual {v1}, Lcom/twitter/library/util/aa;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 382
    invoke-virtual {v2, p1, v1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 384
    invoke-virtual {v2, v1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->b(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Z
    :try_end_0
    .catch Lcom/google/i18n/phonenumbers/NumberParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 385
    goto :goto_0

    .line 391
    :catch_0
    move-exception v1

    .line 392
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 389
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/u;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->k()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/util/u;->q()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/u;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/u;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 5

    .prologue
    .line 110
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "android.permission.RECEIVE_SMS"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 115
    invoke-static {}, Lcom/twitter/library/util/aa;->a()Lcom/twitter/library/util/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/util/aa;->d()I

    move-result v2

    .line 116
    if-eq v2, v0, :cond_0

    if-eqz v2, :cond_0

    move v2, v0

    .line 118
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/util/u;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 116
    goto :goto_0

    :cond_1
    move v0, v1

    .line 118
    goto :goto_1
.end method

.method public f()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/twitter/android/util/u;->s()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/twitter/android/util/u;->s()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/u;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    invoke-virtual {v0}, Lcom/twitter/android/util/f;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public i()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/util/u;->b:Z

    if-nez v0, :cond_0

    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/util/u;->b:Z

    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->l()Lrx/c;

    move-result-object v0

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 197
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 198
    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 200
    :cond_0
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/u;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/twitter/library/util/aa;->a()Lcom/twitter/library/util/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/aa;->f()Z

    move-result v0

    return v0
.end method

.method public l()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/util/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/util/u;->c:Lcom/twitter/android/util/f;

    .line 171
    new-instance v0, Lcom/twitter/android/util/u$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/util/u$2;-><init>(Lcom/twitter/android/util/u;)V

    invoke-static {v0}, Lrx/c;->a(Ljava/util/concurrent/Callable;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/util/u$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/util/u$1;-><init>(Lcom/twitter/android/util/u;)V

    .line 176
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 171
    return-object v0
.end method

.method m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    .line 206
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 207
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    const/4 v0, 0x0

    goto :goto_0
.end method

.method n()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 214
    .line 216
    :try_start_0
    invoke-direct {p0}, Lcom/twitter/android/util/u;->u()Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 217
    if-eqz v2, :cond_0

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 223
    invoke-static {v2}, Lcqc;->a(Landroid/database/Cursor;)V

    .line 225
    :goto_0
    return-object v0

    .line 223
    :cond_0
    invoke-static {v2}, Lcqc;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 220
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 221
    :goto_1
    :try_start_2
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 223
    invoke-static {v2}, Lcqc;->a(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-static {v2}, Lcqc;->a(Landroid/database/Cursor;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 220
    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    move-object v2, v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v1

    move-object v2, v0

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method o()Lcom/twitter/android/util/f;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 242
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "android.permission.READ_PHONE_STATE"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/twitter/android/util/u;->t()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 245
    if-eqz v1, :cond_0

    .line 246
    new-instance v0, Lcom/twitter/android/util/f;

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/util/f;-><init>(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;I)V

    .line 256
    :goto_0
    return-object v0

    .line 250
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "android.permission.READ_CONTACTS"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    invoke-direct {p0}, Lcom/twitter/android/util/u;->v()Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 252
    if-eqz v1, :cond_1

    .line 253
    new-instance v0, Lcom/twitter/android/util/f;

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/util/f;-><init>(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;I)V

    goto :goto_0

    .line 256
    :cond_1
    sget-object v0, Lcom/twitter/android/util/f;->a:Lcom/twitter/android/util/f;

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/twitter/android/util/u;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/u;->a(Lcom/twitter/android/util/s$a;)V

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Lcom/twitter/library/util/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/v;->b()Z

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/twitter/android/util/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Lcom/twitter/library/util/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/v;->c()Z

    move-result v0

    return v0
.end method
