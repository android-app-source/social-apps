.class public Lcom/twitter/android/util/ac;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/util/ab;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/android/util/ab;
    .locals 2

    .prologue
    .line 23
    const-class v1, Lcom/twitter/android/util/ac;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/ab;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/twitter/android/util/ad;

    invoke-direct {v0, p0}, Lcom/twitter/android/util/ad;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/ab;

    .line 25
    const-class v0, Lcom/twitter/android/util/ac;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 27
    :cond_0
    sget-object v0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/ab;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
