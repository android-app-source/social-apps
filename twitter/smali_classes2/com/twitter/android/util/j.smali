.class public Lcom/twitter/android/util/j;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/util/j$a;
    }
.end annotation


# static fields
.field public static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/util/j;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f11009d
        0x7f11009e
        0x7f11009f
        0x7f1100a0
        0x7f1100a1
        0x7f1100a2
        0x7f1100a3
    .end array-data
.end method

.method public static a(Lcom/twitter/model/drafts/DraftAttachment;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "media"

    .line 149
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_select_action"

    .line 150
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 148
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 1

    .prologue
    .line 155
    const-string/jumbo v0, "media"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    return-object v0
.end method

.method public static a(Landroid/util/SparseArray;Lcom/twitter/util/math/Size;Z)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 227
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 228
    const-string/jumbo v1, ""

    .line 229
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 230
    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    .line 231
    if-nez p2, :cond_0

    iget-object v1, v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->e:Ljava/lang/String;

    if-nez v1, :cond_2

    :cond_0
    iget-object v1, v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->b:Ljava/lang/String;

    .line 232
    :goto_1
    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, p1}, Lcom/twitter/util/math/Size;->b(Lcom/twitter/util/math/Size;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 236
    :cond_1
    return-object v1

    .line 231
    :cond_2
    iget-object v1, v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->e:Ljava/lang/String;

    goto :goto_1

    .line 229
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private static a(JLcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 211
    iget-object v2, p2, Lcom/twitter/android/composer/ComposerType;->scribeName:Ljava/lang/String;

    .line 212
    invoke-static {}, Lcom/twitter/android/util/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "trend"

    move-object v1, v0

    .line 213
    :goto_0
    if-eqz p3, :cond_1

    const-string/jumbo v0, "media"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "scribe_select_action"

    .line 214
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    :goto_1
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, ""

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    const/4 v2, 0x2

    const-string/jumbo v5, "found_media"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    aput-object v1, v4, v2

    const/4 v1, 0x4

    aput-object v0, v4, v1

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 216
    return-void

    .line 212
    :cond_0
    const-string/jumbo v0, "category"

    move-object v1, v0

    goto :goto_0

    .line 214
    :cond_1
    const-string/jumbo v0, "cancel"

    goto :goto_1
.end method

.method public static a(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V
    .locals 7
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 85
    .line 86
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    .line 87
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    invoke-virtual {v0}, Lbaa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v1}, Lbaa;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 91
    new-instance v0, Lcom/twitter/android/util/j$1;

    move-object v4, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/util/j$1;-><init>(Lbaa;JLandroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V

    .line 98
    invoke-static {p0, v0}, Lmk;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/twitter/android/util/j;->b(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;ILcom/twitter/android/composer/ComposerType;)V
    .locals 1

    .prologue
    .line 141
    invoke-static {p0, p1, p2, p3, p5}, Lcom/twitter/android/media/foundmedia/GifGalleryActivity;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/android/composer/ComposerType;)Landroid/content/Intent;

    move-result-object v0

    .line 142
    invoke-virtual {p0, v0, p4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 143
    return-void
.end method

.method public static a(Lcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V
    .locals 3
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 118
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 119
    sget-object v2, Lcom/twitter/android/media/foundmedia/a;->a:Lcom/twitter/android/media/foundmedia/a;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/media/foundmedia/a;->a(J)V

    .line 120
    invoke-static {v0, v1, p0, p1}, Lcom/twitter/android/util/j;->a(JLcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V

    .line 121
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 70
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "found_media_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/android/composer/ComposerType;)Z
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    if-ne p0, v0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 79
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "found_media_image_preview_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method static b(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V
    .locals 6

    .prologue
    .line 106
    sget-object v0, Lcom/twitter/android/media/foundmedia/a;->a:Lcom/twitter/android/media/foundmedia/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/foundmedia/a;->a()V

    .line 107
    invoke-static {}, Lcom/twitter/android/util/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    const/4 v2, 0x2

    const-string/jumbo v3, "trending"

    move-object v0, p0

    move v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/util/j;->a(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;ILcom/twitter/android/composer/ComposerType;)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/twitter/android/util/j;->c(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 124
    const-string/jumbo v0, "found_media_trending_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 160
    invoke-static {}, Lcom/twitter/android/util/j;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    const-string/jumbo v1, "found_media_disney_android_5000"

    invoke-static {v1}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 162
    if-eqz v2, :cond_2

    .line 163
    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 186
    :cond_1
    :goto_1
    return-object v0

    .line 163
    :sswitch_0
    const-string/jumbo v3, "control"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v3, "disney_enabled_normal_order"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v3, "disney_enabled_random_order"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 165
    :pswitch_0
    const-string/jumbo v0, "provider3"

    goto :goto_1

    .line 168
    :pswitch_1
    const-string/jumbo v0, "provider4"

    goto :goto_1

    .line 171
    :pswitch_2
    const-string/jumbo v0, "provider5"

    goto :goto_1

    .line 178
    :cond_2
    const-string/jumbo v1, "found_media_provider"

    invoke-static {v1}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 180
    goto :goto_1

    .line 163
    :sswitch_data_0
    .sparse-switch
        -0x3c6bb9e5 -> :sswitch_2
        -0x12598cc1 -> :sswitch_1
        0x38b7655d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V
    .locals 2

    .prologue
    .line 129
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "composer_type"

    .line 130
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 131
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 132
    return-void
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 219
    const-string/jumbo v0, "found_media_result_cache_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
