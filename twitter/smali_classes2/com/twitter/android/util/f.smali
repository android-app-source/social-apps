.class public Lcom/twitter/android/util/f;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/android/util/f;


# instance fields
.field public final b:Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/android/util/f;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/util/f;-><init>(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;I)V

    sput-object v0, Lcom/twitter/android/util/f;->a:Lcom/twitter/android/util/f;

    return-void
.end method

.method public constructor <init>(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/twitter/android/util/f;->b:Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    .line 36
    iput p2, p0, Lcom/twitter/android/util/f;->c:I

    .line 37
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/twitter/android/util/f;->c:I

    packed-switch v0, :pswitch_data_0

    .line 55
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 48
    :pswitch_0
    const-string/jumbo v0, "source_telephonymanager"

    goto :goto_0

    .line 51
    :pswitch_1
    const-string/jumbo v0, "source_google_contact"

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    if-ne p0, p1, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 66
    goto :goto_0

    .line 68
    :cond_3
    check-cast p1, Lcom/twitter/android/util/f;

    .line 69
    iget v2, p0, Lcom/twitter/android/util/f;->c:I

    iget v3, p1, Lcom/twitter/android/util/f;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/twitter/android/util/f;->b:Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    iget-object v3, p1, Lcom/twitter/android/util/f;->b:Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Lcom/twitter/android/util/f;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/f;->b:Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
