.class public Lcom/twitter/android/util/aa;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Landroid/view/View;[IIIII)I
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 64
    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 65
    aget v0, p1, v3

    sub-int/2addr v0, p3

    .line 67
    const/4 v2, -0x1

    if-eq p5, v2, :cond_0

    if-gez v0, :cond_0

    .line 81
    :goto_0
    return v0

    .line 71
    :cond_0
    if-eq p5, v3, :cond_1

    .line 73
    invoke-virtual {p0, v1, v1}, Landroid/view/View;->measure(II)V

    .line 74
    aget v0, p1, v3

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, p4

    .line 75
    if-le v0, p2, :cond_1

    .line 78
    sub-int/2addr v0, p2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 81
    goto :goto_0
.end method

.method public static a(Landroid/widget/ListView;Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v2, v0, Landroid/graphics/Point;->y:I

    .line 43
    const/4 v0, 0x2

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    move-object v0, p1

    move v3, p2

    move v4, p3

    move v5, p5

    .line 44
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/util/aa;->a(Landroid/view/View;[IIIII)I

    move-result v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    new-instance v1, Lcom/twitter/android/util/aa$1;

    invoke-direct {v1, p0, v0, p4}, Lcom/twitter/android/util/aa$1;-><init>(Landroid/widget/ListView;II)V

    invoke-virtual {p0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 55
    :cond_0
    return-void

    .line 43
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
