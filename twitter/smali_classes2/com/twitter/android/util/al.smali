.class public Lcom/twitter/android/util/al;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcok$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$a",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const-string/jumbo v0, "legacy_deciders_web_view_url_whitelist"

    .line 20
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/util/al$1;

    invoke-direct {v2}, Lcom/twitter/android/util/al$1;-><init>()V

    .line 18
    invoke-static {v0, v1, v2}, Lcok;->a(Ljava/lang/String;Ljava/lang/Object;Lcpp;)Lcok$a;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/util/al;->a:Lcok$a;

    .line 17
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/twitter/android/util/al;->a:Lcok$a;

    invoke-virtual {v0}, Lcok$a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom;

    invoke-virtual {v0}, Lcom;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 43
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
