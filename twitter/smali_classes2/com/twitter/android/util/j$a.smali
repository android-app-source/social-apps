.class abstract Lcom/twitter/android/util/j$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/util/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "a"
.end annotation


# instance fields
.field private final a:Lbaa;

.field private final b:J


# direct methods
.method constructor <init>(Lbaa;J)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput-object p1, p0, Lcom/twitter/android/util/j$a;->a:Lbaa;

    .line 245
    iput-wide p2, p0, Lcom/twitter/android/util/j$a;->b:J

    .line 246
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 265
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 250
    const/4 v0, -0x1

    if-ne v0, p2, :cond_1

    .line 251
    check-cast p1, Landroid/app/AlertDialog;

    const v0, 0x7f130335

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 252
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/twitter/android/util/j$a;->a:Lbaa;

    invoke-virtual {v0, v4, v1}, Lbaa;->c(ZZ)V

    .line 255
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/util/j$a;->b:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:accept_data:redirect::impression"

    aput-object v2, v1, v4

    .line 256
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 255
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 257
    invoke-virtual {p0}, Lcom/twitter/android/util/j$a;->a()V

    .line 263
    :goto_0
    return-void

    .line 259
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/util/j$a;->b:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:accept_data:close::impression"

    aput-object v2, v1, v4

    .line 260
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 259
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 261
    invoke-virtual {p0}, Lcom/twitter/android/util/j$a;->b()V

    goto :goto_0
.end method
