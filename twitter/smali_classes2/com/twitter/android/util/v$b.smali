.class public Lcom/twitter/android/util/v$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/util/v$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/util/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/util/v;


# direct methods
.method public constructor <init>(Lcom/twitter/android/util/v;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/twitter/android/util/v$b;->a:Lcom/twitter/android/util/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/n;)V
    .locals 8

    .prologue
    .line 156
    invoke-virtual {p1}, Lcom/twitter/model/timeline/n;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "app_graph_enabled"

    .line 157
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    :cond_0
    new-instance v0, Laut;

    iget-object v1, p0, Lcom/twitter/android/util/v$b;->a:Lcom/twitter/android/util/v;

    invoke-static {v1}, Lcom/twitter/android/util/v;->a(Lcom/twitter/android/util/v;)Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 161
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 163
    iget-object v2, p0, Lcom/twitter/android/util/v$b;->a:Lcom/twitter/android/util/v;

    invoke-static {v2}, Lcom/twitter/android/util/v;->b(Lcom/twitter/android/util/v;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 164
    const-string/jumbo v3, "profile_other"

    iget-object v4, p1, Lcom/twitter/model/timeline/n;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 165
    iget-object v3, p0, Lcom/twitter/android/util/v$b;->a:Lcom/twitter/android/util/v;

    invoke-static {v3}, Lcom/twitter/android/util/v;->c(Lcom/twitter/android/util/v;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    .line 167
    iget-object v3, p0, Lcom/twitter/android/util/v$b;->a:Lcom/twitter/android/util/v;

    invoke-static {v3}, Lcom/twitter/android/util/v;->c(Lcom/twitter/android/util/v;)J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5, v0}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JLaut;)I

    .line 173
    :cond_1
    :goto_0
    invoke-virtual {v0}, Laut;->a()V

    .line 175
    :cond_2
    return-void

    .line 170
    :cond_3
    iget-object v3, p0, Lcom/twitter/android/util/v$b;->a:Lcom/twitter/android/util/v;

    invoke-static {v3}, Lcom/twitter/android/util/v;->b(Lcom/twitter/android/util/v;)J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5, v0}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JLaut;)I

    goto :goto_0
.end method
