.class public Lcom/twitter/android/util/q;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/app/common/base/BaseFragmentActivity;

.field private final b:Lcom/twitter/util/android/f;

.field private final c:Lrx/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/ReplaySubject",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/util/p;

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/util/p;ILcom/twitter/util/android/f;Lcom/twitter/app/common/base/BaseFragmentActivity;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lrx/subjects/ReplaySubject;->r()Lrx/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/util/q;->c:Lrx/subjects/ReplaySubject;

    .line 33
    iput-object p1, p0, Lcom/twitter/android/util/q;->d:Lcom/twitter/android/util/p;

    .line 34
    iput-object p4, p0, Lcom/twitter/android/util/q;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 35
    iput-object p3, p0, Lcom/twitter/android/util/q;->b:Lcom/twitter/util/android/f;

    .line 36
    iput p2, p0, Lcom/twitter/android/util/q;->e:I

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/util/q;)Lrx/subjects/ReplaySubject;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/android/util/q;->c:Lrx/subjects/ReplaySubject;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/util/q;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/q;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/q;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget v1, p0, Lcom/twitter/android/util/q;->e:I

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->i(I)V

    .line 49
    iget-object v0, p0, Lcom/twitter/android/util/q;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget v1, p0, Lcom/twitter/android/util/q;->e:I

    new-instance v2, Lcom/twitter/android/util/q$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/util/q$1;-><init>(Lcom/twitter/android/util/q;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/base/BaseFragmentActivity;->a(ILcom/twitter/app/common/util/o;)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/util/q;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v1, p0, Lcom/twitter/android/util/q;->d:Lcom/twitter/android/util/p;

    iget-object v2, p0, Lcom/twitter/android/util/q;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-interface {v1, v2}, Lcom/twitter/android/util/p;->a(Landroid/app/Activity;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/util/q;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/base/BaseFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 64
    iget-object v0, p0, Lcom/twitter/android/util/q;->c:Lrx/subjects/ReplaySubject;

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/util/q;->b:Lcom/twitter/util/android/f;

    iget-object v1, p0, Lcom/twitter/android/util/q;->d:Lcom/twitter/android/util/p;

    invoke-interface {v1}, Lcom/twitter/android/util/p;->a()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
