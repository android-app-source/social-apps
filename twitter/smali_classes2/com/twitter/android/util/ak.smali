.class public Lcom/twitter/android/util/ak;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/util/ak$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lcom/twitter/android/util/ak$a;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/android/util/ak$a;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/util/ak$a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/provider/t;J)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 50
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 52
    new-instance v0, Lbsk;

    .line 53
    invoke-virtual {p0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    invoke-direct {v0, v1}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    .line 54
    const-wide/16 v2, 0x320

    .line 55
    invoke-virtual {v0, v4, v2, v3}, Lbsk;->a(IJ)V

    .line 56
    const-wide/16 v2, 0x1

    invoke-virtual {v0, v4, v2, v3}, Lbsk;->a(IJ)V

    .line 57
    const-wide/16 v2, 0x2

    invoke-virtual {v0, v4, v2, v3}, Lbsk;->a(IJ)V

    .line 58
    const-wide/16 v2, 0x3

    invoke-virtual {v0, v4, v2, v3}, Lbsk;->a(IJ)V

    .line 59
    const-wide/16 v2, 0x4

    invoke-virtual {v0, v4, v2, v3}, Lbsk;->a(IJ)V

    .line 60
    const-wide/16 v2, 0x5

    invoke-virtual {v0, v4, v2, v3}, Lbsk;->a(IJ)V

    .line 61
    const-wide/16 v2, 0x6

    invoke-virtual {v0, v4, v2, v3}, Lbsk;->a(IJ)V

    .line 63
    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/t;->a(JIJ)V

    .line 64
    return-void
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;)Z
    .locals 1

    .prologue
    .line 26
    if-eqz p0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "vit_unlimited_notifications_verified_users_enabled"

    .line 29
    :goto_0
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 26
    :cond_0
    const-string/jumbo v0, "vit_unlimited_notifications_non_verified_users_enabled"

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;J)Z
    .locals 3

    .prologue
    .line 36
    if-eqz p0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "vit_unlimited_profile_tweets_timeline_enabled"

    .line 37
    :goto_0
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 36
    :goto_1
    return v0

    :cond_0
    const-string/jumbo v0, "vit_unlimited_profile_tweets_timeline_non_verified_users_enabled"

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
