.class Lcom/twitter/android/util/q$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/util/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/util/q;->a()Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/util/q;


# direct methods
.method constructor <init>(Lcom/twitter/android/util/q;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/twitter/android/util/q$1;->a:Lcom/twitter/android/util/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;ILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 52
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    if-eqz p3, :cond_1

    .line 53
    const-string/jumbo v0, "extra_perm_result"

    .line 54
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/android/PermissionResult;

    .line 55
    if-eqz v0, :cond_0

    .line 56
    iget-object v1, p0, Lcom/twitter/android/util/q$1;->a:Lcom/twitter/android/util/q;

    invoke-static {v1}, Lcom/twitter/android/util/q;->a(Lcom/twitter/android/util/q;)Lrx/subjects/ReplaySubject;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/util/android/PermissionResult;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrx/subjects/ReplaySubject;->a(Ljava/lang/Object;)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/util/q$1;->a:Lcom/twitter/android/util/q;

    invoke-static {v0}, Lcom/twitter/android/util/q;->a(Lcom/twitter/android/util/q;)Lrx/subjects/ReplaySubject;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/ReplaySubject;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
