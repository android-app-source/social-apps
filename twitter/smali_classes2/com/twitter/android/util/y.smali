.class public Lcom/twitter/android/util/y;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 152
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/BlockedAccountsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "target_session_owner_id"

    .line 153
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 152
    return-object v0
.end method

.method static synthetic a(Landroid/content/res/Resources;Ljava/lang/String;I)Lcom/twitter/android/widget/PromptDialogFragment;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0, p1, p2}, Lcom/twitter/android/util/y;->b(Landroid/content/res/Resources;Ljava/lang/String;I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "I",
            "Landroid/support/v4/app/FragmentManager;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Lcom/twitter/android/util/y$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/util/y$1;-><init>(Landroid/content/res/Resources;Ljava/lang/String;I)V

    invoke-static {v0, p3}, Lcom/twitter/library/util/ae;->a(Lcta;Landroid/support/v4/app/FragmentManager;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Lcom/twitter/app/common/dialog/b$d;)V

    .line 97
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Lcom/twitter/app/common/dialog/b$d;)V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/twitter/android/util/y;->b(Landroid/content/res/Resources;Ljava/lang/String;I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    .line 103
    if-eqz p4, :cond_0

    .line 104
    invoke-virtual {v0, p4}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 106
    :cond_0
    invoke-virtual {v0, p3}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 107
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 173
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PhoneEntrySettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "account_name"

    .line 174
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "update_phone"

    const/4 v2, 0x1

    .line 175
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "current_phone"

    .line 176
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 177
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 178
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;IILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 65
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 66
    const-string/jumbo v1, "mute_confirm_cnt"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 67
    const/4 v4, 0x3

    if-ge v1, v4, :cond_2

    .line 68
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v4, "mute_confirm_cnt"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 71
    invoke-static {p2}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    const v0, 0x7f0a05a1

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 78
    :goto_0
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, p3}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v4, 0x7f0a05a4

    new-array v5, v2, [Ljava/lang/Object;

    aput-object p1, v5, v3

    .line 79
    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 80
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05a3

    .line 81
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 82
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 83
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 84
    if-eqz p5, :cond_0

    .line 85
    invoke-virtual {v0, p5}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 87
    :cond_0
    invoke-virtual {v0, p4}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 90
    :goto_1
    return v0

    .line 74
    :cond_1
    const v0, 0x7f0a05a2

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    move v0, v3

    .line 90
    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 51
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, p2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09b1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 52
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09b2

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 55
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 56
    if-eqz p4, :cond_0

    .line 57
    invoke-virtual {v0, p4}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 59
    :cond_0
    invoke-virtual {v0, p3}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 60
    return v4
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 45
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Integer;)Z
    .locals 1

    .prologue
    .line 41
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 157
    const-string/jumbo v0, "automated_mute_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/MutedAccountsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "target_session_owner_id"

    .line 159
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    const/16 v1, 0x1a

    .line 162
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 163
    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/users/f;->c(J)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 164
    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/users/f;->a(J)Lcom/twitter/app/users/f;

    move-result-object v0

    const/4 v1, 0x0

    .line 165
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->b(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 166
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->c(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 167
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->a(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 168
    invoke-virtual {v0, p0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/content/res/Resources;Ljava/lang/String;I)Lcom/twitter/android/widget/PromptDialogFragment;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, p2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09d8

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    .line 124
    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09da

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    .line 125
    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00a6

    .line 126
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 127
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 128
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 123
    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/android/util/y;->b(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Lcom/twitter/app/common/dialog/b$d;)V

    .line 134
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Lcom/twitter/app/common/dialog/b$d;)V
    .locals 4

    .prologue
    .line 139
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, p2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0a01

    .line 140
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a03

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 141
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 142
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 143
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 144
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 145
    if-eqz p4, :cond_0

    .line 146
    invoke-virtual {v0, p4}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 148
    :cond_0
    invoke-virtual {v0, p3}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 149
    return-void
.end method
