.class Lcom/twitter/android/util/af$a;
.super Lcok$d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/util/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcok$d;-><init>()V

    .line 42
    const-string/jumbo v0, "timeline_social_proof_contextual_5117"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/util/af$a;->a:Z

    .line 44
    const-string/jumbo v0, "timeline_promoted_content_contextual_5157"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/util/af$a;->b:Z

    .line 46
    const-string/jumbo v0, "timeline_retweet_contextual_5269"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/util/af$a;->c:Z

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/util/af$1;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/android/util/af$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/util/af$a;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/twitter/android/util/af$a;->a:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/util/af$a;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/twitter/android/util/af$a;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/util/af$a;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/twitter/android/util/af$a;->c:Z

    return v0
.end method
