.class Lcom/twitter/android/util/m$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/util/m;->a(Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/android/client/i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/ToolBar;

.field final synthetic b:Lcom/twitter/android/client/i;

.field final synthetic c:Lcom/twitter/android/util/m;


# direct methods
.method constructor <init>(Lcom/twitter/android/util/m;Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/android/client/i;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/twitter/android/util/m$1;->c:Lcom/twitter/android/util/m;

    iput-object p2, p0, Lcom/twitter/android/util/m$1;->a:Lcom/twitter/internal/android/widget/ToolBar;

    iput-object p3, p0, Lcom/twitter/android/util/m$1;->b:Lcom/twitter/android/client/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/android/util/m$1;->a:Lcom/twitter/internal/android/widget/ToolBar;

    const v1, 0x7f1307a5

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lazv;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/twitter/android/util/m$1;->b:Lcom/twitter/android/client/i;

    const-string/jumbo v1, "highlights_tooltip_overflow"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/i;->a(Ljava/lang/String;)V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/m$1;->a:Lcom/twitter/internal/android/widget/ToolBar;

    const v1, 0x7f1307a9

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lazv;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/twitter/android/util/m$1;->b:Lcom/twitter/android/client/i;

    iget-object v1, p0, Lcom/twitter/android/util/m$1;->c:Lcom/twitter/android/util/m;

    .line 192
    invoke-static {v1}, Lcom/twitter/android/util/m;->a(Lcom/twitter/android/util/m;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/android/news/c;->g(J)Ljava/lang/String;

    move-result-object v1

    .line 191
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/i;->a(Ljava/lang/String;)V

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/util/m$1;->a:Lcom/twitter/internal/android/widget/ToolBar;

    const v1, 0x7f1308a6

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lazv;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/twitter/android/util/m$1;->b:Lcom/twitter/android/client/i;

    const-string/jumbo v1, "dm_tooltip"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/i;->a(Ljava/lang/String;)V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/util/m$1;->b:Lcom/twitter/android/client/i;

    const-string/jumbo v1, "guide_tooltip"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/i;->a(Ljava/lang/String;)V

    .line 201
    return-void
.end method
