.class public Lcom/twitter/android/util/v;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/util/v$d;,
        Lcom/twitter/android/util/v$a;,
        Lcom/twitter/android/util/v$c;,
        Lcom/twitter/android/util/v$e;,
        Lcom/twitter/android/util/v$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private d:J

.field private e:Lcom/twitter/android/util/v$b;

.field private f:Lcom/twitter/android/util/v$e;

.field private g:Lcom/twitter/android/util/v$a;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/twitter/android/util/v$c;",
            ">;"
        }
    .end annotation
.end field

.field private i:J


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/util/v$d;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/util/v;->h:Ljava/util/Map;

    .line 49
    iput-object p1, p0, Lcom/twitter/android/util/v;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 50
    invoke-interface {p2}, Lcom/twitter/android/util/v$d;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/util/v;->b:Ljava/lang/String;

    .line 51
    invoke-interface {p2}, Lcom/twitter/android/util/v$d;->o()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/util/v;->c:I

    .line 52
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/v;->a(Lcom/twitter/library/client/Session;)V

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/util/v;)Lcom/twitter/app/common/base/TwitterFragmentActivity;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/util/v;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/util/v;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 80
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/util/v;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 81
    invoke-static {v2}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/android/util/s;->c()Z

    move-result v2

    .line 79
    invoke-static {v0, v1, p1, v2}, Lcom/twitter/library/api/q;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)Lcom/twitter/library/api/q;

    move-result-object v0

    .line 82
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 83
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/util/v;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/twitter/android/util/v;->d:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/android/util/v;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/twitter/android/util/v;->i:J

    return-wide v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/util/v$b;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/util/v;->e:Lcom/twitter/android/util/v$b;

    if-nez v0, :cond_0

    .line 140
    new-instance v0, Lcom/twitter/android/util/v$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/util/v$b;-><init>(Lcom/twitter/android/util/v;)V

    iput-object v0, p0, Lcom/twitter/android/util/v;->e:Lcom/twitter/android/util/v$b;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/v;->e:Lcom/twitter/android/util/v$b;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/twitter/android/util/v;->c:I

    if-ne p1, v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/twitter/android/util/v;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/util/v;->a(Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public a(Lcom/twitter/android/util/v$a;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/twitter/android/util/v;->g:Lcom/twitter/android/util/v$a;

    .line 192
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/util/v;->d:J

    .line 61
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Laiz;)V
    .locals 0

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/twitter/android/util/v;->a(Lcom/twitter/library/client/Session;)V

    .line 76
    return-void
.end method

.method public a(Lcom/twitter/model/timeline/n;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 106
    .line 109
    iget-object v0, p0, Lcom/twitter/android/util/v;->g:Lcom/twitter/android/util/v$a;

    if-eqz v0, :cond_6

    .line 110
    iget-object v0, p0, Lcom/twitter/android/util/v;->g:Lcom/twitter/android/util/v$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/util/v$a;->a(Lcom/twitter/model/timeline/n;)Z

    move-result v0

    .line 113
    :goto_0
    if-nez v0, :cond_0

    .line 114
    if-eqz p1, :cond_5

    .line 115
    const/4 v1, 0x1

    .line 116
    iget-object v0, p0, Lcom/twitter/android/util/v;->h:Ljava/util/Map;

    iget v3, p1, Lcom/twitter/model/timeline/n;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/v$c;

    .line 117
    if-eqz v0, :cond_1

    .line 118
    invoke-interface {v0, p1}, Lcom/twitter/android/util/v$c;->a(Lcom/twitter/model/timeline/n;)V

    move v0, v1

    .line 131
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/util/v;->g:Lcom/twitter/android/util/v$a;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/twitter/android/util/v;->g:Lcom/twitter/android/util/v$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/util/v$a;->b(Lcom/twitter/model/timeline/n;)V

    .line 135
    :cond_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/model/timeline/n;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    invoke-virtual {p0}, Lcom/twitter/android/util/v;->a()Lcom/twitter/android/util/v$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/v$b;->a(Lcom/twitter/model/timeline/n;)V

    move v0, v1

    goto :goto_1

    .line 121
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/model/timeline/n;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 122
    const-string/jumbo v0, "Action prompts are no longer supported! These should not be served.See ANDROID-19459."

    invoke-static {v2, v0}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    move v0, v1

    goto :goto_1

    .line 124
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/model/timeline/n;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 125
    invoke-virtual {p0}, Lcom/twitter/android/util/v;->b()Lcom/twitter/android/util/v$e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/v$e;->a(Lcom/twitter/model/timeline/n;)V

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 127
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method public b()Lcom/twitter/android/util/v$e;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/android/util/v;->f:Lcom/twitter/android/util/v$e;

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Lcom/twitter/android/util/v$e;

    invoke-direct {v0, p0}, Lcom/twitter/android/util/v$e;-><init>(Lcom/twitter/android/util/v;)V

    iput-object v0, p0, Lcom/twitter/android/util/v;->f:Lcom/twitter/android/util/v$e;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/v;->f:Lcom/twitter/android/util/v$e;

    return-object v0
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p2}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 96
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    instance-of v1, p2, Lcom/twitter/library/api/q;

    if-eqz v1, :cond_0

    .line 100
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "prompt"

    sget-object v2, Lcom/twitter/model/timeline/n;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/n;

    .line 101
    invoke-virtual {p0, v0}, Lcom/twitter/android/util/v;->a(Lcom/twitter/model/timeline/n;)V

    goto :goto_0
.end method
