.class public Lcom/twitter/android/HashflagsViewerActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/HashflagsViewerActivity$b;,
        Lcom/twitter/android/HashflagsViewerActivity$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    const v0, 0x7f04013e

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 34
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 35
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 36
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 8

    .prologue
    const/16 v6, 0xc8

    const/4 v1, 0x0

    .line 41
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 44
    invoke-static {}, Lcom/twitter/library/view/b;->b()Ljava/util/List;

    move-result-object v2

    .line 45
    invoke-static {p0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/g;->d()Lcom/twitter/library/media/manager/e;

    move-result-object v3

    .line 47
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/b$b;

    .line 48
    new-instance v5, Lcom/twitter/media/request/a$a;

    iget-object v0, v0, Lcom/twitter/library/view/b$b;->b:Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-static {v6, v6}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 48
    invoke-virtual {v3, v0}, Lcom/twitter/library/media/manager/e;->a(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;

    goto :goto_0

    .line 52
    :cond_0
    const v0, 0x7f1303d0

    invoke-virtual {p0, v0}, Lcom/twitter/android/HashflagsViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/HashflagsViewerActivity;->a:Landroid/widget/ListView;

    .line 53
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    const v0, 0x7f1303cf

    invoke-virtual {p0, v0}, Lcom/twitter/android/HashflagsViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 55
    const-string/jumbo v3, "No hashflags enabled"

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/HashflagsViewerActivity;->a:Landroid/widget/ListView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 59
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Lcom/twitter/android/HashflagsViewerActivity$a;

    .line 61
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/b$b;

    .line 62
    add-int/lit8 v2, v1, 0x1

    new-instance v5, Lcom/twitter/android/HashflagsViewerActivity$a;

    iget-object v6, v0, Lcom/twitter/library/view/b$b;->a:Ljava/lang/String;

    iget-object v7, v0, Lcom/twitter/library/view/b$b;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/library/view/b$b;->a:Ljava/lang/String;

    .line 63
    invoke-static {v0}, Lcom/twitter/library/view/b;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {v5, v6, v7, v0}, Lcom/twitter/android/HashflagsViewerActivity$a;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v1

    move v1, v2

    .line 64
    goto :goto_1

    .line 65
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/HashflagsViewerActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/twitter/android/HashflagsViewerActivity$b;

    invoke-direct {v1, p0, v3}, Lcom/twitter/android/HashflagsViewerActivity$b;-><init>(Landroid/content/Context;[Lcom/twitter/android/HashflagsViewerActivity$a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 66
    return-void
.end method
