.class public Lcom/twitter/android/ProfilePhotoAutoTweetActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;


# instance fields
.field private a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "profile_photo"

    .line 29
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 28
    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 36
    const v0, 0x7f0400da

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 38
    return-object p2
.end method

.method public a()V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->finish()V

    .line 88
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 4

    .prologue
    const v3, 0x7f1302e4

    .line 43
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 45
    const v0, 0x7f05003f

    const v1, 0x7f050040

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->overridePendingTransition(II)V

    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "profile_photo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 48
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 49
    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    iput-object v1, p0, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    .line 51
    iget-object v1, p0, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    if-nez v1, :cond_0

    .line 52
    invoke-static {v0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    .line 53
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a(Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;)V

    .line 54
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    .line 55
    invoke-virtual {v0, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a(Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;)V

    goto :goto_0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 93
    const v0, 0x7f05003c

    const v1, 0x7f05003d

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->overridePendingTransition(II)V

    .line 94
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 64
    packed-switch p1, :pswitch_data_0

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 66
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a()V

    goto :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "profile_tweet_preview"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "cancel"

    aput-object v3, v1, v2

    .line 81
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 80
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 82
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 83
    return-void
.end method
