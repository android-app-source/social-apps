.class Lcom/twitter/android/ck$8;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ck;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/widget/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/core/Tweet;

.field final synthetic b:Lcom/twitter/library/client/Session;

.field final synthetic c:Lcom/twitter/model/util/FriendshipCache;

.field final synthetic d:Lcom/twitter/library/widget/h;

.field final synthetic e:Lcom/twitter/android/ck;


# direct methods
.method constructor <init>(Lcom/twitter/android/ck;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/library/widget/h;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/twitter/android/ck$8;->e:Lcom/twitter/android/ck;

    iput-object p2, p0, Lcom/twitter/android/ck$8;->a:Lcom/twitter/model/core/Tweet;

    iput-object p3, p0, Lcom/twitter/android/ck$8;->b:Lcom/twitter/library/client/Session;

    iput-object p4, p0, Lcom/twitter/android/ck$8;->c:Lcom/twitter/model/util/FriendshipCache;

    iput-object p5, p0, Lcom/twitter/android/ck$8;->d:Lcom/twitter/library/widget/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 483
    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/twitter/android/ck$8;->e:Lcom/twitter/android/ck;

    const-string/jumbo v1, "unblock_dialog"

    const-string/jumbo v2, "unblock"

    iget-object v3, p0, Lcom/twitter/android/ck$8;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 485
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbes;

    iget-object v2, p0, Lcom/twitter/android/ck$8;->e:Lcom/twitter/android/ck;

    iget-object v2, v2, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/ck$8;->b:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/ck$8;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v4, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v6, p0, Lcom/twitter/android/ck$8;->a:Lcom/twitter/model/core/Tweet;

    .line 487
    invoke-virtual {v6}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    const/4 v7, 0x3

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    new-instance v2, Lcom/twitter/android/ck$8$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/ck$8$1;-><init>(Lcom/twitter/android/ck$8;)V

    .line 485
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 497
    iget-object v0, p0, Lcom/twitter/android/ck$8;->d:Lcom/twitter/library/widget/h;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/twitter/android/ck$8;->d:Lcom/twitter/library/widget/h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/h;->d(Z)V

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 501
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ck$8;->e:Lcom/twitter/android/ck;

    const-string/jumbo v1, "unblock_dialog"

    const-string/jumbo v2, "cancel"

    iget-object v3, p0, Lcom/twitter/android/ck$8;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto :goto_0
.end method
