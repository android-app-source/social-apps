.class public Lcom/twitter/android/cb;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const v0, 0x7f13011b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cb;->c:Landroid/widget/TextView;

    .line 42
    const v0, 0x7f1302f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cb;->d:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f130293

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cb;->e:Landroid/widget/TextView;

    .line 44
    return-void
.end method

.method public static a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/ca;F)Landroid/view/View;
    .locals 0

    .prologue
    .line 48
    if-nez p1, :cond_0

    .line 49
    invoke-static {p0, p2, p4}, Lcom/twitter/android/cb;->a(ILandroid/view/ViewGroup;F)Landroid/view/View;

    move-result-object p1

    .line 51
    :cond_0
    invoke-static {p1, p3}, Lcom/twitter/android/cb;->a(Landroid/view/View;Lcom/twitter/android/ca;)V

    .line 52
    return-object p1
.end method

.method public static a(ILandroid/view/ViewGroup;F)Landroid/view/View;
    .locals 4
    .param p0    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 56
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/twitter/android/cb;

    invoke-direct {v1, v0}, Lcom/twitter/android/cb;-><init>(Landroid/view/View;)V

    .line 58
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 59
    const/4 v2, 0x0

    cmpl-float v2, p2, v2

    if-lez v2, :cond_0

    .line 60
    iget-object v1, v1, Lcom/twitter/android/cb;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v3, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 62
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/view/View;Lcom/twitter/android/ca;)V
    .locals 3

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cb;

    .line 67
    iget-object v1, v0, Lcom/twitter/android/cb;->c:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/twitter/android/ca;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v1, v0, Lcom/twitter/android/cb;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 70
    iget v1, p1, Lcom/twitter/android/ca;->a:I

    .line 71
    if-lez v1, :cond_1

    .line 72
    iget-object v2, v0, Lcom/twitter/android/cb;->e:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, v0, Lcom/twitter/android/cb;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, v0, Lcom/twitter/android/cb;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
