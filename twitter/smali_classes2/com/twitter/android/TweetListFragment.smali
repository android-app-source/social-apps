.class public abstract Lcom/twitter/android/TweetListFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/TweetListFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A:",
        "Lcjr",
        "<TT;>;>",
        "Lcom/twitter/android/widget/ScrollingHeaderListFragment",
        "<TT;TA;>;",
        "Lcom/twitter/android/av",
        "<",
        "Landroid/view/View;",
        "Lcom/twitter/model/core/Tweet;",
        ">;"
    }
.end annotation


# instance fields
.field protected G:Lcom/twitter/model/core/Tweet;

.field protected H:Lcom/twitter/android/ck;

.field protected I:Lcom/twitter/android/cl;

.field protected J:I

.field protected K:Z

.field private a:Lwc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->H:Lcom/twitter/android/ck;

    .line 56
    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->I:Lcom/twitter/android/cl;

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/TweetListFragment;->J:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetListFragment;)V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->at()V

    return-void
.end method


# virtual methods
.method protected H_()V
    .locals 4

    .prologue
    .line 156
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->H_()V

    .line 158
    new-instance v0, Lrp;

    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lrp;-><init>(J)V

    .line 160
    new-instance v1, Lcom/twitter/android/TweetListFragment$a;

    invoke-direct {v1, p0}, Lcom/twitter/android/TweetListFragment$a;-><init>(Lcom/twitter/android/TweetListFragment;)V

    invoke-virtual {v0, v1}, Lrp;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 161
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 162
    return-void
.end method

.method protected O()Lcom/twitter/android/ck;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/twitter/android/ck;

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method protected P()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method protected Q()Lwc;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->a:Lwc;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 87
    return-object v0
.end method

.method public a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 33
    check-cast p2, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcno;I)V
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 146
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Lcno;I)V

    .line 147
    invoke-virtual {p0, p2}, Lcom/twitter/android/TweetListFragment;->h(I)V

    .line 149
    if-eq p2, v0, :cond_0

    if-nez p2, :cond_1

    .line 150
    :cond_0
    if-ne p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetListFragment;->e(Z)V

    .line 152
    :cond_1
    return-void

    .line 150
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lwc;)V
    .locals 1

    .prologue
    .line 184
    iput-object p1, p0, Lcom/twitter/android/TweetListFragment;->a:Lwc;

    .line 185
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->H:Lcom/twitter/android/ck;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->H:Lcom/twitter/android/ck;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ck;->a(Lwc;)V

    .line 188
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    if-eqz p1, :cond_0

    .line 77
    const-string/jumbo v0, "state_delete_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->G:Lcom/twitter/model/core/Tweet;

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "en_act"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetListFragment;->K:Z

    .line 81
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->G:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 67
    const-string/jumbo v0, "state_delete_key"

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->G:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 92
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v7

    .line 96
    iget-boolean v0, p0, Lcom/twitter/android/TweetListFragment;->U:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/TweetListFragment;->K:Z

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->H:Lcom/twitter/android/ck;

    if-nez v0, :cond_1

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->O()Lcom/twitter/android/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->H:Lcom/twitter/android/ck;

    .line 100
    :cond_1
    new-instance v0, Lcom/twitter/android/cl;

    iget-object v2, p0, Lcom/twitter/android/TweetListFragment;->H:Lcom/twitter/android/ck;

    iget-object v3, v7, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 101
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->P()Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/cl;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;Lbxb;Landroid/widget/ListView;IZZ)V

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->I:Lcom/twitter/android/cl;

    .line 103
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->I:Lcom/twitter/android/cl;

    invoke-virtual {v7, v0}, Lcom/twitter/app/common/list/l;->a(Landroid/view/View$OnTouchListener;)V

    .line 106
    :cond_2
    iget-object v0, v7, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    instance-of v0, v0, Lcom/twitter/refresh/widget/RefreshableListView;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "timeline:list_layout_duration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/TweetListFragment;->J:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "timeline:list_layout_count:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/TweetListFragment;->J:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    sget-object v2, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 110
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v3

    const/4 v4, 0x1

    .line 109
    invoke-static {v1, v2, v3, v4, v8}, Lcom/twitter/metrics/a;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/a;

    move-result-object v6

    .line 111
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    .line 112
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    move v5, v8

    .line 111
    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;I)Lcom/twitter/metrics/c;

    move-result-object v1

    .line 114
    invoke-virtual {v6}, Lcom/twitter/metrics/a;->i()V

    .line 115
    invoke-virtual {v1}, Lcom/twitter/metrics/c;->i()V

    .line 116
    iget-object v0, v7, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    new-instance v2, Lcom/twitter/android/timeline/o;

    invoke-direct {v2, v6, v1}, Lcom/twitter/android/timeline/o;-><init>(Lcom/twitter/metrics/a;Lcom/twitter/metrics/c;)V

    invoke-virtual {v0, v2}, Lcom/twitter/refresh/widget/RefreshableListView;->setViewLayoutListener(Lcom/twitter/refresh/widget/b;)V

    .line 119
    :cond_3
    return-void
.end method
