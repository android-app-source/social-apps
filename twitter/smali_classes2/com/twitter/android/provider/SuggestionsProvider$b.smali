.class public Lcom/twitter/android/provider/SuggestionsProvider$b;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/provider/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/android/provider/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1378
    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/android/provider/c;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1382
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1383
    new-instance v1, Lcom/twitter/android/provider/c$a;

    invoke-direct {v1}, Lcom/twitter/android/provider/c$a;-><init>()V

    .line 1384
    invoke-virtual {v1, v0}, Lcom/twitter/android/provider/c$a;->a(Ljava/lang/String;)Lcom/twitter/android/provider/c$a;

    move-result-object v1

    const/4 v2, 0x0

    .line 1385
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/provider/c$a;->a(J)Lcom/twitter/android/provider/c$a;

    move-result-object v1

    .line 1387
    const-string/jumbo v2, "hashflags_in_composer_android_enabled"

    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1388
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1389
    invoke-static {v2}, Lcom/twitter/library/view/b;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1390
    new-instance v3, Lcom/twitter/library/view/b;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v3, v2, v0}, Lcom/twitter/library/view/b;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/provider/c$a;->a(Lcom/twitter/library/view/b;)Lcom/twitter/android/provider/c$a;

    .line 1394
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/android/provider/c$a;->a()Lcom/twitter/android/provider/c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1378
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/provider/SuggestionsProvider$b;->a(Landroid/database/Cursor;)Lcom/twitter/android/provider/c;

    move-result-object v0

    return-object v0
.end method
