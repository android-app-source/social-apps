.class Lcom/twitter/android/provider/d$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpv;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/provider/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpv",
        "<",
        "Lcom/twitter/library/provider/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/database/dm/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/provider/d$b;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/twitter/android/provider/d$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/database/dm/c;",
            ">;",
            "Lcom/twitter/android/provider/d$b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iput-object p1, p0, Lcom/twitter/android/provider/d$a;->a:Ljava/util/Map;

    .line 165
    iput-object p2, p0, Lcom/twitter/android/provider/d$a;->b:Lcom/twitter/android/provider/d$b;

    .line 166
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/provider/c;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 170
    if-nez p1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v0

    .line 174
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/provider/d$a;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/library/provider/c;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 178
    invoke-virtual {p1}, Lcom/twitter/library/provider/c;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 179
    invoke-virtual {p1}, Lcom/twitter/library/provider/c;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    :cond_2
    move v0, v1

    .line 198
    goto :goto_0

    .line 184
    :cond_3
    check-cast p1, Lcom/twitter/library/provider/d;

    .line 185
    iget-object v2, p1, Lcom/twitter/library/provider/d;->b:Lcom/twitter/model/core/TwitterUser;

    .line 186
    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v3, p0, Lcom/twitter/android/provider/d$a;->b:Lcom/twitter/android/provider/d$b;

    iget-wide v6, v3, Lcom/twitter/android/provider/d$b;->b:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_4

    iget-object v3, p1, Lcom/twitter/library/provider/d;->d:Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    sget-object v4, Lcom/twitter/library/database/dm/ShareHistoryTable$Type;->a:Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    if-ne v3, v4, :cond_0

    .line 189
    :cond_4
    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v3, p0, Lcom/twitter/android/provider/d$a;->b:Lcom/twitter/android/provider/d$b;

    iget-wide v6, v3, Lcom/twitter/android/provider/d$b;->c:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 193
    invoke-static {v2}, Lcom/twitter/library/dm/e;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 157
    check-cast p1, Lcom/twitter/library/provider/c;

    invoke-virtual {p0, p1}, Lcom/twitter/android/provider/d$a;->a(Lcom/twitter/library/provider/c;)Z

    move-result v0

    return v0
.end method
