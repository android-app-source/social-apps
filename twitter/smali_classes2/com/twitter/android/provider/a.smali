.class public Lcom/twitter/android/provider/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lna;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/provider/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lna",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lna;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lna",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/provider/d;

.field private final c:Lcom/twitter/android/provider/d$b;

.field private final d:Lcom/twitter/android/provider/a$a;


# direct methods
.method public constructor <init>(Lna;Lcom/twitter/android/provider/d;Lcom/twitter/android/provider/d$b;Lcom/twitter/android/provider/a$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lna",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/twitter/android/provider/d;",
            "Lcom/twitter/android/provider/d$b;",
            "Lcom/twitter/android/provider/a$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/twitter/android/provider/a;->a:Lna;

    .line 51
    iput-object p2, p0, Lcom/twitter/android/provider/a;->b:Lcom/twitter/android/provider/d;

    .line 52
    iput-object p3, p0, Lcom/twitter/android/provider/a;->c:Lcom/twitter/android/provider/d$b;

    .line 53
    iput-object p4, p0, Lcom/twitter/android/provider/a;->d:Lcom/twitter/android/provider/a$a;

    .line 54
    return-void
.end method

.method static a(Lcbi;)Lcbi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<*>;)",
            "Lcbi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Lcbl;

    invoke-static {p0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/provider/a$3;

    invoke-direct {v2}, Lcom/twitter/android/provider/a$3;-><init>()V

    invoke-static {v1, v2}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method static a(Lcbi;Lcbi;)Lcbi;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<*>;",
            "Lcbi",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)",
            "Lcbi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {p1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 93
    new-instance v1, Lcbl;

    invoke-static {p0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/provider/a$2;

    invoke-direct {v3, v0}, Lcom/twitter/android/provider/a$2;-><init>(Ljava/util/Set;)V

    invoke-static {v2, v3}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/provider/a;)Lcom/twitter/android/provider/a$a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/provider/a;->d:Lcom/twitter/android/provider/a$a;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Lcom/twitter/library/provider/c;
    .locals 1

    .prologue
    .line 30
    invoke-static {p0}, Lcom/twitter/android/provider/a;->b(Ljava/lang/Object;)Lcom/twitter/library/provider/c;

    move-result-object v0

    return-object v0
.end method

.method private a(Lna$a;)Lna$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lna$a",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lna$a",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/android/provider/a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/provider/a$1;-><init>(Lcom/twitter/android/provider/a;Lna$a;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/provider/a;)Lcom/twitter/android/provider/d$b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/provider/a;->c:Lcom/twitter/android/provider/d$b;

    return-object v0
.end method

.method private static b(Ljava/lang/Object;)Lcom/twitter/library/provider/c;
    .locals 1

    .prologue
    .line 121
    instance-of v0, p0, Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    .line 122
    new-instance v0, Lcom/twitter/library/provider/d$a;

    invoke-direct {v0}, Lcom/twitter/library/provider/d$a;-><init>()V

    check-cast p0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, p0}, Lcom/twitter/library/provider/d$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/provider/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 126
    :goto_0
    return-object v0

    .line 123
    :cond_0
    instance-of v0, p0, Lcom/twitter/model/dms/q;

    if-eqz v0, :cond_1

    .line 124
    new-instance v0, Lcom/twitter/library/provider/b$a;

    invoke-direct {v0}, Lcom/twitter/library/provider/b$a;-><init>()V

    check-cast p0, Lcom/twitter/model/dms/q;

    invoke-virtual {v0, p0}, Lcom/twitter/library/provider/b$a;->a(Lcom/twitter/model/dms/q;)Lcom/twitter/library/provider/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    goto :goto_0

    .line 126
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static b(Lcbi;)Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p0}, Lcbi;->be_()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 135
    invoke-virtual {p0}, Lcbi;->i()Ljava/util/ListIterator;

    move-result-object v2

    .line 136
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 138
    instance-of v3, v0, Lcom/twitter/library/provider/d;

    if-eqz v3, :cond_1

    .line 139
    check-cast v0, Lcom/twitter/library/provider/d;

    iget-object v0, v0, Lcom/twitter/library/provider/d;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 140
    :cond_1
    instance-of v3, v0, Lcom/twitter/library/provider/b;

    if-eqz v3, :cond_0

    .line 141
    check-cast v0, Lcom/twitter/library/provider/b;

    iget-object v0, v0, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 145
    :cond_2
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/provider/a;)Lcom/twitter/android/provider/d;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/provider/a;->b:Lcom/twitter/android/provider/d;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lna$a;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;Lna$a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lna$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lna$a",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/provider/a;->a:Lna;

    invoke-direct {p0, p2}, Lcom/twitter/android/provider/a;->a(Lna$a;)Lna$a;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lna;->a(Ljava/lang/Object;Lna$a;)V

    .line 59
    return-void
.end method
