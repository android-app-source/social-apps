.class public Lcom/twitter/android/provider/SuggestionsProvider$k;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/provider/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/android/provider/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1352
    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/android/provider/f;
    .locals 4

    .prologue
    .line 1356
    new-instance v0, Lcom/twitter/android/provider/f$a;

    invoke-direct {v0}, Lcom/twitter/android/provider/f$a;-><init>()V

    const/4 v1, 0x3

    .line 1357
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/f$a;->b(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;

    move-result-object v0

    const/4 v1, 0x2

    .line 1358
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/f$a;->a(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;

    move-result-object v0

    const/4 v1, 0x5

    .line 1359
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/f$a;->a(I)Lcom/twitter/android/provider/f$a;

    move-result-object v0

    const/4 v1, 0x4

    .line 1360
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/f$a;->c(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 1361
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/provider/f$a;->a(J)Lcom/twitter/android/provider/f$a;

    move-result-object v0

    const/4 v1, 0x6

    .line 1362
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/f$a;->b(I)Lcom/twitter/android/provider/f$a;

    move-result-object v0

    const/4 v1, 0x7

    .line 1363
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/f$a;->d(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;

    move-result-object v0

    .line 1364
    invoke-virtual {v0}, Lcom/twitter/android/provider/f$a;->e()Lcom/twitter/android/provider/f;

    move-result-object v0

    .line 1356
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1352
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/provider/SuggestionsProvider$k;->a(Landroid/database/Cursor;)Lcom/twitter/android/provider/f;

    move-result-object v0

    return-object v0
.end method
