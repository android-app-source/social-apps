.class public Lcom/twitter/android/provider/SuggestionsProvider;
.super Landroid/content/ContentProvider;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/provider/SuggestionsProvider$f;,
        Lcom/twitter/android/provider/SuggestionsProvider$a;,
        Lcom/twitter/android/provider/SuggestionsProvider$h;,
        Lcom/twitter/android/provider/SuggestionsProvider$j;,
        Lcom/twitter/android/provider/SuggestionsProvider$i;,
        Lcom/twitter/android/provider/SuggestionsProvider$g;,
        Lcom/twitter/android/provider/SuggestionsProvider$d;,
        Lcom/twitter/android/provider/SuggestionsProvider$e;,
        Lcom/twitter/android/provider/SuggestionsProvider$b;,
        Lcom/twitter/android/provider/SuggestionsProvider$c;,
        Lcom/twitter/android/provider/SuggestionsProvider$k;,
        Lcom/twitter/android/provider/SuggestionsProvider$l;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field public static final i:Ljava/util/regex/Pattern;

.field public static final j:Ljava/util/regex/Pattern;

.field private static final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/api/search/TwitterTypeAheadGroup;",
            ">;"
        }
    .end annotation
.end field

.field private static final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final o:Landroid/content/UriMatcher;

.field private static final p:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v3, 0x1

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".provider.SuggestionsProvider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->b:Ljava/lang/String;

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "compose_users"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->c:Landroid/net/Uri;

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "hashtags"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->d:Landroid/net/Uri;

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "dmableusers"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->e:Landroid/net/Uri;

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "locations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->f:Landroid/net/Uri;

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "users"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->g:Landroid/net/Uri;

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "search_suggest_query"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->h:Landroid/net/Uri;

    .line 140
    const-string/jumbo v0, "[\\w ]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->i:Ljava/util/regex/Pattern;

    .line 145
    const-string/jumbo v0, "\\A@?#?\\w+\\z"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->j:Ljava/util/regex/Pattern;

    .line 178
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const-string/jumbo v1, "s_type"

    aput-object v1, v0, v3

    const-string/jumbo v1, "suggest_text_1"

    aput-object v1, v0, v4

    const-string/jumbo v1, "suggest_intent_query"

    aput-object v1, v0, v5

    const-string/jumbo v1, "suggest_intent_action"

    aput-object v1, v0, v6

    const-string/jumbo v1, "suggest_text_2"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string/jumbo v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "filter_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "filter_location"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "filter_follow"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "user_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "image_url"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "user_verified"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "customer_service_state"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "friendship"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "soc_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->p:[Ljava/lang/String;

    .line 199
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->k:Ljava/util/Map;

    .line 200
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->l:Ljava/util/Map;

    .line 201
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->m:Ljava/util/Map;

    .line 203
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    .line 204
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "search_suggest_query"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 205
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "search_suggest_query/*"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 206
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "search_suggest_shortcut"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 207
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "search_suggest_shortcut/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 208
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "compose_users"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 209
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "compose_users/*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 210
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "hashtags"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 211
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "hashtags/*"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 212
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "locations"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 213
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "dmableusers"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 214
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "dmableusers/*"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 215
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->a:Ljava/lang/String;

    const-string/jumbo v2, "users"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 217
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v6}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->n:Ljava/util/Map;

    .line 218
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->n:Ljava/util/Map;

    const-string/jumbo v1, "news"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->n:Ljava/util/Map;

    const-string/jumbo v1, "users"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->n:Ljava/util/Map;

    const-string/jumbo v1, "images"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->n:Ljava/util/Map;

    const-string/jumbo v1, "videos"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/database/MatrixCursor;ILcom/twitter/android/provider/SuggestionsProvider$f;)I
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 976
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 977
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 978
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 979
    const v1, 0x7f0a07d6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p3, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 980
    iget-object v1, p3, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 981
    const-string/jumbo v1, "com.twitter.android.action.USER_SHOW"

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 982
    add-int/lit8 v0, p2, 0x1

    return v0
.end method

.method private static a(Landroid/content/Context;Landroid/database/MatrixCursor;ILcom/twitter/android/provider/SuggestionsProvider$f;Ljava/util/Set;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/MatrixCursor;",
            "I",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 996
    invoke-interface {p4, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 997
    invoke-interface {p4, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 998
    iget-object v0, p3, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 999
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    .line 1000
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1001
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1002
    const v2, 0x7f0a07d4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1003
    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1004
    const-string/jumbo v0, "com.twitter.android.action.SEARCH"

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1005
    add-int/lit8 p2, p2, 0x1

    .line 1007
    :cond_0
    return p2
.end method

.method private static a(Landroid/database/MatrixCursor;ILandroid/database/Cursor;Ljava/util/Set;I)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/MatrixCursor;",
            "I",
            "Landroid/database/Cursor;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 1022
    invoke-interface {p2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p4, :cond_1

    .line 1060
    :cond_0
    :goto_0
    return p1

    :cond_1
    move v0, v1

    .line 1028
    :cond_2
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1029
    if-eqz v2, :cond_5

    .line 1052
    :cond_3
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1054
    :cond_4
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1055
    if-nez v0, :cond_0

    .line 1058
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    .line 1033
    :cond_5
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1034
    new-instance v3, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v3, v2}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 1035
    invoke-interface {p3, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1036
    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1037
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 1038
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1039
    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1040
    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1041
    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1042
    const-string/jumbo v2, "com.twitter.android.action.SEARCH_RECENT"

    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1043
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1044
    sget-object v2, Lcom/twitter/database/schema/a$s;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 1045
    const-string/jumbo v4, "type"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1046
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1047
    add-int/lit8 p1, p1, 0x1

    .line 1048
    add-int/lit8 v0, v0, 0x1

    .line 1050
    :cond_6
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    if-lt v0, p4, :cond_2

    goto :goto_1
.end method

.method private static a(Landroid/database/MatrixCursor;ILjava/util/List;Ljava/util/Set;I)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/MatrixCursor;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 1133
    .line 1134
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAhead;

    .line 1135
    if-lt v2, p4, :cond_1

    .line 1167
    :cond_0
    return p1

    .line 1140
    :cond_1
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->n:Ljava/util/Map;

    iget-object v5, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/model/search/b;

    iget-object v5, v5, Lcom/twitter/model/search/b;->m:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v3

    .line 1146
    :goto_1
    new-instance v5, Lcom/twitter/android/provider/SuggestionsProvider$f;

    iget-object v6, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/model/search/b;

    iget-object v6, v6, Lcom/twitter/model/search/b;->c:Ljava/lang/String;

    invoke-direct {v5, v6, v1}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;I)V

    .line 1147
    invoke-interface {p3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1148
    invoke-interface {p3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1149
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    .line 1150
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1151
    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1153
    iget-object v6, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/model/search/b;

    iget-object v6, v6, Lcom/twitter/model/search/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1154
    iget-object v6, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/model/search/b;

    iget-object v6, v6, Lcom/twitter/model/search/b;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1155
    const-string/jumbo v6, "com.twitter.android.action.SEARCH_TYPEAHEAD_TOPIC"

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1156
    invoke-virtual {v5, v9}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1157
    invoke-virtual {v5, v9}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1158
    const-wide/16 v6, -0x1

    iget-object v0, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/model/search/b;

    iget-object v0, v0, Lcom/twitter/model/search/b;->c:Ljava/lang/String;

    const/16 v8, 0xc

    invoke-static {v6, v7, v0, v8, p1}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 1160
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1161
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1163
    add-int/lit8 p1, p1, 0x1

    .line 1164
    add-int/lit8 v0, v2, 0x1

    :goto_2
    move v2, v0

    .line 1166
    goto :goto_0

    .line 1143
    :cond_2
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->n:Ljava/util/Map;

    iget-object v5, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/model/search/b;

    iget-object v5, v5, Lcom/twitter/model/search/b;->m:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;Ljava/util/Set;IZ)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/database/MatrixCursor;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;IZ)I"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1272
    const-string/jumbo v1, "search_queries"

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider$j;->a:[Ljava/lang/String;

    const-string/jumbo v3, "cluster_titles NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1275
    if-eqz v3, :cond_3

    move v1, v8

    move v2, p2

    .line 1278
    :goto_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    if-ge v1, p5, :cond_1

    .line 1279
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1280
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1281
    sget-object v6, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 1282
    invoke-static {v6}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v6

    .line 1281
    invoke-static {v0, v6}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1284
    if-eqz v0, :cond_0

    .line 1285
    invoke-static {p3, v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 1289
    :goto_1
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1290
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v6

    .line 1291
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1292
    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1293
    invoke-virtual {v6, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1294
    invoke-virtual {v6, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1295
    const-string/jumbo v7, "com.twitter.android.action.SEARCH_TREND"

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1296
    invoke-virtual {v6, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1298
    add-int/lit8 v2, v2, 0x1

    .line 1299
    new-instance v0, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v0, v5}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1300
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    :goto_2
    move v2, v1

    move v1, v0

    .line 1302
    goto :goto_0

    :cond_0
    move-object v0, v4

    .line 1287
    goto :goto_1

    .line 1304
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1307
    :goto_3
    return v2

    .line 1304
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_2

    :cond_3
    move v2, p2

    goto :goto_3
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;I)I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/database/MatrixCursor;",
            "I",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 922
    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->size()I

    move-result v2

    move/from16 v0, p7

    if-ge v2, v0, :cond_3

    .line 923
    const/4 v3, 0x1

    const-string/jumbo v4, "tokens_user_view"

    sget-object v5, Lcom/twitter/android/provider/SuggestionsProvider$i;->a:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v2, 0x64

    .line 925
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    move-object v2, p0

    move-object v6, p3

    move-object/from16 v7, p4

    move-object/from16 v10, p5

    .line 923
    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 926
    if-eqz v2, :cond_3

    .line 927
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->size()I

    move-result v3

    move/from16 v0, p7

    if-ge v3, v0, :cond_2

    .line 928
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 929
    new-instance v4, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v4, v3}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 930
    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 931
    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 932
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v4

    .line 933
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 934
    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 935
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 936
    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 937
    const/4 v5, 0x2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 938
    const/4 v6, 0x1

    if-ne v6, v5, :cond_1

    .line 939
    const-string/jumbo v5, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 943
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x40

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 944
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 945
    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 946
    const/4 v3, 0x0

    const/4 v5, 0x3

    invoke-static {v6, v7, v3, v5, p2}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v3

    .line 948
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 949
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 950
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 951
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 952
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 953
    const/4 v3, 0x4

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 954
    const/4 v3, 0x5

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 955
    const/16 v3, 0x8

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 956
    const/4 v3, 0x6

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 957
    const/4 v3, 0x7

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 958
    add-int/lit8 p2, p2, 0x1

    goto/16 :goto_0

    .line 941
    :cond_1
    const-string/jumbo v5, "com.twitter.android.action.USER_SHOW_SEARCH_SUGGESTION"

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_1

    .line 961
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 964
    :cond_3
    return p2
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/twitter/library/provider/ParcelableMatrixCursor;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Landroid/database/Cursor;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/twitter/library/provider/ParcelableMatrixCursor;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 581
    const-string/jumbo v1, "locations"

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider$d;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p1

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 583
    if-eqz v0, :cond_3

    .line 584
    invoke-virtual {p0}, Lcom/twitter/android/provider/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 585
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 586
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 587
    new-instance v3, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v3, v2}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 588
    invoke-interface {p6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 589
    invoke-interface {p6, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 590
    invoke-virtual {p2}, Lcom/twitter/library/provider/ParcelableMatrixCursor;->a()Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    move-result-object v3

    .line 591
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 592
    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 593
    invoke-virtual {v3, v2}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 594
    const/4 v4, 0x3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 595
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 596
    const v5, 0x7f0a0974

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    aput-object v4, v6, v2

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    goto :goto_0

    .line 598
    :cond_1
    invoke-virtual {v3, v2}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    goto :goto_0

    .line 602
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 604
    :cond_3
    return-object p2
.end method

.method private a(Ljava/lang/String;IZ)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/16 v8, 0x29

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 459
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 460
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 461
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x40

    if-eq v3, v4, :cond_0

    move v0, v1

    .line 462
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/twitter/library/util/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x25

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 464
    invoke-static {v3}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 465
    const-string/jumbo v6, "(users_username LIKE "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    if-eqz v0, :cond_1

    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "% "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 468
    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 469
    const-string/jumbo v3, " OR users_name LIKE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " OR "

    .line 470
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "users_name"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " LIKE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 472
    :cond_1
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 474
    :cond_2
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 475
    if-eqz p2, :cond_5

    .line 476
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_3

    .line 477
    const-string/jumbo v0, " AND "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    :cond_3
    const-string/jumbo v0, "(users_friendship&"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x3d

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 480
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 481
    if-eqz p3, :cond_4

    .line 482
    const-string/jumbo v0, " OR users_user_id = "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 484
    :cond_4
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 487
    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 488
    :goto_0
    invoke-static {v6, v7}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string/jumbo v2, "tokens_user_view"

    sget-object v3, Lcom/twitter/android/provider/SuggestionsProvider$i;->a:[Ljava/lang/String;

    const-string/jumbo v8, "tokens_weight DESC, LOWER(users_username) ASC"

    move-object v6, v5

    move-object v7, v5

    move-object v9, v5

    .line 489
    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 488
    return-object v0

    :cond_6
    move-object v4, v5

    .line 487
    goto :goto_0
.end method

.method private a(Ljava/lang/String;IZZZLjava/util/List;Lcpv;)Landroid/database/Cursor;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZZZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcpv",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 337
    new-instance v5, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/twitter/android/provider/SuggestionsProvider$l;->a:[Ljava/lang/String;

    invoke-direct {v5, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 339
    invoke-static {}, Lcom/twitter/android/client/w;->g()I

    move-result v15

    .line 340
    if-eqz p6, :cond_5

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 341
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-direct {v0, v5, v1, v15}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;Ljava/util/List;I)Ljava/util/Set;

    move-result-object v4

    move-object v14, v4

    .line 345
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v16

    .line 346
    if-eqz v16, :cond_2

    .line 347
    :cond_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 348
    const/4 v4, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 349
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 350
    const/4 v4, 0x1

    .line 351
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x0

    .line 352
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x4

    .line 353
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v4, 0x5

    .line 354
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v4, 0x6

    .line 355
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/16 v4, 0x8

    .line 356
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v4, p0

    .line 350
    invoke-direct/range {v4 .. v13}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 357
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 359
    invoke-virtual {v5}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-lt v4, v15, :cond_0

    .line 364
    :cond_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 367
    :cond_2
    if-eqz p3, :cond_3

    .line 368
    invoke-static/range {p1 .. p1}, Lcom/twitter/library/util/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v4, p0

    move v7, v15

    move-object v8, v14

    move-object/from16 v9, p7

    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;Ljava/lang/String;ILjava/util/Set;Lcpv;)V

    .line 371
    :cond_3
    if-eqz p5, :cond_4

    .line 372
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v1}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;Ljava/lang/String;)V

    .line 374
    :cond_4
    return-object v5

    .line 343
    :cond_5
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move-object v14, v4

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;
    .locals 3

    .prologue
    .line 659
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->k:Ljava/util/Map;

    monitor-enter v1

    .line 660
    :try_start_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->k:Ljava/util/Map;

    invoke-static {p0}, Lcom/twitter/android/provider/SuggestionsProvider;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    monitor-exit v1

    return-object v0

    .line 661
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Landroid/database/MatrixCursor;Ljava/util/List;I)Ljava/util/Set;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/MatrixCursor;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 379
    invoke-virtual/range {p1 .. p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    move/from16 v0, p3

    if-ge v2, v0, :cond_3

    .line 380
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_id IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    .line 381
    move-object/from16 v0, p2

    invoke-static {v3, v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "friendship"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " <> 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 383
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 384
    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 385
    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string/jumbo v3, "users"

    sget-object v4, Lcom/twitter/android/provider/SuggestionsProvider$l;->a:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 387
    if-eqz v14, :cond_3

    .line 388
    new-instance v15, Ljava/util/HashMap;

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v15, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 389
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 391
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v14}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v15, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 393
    :cond_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Ljava/lang/Long;

    .line 394
    invoke-interface {v15, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 395
    if-eqz v2, :cond_1

    .line 396
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 397
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v2, 0x2

    .line 398
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x3

    .line 399
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x4

    .line 400
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x5

    .line 401
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/4 v2, 0x6

    .line 402
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v2, 0x7

    .line 403
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 397
    invoke-direct/range {v2 .. v11}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 404
    invoke-interface {v13, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 406
    invoke-virtual/range {p1 .. p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    move/from16 v0, p3

    if-lt v2, v0, :cond_1

    .line 411
    :cond_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 414
    :cond_3
    return-object v13
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 665
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->k:Ljava/util/Map;

    monitor-enter v1

    .line 666
    :try_start_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 667
    monitor-exit v1

    .line 668
    return-void

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Landroid/database/MatrixCursor;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 2

    .prologue
    .line 446
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 447
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 448
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 449
    invoke-virtual {v0, p4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 450
    invoke-virtual {v0, p5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 451
    invoke-virtual {v0, p6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 452
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 453
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 454
    invoke-static {p9}, Lcom/twitter/model/businessprofiles/h$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 455
    return-void
.end method

.method private a(Landroid/database/MatrixCursor;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 494
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/model/util/g;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result v0

    .line 498
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    .line 499
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 500
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 501
    invoke-static {p2}, Lcom/twitter/library/util/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 502
    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 503
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 504
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 505
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 506
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 508
    :cond_0
    return-void
.end method

.method private a(Landroid/database/MatrixCursor;Ljava/lang/String;ILjava/util/Set;Lcpv;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/MatrixCursor;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcpv",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 419
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    move/from16 v0, p3

    if-ge v2, v0, :cond_1

    .line 420
    invoke-static {p2}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 421
    if-eqz v2, :cond_1

    .line 422
    if-nez p5, :cond_2

    .line 424
    :goto_0
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/search/TwitterTypeAhead;

    .line 425
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result v3

    move/from16 v0, p3

    if-lt v3, v0, :cond_3

    .line 442
    :cond_1
    return-void

    .line 423
    :cond_2
    move-object/from16 v0, p5

    invoke-static {v2, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v2

    goto :goto_0

    .line 429
    :cond_3
    iget-object v3, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v3, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 430
    iget-object v3, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v3, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v3, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v6, v3, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-object v3, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v7, v3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iget-object v3, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v8, v3, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    iget-object v3, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v3, v3, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v3, :cond_4

    const/4 v9, 0x2

    :goto_2
    iget-object v3, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget v10, v3, Lcom/twitter/model/core/TwitterUser;->U:I

    iget-object v2, v2, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v11, v2, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v2 .. v11}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1

    :cond_4
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 3

    .prologue
    .line 653
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->k:Ljava/util/Map;

    monitor-enter v1

    .line 654
    :try_start_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->k:Ljava/util/Map;

    invoke-static {p0}, Lcom/twitter/android/provider/SuggestionsProvider;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 655
    monitor-exit v1

    .line 656
    return-void

    .line 655
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 672
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->l:Ljava/util/Map;

    monitor-enter v1

    .line 673
    :try_start_0
    const-string/jumbo v0, "@"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    :goto_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->l:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 675
    monitor-exit v1

    .line 676
    return-void

    .line 673
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 675
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Landroid/database/MatrixCursor;ILandroid/database/Cursor;Ljava/util/Set;I)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/MatrixCursor;",
            "I",
            "Landroid/database/Cursor;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v7, 0x6

    .line 1075
    invoke-interface {p2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p4, :cond_1

    .line 1117
    :cond_0
    :goto_0
    return p1

    :cond_1
    move v0, v1

    .line 1081
    :cond_2
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1082
    if-eq v2, v7, :cond_5

    .line 1109
    :cond_3
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1111
    :cond_4
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1112
    if-ne v0, v7, :cond_0

    .line 1115
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    .line 1086
    :cond_5
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1087
    new-instance v3, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v3, v2}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 1088
    invoke-interface {p3, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1089
    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1090
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 1091
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1092
    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1093
    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1094
    const/4 v4, 0x1

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1095
    const-string/jumbo v4, "com.twitter.android.action.SEARCH_QUERY_SAVED"

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1096
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1097
    sget-object v4, Lcom/twitter/database/schema/a$s;->a:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 1098
    const-string/jumbo v5, "type"

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1099
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1100
    const/4 v4, 0x3

    .line 1101
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/16 v6, 0xd

    .line 1100
    invoke-static {v4, v5, v2, v6, p1}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v2

    .line 1103
    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1104
    add-int/lit8 p1, p1, 0x1

    .line 1105
    add-int/lit8 v0, v0, 0x1

    .line 1107
    :cond_6
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    if-lt v0, p4, :cond_2

    goto/16 :goto_1
.end method

.method private static b(Landroid/database/MatrixCursor;ILjava/util/List;Ljava/util/Set;I)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/MatrixCursor;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 1183
    .line 1184
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAhead;

    .line 1185
    if-lt v1, p4, :cond_1

    .line 1213
    :cond_0
    return p1

    .line 1188
    :cond_1
    new-instance v3, Lcom/twitter/android/provider/SuggestionsProvider$f;

    iget-object v5, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v5, v5, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-direct {v3, v5}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 1189
    invoke-interface {p3, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1190
    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1191
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    .line 1192
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1193
    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1194
    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, v3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1195
    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, v3, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1196
    const-string/jumbo v3, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1197
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x40

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v6, v6, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1198
    invoke-virtual {v5, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1199
    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    .line 1200
    invoke-virtual {v3}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v6

    const/4 v3, 0x3

    .line 1199
    invoke-static {v6, v7, v8, v3, p1}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v3

    .line 1201
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1202
    invoke-virtual {v5, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1203
    invoke-virtual {v5, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1204
    invoke-virtual {v5, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1205
    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v3}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1206
    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, v3, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1207
    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v3, v3, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x2

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1208
    iget-object v0, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1209
    add-int/lit8 p1, p1, 0x1

    .line 1210
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 1212
    goto/16 :goto_0

    :cond_2
    move v3, v2

    .line 1207
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;I)I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/database/MatrixCursor;",
            "I",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/provider/SuggestionsProvider$f;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 1232
    if-lez p7, :cond_2

    .line 1233
    const/4 v3, 0x1

    const-string/jumbo v4, "tokens_topic_view"

    sget-object v5, Lcom/twitter/android/provider/SuggestionsProvider$h;->a:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v2, 0x64

    .line 1235
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    move-object v2, p0

    move-object v6, p3

    move-object/from16 v7, p4

    move-object/from16 v10, p5

    .line 1233
    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1237
    if-eqz v4, :cond_2

    .line 1239
    const/4 v2, 0x0

    move v3, p2

    .line 1240
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    move/from16 v0, p7

    if-ge v2, v0, :cond_1

    .line 1241
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1242
    new-instance v6, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v6, v5}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 1243
    move-object/from16 v0, p6

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1244
    move-object/from16 v0, p6

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1245
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v6

    .line 1246
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1247
    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1248
    invoke-virtual {v6, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1249
    invoke-virtual {v6, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1250
    const-string/jumbo v7, "com.twitter.android.action.SEARCH_TYPEAHEAD_TOPIC"

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1251
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1252
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1253
    const-wide/16 v8, -0x1

    const/16 v7, 0xc

    invoke-static {v8, v9, v5, v7, v3}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v5

    .line 1255
    invoke-virtual {v5}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1256
    add-int/lit8 v3, v3, 0x1

    .line 1257
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1261
    :cond_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    move p2, v3

    .line 1265
    :cond_2
    return p2

    .line 1261
    :catchall_0
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method public static b(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;"
        }
    .end annotation

    .prologue
    .line 680
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->l:Ljava/util/Map;

    monitor-enter v1

    .line 681
    :try_start_0
    const-string/jumbo v0, "@"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    :goto_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->l:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 681
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 683
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 687
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->l:Ljava/util/Map;

    monitor-enter v1

    .line 688
    :try_start_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 689
    monitor-exit v1

    .line 690
    return-void

    .line 689
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 694
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->m:Ljava/util/Map;

    monitor-enter v1

    .line 695
    :try_start_0
    const-string/jumbo v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    :goto_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->m:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    monitor-exit v1

    .line 698
    return-void

    .line 695
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 697
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;"
        }
    .end annotation

    .prologue
    .line 702
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->m:Ljava/util/Map;

    monitor-enter v1

    .line 703
    :try_start_0
    const-string/jumbo v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 704
    :goto_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->m:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 703
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 705
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c()V
    .locals 2

    .prologue
    .line 709
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->m:Ljava/util/Map;

    monitor-enter v1

    .line 710
    :try_start_0
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 711
    monitor-exit v1

    .line 712
    return-void

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1320
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1321
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1322
    sget-object v1, Lcrv;->b:Ljava/util/regex/Pattern;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1323
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    .line 1327
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_1

    const-string/jumbo v0, " "

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1325
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1327
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_1
.end method

.method private e(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 30

    .prologue
    .line 721
    invoke-static/range {p1 .. p1}, Lcom/twitter/android/provider/SuggestionsProvider;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 723
    new-instance v26, Lcom/twitter/android/provider/SuggestionsProvider$f;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 725
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v27

    .line 726
    invoke-static/range {v27 .. v27}, Lcom/twitter/library/provider/x;->b(Landroid/content/Context;)J

    move-result-wide v4

    .line 727
    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 729
    new-instance v19, Ljava/util/HashSet;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashSet;-><init>()V

    .line 730
    invoke-virtual {v4}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 732
    invoke-static {}, Lcom/twitter/android/client/w;->e()I

    move-result v12

    .line 733
    invoke-static {}, Lcom/twitter/android/client/w;->d()I

    move-result v20

    .line 734
    invoke-static {}, Lcom/twitter/android/client/w;->c()I

    move-result v13

    .line 735
    invoke-static {}, Lcom/twitter/android/client/w;->f()I

    move-result v22

    .line 737
    const/16 v17, 0x0

    .line 738
    new-instance v14, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/twitter/android/provider/SuggestionsProvider;->p:[Ljava/lang/String;

    invoke-direct {v14, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 754
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 755
    const-string/jumbo v6, "search_queries"

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 756
    const-string/jumbo v6, "name LIKE "

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 757
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 758
    const-string/jumbo v6, " AND query!=\'\'"

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 759
    const-string/jumbo v6, " AND query NOT LIKE \'place:%\'"

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 760
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 762
    sget-object v6, Lcom/twitter/android/provider/SuggestionsProvider$g;->a:[Ljava/lang/String;

    const-string/jumbo v7, "type IN (6,0)"

    const/4 v8, 0x0

    const-string/jumbo v9, "name"

    const/4 v10, 0x0

    const-string/jumbo v11, "type ASC, query_id DESC, time DESC"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v28

    .line 766
    invoke-static/range {v25 .. v25}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 768
    add-int v4, v20, v12

    if-nez v4, :cond_3

    .line 769
    const/4 v4, 0x0

    move-object/from16 v24, v4

    .line 776
    :goto_0
    const/4 v4, 0x0

    .line 778
    if-eqz v28, :cond_f

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 780
    move/from16 v0, v17

    move-object/from16 v1, v28

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v14, v0, v1, v2, v3}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;ILandroid/database/Cursor;Ljava/util/Set;I)I

    move-result v6

    .line 782
    sub-int v7, v6, v17

    add-int/2addr v4, v7

    .line 785
    sub-int v7, v20, v4

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-static {v14, v6, v0, v1, v7}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Landroid/database/MatrixCursor;ILandroid/database/Cursor;Ljava/util/Set;I)I

    move-result v17

    .line 787
    sub-int v6, v17, v6

    add-int/2addr v4, v6

    move/from16 v6, v17

    .line 790
    :goto_1
    if-eqz v24, :cond_e

    .line 792
    move-object/from16 v0, v24

    iget-object v7, v0, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->b:Ljava/util/List;

    sub-int v8, v20, v4

    move-object/from16 v0, v19

    invoke-static {v14, v6, v7, v0, v8}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;ILjava/util/List;Ljava/util/Set;I)I

    move-result v15

    .line 794
    sub-int v6, v15, v6

    add-int/2addr v4, v6

    .line 797
    :goto_2
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 798
    const/16 v21, 0x0

    .line 803
    const/4 v6, 0x1

    new-array v9, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x25

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v6

    .line 804
    sget-object v6, Lcom/twitter/android/provider/SuggestionsProvider;->j:Ljava/util/regex/Pattern;

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 805
    const/4 v6, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 806
    const/16 v7, 0x40

    if-ne v6, v7, :cond_4

    .line 808
    if-nez v4, :cond_d

    .line 809
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v14, v15, v1, v2}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/content/Context;Landroid/database/MatrixCursor;ILcom/twitter/android/provider/SuggestionsProvider$f;Ljava/util/Set;)I

    move-result v7

    .line 815
    :goto_3
    const-string/jumbo v8, "tokens_text LIKE ? AND users_username NOT NULL"

    const-string/jumbo v10, "tokens_weight DESC, LOWER(users_username) ASC"

    move-object v6, v14

    invoke-static/range {v5 .. v12}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;I)I

    move-result v15

    .line 818
    sub-int v6, v15, v7

    add-int v8, v21, v6

    .line 820
    const/4 v6, 0x1

    .line 821
    const/4 v7, 0x0

    move/from16 v29, v6

    move v6, v7

    move v7, v8

    move v8, v4

    move/from16 v4, v29

    .line 834
    :goto_4
    const/4 v10, 0x0

    move/from16 v21, v4

    move/from16 v22, v10

    move/from16 v23, v6

    move v4, v7

    move v6, v8

    .line 843
    :goto_5
    if-eqz v23, :cond_8

    .line 848
    if-eqz v22, :cond_7

    .line 850
    const-string/jumbo v16, "search_queries_query LIKE ?"

    .line 851
    const-string/jumbo v8, "users_name LIKE ?"

    .line 852
    const-string/jumbo v10, "tokens_weight DESC, LOWER(users_name) ASC"

    .line 861
    :goto_6
    const-string/jumbo v18, "tokens_weight DESC, LOWER(search_queries_query) ASC"

    sub-int v20, v20, v6

    move-object v13, v5

    move-object/from16 v17, v9

    invoke-static/range {v13 .. v20}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;I)I

    move-result v7

    .line 864
    sub-int v13, v7, v15

    add-int/2addr v6, v13

    .line 866
    if-nez v6, :cond_0

    .line 867
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v14, v7, v1, v2}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/content/Context;Landroid/database/MatrixCursor;ILcom/twitter/android/provider/SuggestionsProvider$f;Ljava/util/Set;)I

    move-result v7

    :cond_0
    move-object v6, v14

    .line 871
    invoke-static/range {v5 .. v12}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;I)I

    move-result v15

    .line 873
    sub-int v5, v15, v7

    add-int/2addr v4, v5

    move v6, v4

    move v5, v15

    .line 878
    :goto_7
    if-eqz v24, :cond_b

    .line 880
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a:Ljava/util/List;

    sub-int v7, v12, v6

    invoke-static {v14, v5, v4, v11, v7}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Landroid/database/MatrixCursor;ILjava/util/List;Ljava/util/Set;I)I

    move-result v4

    .line 882
    sub-int v5, v4, v5

    add-int/2addr v5, v6

    .line 885
    :goto_8
    if-nez v22, :cond_1

    .line 886
    if-eqz v21, :cond_9

    .line 887
    new-instance v5, Lcom/twitter/android/provider/SuggestionsProvider$f;

    const/4 v6, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-static {v0, v14, v4, v5}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/content/Context;Landroid/database/MatrixCursor;ILcom/twitter/android/provider/SuggestionsProvider$f;)I

    .line 900
    :cond_1
    :goto_9
    if-eqz v28, :cond_2

    .line 901
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 903
    :cond_2
    return-object v14

    .line 772
    :cond_3
    invoke-static/range {v25 .. v25}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    move-result-object v4

    move-object/from16 v24, v4

    goto/16 :goto_0

    .line 822
    :cond_4
    const/16 v7, 0x23

    if-ne v6, v7, :cond_5

    .line 824
    const-string/jumbo v16, "tokens_text LIKE ? AND search_queries_query NOT NULL"

    const-string/jumbo v18, "tokens_weight DESC, LOWER(search_queries_query) ASC"

    move-object v13, v5

    move-object/from16 v17, v9

    invoke-static/range {v13 .. v20}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;I)I

    move-result v8

    .line 827
    sub-int v6, v8, v15

    add-int v7, v4, v6

    .line 828
    const/4 v4, 0x0

    .line 829
    const/4 v6, 0x0

    move v15, v8

    move v8, v7

    move/from16 v7, v21

    goto/16 :goto_4

    .line 831
    :cond_5
    const/4 v6, 0x0

    .line 832
    const/4 v7, 0x1

    move v8, v4

    move v4, v6

    move v6, v7

    move/from16 v7, v21

    goto/16 :goto_4

    .line 836
    :cond_6
    const/4 v6, 0x0

    .line 840
    sget-object v7, Lcom/twitter/android/provider/SuggestionsProvider;->i:Ljava/util/regex/Pattern;

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    move/from16 v22, v7

    move/from16 v23, v7

    move/from16 v29, v6

    move v6, v4

    move/from16 v4, v21

    move/from16 v21, v29

    goto/16 :goto_5

    .line 855
    :cond_7
    const-string/jumbo v16, "tokens_text LIKE ? AND search_queries_query NOT NULL"

    .line 856
    const-string/jumbo v8, "tokens_text LIKE ? AND users_username NOT NULL"

    .line 857
    const-string/jumbo v10, "tokens_weight DESC, LOWER(users_username) ASC"

    goto/16 :goto_6

    .line 874
    :cond_8
    if-nez v6, :cond_c

    if-nez v21, :cond_c

    .line 875
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v14, v15, v1, v2}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/content/Context;Landroid/database/MatrixCursor;ILcom/twitter/android/provider/SuggestionsProvider$f;Ljava/util/Set;)I

    move-result v15

    move v6, v4

    move v5, v15

    goto/16 :goto_7

    .line 888
    :cond_9
    if-eqz v23, :cond_1

    .line 889
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v14, v4, v1}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/content/Context;Landroid/database/MatrixCursor;ILcom/twitter/android/provider/SuggestionsProvider$f;)I

    goto :goto_9

    .line 893
    :cond_a
    const v4, 0x7f0a0b97

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v21, 0x1

    move-object v15, v5

    move-object/from16 v16, v14

    move/from16 v20, v22

    invoke-static/range {v15 .. v21}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;ILjava/lang/String;Ljava/util/Set;IZ)I

    move-result v4

    .line 895
    if-eqz v28, :cond_1

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 896
    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-static {v14, v4, v0, v1, v13}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;ILandroid/database/Cursor;Ljava/util/Set;I)I

    move-result v4

    .line 897
    const v5, 0x7fffffff

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-static {v14, v4, v0, v1, v5}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Landroid/database/MatrixCursor;ILandroid/database/Cursor;Ljava/util/Set;I)I

    goto/16 :goto_9

    :cond_b
    move v4, v5

    goto/16 :goto_8

    :cond_c
    move v6, v4

    move v5, v15

    goto/16 :goto_7

    :cond_d
    move v7, v15

    goto/16 :goto_3

    :cond_e
    move v15, v6

    goto/16 :goto_2

    :cond_f
    move/from16 v6, v17

    goto/16 :goto_1
.end method


# virtual methods
.method a(Ljava/lang/String;Landroid/database/MatrixCursor;)Landroid/database/Cursor;
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 288
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 291
    if-nez v0, :cond_1

    move-object v5, v6

    move-object v4, v6

    .line 303
    :goto_0
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 305
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 306
    invoke-static {}, Lcom/twitter/android/client/w;->g()I

    move-result v12

    .line 307
    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string/jumbo v2, "tokens_user_view"

    sget-object v3, Lcom/twitter/android/provider/SuggestionsProvider$a;->a:[Ljava/lang/String;

    const-string/jumbo v8, "tokens_weight DESC, LOWER(users_username) ASC"

    .line 310
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    move-object v7, v6

    .line 307
    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 311
    if-eqz v3, :cond_4

    move v0, v10

    .line 312
    :cond_0
    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 313
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 314
    if-eqz v4, :cond_0

    .line 315
    invoke-virtual {p2}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    .line 316
    add-int/lit8 v2, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 317
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 318
    invoke-virtual {v5, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 319
    const/4 v0, 0x2

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 320
    const/4 v0, 0x3

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 321
    const/4 v0, 0x4

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 322
    const/4 v0, 0x5

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 323
    const/4 v0, 0x6

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 324
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v0, v2

    goto :goto_1

    .line 295
    :cond_1
    sget-object v0, Lcrv;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    const-string/jumbo v0, "users_name LIKE ?"

    .line 300
    :goto_2
    new-array v5, v1, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v10

    move-object v4, v0

    goto/16 :goto_0

    .line 298
    :cond_2
    const-string/jumbo v0, "tokens_text LIKE ? AND users_username NOT NULL"

    goto :goto_2

    .line 327
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move v4, v12

    move-object v5, v11

    .line 330
    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/MatrixCursor;Ljava/lang/String;ILjava/util/Set;Lcpv;)V

    .line 331
    return-object p2
.end method

.method a(Ljava/lang/String;Landroid/database/MatrixCursor;Z)Landroid/database/Cursor;
    .locals 13

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/twitter/android/provider/SuggestionsProvider;->getContext()Landroid/content/Context;

    .line 514
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 515
    const/4 v0, 0x0

    .line 516
    invoke-static {p1}, Lcom/twitter/android/provider/SuggestionsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 517
    if-eqz v1, :cond_7

    .line 518
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAhead;

    .line 519
    invoke-virtual {p2}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v4

    .line 520
    add-int/lit8 v2, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 521
    iget-object v1, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->g:Ljava/lang/String;

    invoke-virtual {v4, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 523
    new-instance v1, Lcom/twitter/android/provider/SuggestionsProvider$f;

    iget-object v0, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->g:Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    invoke-interface {v11, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 524
    goto :goto_0

    :cond_0
    move v10, v1

    .line 527
    :goto_1
    invoke-static {}, Lcom/twitter/android/client/w;->g()I

    move-result v12

    .line 528
    if-ge v10, v12, :cond_3

    .line 529
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 530
    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x1

    const-string/jumbo v2, "tokens_topic_view"

    sget-object v3, Lcom/twitter/android/provider/SuggestionsProvider$h;->a:[Ljava/lang/String;

    const-string/jumbo v4, "tokens_text LIKE ? AND search_queries_query NOT NULL"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string/jumbo v8, "tokens_weight DESC, LOWER(search_queries_query) ASC"

    .line 533
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 530
    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 534
    if-eqz v1, :cond_3

    .line 535
    :cond_1
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 537
    new-instance v0, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v0, v2}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    invoke-interface {v11, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 538
    if-lt v10, v12, :cond_6

    .line 546
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 550
    :cond_3
    if-ge v10, v12, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_5

    .line 551
    invoke-virtual {p2}, Landroid/database/MatrixCursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_4

    if-eqz p3, :cond_5

    :cond_4
    new-instance v0, Lcom/twitter/android/provider/SuggestionsProvider$f;

    invoke-direct {v0, p1}, Lcom/twitter/android/provider/SuggestionsProvider$f;-><init>(Ljava/lang/String;)V

    .line 552
    invoke-interface {v11, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 553
    invoke-virtual {p2}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 554
    add-int/lit8 v1, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 555
    invoke-virtual {v0, p1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 558
    :cond_5
    return-object p2

    .line 541
    :cond_6
    invoke-virtual {p2}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 542
    add-int/lit8 v0, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 543
    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move v10, v0

    goto :goto_2

    :cond_7
    move v10, v0

    goto/16 :goto_1
.end method

.method a(Ljava/lang/String;Lcom/twitter/library/provider/ParcelableMatrixCursor;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 563
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 562
    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 564
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 565
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    const-string/jumbo v3, "country=name OR country=\'\'"

    const/4 v4, 0x0

    const-string/jumbo v5, "country ASC"

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/twitter/library/provider/ParcelableMatrixCursor;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object p2

    .line 575
    :goto_0
    return-object p2

    .line 569
    :cond_0
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 570
    const-string/jumbo v3, "country LIKE ? AND country=name"

    const-string/jumbo v5, "country ASC"

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/twitter/library/provider/ParcelableMatrixCursor;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Landroid/database/Cursor;

    .line 572
    const-string/jumbo v3, "name LIKE ?"

    const-string/jumbo v5, "name ASC,country ASC"

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/twitter/library/provider/ParcelableMatrixCursor;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Landroid/database/Cursor;

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 644
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Delete not supported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 609
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 632
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 611
    :pswitch_0
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    .line 629
    :goto_0
    return-object v0

    .line 614
    :pswitch_1
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.android.search.suggest"

    goto :goto_0

    .line 617
    :pswitch_2
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.suggest_compose_users"

    goto :goto_0

    .line 620
    :pswitch_3
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.suggest_hashtags"

    goto :goto_0

    .line 623
    :pswitch_4
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.suggest_dmable_users"

    goto :goto_0

    .line 626
    :pswitch_5
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.suggest_locations"

    goto :goto_0

    .line 629
    :pswitch_6
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.suggest_users"

    goto :goto_0

    .line 609
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 639
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Insert not supported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 232
    const-string/jumbo v0, "SuggestionsProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "QUERY uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    sget-object v0, Lcom/twitter/android/provider/SuggestionsProvider;->o:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 234
    packed-switch v0, :pswitch_data_0

    .line 282
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 237
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/provider/SuggestionsProvider;->e(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 278
    :goto_1
    return-object v0

    .line 236
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_1

    .line 250
    :pswitch_2
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider$l;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0, p3, v0}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Landroid/database/MatrixCursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 253
    :pswitch_3
    invoke-static {p3}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-eq v0, v1, :cond_2

    .line 254
    :cond_1
    if-eqz p3, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object p3, v0

    .line 256
    :cond_2
    const-string/jumbo v0, "add_query_to_empty_result"

    const/4 v1, 0x0

    .line 257
    invoke-static {p1, v0, v1}, Lcom/twitter/util/ac;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v0

    .line 258
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider$c;->a:[Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0, p3, v1, v0}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Landroid/database/MatrixCursor;Z)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 254
    :cond_3
    const-string/jumbo v0, "#"

    goto :goto_2

    .line 262
    :pswitch_4
    const-string/jumbo v0, "add_real_time_suggestions"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/twitter/util/ac;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v3

    .line 264
    const-string/jumbo v0, "add_query"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/twitter/util/ac;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v5

    .line 265
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/twitter/android/provider/b;

    invoke-direct {v7}, Lcom/twitter/android/provider/b;-><init>()V

    move-object v0, p0

    move-object v1, p3

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;IZZZLjava/util/List;Lcpv;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 269
    :pswitch_5
    new-instance v0, Lcom/twitter/library/provider/ParcelableMatrixCursor;

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider$e;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0, p3, v0}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Lcom/twitter/library/provider/ParcelableMatrixCursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 273
    :pswitch_6
    const-string/jumbo v0, "friendship"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/twitter/util/ac;->a(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v2

    .line 274
    const-string/jumbo v0, "add_real_time_suggestions"

    const/4 v1, 0x0

    .line 275
    invoke-static {p1, v0, v1}, Lcom/twitter/util/ac;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v3

    .line 276
    const-string/jumbo v0, "additional_user_ids"

    const/4 v1, 0x0

    .line 277
    invoke-static {p1, v0, v1}, Lcom/twitter/util/ac;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 278
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p3

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;IZZZLjava/util/List;Lcpv;)Landroid/database/Cursor;

    move-result-object v0

    goto/16 :goto_1

    .line 234
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 649
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Update not supported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
