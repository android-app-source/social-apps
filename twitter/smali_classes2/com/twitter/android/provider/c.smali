.class public Lcom/twitter/android/provider/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/provider/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/provider/c$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Lcom/twitter/library/view/b;


# direct methods
.method private constructor <init>(Lcom/twitter/android/provider/c$a;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {p1}, Lcom/twitter/android/provider/c$a;->a(Lcom/twitter/android/provider/c$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/provider/c;->a:Ljava/lang/String;

    .line 18
    invoke-static {p1}, Lcom/twitter/android/provider/c$a;->b(Lcom/twitter/android/provider/c$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/provider/c;->b:J

    .line 19
    invoke-static {p1}, Lcom/twitter/android/provider/c$a;->c(Lcom/twitter/android/provider/c$a;)Lcom/twitter/library/view/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/provider/c;->c:Lcom/twitter/library/view/b;

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/provider/c$a;Lcom/twitter/android/provider/c$1;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/twitter/android/provider/c;-><init>(Lcom/twitter/android/provider/c$a;)V

    return-void
.end method
