.class Lcom/twitter/android/provider/SuggestionsProvider$f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/provider/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "f"
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1514
    iput-object p1, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    .line 1515
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->b:I

    .line 1516
    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519
    iput-object p1, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    .line 1520
    iput p2, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->b:I

    .line 1521
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1525
    if-ne p0, p1, :cond_1

    .line 1537
    :cond_0
    :goto_0
    return v0

    .line 1528
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1529
    goto :goto_0

    .line 1532
    :cond_3
    check-cast p1, Lcom/twitter/android/provider/SuggestionsProvider$f;

    .line 1534
    iget-object v2, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1535
    iget-object v2, p1, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1537
    :cond_4
    iget v2, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->b:I

    iget v3, p1, Lcom/twitter/android/provider/SuggestionsProvider$f;->b:I

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1544
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/android/provider/SuggestionsProvider$f;->b:I

    add-int/2addr v0, v1

    .line 1545
    return v0
.end method
