.class public Lcom/twitter/android/provider/c$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/provider/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:Lcom/twitter/library/view/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/provider/c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/provider/c$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/provider/c$a;)J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/twitter/android/provider/c$a;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/android/provider/c$a;)Lcom/twitter/library/view/b;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/provider/c$a;->c:Lcom/twitter/library/view/b;

    return-object v0
.end method


# virtual methods
.method public a(J)Lcom/twitter/android/provider/c$a;
    .locals 1

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/twitter/android/provider/c$a;->b:J

    .line 36
    return-object p0
.end method

.method public a(Lcom/twitter/library/view/b;)Lcom/twitter/android/provider/c$a;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/twitter/android/provider/c$a;->c:Lcom/twitter/library/view/b;

    .line 42
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/provider/c$a;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/twitter/android/provider/c$a;->a:Ljava/lang/String;

    .line 30
    return-object p0
.end method

.method public a()Lcom/twitter/android/provider/c;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/android/provider/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/provider/c;-><init>(Lcom/twitter/android/provider/c$a;Lcom/twitter/android/provider/c$1;)V

    return-object v0
.end method
