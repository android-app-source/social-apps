.class public Lcom/twitter/android/provider/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lna;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/provider/d$a;,
        Lcom/twitter/android/provider/d$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lna",
        "<",
        "Lcom/twitter/android/provider/d$b;",
        "Lcom/twitter/library/provider/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/provider/d$b;",
            "Lcbl",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/provider/d;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(JI)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {p0, p1}, Lcom/twitter/library/database/dm/a;->b(J)Lcom/twitter/library/database/dm/a;

    move-result-object v0

    .line 71
    invoke-static {p0, p1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v3

    .line 72
    invoke-interface {v0, p2}, Lcom/twitter/library/database/dm/b;->a(I)Ljava/util/List;

    move-result-object v1

    .line 73
    new-instance v0, Lbta;

    invoke-virtual {v3}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v2

    invoke-direct {v0, v2}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    .line 74
    invoke-virtual {v0, p0, p1}, Lbta;->a(J)Ljava/lang/Iterable;

    move-result-object v0

    .line 73
    invoke-static {v0}, Lcom/twitter/android/provider/d;->a(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v2

    .line 75
    invoke-static {v1, v3}, Lcom/twitter/android/provider/d;->a(Ljava/util/List;Lcom/twitter/library/provider/w;)Ljava/util/Map;

    move-result-object v3

    .line 76
    const/4 v6, 0x1

    move-wide v4, p0

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/provider/d;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;JZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/List;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-interface {p0, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;JZ)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/database/dm/c;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/dms/q;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;JZ)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 108
    invoke-static {}, Lcom/twitter/util/collection/h;->f()Lcom/twitter/util/collection/h;

    move-result-object v5

    .line 109
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/database/dm/c;

    .line 110
    iget-boolean v1, v0, Lcom/twitter/library/database/dm/c;->b:Z

    if-eqz v1, :cond_1

    .line 111
    iget-object v1, v0, Lcom/twitter/library/database/dm/c;->a:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/q;

    .line 112
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/twitter/model/dms/q;->k:Z

    if-nez v2, :cond_0

    .line 113
    new-instance v2, Lcom/twitter/library/provider/b$a;

    invoke-direct {v2}, Lcom/twitter/library/provider/b$a;-><init>()V

    .line 114
    invoke-virtual {v2, v1}, Lcom/twitter/library/provider/b$a;->a(Lcom/twitter/model/dms/q;)Lcom/twitter/library/provider/b$a;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/database/dm/c;->c:Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/b$a;->a(Lcom/twitter/library/database/dm/ShareHistoryTable$Type;)Lcom/twitter/library/provider/c$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/c$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 113
    invoke-virtual {v5, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 117
    :cond_1
    iget-object v1, v0, Lcom/twitter/library/database/dm/c;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 118
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    .line 119
    if-eqz v1, :cond_0

    .line 120
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9, p3, p4}, Lcom/twitter/library/dm/e;->a(JJ)Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/q;

    .line 122
    if-nez v2, :cond_3

    if-nez p5, :cond_2

    move v2, v3

    .line 123
    :goto_1
    if-eqz v2, :cond_0

    .line 124
    new-instance v2, Lcom/twitter/library/provider/d$a;

    invoke-direct {v2}, Lcom/twitter/library/provider/d$a;-><init>()V

    .line 125
    invoke-virtual {v2, v1}, Lcom/twitter/library/provider/d$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/provider/d$a;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/database/dm/c;->c:Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/d$a;->a(Lcom/twitter/library/database/dm/ShareHistoryTable$Type;)Lcom/twitter/library/provider/c$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/c$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 124
    invoke-virtual {v5, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    :cond_2
    move v2, v4

    .line 122
    goto :goto_1

    :cond_3
    iget-boolean v2, v2, Lcom/twitter/model/dms/q;->k:Z

    if-nez v2, :cond_4

    move v2, v3

    goto :goto_1

    :cond_4
    move v2, v4

    goto :goto_1

    .line 131
    :cond_5
    invoke-virtual {v5}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method static a(Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/dms/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 82
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 83
    iget-object v3, v0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 85
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method static a(Ljava/util/List;Lcom/twitter/library/provider/w;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/database/dm/c;",
            ">;",
            "Lcom/twitter/library/provider/w;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 92
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/database/dm/c;

    .line 93
    iget-boolean v3, v0, Lcom/twitter/library/database/dm/c;->b:Z

    if-nez v3, :cond_0

    .line 95
    :try_start_0
    iget-object v0, v0, Lcom/twitter/library/database/dm/c;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {p1, v0}, Lcom/twitter/library/provider/w;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/provider/d$b;)Lcbi;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/provider/d$b;",
            ")",
            "Lcbi",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/provider/d;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-wide v0, p1, Lcom/twitter/android/provider/d$b;->b:J

    invoke-static {v0, v1}, Lcom/twitter/library/database/dm/a;->b(J)Lcom/twitter/library/database/dm/a;

    move-result-object v0

    .line 47
    iget-wide v2, p1, Lcom/twitter/android/provider/d$b;->b:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v3

    .line 49
    iget-wide v4, p1, Lcom/twitter/android/provider/d$b;->a:J

    .line 50
    invoke-interface {v0, v4, v5}, Lcom/twitter/library/database/dm/b;->c(J)Ljava/util/Map;

    move-result-object v1

    .line 51
    new-instance v7, Lcom/twitter/android/provider/d$a;

    invoke-direct {v7, v1, p1}, Lcom/twitter/android/provider/d$a;-><init>(Ljava/util/Map;Lcom/twitter/android/provider/d$b;)V

    .line 53
    const/4 v1, -0x1

    .line 54
    invoke-interface {v0, v1}, Lcom/twitter/library/database/dm/b;->a(I)Ljava/util/List;

    move-result-object v1

    .line 55
    new-instance v0, Lbta;

    invoke-virtual {v3}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v2

    invoke-direct {v0, v2}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    iget-wide v4, p1, Lcom/twitter/android/provider/d$b;->b:J

    .line 56
    invoke-virtual {v0, v4, v5}, Lbta;->a(J)Ljava/lang/Iterable;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcom/twitter/android/provider/d;->a(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v2

    .line 57
    invoke-static {v1, v3}, Lcom/twitter/android/provider/d;->a(Ljava/util/List;Lcom/twitter/library/provider/w;)Ljava/util/Map;

    move-result-object v3

    .line 58
    iget-wide v4, p1, Lcom/twitter/android/provider/d$b;->b:J

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/provider/d;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;JZ)Ljava/util/List;

    move-result-object v0

    .line 60
    invoke-static {v0, v7}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/twitter/android/provider/d;->a:Ljava/util/Map;

    new-instance v2, Lcbl;

    iget v3, p1, Lcom/twitter/android/provider/d$b;->d:I

    invoke-static {v0, v3}, Lcom/twitter/android/provider/d;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/provider/d;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbi;

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public a(Lcom/twitter/android/provider/d$b;Lna$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/provider/d$b;",
            "Lna$a",
            "<",
            "Lcom/twitter/android/provider/d$b;",
            "Lcom/twitter/library/provider/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/twitter/android/provider/d;->a(Lcom/twitter/android/provider/d$b;)Lcbi;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lna$a;->a(Ljava/lang/Object;Lcbi;)V

    .line 41
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lna$a;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lcom/twitter/android/provider/d$b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/provider/d;->a(Lcom/twitter/android/provider/d$b;Lna$a;)V

    return-void
.end method
