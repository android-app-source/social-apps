.class Lcom/twitter/android/provider/a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lna$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/provider/a;->a(Lna$a;)Lna$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lna$a",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lna$a;

.field final synthetic b:Lcom/twitter/android/provider/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/provider/a;Lna$a;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/android/provider/a$1;->b:Lcom/twitter/android/provider/a;

    iput-object p2, p0, Lcom/twitter/android/provider/a$1;->a:Lna$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lcbi;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/provider/a$1;->a(Ljava/lang/String;Lcbi;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcbi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcbi",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {p2}, Lcom/twitter/android/provider/a;->a(Lcbi;)Lcbi;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/twitter/android/provider/a$1;->b:Lcom/twitter/android/provider/a;

    invoke-static {v1}, Lcom/twitter/android/provider/a;->a(Lcom/twitter/android/provider/a;)Lcom/twitter/android/provider/a$a;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/android/provider/a$a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/twitter/android/provider/a$1;->b:Lcom/twitter/android/provider/a;

    .line 73
    invoke-static {v1}, Lcom/twitter/android/provider/a;->c(Lcom/twitter/android/provider/a;)Lcom/twitter/android/provider/d;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/provider/a$1;->b:Lcom/twitter/android/provider/a;

    invoke-static {v2}, Lcom/twitter/android/provider/a;->b(Lcom/twitter/android/provider/a;)Lcom/twitter/android/provider/d$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/provider/d;->a(Lcom/twitter/android/provider/d$b;)Lcbi;

    move-result-object v1

    .line 74
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 75
    invoke-static {v1}, Lcom/twitter/android/provider/a;->b(Lcbi;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 76
    invoke-static {v0, v1}, Lcom/twitter/android/provider/a;->a(Lcbi;Lcbi;)Lcbi;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 82
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/provider/a$1;->a:Lna$a;

    new-instance v2, Lcbl;

    invoke-direct {v2, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-interface {v1, p1, v2}, Lna$a;->a(Ljava/lang/Object;Lcbi;)V

    .line 83
    return-void

    .line 79
    :cond_0
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
