.class public final Lcom/twitter/android/provider/f$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/provider/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/android/provider/f;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:J

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/provider/f$a;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/twitter/android/provider/f$a;->f:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/android/provider/f$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/provider/f$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/provider/f$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/provider/f$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/provider/f$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/provider/f$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/provider/f$a;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/twitter/android/provider/f$a;->d:I

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/provider/f$a;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/twitter/android/provider/f$a;->e:I

    return v0
.end method

.method static synthetic g(Lcom/twitter/android/provider/f$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/provider/f$a;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/android/provider/f$a;
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/twitter/android/provider/f$a;->d:I

    .line 62
    return-object p0
.end method

.method public a(J)Lcom/twitter/android/provider/f$a;
    .locals 1

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/twitter/android/provider/f$a;->f:J

    .line 67
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/twitter/android/provider/f$a;->a:Ljava/lang/String;

    .line 47
    return-object p0
.end method

.method public b(I)Lcom/twitter/android/provider/f$a;
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/twitter/android/provider/f$a;->e:I

    .line 72
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/android/provider/f$a;->b:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/provider/f$a;->e()Lcom/twitter/android/provider/f;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/twitter/android/provider/f$a;->c:Ljava/lang/String;

    .line 57
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/android/provider/f$a;
    .locals 1

    .prologue
    .line 76
    invoke-static {p1}, Lcom/twitter/model/businessprofiles/h$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/provider/f$a;->g:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public e()Lcom/twitter/android/provider/f;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/twitter/android/provider/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/provider/f;-><init>(Lcom/twitter/android/provider/f$a;Lcom/twitter/android/provider/f$1;)V

    return-object v0
.end method
