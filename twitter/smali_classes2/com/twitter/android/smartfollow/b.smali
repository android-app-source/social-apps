.class public final Lcom/twitter/android/smartfollow/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/smartfollow/w;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/smartfollow/b$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/c;",
            ">;"
        }
    .end annotation
.end field

.field private D:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private E:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;"
        }
    .end annotation
.end field

.field private F:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/j;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;"
        }
    .end annotation
.end field

.field private H:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/d;",
            ">;"
        }
    .end annotation
.end field

.field private I:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private J:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/b;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;"
        }
    .end annotation
.end field

.field private L:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private M:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lsu;",
            ">;"
        }
    .end annotation
.end field

.field private N:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ltc;",
            ">;"
        }
    .end annotation
.end field

.field private O:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lta;",
            ">;"
        }
    .end annotation
.end field

.field private P:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/interestpicker/a;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private R:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/sharelocation/a;",
            ">;"
        }
    .end annotation
.end field

.field private S:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private T:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/waitingforsuggestions/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private U:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/waitingforsuggestions/a;",
            ">;"
        }
    .end annotation
.end field

.field private V:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private W:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/importcontacts/a;",
            ">;"
        }
    .end annotation
.end field

.field private X:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private Z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/finishingtimeline/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/finishingtimeline/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsv;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lanr;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/h;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laem$a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laet;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laes;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/j;",
            ">;>;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/n;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ah;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laeq;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laem;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/d;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/twitter/android/smartfollow/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/smartfollow/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/smartfollow/b$a;)V
    .locals 1

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    sget-boolean v0, Lcom/twitter/android/smartfollow/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 200
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/smartfollow/b;->a(Lcom/twitter/android/smartfollow/b$a;)V

    .line 201
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/smartfollow/b$a;Lcom/twitter/android/smartfollow/b$1;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/twitter/android/smartfollow/b;-><init>(Lcom/twitter/android/smartfollow/b$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/android/smartfollow/b$a;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Lcom/twitter/android/smartfollow/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/smartfollow/b$a;-><init>(Lcom/twitter/android/smartfollow/b$1;)V

    return-object v0
.end method

.method private a(Lcom/twitter/android/smartfollow/b$a;)V
    .locals 6

    .prologue
    .line 210
    new-instance v0, Lcom/twitter/android/smartfollow/b$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/b$1;-><init>(Lcom/twitter/android/smartfollow/b;Lcom/twitter/android/smartfollow/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->b:Lcta;

    .line 226
    invoke-static {p1}, Lcom/twitter/android/smartfollow/b$a;->b(Lcom/twitter/android/smartfollow/b$a;)Lano;

    move-result-object v0

    .line 225
    invoke-static {v0}, Lanq;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 224
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->c:Lcta;

    .line 228
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->c:Lcta;

    .line 230
    invoke-static {v0}, Lcom/twitter/android/smartfollow/l;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 229
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->d:Lcta;

    .line 236
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->b:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->d:Lcta;

    .line 235
    invoke-static {v0, v1, v2}, Lcom/twitter/android/smartfollow/finishingtimeline/b;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 234
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->e:Lcta;

    .line 240
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->e:Lcta;

    .line 241
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->f:Lcta;

    .line 243
    new-instance v0, Lcom/twitter/android/smartfollow/b$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/b$2;-><init>(Lcom/twitter/android/smartfollow/b;Lcom/twitter/android/smartfollow/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->g:Lcta;

    .line 256
    new-instance v0, Lcom/twitter/android/smartfollow/b$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/b$3;-><init>(Lcom/twitter/android/smartfollow/b;Lcom/twitter/android/smartfollow/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->h:Lcta;

    .line 269
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->g:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->h:Lcta;

    .line 271
    invoke-static {v0, v1}, Lcom/twitter/android/smartfollow/j;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 270
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->i:Lcta;

    .line 274
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->i:Lcta;

    .line 276
    invoke-static {v0}, Lbsw;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 275
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->j:Lcta;

    .line 280
    invoke-static {}, Lcom/twitter/android/smartfollow/o;->c()Ldagger/internal/c;

    move-result-object v0

    .line 279
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->k:Lcta;

    .line 285
    invoke-static {p1}, Lcom/twitter/android/smartfollow/b$a;->b(Lcom/twitter/android/smartfollow/b$a;)Lano;

    move-result-object v0

    .line 284
    invoke-static {v0}, Lanp;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 283
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->l:Lcta;

    .line 287
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->l:Lcta;

    .line 288
    invoke-static {v0}, Lcom/twitter/android/smartfollow/r;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->m:Lcta;

    .line 291
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->m:Lcta;

    .line 292
    invoke-static {v0}, Lcom/twitter/android/smartfollow/m;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->n:Lcta;

    .line 295
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->n:Lcta;

    .line 297
    invoke-static {v0}, Lcom/twitter/android/smartfollow/p;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 296
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->o:Lcta;

    .line 300
    new-instance v0, Lcom/twitter/android/smartfollow/b$4;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/b$4;-><init>(Lcom/twitter/android/smartfollow/b;Lcom/twitter/android/smartfollow/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->p:Lcta;

    .line 313
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->p:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->h:Lcta;

    .line 315
    invoke-static {v0, v1}, Lcom/twitter/android/smartfollow/u;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 314
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->q:Lcta;

    .line 318
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->p:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->h:Lcta;

    .line 320
    invoke-static {v0, v1}, Lcom/twitter/android/smartfollow/s;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 319
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->r:Lcta;

    .line 323
    new-instance v0, Lcom/twitter/android/smartfollow/b$5;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/b$5;-><init>(Lcom/twitter/android/smartfollow/b;Lcom/twitter/android/smartfollow/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->s:Lcta;

    .line 336
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->s:Lcta;

    .line 338
    invoke-static {v0}, Lcom/twitter/android/people/h;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 337
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->t:Lcta;

    .line 342
    invoke-static {}, Lcom/twitter/android/smartfollow/followpeople/o;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->u:Lcta;

    .line 344
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->u:Lcta;

    .line 345
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->v:Lcta;

    .line 350
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->p:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->h:Lcta;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/b;->t:Lcta;

    iget-object v4, p0, Lcom/twitter/android/smartfollow/b;->v:Lcta;

    .line 349
    invoke-static {v0, v1, v2, v3, v4}, Laer;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 348
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->w:Lcta;

    .line 356
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->k:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->o:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->q:Lcta;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/b;->r:Lcta;

    iget-object v4, p0, Lcom/twitter/android/smartfollow/b;->w:Lcta;

    iget-object v5, p0, Lcom/twitter/android/smartfollow/b;->t:Lcta;

    .line 358
    invoke-static/range {v0 .. v5}, Laen;->a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 357
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->x:Lcta;

    .line 366
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->v:Lcta;

    .line 367
    invoke-static {v0}, Lcom/twitter/android/people/adapters/e;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->y:Lcta;

    .line 369
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->j:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->x:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->y:Lcta;

    .line 371
    invoke-static {v0, v1, v2}, Lcom/twitter/android/people/j;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 370
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->z:Lcta;

    .line 376
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->c:Lcta;

    .line 378
    invoke-static {v0}, Lcom/twitter/android/smartfollow/n;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 377
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->A:Lcta;

    .line 381
    new-instance v0, Lcom/twitter/android/smartfollow/b$6;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/b$6;-><init>(Lcom/twitter/android/smartfollow/b;Lcom/twitter/android/smartfollow/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->B:Lcta;

    .line 394
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->m:Lcta;

    .line 395
    invoke-static {v0}, Lcom/twitter/android/smartfollow/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->C:Lcta;

    .line 399
    invoke-static {}, Lcom/twitter/android/smartfollow/followpeople/g;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->D:Lcta;

    .line 403
    invoke-static {}, Lcom/twitter/android/smartfollow/followpeople/m;->c()Ldagger/internal/c;

    move-result-object v0

    .line 402
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->E:Lcta;

    .line 405
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->B:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->C:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->E:Lcta;

    .line 406
    invoke-static {v0, v1, v2}, Lcom/twitter/android/smartfollow/followpeople/k;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->F:Lcta;

    .line 411
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->F:Lcta;

    .line 414
    invoke-static {v0}, Lcom/twitter/android/smartfollow/t;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 412
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->G:Lcta;

    .line 416
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->B:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->C:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->D:Lcta;

    .line 417
    invoke-static {v0, v1, v2}, Lcom/twitter/android/smartfollow/followpeople/e;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->H:Lcta;

    .line 422
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->H:Lcta;

    .line 424
    invoke-static {v0}, Lcom/twitter/android/smartfollow/q;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 423
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->I:Lcta;

    .line 427
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->B:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->C:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->D:Lcta;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/b;->E:Lcta;

    iget-object v4, p0, Lcom/twitter/android/smartfollow/b;->G:Lcta;

    iget-object v5, p0, Lcom/twitter/android/smartfollow/b;->I:Lcta;

    .line 429
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/smartfollow/followpeople/c;->a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 428
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->J:Lcta;

    .line 440
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->z:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->A:Lcta;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/b;->J:Lcta;

    .line 439
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/smartfollow/followpeople/i;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 438
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->K:Lcta;

    .line 445
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->K:Lcta;

    .line 446
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->L:Lcta;

    .line 451
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->g:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->h:Lcta;

    .line 450
    invoke-static {v0, v1, v2}, Lsv;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 449
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->M:Lcta;

    .line 455
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->M:Lcta;

    .line 457
    invoke-static {v0}, Ltd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 456
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->N:Lcta;

    .line 459
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->N:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->h:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->i:Lcta;

    .line 461
    invoke-static {v0, v1, v2}, Ltb;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 460
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->O:Lcta;

    .line 469
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->O:Lcta;

    .line 468
    invoke-static {v0, v1}, Lcom/twitter/android/smartfollow/interestpicker/b;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 467
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->P:Lcta;

    .line 472
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->P:Lcta;

    .line 473
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->Q:Lcta;

    .line 477
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/smartfollow/sharelocation/b;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    .line 476
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->R:Lcta;

    .line 479
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->R:Lcta;

    .line 480
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->S:Lcta;

    .line 482
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->c:Lcta;

    .line 484
    invoke-static {v0}, Lcom/twitter/android/smartfollow/v;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 483
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->T:Lcta;

    .line 490
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->b:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/b;->T:Lcta;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/b;->x:Lcta;

    .line 489
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/smartfollow/waitingforsuggestions/b;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 488
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->U:Lcta;

    .line 495
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->U:Lcta;

    .line 496
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->V:Lcta;

    .line 501
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    .line 500
    invoke-static {v0}, Lcom/twitter/android/smartfollow/importcontacts/b;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    .line 499
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->W:Lcta;

    .line 503
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->W:Lcta;

    .line 504
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->X:Lcta;

    .line 506
    const/4 v0, 0x6

    const/4 v1, 0x0

    .line 507
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->f:Lcta;

    .line 508
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->L:Lcta;

    .line 509
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->Q:Lcta;

    .line 510
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->S:Lcta;

    .line 511
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->V:Lcta;

    .line 512
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/b;->X:Lcta;

    .line 513
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 514
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->Y:Lcta;

    .line 516
    new-instance v0, Lcom/twitter/android/smartfollow/b$7;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/b$7;-><init>(Lcom/twitter/android/smartfollow/b;Lcom/twitter/android/smartfollow/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->Z:Lcta;

    .line 529
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->Z:Lcta;

    .line 531
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 530
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/b;->aa:Lcta;

    .line 532
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 536
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->Y:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->aa:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/android/smartfollow/finishingtimeline/a;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->e:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/finishingtimeline/a;

    return-object v0
.end method

.method public e()Lcom/twitter/android/smartfollow/followpeople/h;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->K:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/h;

    return-object v0
.end method

.method public f()Lcom/twitter/android/smartfollow/interestpicker/a;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->P:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestpicker/a;

    return-object v0
.end method

.method public g()Lcom/twitter/android/smartfollow/sharelocation/a;
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->R:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/sharelocation/a;

    return-object v0
.end method

.method public h()Lcom/twitter/android/smartfollow/waitingforsuggestions/a;
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->U:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;

    return-object v0
.end method

.method public i()Lcom/twitter/android/smartfollow/importcontacts/a;
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/twitter/android/smartfollow/b;->W:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/importcontacts/a;

    return-object v0
.end method
