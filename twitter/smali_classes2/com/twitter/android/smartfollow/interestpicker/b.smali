.class public final Lcom/twitter/android/smartfollow/interestpicker/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/smartfollow/interestpicker/a;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/smartfollow/interestpicker/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/twitter/android/smartfollow/interestpicker/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/smartfollow/interestpicker/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/smartfollow/interestpicker/a;",
            ">;",
            "Lcta",
            "<",
            "Lta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lcom/twitter/android/smartfollow/interestpicker/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/smartfollow/interestpicker/b;->b:Lcsd;

    .line 24
    sget-boolean v0, Lcom/twitter/android/smartfollow/interestpicker/b;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/smartfollow/interestpicker/b;->c:Lcta;

    .line 26
    return-void
.end method

.method public static a(Lcsd;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/smartfollow/interestpicker/a;",
            ">;",
            "Lcta",
            "<",
            "Lta;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/smartfollow/interestpicker/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/android/smartfollow/interestpicker/b;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/interestpicker/b;-><init>(Lcsd;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/smartfollow/interestpicker/a;
    .locals 3

    .prologue
    .line 30
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/b;->b:Lcsd;

    new-instance v2, Lcom/twitter/android/smartfollow/interestpicker/a;

    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/b;->c:Lcta;

    .line 32
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lta;

    invoke-direct {v2, v0}, Lcom/twitter/android/smartfollow/interestpicker/a;-><init>(Lta;)V

    .line 30
    invoke-static {v1, v2}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestpicker/a;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/b;->a()Lcom/twitter/android/smartfollow/interestpicker/a;

    move-result-object v0

    return-object v0
.end method
