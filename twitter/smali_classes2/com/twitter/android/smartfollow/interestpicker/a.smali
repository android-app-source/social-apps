.class public Lcom/twitter/android/smartfollow/interestpicker/a;
.super Lcom/twitter/android/smartfollow/a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/onboarding/flowstep/common/h;
.implements Lsz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/smartfollow/a",
        "<",
        "Landroid/os/Parcelable;",
        "Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;",
        ">;",
        "Lcom/twitter/app/onboarding/flowstep/common/h",
        "<",
        "Lcom/twitter/model/onboarding/e",
        "<",
        "Ljava/lang/String;",
        ">;>;",
        "Lsz;"
    }
.end annotation


# instance fields
.field private final j:Lta;

.field private k:Lcnk;


# direct methods
.method public constructor <init>(Lta;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/a;-><init>()V

    .line 54
    sget-object v0, Lcnk;->a:Lcnk;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->k:Lcnk;

    .line 58
    iput-object p1, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    .line 59
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {v0}, Lta;->e()V

    .line 60
    return-void
.end method


# virtual methods
.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 189
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 190
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    .line 191
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "extra_search_terms"

    sget-object v3, Ltg;->f:Ltg$b;

    .line 192
    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 191
    invoke-static {v0, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 190
    invoke-virtual {v1, v0}, Lta;->b(Ljava/util/List;)V

    .line 194
    :cond_0
    return-void
.end method

.method public a(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lsx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-virtual {p1}, Lcbi;->be_()I

    move-result v0

    if-lez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {v0}, Lta;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 171
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->l()V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->m()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/app/common/di/scope/InjectionScope;)V

    .line 71
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {v0}, Lta;->f()V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->e:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lta;->a(Lsz;)V

    goto :goto_0
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string/jumbo v0, "presenter_interest_picker"

    return-object v0
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->d()Lcom/twitter/android/smartfollow/f;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/twitter/android/smartfollow/f;->a(JLjava/lang/String;I)V

    .line 104
    return-void
.end method

.method public synthetic e()Lcom/twitter/model/onboarding/a;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->w()Lcom/twitter/model/onboarding/e;

    move-result-object v0

    return-object v0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->n()V

    .line 138
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getFirstVisibleItem()Lcnk;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->k:Lcnk;

    .line 141
    :cond_0
    return-void
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    const-string/jumbo v0, "interest_picker_screen"

    return-object v0
.end method

.method protected p()V
    .locals 5

    .prologue
    .line 80
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->p()V

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lta;->a(Z)V

    .line 84
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {v0, p0}, Lta;->a(Lsz;)V

    .line 85
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lta$a;

    invoke-virtual {v1, v0}, Lta;->a(Lta$a;)V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {v0}, Lta;->a()V

    .line 87
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lta;->b(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->g()Landroid/content/Context;

    move-result-object v0

    .line 89
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 90
    new-instance v2, Lnd;

    const-string/jumbo v3, "welcome_flow"

    invoke-direct {v2, v0, v1, v3}, Lnd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 93
    iget-object v3, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    new-instance v4, Lte;

    invoke-direct {v4, v0, v1, v2}, Lte;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lnd;)V

    invoke-virtual {v3, v4}, Lta;->a(Lte;)V

    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->k:Lcnk;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->a(Lcnk;)V

    .line 96
    return-void
.end method

.method public r()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const-string/jumbo v1, "skip"

    invoke-virtual {v0, v1}, Lta;->a(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v0

    .line 110
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a([Ljava/lang/String;)V

    .line 111
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b([Ljava/lang/String;)V

    .line 112
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->r()V

    .line 113
    return-void
.end method

.method public s()V
    .locals 3

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v1

    .line 118
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const/4 v2, 0x1

    .line 119
    invoke-virtual {v0, v2}, Lta;->a(I)Ljava/util/List;

    move-result-object v0

    .line 120
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a([Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const/4 v2, 0x2

    .line 122
    invoke-virtual {v0, v2}, Lta;->a(I)Ljava/util/List;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b([Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const-string/jumbo v1, "selected"

    invoke-virtual {v0, v1}, Lta;->a(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const/4 v2, 0x0

    .line 126
    invoke-virtual {v1, v2}, Lta;->a(I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lta;->a(Ljava/util/List;)V

    .line 128
    const-string/jumbo v0, "interest_persistence_request_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->u()V

    .line 132
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->s()V

    .line 133
    return-void
.end method

.method protected t()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {v0}, Lta;->c()V

    .line 100
    return-void
.end method

.method protected u()V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 147
    invoke-static {v0}, Lcom/twitter/util/b;->c(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 149
    new-instance v3, Lcom/twitter/model/json/stratostore/JsonInterestSelections;

    invoke-direct {v3}, Lcom/twitter/model/json/stratostore/JsonInterestSelections;-><init>()V

    .line 150
    iget-object v4, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    .line 151
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->e()Lcom/twitter/model/stratostore/SourceLocation;

    move-result-object v5

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-virtual {v4, v5, v0, v1, v2}, Lta;->a(Lcom/twitter/model/stratostore/SourceLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/model/json/stratostore/JsonInterestSelections;->a:Ljava/util/List;

    .line 153
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/a;->d()Lcom/twitter/android/smartfollow/f;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/twitter/android/smartfollow/f;->a(Lcom/twitter/model/json/stratostore/JsonInterestSelections;)V

    .line 155
    :cond_0
    return-void
.end method

.method protected v()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    invoke-virtual {v0}, Lta;->d()V

    .line 159
    return-void
.end method

.method public w()Lcom/twitter/model/onboarding/e;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/model/onboarding/e",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v1, Lcom/twitter/model/onboarding/e;

    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const/4 v3, 0x1

    .line 182
    invoke-virtual {v2, v3}, Lta;->a(I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/smartfollow/interestpicker/a;->j:Lta;

    const/4 v3, 0x2

    .line 183
    invoke-virtual {v2, v3}, Lta;->a(I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-direct {v1, v0}, Lcom/twitter/model/onboarding/e;-><init>(Ljava/util/Set;)V

    .line 181
    return-object v1
.end method
