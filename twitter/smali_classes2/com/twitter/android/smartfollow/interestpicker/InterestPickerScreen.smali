.class public Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;
.super Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Lta$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/smartfollow/BaseSmartFollowScreen",
        "<",
        "Lcom/twitter/android/smartfollow/interestpicker/a;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Lta$a;"
    }
.end annotation


# instance fields
.field private b:Landroid/support/v7/widget/RecyclerView;

.field private c:Lcom/twitter/internal/android/widget/PopupEditText;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/ProgressBar;

.field private f:Landroid/view/View;

.field private g:Z

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->i:Landroid/view/View;

    return-object v0
.end method

.method private c(Z)V
    .locals 4

    .prologue
    .line 189
    iput-boolean p1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->j:Z

    .line 190
    if-eqz p1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    .line 192
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    check-cast v0, Lbac;

    invoke-virtual {v0}, Lbac;->b()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->d:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 193
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    .line 194
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    :goto_0
    return-void

    .line 199
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen$1;-><init>(Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 133
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getPresenter()Lcom/twitter/android/smartfollow/a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestpicker/a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/smartfollow/interestpicker/a;->b(J)V

    .line 141
    return-void
.end method

.method public a(Lcnk;)V
    .locals 3

    .prologue
    .line 209
    iget v0, p1, Lcnk;->e:I

    if-ltz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/FlowLayoutManager;

    iget v1, p1, Lcnk;->e:I

    iget v2, p1, Lcnk;->d:I

    .line 211
    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/FlowLayoutManager;->a(II)V

    .line 213
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->e:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_2

    .line 117
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->g:Z

    .line 129
    :goto_0
    return-void

    .line 121
    :cond_2
    if-eqz p1, :cond_3

    .line 122
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 128
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 125
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 126
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->e:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method

.method protected getFirstVisibleItem()Lcnk;
    .locals 6

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/FlowLayoutManager;

    .line 181
    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/FlowLayoutManager;->a()Landroid/view/View;

    move-result-object v2

    .line 182
    if-eqz v2, :cond_0

    new-instance v1, Lcnk;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    .line 183
    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->getChildItemId(Landroid/view/View;)J

    move-result-wide v4

    .line 184
    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/FlowLayoutManager;->c()I

    move-result v3

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v0

    invoke-direct {v1, v4, v5, v3, v0}, Lcnk;-><init>(JII)V

    move-object v0, v1

    .line 182
    :goto_0
    return-object v0

    .line 184
    :cond_0
    sget-object v0, Lcnk;->a:Lcnk;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 146
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getPresenter()Lcom/twitter/android/smartfollow/a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestpicker/a;

    .line 147
    const v2, 0x7f1303f1

    if-ne v1, v2, :cond_0

    .line 148
    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/interestpicker/a;->v()V

    .line 154
    :goto_0
    return-void

    .line 149
    :cond_0
    const v2, 0x7f130437

    if-ne v1, v2, :cond_1

    .line 150
    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/interestpicker/a;->t()V

    goto :goto_0

    .line 152
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 158
    invoke-static {p0, p0}, Lcom/twitter/util/ui/k;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 159
    invoke-super {p0}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->onDetachedFromWindow()V

    .line 160
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 56
    invoke-super {p0}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->onFinishInflate()V

    .line 57
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 58
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 59
    const v0, 0x7f130435

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    .line 60
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    new-instance v5, Lcom/twitter/internal/android/widget/FlowLayoutManager;

    invoke-direct {v5}, Lcom/twitter/internal/android/widget/FlowLayoutManager;-><init>()V

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 61
    const v0, 0x7f0e02a4

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 62
    iget-object v5, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    new-instance v6, Lbac$a;

    new-instance v7, Lsy$a;

    invoke-direct {v7, v1, v0, v1, v0}, Lsy$a;-><init>(IIII)V

    invoke-direct {v6, v7}, Lbac$a;-><init>(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 65
    const v0, 0x7f130064

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->e:Landroid/widget/ProgressBar;

    .line 66
    const v0, 0x7f130436

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->f:Landroid/view/View;

    .line 67
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->f:Landroid/view/View;

    const v5, 0x7f1303f1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f04016b

    iget-object v6, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    .line 70
    invoke-virtual {v0, v5, v6, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->d:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->d:Landroid/view/View;

    const v5, 0x7f130437

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v5, 0x1

    new-array v5, v5, [Landroid/text/InputFilter;

    new-instance v6, Landroid/text/InputFilter$LengthFilter;

    const/16 v7, 0x19

    invoke-direct {v6, v7}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v6, v5, v1

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/PopupEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 74
    const v0, 0x7f0a0425

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/twitter/android/k;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->h:Landroid/view/View;

    .line 75
    const v0, 0x7f130479

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->i:Landroid/view/View;

    .line 77
    iget-object v3, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->e:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->g:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v3, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->g:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 79
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 80
    return-void

    :cond_0
    move v0, v2

    .line 77
    goto :goto_0

    :cond_1
    move v2, v1

    .line 78
    goto :goto_1
.end method

.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    const v2, 0x7f0e0514

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, v1, v0

    .line 167
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->getHeight()I

    move-result v1

    .line 168
    if-lez v1, :cond_1

    if-ge v1, v0, :cond_1

    const/4 v0, 0x1

    .line 169
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->j:Z

    if-eq v0, v1, :cond_0

    .line 170
    invoke-direct {p0, v0}, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->c(Z)V

    .line 172
    :cond_0
    return-void

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAdapterAndAttachHeaders(Lsy;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 101
    if-eqz p1, :cond_0

    .line 102
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 104
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 105
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1, v0}, Lsy;->a(Ljava/util/List;)V

    .line 107
    :cond_0
    return-void
.end method

.method public setSeamfulSignupHeader(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 85
    return-void
.end method

.method public setSearchHint(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/PopupEditText;->setHint(I)V

    .line 112
    return-void
.end method

.method public setSignupHeader(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 90
    return-void
.end method

.method public setupSearchController(Lte;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {p1, v0}, Lte;->a(Lcom/twitter/internal/android/widget/PopupEditText;)V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestpicker/InterestPickerScreen;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    return-void
.end method
