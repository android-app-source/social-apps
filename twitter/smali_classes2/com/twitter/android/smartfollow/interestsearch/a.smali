.class public final Lcom/twitter/android/smartfollow/interestsearch/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/smartfollow/interestsearch/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/smartfollow/interestsearch/a$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lnd;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lte;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/twitter/android/smartfollow/interestsearch/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/smartfollow/interestsearch/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/smartfollow/interestsearch/a$a;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget-boolean v0, Lcom/twitter/android/smartfollow/interestsearch/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/smartfollow/interestsearch/a;->a(Lcom/twitter/android/smartfollow/interestsearch/a$a;)V

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/smartfollow/interestsearch/a$a;Lcom/twitter/android/smartfollow/interestsearch/a$1;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/android/smartfollow/interestsearch/a;-><init>(Lcom/twitter/android/smartfollow/interestsearch/a$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/android/smartfollow/interestsearch/a$a;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/android/smartfollow/interestsearch/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/smartfollow/interestsearch/a$a;-><init>(Lcom/twitter/android/smartfollow/interestsearch/a$1;)V

    return-object v0
.end method

.method private a(Lcom/twitter/android/smartfollow/interestsearch/a$a;)V
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/twitter/android/smartfollow/interestsearch/a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/interestsearch/a$1;-><init>(Lcom/twitter/android/smartfollow/interestsearch/a;Lcom/twitter/android/smartfollow/interestsearch/a$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->b:Lcta;

    .line 64
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->b:Lcta;

    .line 66
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 65
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->c:Lcta;

    .line 68
    new-instance v0, Lcom/twitter/android/smartfollow/interestsearch/a$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/interestsearch/a$2;-><init>(Lcom/twitter/android/smartfollow/interestsearch/a;Lcom/twitter/android/smartfollow/interestsearch/a$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->d:Lcta;

    .line 81
    new-instance v0, Lcom/twitter/android/smartfollow/interestsearch/a$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/smartfollow/interestsearch/a$3;-><init>(Lcom/twitter/android/smartfollow/interestsearch/a;Lcom/twitter/android/smartfollow/interestsearch/a$a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->e:Lcta;

    .line 94
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->d:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->e:Lcta;

    .line 96
    invoke-static {v0, v1}, Lcom/twitter/android/smartfollow/interestsearch/e;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 95
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->f:Lcta;

    .line 99
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->d:Lcta;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->e:Lcta;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->f:Lcta;

    .line 101
    invoke-static {v0, v1, v2}, Lcom/twitter/android/smartfollow/interestsearch/d;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 100
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->g:Lcta;

    .line 103
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lte;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/a;->g:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lte;

    return-object v0
.end method
