.class public Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Lsy$b;
.implements Lte$a;


# instance fields
.field protected a:Lsy;

.field private b:Lte;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltg;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 41
    new-instance v0, Lsy;

    invoke-direct {v0, p0}, Lsy;-><init>(Lsy$b;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a:Lsy;

    .line 49
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->d:J

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 167
    new-instance v0, Ltg$a;

    invoke-direct {v0}, Ltg$a;-><init>()V

    .line 168
    invoke-virtual {v0, p1}, Ltg$a;->a(Ljava/lang/String;)Lsx$a;

    move-result-object v0

    check-cast v0, Ltg$a;

    .line 169
    invoke-virtual {v0, p2}, Ltg$a;->b(Ljava/lang/String;)Lsx$a;

    move-result-object v0

    check-cast v0, Ltg$a;

    iget-wide v2, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->d:J

    .line 170
    invoke-virtual {v0, v2, v3}, Ltg$a;->b(J)Lsx$a;

    move-result-object v0

    check-cast v0, Ltg$a;

    const/4 v1, 0x2

    .line 171
    invoke-virtual {v0, v1}, Ltg$a;->a(I)Ltg$a;

    move-result-object v0

    .line 172
    invoke-virtual {v0, v4}, Ltg$a;->a(Z)Ltg$a;

    move-result-object v0

    .line 173
    invoke-virtual {v0, v4}, Ltg$a;->c(Z)Ltg$a;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Ltg$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltg;

    .line 175
    invoke-direct {p0, v0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a(Ltg;)V

    .line 176
    return-void
.end method

.method private a(Ljava/lang/String;Ltg;)V
    .locals 4

    .prologue
    .line 160
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "smart_follow_flow"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "interest_picker_search"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    aput-object p1, v1, v2

    .line 161
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    .line 163
    invoke-interface {v1, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {p2, v1}, Lsw;->a(Ltg;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 160
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 164
    return-void
.end method

.method private a(Ltg;)V
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    invoke-static {v0, p1}, Lsw;->a(Ljava/util/List;Lsx;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    .line 155
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a:Lsy;

    new-instance v2, Lcbl;

    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-direct {v2, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-virtual {v1, v2}, Lsy;->a(Lcbi;)V

    .line 156
    const-string/jumbo v0, "select"

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a(Ljava/lang/String;Ltg;)V

    .line 157
    return-void
.end method


# virtual methods
.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->d()Lcom/twitter/android/smartfollow/interestsearch/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 84
    const v0, 0x7f040170

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 85
    const v0, 0x7f130437

    .line 86
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    .line 87
    const v1, 0x7f130435

    .line 88
    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0e02a4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 91
    new-instance v4, Lcom/twitter/internal/android/widget/FlowLayoutManager;

    invoke-direct {v4}, Lcom/twitter/internal/android/widget/FlowLayoutManager;-><init>()V

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 92
    new-instance v4, Lbac$a;

    new-instance v5, Lsy$a;

    invoke-direct {v5, v6, v2, v6, v2}, Lsy$a;-><init>(IIII)V

    invoke-direct {v4, v5}, Lbac$a;-><init>(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 95
    iget-object v4, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a:Lsy;

    new-instance v5, Lcbl;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    invoke-static {v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-direct {v5, v2}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-virtual {v4, v5}, Lsy;->a(Lcbi;)V

    .line 96
    iget-object v2, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a:Lsy;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 98
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->b:Lte;

    invoke-virtual {v1, p0}, Lte;->a(Lte$a;)V

    .line 99
    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->b:Lte;

    invoke-virtual {v1, v0}, Lte;->a(Lcom/twitter/internal/android/widget/PopupEditText;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->b:Lte;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lte;->b(Ljava/lang/String;)V

    .line 101
    return-object v3
.end method

.method protected a(Lank;)Lcom/twitter/android/smartfollow/interestsearch/f;
    .locals 2

    .prologue
    .line 115
    invoke-static {}, Lcom/twitter/android/smartfollow/interestsearch/a;->a()Lcom/twitter/android/smartfollow/interestsearch/a$a;

    move-result-object v0

    .line 116
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/interestsearch/a$a;->a(Lamu;)Lcom/twitter/android/smartfollow/interestsearch/a$a;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/interestsearch/a$a;->a()Lcom/twitter/android/smartfollow/interestsearch/f;

    move-result-object v0

    .line 114
    return-object v0
.end method

.method public a(Lcom/twitter/android/util/CategoryListItem;)V
    .locals 2

    .prologue
    .line 133
    invoke-virtual {p1}, Lcom/twitter/android/util/CategoryListItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/util/CategoryListItem;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public a(Lsx;)V
    .locals 1

    .prologue
    .line 124
    instance-of v0, p1, Ltg;

    if-eqz v0, :cond_0

    .line 125
    check-cast p1, Ltg;

    .line 126
    iget-boolean v0, p1, Ltg;->h:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "select"

    .line 127
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a(Ljava/lang/String;Ltg;)V

    .line 129
    :cond_0
    return-void

    .line 126
    :cond_1
    const-string/jumbo v0, "deselect"

    goto :goto_0
.end method

.method protected synthetic c(Lank;)Lcom/twitter/app/common/abs/c;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a(Lank;)Lcom/twitter/android/smartfollow/interestsearch/f;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method protected synthetic d(Lank;)Lann;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->a(Lank;)Lcom/twitter/android/smartfollow/interestsearch/f;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/twitter/android/smartfollow/interestsearch/b;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/smartfollow/interestsearch/b;->a(Landroid/os/Bundle;)Lcom/twitter/android/smartfollow/interestsearch/b;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 144
    iget-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 145
    instance-of v1, v0, Ltg;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ltg;

    iget-boolean v1, v1, Ltg;->h:Z

    if-eqz v1, :cond_0

    .line 146
    check-cast v0, Ltg;

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 149
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/interestsearch/f;

    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->d()Lcom/twitter/android/smartfollow/interestsearch/b;

    move-result-object v1

    .line 68
    invoke-interface {v0}, Lcom/twitter/android/smartfollow/interestsearch/f;->d()Lte;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->b:Lte;

    .line 69
    invoke-virtual {v1}, Lcom/twitter/android/smartfollow/interestsearch/b;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->e:Ljava/lang/String;

    .line 70
    invoke-virtual {v1}, Lcom/twitter/android/smartfollow/interestsearch/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->d:J

    .line 72
    if-eqz p1, :cond_0

    .line 73
    const-string/jumbo v0, "state_search_terms"

    sget-object v1, Ltg;->f:Ltg$b;

    .line 74
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 73
    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 75
    if-eqz v0, :cond_0

    .line 76
    iput-object v0, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    .line 79
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 106
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 107
    const-string/jumbo v0, "state_search_terms"

    iget-object v1, p0, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchFragment;->c:Ljava/util/List;

    sget-object v2, Ltg;->f:Ltg$b;

    .line 108
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 107
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 109
    return-void
.end method
