.class public Lcom/twitter/android/smartfollow/e$c;
.super Lcom/twitter/android/twitterflows/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/smartfollow/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/android/twitterflows/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 20
    const v0, 0x7f0403c9

    return v0
.end method

.method public a(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)Lcom/twitter/android/twitterflows/d;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/e$c;->b(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)Lcom/twitter/android/twitterflows/d;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)Lcom/twitter/android/twitterflows/d;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/e$c;->c(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/twitter/android/smartfollow/e$a;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/e$a;-><init>()V

    .line 46
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/e$c;->d()Lcom/twitter/android/twitterflows/d;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string/jumbo v0, "presenter_waiting_for_people_discovery"

    return-object v0
.end method

.method protected c(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)Z
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c()Z

    move-result v0

    return v0
.end method

.method protected d()Lcom/twitter/android/twitterflows/d;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/twitter/android/smartfollow/e$b;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/e$b;-><init>()V

    return-object v0
.end method
