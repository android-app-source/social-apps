.class public Lcom/twitter/android/smartfollow/waitingforsuggestions/a;
.super Lcom/twitter/android/smartfollow/a;
.source "Twttr"


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/smartfollow/waitingforsuggestions/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/smartfollow/a",
        "<",
        "Lcom/twitter/app/common/util/StateSaver",
        "<",
        "Lcom/twitter/android/smartfollow/waitingforsuggestions/a;",
        ">;",
        "Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;",
        ">;"
    }
.end annotation


# static fields
.field protected static final j:J

.field private static final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected k:Ljava/lang/String;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected l:I
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected m:I
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected n:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected o:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected p:Landroid/content/BroadcastReceiver;

.field private final r:Laem;

.field private s:Lrx/j;

.field private final t:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 45
    const-string/jumbo v0, "smart_nux_smart_follow_timings_message_display_duration"

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->j:J

    .line 47
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    const v1, 0x7f0a08e3

    .line 48
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    const v1, 0x7f0a08e4

    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sput-object v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->q:Ljava/util/List;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/twitter/app/common/util/StateSaver;Laem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/waitingforsuggestions/a;",
            ">;",
            "Laem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/a;-><init>()V

    .line 52
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->k:Ljava/lang/String;

    .line 68
    iput-object p1, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->t:Landroid/os/Handler;

    .line 69
    invoke-virtual {p2, p0}, Lcom/twitter/app/common/util/StateSaver;->a(Ljava/lang/Object;)V

    .line 70
    iput-object p3, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->r:Laem;

    .line 71
    return-void
.end method

.method private A()Z
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/smartfollow/waitingforsuggestions/a;)Lcom/twitter/android/smartfollow/SmartFollowFlowData;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->s:Lrx/j;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->s:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->s:Lrx/j;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->r:Laem;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 115
    :cond_1
    return-void
.end method

.method protected a(Ljava/lang/Runnable;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 169
    iget v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->l:I

    add-int/lit8 v0, v0, 0x1

    .line 170
    iget v1, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->m:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->m:I

    .line 172
    iget-boolean v1, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->n:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->m:I

    sget-object v2, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->A()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->s()V

    .line 182
    :goto_0
    return-void

    .line 175
    :cond_0
    sget-object v1, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->l:I

    .line 176
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;

    sget-object v1, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->q:Ljava/util/List;

    iget v2, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->l:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;->setDescription(I)V

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->t:Landroid/os/Handler;

    sget-wide v2, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->j:J

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string/jumbo v0, "presenter_waiting_for_people_discovery"

    return-object v0
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->u()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->s:Lrx/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->s:Lrx/j;

    invoke-interface {v0}, Lrx/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 209
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->n()V

    .line 210
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->t:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 211
    return-void
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    const-string/jumbo v0, "waiting_for_suggestions_screen"

    return-object v0
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->p()V

    .line 127
    iget-boolean v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->o:Z

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->y()V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->v()V

    goto :goto_0
.end method

.method protected t()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->r:Laem;

    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->f()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Laem;->a(Ljava/util/Map;)Lrx/c;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/smartfollow/waitingforsuggestions/a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a$1;-><init>(Lcom/twitter/android/smartfollow/waitingforsuggestions/a;)V

    .line 87
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->s:Lrx/j;

    .line 100
    return-void
.end method

.method public u()Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/waitingforsuggestions/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsPresenterSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsPresenterSavedState;-><init>(Lcom/twitter/android/smartfollow/waitingforsuggestions/a;)V

    return-object v0
.end method

.method protected v()V
    .locals 4

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->z()V

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->w()Z

    .line 138
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;

    sget-object v1, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->q:Ljava/util/List;

    iget v2, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->l:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;->setDescription(I)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->t:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->t:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/smartfollow/waitingforsuggestions/a$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a$2;-><init>(Lcom/twitter/android/smartfollow/waitingforsuggestions/a;)V

    sget-wide v2, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->j:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 149
    return-void
.end method

.method protected w()Z
    .locals 3

    .prologue
    .line 152
    invoke-static {}, Lcom/twitter/android/ContactsUploadService;->c()Z

    move-result v0

    .line 154
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->e()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->n:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->t()V

    .line 156
    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->p:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 158
    new-instance v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a$a;-><init>(Lcom/twitter/android/smartfollow/waitingforsuggestions/a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->p:Landroid/content/BroadcastReceiver;

    .line 159
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 160
    const-string/jumbo v1, "upload_success_broadcast"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->g()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected x()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->t:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->o:Z

    .line 188
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->l:I

    .line 189
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->y()V

    .line 190
    return-void
.end method

.method protected y()V
    .locals 2

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;->a()V

    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;->setView(Z)V

    .line 198
    :cond_0
    return-void
.end method

.method protected z()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 201
    iput-boolean v1, p0, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->o:Z

    .line 202
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/waitingforsuggestions/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/waitingforsuggestions/WaitingForSuggestionsScreen;->setView(Z)V

    .line 205
    :cond_0
    return-void
.end method
