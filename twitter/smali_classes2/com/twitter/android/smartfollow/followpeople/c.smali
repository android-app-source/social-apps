.class public final Lcom/twitter/android/smartfollow/followpeople/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/smartfollow/followpeople/b;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/twitter/android/smartfollow/followpeople/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/smartfollow/followpeople/c;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcnz;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/c;",
            ">;",
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;",
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;",
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;",
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/b;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/c;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/c;->b:Lcta;

    .line 42
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/c;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/smartfollow/followpeople/c;->c:Lcta;

    .line 44
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/c;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/smartfollow/followpeople/c;->d:Lcta;

    .line 46
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/c;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/smartfollow/followpeople/c;->e:Lcta;

    .line 48
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/c;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_4
    iput-object p5, p0, Lcom/twitter/android/smartfollow/followpeople/c;->f:Lcta;

    .line 50
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/c;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_5
    iput-object p6, p0, Lcom/twitter/android/smartfollow/followpeople/c;->g:Lcta;

    .line 52
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcnz;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/c;",
            ">;",
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;",
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;",
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;>;",
            "Lcta",
            "<",
            "Lmf",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/b;",
            ">;>;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/smartfollow/followpeople/c;-><init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/smartfollow/followpeople/b;
    .locals 7

    .prologue
    .line 56
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/b;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/c;->b:Lcta;

    .line 57
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcnz;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/followpeople/c;->c:Lcta;

    .line 58
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/model/c;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/followpeople/c;->d:Lcta;

    .line 59
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmd;

    iget-object v4, p0, Lcom/twitter/android/smartfollow/followpeople/c;->e:Lcta;

    .line 60
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmd;

    iget-object v5, p0, Lcom/twitter/android/smartfollow/followpeople/c;->f:Lcta;

    .line 61
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmf;

    iget-object v6, p0, Lcom/twitter/android/smartfollow/followpeople/c;->g:Lcta;

    .line 62
    invoke-interface {v6}, Lcta;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmf;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/smartfollow/followpeople/b;-><init>(Lcnz;Lcom/twitter/analytics/model/c;Lmd;Lmd;Lmf;Lmf;)V

    .line 56
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/c;->a()Lcom/twitter/android/smartfollow/followpeople/b;

    move-result-object v0

    return-object v0
.end method
