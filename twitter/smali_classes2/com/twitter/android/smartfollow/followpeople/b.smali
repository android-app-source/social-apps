.class public Lcom/twitter/android/smartfollow/followpeople/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcnz;

.field private final b:Lmf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lmf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmf",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lmd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lmd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmd",
            "<",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/analytics/model/a;

.field private final g:Lcom/twitter/analytics/model/a;

.field private final h:Lcom/twitter/analytics/model/a;

.field private final i:Lcom/twitter/analytics/model/a;


# direct methods
.method public constructor <init>(Lcnz;Lcom/twitter/analytics/model/c;Lmd;Lmd;Lmf;Lmf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcnz;",
            "Lcom/twitter/analytics/model/c;",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;",
            "Lmd",
            "<",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;",
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;",
            "Lmf",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/b;->a:Lcnz;

    .line 52
    iput-object p3, p0, Lcom/twitter/android/smartfollow/followpeople/b;->d:Lmd;

    .line 53
    iput-object p4, p0, Lcom/twitter/android/smartfollow/followpeople/b;->e:Lmd;

    .line 54
    iput-object p5, p0, Lcom/twitter/android/smartfollow/followpeople/b;->b:Lmf;

    .line 55
    iput-object p6, p0, Lcom/twitter/android/smartfollow/followpeople/b;->c:Lmf;

    .line 56
    const-string/jumbo v0, "follow_people_screen"

    const-string/jumbo v1, "category"

    const-string/jumbo v2, "select"

    invoke-static {p2, v0, v1, v2}, Lcom/twitter/analytics/model/a;->a(Lcom/twitter/analytics/model/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->f:Lcom/twitter/analytics/model/a;

    .line 57
    const-string/jumbo v0, "follow_people_screen"

    const-string/jumbo v1, "category"

    const-string/jumbo v2, "unselect"

    .line 58
    invoke-static {p2, v0, v1, v2}, Lcom/twitter/analytics/model/a;->a(Lcom/twitter/analytics/model/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->g:Lcom/twitter/analytics/model/a;

    .line 59
    const-string/jumbo v0, "follow_people_screen"

    const-string/jumbo v1, "recommendation"

    const-string/jumbo v2, "select"

    invoke-static {p2, v0, v1, v2}, Lcom/twitter/analytics/model/a;->a(Lcom/twitter/analytics/model/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->h:Lcom/twitter/analytics/model/a;

    .line 60
    const-string/jumbo v0, "follow_people_screen"

    const-string/jumbo v1, "recommendation"

    const-string/jumbo v2, "unselect"

    .line 61
    invoke-static {p2, v0, v1, v2}, Lcom/twitter/analytics/model/a;->a(Lcom/twitter/analytics/model/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->i:Lcom/twitter/analytics/model/a;

    .line 62
    return-void
.end method

.method private a(Lcom/twitter/analytics/model/a;Lcom/twitter/analytics/model/ScribeItem;)V
    .locals 4

    .prologue
    .line 90
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/b;->a:Lcnz;

    invoke-virtual {v1}, Lcnz;->b()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 91
    invoke-virtual {v0, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 92
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/a;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 93
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 94
    return-void
.end method

.method private a(Lcom/twitter/android/people/adapters/b$k;Lcom/twitter/analytics/model/a;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->e:Lmd;

    invoke-interface {v0, p1}, Lmd;->a(Ljava/lang/Object;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/smartfollow/followpeople/b;->a(Lcom/twitter/analytics/model/a;Lcom/twitter/analytics/model/ScribeItem;)V

    .line 87
    return-void
.end method

.method private a(Lcom/twitter/model/people/b;Lcom/twitter/analytics/model/a;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->d:Lmd;

    invoke-interface {v0, p1}, Lmd;->a(Ljava/lang/Object;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/smartfollow/followpeople/b;->a(Lcom/twitter/analytics/model/a;Lcom/twitter/analytics/model/ScribeItem;)V

    .line 79
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/people/adapters/b$k;Z)V
    .locals 1

    .prologue
    .line 82
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->h:Lcom/twitter/analytics/model/a;

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/smartfollow/followpeople/b;->a(Lcom/twitter/android/people/adapters/b$k;Lcom/twitter/analytics/model/a;)V

    .line 83
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->i:Lcom/twitter/analytics/model/a;

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/people/adapters/b;)V
    .locals 2

    .prologue
    .line 65
    instance-of v0, p1, Lcom/twitter/android/people/adapters/b$k;

    if-eqz v0, :cond_0

    .line 66
    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/b;->b:Lmf;

    move-object v0, p1

    check-cast v0, Lcom/twitter/android/people/adapters/b$k;

    invoke-virtual {v1, v0}, Lmf;->a(Ljava/lang/Object;)V

    .line 68
    :cond_0
    instance-of v0, p1, Lcom/twitter/android/people/adapters/viewbinders/p;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->c:Lmf;

    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/p;

    invoke-interface {p1}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmf;->a(Ljava/lang/Object;)V

    .line 71
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/model/people/b;Z)V
    .locals 1

    .prologue
    .line 74
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->f:Lcom/twitter/analytics/model/a;

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/smartfollow/followpeople/b;->a(Lcom/twitter/model/people/b;Lcom/twitter/analytics/model/a;)V

    .line 75
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/b;->g:Lcom/twitter/analytics/model/a;

    goto :goto_0
.end method
