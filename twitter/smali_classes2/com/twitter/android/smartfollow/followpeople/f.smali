.class public Lcom/twitter/android/smartfollow/followpeople/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lmd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lmd",
        "<",
        "Lcom/twitter/model/people/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/model/ScribeItem;
    .locals 3

    .prologue
    .line 23
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 24
    new-instance v0, Lcom/twitter/analytics/feature/model/c$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/c$a;-><init>()V

    .line 25
    invoke-interface {p1}, Lcom/twitter/model/people/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/c$a;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/c$a;

    move-result-object v0

    .line 26
    invoke-interface {p1}, Lcom/twitter/model/people/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/c$a;->b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/c$a;

    move-result-object v0

    .line 27
    invoke-interface {p1}, Lcom/twitter/model/people/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/c$a;->c(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/c$a;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/c$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/c;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->au:Lcom/twitter/analytics/feature/model/c;

    .line 29
    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lcom/twitter/model/people/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/followpeople/f;->a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method
