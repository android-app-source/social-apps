.class public final Lcom/twitter/android/smartfollow/followpeople/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/smartfollow/followpeople/d;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/twitter/android/smartfollow/followpeople/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/smartfollow/followpeople/e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcnz;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/c;",
            ">;",
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/e;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/e;->b:Lcta;

    .line 29
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/e;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/smartfollow/followpeople/e;->c:Lcta;

    .line 31
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/e;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/smartfollow/followpeople/e;->d:Lcta;

    .line 33
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcnz;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/c;",
            ">;",
            "Lcta",
            "<",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/e;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/smartfollow/followpeople/e;-><init>(Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/smartfollow/followpeople/d;
    .locals 4

    .prologue
    .line 37
    new-instance v3, Lcom/twitter/android/smartfollow/followpeople/d;

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/e;->b:Lcta;

    .line 38
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnz;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/e;->c:Lcta;

    .line 39
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/model/c;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/followpeople/e;->d:Lcta;

    .line 40
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmd;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/android/smartfollow/followpeople/d;-><init>(Lcnz;Lcom/twitter/analytics/model/c;Lmd;)V

    .line 37
    return-object v3
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/e;->a()Lcom/twitter/android/smartfollow/followpeople/d;

    move-result-object v0

    return-object v0
.end method
