.class public Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;
.super Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/smartfollow/BaseSmartFollowScreen",
        "<",
        "Lcom/twitter/android/smartfollow/followpeople/h;",
        ">;",
        "Landroid/widget/AbsListView$OnScrollListener;"
    }
.end annotation


# instance fields
.field private b:Lcom/twitter/refresh/widget/RefreshableListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method private a()Landroid/view/View;
    .locals 3

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    const v1, 0x7f0a08da

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 90
    const v2, 0x7f0a08d9

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-static {v0, v1, v2}, Lcom/twitter/android/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/android/smartfollow/followpeople/a;Lcnk;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 67
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-direct {p0}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->a()Landroid/view/View;

    move-result-object v1

    const-string/jumbo v2, "FollowPeoplePresenterHeader"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->setHeaderDividersEnabled(Z)V

    .line 70
    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v2, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen$1;-><init>(Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;Lcom/twitter/android/smartfollow/followpeople/a;)V

    invoke-static {p1, v1, v0, v2}, Lcom/twitter/android/widget/ag;->a(Lcom/twitter/android/widget/ad;Landroid/view/ViewGroup;Landroid/view/View;Lcom/twitter/android/widget/ae;)V

    .line 80
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0, p0}, Lcom/twitter/refresh/widget/RefreshableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 81
    if-eqz p2, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    iget v1, p2, Lcnk;->e:I

    iget v2, p2, Lcnk;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/refresh/widget/RefreshableListView;->setSelectionFromTop(II)V

    .line 84
    :cond_0
    return-void
.end method

.method protected getFirstVisibleItem()Lcnk;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 45
    iget-object v3, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    .line 46
    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 47
    invoke-virtual {v3}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    .line 52
    if-ge v1, v0, :cond_0

    .line 54
    sub-int v1, v0, v1

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 60
    :goto_0
    new-instance v4, Lcnk;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v6

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_1
    invoke-direct {v4, v6, v7, v1, v0}, Lcnk;-><init>(JII)V

    return-object v4

    .line 57
    :cond_0
    invoke-virtual {v3, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move v8, v1

    move-object v1, v0

    move v0, v8

    goto :goto_0

    :cond_1
    move v1, v2

    .line 60
    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->onFinishInflate()V

    .line 36
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    .line 37
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v0}, Lcom/twitter/android/widget/ag;->a(Landroid/view/View;)Lcom/twitter/android/widget/af;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    .line 103
    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->b:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->getHeaderViewsCount()I

    move-result v1

    invoke-interface {v0, p2, v1}, Lcom/twitter/android/widget/af;->a(II)V

    .line 105
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method
