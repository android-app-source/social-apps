.class public final Lcom/twitter/android/smartfollow/followpeople/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/smartfollow/followpeople/h;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/twitter/android/smartfollow/followpeople/i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/smartfollow/followpeople/i;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;>;",
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/i;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/i;->b:Lcsd;

    .line 31
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/i;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/smartfollow/followpeople/i;->c:Lcta;

    .line 33
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/i;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/smartfollow/followpeople/i;->d:Lcta;

    .line 35
    sget-boolean v0, Lcom/twitter/android/smartfollow/followpeople/i;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/smartfollow/followpeople/i;->e:Lcta;

    .line 37
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;>;",
            "Lcta",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/b;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/i;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/twitter/android/smartfollow/followpeople/i;-><init>(Lcsd;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/smartfollow/followpeople/h;
    .locals 5

    .prologue
    .line 41
    iget-object v3, p0, Lcom/twitter/android/smartfollow/followpeople/i;->b:Lcsd;

    new-instance v4, Lcom/twitter/android/smartfollow/followpeople/h;

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/i;->c:Lcta;

    .line 44
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/i;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/i;->d:Lcta;

    .line 45
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/util/StateSaver;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/followpeople/i;->e:Lcta;

    .line 46
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/smartfollow/followpeople/b;

    invoke-direct {v4, v0, v1, v2}, Lcom/twitter/android/smartfollow/followpeople/h;-><init>(Lcom/twitter/android/people/i;Lcom/twitter/app/common/util/StateSaver;Lcom/twitter/android/smartfollow/followpeople/b;)V

    .line 41
    invoke-static {v3, v4}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/h;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/i;->a()Lcom/twitter/android/smartfollow/followpeople/h;

    move-result-object v0

    return-object v0
.end method
