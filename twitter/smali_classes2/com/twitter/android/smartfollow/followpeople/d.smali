.class public Lcom/twitter/android/smartfollow/followpeople/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lme;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lme",
        "<",
        "Lcom/twitter/model/people/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcnz;

.field private final b:Lcom/twitter/analytics/model/a;

.field private final c:Lmd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcnz;Lcom/twitter/analytics/model/c;Lmd;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcnz;",
            "Lcom/twitter/analytics/model/c;",
            "Lmd",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/d;->a:Lcnz;

    .line 33
    const-string/jumbo v0, "follow_people_screen"

    const-string/jumbo v1, "category"

    const-string/jumbo v2, "impression"

    invoke-static {p2, v0, v1, v2}, Lcom/twitter/analytics/model/a;->a(Lcom/twitter/analytics/model/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/d;->b:Lcom/twitter/analytics/model/a;

    .line 34
    iput-object p3, p0, Lcom/twitter/android/smartfollow/followpeople/d;->c:Lmd;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/d;->a:Lcnz;

    invoke-virtual {v1}, Lcnz;->b()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/d;->b:Lcom/twitter/analytics/model/a;

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/a;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/d;->c:Lmd;

    .line 42
    invoke-interface {v1, p1}, Lmd;->a(Ljava/lang/Object;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lcom/twitter/model/people/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/followpeople/d;->a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    return-object v0
.end method
