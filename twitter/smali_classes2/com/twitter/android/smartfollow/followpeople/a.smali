.class public Lcom/twitter/android/smartfollow/followpeople/a;
.super Lcjr;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ad;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/smartfollow/followpeople/a$d;,
        Lcom/twitter/android/smartfollow/followpeople/a$a;,
        Lcom/twitter/android/smartfollow/followpeople/a$c;,
        Lcom/twitter/android/smartfollow/followpeople/a$e;,
        Lcom/twitter/android/smartfollow/followpeople/a$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/android/people/adapters/b;",
        ">;",
        "Lcom/twitter/android/widget/ad;"
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/smartfollow/followpeople/a$a;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Lcom/twitter/android/smartfollow/followpeople/b;

.field private g:Lcom/twitter/android/smartfollow/followpeople/a$d;

.field private h:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "PYMK"

    const v2, 0x7f0a08d8

    .line 71
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "LOCATION_GEO"

    const v2, 0x7f0a08d7

    .line 72
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "TWITTER_HISTORY"

    const v2, 0x7f0a08db

    .line 73
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/android/smartfollow/followpeople/a;->b:Ljava/util/Map;

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/smartfollow/followpeople/a$a;Ljava/util/Set;Lcom/twitter/android/smartfollow/followpeople/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/android/smartfollow/followpeople/a$a;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/twitter/android/smartfollow/followpeople/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 77
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->a:Ljava/util/Map;

    .line 90
    iput-object p2, p0, Lcom/twitter/android/smartfollow/followpeople/a;->c:Lcom/twitter/android/smartfollow/followpeople/a$a;

    .line 91
    iput-object p3, p0, Lcom/twitter/android/smartfollow/followpeople/a;->d:Ljava/util/Set;

    .line 92
    new-instance v0, Landroid/support/v7/view/ContextThemeWrapper;

    const v1, 0x7f0d032c

    invoke-direct {v0, p1, v1}, Landroid/support/v7/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->e:Landroid/view/LayoutInflater;

    .line 93
    iput-object p4, p0, Lcom/twitter/android/smartfollow/followpeople/a;->f:Lcom/twitter/android/smartfollow/followpeople/b;

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/smartfollow/followpeople/a;)Lcom/twitter/android/smartfollow/followpeople/a$d;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->g:Lcom/twitter/android/smartfollow/followpeople/a$d;

    return-object v0
.end method

.method private a(JZ)V
    .locals 3

    .prologue
    .line 391
    if-eqz p3, :cond_0

    .line 392
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->d:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 396
    :goto_0
    return-void

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->d:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/smartfollow/followpeople/a;Lcom/twitter/ui/user/UserView;Lcom/twitter/android/people/adapters/b$k;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Lcom/twitter/ui/user/UserView;Lcom/twitter/android/people/adapters/b$k;)V

    return-void
.end method

.method private a(Lcom/twitter/ui/user/UserView;Lcom/twitter/android/people/adapters/b$k;)V
    .locals 4

    .prologue
    .line 371
    iget-object v0, p1, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 372
    invoke-virtual {p2}, Lcom/twitter/android/people/adapters/b$k;->a()J

    move-result-wide v2

    iget-object v1, p1, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-direct {p0, v2, v3, v1}, Lcom/twitter/android/smartfollow/followpeople/a;->a(JZ)V

    .line 373
    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/a;->f:Lcom/twitter/android/smartfollow/followpeople/b;

    invoke-virtual {v1, p2, v0}, Lcom/twitter/android/smartfollow/followpeople/b;->a(Lcom/twitter/android/people/adapters/b$k;Z)V

    .line 375
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/a$c;

    iget-object v1, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->e:Ljava/lang/String;

    .line 377
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/a$c;

    iget-object v0, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->e:Ljava/lang/String;

    .line 381
    invoke-static {v1, v0}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->h:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Ljava/lang/String;Landroid/view/View;)V

    .line 386
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/a;->notifyDataSetChanged()V

    .line 387
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->c:Lcom/twitter/android/smartfollow/followpeople/a$a;

    invoke-interface {v0}, Lcom/twitter/android/smartfollow/followpeople/a$a;->bb_()V

    .line 388
    return-void
.end method

.method private static c(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    instance-of v0, p0, Lcom/twitter/android/people/adapters/viewbinders/p;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/twitter/android/people/adapters/viewbinders/p;

    .line 209
    invoke-interface {p0}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    iget-object v0, v0, Lcom/twitter/model/people/g;->a:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    iget-object v0, v0, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    .line 208
    :goto_0
    return-object v0

    .line 209
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method private static d(Lcom/twitter/android/people/adapters/b;)J
    .locals 2

    .prologue
    .line 322
    instance-of v0, p0, Lcom/twitter/android/people/adapters/b$k;

    if-eqz v0, :cond_0

    .line 323
    check-cast p0, Lcom/twitter/android/people/adapters/b$k;

    iget-object v0, p0, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    iget-object v0, v0, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 325
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/android/people/adapters/b;)I
    .locals 1

    .prologue
    .line 303
    instance-of v0, p1, Lcom/twitter/android/people/adapters/b$i;

    if-eqz v0, :cond_0

    .line 304
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    .line 305
    :cond_0
    instance-of v0, p1, Lcom/twitter/android/people/adapters/b$e;

    if-eqz v0, :cond_1

    .line 306
    const/4 v0, 0x2

    goto :goto_0

    .line 307
    :cond_1
    instance-of v0, p1, Lcom/twitter/android/people/adapters/b$j;

    if-eqz v0, :cond_2

    .line 308
    const/4 v0, 0x3

    goto :goto_0

    .line 309
    :cond_2
    instance-of v0, p1, Lcom/twitter/android/people/adapters/b$k;

    if-eqz v0, :cond_3

    .line 310
    const/4 v0, 0x0

    goto :goto_0

    .line 312
    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 46
    check-cast p1, Lcom/twitter/android/people/adapters/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Lcom/twitter/android/people/adapters/b;)I

    move-result v0

    return v0
.end method

.method public a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/a;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 234
    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/a;->c(Landroid/view/View;)V

    .line 235
    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/android/people/adapters/b;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 104
    invoke-virtual {p0, p2}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Lcom/twitter/android/people/adapters/b;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 131
    new-instance v1, Landroid/widget/Space;

    invoke-direct {v1, p1}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    .line 132
    sget-object v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->a:Lcom/twitter/android/smartfollow/followpeople/a$c;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Got unsupported item: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/g;->a(Ljava/lang/String;)V

    .line 136
    :goto_0
    return-object v1

    .line 106
    :pswitch_0
    const v0, 0x7f0403c5

    invoke-virtual {p0, v0, p3}, Lcom/twitter/android/smartfollow/followpeople/a;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 110
    :pswitch_1
    const v0, 0x7f0403c7

    invoke-virtual {p0, v0, p3}, Lcom/twitter/android/smartfollow/followpeople/a;->b(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 114
    :pswitch_2
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403c4

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 115
    sget-object v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->a:Lcom/twitter/android/smartfollow/followpeople/a$c;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 119
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f040063

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 120
    check-cast v0, Lcom/twitter/ui/user/UserView;

    .line 121
    new-instance v2, Lcom/twitter/android/db;

    invoke-direct {v2, v0}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;)V

    .line 122
    new-instance v3, Lcom/twitter/android/smartfollow/followpeople/a$c$a;

    invoke-direct {v3}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;-><init>()V

    .line 124
    invoke-virtual {v3, v2}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;->a(Lcom/twitter/android/db;)Lcom/twitter/android/smartfollow/followpeople/a$c$a;

    move-result-object v2

    .line 125
    invoke-virtual {v2}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;->a()Lcom/twitter/android/smartfollow/followpeople/a$c;

    move-result-object v2

    .line 127
    invoke-virtual {v0, v2}, Lcom/twitter/ui/user/UserView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 46
    check-cast p2, Lcom/twitter/android/people/adapters/b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Landroid/content/Context;Lcom/twitter/android/people/adapters/b;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 438
    return-void
.end method

.method public a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 442
    sub-int v0, p2, p3

    .line 443
    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 444
    invoke-static {v0}, Lcom/twitter/android/smartfollow/followpeople/a;->c(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Ljava/lang/String;Landroid/view/View;)V

    .line 445
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/people/adapters/b;)V
    .locals 5

    .prologue
    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/a$c;

    .line 143
    invoke-static {p3}, Lcom/twitter/android/smartfollow/followpeople/a;->c(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;

    move-result-object v3

    .line 144
    invoke-virtual {p0, p3}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Lcom/twitter/android/people/adapters/b;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 190
    :pswitch_0
    iput-object v3, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->e:Ljava/lang/String;

    .line 193
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->f:Lcom/twitter/android/smartfollow/followpeople/b;

    invoke-virtual {v0, p3}, Lcom/twitter/android/smartfollow/followpeople/b;->a(Lcom/twitter/android/people/adapters/b;)V

    .line 194
    return-void

    .line 146
    :pswitch_1
    invoke-virtual {p0, v3, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0

    :pswitch_2
    move-object v0, p3

    .line 150
    check-cast v0, Lcom/twitter/android/people/adapters/b$j;

    iget-object v0, v0, Lcom/twitter/android/people/adapters/b$j;->a:Lcom/twitter/model/people/i;

    iget-object v0, v0, Lcom/twitter/model/people/i;->d:Ljava/lang/String;

    invoke-virtual {p0, v3, v0, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0

    :pswitch_3
    move-object v1, p3

    .line 154
    check-cast v1, Lcom/twitter/android/people/adapters/b$k;

    .line 155
    check-cast p1, Lcom/twitter/ui/user/UserSocialView;

    .line 156
    iget-object v2, v1, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    iget-object v2, v2, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/TwitterUser;

    .line 157
    iput-object v3, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->e:Ljava/lang/String;

    .line 158
    invoke-virtual {p1, v2}, Lcom/twitter/ui/user/UserSocialView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 159
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/twitter/ui/user/UserSocialView;->setContentSize(F)V

    .line 160
    iget-object v3, v2, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    .line 161
    invoke-static {v3}, Lcom/twitter/android/profiles/v;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    .line 160
    invoke-virtual {p1, v3, v4}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 162
    iget-object v0, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->b:Lcom/twitter/android/db;

    .line 163
    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0, v2}, Lcom/twitter/android/db;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 166
    :cond_0
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/a$1;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/smartfollow/followpeople/a$1;-><init>(Lcom/twitter/android/smartfollow/followpeople/a;Lcom/twitter/android/people/adapters/b$k;)V

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/a$2;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/smartfollow/followpeople/a$2;-><init>(Lcom/twitter/android/smartfollow/followpeople/a;Lcom/twitter/android/people/adapters/b$k;)V

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setCheckBoxClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 186
    iget-object v0, p1, Lcom/twitter/ui/user/UserSocialView;->s:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/a;->d:Ljava/util/Set;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 144
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p3, Lcom/twitter/android/people/adapters/b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/people/adapters/b;)V

    return-void
.end method

.method protected a(Lcbi;)V
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 466
    if-eqz p1, :cond_2

    .line 467
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Lcbi;->be_()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 468
    invoke-virtual {p1, v2}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 469
    if-eqz v0, :cond_0

    .line 472
    invoke-static {v0}, Lcom/twitter/android/smartfollow/followpeople/a;->c(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;

    move-result-object v4

    .line 473
    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 474
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 475
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 476
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 481
    :goto_1
    invoke-static {v1, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 478
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 485
    :cond_2
    iput-object v3, p0, Lcom/twitter/android/smartfollow/followpeople/a;->a:Ljava/util/Map;

    .line 486
    return-void
.end method

.method public a(Lcom/twitter/android/smartfollow/followpeople/a$d;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/a;->g:Lcom/twitter/android/smartfollow/followpeople/a$d;

    .line 98
    return-void
.end method

.method a(Ljava/lang/String;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 218
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/a$c;

    .line 219
    sget-object v1, Lcom/twitter/android/smartfollow/followpeople/a;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/a;->j()Landroid/content/Context;

    move-result-object v2

    sget-object v1, Lcom/twitter/android/smartfollow/followpeople/a;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 224
    :goto_0
    iput-object p1, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->e:Ljava/lang/String;

    .line 225
    iget-object v0, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->c:Lcom/twitter/android/smartfollow/followpeople/a$b;

    .line 226
    if-eqz v0, :cond_0

    .line 227
    iget-object v2, v0, Lcom/twitter/android/smartfollow/followpeople/a$b;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, v0, Lcom/twitter/android/smartfollow/followpeople/a$b;->c:Landroid/widget/CheckBox;

    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 230
    :cond_0
    return-void

    :cond_1
    move-object v1, p1

    .line 222
    goto :goto_0
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 288
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/a$c;

    .line 289
    iput-object p1, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->e:Ljava/lang/String;

    .line 290
    iget-object v0, v0, Lcom/twitter/android/smartfollow/followpeople/a$c;->d:Lcom/twitter/android/smartfollow/followpeople/a$e;

    .line 291
    if-eqz v0, :cond_0

    .line 292
    iget-object v0, v0, Lcom/twitter/android/smartfollow/followpeople/a$e;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 349
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->b(Ljava/lang/String;)Lcbi;

    move-result-object v1

    .line 350
    invoke-virtual {v1}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 351
    instance-of v3, v0, Lcom/twitter/android/people/adapters/b$k;

    if-eqz v3, :cond_0

    .line 352
    check-cast v0, Lcom/twitter/android/people/adapters/b$k;

    iget-object v0, v0, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    iget-object v0, v0, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-direct {p0, v4, v5, p2}, Lcom/twitter/android/smartfollow/followpeople/a;->a(JZ)V

    goto :goto_0

    .line 356
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/a;->notifyDataSetChanged()V

    .line 357
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->c:Lcom/twitter/android/smartfollow/followpeople/a$a;

    invoke-interface {v0}, Lcom/twitter/android/smartfollow/followpeople/a$a;->bb_()V

    .line 358
    invoke-virtual {v1}, Lcbi;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 359
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 360
    instance-of v1, v0, Lcom/twitter/android/people/adapters/viewbinders/p;

    if-eqz v1, :cond_2

    .line 361
    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/a;->f:Lcom/twitter/android/smartfollow/followpeople/b;

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/p;

    .line 362
    invoke-interface {v0}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    .line 361
    invoke-virtual {v1, v0, p2}, Lcom/twitter/android/smartfollow/followpeople/b;->a(Lcom/twitter/model/people/b;Z)V

    .line 365
    :cond_2
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->b(Ljava/lang/String;)Lcbi;

    move-result-object v0

    .line 333
    invoke-virtual {v0}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 334
    instance-of v2, v0, Lcom/twitter/android/people/adapters/b$k;

    if-eqz v2, :cond_0

    .line 335
    iget-object v2, p0, Lcom/twitter/android/smartfollow/followpeople/a;->d:Ljava/util/Set;

    check-cast v0, Lcom/twitter/android/people/adapters/b$k;

    iget-object v0, v0, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    iget-object v0, v0, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    const/4 v0, 0x0

    .line 340
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a_(II)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 418
    sub-int v0, p1, p2

    .line 419
    const/4 v2, 0x0

    .line 420
    if-ltz v0, :cond_2

    .line 421
    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 423
    instance-of v3, v0, Lcom/twitter/android/people/adapters/b$i;

    if-eqz v3, :cond_0

    move v0, v1

    .line 433
    :goto_0
    return v0

    .line 425
    :cond_0
    instance-of v3, v0, Lcom/twitter/android/people/adapters/b$k;

    if-eqz v3, :cond_2

    .line 426
    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/a;->b(Lcom/twitter/android/people/adapters/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 429
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public b(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/a;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 264
    new-instance v1, Lcom/twitter/android/smartfollow/followpeople/a$e;

    invoke-direct {v1, v0}, Lcom/twitter/android/smartfollow/followpeople/a$e;-><init>(Landroid/view/View;)V

    .line 265
    new-instance v2, Lcom/twitter/android/smartfollow/followpeople/a$c$a;

    invoke-direct {v2}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;-><init>()V

    .line 267
    invoke-virtual {v2, v1}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;->a(Lcom/twitter/android/smartfollow/followpeople/a$e;)Lcom/twitter/android/smartfollow/followpeople/a$c$a;

    move-result-object v1

    .line 268
    invoke-virtual {v1}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;->a()Lcom/twitter/android/smartfollow/followpeople/a$c;

    move-result-object v1

    .line 269
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 270
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f1307f7

    if-ne v1, v2, :cond_0

    .line 271
    new-instance v1, Lcom/twitter/android/smartfollow/followpeople/a$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/smartfollow/followpeople/a$4;-><init>(Lcom/twitter/android/smartfollow/followpeople/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    :cond_0
    return-object v0
.end method

.method protected b(Ljava/lang/String;)Lcbi;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcbi",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 406
    new-instance v3, Lcbl$a;

    invoke-direct {v3}, Lcbl$a;-><init>()V

    .line 407
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 408
    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v2, v1, :cond_0

    .line 410
    invoke-virtual {p0, v2}, Lcom/twitter/android/smartfollow/followpeople/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 409
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 413
    :cond_0
    invoke-virtual {v3}, Lcbl$a;->a()Lcbl;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/a;->h:Landroid/view/View;

    .line 214
    return-void
.end method

.method protected b(Lcom/twitter/android/people/adapters/b;)Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 454
    invoke-static {p1}, Lcom/twitter/android/smartfollow/followpeople/a;->c(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/a;->b(Ljava/lang/String;)Lcbi;

    move-result-object v0

    .line 455
    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 456
    if-ltz v1, :cond_0

    invoke-virtual {v0, v1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 239
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/a$b;

    invoke-direct {v0, p1}, Lcom/twitter/android/smartfollow/followpeople/a$b;-><init>(Landroid/view/View;)V

    .line 240
    new-instance v1, Lcom/twitter/android/smartfollow/followpeople/a$c$a;

    invoke-direct {v1}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;-><init>()V

    .line 242
    invoke-virtual {v1, v0}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;->a(Lcom/twitter/android/smartfollow/followpeople/a$b;)Lcom/twitter/android/smartfollow/followpeople/a$c$a;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/followpeople/a$c$a;->a()Lcom/twitter/android/smartfollow/followpeople/a$c;

    move-result-object v0

    .line 244
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 246
    instance-of v0, p1, Lcom/twitter/android/widget/SmartFollowHeaderView;

    if-eqz v0, :cond_0

    .line 247
    check-cast p1, Lcom/twitter/android/widget/SmartFollowHeaderView;

    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/a$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/followpeople/a$3;-><init>(Lcom/twitter/android/smartfollow/followpeople/a;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/SmartFollowHeaderView;->setOnCheckChangedListener(Lcom/twitter/android/widget/SmartFollowHeaderView$a;)V

    .line 259
    :cond_0
    return-void
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/followpeople/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    invoke-static {v0}, Lcom/twitter/android/smartfollow/followpeople/a;->d(Lcom/twitter/android/people/adapters/b;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x5

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/a;->g()Lcbi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Lcbi;)V

    .line 199
    invoke-super {p0}, Lcjr;->notifyDataSetChanged()V

    .line 200
    return-void
.end method
