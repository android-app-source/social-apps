.class public Lcom/twitter/android/smartfollow/followpeople/h;
.super Lcom/twitter/android/smartfollow/a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/smartfollow/followpeople/a$a;
.implements Lcom/twitter/android/smartfollow/followpeople/a$d;
.implements Lcom/twitter/app/onboarding/flowstep/common/h;


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/smartfollow/a",
        "<",
        "Lcom/twitter/app/common/util/StateSaver",
        "<",
        "Lcom/twitter/android/smartfollow/followpeople/h;",
        ">;",
        "Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;",
        ">;",
        "Lcom/twitter/android/smartfollow/followpeople/a$a;",
        "Lcom/twitter/android/smartfollow/followpeople/a$d;",
        "Lcom/twitter/app/onboarding/flowstep/common/h",
        "<",
        "Lcom/twitter/model/onboarding/e",
        "<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field protected j:Ljava/util/Set;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field protected k:Lcnk;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected l:Lcom/twitter/android/smartfollow/followpeople/a;

.field private final m:Lcom/twitter/android/people/i;

.field private final n:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/twitter/android/smartfollow/followpeople/b;

.field private q:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lrx/j;

.field private s:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/people/i;Lcom/twitter/app/common/util/StateSaver;Lcom/twitter/android/smartfollow/followpeople/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/people/i;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;",
            "Lcom/twitter/android/smartfollow/followpeople/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/a;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    .line 56
    sget-object v0, Lcnk;->a:Lcnk;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->k:Lcnk;

    .line 60
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->n:Landroid/support/v4/util/LongSparseArray;

    .line 61
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->o:Ljava/util/Map;

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->s:Z

    .line 71
    invoke-virtual {p2, p0}, Lcom/twitter/app/common/util/StateSaver;->a(Ljava/lang/Object;)V

    .line 72
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->m:Lcom/twitter/android/people/i;

    .line 73
    iput-object p3, p0, Lcom/twitter/android/smartfollow/followpeople/h;->p:Lcom/twitter/android/smartfollow/followpeople/b;

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/smartfollow/followpeople/h;Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->q:Ljava/lang/Iterable;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/smartfollow/followpeople/h;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->w()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/smartfollow/followpeople/h;)Landroid/support/v4/util/LongSparseArray;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->n:Landroid/support/v4/util/LongSparseArray;

    return-object v0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->l:Lcom/twitter/android/smartfollow/followpeople/a;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/followpeople/a;->k()Lcjt;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->q:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/h;->a(Ljava/lang/Iterable;)Lcbi;

    move-result-object v0

    invoke-interface {v1, v0}, Lcjt;->a(Lcbi;)Lcbi;

    .line 100
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->bb_()V

    .line 101
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Iterable;)Lcbi;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;)",
            "Lcbi",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 157
    new-instance v6, Lcbl$a;

    invoke-direct {v6}, Lcbl$a;-><init>()V

    .line 159
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->n:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 161
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v7

    .line 162
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v8

    .line 166
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 167
    instance-of v2, v0, Lcom/twitter/android/people/adapters/b$j;

    if-eqz v2, :cond_0

    .line 168
    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/h;->a(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;

    move-result-object v2

    .line 169
    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/p;

    .line 170
    invoke-interface {v0}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    iget-object v0, v0, Lcom/twitter/model/people/g;->f:Lcom/twitter/model/people/i;

    iget v0, v0, Lcom/twitter/model/people/i;->b:I

    .line 171
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 175
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v2, v3

    move v4, v3

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 176
    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/h;->a(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;

    move-result-object v10

    .line 177
    instance-of v1, v0, Lcom/twitter/android/people/adapters/b$e;

    if-nez v1, :cond_2

    instance-of v1, v0, Lcom/twitter/android/people/adapters/b$i;

    if-eqz v1, :cond_3

    .line 178
    :cond_2
    invoke-virtual {v6, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    move v0, v2

    move v1, v4

    :goto_2
    move v2, v0

    move v4, v1

    .line 211
    goto :goto_1

    .line 179
    :cond_3
    instance-of v1, v0, Lcom/twitter/android/people/adapters/b$k;

    if-eqz v1, :cond_a

    .line 180
    invoke-interface {v8, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 181
    invoke-interface {v7, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 182
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    :cond_4
    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 185
    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->o:Ljava/util/Map;

    invoke-interface {v1, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->o:Ljava/util/Map;

    .line 186
    invoke-interface {v1, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v5, v1

    .line 187
    :goto_3
    invoke-interface {v7, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v5, v11

    if-ge v1, v5, :cond_8

    .line 188
    invoke-interface {v7, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    move-object v1, v0

    .line 194
    check-cast v1, Lcom/twitter/android/people/adapters/b$k;

    iget-object v1, v1, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    .line 195
    iget-object v5, v1, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    .line 196
    invoke-virtual {v6, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 197
    iget-boolean v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->s:Z

    if-eqz v0, :cond_9

    iget-boolean v0, v1, Lcom/twitter/model/people/l;->d:Z

    if-eqz v0, :cond_9

    .line 198
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    invoke-virtual {v5}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->n:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v5}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v10

    iget-object v1, v1, Lcom/twitter/model/people/l;->c:Ljava/lang/String;

    .line 203
    invoke-static {v5, v4, v1}, Lcom/twitter/android/smartfollow/x;->a(Lcom/twitter/model/core/TwitterUser;ILjava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 202
    invoke-virtual {v0, v10, v11, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 204
    add-int/lit8 v4, v4, 0x1

    move v0, v2

    move v1, v4

    .line 205
    goto/16 :goto_2

    :cond_7
    move v5, v3

    .line 186
    goto :goto_3

    .line 190
    :cond_8
    const/4 v0, 0x1

    move v2, v0

    .line 191
    goto/16 :goto_1

    .line 199
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->l:Lcom/twitter/android/smartfollow/followpeople/a;

    invoke-virtual {v0, v10}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 200
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    invoke-virtual {v5}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 205
    :cond_a
    instance-of v1, v0, Lcom/twitter/android/people/adapters/b$j;

    if-eqz v1, :cond_c

    .line 206
    if-eqz v2, :cond_c

    .line 208
    invoke-virtual {v6, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    move v0, v3

    move v1, v4

    goto/16 :goto_2

    .line 212
    :cond_b
    iput-boolean v3, p0, Lcom/twitter/android/smartfollow/followpeople/h;->s:Z

    .line 213
    invoke-virtual {v6}, Lcbl$a;->a()Lcbl;

    move-result-object v0

    return-object v0

    :cond_c
    move v0, v2

    move v1, v4

    goto/16 :goto_2
.end method

.method protected a(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    instance-of v0, p1, Lcom/twitter/android/people/adapters/viewbinders/p;

    if-eqz v0, :cond_0

    .line 219
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/p;

    .line 220
    invoke-interface {p1}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    iget-object v0, v0, Lcom/twitter/model/people/g;->a:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    iget-object v0, v0, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    .line 222
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 267
    invoke-super {p0, p1}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/app/common/di/scope/InjectionScope;)V

    .line 268
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->r:Lrx/j;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->r:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 270
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->r:Lrx/j;

    .line 272
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 246
    const-string/jumbo v0, "category"

    const-string/jumbo v1, "more"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/smartfollow/followpeople/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->q:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/b;

    .line 248
    instance-of v2, v0, Lcom/twitter/android/people/adapters/b$j;

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/followpeople/h;->a(Lcom/twitter/android/people/adapters/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/p;

    .line 250
    invoke-interface {v0}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    iget-object v0, v0, Lcom/twitter/model/people/g;->f:Lcom/twitter/model/people/i;

    iget v2, v0, Lcom/twitter/model/people/i;->c:I

    .line 251
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->o:Ljava/util/Map;

    .line 252
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 253
    :goto_1
    iget-object v3, p0, Lcom/twitter/android/smartfollow/followpeople/h;->o:Ljava/util/Map;

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 252
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 256
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->w()V

    .line 257
    return-void
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    const-string/jumbo v0, "presenter_follow_people_discovery"

    return-object v0
.end method

.method public bb_()V
    .locals 5

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->g()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a03a8

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 131
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->setCtaCopy(Ljava/lang/String;)V

    .line 134
    :cond_0
    return-void

    .line 129
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->g()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0209

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->t()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lcom/twitter/model/onboarding/a;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->v()Lcom/twitter/model/onboarding/e;

    move-result-object v0

    return-object v0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->n()V

    .line 106
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->getFirstVisibleItem()Lcnk;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->k:Lcnk;

    .line 109
    :cond_0
    return-void
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string/jumbo v0, "follow_people_screen"

    return-object v0
.end method

.method protected p()V
    .locals 4

    .prologue
    .line 78
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->p()V

    .line 79
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->l:Lcom/twitter/android/smartfollow/followpeople/a;

    if-nez v0, :cond_0

    .line 82
    new-instance v1, Lcom/twitter/android/smartfollow/followpeople/a;

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/followpeople/h;->p:Lcom/twitter/android/smartfollow/followpeople/b;

    invoke-direct {v1, v0, p0, v2, v3}, Lcom/twitter/android/smartfollow/followpeople/a;-><init>(Landroid/content/Context;Lcom/twitter/android/smartfollow/followpeople/a$a;Ljava/util/Set;Lcom/twitter/android/smartfollow/followpeople/b;)V

    iput-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->l:Lcom/twitter/android/smartfollow/followpeople/a;

    .line 84
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->l:Lcom/twitter/android/smartfollow/followpeople/a;

    invoke-virtual {v0, p0}, Lcom/twitter/android/smartfollow/followpeople/a;->a(Lcom/twitter/android/smartfollow/followpeople/a$d;)V

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->f()Ljava/util/Map;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->m:Lcom/twitter/android/people/i;

    invoke-virtual {v1, v0}, Lcom/twitter/android/people/i;->a(Ljava/util/Map;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/smartfollow/followpeople/h$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/smartfollow/followpeople/h$1;-><init>(Lcom/twitter/android/smartfollow/followpeople/h;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->r:Lrx/j;

    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->l:Lcom/twitter/android/smartfollow/followpeople/a;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/followpeople/h;->k:Lcnk;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/smartfollow/followpeople/FollowPeopleScreen;->a(Lcom/twitter/android/smartfollow/followpeople/a;Lcnk;)V

    .line 96
    return-void
.end method

.method public s()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 140
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    .line 141
    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a([J)V

    .line 144
    :cond_0
    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    .line 145
    const-string/jumbo v1, "follow_many"

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/followpeople/h;->u()Ljava/util/List;

    move-result-object v2

    .line 145
    invoke-virtual {p0, v3, v1, v0, v2}, Lcom/twitter/android/smartfollow/followpeople/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 151
    :goto_0
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->s()V

    .line 152
    return-void

    .line 148
    :cond_1
    const-string/jumbo v0, "follow_none"

    invoke-virtual {p0, v3, v0}, Lcom/twitter/android/smartfollow/followpeople/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public t()Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/twitter/android/smartfollow/followpeople/FollowPeoplePresenterSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/followpeople/FollowPeoplePresenterSavedState;-><init>(Lcom/twitter/android/smartfollow/followpeople/h;)V

    return-object v0
.end method

.method protected u()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    new-instance v1, Lcom/twitter/android/smartfollow/followpeople/h$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/smartfollow/followpeople/h$2;-><init>(Lcom/twitter/android/smartfollow/followpeople/h;)V

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public v()Lcom/twitter/model/onboarding/e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/model/onboarding/e",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    new-instance v0, Lcom/twitter/model/onboarding/e;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/followpeople/h;->j:Ljava/util/Set;

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/e;-><init>(Ljava/util/Set;)V

    return-object v0
.end method
