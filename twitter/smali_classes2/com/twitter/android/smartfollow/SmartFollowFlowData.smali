.class public Lcom/twitter/android/smartfollow/SmartFollowFlowData;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/smartfollow/SmartFollowFlowData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:[Ljava/lang/String;

.field private b:[Ljava/lang/String;

.field private c:[J

.field private d:Lcom/twitter/model/stratostore/SourceLocation;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/android/smartfollow/SmartFollowFlowData$1;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData$1;-><init>()V

    sput-object v0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a:[Ljava/lang/String;

    .line 31
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b:[Ljava/lang/String;

    .line 32
    new-array v0, v1, [J

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c:[J

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a:[Ljava/lang/String;

    .line 31
    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b:[Ljava/lang/String;

    .line 32
    new-array v2, v1, [J

    iput-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c:[J

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a:[Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b:[Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c:[J

    .line 43
    invoke-static {}, Lcom/twitter/model/stratostore/SourceLocation;->values()[Lcom/twitter/model/stratostore/SourceLocation;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->d:Lcom/twitter/model/stratostore/SourceLocation;

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->e:Z

    .line 45
    return-void

    :cond_0
    move v0, v1

    .line 44
    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/stratostore/SourceLocation;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->d:Lcom/twitter/model/stratostore/SourceLocation;

    .line 98
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->e:Z

    .line 81
    return-void
.end method

.method public a([J)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c:[J

    .line 94
    return-void
.end method

.method public a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a:[Ljava/lang/String;

    .line 68
    return-void
.end method

.method public a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public b([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b:[Ljava/lang/String;

    .line 77
    return-void
.end method

.method public b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->e:Z

    return v0
.end method

.method public d()[J
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c:[J

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public e()Lcom/twitter/model/stratostore/SourceLocation;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->d:Lcom/twitter/model/stratostore/SourceLocation;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/stratostore/SourceLocation;

    return-object v0
.end method

.method public f()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/util/collection/i;->a(I)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "interest_ids"

    const-string/jumbo v2, ","

    .line 108
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "custom_interests"

    const-string/jumbo v2, ","

    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 107
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->b:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->c:[J

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 52
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->d:Lcom/twitter/model/stratostore/SourceLocation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->d:Lcom/twitter/model/stratostore/SourceLocation;

    invoke-virtual {v0}, Lcom/twitter/model/stratostore/SourceLocation;->ordinal()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget-boolean v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->e:Z

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    return-void

    :cond_1
    move v0, v1

    .line 52
    goto :goto_0
.end method
