.class public final Lcom/twitter/android/smartfollow/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/model/util/FriendshipCache;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/twitter/android/smartfollow/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/twitter/android/smartfollow/o;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/o;-><init>()V

    sput-object v0, Lcom/twitter/android/smartfollow/o;->a:Lcom/twitter/android/smartfollow/o;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static c()Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    sget-object v0, Lcom/twitter/android/smartfollow/o;->a:Lcom/twitter/android/smartfollow/o;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/model/util/FriendshipCache;
    .locals 2

    .prologue
    .line 20
    invoke-static {}, Lcom/twitter/android/smartfollow/i;->a()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    const-string/jumbo v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 19
    invoke-static {v0, v1}, Ldagger/internal/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/o;->a()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    return-object v0
.end method
