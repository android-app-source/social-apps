.class public Lcom/twitter/android/smartfollow/g;
.super Laol;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/smartfollow/f;


# instance fields
.field private final a:Lcom/twitter/app/common/base/BaseFragmentActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Laol;-><init>(Landroid/app/Activity;)V

    .line 37
    iput-object p1, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 38
    return-void
.end method


# virtual methods
.method public a([JLcom/twitter/library/service/t;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbhr;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 52
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, p1, v4}, Lbhr;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;[JZ)V

    .line 50
    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/DispatchActivity;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 45
    return-void
.end method

.method public varargs a(I[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v0, p1, v1, p2}, Lcom/twitter/util/android/f;->a(ILandroid/app/Activity;[Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public a(JLjava/lang/String;I)V
    .locals 5

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const-class v3, Lcom/twitter/android/smartfollow/interestsearch/InterestSearchActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "extra_parent_id"

    .line 93
    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "extra_scribe_page"

    .line 94
    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1, p4}, Lcom/twitter/app/common/base/BaseFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 95
    return-void
.end method

.method public a(Lcom/twitter/model/json/stratostore/JsonInterestSelections;)V
    .locals 3

    .prologue
    .line 83
    new-instance v0, Lbfi;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 84
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lbfi;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/json/stratostore/JsonInterestSelections;)V

    .line 86
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 87
    return-void
.end method

.method public a([Ljava/lang/String;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 112
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v1, v2, p1}, Lcom/twitter/util/android/f;->b(Landroid/content/Context;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 111
    invoke-static {v0, p2, v1}, Lcom/twitter/android/runtimepermissions/a;->a(Landroid/content/Context;Landroid/view/View;Ljava/util/Set;)V

    .line 113
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 104
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const-string/jumbo v2, "location_fatigue"

    .line 106
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 105
    invoke-static {v1, v2, v4, v5}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 107
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 63
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 65
    if-nez v1, :cond_0

    .line 79
    :goto_0
    return-void

    .line 69
    :cond_0
    new-instance v2, Lbfr;

    iget-object v3, p0, Lcom/twitter/android/smartfollow/g;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v0, v1, v4}, Lbfr;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;I)V

    .line 70
    invoke-static {}, Lcom/twitter/android/av/m;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Lbfr;->e(I)Lbge;

    move-result-object v0

    const-string/jumbo v1, "HTL request after completing Signup/NUX is considered to be user initiated action. The app may or may not be visible"

    .line 71
    invoke-virtual {v0, v1}, Lbge;->m(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbfr;

    .line 74
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    invoke-virtual {v0, p1}, Lbfr;->c(Ljava/lang/String;)Lbge;

    .line 78
    :cond_1
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0
.end method
