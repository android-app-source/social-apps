.class public Lcom/twitter/android/smartfollow/finishingtimeline/a;
.super Lcom/twitter/android/smartfollow/a;
.source "Twttr"


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/smartfollow/finishingtimeline/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/smartfollow/a",
        "<",
        "Lcom/twitter/app/common/util/StateSaver",
        "<",
        "Lcom/twitter/android/smartfollow/finishingtimeline/a;",
        ">;",
        "Lcom/twitter/android/smartfollow/finishingtimeline/FinishingTimelineScreen;",
        ">;"
    }
.end annotation


# static fields
.field protected static final j:J

.field private static final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected k:Ljava/lang/String;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected l:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected m:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected n:Lcom/twitter/android/smartfollow/finishingtimeline/a$a;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field protected o:I
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field protected p:I
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field private final r:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    const-string/jumbo v0, "smart_nux_smart_follow_timings_message_display_duration"

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->j:J

    .line 41
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    const v1, 0x7f0a08e4

    .line 42
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    const v1, 0x7f0a08e5

    .line 43
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    const v1, 0x7f0a08e6

    .line 44
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sput-object v0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->q:Ljava/util/List;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/twitter/app/common/util/StateSaver;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/finishingtimeline/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/a;-><init>()V

    .line 47
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->k:Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/twitter/android/smartfollow/finishingtimeline/a$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a$a;-><init>(Lcom/twitter/android/smartfollow/finishingtimeline/a;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->n:Lcom/twitter/android/smartfollow/finishingtimeline/a$a;

    .line 51
    iput v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->o:I

    .line 52
    iput v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->p:I

    .line 58
    iput-object p1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->r:Landroid/os/Handler;

    .line 59
    invoke-virtual {p2, p0}, Lcom/twitter/app/common/util/StateSaver;->a(Ljava/lang/Object;)V

    .line 60
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Runnable;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 116
    iget v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->o:I

    add-int/lit8 v0, v0, 0x1

    .line 117
    iget v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->p:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->p:I

    .line 120
    iget-boolean v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->l:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->p:I

    sget-object v2, Lcom/twitter/android/smartfollow/finishingtimeline/a;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->s()V

    .line 130
    :goto_0
    return-void

    .line 123
    :cond_0
    sget-object v1, Lcom/twitter/android/smartfollow/finishingtimeline/a;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->o:I

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/finishingtimeline/FinishingTimelineScreen;

    sget-object v1, Lcom/twitter/android/smartfollow/finishingtimeline/a;->q:Ljava/util/List;

    iget v2, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->o:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/finishingtimeline/FinishingTimelineScreen;->setDescription(I)V

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->r:Landroid/os/Handler;

    sget-wide v2, Lcom/twitter/android/smartfollow/finishingtimeline/a;->j:J

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string/jumbo v0, "presenter_finishing_timeline"

    return-object v0
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->e()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/finishingtimeline/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/android/smartfollow/finishingtimeline/FinishingTimelinePresenterSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/finishingtimeline/FinishingTimelinePresenterSavedState;-><init>(Lcom/twitter/android/smartfollow/finishingtimeline/a;)V

    return-object v0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->n()V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->r:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 136
    return-void
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const-string/jumbo v0, "finishing_timeline_screen"

    return-object v0
.end method

.method protected p()V
    .locals 4

    .prologue
    .line 76
    invoke-super {p0}, Lcom/twitter/android/smartfollow/a;->p()V

    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-boolean v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->l:Z

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->d()[J

    move-result-object v0

    .line 81
    array-length v1, v0

    if-lez v1, :cond_1

    .line 82
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/p;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->d()Lcom/twitter/android/smartfollow/f;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->n:Lcom/twitter/android/smartfollow/finishingtimeline/a$a;

    invoke-interface {v1, v0, v2}, Lcom/twitter/android/smartfollow/f;->a([JLcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->k:Ljava/lang/String;

    .line 90
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/finishingtimeline/FinishingTimelineScreen;

    sget-object v1, Lcom/twitter/android/smartfollow/finishingtimeline/a;->q:Ljava/util/List;

    iget v2, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->o:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/finishingtimeline/FinishingTimelineScreen;->setDescription(I)V

    .line 92
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->t()V

    .line 94
    iget-object v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->r:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->r:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/smartfollow/finishingtimeline/a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a$1;-><init>(Lcom/twitter/android/smartfollow/finishingtimeline/a;)V

    sget-wide v2, Lcom/twitter/android/smartfollow/finishingtimeline/a;->j:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 101
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->l:Z

    goto :goto_0
.end method

.method protected t()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->d()Lcom/twitter/android/smartfollow/f;

    move-result-object v0

    .line 108
    iget-boolean v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->l:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->m:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/finishingtimeline/a;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/smartfollow/f;->b(Ljava/lang/String;)V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/smartfollow/finishingtimeline/a;->m:Z

    .line 112
    :cond_0
    return-void
.end method
