.class public abstract Lcom/twitter/android/smartfollow/i;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Lcom/twitter/android/smartfollow/h;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/twitter/android/smartfollow/h;->a:I

    return v0
.end method

.method static a(I)Laem$a;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Laem$a$a;

    invoke-direct {v0}, Laem$a$a;-><init>()V

    .line 84
    invoke-static {p0}, Lcom/twitter/android/smartfollow/i;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laem$a$a;->a(Ljava/lang/String;)Laem$a$a;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Laem$a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laem$a;

    .line 83
    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Laet;
    .locals 3

    .prologue
    .line 102
    new-instance v0, Laet;

    new-instance v1, Lauh;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v0, v1, p1}, Laet;-><init>(Lauj;Lcom/twitter/library/client/Session;)V

    return-object v0
.end method

.method static a(Lanr;)Lcom/twitter/android/smartfollow/h;
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/h;

    return-object v0
.end method

.method static a(Lank;)Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lank;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/finishingtimeline/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    const-string/jumbo v0, "presenter_finishing_timeline"

    invoke-static {p0, v0}, Lcom/twitter/android/smartfollow/i;->a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lank;",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 169
    invoke-static {}, Lcom/twitter/app/common/util/StateSaver;->a()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v1

    .line 168
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/StateSaver;

    return-object v0
.end method

.method static a()Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    return-object v0
.end method

.method static a(Landroid/app/Application;Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/util/a;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    return-object v0
.end method

.method static a(Lcom/twitter/android/smartfollow/followpeople/d;)Lmf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/smartfollow/followpeople/d;",
            ")",
            "Lmf",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v0, Lmf;

    new-instance v1, Lcom/twitter/android/smartfollow/i$1;

    invoke-direct {v1}, Lcom/twitter/android/smartfollow/i$1;-><init>()V

    invoke-direct {v0, v1, p0}, Lmf;-><init>(Lmf$a;Lme;)V

    return-object v0
.end method

.method static a(Lcom/twitter/android/smartfollow/followpeople/j;)Lmf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/smartfollow/followpeople/j;",
            ")",
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/people/adapters/b$k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    invoke-static {p0}, Lmf;->a(Lme;)Lmf;

    move-result-object v0

    return-object v0
.end method

.method static b(Landroid/content/Context;Lcom/twitter/library/client/Session;)Laes;
    .locals 3

    .prologue
    .line 110
    new-instance v0, Laes;

    new-instance v1, Lauh;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v0, v1, p1}, Laes;-><init>(Lauj;Lcom/twitter/library/client/Session;)V

    return-object v0
.end method

.method static b(Lcom/twitter/android/smartfollow/h;)Lcom/twitter/analytics/model/c;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/android/smartfollow/h;->b:Lcom/twitter/analytics/model/c;

    return-object v0
.end method

.method static b(Lank;)Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lank;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/followpeople/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    const-string/jumbo v0, "presenter_follow_people_discovery"

    invoke-static {p0, v0}, Lcom/twitter/android/smartfollow/i;->a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    .line 91
    const-string/jumbo v0, "rux"

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "nux"

    goto :goto_0
.end method

.method static c(Lank;)Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lank;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/smartfollow/waitingforsuggestions/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    const-string/jumbo v0, "presenter_waiting_for_people_discovery"

    invoke-static {p0, v0}, Lcom/twitter/android/smartfollow/i;->a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method
