.class public Lcom/twitter/android/smartfollow/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/smartfollow/a$a;


# instance fields
.field private final a:Lcom/twitter/android/twitterflows/h;

.field private final b:Lcom/twitter/android/twitterflows/b;

.field private final c:Lcom/twitter/android/smartfollow/f;


# direct methods
.method public constructor <init>(Lcom/twitter/android/twitterflows/h;Lcom/twitter/android/twitterflows/b;Lcom/twitter/android/smartfollow/f;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/twitter/android/smartfollow/c;->a:Lcom/twitter/android/twitterflows/h;

    .line 22
    iput-object p2, p0, Lcom/twitter/android/smartfollow/c;->b:Lcom/twitter/android/twitterflows/b;

    .line 23
    iput-object p3, p0, Lcom/twitter/android/smartfollow/c;->c:Lcom/twitter/android/smartfollow/f;

    .line 24
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/smartfollow/c;->a:Lcom/twitter/android/twitterflows/h;

    invoke-virtual {v0, p2}, Lcom/twitter/android/twitterflows/h;->a(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)Lcom/twitter/android/twitterflows/d;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/smartfollow/c;->a(Landroid/view/View;Lcom/twitter/android/twitterflows/d;)V

    .line 29
    return-void
.end method

.method a(Landroid/view/View;Lcom/twitter/android/twitterflows/d;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 38
    if-eqz p2, :cond_0

    .line 39
    iget-object v0, p0, Lcom/twitter/android/smartfollow/c;->b:Lcom/twitter/android/twitterflows/b;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/twitterflows/b;->a(Landroid/view/View;Lcom/twitter/android/twitterflows/d;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/smartfollow/c;->c:Lcom/twitter/android/smartfollow/f;

    invoke-interface {v0}, Lcom/twitter/android/smartfollow/f;->a()V

    goto :goto_0
.end method

.method public b(Landroid/view/View;Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/smartfollow/c;->a:Lcom/twitter/android/twitterflows/h;

    invoke-virtual {v0, p2}, Lcom/twitter/android/twitterflows/h;->a(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)Lcom/twitter/android/twitterflows/d;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/smartfollow/c;->a(Landroid/view/View;Lcom/twitter/android/twitterflows/d;)V

    .line 34
    return-void
.end method
