.class public Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;
.super Lcom/twitter/android/twitterflows/TwitterFlowsActivity;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/smartfollow/f;

.field private final b:Lcom/twitter/android/twitterflows/b;

.field private c:Lcom/twitter/android/smartfollow/a;

.field private d:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

.field private e:Lcom/twitter/android/twitterflows/a;

.field private f:Landroid/widget/FrameLayout;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/smartfollow/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;-><init>()V

    .line 44
    new-instance v0, Lcom/twitter/android/smartfollow/g;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/g;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->a:Lcom/twitter/android/smartfollow/f;

    .line 45
    new-instance v0, Lcom/twitter/android/twitterflows/c;

    invoke-direct {v0}, Lcom/twitter/android/twitterflows/c;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->b:Lcom/twitter/android/twitterflows/b;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lcom/twitter/android/smartfollow/d;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/d;-><init>()V

    const-string/jumbo v1, "welcome"

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->a(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    const/4 v1, 0x1

    .line 56
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->a(Z)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    const/4 v1, 0x0

    .line 57
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->a(I)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    const-string/jumbo v1, "signup"

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->b(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/stratostore/SourceLocation;->a:Lcom/twitter/model/stratostore/SourceLocation;

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->a(Lcom/twitter/model/stratostore/SourceLocation;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 60
    invoke-virtual {v0, p0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 54
    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 233
    if-eqz v0, :cond_0

    .line 234
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 236
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 67
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 68
    const v1, 0x7f0403c0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 69
    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-super {p0, p1, p2}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 76
    if-eqz p1, :cond_0

    .line 77
    const-string/jumbo v0, "flow_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    .line 82
    :goto_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Intent;)Lcom/twitter/android/smartfollow/d;

    move-result-object v4

    .line 86
    invoke-virtual {v4}, Lcom/twitter/android/smartfollow/d;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 87
    new-instance v0, Lcom/twitter/android/smartfollow/y;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/y;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->e:Lcom/twitter/android/twitterflows/a;

    .line 92
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->W()Lanh;

    move-result-object v5

    .line 93
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/w;

    .line 94
    const/4 v6, 0x6

    new-array v6, v6, [Lcom/twitter/android/smartfollow/a;

    .line 95
    invoke-interface {v0}, Lcom/twitter/android/smartfollow/w;->d()Lcom/twitter/android/smartfollow/finishingtimeline/a;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v7, 0x1

    .line 96
    invoke-interface {v0}, Lcom/twitter/android/smartfollow/w;->e()Lcom/twitter/android/smartfollow/followpeople/h;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    .line 97
    invoke-interface {v0}, Lcom/twitter/android/smartfollow/w;->f()Lcom/twitter/android/smartfollow/interestpicker/a;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    .line 98
    invoke-interface {v0}, Lcom/twitter/android/smartfollow/w;->i()Lcom/twitter/android/smartfollow/importcontacts/a;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    .line 99
    invoke-interface {v0}, Lcom/twitter/android/smartfollow/w;->g()Lcom/twitter/android/smartfollow/sharelocation/a;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    .line 100
    invoke-interface {v0}, Lcom/twitter/android/smartfollow/w;->h()Lcom/twitter/android/smartfollow/waitingforsuggestions/a;

    move-result-object v0

    aput-object v0, v6, v7

    .line 102
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v7

    .line 103
    array-length v8, v6

    move v0, v1

    :goto_2
    if-ge v0, v8, :cond_2

    aget-object v1, v6, v0

    .line 104
    iget-object v9, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->a:Lcom/twitter/android/smartfollow/f;

    invoke-virtual {v1, v9}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/f;)V

    .line 105
    iget-object v9, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-virtual {v1, v9}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V

    .line 106
    invoke-virtual {v4}, Lcom/twitter/android/smartfollow/d;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/twitter/android/smartfollow/a;->c_(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/smartfollow/a;->a(J)V

    .line 108
    iget-object v9, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->b:Lcom/twitter/android/twitterflows/b;

    invoke-virtual {v1, v9}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/twitterflows/b;)V

    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/twitter/android/smartfollow/a;->a(Landroid/content/Context;)V

    .line 110
    invoke-virtual {v4}, Lcom/twitter/android/smartfollow/d;->c()Z

    move-result v9

    invoke-virtual {v1, v9}, Lcom/twitter/android/smartfollow/a;->a(Z)V

    .line 111
    invoke-virtual {v4}, Lcom/twitter/android/smartfollow/d;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/twitter/android/smartfollow/a;->b(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v5, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 113
    invoke-virtual {v1}, Lcom/twitter/android/smartfollow/a;->al_()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9, v1}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 79
    :cond_0
    new-instance v0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    goto/16 :goto_0

    .line 89
    :cond_1
    new-instance v0, Lcom/twitter/android/smartfollow/z;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/z;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->e:Lcom/twitter/android/twitterflows/a;

    goto/16 :goto_1

    .line 115
    :cond_2
    invoke-virtual {v7}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->g:Ljava/util/Map;

    .line 116
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-virtual {v4}, Lcom/twitter/android/smartfollow/d;->d()Lcom/twitter/model/stratostore/SourceLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a(Lcom/twitter/model/stratostore/SourceLocation;)V

    .line 118
    const v0, 0x7f1306a7

    invoke-virtual {p0, v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->f:Landroid/widget/FrameLayout;

    .line 119
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->a(Landroid/os/Bundle;)V

    .line 120
    return-void
.end method

.method public a(Lflow/Flow$c;Lflow/Flow$d;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 174
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->S()Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->p_()V

    .line 221
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v1, p1, Lflow/Flow$c;->a:Lflow/b;

    .line 181
    const/4 v0, 0x0

    .line 182
    iget-object v2, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v8}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 183
    if-eqz v1, :cond_6

    .line 184
    invoke-virtual {v1}, Lflow/b;->e()Lflow/f;

    move-result-object v0

    .line 185
    iget-object v1, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v3, Lflow/Flow$Direction;->b:Lflow/Flow$Direction;

    if-eq v1, v3, :cond_1

    iget-object v1, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v3, Lflow/Flow$Direction;->c:Lflow/Flow$Direction;

    if-ne v1, v3, :cond_2

    .line 186
    :cond_1
    if-eqz v2, :cond_2

    .line 187
    invoke-interface {v0, v2}, Lflow/f;->a(Landroid/view/View;)V

    :cond_2
    move-object v4, v0

    .line 192
    :goto_1
    iget-object v5, p1, Lflow/Flow$c;->b:Lflow/b;

    .line 193
    invoke-virtual {v5}, Lflow/b;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/twitterflows/h;

    .line 194
    iget-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->g:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/twitter/android/twitterflows/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/smartfollow/a;

    iput-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    .line 195
    iget-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/twitterflows/h;)V

    .line 197
    iget-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    new-instance v3, Lcom/twitter/android/smartfollow/c;

    iget-object v6, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->b:Lcom/twitter/android/twitterflows/b;

    iget-object v7, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->a:Lcom/twitter/android/smartfollow/f;

    invoke-direct {v3, v0, v6, v7}, Lcom/twitter/android/smartfollow/c;-><init>(Lcom/twitter/android/twitterflows/h;Lcom/twitter/android/twitterflows/b;Lcom/twitter/android/smartfollow/f;)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/a$a;)V

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 203
    invoke-virtual {v0}, Lcom/twitter/android/twitterflows/h;->a()I

    move-result v0

    iget-object v3, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 205
    check-cast v0, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    iget-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->setPresenter(Lcom/twitter/android/smartfollow/a;)V

    .line 206
    iget-object v0, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v1, Lflow/Flow$Direction;->c:Lflow/Flow$Direction;

    if-ne v0, v1, :cond_4

    if-eqz v4, :cond_4

    .line 208
    invoke-interface {v4, v3}, Lflow/f;->b(Landroid/view/View;)V

    .line 220
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->f:Landroid/widget/FrameLayout;

    iget-object v4, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    move-object v0, p0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Flow$Direction;Lflow/Flow$d;)V

    goto :goto_0

    .line 209
    :cond_4
    iget-object v0, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v1, Lflow/Flow$Direction;->a:Lflow/Flow$Direction;

    if-ne v0, v1, :cond_5

    .line 210
    invoke-virtual {v5}, Lflow/b;->e()Lflow/f;

    move-result-object v0

    .line 211
    invoke-interface {v0, v3}, Lflow/f;->b(Landroid/view/View;)V

    .line 212
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->i()V

    goto :goto_2

    .line 213
    :cond_5
    iget-object v0, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v1, Lflow/Flow$Direction;->b:Lflow/Flow$Direction;

    if-ne v0, v1, :cond_3

    .line 217
    invoke-direct {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->i()V

    goto :goto_2

    :cond_6
    move-object v4, v0

    goto :goto_1
.end method

.method protected b()Lcom/twitter/android/twitterflows/d;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->e:Lcom/twitter/android/twitterflows/a;

    invoke-interface {v0}, Lcom/twitter/android/twitterflows/a;->b()Lcom/twitter/android/twitterflows/d;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lcom/twitter/android/smartfollow/w;
    .locals 5

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Intent;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/twitter/analytics/model/c;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/d;->a()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "smart_follow_flow"

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-static {}, Lcom/twitter/android/smartfollow/b;->a()Lcom/twitter/android/smartfollow/b$a;

    move-result-object v2

    .line 130
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/smartfollow/b$a;->a(Lamu;)Lcom/twitter/android/smartfollow/b$a;

    move-result-object v2

    new-instance v3, Lano;

    new-instance v4, Lcom/twitter/android/smartfollow/h;

    .line 132
    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/d;->e()I

    move-result v0

    invoke-direct {v4, v0, v1}, Lcom/twitter/android/smartfollow/h;-><init>(ILcom/twitter/analytics/model/c;)V

    invoke-direct {v3, p1, v4}, Lano;-><init>(Lank;Lanr;)V

    .line 131
    invoke-virtual {v2, v3}, Lcom/twitter/android/smartfollow/b$a;->a(Lano;)Lcom/twitter/android/smartfollow/b$a;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/b$a;->a()Lcom/twitter/android/smartfollow/w;

    move-result-object v0

    .line 128
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d(Lank;)Lcom/twitter/android/smartfollow/w;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d(Lank;)Lcom/twitter/android/smartfollow/w;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d(Lank;)Lcom/twitter/android/smartfollow/w;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 167
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/smartfollow/a;->a(IILandroid/content/Intent;)V

    .line 170
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/a;->q()V

    .line 162
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/a;->n()V

    .line 153
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->onPause()V

    .line 154
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/smartfollow/a;->a(I[Ljava/lang/String;[I)V

    .line 229
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 145
    const-string/jumbo v0, "flow_data"

    iget-object v1, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->d:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    return-void
.end method

.method protected p_()V
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/a;

    const/4 v1, 0x0

    const-string/jumbo v2, "logout"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->p_()V

    .line 244
    return-void
.end method
