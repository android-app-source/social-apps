.class public abstract Lcom/twitter/android/smartfollow/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lala;
.implements Lang;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/smartfollow/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Landroid/os/Parcelable;",
        "T:",
        "Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;",
        ">",
        "Ljava/lang/Object;",
        "Lala;",
        "Lang",
        "<TS;>;"
    }
.end annotation


# instance fields
.field protected a:J

.field b:Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field c:Landroid/content/Context;

.field d:Ljava/lang/String;

.field e:Lcom/twitter/android/twitterflows/b;

.field f:Lcom/twitter/android/smartfollow/f;

.field g:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

.field h:Lcom/twitter/android/twitterflows/h;

.field i:Lcom/twitter/android/smartfollow/a$a;

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method public a(I[Ljava/lang/String;[I)V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 65
    iput-wide p1, p0, Lcom/twitter/android/smartfollow/a;->a:J

    .line 66
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->c:Landroid/content/Context;

    .line 70
    return-void
.end method

.method public final a(Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 44
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->b:Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->p()V

    .line 46
    return-void
.end method

.method public final a(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->g:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    .line 58
    return-void
.end method

.method public final a(Lcom/twitter/android/smartfollow/a$a;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->i:Lcom/twitter/android/smartfollow/a$a;

    .line 54
    return-void
.end method

.method public final a(Lcom/twitter/android/smartfollow/f;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->f:Lcom/twitter/android/smartfollow/f;

    .line 50
    return-void
.end method

.method public final a(Lcom/twitter/android/twitterflows/b;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->e:Lcom/twitter/android/twitterflows/b;

    .line 74
    return-void
.end method

.method public a(Lcom/twitter/android/twitterflows/h;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->h:Lcom/twitter/android/twitterflows/h;

    .line 86
    return-void
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->e:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_1

    .line 159
    invoke-virtual {p0, v1}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/f;)V

    .line 160
    invoke-virtual {p0, v1}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/a$a;)V

    .line 161
    invoke-virtual {p0, v1}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/twitterflows/b;)V

    .line 162
    invoke-virtual {p0, v1}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V

    .line 163
    invoke-virtual {p0, v1}, Lcom/twitter/android/smartfollow/a;->c_(Ljava/lang/String;)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 166
    iput-object v1, p0, Lcom/twitter/android/smartfollow/a;->b:Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    .line 167
    iput-object v1, p0, Lcom/twitter/android/smartfollow/a;->c:Landroid/content/Context;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 199
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/smartfollow/a;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 211
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->o()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 213
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    invoke-virtual {v0, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 217
    :cond_0
    if-eqz p4, :cond_1

    .line 218
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    .line 221
    :cond_1
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 222
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/twitter/android/smartfollow/a;->k:Z

    .line 78
    return-void
.end method

.method protected final b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->b:Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->j:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public c()Landroid/os/Parcelable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/twitter/android/smartfollow/a;->d:Ljava/lang/String;

    .line 62
    return-void
.end method

.method protected final d()Lcom/twitter/android/smartfollow/f;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->f:Lcom/twitter/android/smartfollow/f;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/f;

    return-object v0
.end method

.method protected final f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->g:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    return-object v0
.end method

.method public g()Landroid/content/Context;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string/jumbo v0, "smart_follow_flow"

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/twitter/android/smartfollow/a;->k:Z

    return v0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->b(Z)V

    .line 141
    :cond_0
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->b(Z)V

    .line 147
    :cond_0
    return-void
.end method

.method public n()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method protected abstract o()Ljava/lang/String;
.end method

.method protected p()V
    .locals 2

    .prologue
    .line 226
    const/4 v0, 0x0

    const-string/jumbo v1, "impression"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    .line 231
    const/4 v0, 0x0

    const-string/jumbo v1, "back"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method public r()V
    .locals 3

    .prologue
    .line 236
    const/4 v0, 0x0

    const-string/jumbo v1, "skip"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->i:Lcom/twitter/android/smartfollow/a$a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->i:Lcom/twitter/android/smartfollow/a$a;

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/smartfollow/a$a;->a(Landroid/view/View;Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V

    .line 240
    :cond_0
    return-void
.end method

.method public s()V
    .locals 3

    .prologue
    .line 244
    const/4 v0, 0x0

    const-string/jumbo v1, "next"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->i:Lcom/twitter/android/smartfollow/a$a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/twitter/android/smartfollow/a;->i:Lcom/twitter/android/smartfollow/a$a;

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->b()Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/smartfollow/a;->f()Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/smartfollow/a$a;->b(Landroid/view/View;Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V

    .line 248
    :cond_0
    return-void
.end method
