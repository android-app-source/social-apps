.class public Lcom/twitter/android/smartfollow/d;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/android/smartfollow/d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 23
    return-void
.end method

.method private constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/app/common/base/h;-><init>(Landroid/content/Intent;)V

    .line 27
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/android/smartfollow/d;
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/twitter/android/smartfollow/d;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/d;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/twitter/android/smartfollow/d;
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_flow_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 51
    return-object p0
.end method

.method public a(Lcom/twitter/model/stratostore/SourceLocation;)Lcom/twitter/android/smartfollow/d;
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_source_location"

    invoke-virtual {p1}, Lcom/twitter/model/stratostore/SourceLocation;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 46
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/smartfollow/d;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_make_home_timeline_request"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 36
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_scribe_page"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_htl_request_context"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_htl_request_context"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_make_home_timeline_request"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d()Lcom/twitter/model/stratostore/SourceLocation;
    .locals 4

    .prologue
    .line 72
    invoke-static {}, Lcom/twitter/model/stratostore/SourceLocation;->values()[Lcom/twitter/model/stratostore/SourceLocation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v2, "extra_source_location"

    sget-object v3, Lcom/twitter/model/stratostore/SourceLocation;->a:Lcom/twitter/model/stratostore/SourceLocation;

    invoke-virtual {v3}, Lcom/twitter/model/stratostore/SourceLocation;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public e()I
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/smartfollow/d;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_flow_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
