.class Lcom/twitter/android/SearchPhotosFragment$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/SearchPhotosFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/av",
        "<",
        "Landroid/view/View;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/SearchPhotosFragment;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/android/SearchPhotosFragment;)V
    .locals 1

    .prologue
    .line 307
    iput-object p1, p0, Lcom/twitter/android/SearchPhotosFragment$a;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment$a;->b:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/SearchPhotosFragment;Lcom/twitter/android/SearchPhotosFragment$1;)V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0, p1}, Lcom/twitter/android/SearchPhotosFragment$a;-><init>(Lcom/twitter/android/SearchPhotosFragment;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 312
    .line 313
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaListFragment$c;

    iget-object v0, v0, Lcom/twitter/android/MediaListFragment$c;->a:Lcom/twitter/android/MediaListFragment$b;

    .line 314
    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment$a;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchPhotosFragment;->b(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0}, Lcom/twitter/android/MediaListFragment$b;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 315
    invoke-interface {v0}, Lcom/twitter/android/MediaListFragment$b;->d()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    .line 316
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment$a;->b:Ljava/util/Set;

    iget-object v3, v1, Lcgi;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 317
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v2, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 319
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment$a;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-virtual {v1}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 320
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0}, Lcom/twitter/android/MediaListFragment$b;->d()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    const/4 v2, 0x0

    .line 319
    invoke-static {v1, v0, v2}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 323
    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment$a;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchPhotosFragment;->c(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    :cond_1
    return-void
.end method
