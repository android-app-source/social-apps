.class public Lcom/twitter/android/CollectionPermalinkActivity;
.super Lcom/twitter/android/UserQueryActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/android/UserQueryActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/android/ListFragmentActivity$a;
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/app/collections/CollectionPermalinkFragment;

    invoke-direct {v0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;-><init>()V

    .line 44
    new-instance v1, Lcom/twitter/app/collections/a$a;

    .line 45
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/app/collections/a$a;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1}, Lcom/twitter/app/collections/a$a;->a()Lcom/twitter/app/collections/a;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 46
    new-instance v1, Lcom/twitter/android/ListFragmentActivity$a;

    invoke-direct {v1, v0}, Lcom/twitter/android/ListFragmentActivity$a;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    return-object v1
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 53
    return-object p2
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f0a040e

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionPermalinkActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 75
    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f1302e4

    .line 76
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/timeline/TimelineFragment;

    .line 77
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 80
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/twitter/android/UserQueryActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkActivity;->b()V

    .line 60
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/twitter/android/UserQueryActivity;->onStart()V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f1302e4

    .line 66
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/timeline/TimelineFragment;

    .line 67
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/timeline/TimelineFragment;->d(Z)V

    .line 68
    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aP()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 69
    invoke-static {}, Lbgd;->a()Lbgd;

    move-result-object v0

    invoke-virtual {v0}, Lbgd;->b()V

    .line 70
    return-void

    .line 67
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
