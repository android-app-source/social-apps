.class public Lcom/twitter/android/dialog/a$a;
.super Lcom/twitter/android/dialog/g$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/dialog/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/twitter/android/dialog/g$b;-><init>(I)V

    .line 31
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/android/dialog/a$a;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/dialog/a$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    return-object p0
.end method

.method protected af_()V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/dialog/a$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Landroid/support/v4/app/Fragment$InstantiationException;

    const-string/jumbo v1, "Missing scribe page"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v4/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0

    .line 44
    :cond_0
    return-void
.end method

.method protected b()Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;

    invoke-direct {v0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;-><init>()V

    return-object v0
.end method

.method protected synthetic c()Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/android/dialog/a$a;->b()Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Lcom/twitter/android/dialog/TakeoverDialogFragment;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/android/dialog/a$a;->b()Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;

    move-result-object v0

    return-object v0
.end method
