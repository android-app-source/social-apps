.class public abstract Lcom/twitter/android/dialog/h$a;
.super Lcom/twitter/android/dialog/g$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/dialog/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Lcom/twitter/android/dialog/h$a",
        "<T",
        "L;",
        ">;>",
        "Lcom/twitter/android/dialog/g$a",
        "<T",
        "L;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/android/dialog/g$a;-><init>(I)V

    .line 34
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/n;)Lcom/twitter/android/dialog/h$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/n;",
            ")T",
            "L;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/dialog/h$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "prompt"

    sget-object v2, Lcom/twitter/model/timeline/n;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 43
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/h$a;

    return-object v0
.end method

.method public b(Lcom/twitter/model/timeline/n;)Lcom/twitter/android/dialog/h$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/n;",
            ")T",
            "L;"
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/twitter/android/dialog/h$a;->a(Lcom/twitter/model/timeline/n;)Lcom/twitter/android/dialog/h$a;

    .line 49
    iget-object v0, p1, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/dialog/h$a;->b(Ljava/lang/String;)Lcom/twitter/android/dialog/g$a;

    .line 50
    iget-object v0, p1, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/dialog/h$a;->c(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    .line 51
    iget-object v0, p1, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/dialog/h$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    .line 52
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/h$a;

    return-object v0
.end method
