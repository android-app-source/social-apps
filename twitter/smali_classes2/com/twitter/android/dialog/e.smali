.class public Lcom/twitter/android/dialog/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/twitter/android/dialog/e;->a:Landroid/content/Context;

    .line 18
    return-void
.end method

.method private static b(Ljava/util/List;)[Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/dialog/d;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 36
    new-array v3, v2, [Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 38
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/d;

    iget-object v0, v0, Lcom/twitter/android/dialog/d;->a:Ljava/lang/String;

    aput-object v0, v3, v1

    .line 37
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 40
    :cond_0
    return-object v3
.end method


# virtual methods
.method public a(Ljava/util/List;)Landroid/app/Dialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/dialog/d;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .prologue
    .line 22
    invoke-static {p1}, Lcom/twitter/android/dialog/e;->b(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    .line 23
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/twitter/android/dialog/e;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/twitter/android/dialog/e$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/dialog/e$1;-><init>(Lcom/twitter/android/dialog/e;Ljava/util/List;)V

    .line 24
    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 31
    return-object v0
.end method
