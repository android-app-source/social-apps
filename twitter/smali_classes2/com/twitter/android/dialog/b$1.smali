.class final Lcom/twitter/android/dialog/b$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/dialog/b;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/twitter/android/dialog/b$1;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/dialog/b$1;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/dialog/b$1;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/twitter/android/dialog/b$1;->d:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 33
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 34
    iget-object v2, p0, Lcom/twitter/android/dialog/b$1;->a:Ljava/lang/String;

    .line 35
    invoke-static {v2, v0, v1}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;J)Lcom/twitter/android/people/ai;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/twitter/android/dialog/b$1;->b:Ljava/lang/String;

    const-string/jumbo v2, "learn_more"

    const-string/jumbo v3, "click"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 38
    iget-object v0, p0, Lcom/twitter/android/dialog/b$1;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/dialog/b$1;->c:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/twitter/android/dialog/b$1;->d:Landroid/content/res/Resources;

    const v3, 0x7f0a0c47

    .line 39
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 40
    return-void
.end method
