.class public Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/android/dialog/h;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/dialog/h;->a(Landroid/os/Bundle;)Lcom/twitter/android/dialog/h;

    move-result-object v0

    return-object v0
.end method

.method protected a(I)V
    .locals 4

    .prologue
    .line 66
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->b:Landroid/content/Context;

    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->l()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->m()Lcom/twitter/model/timeline/n;

    move-result-object v3

    iget v3, v3, Lcom/twitter/model/timeline/n;->c:I

    invoke-static {v1, v2, p1, v3}, Lcom/twitter/library/api/p;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;II)Lcom/twitter/library/api/p;

    move-result-object v1

    .line 66
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 68
    return-void
.end method

.method public synthetic b()Lcom/twitter/android/dialog/g;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->a()Lcom/twitter/android/dialog/h;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/android/dialog/f;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->a()Lcom/twitter/android/dialog/h;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->a()Lcom/twitter/android/dialog/h;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 32
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->h()V

    .line 33
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->a(I)V

    .line 34
    return-void
.end method

.method protected i()V
    .locals 9
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->i()V

    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->m()Lcom/twitter/model/timeline/n;

    move-result-object v0

    iget-object v3, v0, Lcom/twitter/model/timeline/n;->g:Ljava/lang/String;

    .line 46
    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 48
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    .line 47
    invoke-static/range {v1 .. v8}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 50
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->a(I)V

    .line 51
    return-void
.end method

.method protected k()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 61
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->k()V

    .line 62
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->a(I)V

    .line 63
    return-void
.end method

.method protected m()Lcom/twitter/model/timeline/n;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/android/dialog/TakeoverPromptDialogFragment;->a()Lcom/twitter/android/dialog/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/dialog/h;->a()Lcom/twitter/model/timeline/n;

    move-result-object v0

    return-object v0
.end method
