.class public Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f1100c8

    invoke-static {p0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    aput-object v2, v0, v1

    .line 41
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "{{}}"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/dialog/a;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/dialog/a;->a(Landroid/os/Bundle;)Lcom/twitter/android/dialog/a;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->a(Landroid/app/Dialog;Landroid/os/Bundle;)V

    .line 30
    const v0, 0x7f13001c

    invoke-virtual {p0, v0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 31
    invoke-virtual {p0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 32
    invoke-virtual {p0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->a()Lcom/twitter/android/dialog/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/dialog/a;->a()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "contacts_sync_prompt"

    .line 31
    invoke-static {v1, v0, v2, v3}, Lcom/twitter/android/dialog/b;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1100c9

    invoke-static {v1, v2}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    return-void
.end method

.method public synthetic b()Lcom/twitter/android/dialog/g;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->a()Lcom/twitter/android/dialog/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/android/dialog/f;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->a()Lcom/twitter/android/dialog/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/android/dialog/AddressbookTakeoverDialogFragment;->a()Lcom/twitter/android/dialog/a;

    move-result-object v0

    return-object v0
.end method
