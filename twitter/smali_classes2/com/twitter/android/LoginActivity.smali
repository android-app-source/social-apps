.class public Lcom/twitter/android/LoginActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/ui/widget/TwitterEditText$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/LoginActivity$a;,
        Lcom/twitter/android/LoginActivity$b;
    }
.end annotation


# static fields
.field private static final b:[I


# instance fields
.field a:Z

.field private final c:Lcom/twitter/android/LoginActivity$a;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/ui/widget/TwitterEditText;

.field private h:Lcom/twitter/ui/widget/TwitterEditText;

.field private i:Landroid/widget/Button;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:I

.field private m:Z

.field private n:Lcom/twitter/android/util/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 101
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010454

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/android/LoginActivity;->b:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 118
    new-instance v0, Lcom/twitter/android/LoginActivity$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/LoginActivity$a;-><init>(Lcom/twitter/android/LoginActivity;)V

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->c:Lcom/twitter/android/LoginActivity$a;

    .line 122
    const-string/jumbo v0, "no_prefill"

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    .line 123
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/LoginActivity;I)I
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Lcom/twitter/android/LoginActivity;->d:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/LoginActivity;)Lcom/twitter/ui/widget/TwitterEditText;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 401
    const-string/jumbo v0, "native_password_reset_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PasswordResetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "account_id"

    .line 403
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 404
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 408
    :goto_0
    return-void

    .line 406
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/twitter/android/util/o;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/LoginActivity;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/twitter/android/LoginActivity;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/net/Uri;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 462
    const-string/jumbo v0, "native_password_reset_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    const-string/jumbo v0, "screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 466
    const-string/jumbo v1, "login_verification_user_id"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 467
    const-string/jumbo v2, "login_verification_request_id"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 468
    const-string/jumbo v2, "login_verification_cause"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 469
    const-string/jumbo v2, "login_verification_request_url"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 470
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 471
    invoke-static {v4}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 476
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 477
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/LoginActivity;->l:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 481
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v8

    .line 482
    iget v0, p0, Lcom/twitter/android/LoginActivity;->l:I

    if-ne v0, v7, :cond_2

    .line 483
    const-string/jumbo v0, "login_verification_type"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 484
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 489
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    .line 493
    iput-boolean v7, p0, Lcom/twitter/android/LoginActivity;->k:Z

    .line 494
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->c:Lcom/twitter/android/LoginActivity$a;

    new-instance v1, Lcom/twitter/model/account/LoginVerificationRequiredResponse;

    iget v7, p0, Lcom/twitter/android/LoginActivity;->l:I

    invoke-direct/range {v1 .. v7}, Lcom/twitter/model/account/LoginVerificationRequiredResponse;-><init>(JLjava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v0, v8, v1}, Lcom/twitter/android/LoginActivity$a;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginVerificationRequiredResponse;)V

    goto :goto_0

    .line 498
    :cond_2
    invoke-virtual {p0, v7}, Lcom/twitter/android/LoginActivity;->showDialog(I)V

    .line 499
    iput-boolean v7, p0, Lcom/twitter/android/LoginActivity;->k:Z

    .line 500
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/LoginActivity;->c:Lcom/twitter/android/LoginActivity$a;

    move-object v1, v8

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 490
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 478
    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/LoginActivity;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->k:Z

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/LoginActivity;)I
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lcom/twitter/android/LoginActivity;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/twitter/android/LoginActivity;->d:I

    return v0
.end method

.method static synthetic i(Lcom/twitter/android/LoginActivity;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->q()V

    return-void
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 425
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->l()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    .line 426
    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 425
    invoke-static {v0, v1}, Lcom/twitter/library/util/af;->a(ZLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    invoke-static {p0}, Lcom/twitter/library/util/af;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 424
    :goto_0
    return v0

    .line 426
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 442
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-static {p0, v0, v7}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 444
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->r()Ljava/lang/String;

    move-result-object v1

    .line 445
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 447
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "login:identifier:%s::prefill_changed"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    aput-object v6, v5, v7

    .line 449
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 448
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 452
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "login:form:::submit"

    aput-object v4, v3, v7

    .line 453
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 454
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 456
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/LoginActivity;->c:Lcom/twitter/android/LoginActivity$a;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$d;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Ljava/lang/String;

    .line 458
    invoke-virtual {p0, v8}, Lcom/twitter/android/LoginActivity;->showDialog(I)V

    .line 459
    return-void
.end method

.method static synthetic j(Lcom/twitter/android/LoginActivity;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->n()V

    return-void
.end method

.method static synthetic k(Lcom/twitter/android/LoginActivity;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->o()V

    return-void
.end method

.method static synthetic l(Lcom/twitter/android/LoginActivity;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/twitter/android/LoginActivity;->d:I

    return v0
.end method

.method private l()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 525
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "login:form::signup:click"

    aput-object v4, v3, v0

    .line 526
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 525
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 528
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 529
    new-instance v3, Lcom/twitter/android/v$a;

    invoke-direct {v3}, Lcom/twitter/android/v$a;-><init>()V

    iget-boolean v4, p0, Lcom/twitter/android/LoginActivity;->m:Z

    .line 530
    invoke-virtual {v3, v4}, Lcom/twitter/android/v$a;->b(Z)Lcom/twitter/android/v$a;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/LoginActivity;->m:Z

    if-nez v4, :cond_0

    move v0, v1

    .line 531
    :cond_0
    invoke-virtual {v3, v0}, Lcom/twitter/android/v$a;->c(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/android/LoginActivity;->a:Z

    .line 532
    invoke-virtual {v0, v3}, Lcom/twitter/android/v$a;->a(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/LoginActivity;->n:Lcom/twitter/android/util/ab;

    .line 533
    invoke-interface {v3}, Lcom/twitter/android/util/ab;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/v$a;->d(Z)Lcom/twitter/android/v$a;

    move-result-object v3

    .line 534
    const-string/jumbo v0, "android.intent.extra.INTENT"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    const-string/jumbo v0, "android.intent.extra.INTENT"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v3, v0}, Lcom/twitter/android/v$a;->a(Landroid/content/Intent;)Lcom/twitter/android/v$a;

    .line 537
    :cond_1
    new-instance v2, Lcom/twitter/android/FlowActivity$a;

    invoke-direct {v2, p0}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v3}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lako;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/FlowActivity$a;->a(Lako;I)V

    .line 538
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 615
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09ca

    .line 616
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09c9

    .line 617
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0616

    .line 618
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a03cf

    .line 619
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 620
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 621
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 622
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login::use_temporary_password_prompt::impression"

    aput-object v3, v1, v2

    .line 623
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 622
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 624
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    const v2, 0x7f0a0771

    .line 627
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 628
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0772

    .line 629
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0995

    .line 630
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 631
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 632
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 633
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 634
    return-void
.end method

.method private q()V
    .locals 4

    .prologue
    .line 638
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a04cc

    .line 639
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0616

    .line 640
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 641
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 642
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 643
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login::ambiguity_alert::impression"

    aput-object v3, v1, v2

    .line 644
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 643
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 645
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 4

    .prologue
    .line 688
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login::::success"

    aput-object v3, v1, v2

    .line 689
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 690
    const-string/jumbo v1, "app_download_client_event"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 691
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v1

    .line 692
    const-string/jumbo v2, "4"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 693
    sget-object v1, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v1}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v1

    .line 694
    if-eqz v1, :cond_0

    .line 695
    const-string/jumbo v2, "6"

    invoke-virtual {v1}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 696
    invoke-virtual {v1}, Lcom/twitter/library/api/c;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 698
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 699
    return-void
.end method

.method private t()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 729
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    invoke-static {p0}, Lcom/twitter/library/network/f;->a(Landroid/content/Context;)Lcom/twitter/library/network/e;

    move-result-object v0

    .line 731
    invoke-interface {v0}, Lcom/twitter/library/network/e;->c()Ljava/lang/String;

    move-result-object v0

    .line 732
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 733
    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 734
    const-string/jumbo v1, "email"

    iput-object v1, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    .line 735
    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Ljava/lang/String;

    .line 737
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->requestFocus()Z

    .line 740
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "login:identifier:%s::prefill"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    aput-object v4, v3, v5

    .line 741
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 740
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 742
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    .line 748
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    .line 749
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0403ef

    .line 751
    invoke-static {p0}, Lcom/twitter/android/cw;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 752
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 754
    new-instance v1, Lcom/twitter/android/LoginActivity$b;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/LoginActivity$b;-><init>(Lcom/twitter/android/LoginActivity;Lcom/twitter/internal/android/widget/PopupEditText;)V

    .line 755
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 139
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->n:Lcom/twitter/android/util/ab;

    if-nez v0, :cond_0

    .line 140
    invoke-static {p0}, Lcom/twitter/android/util/ac;->a(Landroid/content/Context;)Lcom/twitter/android/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->n:Lcom/twitter/android/util/ab;

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x7f0d0310

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 143
    invoke-static {p0}, Lcom/twitter/android/bw;->a(Landroid/app/Activity;)V

    .line 144
    const v0, 0x7f04018b

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 145
    invoke-virtual {p2, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 146
    invoke-virtual {p2, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 147
    invoke-virtual {p2, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 148
    invoke-virtual {p2, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 149
    return-object p2
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 4

    .prologue
    const/4 v0, -0x2

    .line 582
    packed-switch p2, :pswitch_data_0

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 584
    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 585
    const v0, 0x7f0a0be8

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->a(I)V

    goto :goto_0

    .line 591
    :pswitch_1
    if-ne p3, v0, :cond_0

    .line 592
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const v2, 0x7f0a0bcf

    .line 593
    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 592
    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 594
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login::use_temporary_password_prompt:get_help:click"

    aput-object v3, v1, v2

    .line 595
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 594
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 601
    :pswitch_2
    if-ne p3, v0, :cond_0

    .line 602
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const v2, 0x7f0a0be6

    .line 603
    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 602
    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 582
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 655
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    .line 656
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 658
    iget-boolean v1, p0, Lcom/twitter/android/LoginActivity;->a:Z

    .line 659
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v4

    .line 658
    invoke-static {p0, p1, v1, p2, v4}, Lcom/twitter/android/am;->a(Landroid/app/Activity;Lcom/twitter/library/client/Session;ZLjava/lang/String;Lcom/twitter/library/client/v;)V

    .line 661
    iget-boolean v1, p0, Lcom/twitter/android/LoginActivity;->a:Z

    invoke-static {p0, v1, v2, v3}, Lcom/twitter/android/am;->a(Landroid/app/Activity;ZJ)V

    .line 663
    iget-boolean v1, p0, Lcom/twitter/android/LoginActivity;->m:Z

    if-nez v1, :cond_1

    .line 664
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/DispatchActivity;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 665
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "android.intent.extra.INTENT"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 666
    const-string/jumbo v4, "android.intent.extra.INTENT"

    .line 667
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "android.intent.extra.INTENT"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    .line 666
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 669
    :cond_0
    const/high16 v4, 0x4000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 672
    :cond_1
    const/4 v1, -0x1

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v5, "AbsFragmentActivity_account_name"

    .line 673
    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 672
    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 675
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/util/AppEventTrack$EventType;->c:Lcom/twitter/android/util/AppEventTrack$EventType;

    new-array v4, v7, [Ljava/lang/String;

    invoke-static {v0, v1, v4}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/android/util/AppEventTrack$EventType;[Ljava/lang/String;)V

    .line 676
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->s()V

    .line 678
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, "login:identifier:%s::success"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v2, v3, v0}, Lcom/twitter/android/am;->a(J[Ljava/lang/String;)V

    .line 680
    const-string/jumbo v0, "login::::success"

    invoke-static {p0, v2, v3, v0, v7}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;JLjava/lang/String;Z)V

    .line 681
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-static {p0, p1}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbbg;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 682
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->finish()V

    .line 683
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 507
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 508
    const v2, 0x7f1308c9

    if-ne v1, v2, :cond_0

    .line 509
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/settings/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 519
    :goto_0
    return v0

    .line 511
    :cond_0
    const v2, 0x7f1308cb

    if-ne v1, v2, :cond_1

    .line 512
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/AdvancedDiscoSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "extra_is_signup_process"

    .line 513
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 512
    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 515
    :cond_1
    const v2, 0x7f1308ca

    if-ne v1, v2, :cond_2

    .line 516
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/settings/ProxySettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 519
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 2

    .prologue
    .line 347
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    .line 348
    const v0, 0x7f140027

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 349
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 350
    invoke-virtual {v0}, Lcof;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    :cond_0
    const v0, 0x7f140028

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 353
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/twitter/ui/widget/TwitterEditText;)Z
    .locals 4

    .prologue
    const/16 v3, 0x91

    .line 703
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    if-ne v0, p1, :cond_1

    .line 704
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 705
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getSelectionStart()I

    move-result v0

    .line 706
    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getSelectionEnd()I

    move-result v1

    .line 707
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TwitterEditText;->getInputType()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 708
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setInputType(I)V

    .line 709
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    sget-object v3, Lcom/twitter/android/LoginActivity;->b:[I

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    .line 715
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setSelection(II)V

    .line 716
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 717
    const/4 v0, 0x1

    .line 719
    :goto_1
    return v0

    .line 711
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    const/16 v3, 0x81

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setInputType(I)V

    .line 712
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    goto :goto_0

    .line 719
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->i:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 421
    return-void
.end method

.method public b(Lcmr;)I
    .locals 4

    .prologue
    .line 337
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 338
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04018c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 340
    const v2, 0x7f130469

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 342
    const/4 v0, 0x2

    return v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 156
    const v0, 0x7f0a04db

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->setTitle(I)V

    .line 158
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 161
    const-string/jumbo v0, "add_account"

    invoke-virtual {v2, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    .line 164
    const-string/jumbo v0, "authorize_account"

    invoke-virtual {v2, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity;->m:Z

    .line 166
    const v0, 0x7f13046e

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    .line 167
    const v0, 0x7f13046f

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    .line 168
    const v0, 0x7f130470

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->i:Landroid/widget/Button;

    .line 170
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnStatusIconClickListener(Lcom/twitter/ui/widget/TwitterEditText$a;)V

    .line 176
    const v0, 0x7f1301e3

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iput v7, p0, Lcom/twitter/android/LoginActivity;->d:I

    .line 180
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 184
    if-eqz v3, :cond_2

    .line 185
    const-string/jumbo v0, "screen_name"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 186
    const-string/jumbo v0, "password"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 192
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 195
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 196
    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v6, "login:::username:prefill"

    aput-object v6, v2, v7

    invoke-static {v4, v5, v2}, Lcom/twitter/android/am;->a(J[Ljava/lang/String;)V

    .line 197
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 198
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 199
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->requestFocus()Z

    .line 208
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    new-instance v1, Lcom/twitter/android/LoginActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/LoginActivity$1;-><init>(Lcom/twitter/android/LoginActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 225
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    new-instance v1, Lcom/twitter/android/LoginActivity$2;

    invoke-direct {v1, p0, v4, v5}, Lcom/twitter/android/LoginActivity$2;-><init>(Lcom/twitter/android/LoginActivity;J)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 235
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->i:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 237
    const v0, 0x7f1303a0

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 238
    const v1, 0x7f0a04dc

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    .line 240
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/library/util/af;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    invoke-static {}, Lcom/twitter/library/util/b;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Lcom/twitter/ui/widget/TwitterEditText;

    const-string/jumbo v1, "@twitter.com email"

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setLabelText(Ljava/lang/CharSequence;)V

    .line 245
    :cond_0
    if-nez p1, :cond_6

    .line 246
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "login::::impression"

    aput-object v2, v1, v7

    .line 247
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 248
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 250
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "login"

    aput-object v0, v2, v7

    const-string/jumbo v0, ""

    aput-object v0, v2, v8

    const/4 v4, 0x2

    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    if-eqz v0, :cond_5

    const-string/jumbo v0, "switch_account"

    :goto_2
    aput-object v0, v2, v4

    const/4 v0, 0x3

    const-string/jumbo v4, ""

    aput-object v4, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v2, v0

    .line 251
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 253
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 255
    const-string/jumbo v0, "login"

    invoke-static {p0, v0}, Lcom/twitter/android/al;->b(Landroid/app/Activity;Ljava/lang/String;)V

    .line 257
    if-eqz v3, :cond_1

    .line 258
    invoke-direct {p0, v3}, Lcom/twitter/android/LoginActivity;->b(Landroid/net/Uri;)V

    .line 261
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->t()V

    .line 272
    :goto_3
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->u()V

    .line 273
    return-void

    .line 188
    :cond_2
    const-string/jumbo v0, "screen_name"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 189
    const-string/jumbo v0, "password"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 201
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->h:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->i:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_1

    .line 205
    :cond_4
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, "login:::username:prefill_fail"

    aput-object v1, v0, v7

    invoke-static {v4, v5, v0}, Lcom/twitter/android/am;->a(J[Ljava/lang/String;)V

    goto/16 :goto_1

    .line 250
    :cond_5
    const-string/jumbo v0, "logged_out"

    goto :goto_2

    .line 263
    :cond_6
    const-string/jumbo v0, "reqId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Ljava/lang/String;

    .line 264
    const-string/jumbo v0, "passwordResetLogin"

    invoke-virtual {p1, v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity;->k:Z

    .line 265
    const-string/jumbo v0, "loginVerificationCause"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/LoginActivity;->l:I

    .line 266
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->c:Lcom/twitter/android/LoginActivity$a;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lcom/twitter/library/client/v$f;)V

    .line 268
    const-string/jumbo v0, "prefill_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    .line 269
    const-string/jumbo v0, "prefill_value"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Ljava/lang/String;

    goto :goto_3
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 412
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 288
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 289
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->e(Ljava/lang/String;)V

    .line 290
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 542
    packed-switch p1, :pswitch_data_0

    .line 573
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 544
    :pswitch_0
    if-ne p2, v2, :cond_0

    .line 545
    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    if-nez v0, :cond_1

    .line 546
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 547
    if-eqz v0, :cond_1

    .line 548
    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 552
    :cond_1
    invoke-virtual {p0, v2, p3}, Lcom/twitter/android/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 553
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->finish()V

    goto :goto_0

    .line 559
    :pswitch_1
    if-ne p2, v2, :cond_0

    .line 560
    invoke-virtual {p0, v2, p3}, Lcom/twitter/android/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 561
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->finish()V

    goto :goto_0

    .line 567
    :pswitch_2
    if-ne p2, v2, :cond_0

    .line 568
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->b(Landroid/net/Uri;)V

    goto :goto_0

    .line 542
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    if-eqz v0, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "accountAuthenticatorResponse"

    .line 326
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    .line 327
    if-eqz v0, :cond_0

    .line 328
    const/4 v1, 0x4

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    .line 331
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 332
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 379
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 381
    :sswitch_0
    const v0, 0x7f0a0be9

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->a(I)V

    goto :goto_0

    .line 385
    :sswitch_1
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->j()V

    goto :goto_0

    .line 391
    :sswitch_2
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->l()V

    goto :goto_0

    .line 379
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f1301e3 -> :sswitch_0
        0x7f130469 -> :sswitch_2
        0x7f130470 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 358
    packed-switch p1, :pswitch_data_0

    .line 372
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 360
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 361
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 362
    if-ne p1, v3, :cond_0

    .line 363
    const v1, 0x7f0a04d9

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 367
    :goto_1
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 368
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 365
    :cond_0
    const v1, 0x7f0a08ce

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 358
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 294
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 296
    sget-object v0, Lcom/twitter/android/LoginActivity$3;->a:[I

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session$LoginStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 298
    :pswitch_0
    const-string/jumbo v0, "android.intent.action.MAIN"

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->setResult(I)V

    .line 300
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/twitter/app/main/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0

    .line 305
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->showDialog(I)V

    goto :goto_0

    .line 309
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    goto :goto_0

    .line 313
    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    goto :goto_0

    .line 296
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 278
    const-string/jumbo v0, "reqId"

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string/jumbo v0, "passwordResetLogin"

    iget-boolean v1, p0, Lcom/twitter/android/LoginActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 280
    const-string/jumbo v0, "loginVerificationCause"

    iget v1, p0, Lcom/twitter/android/LoginActivity;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 282
    const-string/jumbo v0, "prefill_type"

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string/jumbo v0, "prefill_value"

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x0

    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 416
    return-void
.end method
