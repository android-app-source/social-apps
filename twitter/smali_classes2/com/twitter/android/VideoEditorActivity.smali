.class public Lcom/twitter/android/VideoEditorActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/twitter/android/VideoEditorFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/VideoEditorActivity;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/media/EditableVideo;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/app/common/base/h;

    invoke-direct {v0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 41
    invoke-virtual {v0, p2}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    const-class v1, Lcom/twitter/android/VideoEditorActivity;

    .line 42
    invoke-virtual {v0, p0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "editable_video"

    .line 43
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/model/media/EditableVideo;
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "editable_video"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableVideo;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 78
    new-instance v0, Lcom/twitter/android/VideoEditorActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/VideoEditorActivity$1;-><init>(Lcom/twitter/android/VideoEditorActivity;)V

    .line 85
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a035f

    .line 86
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a002b

    .line 87
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0278

    .line 88
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f6

    const/4 v2, 0x0

    .line 89
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 91
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f04002a

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 56
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 3

    .prologue
    const v2, 0x7f130155

    .line 95
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 96
    const v0, 0x7f0a035f

    invoke-virtual {p0, v0}, Lcom/twitter/android/VideoEditorActivity;->setTitle(I)V

    .line 97
    invoke-virtual {p0}, Lcom/twitter/android/VideoEditorActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 98
    if-nez p1, :cond_0

    .line 99
    new-instance v1, Lcom/twitter/android/VideoEditorFragment;

    invoke-direct {v1}, Lcom/twitter/android/VideoEditorFragment;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/VideoEditorActivity;->b:Lcom/twitter/android/VideoEditorFragment;

    .line 100
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/VideoEditorActivity;->b:Lcom/twitter/android/VideoEditorFragment;

    .line 101
    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/VideoEditorFragment;

    iput-object v0, p0, Lcom/twitter/android/VideoEditorActivity;->b:Lcom/twitter/android/VideoEditorFragment;

    goto :goto_0
.end method

.method public a(Lcmm;)Z
    .locals 4

    .prologue
    .line 118
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/twitter/android/VideoEditorActivity;->b:Lcom/twitter/android/VideoEditorFragment;

    .line 120
    invoke-interface {p1}, Lcmm;->a()I

    move-result v2

    const v3, 0x7f130164

    if-ne v2, v3, :cond_0

    .line 121
    invoke-virtual {v1}, Lcom/twitter/android/VideoEditorFragment;->h()Lcom/twitter/model/media/EditableVideo;

    move-result-object v1

    .line 122
    const-string/jumbo v2, "editable_video"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 123
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/VideoEditorActivity;->setResult(ILandroid/content/Intent;)V

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/VideoEditorActivity;->finish()V

    .line 125
    const/4 v0, 0x1

    .line 127
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    .line 112
    const v0, 0x7f140031

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 145
    if-ne p1, v1, :cond_0

    .line 146
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/VideoEditorActivity;->setResult(I)V

    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/VideoEditorActivity;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/VideoEditorActivity;->b:Lcom/twitter/android/VideoEditorFragment;

    invoke-virtual {v0}, Lcom/twitter/android/VideoEditorFragment;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/twitter/android/VideoEditorActivity;->i()V

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 132
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 134
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/VideoEditorActivity;->a:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    const v1, 0x7f0a07b4

    .line 136
    invoke-virtual {p0, v1}, Lcom/twitter/android/VideoEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/VideoEditorActivity;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, p0, v2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v1, "video_trimmer:::video"

    .line 137
    invoke-virtual {v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 139
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/VideoEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 141
    :cond_0
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/VideoEditorActivity;->b:Lcom/twitter/android/VideoEditorFragment;

    invoke-virtual {v0}, Lcom/twitter/android/VideoEditorFragment;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/twitter/android/VideoEditorActivity;->i()V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->p()V

    goto :goto_0
.end method
