.class public Lcom/twitter/android/suggestionselection/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/autocomplete/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/suggestionselection/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/android/autocomplete/a;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/suggestionselection/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/suggestionselection/a$a",
            "<TT;TS;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/autocomplete/SuggestionEditText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/autocomplete/SuggestionEditText",
            "<TT;TS;>;"
        }
    .end annotation
.end field

.field private final d:Landroid/text/TextWatcher;

.field private final e:I

.field private final f:Lnh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnh",
            "<TT;>;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/suggestionselection/a$a;Landroid/text/TextWatcher;Lna;Lnh;ILjava/util/Set;Landroid/os/Bundle;Lcom/twitter/android/autocomplete/SuggestionEditText;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/android/suggestionselection/a$a",
            "<TT;TS;>;",
            "Landroid/text/TextWatcher;",
            "Lna",
            "<TT;TS;>;",
            "Lnh",
            "<TT;>;I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/os/Bundle;",
            "Lcom/twitter/android/autocomplete/SuggestionEditText",
            "<TT;TS;>;Z)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/suggestionselection/a;->g:Ljava/util/Set;

    .line 71
    iput-object p1, p0, Lcom/twitter/android/suggestionselection/a;->a:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lcom/twitter/android/suggestionselection/a;->b:Lcom/twitter/android/suggestionselection/a$a;

    .line 73
    iput-object p5, p0, Lcom/twitter/android/suggestionselection/a;->f:Lnh;

    .line 74
    iput p6, p0, Lcom/twitter/android/suggestionselection/a;->e:I

    .line 75
    iput-object p7, p0, Lcom/twitter/android/suggestionselection/a;->h:Ljava/util/Set;

    .line 76
    iput-boolean p10, p0, Lcom/twitter/android/suggestionselection/a;->i:Z

    .line 78
    iput-object p9, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 80
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    new-instance v1, Lcom/twitter/android/suggestionselection/a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/suggestionselection/a$1;-><init>(Lcom/twitter/android/suggestionselection/a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSuggestionListener(Lcom/twitter/android/autocomplete/SuggestionEditText$e;)V

    .line 97
    new-instance v0, Lcom/twitter/android/suggestionselection/a$2;

    invoke-direct {v0, p0, p3}, Lcom/twitter/android/suggestionselection/a$2;-><init>(Lcom/twitter/android/suggestionselection/a;Landroid/text/TextWatcher;)V

    iput-object v0, p0, Lcom/twitter/android/suggestionselection/a;->d:Landroid/text/TextWatcher;

    .line 148
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a;->d:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p4}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSuggestionProvider(Lna;)V

    .line 151
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p5}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setTokenizer(Lnj;)V

    .line 152
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/twitter/android/suggestionselection/a;->a(I)V

    .line 156
    :cond_0
    if-eqz p8, :cond_1

    .line 157
    invoke-direct {p0, p8}, Lcom/twitter/android/suggestionselection/a;->a(Landroid/os/Bundle;)V

    .line 159
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/suggestionselection/a;)Lcom/twitter/android/suggestionselection/a$a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->b:Lcom/twitter/android/suggestionselection/a$a;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 247
    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getGravity()I

    move-result v1

    and-int/lit8 v1, v1, 0x70

    or-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setGravity(I)V

    .line 248
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 162
    const-string/jumbo v0, "items"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcnx;->a:Lcom/twitter/util/serialization/l;

    .line 163
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 162
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 164
    const-string/jumbo v1, "partial_item"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 166
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 168
    if-eqz v0, :cond_2

    .line 169
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/twitter/util/collection/o;->a(I)Lcom/twitter/util/collection/o;

    move-result-object v3

    .line 170
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnx;

    .line 171
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 172
    new-instance v6, Lcny;

    iget-object v7, p0, Lcom/twitter/android/suggestionselection/a;->a:Landroid/content/Context;

    iget-boolean v8, p0, Lcom/twitter/android/suggestionselection/a;->i:Z

    invoke-direct {v6, v0, v7, v8}, Lcny;-><init>(Lcnx;Landroid/content/Context;Z)V

    .line 173
    iget-object v7, v0, Lcnx;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 174
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-virtual {v2, v6, v5, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 175
    iget-wide v6, v0, Lcnx;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto :goto_0

    .line 177
    :cond_0
    invoke-virtual {v3}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 181
    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/util/Set;)V

    .line 183
    if-eqz v1, :cond_1

    .line 184
    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 186
    :cond_1
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/lang/CharSequence;I)V

    .line 187
    return-void

    .line 179
    :cond_2
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/suggestionselection/a;I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/twitter/android/suggestionselection/a;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/suggestionselection/a;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    iput-object p1, p0, Lcom/twitter/android/suggestionselection/a;->g:Ljava/util/Set;

    .line 327
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->b:Lcom/twitter/android/suggestionselection/a$a;

    invoke-interface {v0}, Lcom/twitter/android/suggestionselection/a$a;->bi_()V

    .line 328
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/suggestionselection/a;)Lcom/twitter/android/autocomplete/SuggestionEditText;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/suggestionselection/a;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/a;->f()V

    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/a;->c()Lnh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionEnd()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lnh;->c(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 222
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 223
    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 224
    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e02f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 226
    iget-object v2, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    new-instance v3, Lcom/twitter/android/suggestionselection/a$3;

    invoke-direct {v3, p0, v1, v0}, Lcom/twitter/android/suggestionselection/a$3;-><init>(Lcom/twitter/android/suggestionselection/a;ILandroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Lcom/twitter/android/autocomplete/SuggestionEditText;->post(Ljava/lang/Runnable;)Z

    .line 240
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->a:Landroid/content/Context;

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 296
    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 297
    return-void
.end method


# virtual methods
.method public a(Landroid/text/Editable;)Landroid/os/Bundle;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 191
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v2, Lcny;

    invoke-interface {p1, v1, v0, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcny;

    .line 192
    array-length v2, v0

    invoke-static {v2}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 193
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 194
    invoke-virtual {v4}, Lcny;->a()Lcnx;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 193
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 197
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 198
    const-string/jumbo v1, "items"

    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcnx;->a:Lcom/twitter/util/serialization/l;

    .line 199
    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 198
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 200
    const-string/jumbo v1, "partial_item"

    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    return-object v0
.end method

.method public a(JLjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 251
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/suggestionselection/a;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    new-instance v5, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 256
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Lcny;

    invoke-virtual {v5, v4, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcny;

    .line 258
    const/4 v2, 0x0

    .line 259
    array-length v6, v0

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_2

    aget-object v1, v0, v3

    .line 260
    invoke-virtual {v1}, Lcny;->a()Lcnx;

    move-result-object v7

    iget-wide v8, v7, Lcnx;->b:J

    cmp-long v7, v8, p1

    if-nez v7, :cond_5

    .line 259
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_1

    .line 265
    :cond_2
    if-eqz v2, :cond_3

    .line 266
    const-string/jumbo v0, ""

    invoke-static {v5, v2, v0, v4}, Lcom/twitter/library/util/af;->a(Landroid/text/Editable;Ljava/lang/Object;Ljava/lang/String;Z)V

    .line 267
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {p0, v5, v0}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/lang/CharSequence;I)V

    .line 289
    :goto_3
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/a;->b()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/util/Set;)V

    goto :goto_0

    .line 268
    :cond_3
    array-length v0, v0

    iget v1, p0, Lcom/twitter/android/suggestionselection/a;->e:I

    if-ge v0, v1, :cond_0

    .line 271
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 272
    new-instance v0, Lcnx$a;

    invoke-direct {v0}, Lcnx$a;-><init>()V

    .line 273
    invoke-virtual {v0, p1, p2}, Lcnx$a;->a(J)Lcnx$a;

    move-result-object v0

    .line 274
    invoke-virtual {v0, v1}, Lcnx$a;->a(Ljava/lang/String;)Lcnx$a;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Lcnx$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnx;

    .line 276
    new-instance v2, Lcny;

    iget-object v3, p0, Lcom/twitter/android/suggestionselection/a;->a:Landroid/content/Context;

    iget-boolean v4, p0, Lcom/twitter/android/suggestionselection/a;->i:Z

    invoke-direct {v2, v0, v3, v4}, Lcny;-><init>(Lcnx;Landroid/content/Context;Z)V

    .line 278
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->f:Lnh;

    iget-object v3, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 279
    invoke-virtual {v3}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionEnd()I

    move-result v3

    .line 278
    invoke-virtual {v0, v5, v3}, Lnh;->a_(Ljava/lang/CharSequence;I)Lnj$a;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_4

    .line 281
    iget v3, v0, Lnj$a;->a:I

    iget v4, v0, Lnj$a;->b:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v3, v4, v6}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 282
    iget v3, v0, Lnj$a;->a:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x1

    .line 283
    iget v0, v0, Lnj$a;->a:I

    const/16 v3, 0x21

    invoke-virtual {v5, v2, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 284
    invoke-virtual {p0, v5, v1}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/lang/CharSequence;I)V

    .line 287
    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/a;->g()V

    goto :goto_3

    :cond_5
    move-object v1, v2

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a;->d:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 215
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(I)V

    .line 217
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a;->d:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 218
    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/a;->f()V

    .line 219
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->h:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->g:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 301
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->c:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 302
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Lcny;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcny;

    .line 303
    array-length v2, v0

    invoke-static {v2}, Lcom/twitter/util/collection/o;->a(I)Lcom/twitter/util/collection/o;

    move-result-object v2

    .line 304
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 305
    invoke-virtual {v4}, Lcny;->a()Lcnx;

    move-result-object v4

    iget-wide v4, v4, Lcnx;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 304
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 307
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public b(J)Z
    .locals 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->h:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lnh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnh",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->f:Lnh;

    return-object v0
.end method

.method public d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a;->h:Ljava/util/Set;

    return-object v0
.end method
