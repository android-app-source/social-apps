.class public abstract Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/suggestionselection/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/app/common/abs/AbsFragment;",
        "Lcom/twitter/android/suggestionselection/a$a",
        "<TT;TS;>;"
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/autocomplete/ListViewSuggestionEditText",
            "<TT;TS;>;"
        }
    .end annotation
.end field

.field protected b:Landroid/widget/ListView;

.field protected c:Lmq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmq",
            "<TT;TS;>;"
        }
    .end annotation
.end field

.field protected d:Lcom/twitter/android/suggestionselection/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/suggestionselection/a",
            "<TT;TS;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/view/LayoutInflater;)Landroid/view/View;
.end method

.method protected final a(Landroid/view/LayoutInflater;I)Landroid/view/View;
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment$1;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment$1;-><init>(Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;Landroid/view/View;)V

    .line 60
    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 67
    const v0, 0x7f1302a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->b:Landroid/widget/ListView;

    .line 69
    const v0, 0x7f13029e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    iput-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    .line 70
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setLongClickable(Z)V

    .line 71
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    iget-object v2, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setListView(Landroid/widget/ListView;)V

    .line 73
    return-object v1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v11

    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    const-string/jumbo v1, "preselected_items"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/b;->e(Ljava/lang/String;)[J

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a([J)Ljava/util/List;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v7

    .line 81
    new-instance v0, Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->h()Landroid/text/TextWatcher;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->e()Lna;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->f()Lnh;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->l()I

    move-result v6

    iget-object v9, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    .line 83
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->i()Z

    move-result v10

    move-object v2, p0

    move-object v8, p2

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/suggestionselection/a;-><init>(Landroid/content/Context;Lcom/twitter/android/suggestionselection/a$a;Landroid/text/TextWatcher;Lna;Lnh;ILjava/util/Set;Landroid/os/Bundle;Lcom/twitter/android/autocomplete/SuggestionEditText;Z)V

    iput-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->d:Lcom/twitter/android/suggestionselection/a;

    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->g()Lmq;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->c:Lmq;

    .line 86
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->c:Lmq;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setAdapter(Lmq;)V

    .line 87
    return-object v11
.end method

.method public a(Ljava/lang/Object;Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcbi",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->b:Landroid/widget/ListView;

    .line 120
    new-instance v1, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment$2;-><init>(Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 126
    return-void
.end method

.method public bi_()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->c:Lmq;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->c:Lmq;

    invoke-virtual {v0}, Lmq;->notifyDataSetChanged()V

    .line 155
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method protected abstract e()Lna;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lna",
            "<TT;TS;>;"
        }
    .end annotation
.end method

.method protected abstract f()Lnh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnh",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected abstract g()Lmq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmq",
            "<TT;TS;>;"
        }
    .end annotation
.end method

.method protected h()Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method protected i()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->b()V

    .line 134
    return-void
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 141
    const v0, 0x7fffffff

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->c()V

    .line 113
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onDestroy()V

    .line 114
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 147
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->d:Lcom/twitter/android/suggestionselection/a;

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/suggestionselection/a;->a(Landroid/text/Editable;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 148
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 101
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onStart()V

    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->requestFocus()Z

    .line 104
    invoke-virtual {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 106
    :cond_0
    return-void
.end method
