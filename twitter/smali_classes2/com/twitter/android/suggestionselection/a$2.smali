.class Lcom/twitter/android/suggestionselection/a$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/suggestionselection/a;-><init>(Landroid/content/Context;Lcom/twitter/android/suggestionselection/a$a;Landroid/text/TextWatcher;Lna;Lnh;ILjava/util/Set;Landroid/os/Bundle;Lcom/twitter/android/autocomplete/SuggestionEditText;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/text/TextWatcher;

.field final synthetic b:Lcom/twitter/android/suggestionselection/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/suggestionselection/a;Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/android/suggestionselection/a$2;->b:Lcom/twitter/android/suggestionselection/a;

    iput-object p2, p0, Lcom/twitter/android/suggestionselection/a$2;->a:Landroid/text/TextWatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 114
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/b;->a(C)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 117
    :goto_0
    iget-object v3, p0, Lcom/twitter/android/suggestionselection/a$2;->b:Lcom/twitter/android/suggestionselection/a;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    :goto_1
    invoke-static {v3, v0}, Lcom/twitter/android/suggestionselection/a;->a(Lcom/twitter/android/suggestionselection/a;I)V

    .line 119
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, Lcny;

    invoke-interface {p1, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcny;

    .line 120
    array-length v3, v0

    if-lez v3, :cond_7

    .line 122
    iget-object v3, p0, Lcom/twitter/android/suggestionselection/a$2;->b:Lcom/twitter/android/suggestionselection/a;

    invoke-static {v3}, Lcom/twitter/android/suggestionselection/a;->b(Lcom/twitter/android/suggestionselection/a;)Lcom/twitter/android/autocomplete/SuggestionEditText;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 124
    array-length v5, v0

    move v4, v1

    move v3, v1

    :goto_2
    if-ge v4, v5, :cond_5

    aget-object v6, v0, v4

    .line 125
    invoke-interface {p1, v6}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 126
    invoke-interface {p1, v6}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 127
    const/4 v9, -0x1

    if-le v7, v9, :cond_2

    if-lt v8, v7, :cond_2

    .line 128
    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    .line 129
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcny;->a()Lcnx;

    move-result-object v9

    iget-object v9, v9, Lcnx;->c:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x20

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 130
    const-string/jumbo v3, ""

    invoke-static {p1, v6, v3, v1}, Lcom/twitter/library/util/af;->a(Landroid/text/Editable;Ljava/lang/Object;Ljava/lang/String;Z)V

    move v3, v2

    .line 124
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    .line 116
    goto :goto_0

    .line 117
    :cond_4
    const/4 v0, 0x3

    goto :goto_1

    .line 135
    :cond_5
    if-eqz v3, :cond_6

    .line 136
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->b:Lcom/twitter/android/suggestionselection/a;

    iget-object v1, p0, Lcom/twitter/android/suggestionselection/a$2;->b:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v1}, Lcom/twitter/android/suggestionselection/a;->b()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/suggestionselection/a;->a(Lcom/twitter/android/suggestionselection/a;Ljava/util/Set;)V

    .line 138
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->b:Lcom/twitter/android/suggestionselection/a;

    invoke-static {v0}, Lcom/twitter/android/suggestionselection/a;->b(Lcom/twitter/android/suggestionselection/a;)Lcom/twitter/android/autocomplete/SuggestionEditText;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 141
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->b:Lcom/twitter/android/suggestionselection/a;

    invoke-static {v0}, Lcom/twitter/android/suggestionselection/a;->c(Lcom/twitter/android/suggestionselection/a;)V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->a:Landroid/text/TextWatcher;

    if-eqz v0, :cond_8

    .line 144
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->a:Landroid/text/TextWatcher;

    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 146
    :cond_8
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->a:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->a:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 103
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->a:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/twitter/android/suggestionselection/a$2;->a:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 110
    :cond_0
    return-void
.end method
