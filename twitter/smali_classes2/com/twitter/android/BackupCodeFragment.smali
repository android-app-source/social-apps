.class public Lcom/twitter/android/BackupCodeFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$a;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/BackupCodeFragment$c;,
        Lcom/twitter/android/BackupCodeFragment$b;,
        Lcom/twitter/android/BackupCodeFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Landroid/database/Cursor;",
        "Lcom/twitter/android/BackupCodeFragment$a;",
        ">;",
        "Lcom/twitter/app/common/dialog/b$a;",
        "Lcom/twitter/app/common/dialog/b$d;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/BackupCodeFragment;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, Lcom/twitter/android/BackupCodeFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 146
    iget-object v0, p0, Lcom/twitter/android/BackupCodeFragment;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "_id"

    aput-object v2, v1, v3

    const-string/jumbo v2, "code"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 150
    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 154
    :goto_0
    new-instance v1, Lcbe;

    invoke-direct {v1, v0}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/BackupCodeFragment;->b(Lcbi;)V

    .line 155
    iput-object p1, p0, Lcom/twitter/android/BackupCodeFragment;->b:Ljava/lang/String;

    .line 157
    :cond_0
    return-void

    .line 152
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    .line 265
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_1

    .line 266
    :cond_0
    const-string/jumbo v0, ""

    .line 268
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 253
    if-eqz v0, :cond_0

    .line 254
    invoke-static {v0}, Lcom/twitter/android/BackupCodeFragment$a;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 255
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 257
    const v0, 0x7f0a021b

    invoke-virtual {p0, v0}, Lcom/twitter/android/BackupCodeFragment;->a_(I)V

    .line 261
    :cond_0
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 320
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "backup_code::take_screenshot::impression"

    aput-object v3, v1, v2

    .line 321
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 320
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 322
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v4}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a04e9

    .line 323
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a04fa

    .line 324
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 325
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 326
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 327
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 328
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 329
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 330
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 331
    return-void
.end method

.method private q()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 358
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 359
    invoke-virtual {v0, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 360
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 361
    invoke-virtual {v0, v4}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 363
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 364
    if-eqz v0, :cond_0

    .line 365
    new-instance v2, Lcom/twitter/android/BackupCodeFragment$c;

    const v3, 0x7f0a07bd

    .line 366
    invoke-virtual {p0, v3}, Lcom/twitter/android/BackupCodeFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3}, Lcom/twitter/android/BackupCodeFragment$c;-><init>(Lcom/twitter/android/BackupCodeFragment;Landroid/content/Context;Ljava/lang/String;)V

    .line 367
    new-array v0, v5, [Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    invoke-virtual {v2, v0}, Lcom/twitter/android/BackupCodeFragment$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 371
    :goto_0
    return-void

    .line 369
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->c()V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 314
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "backup_code::take_screenshot:cancel:click"

    aput-object v3, v1, v2

    .line 315
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 314
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 316
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 281
    if-ne p2, v1, :cond_0

    .line 282
    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    .line 283
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "backup_code::take_screenshot:ok:click"

    aput-object v2, v1, v4

    .line 284
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 283
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 286
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 287
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/BackupCodeFragment;->a:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 288
    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->q()V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    new-instance v1, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 291
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a07b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/BackupCodeFragment;->a:[Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v0, "backup_code::take_screenshot:"

    .line 293
    invoke-virtual {v1, v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 294
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BackupCodeFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 297
    :cond_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "backup_code::take_screenshot:cancel:click"

    aput-object v2, v1, v4

    .line 298
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 297
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 239
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 240
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p3, v1, :cond_1

    if-eqz v0, :cond_1

    .line 241
    const-string/jumbo v1, ""

    invoke-direct {p0, v1}, Lcom/twitter/android/BackupCodeFragment;->b(Ljava/lang/String;)V

    .line 242
    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-static {v0, v2, v3}, Lbaw;->c(Landroid/content/Context;J)V

    .line 243
    new-instance v0, Lbag;

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lbag;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V

    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1, v4}, Lcom/twitter/android/BackupCodeFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/p;

    invoke-virtual {v0, p3}, Lcom/twitter/android/widget/p;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->f()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 178
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 179
    const v0, 0x7f04004c

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 180
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 118
    packed-switch p2, :pswitch_data_0

    .line 143
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    .line 120
    check-cast v0, Lbag;

    invoke-virtual {v0}, Lbag;->e()Lcom/twitter/model/account/b;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    invoke-virtual {v0}, Lcom/twitter/model/account/b;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BackupCodeFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    .line 127
    check-cast v0, Lbag;

    invoke-virtual {v0}, Lbag;->e()Lcom/twitter/model/account/b;

    move-result-object v0

    .line 128
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    invoke-virtual {v0}, Lcom/twitter/model/account/b;->a()[Ljava/lang/String;

    move-result-object v0

    .line 130
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    new-instance v0, Lbag;

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lbag;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V

    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/android/BackupCodeFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0

    .line 134
    :cond_1
    aget-object v0, v0, v3

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/BackupCodeFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 335
    if-eqz p1, :cond_0

    .line 336
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "backup_code::take_screenshot::success"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 337
    const v0, 0x7f0a07c4

    invoke-virtual {p0, v0}, Lcom/twitter/android/BackupCodeFragment;->a_(I)V

    .line 341
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->c()V

    goto :goto_0
.end method

.method a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 161
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    const v1, 0x7f0a04ef

    invoke-virtual {p0, v1}, Lcom/twitter/android/BackupCodeFragment;->a_(I)V

    .line 163
    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/android/BackupCodeFragment;->b(Ljava/lang/String;)V

    .line 171
    if-eqz p2, :cond_0

    .line 172
    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->k()V

    goto :goto_0
.end method

.method a_(I)V
    .locals 3

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 273
    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p0, p1}, Lcom/twitter/android/BackupCodeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 276
    :cond_0
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 108
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->b()V

    .line 109
    iget-object v0, p0, Lcom/twitter/android/BackupCodeFragment;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/twitter/android/BackupCodeFragment;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lbag;

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lbag;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V

    const/16 v1, 0xc

    invoke-virtual {p0, v0, v1, v4}, Lcom/twitter/android/BackupCodeFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 113
    :cond_0
    return-void
.end method

.method c()V
    .locals 4

    .prologue
    .line 345
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "backup_code::take_screenshot::failure"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 346
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09a8

    .line 347
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09a9

    .line 348
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0616

    .line 349
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 350
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 351
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 352
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 305
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 306
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->q()V

    .line 309
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 77
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0, v7}, Lcom/twitter/android/BackupCodeFragment;->setRetainInstance(Z)V

    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 82
    const-string/jumbo v2, "bc_account_id"

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    .line 84
    if-nez p1, :cond_0

    .line 85
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/BackupCodeFragment;->a_:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "backup_code::::impression"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 91
    :cond_0
    if-nez p1, :cond_1

    const-string/jumbo v1, "show_welcome"

    invoke-virtual {v0, v1, v6}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->k()V

    .line 94
    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/list/TwitterListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/twitter/android/BackupCodeFragment$a;

    invoke-direct {v1, v0}, Lcom/twitter/android/BackupCodeFragment$a;-><init>(Landroid/content/Context;)V

    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/BackupCodeFragment$b;

    invoke-direct {v3, v0, v1}, Lcom/twitter/android/BackupCodeFragment$b;-><init>(Landroid/content/Context;Lcom/twitter/android/BackupCodeFragment$a;)V

    invoke-virtual {v2, v1, v3}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    .line 103
    const-string/jumbo v0, ""

    invoke-direct {p0, v0}, Lcom/twitter/android/BackupCodeFragment;->b(Ljava/lang/String;)V

    .line 104
    return-void
.end method
