.class Lcom/twitter/android/metrics/g$a;
.super Lcno$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/metrics/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/metrics/g;

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Lcom/twitter/android/metrics/g;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/metrics/g$a;->a:Lcom/twitter/android/metrics/g;

    invoke-direct {p0}, Lcno$a;-><init>()V

    .line 59
    invoke-direct {p0}, Lcom/twitter/android/metrics/g$a;->b()V

    .line 60
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 63
    iput v0, p0, Lcom/twitter/android/metrics/g$a;->b:I

    .line 64
    iput v0, p0, Lcom/twitter/android/metrics/g$a;->c:I

    .line 65
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 68
    iget v0, p0, Lcom/twitter/android/metrics/g$a;->c:I

    iget v1, p0, Lcom/twitter/android/metrics/g$a;->b:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public a(Lcno;I)V
    .locals 2

    .prologue
    .line 73
    invoke-interface {p1}, Lcno;->c()Lcnm;

    move-result-object v0

    .line 74
    if-nez p2, :cond_1

    .line 75
    iget v0, v0, Lcnm;->c:I

    iput v0, p0, Lcom/twitter/android/metrics/g$a;->c:I

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 77
    invoke-direct {p0}, Lcom/twitter/android/metrics/g$a;->b()V

    .line 78
    iget v0, v0, Lcnm;->c:I

    iput v0, p0, Lcom/twitter/android/metrics/g$a;->b:I

    goto :goto_0
.end method
