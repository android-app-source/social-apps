.class public Lcom/twitter/android/metrics/b;
.super Lcom/twitter/metrics/f;
.source "Twttr"


# instance fields
.field a:J

.field b:J

.field c:J

.field d:Z

.field e:Z

.field private w:J

.field private x:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/h;)V
    .locals 6

    .prologue
    .line 35
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/metrics/f;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;Z)V

    .line 36
    invoke-static {}, Lcom/twitter/metrics/ForegroundMetricTracker;->a()Lcom/twitter/metrics/ForegroundMetricTracker;

    move-result-object v0

    sget-object v1, Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;->a:Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/f;Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;)V

    .line 38
    return-void
.end method

.method public static a(Lcnz;Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/h;)Lcom/twitter/android/metrics/b;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/android/metrics/b;

    invoke-direct {v0, p1, p1, p2, p3}, Lcom/twitter/android/metrics/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/h;)V

    .line 29
    invoke-virtual {p0}, Lcnz;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/metrics/b;->b(J)V

    .line 30
    return-object v0
.end method


# virtual methods
.method public aQ_()V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/metrics/b;->d:Z

    .line 54
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/metrics/b;->w:J

    .line 55
    return-void
.end method

.method public aR_()V
    .locals 4

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/twitter/android/metrics/b;->d:Z

    if-eqz v0, :cond_0

    .line 59
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/metrics/b;->w:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/android/metrics/b;->b:J

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/metrics/b;->d:Z

    .line 62
    :cond_0
    return-void
.end method

.method public aS_()V
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/metrics/b;->e:Z

    .line 66
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/metrics/b;->x:J

    .line 67
    return-void
.end method

.method public aT_()V
    .locals 4

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/twitter/android/metrics/b;->e:Z

    if-eqz v0, :cond_0

    .line 71
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/metrics/b;->x:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/android/metrics/b;->c:J

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/metrics/b;->e:Z

    .line 74
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/metrics/b;->a:J

    .line 43
    invoke-super {p0}, Lcom/twitter/metrics/f;->b()V

    .line 44
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Lcom/twitter/metrics/f;->c()V

    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/metrics/b;->o()V

    .line 50
    return-void
.end method
