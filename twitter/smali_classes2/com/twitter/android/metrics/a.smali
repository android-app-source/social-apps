.class public Lcom/twitter/android/metrics/a;
.super Lcom/twitter/metrics/n;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;)V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/twitter/library/metrics/c;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "app:ready"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/metrics/n;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V

    .line 26
    return-void
.end method

.method public static a(Lcom/twitter/metrics/j;J)Lcom/twitter/android/metrics/a;
    .locals 3

    .prologue
    .line 15
    const-string/jumbo v0, "app:ready"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/j;->a(Ljava/lang/String;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lcom/twitter/android/metrics/a;

    const-string/jumbo v1, "app:ready"

    sget-object v2, Lcom/twitter/android/metrics/a;->n:Lcom/twitter/metrics/g$b;

    invoke-direct {v0, v1, v2, p0}, Lcom/twitter/android/metrics/a;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;)V

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/j;->d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 18
    invoke-virtual {v0, p1, p2}, Lcom/twitter/metrics/f;->b(J)V

    .line 19
    const-string/jumbo v1, "AppMetrics"

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/f;->e(Ljava/lang/String;)V

    .line 21
    :cond_0
    check-cast v0, Lcom/twitter/android/metrics/a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "app:ready"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/twitter/android/metrics/LaunchTracker;->a()Lcom/twitter/android/metrics/LaunchTracker;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/metrics/LaunchTracker;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
