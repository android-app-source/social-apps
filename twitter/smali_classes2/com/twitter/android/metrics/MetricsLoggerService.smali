.class public Lcom/twitter/android/metrics/MetricsLoggerService;
.super Landroid/app/IntentService;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Ljava/io/BufferedOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/metrics/MetricsLoggerService;->a:Ljava/util/Map;

    .line 49
    sget-object v0, Lcom/twitter/android/metrics/MetricsLoggerService;->a:Ljava/util/Map;

    const-string/jumbo v1, "write"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/twitter/android/metrics/MetricsLoggerService;->a:Ljava/util/Map;

    const-string/jumbo v1, "begin"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/twitter/android/metrics/MetricsLoggerService;->a:Ljava/util/Map;

    const-string/jumbo v1, "end"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/twitter/android/i;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "/sdcard/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/i;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/twitter/android/metrics/MetricsLoggerService;->b:Ljava/lang/String;

    return-void

    :cond_0
    const-string/jumbo v0, "/sdcard/twitter-metrics"

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    const-string/jumbo v0, "MetricsLogger"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 61
    return-void
.end method


# virtual methods
.method protected a()Ljava/io/File;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/twitter/android/metrics/MetricsLoggerService;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 67
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/metrics/MetricsLoggerService;->a()Ljava/io/File;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 146
    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 78
    sget-object v1, Lcom/twitter/android/metrics/MetricsLoggerService;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 79
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 82
    :goto_0
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/metrics/MetricsLoggerService;->a()Ljava/io/File;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    packed-switch v0, :pswitch_data_0

    .line 135
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 136
    :goto_2
    return-void

    .line 79
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    const-string/jumbo v0, "MetricsLogger"

    const-string/jumbo v1, "symlink \'/sdcard\' doesn\'t exist"

    invoke-static {v0, v1}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 93
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;

    const-string/jumbo v1, "\n["

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 94
    :catch_1
    move-exception v0

    goto :goto_1

    .line 100
    :pswitch_1
    const/4 v1, 0x0

    .line 102
    :try_start_2
    invoke-virtual {p0}, Lcom/twitter/android/metrics/MetricsLoggerService;->a()Ljava/io/File;

    move-result-object v2

    .line 103
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string/jumbo v3, "r"

    invoke-direct {v0, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    :try_start_3
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 105
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 106
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v1

    .line 107
    const/16 v2, 0x5b

    if-eq v2, v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;

    const-string/jumbo v2, ","

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 113
    :cond_1
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 116
    :goto_3
    const-string/jumbo v0, "log"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/performance/PerformanceScribeLog;

    .line 117
    iget-object v1, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/performance/PerformanceScribeLog;->a(Ljava/io/OutputStream;)V

    goto :goto_1

    .line 111
    :catch_2
    move-exception v0

    move-object v0, v1

    .line 113
    :goto_4
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_3

    :catchall_0
    move-exception v0

    :goto_5
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    .line 122
    :pswitch_2
    :try_start_4
    iget-object v0, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;

    const-string/jumbo v1, "]"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 123
    iget-object v0, p0, Lcom/twitter/android/metrics/MetricsLoggerService;->c:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 126
    :goto_6
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "write_finished"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/metrics/MetricsLoggerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    .line 128
    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_1

    .line 124
    :catch_3
    move-exception v0

    goto :goto_6

    .line 113
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_5

    .line 111
    :catch_4
    move-exception v1

    goto :goto_4

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
