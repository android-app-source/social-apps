.class public Lcom/twitter/android/GeoDebugActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lbqk;
.implements Lcom/google/android/gms/maps/c$c;
.implements Lcom/google/android/gms/maps/c$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/GeoDebugActivity$a;,
        Lcom/twitter/android/GeoDebugActivity$c;,
        Lcom/twitter/android/GeoDebugActivity$b;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/maps/model/d;",
            "Lcom/twitter/android/GeoDebugActivity$c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lbqo;

.field private c:Lcom/google/android/gms/maps/SupportMapFragment;

.field private d:Lcom/google/android/gms/maps/c;

.field private e:Lcom/google/android/gms/maps/model/d;

.field private f:Landroid/app/Dialog;

.field private g:Lcom/twitter/android/GeoDebugActivity$a;

.field private h:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/GeoDebugActivity;Lcom/google/android/gms/maps/c;)Lcom/google/android/gms/maps/c;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;Landroid/location/Location;)Lcom/twitter/android/GeoDebugActivity$c;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 212
    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 213
    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 214
    invoke-direct {p0, p1}, Lcom/twitter/android/GeoDebugActivity;->b(Lcom/google/android/gms/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 215
    iget-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/d;

    move-result-object v2

    .line 216
    if-eqz v2, :cond_0

    .line 217
    new-instance v0, Lcom/twitter/android/GeoDebugActivity$c;

    invoke-direct {v0, v1}, Lcom/twitter/android/GeoDebugActivity$c;-><init>(Lcom/twitter/android/GeoDebugActivity$1;)V

    .line 218
    iput-object p2, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    .line 219
    new-instance v1, Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/CircleOptions;-><init>()V

    .line 220
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/CircleOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    iget-object v3, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-double v4, v3

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/maps/model/CircleOptions;->a(D)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    const v3, 0x331da1f2

    .line 221
    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/CircleOptions;->b(I)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    const/high16 v3, 0x40400000    # 3.0f

    .line 222
    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/CircleOptions;->a(F)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    const v3, -0x33ba5c1b    # -5.1810196E7f

    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/CircleOptions;->a(I)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    .line 223
    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/CircleOptions;)Lcom/google/android/gms/maps/model/c;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/GeoDebugActivity$c;->b:Lcom/google/android/gms/maps/model/c;

    .line 224
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/GeoDebugActivity;)Lbqo;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/maps/model/LatLng;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 232
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 233
    new-instance v1, Ljava/text/DecimalFormat;

    invoke-direct {v1}, Ljava/text/DecimalFormat;-><init>()V

    .line 234
    invoke-virtual {v1, v0}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 235
    const/4 v0, 0x7

    invoke-virtual {v1, v0}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/google/android/gms/maps/model/LatLng;)Landroid/location/Location;
    .locals 4

    .prologue
    .line 245
    new-instance v0, Landroid/location/Location;

    const-string/jumbo v1, "gps"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 246
    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 247
    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 248
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 249
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 250
    invoke-direct {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->c(Landroid/location/Location;)V

    .line 252
    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/GeoDebugActivity;)Lbqn;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->R()Lbqn;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/location/Location;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 418
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 419
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    .line 421
    :cond_0
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const v3, 0x7f1303b0

    .line 139
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/google/android/gms/maps/SupportMapFragment;

    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/google/android/gms/maps/SupportMapFragment;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/google/android/gms/maps/SupportMapFragment;

    if-nez v0, :cond_1

    .line 144
    invoke-static {}, Lcom/google/android/gms/maps/SupportMapFragment;->a()Lcom/google/android/gms/maps/SupportMapFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/google/android/gms/maps/SupportMapFragment;

    .line 146
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/google/android/gms/maps/SupportMapFragment;

    const-string/jumbo v2, "map"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 148
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 150
    :cond_1
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/google/android/gms/maps/SupportMapFragment;

    new-instance v1, Lcom/twitter/android/GeoDebugActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/GeoDebugActivity$1;-><init>(Lcom/twitter/android/GeoDebugActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/SupportMapFragment;->a(Lcom/google/android/gms/maps/e;)V

    .line 173
    :cond_0
    return-void
.end method

.method private l()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 179
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    new-instance v1, Lcom/google/android/gms/maps/model/LatLngBounds$a;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/LatLngBounds$a;-><init>()V

    .line 183
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    invoke-virtual {v0}, Lbqo;->e()Ljava/util/List;

    move-result-object v2

    .line 184
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 185
    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 186
    invoke-virtual {v1, v4}, Lcom/google/android/gms/maps/model/LatLngBounds$a;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$a;

    .line 187
    invoke-direct {p0, v4, v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/google/android/gms/maps/model/LatLng;Landroid/location/Location;)Lcom/twitter/android/GeoDebugActivity$c;

    goto :goto_1

    .line 189
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 191
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 192
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 193
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-static {v1, v2}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    goto :goto_0

    .line 195
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v2, v2, 0x2

    .line 197
    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    .line 198
    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/LatLngBounds$a;->a()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    invoke-static {v1, v0, v2, v5}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/maps/a;

    move-result-object v0

    .line 197
    invoke-virtual {v3, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    const v0, 0x7f040113

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 101
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 102
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 103
    return-object p2
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 337
    if-nez p1, :cond_0

    .line 354
    :goto_0
    return-void

    .line 341
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 342
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-static {v0, v2}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v2

    const/16 v3, 0x3e8

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;ILcom/google/android/gms/maps/c$a;)V

    .line 343
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/google/android/gms/maps/model/d;

    if-eqz v1, :cond_1

    .line 344
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/google/android/gms/maps/model/d;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/d;->a()V

    .line 346
    :cond_1
    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 347
    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 348
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 349
    const/high16 v0, 0x43700000    # 240.0f

    .line 350
    invoke-static {v0}, Lcom/google/android/gms/maps/model/b;->a(F)Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    .line 351
    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 352
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/google/android/gms/maps/model/d;

    .line 353
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->R()Lbqn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbqn;->b(Lbqk;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    .line 368
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/GeoDebugActivity;->c(Lcom/google/android/gms/maps/model/LatLng;)Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/google/android/gms/maps/model/LatLng;Landroid/location/Location;)Lcom/twitter/android/GeoDebugActivity$c;

    move-result-object v0

    .line 366
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    invoke-virtual {v1, v0}, Lbqo;->a(Landroid/location/Location;)V

    .line 367
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    invoke-virtual {v0}, Lbqo;->a()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/maps/model/d;)V
    .locals 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    new-instance v1, Lcom/twitter/android/GeoDebugActivity$a;

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    .line 330
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/GeoDebugActivity$c;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    invoke-direct {v1, p1, v0, p0}, Lcom/twitter/android/GeoDebugActivity$a;-><init>(Lcom/google/android/gms/maps/model/d;Landroid/location/Location;Landroid/content/DialogInterface$OnClickListener;)V

    iput-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->g:Lcom/twitter/android/GeoDebugActivity$a;

    .line 331
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Lcom/twitter/android/GeoDebugActivity$a;

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "EditLocation"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/GeoDebugActivity$a;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 333
    :cond_0
    return-void
.end method

.method public b(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 108
    const v0, 0x7f0a0bb0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->setTitle(I)V

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    .line 111
    invoke-static {p0}, Lbqo;->a(Landroid/content/Context;)Lbqo;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    .line 113
    const v0, 0x7f1303b3

    invoke-virtual {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 114
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :goto_0
    const v0, 0x7f1303b2

    invoke-virtual {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 121
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    const v0, 0x7f1303b1

    invoke-virtual {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Landroid/widget/CheckBox;

    .line 124
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    invoke-virtual {v1}, Lbqo;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 127
    invoke-direct {p0}, Lcom/twitter/android/GeoDebugActivity;->i()V

    .line 128
    return-void

    .line 117
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    invoke-virtual {v0, p2}, Lbqo;->a(Z)V

    .line 373
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 257
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Landroid/app/Dialog;

    if-ne p1, v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Landroid/app/Dialog;

    const v1, 0x7f1303b4

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 259
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 260
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    new-instance v1, Lcom/twitter/android/GeoDebugActivity$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/GeoDebugActivity$b;-><init>(Lcom/twitter/android/GeoDebugActivity;Lcom/twitter/android/GeoDebugActivity$1;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/GeoDebugActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 262
    const v0, 0x7f0a04b6

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Lcom/twitter/android/GeoDebugActivity$a;

    invoke-virtual {v0}, Lcom/twitter/android/GeoDebugActivity$a;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Lcom/twitter/android/GeoDebugActivity$a;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity$a;->a(Lcom/twitter/android/GeoDebugActivity$a;)Lcom/google/android/gms/maps/model/d;

    move-result-object v1

    .line 270
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 282
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/GeoDebugActivity$c;

    .line 284
    iget-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    iget-object v3, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    invoke-virtual {v2, v3}, Lbqo;->b(Landroid/location/Location;)V

    .line 285
    iget-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    invoke-virtual {v2}, Lbqo;->a()V

    .line 286
    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity$c;->b:Lcom/google/android/gms/maps/model/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/c;->a()V

    .line 287
    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/d;->a()V

    goto :goto_0

    .line 272
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Lcom/twitter/android/GeoDebugActivity$a;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity$a;->b(Lcom/twitter/android/GeoDebugActivity$a;)Landroid/location/Location;

    move-result-object v2

    .line 274
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/GeoDebugActivity$c;

    .line 275
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    iget-object v3, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    invoke-virtual {v1, v3, v2}, Lbqo;->a(Landroid/location/Location;Landroid/location/Location;)V

    .line 276
    iput-object v2, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    .line 277
    iget-object v1, v0, Lcom/twitter/android/GeoDebugActivity$c;->b:Lcom/google/android/gms/maps/model/c;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity$c;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/maps/model/c;->a(D)V

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 300
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 324
    :goto_0
    return-void

    .line 302
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 303
    const v1, 0x7f040114

    .line 304
    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 303
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00dd

    .line 305
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f6

    .line 306
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 307
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Landroid/app/Dialog;

    .line 308
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 312
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->a()V

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 316
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->b:Lbqo;

    invoke-virtual {v0}, Lbqo;->b()V

    .line 317
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x7f1303b2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStart()V

    .line 133
    invoke-direct {p0}, Lcom/twitter/android/GeoDebugActivity;->j()V

    .line 134
    invoke-direct {p0}, Lcom/twitter/android/GeoDebugActivity;->l()V

    .line 135
    const v0, 0x7f0a0bab

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 136
    return-void
.end method
