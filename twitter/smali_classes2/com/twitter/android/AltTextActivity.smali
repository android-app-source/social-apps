.class public Lcom/twitter/android/AltTextActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/ScrollView;

.field private b:Lcom/twitter/media/ui/image/MediaImageView;

.field private c:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/AltTextActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/AltTextActivity;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/AltTextActivity;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/AltTextActivity;->a:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/AltTextActivity;)Lcom/twitter/media/ui/image/MediaImageView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/AltTextActivity;->b:Lcom/twitter/media/ui/image/MediaImageView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    const v0, 0x7f040034

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 107
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 108
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 109
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 34
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 36
    const v0, 0x7f13016b

    invoke-virtual {p0, v0}, Lcom/twitter/android/AltTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/twitter/android/AltTextActivity;->a:Landroid/widget/ScrollView;

    .line 37
    const v0, 0x7f13016d

    invoke-virtual {p0, v0}, Lcom/twitter/android/AltTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/AltTextActivity;->b:Lcom/twitter/media/ui/image/MediaImageView;

    .line 38
    const v0, 0x7f13016e

    invoke-virtual {p0, v0}, Lcom/twitter/android/AltTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/AltTextActivity;->c:Landroid/widget/EditText;

    .line 40
    invoke-virtual {p0, v5}, Lcom/twitter/android/AltTextActivity;->h(Z)V

    .line 42
    invoke-virtual {p0}, Lcom/twitter/android/AltTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 43
    const-string/jumbo v0, "editable_image"

    .line 44
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 46
    if-eqz v0, :cond_1

    .line 47
    iget-object v3, p0, Lcom/twitter/android/AltTextActivity;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v1, Lcom/twitter/media/model/ImageFile;

    iget-object v1, v1, Lcom/twitter/media/model/ImageFile;->f:Lcom/twitter/util/math/Size;

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->g()F

    move-result v1

    invoke-virtual {v3, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 48
    iget-object v1, p0, Lcom/twitter/android/AltTextActivity;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {p0, v0}, Lbru;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 53
    :goto_0
    const-string/jumbo v0, "alt_text"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    .line 55
    iget-object v1, p0, Lcom/twitter/android/AltTextActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AltTextActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/twitter/android/AltTextActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/AltTextActivity$1;-><init>(Lcom/twitter/android/AltTextActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/AltTextActivity;->a:Landroid/widget/ScrollView;

    new-instance v1, Lcom/twitter/android/AltTextActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/AltTextActivity$2;-><init>(Lcom/twitter/android/AltTextActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/AltTextActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/AltTextActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a005a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 83
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 82
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 85
    iget-object v2, p0, Lcom/twitter/android/AltTextActivity;->c:Landroid/widget/EditText;

    new-instance v3, Lcom/twitter/android/AltTextActivity$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/android/AltTextActivity$3;-><init>(Lcom/twitter/android/AltTextActivity;ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 101
    return-void

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/AltTextActivity;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcmm;)Z
    .locals 4

    .prologue
    const v2, 0x7f1308ad

    .line 120
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 121
    if-eq v0, v2, :cond_0

    const v1, 0x7f130043

    if-ne v0, v1, :cond_2

    .line 122
    :cond_0
    if-ne v0, v2, :cond_1

    .line 123
    iget-object v0, p0, Lcom/twitter/android/AltTextActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 124
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "alt_text"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/AltTextActivity;->setResult(ILandroid/content/Intent;)V

    .line 128
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/AltTextActivity;->finish()V

    .line 129
    const/4 v0, 0x1

    .line 131
    :goto_1
    return v0

    .line 126
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AltTextActivity;->setResult(I)V

    goto :goto_0

    .line 131
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_1
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 114
    const v0, 0x7f140018

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 115
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    move-result v0

    return v0
.end method
