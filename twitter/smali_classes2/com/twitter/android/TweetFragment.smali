.class public Lcom/twitter/android/TweetFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lata;
.implements Lcne;
.implements Lcom/twitter/android/bm$b;
.implements Lcom/twitter/android/widget/TweetDetailView$b;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/library/provider/o$a;
.implements Lwn;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/TweetFragment$b;,
        Lcom/twitter/android/TweetFragment$c;,
        Lcom/twitter/android/TweetFragment$d;,
        Lcom/twitter/android/TweetFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/TweetListFragment",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        "Lcom/twitter/android/cv;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lata;",
        "Lcne;",
        "Lcom/twitter/android/bm$b;",
        "Lcom/twitter/android/widget/TweetDetailView$b;",
        "Lcom/twitter/app/common/dialog/b$d;",
        "Lcom/twitter/library/provider/o$a;",
        "Lwn;"
    }
.end annotation


# static fields
.field private static c:Lcom/twitter/android/cq;


# instance fields
.field private A:I

.field private B:I

.field private C:Lcom/twitter/android/widget/ToggleImageButton;

.field private D:Lcom/twitter/android/widget/ToggleImageButton;

.field private E:Lcom/twitter/android/widget/EngagementActionBar;

.field private F:Z

.field a:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private aa:Lcom/twitter/android/cc;

.field private ab:Z

.field private ac:Ljava/lang/String;

.field private ad:Z

.field private ae:J

.field private af:Lati;

.field private ag:Lrx/j;

.field private ah:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

.field private ai:Lakv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakv",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation
.end field

.field private final aj:Lcom/twitter/model/util/FriendshipCache;

.field private ak:Lbpl;

.field private al:Lcom/twitter/android/cm;

.field b:Lcom/twitter/android/widget/TweetDetailView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private d:Lcom/twitter/android/cp;

.field private e:Lcom/twitter/library/client/Session;

.field private f:Lcom/twitter/model/core/Tweet;

.field private g:Lcom/twitter/library/card/CardContext;

.field private h:Lcom/twitter/library/widget/renderablecontent/d;

.field private i:Lcom/twitter/android/card/d;

.field private j:Lbxa;

.field private k:Z

.field private l:Lcom/twitter/library/api/ActivitySummary;

.field private m:Lcom/twitter/android/TweetFragment$a;

.field private n:Lcom/twitter/android/ct;

.field private o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private p:Lcom/twitter/library/widget/PageableListView;

.field private q:Laji;

.field private s:Lcom/twitter/model/moments/Moment;

.field private t:Lcom/twitter/model/timeline/ar;

.field private final u:Landroid/view/View$OnClickListener;

.field private final v:Lcom/twitter/android/TweetFragment$b;

.field private final w:Landroid/view/View$OnClickListener;

.field private x:Z

.field private y:Z

.field private z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lcom/twitter/android/cq;

    invoke-direct {v0}, Lcom/twitter/android/cq;-><init>()V

    sput-object v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/cq;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 341
    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    .line 278
    new-instance v0, Lcom/twitter/android/TweetFragment$c;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/TweetFragment$c;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/TweetFragment$1;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/view/View$OnClickListener;

    .line 281
    new-instance v0, Lcom/twitter/android/TweetFragment$b;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/TweetFragment$b;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/TweetFragment$1;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->v:Lcom/twitter/android/TweetFragment$b;

    .line 285
    new-instance v0, Lcom/twitter/android/TweetFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/TweetFragment$1;-><init>(Lcom/twitter/android/TweetFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->w:Landroid/view/View$OnClickListener;

    .line 335
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->aj:Lcom/twitter/model/util/FriendshipCache;

    .line 342
    new-instance v0, Lcom/twitter/app/common/list/i$b;

    invoke-direct {v0}, Lcom/twitter/app/common/list/i$b;-><init>()V

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/i$b;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v1, "en_act"

    .line 343
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/i$b;->b()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 342
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 344
    return-void
.end method

.method static synthetic A(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method private A()Z
    .locals 1

    .prologue
    .line 1131
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic B(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private B()V
    .locals 9

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lbwr;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    .line 1161
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    .line 1162
    if-nez v1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->q()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->S()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1238
    :cond_0
    :goto_0
    return-void

    .line 1165
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 1166
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->M()Z

    move-result v8

    .line 1167
    iget-boolean v2, p0, Lcom/twitter/android/TweetFragment;->a:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1168
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->x()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->k:Z

    if-eqz v0, :cond_2

    .line 1170
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->N()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    move v7, v0

    .line 1172
    :goto_1
    new-instance v0, Lbxy;

    .line 1173
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct/range {v0 .. v6}, Lbxy;-><init>(ZLandroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 1174
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, v2, v3}, Lbxy;->a(ILjava/lang/Object;)V

    .line 1176
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    .line 1177
    invoke-static {v2}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v2

    .line 1178
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->C()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1179
    new-instance v0, Lcom/twitter/android/TweetFragment$4;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/TweetFragment$4;-><init>(Lcom/twitter/android/TweetFragment;Lbaa;)V

    .line 1191
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1170
    :cond_3
    const/4 v0, 0x0

    move v7, v0

    goto :goto_1

    .line 1192
    :cond_4
    if-eqz v7, :cond_5

    .line 1193
    new-instance v0, Lcom/twitter/android/TweetFragment$5;

    invoke-direct {v0, p0}, Lcom/twitter/android/TweetFragment$5;-><init>(Lcom/twitter/android/TweetFragment;)V

    .line 1225
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->y()Z

    move-result v2

    invoke-virtual {v1, v0, v8, v2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/widget/PossiblySensitiveWarningView$a;ZZ)V

    goto :goto_0

    .line 1227
    :cond_5
    invoke-virtual {v0}, Lbxy;->a()Lcom/twitter/library/widget/renderablecontent/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/widget/renderablecontent/d;

    .line 1228
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    .line 1229
    new-instance v0, Lcom/twitter/android/card/f;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    .line 1230
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->g:Lcom/twitter/library/card/CardContext;

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/card/CardContext;)V

    .line 1231
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 1232
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 1234
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    if-eqz v0, :cond_0

    .line 1235
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->setContentHost(Lcom/twitter/library/widget/renderablecontent/d;)V

    goto/16 :goto_0
.end method

.method static synthetic C(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/card/d;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    return-object v0
.end method

.method private C()Z
    .locals 2

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    .line 1242
    invoke-static {v0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    .line 1243
    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->y:Z

    if-nez v1, :cond_1

    const-string/jumbo v1, "twitter_access_android_media_forward_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1244
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1245
    invoke-static {v1}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1246
    invoke-virtual {v0}, Lbaa;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1247
    :cond_0
    invoke-virtual {v0}, Lbaa;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1243
    :goto_0
    return v0

    .line 1247
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic D(Lcom/twitter/android/TweetFragment;)Laji;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->q:Laji;

    return-object v0
.end method

.method private D()V
    .locals 3

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1341
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->E()Lapb;

    move-result-object v1

    iget-object v1, v1, Lapb;->d:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1343
    :cond_0
    return-void
.end method

.method private E()Lapb;
    .locals 4

    .prologue
    .line 1346
    new-instance v0, Laiz$a;

    invoke-direct {v0}, Laiz$a;-><init>()V

    const/16 v1, 0xe

    .line 1347
    invoke-virtual {v0, v1}, Laiz$a;->a(I)Laiz$a;

    move-result-object v0

    .line 1348
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Laiz$a;->a(J)Laiz$a;

    move-result-object v0

    .line 1349
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Laiz$a;->b(J)Laiz$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->t:J

    .line 1350
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laiz$a;->a(Ljava/lang/String;)Laiz$a;

    move-result-object v0

    .line 1351
    invoke-virtual {v0}, Laiz$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiz;

    .line 1346
    invoke-static {v0}, Laja;->a(Laiz;)Lapb;

    move-result-object v0

    return-object v0
.end method

.method static synthetic E(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/cc;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->aa:Lcom/twitter/android/cc;

    return-object v0
.end method

.method static synthetic F(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$b;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->v:Lcom/twitter/android/TweetFragment$b;

    return-object v0
.end method

.method private F()V
    .locals 8

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1617
    invoke-static {v0, v1}, Lbxt;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    .line 1618
    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1619
    const-string/jumbo v0, "translation_request"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1620
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1621
    if-eqz v0, :cond_1

    .line 1622
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 1623
    new-instance v1, Lbhf;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    .line 1624
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->G:J

    .line 1625
    invoke-static {}, Lbpj;->b()Z

    move-result v7

    invoke-direct/range {v1 .. v7}, Lbhf;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;Z)V

    .line 1626
    const/4 v0, 0x6

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1627
    iget-object v0, v1, Lbhf;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->ac:Ljava/lang/String;

    .line 1629
    :cond_0
    return-void

    .line 1622
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method private G()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 1981
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 1983
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    if-eqz v1, :cond_1

    .line 1984
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-boolean v1, v1, Lcom/twitter/model/core/Tweet;->a:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->c:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->c:[J

    array-length v1, v1

    if-lez v1, :cond_0

    .line 1986
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    iget-object v4, v1, Lcom/twitter/library/api/ActivitySummary;->c:[J

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-wide v6, v4, v1

    .line 1987
    cmp-long v6, v6, v2

    if-nez v6, :cond_2

    .line 1988
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iput-boolean v8, v1, Lcom/twitter/model/core/Tweet;->a:Z

    .line 1989
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->C:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v1, v8}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 1995
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->d:[J

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->d:[J

    array-length v1, v1

    if-lez v1, :cond_1

    .line 1997
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->d:[J

    array-length v4, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-wide v6, v1, v0

    .line 1998
    cmp-long v5, v6, v2

    if-nez v5, :cond_3

    .line 1999
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet;->a(Z)V

    .line 2000
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->D:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0, v8}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 2006
    :cond_1
    return-void

    .line 1986
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1997
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic G(Lcom/twitter/android/TweetFragment;)Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->ab:Z

    return v0
.end method

.method static synthetic H(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/api/ActivitySummary;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    return-object v0
.end method

.method static synthetic I(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/widget/EngagementActionBar;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    return-object v0
.end method

.method static synthetic J(Lcom/twitter/android/TweetFragment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic K(Lcom/twitter/android/TweetFragment;)Latr;
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    return-object v0
.end method

.method static synthetic L(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic M(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method private M()Z
    .locals 1

    .prologue
    .line 2009
    const-string/jumbo v0, "blocker_interstitial_enabled"

    .line 2010
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 2011
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic N(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method private N()Z
    .locals 4

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic O(Lcom/twitter/android/TweetFragment;)Lati;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->af:Lati;

    return-object v0
.end method

.method private a(Lcom/twitter/library/service/s;)I
    .locals 1

    .prologue
    .line 1155
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgo;

    .line 1156
    invoke-virtual {v0}, Lbgo;->G()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/timeline/ar;)Lcom/twitter/android/timeline/cf;
    .locals 4

    .prologue
    .line 2588
    new-instance v0, Lcom/twitter/android/timeline/cf$a;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/timeline/cf$a;-><init>(J)V

    .line 2589
    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/cf$a;->a(Lcom/twitter/model/timeline/ar;)Lcom/twitter/android/timeline/cf$a;

    move-result-object v0

    .line 2590
    invoke-virtual {v0}, Lcom/twitter/android/timeline/cf$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 2588
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/media/widget/TweetMediaView$a;
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/media/widget/TweetMediaView$a;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/media/widget/TweetMediaView$a;
    .locals 1

    .prologue
    .line 2045
    new-instance v0, Lcom/twitter/android/TweetFragment$7;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/TweetFragment$7;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/TweetView;)V

    return-object v0
.end method

.method private a(JLcom/twitter/model/timeline/ar;)V
    .locals 5

    .prologue
    .line 2594
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    .line 2595
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {p1, p2, v2, v3}, Lcom/twitter/database/schema/a;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    .line 2596
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2597
    if-eqz p3, :cond_0

    .line 2598
    const-string/jumbo v1, "subbranch"

    sget-object v2, Lcom/twitter/model/timeline/ar;->a:Lcom/twitter/util/serialization/i;

    invoke-static {v0, v1, p3, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 2601
    :cond_0
    const/16 v1, 0x23c1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2603
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1277
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1278
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq p2, v1, :cond_0

    .line 1279
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1280
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 1282
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;JLcom/twitter/model/timeline/ar;)V
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment;->a(JLcom/twitter/model/timeline/ar;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/model/core/Tweet;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;I)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    return-void
.end method

.method private a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 2034
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->aj:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/Tweet;)V

    .line 2035
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->aj:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TweetView;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 2037
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    .line 2038
    sget v0, Lcni;->a:F

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    .line 2039
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    .line 2040
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 938
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    .line 940
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    .line 939
    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;I)V

    .line 941
    return-void

    .line 940
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;I)V
    .locals 1

    .prologue
    .line 861
    invoke-direct {p0, p3}, Lcom/twitter/android/TweetFragment;->c(I)Lcom/twitter/android/timeline/cf;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;ILcom/twitter/android/timeline/cf;)V

    .line 862
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;ILcom/twitter/android/timeline/cf;)V
    .locals 10

    .prologue
    .line 866
    new-instance v1, Lbgo;

    .line 867
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->t:J

    .line 868
    invoke-static {p3}, Lcom/twitter/android/TweetFragment;->e(I)I

    move-result v8

    new-instance v9, Lbgk;

    invoke-direct {v9, p4}, Lbgk;-><init>(Lcom/twitter/android/timeline/cf;)V

    move-object v3, p2

    invoke-direct/range {v1 .. v9}, Lbgo;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJILbgp;)V

    .line 869
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, p3}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 870
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    invoke-virtual {v0, p4}, Latr;->a(Lcom/twitter/android/timeline/cf;)V

    .line 872
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V
    .locals 6

    .prologue
    .line 2478
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    .line 2480
    if-eqz v1, :cond_0

    .line 2481
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 2485
    :goto_0
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 2486
    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v2, v3, p2, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 2487
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    const-string/jumbo v4, ""

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string/jumbo v4, "media_forward:platform_photo_card"

    aput-object v4, v3, v0

    const/4 v0, 0x3

    aput-object p1, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2488
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    .line 2489
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 2490
    return-void

    .line 2483
    :cond_0
    const-string/jumbo v0, "tweet"

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2473
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    invoke-virtual {v0, p1, p2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2474
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1718
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 1719
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v1, v0, v2, v6}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 1721
    new-array v0, v5, [Ljava/lang/String;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "tweet"

    aput-object v3, v2, v4

    aput-object v6, v2, v5

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    aput-object p3, v2, v3

    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v2, "tweet::tweet::impression"

    .line 1722
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1723
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    .line 1724
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 1725
    return-void
.end method

.method private a(ZI)V
    .locals 4

    .prologue
    .line 1123
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getHeaderViewsCount()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->G:J

    .line 1125
    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/TweetFragment$a;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 1126
    invoke-virtual {p0, v0, p2, p1}, Lcom/twitter/android/TweetFragment;->a(IIZ)V

    .line 1128
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Z)Z
    .locals 0

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/twitter/android/TweetFragment;->y:Z

    return p1
.end method

.method private aO()V
    .locals 2

    .prologue
    .line 2620
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->al:Lcom/twitter/android/cm;

    if-eqz v0, :cond_0

    .line 2621
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->al:Lcom/twitter/android/cm;

    invoke-virtual {v0}, Lcom/twitter/android/cm;->b()V

    .line 2622
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/TweetFragment;->ae:J

    .line 2624
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;Z)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->f(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1648
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    .line 1649
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1651
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_1

    .line 1652
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1656
    :goto_0
    const-string/jumbo v2, "association"

    new-instance v3, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v3, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const/4 v0, 0x1

    .line 1658
    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->t:J

    .line 1659
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 1656
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1661
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1662
    const-string/jumbo v0, "pc"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1663
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v2

    invoke-static {v2}, Lcgi;->a(Lcgi;)[B

    move-result-object v2

    .line 1662
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1665
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    .line 1666
    return-void

    .line 1654
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/TweetFragment;)J
    .locals 2

    .prologue
    .line 217
    iget-wide v0, p0, Lcom/twitter/android/TweetFragment;->a_:J

    return-wide v0
.end method

.method private c(I)Lcom/twitter/android/timeline/cf;
    .locals 3

    .prologue
    .line 887
    packed-switch p1, :pswitch_data_0

    .line 902
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unexpected fetch type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 889
    :pswitch_1
    const/4 v0, 0x0

    .line 899
    :goto_0
    return-object v0

    .line 892
    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->x()Lbgy;

    move-result-object v0

    invoke-interface {v0}, Lbgy;->a()Lcom/twitter/android/timeline/cf;

    move-result-object v0

    goto :goto_0

    .line 895
    :pswitch_3
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->x()Lbgy;

    move-result-object v0

    invoke-interface {v0}, Lbgy;->b()Lcom/twitter/android/timeline/cf;

    move-result-object v0

    goto :goto_0

    .line 898
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/model/timeline/ar;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/model/timeline/ar;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/model/timeline/ar;)Lcom/twitter/android/timeline/cf;

    move-result-object v0

    goto :goto_0

    .line 887
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private c(Lcnk;)V
    .locals 4

    .prologue
    .line 1397
    iget-wide v0, p1, Lcnk;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1398
    iget-wide v0, p1, Lcnk;->c:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->a(J)I

    move-result v0

    .line 1399
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    .line 1400
    iget-object v2, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 1401
    iget v2, p1, Lcnk;->d:I

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 1404
    :cond_0
    return-void
.end method

.method private c(Lcom/twitter/model/core/Tweet;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2494
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2495
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 2496
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 2497
    new-array v1, v5, [Ljava/lang/String;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "tweet:::quoted_tweet:show"

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 2498
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2500
    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2469
    const-string/jumbo v0, ""

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2470
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private static e(I)I
    .locals 3

    .prologue
    .line 909
    packed-switch p0, :pswitch_data_0

    .line 928
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unexpected fetch type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 911
    :pswitch_1
    const/4 v0, 0x1

    .line 925
    :goto_0
    return v0

    .line 916
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 919
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 922
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 925
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 909
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic e(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/cn;
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->v()Lcom/twitter/android/cn;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$a;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    return-object v0
.end method

.method private f(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1757
    if-eqz p1, :cond_0

    .line 1758
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iput-boolean v2, v0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 1759
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    .line 1760
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->C:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 1761
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->C:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ToggleImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1771
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    .line 1772
    if-eqz v0, :cond_1

    .line 1773
    iget-object v1, v0, Lcom/twitter/library/api/ActivitySummary;->d:[J

    .line 1774
    iget-object v0, v0, Lcom/twitter/library/api/ActivitySummary;->c:[J

    .line 1780
    :goto_1
    new-instance v2, Lcom/twitter/library/api/ActivitySummary;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget v3, v3, Lcom/twitter/model/core/Tweet;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget v4, v4, Lcom/twitter/model/core/Tweet;->k:I

    .line 1781
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/twitter/library/api/ActivitySummary;-><init>(Ljava/lang/String;Ljava/lang/String;[J[J)V

    iput-object v2, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    .line 1782
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;Lcom/twitter/android/widget/TweetDetailView$b;)V

    .line 1783
    return-void

    .line 1763
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iput-boolean v3, v0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 1764
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    .line 1765
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->C:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 1766
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->C:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ToggleImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1776
    :cond_1
    new-array v1, v3, [J

    .line 1777
    new-array v0, v3, [J

    goto :goto_1
.end method

.method static synthetic g(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->W:Lcom/twitter/android/av/j;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->W:Lcom/twitter/android/av/j;

    return-object v0
.end method

.method private h(J)V
    .locals 3

    .prologue
    .line 2606
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->al:Lcom/twitter/android/cm;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/TweetFragment;->ae:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 2607
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->aO()V

    .line 2608
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->al:Lcom/twitter/android/cm;

    new-instance v1, Lcom/twitter/android/TweetFragment$10;

    invoke-direct {v1, p0}, Lcom/twitter/android/TweetFragment$10;-><init>(Lcom/twitter/android/TweetFragment;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/cm;->a(JLcom/twitter/library/network/livepipeline/g;)V

    .line 2615
    iput-wide p1, p0, Lcom/twitter/android/TweetFragment;->ae:J

    .line 2617
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/twitter/android/TweetFragment;)Lbpl;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ak:Lbpl;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/widget/ToggleImageButton;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->C:Lcom/twitter/android/widget/ToggleImageButton;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->W:Lcom/twitter/android/av/j;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->W:Lcom/twitter/android/av/j;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/revenue/o;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->X:Lcom/twitter/android/revenue/o;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/revenue/o;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->X:Lcom/twitter/android/revenue/o;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/TweetFragment;)Lbxa;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/widget/PageableListView;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->B()V

    return-void
.end method

.method static synthetic r(Lcom/twitter/android/TweetFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/android/TweetFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method private u()Latr;
    .locals 1

    .prologue
    .line 685
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Latn;

    .line 686
    invoke-interface {v0}, Latn;->e()Latr;

    move-result-object v0

    return-object v0
.end method

.method private v()Lcom/twitter/android/cn;
    .locals 1

    .prologue
    .line 691
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Latn;

    .line 692
    invoke-interface {v0}, Latn;->d()Lcom/twitter/android/cn;

    move-result-object v0

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private w()Lcom/twitter/android/timeline/bl;
    .locals 2

    .prologue
    .line 876
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment$a;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 877
    invoke-static {}, Lcom/twitter/android/timeline/bl;->bd_()Lcom/twitter/android/timeline/bl;

    move-result-object v1

    .line 876
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    return-object v0
.end method

.method static synthetic w(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method private x()Lbgy;
    .locals 2

    .prologue
    .line 882
    new-instance v0, Lbgz;

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->w()Lcom/twitter/android/timeline/bl;

    move-result-object v1

    invoke-direct {v0, v1}, Lbgz;-><init>(Lcom/twitter/android/timeline/bl;)V

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/timeline/bl;
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->w()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/android/TweetFragment;)Lcom/twitter/metrics/j;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v0

    return-object v0
.end method

.method private y()Z
    .locals 1

    .prologue
    .line 934
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/model/timeline/ar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic z(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 1108
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    new-instance v1, Lcom/twitter/android/TweetFragment$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/TweetFragment$3;-><init>(Lcom/twitter/android/TweetFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->post(Ljava/lang/Runnable;)Z

    .line 1115
    return-void
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    .prologue
    .line 502
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    return v0
.end method

.method protected N_()Z
    .locals 1

    .prologue
    .line 507
    invoke-static {}, Lcom/twitter/android/revenue/k;->a()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 489
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 490
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/library/widget/PageableListView;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    .line 491
    return-object v1
.end method

.method protected a(Lank;)Latn;
    .locals 2

    .prologue
    .line 678
    invoke-static {}, Latb;->a()Latb$a;

    move-result-object v0

    .line 679
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Latb$a;->a(Lamu;)Latb$a;

    move-result-object v0

    .line 680
    invoke-virtual {v0}, Latb$a;->a()Latn;

    move-result-object v0

    .line 678
    return-object v0
.end method

.method public a(IIZ)V
    .locals 2

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    .line 1140
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->A()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1141
    if-eqz p3, :cond_1

    .line 1142
    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/widget/PageableListView;->smoothScrollToPositionFromTop(II)V

    .line 1147
    :cond_0
    :goto_0
    return-void

    .line 1144
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/common/list/l;->a(II)V

    goto :goto_0
.end method

.method protected a(JJ)V
    .locals 3

    .prologue
    .line 519
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TweetListFragment;->a(JJ)V

    .line 520
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/cp;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/cp;->a(J)V

    .line 521
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/cp;

    const-string/jumbo v1, "tweet::stream::results"

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/cp;->a(JLjava/lang/String;)V

    .line 522
    return-void
.end method

.method public a(JLcom/twitter/model/core/Tweet;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1804
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1805
    iget-wide v4, p3, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    move v0, v1

    .line 1806
    :goto_0
    if-eqz p4, :cond_3

    .line 1807
    iput-boolean v2, p3, Lcom/twitter/model/core/Tweet;->c:Z

    .line 1808
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->D:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 1809
    if-eqz v0, :cond_0

    .line 1810
    const-string/jumbo v0, "self_unretweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1812
    :cond_0
    const-string/jumbo v0, "unretweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1822
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 1805
    goto :goto_0

    .line 1814
    :cond_3
    iput-boolean v1, p3, Lcom/twitter/model/core/Tweet;->c:Z

    .line 1815
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->D:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 1816
    if-eqz v0, :cond_4

    .line 1817
    const-string/jumbo v0, "self_retweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1819
    :cond_4
    const-string/jumbo v0, "retweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(JZZZ)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1827
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1854
    :cond_0
    :goto_0
    return-void

    .line 1832
    :cond_1
    if-nez p3, :cond_2

    .line 1833
    if-eqz p4, :cond_3

    .line 1834
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iput-boolean v2, v0, Lcom/twitter/model/core/Tweet;->c:Z

    .line 1835
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->D:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 1848
    :cond_2
    :goto_1
    new-instance v0, Lcom/twitter/library/api/activity/d;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    .line 1850
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->t:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/library/api/activity/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    const/4 v1, 0x2

    .line 1848
    invoke-virtual {p0, v0, v1, v6}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0

    .line 1837
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iput-boolean v6, v0, Lcom/twitter/model/core/Tweet;->c:Z

    .line 1838
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->D:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 1839
    if-nez p5, :cond_2

    .line 1840
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a099d

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1841
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    .prologue
    .line 1932
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1934
    packed-switch p2, :pswitch_data_0

    .line 1958
    :cond_0
    :goto_0
    return-void

    .line 1936
    :pswitch_0
    const/4 v1, -0x1

    if-ne p3, v1, :cond_0

    .line 1937
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_1

    .line 1938
    const-string/jumbo v1, "delete"

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1939
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/client/p;

    .line 1940
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1939
    invoke-static {v2, v3, v4}, Lbhb;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lbhb;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 1943
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1944
    const-string/jumbo v2, "status_id"

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->u:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1945
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1946
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 1948
    :cond_1
    new-instance v0, Lcpb;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "ANDROID-17421"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0

    .line 1934
    :pswitch_data_0
    .packed-switch 0x66
        :pswitch_0
    .end packed-switch
.end method

.method a(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 480
    const-string/jumbo v0, "as"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ActivitySummary;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    .line 481
    const-string/jumbo v0, "dw"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->y:Z

    .line 482
    const-string/jumbo v0, "fss"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->F:Z

    .line 483
    const-string/jumbo v0, "display_possibly_sensitive_media"

    .line 484
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->a:Z

    .line 485
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1375
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1394
    :goto_0
    return-void

    .line 1377
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v0

    .line 1378
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v1, p2}, Lcom/twitter/android/TweetFragment$a;->b(Landroid/database/Cursor;)V

    .line 1379
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment$a;->b()V

    .line 1380
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->x()Lbgy;

    move-result-object v2

    invoke-interface {v2}, Lbgy;->a()Lcom/twitter/android/timeline/cf;

    move-result-object v2

    .line 1381
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->x()Lbgy;

    move-result-object v3

    invoke-interface {v3}, Lbgy;->b()Lcom/twitter/android/timeline/cf;

    move-result-object v3

    .line 1380
    invoke-virtual {v1, v2, v3}, Latr;->a(Lcom/twitter/android/timeline/cf;Lcom/twitter/android/timeline/cf;)V

    .line 1382
    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->k:Z

    if-eqz v1, :cond_0

    .line 1383
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->k()V

    .line 1387
    :goto_1
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->z()V

    goto :goto_0

    .line 1385
    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Lcnk;)V

    goto :goto_1

    .line 1375
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1729
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1730
    iget-boolean v1, v0, Lcom/twitter/model/core/Tweet;->a:Z

    if-eqz v1, :cond_1

    .line 1731
    new-instance v1, Lbfp;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, v0, Lcom/twitter/model/core/Tweet;->u:J

    invoke-direct/range {v1 .. v7}, Lbfp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 1733
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfp;->a(Lcgi;)Lbfp;

    move-result-object v0

    const/4 v1, 0x4

    .line 1731
    invoke-virtual {p0, v0, v1, v8}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1735
    invoke-direct {p0, v8}, Lcom/twitter/android/TweetFragment;->f(Z)V

    .line 1736
    const-string/jumbo v0, "unfavorite"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1737
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ak:Lbpl;

    if-eqz v0, :cond_0

    .line 1738
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ak:Lbpl;

    invoke-virtual {v0, p1}, Lbpl;->c(Landroid/view/View;)V

    .line 1754
    :cond_0
    :goto_0
    return-void

    .line 1741
    :cond_1
    new-instance v1, Lbfm;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, v0, Lcom/twitter/model/core/Tweet;->u:J

    invoke-direct/range {v1 .. v7}, Lbfm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 1745
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbfm;->a(Lcgi;)Lbfm;

    move-result-object v1

    .line 1746
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfm;->a(Ljava/lang/Boolean;)Lbfm;

    move-result-object v0

    const/4 v1, 0x3

    .line 1741
    invoke-virtual {p0, v0, v1, v8}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1748
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->f(Z)V

    .line 1749
    const-string/jumbo v0, "favorite"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1750
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ak:Lbpl;

    if-eqz v0, :cond_0

    .line 1751
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ak:Lbpl;

    invoke-virtual {v0, p1}, Lbpl;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;I[J)V
    .locals 4

    .prologue
    .line 1962
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1963
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1964
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p2, v1}, Lcom/twitter/android/al;->c(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    .line 1974
    :goto_0
    return-void

    .line 1966
    :cond_0
    new-instance v1, Lcom/twitter/app/users/f;

    invoke-direct {v1}, Lcom/twitter/app/users/f;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    .line 1967
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/users/f;->a(J)Lcom/twitter/app/users/f;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->t:J

    .line 1968
    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/users/f;->b(J)Lcom/twitter/app/users/f;

    move-result-object v1

    .line 1969
    invoke-virtual {v1, p2}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v1

    .line 1970
    invoke-virtual {v1, p3}, Lcom/twitter/app/users/f;->a([J)Lcom/twitter/app/users/f;

    move-result-object v1

    const/4 v2, 0x1

    .line 1971
    invoke-virtual {v1, v2}, Lcom/twitter/app/users/f;->a(Z)Lcom/twitter/app/users/f;

    move-result-object v1

    .line 1972
    invoke-virtual {v1, v0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1966
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 496
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    .line 497
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/cp;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/cp;->b(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    .line 498
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 217
    check-cast p2, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 1678
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/TweetListFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 1680
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 1681
    instance-of v1, v0, Lcom/twitter/android/timeline/ak;

    if-eqz v1, :cond_1

    .line 1682
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/ak;

    .line 1683
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1684
    iget-object v0, v0, Lcom/twitter/android/timeline/ak;->b:Lcom/twitter/model/core/Tweet;

    .line 1685
    invoke-static {v1, v0}, Lcom/twitter/android/widget/ConfirmCancelPendingTweetDialog;->a(Landroid/support/v4/app/FragmentManager;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/widget/ConfirmCancelPendingTweetDialog;

    .line 1715
    :cond_0
    :goto_0
    return-void

    .line 1686
    :cond_1
    instance-of v1, v0, Lcom/twitter/android/timeline/cd;

    if-eqz v1, :cond_3

    .line 1687
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 1688
    invoke-static {v0}, Lbxh;->a(Lcom/twitter/android/timeline/cd;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "parent_tweet"

    .line 1689
    :goto_1
    const-string/jumbo v2, ""

    const-string/jumbo v3, "click"

    invoke-direct {p0, v2, v1, v3}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->u:J

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/TweetFragment;->a(JLcom/twitter/model/timeline/ar;)V

    goto :goto_0

    .line 1688
    :cond_2
    const-string/jumbo v1, "child_tweet"

    goto :goto_1

    .line 1691
    :cond_3
    instance-of v1, v0, Lcom/twitter/android/timeline/cf;

    if-eqz v1, :cond_0

    .line 1692
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 1693
    iget-object v1, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v1, v1, Lcom/twitter/model/timeline/ar;->c:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1703
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Latl;

    .line 1704
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Latl;->a(Z)V

    .line 1705
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    invoke-virtual {v1, v2}, Latr;->a(Lcom/twitter/model/timeline/ar;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1706
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/4 v3, 0x6

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;ILcom/twitter/android/timeline/cf;)V

    goto :goto_0

    .line 1695
    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->w()Lcom/twitter/android/timeline/bl;

    move-result-object v1

    add-int/lit8 v2, p3, -0x1

    invoke-virtual {v1, v2}, Lcom/twitter/android/timeline/bl;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/bk;

    .line 1696
    instance-of v2, v1, Lcom/twitter/android/timeline/cd;

    if-eqz v2, :cond_0

    .line 1697
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/cd;

    .line 1698
    iget-object v1, v1, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->u:J

    iget-object v0, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/TweetFragment;->a(JLcom/twitter/model/timeline/ar;)V

    goto :goto_0

    .line 1693
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lati;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->af:Lati;

    .line 355
    return-void
.end method

.method public a(Lcax;)V
    .locals 3

    .prologue
    .line 1491
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 1492
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcax;Lcom/twitter/library/widget/TweetView;)V

    .line 1493
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 542
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 543
    const v0, 0x7f04008e

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 544
    return-void
.end method

.method a(Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    .prologue
    .line 1670
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1671
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-static {p1, v0}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1673
    :cond_0
    return-void
.end method

.method public final a(Lcom/twitter/library/service/s;II)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 945
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 946
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 947
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v1

    .line 948
    packed-switch p2, :pswitch_data_0

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 950
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 953
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgo;

    .line 954
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v2

    invoke-virtual {v0}, Lbgo;->t()Lcom/twitter/android/timeline/cf;

    move-result-object v3

    .line 955
    invoke-virtual {v0}, Lbgo;->G()I

    move-result v0

    .line 954
    invoke-virtual {v2, v3, v1, v0}, Latr;->a(Lcom/twitter/android/timeline/cf;ZI)V

    .line 956
    packed-switch p3, :pswitch_data_1

    .line 1009
    :cond_1
    :goto_1
    :pswitch_1
    if-eqz v1, :cond_3

    .line 1010
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 959
    :pswitch_2
    if-nez v1, :cond_2

    .line 960
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->k()V

    .line 961
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 962
    if-eqz v0, :cond_1

    .line 963
    const v2, 0x7f1302e4

    .line 965
    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0a0984

    .line 966
    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x2

    .line 964
    invoke-static {v0, v2, v3, v4}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v2

    const v3, 0x7f0a0985

    .line 968
    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/twitter/android/TweetFragment$2;

    invoke-direct {v3, p0}, Lcom/twitter/android/TweetFragment$2;-><init>(Lcom/twitter/android/TweetFragment;)V

    invoke-virtual {v2, v0, v3}, Landroid/support/design/widget/Snackbar;->setAction(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 975
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    goto :goto_1

    .line 978
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    invoke-virtual {v0}, Latr;->a()V

    .line 979
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/service/s;)I

    move-result v0

    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    .line 980
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    invoke-virtual {v0}, Latr;->c()V

    .line 981
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    invoke-virtual {v0}, Latr;->d()V

    goto :goto_1

    .line 987
    :pswitch_3
    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/service/s;)I

    move-result v0

    if-nez v0, :cond_1

    .line 988
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    invoke-virtual {v0}, Latr;->c()V

    goto :goto_1

    .line 993
    :pswitch_4
    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/service/s;)I

    move-result v0

    if-nez v0, :cond_1

    .line 994
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    invoke-virtual {v0}, Latr;->d()V

    goto :goto_1

    .line 999
    :pswitch_5
    if-nez v1, :cond_1

    .line 1001
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment$a;->notifyDataSetChanged()V

    goto/16 :goto_1

    .line 1012
    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->z()V

    goto/16 :goto_0

    .line 1017
    :pswitch_6
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 1019
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/twitter/network/l;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 1020
    check-cast p1, Lcom/twitter/library/api/activity/d;

    invoke-virtual {p1}, Lcom/twitter/library/api/activity/d;->e()Lcom/twitter/library/api/ActivitySummary;

    move-result-object v0

    .line 1022
    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    .line 1023
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment$a;->notifyDataSetChanged()V

    .line 1024
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->G()V

    goto/16 :goto_0

    .line 1029
    :pswitch_7
    check-cast p1, Lbfm;

    .line 1030
    invoke-virtual {p1}, Lbfm;->s()[I

    move-result-object v1

    .line 1033
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v2, 0x193

    if-ne v0, v2, :cond_4

    .line 1034
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a([I)V

    .line 1038
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1044
    new-instance v0, Lcom/twitter/library/api/activity/d;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->t:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/library/api/activity/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    invoke-virtual {p0, v0, v7, v6}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1051
    :pswitch_8
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1059
    new-instance v0, Lcom/twitter/library/api/activity/d;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->t:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/library/api/activity/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    invoke-virtual {p0, v0, v7, v6}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1066
    :pswitch_9
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1067
    check-cast p1, Lbio;

    .line 1068
    iget-object v0, p1, Lbio;->a:Lcom/twitter/model/core/TwitterUser;

    .line 1069
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v1, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1070
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget v2, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    iput v2, v1, Lcom/twitter/model/core/Tweet;->l:I

    .line 1071
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->y:J

    iput-wide v2, v1, Lcom/twitter/model/core/Tweet;->m:J

    goto/16 :goto_0

    .line 1077
    :pswitch_a
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    .line 1081
    check-cast p1, Lbhf;

    .line 1082
    invoke-virtual {p1}, Lbhf;->e()Lcom/twitter/model/core/Translation;

    move-result-object v1

    .line 1083
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    .line 1084
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/Translation;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1085
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->c()V

    goto/16 :goto_0

    .line 1087
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1088
    iget-object v2, v1, Lcom/twitter/model/core/Translation;->c:Ljava/lang/String;

    iget-object v1, v1, Lcom/twitter/model/core/Translation;->c:Ljava/lang/String;

    invoke-static {v2, v1}, Lbxt;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1090
    const v2, 0x7f0a096d

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1093
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1096
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a096b

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1097
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 948
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 956
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/core/MediaEntity;)V
    .locals 3

    .prologue
    .line 1497
    invoke-static {p1}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1498
    new-instance v0, Lcom/twitter/android/av/ad;

    invoke-direct {v0}, Lcom/twitter/android/av/ad;-><init>()V

    .line 1499
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ad;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1500
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/ab;

    move-result-object v0

    const/4 v1, 0x1

    .line 1501
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->c(Z)Lcom/twitter/library/av/ab;

    move-result-object v0

    const/4 v1, 0x0

    .line 1502
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->e(Z)Lcom/twitter/library/av/ab;

    move-result-object v0

    .line 1503
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->a(Ljava/lang/Object;)V

    .line 1508
    :goto_0
    return-void

    .line 1505
    :cond_0
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 1506
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 668
    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 669
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->h(J)V

    .line 670
    invoke-static {p1}, Lcom/twitter/library/card/CardContextFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/card/CardContext;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->g:Lcom/twitter/library/card/CardContext;

    .line 671
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 672
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->r()V

    .line 673
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;Laji;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 698
    iput-object p2, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    .line 699
    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 700
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-direct {p0, v4, v5}, Lcom/twitter/android/TweetFragment;->h(J)V

    .line 701
    invoke-static {p1}, Lcom/twitter/library/card/CardContextFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/card/CardContext;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->g:Lcom/twitter/library/card/CardContext;

    .line 702
    iput-object p3, p0, Lcom/twitter/android/TweetFragment;->q:Laji;

    .line 703
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ct;->a(Lcom/twitter/library/client/Session;)V

    .line 704
    iput-boolean p4, p0, Lcom/twitter/android/TweetFragment;->ab:Z

    .line 705
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->aa:Lcom/twitter/android/cc;

    if-nez v0, :cond_a

    .line 706
    new-instance v0, Lcom/twitter/android/cc;

    invoke-direct {v0, p1, p3}, Lcom/twitter/android/cc;-><init>(Lcom/twitter/model/core/Tweet;Laji;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->aa:Lcom/twitter/android/cc;

    .line 711
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->x:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 717
    invoke-static {p1}, Lcom/twitter/android/av/f;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    iput-boolean v1, p0, Lcom/twitter/android/TweetFragment;->x:Z

    .line 719
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->g:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/EngagementActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 724
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/EngagementActionBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 725
    new-instance v0, Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, p0, v3, p0}, Lcom/twitter/android/TweetFragment$a;-><init>(Lcom/twitter/android/TweetFragment;Landroid/content/Context;Lcom/twitter/android/av;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    .line 727
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->K_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 728
    new-instance v0, Lcom/twitter/android/TweetFragment$16;

    invoke-direct {v0, p0}, Lcom/twitter/android/TweetFragment$16;-><init>(Lcom/twitter/android/TweetFragment;)V

    .line 736
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/TweetFragment;->a(Ljava/util/List;I)V

    .line 739
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    .line 740
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v3

    .line 743
    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    if-nez v4, :cond_3

    .line 747
    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->z:Landroid/view/View;

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/PageableListView;->a(Landroid/view/View;)V

    .line 750
    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->b()V

    .line 752
    if-eqz v3, :cond_2

    .line 753
    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->a()V

    .line 754
    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    .line 756
    :cond_2
    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    .line 758
    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/PageableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 761
    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v0

    .line 762
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Latr;->a(J)V

    .line 763
    invoke-virtual {v0}, Latr;->b()Z

    move-result v4

    .line 764
    if-nez v4, :cond_b

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->k:Z

    .line 765
    if-eqz v4, :cond_c

    .line 766
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 776
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    new-instance v1, Lcom/twitter/android/TweetFragment$17;

    invoke-direct {v1, p0}, Lcom/twitter/android/TweetFragment$17;-><init>(Lcom/twitter/android/TweetFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->setOnTweetAnalyticsClickListener(Landroid/view/View$OnClickListener;)V

    .line 783
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    if-nez v0, :cond_4

    .line 784
    new-instance v0, Lcom/twitter/library/api/activity/d;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->t:J

    invoke-direct {v0, v1, p2, v4, v5}, Lcom/twitter/library/api/activity/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 788
    :cond_4
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->q()Z

    move-result v0

    if-nez v0, :cond_5

    .line 789
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->B()V

    .line 792
    :cond_5
    if-eqz v3, :cond_6

    .line 793
    invoke-virtual {p0, v2}, Lcom/twitter/android/TweetFragment;->b(Z)V

    .line 797
    :cond_6
    const-string/jumbo v0, "translate_tweet_auto"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 798
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->F()V

    .line 799
    const-string/jumbo v0, "translation_auto"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 802
    :cond_7
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 803
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 804
    if-eqz v3, :cond_9

    .line 805
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 806
    if-eqz v0, :cond_9

    .line 807
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 808
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_d

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 809
    :goto_3
    const-string/jumbo v4, "email_redirect_retweet"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_8

    const-string/jumbo v4, "retweet"

    .line 810
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 811
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->c:Z

    .line 814
    if-nez v0, :cond_e

    .line 815
    new-instance v0, Lcom/twitter/android/bm$a;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/bm$a;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    const/16 v1, 0x65

    .line 816
    invoke-virtual {v0, v1}, Lcom/twitter/android/bm$a;->a(I)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 817
    invoke-virtual {v0, p0}, Lcom/twitter/android/bm$a;->a(Lcom/twitter/android/bm$b;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 818
    invoke-virtual {v0, p0}, Lcom/twitter/android/bm$a;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 819
    invoke-virtual {v0}, Lcom/twitter/android/bm$a;->a()Lcom/twitter/android/bm;

    move-result-object v0

    .line 820
    invoke-virtual {v0}, Lcom/twitter/android/bm;->b()V

    .line 839
    :cond_9
    :goto_4
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->v()Lcom/twitter/android/cn;

    move-result-object v0

    .line 842
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/cn;->a(Lcom/twitter/library/client/Session;Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    .line 844
    new-instance v2, Lcom/twitter/android/TweetFragment$18;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/TweetFragment$18;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/cn;)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->ag:Lrx/j;

    .line 858
    return-void

    .line 708
    :cond_a
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->aa:Lcom/twitter/android/cc;

    invoke-virtual {v0, p1, p3}, Lcom/twitter/android/cc;->a(Lcom/twitter/model/core/Tweet;Laji;)V

    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 764
    goto/16 :goto_1

    .line 769
    :cond_c
    new-instance v0, Lcom/twitter/android/timeline/cd$b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/android/timeline/cd$b;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 770
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cd$b;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$b;

    .line 771
    invoke-virtual {v0}, Lcom/twitter/android/timeline/cd$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 772
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment$a;->k()Lcjt;

    move-result-object v1

    new-instance v4, Lcbl;

    .line 773
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    .line 772
    invoke-interface {v1, v4}, Lcjt;->a(Lcbi;)Lcbi;

    .line 774
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_2

    .line 808
    :cond_d
    const-string/jumbo v0, ""

    goto/16 :goto_3

    .line 822
    :cond_e
    const-string/jumbo v0, "email_redirect_retweet"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_4

    .line 824
    :cond_f
    const-string/jumbo v1, "email_redirect_favorite"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_10

    const-string/jumbo v1, "favorite"

    .line 826
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string/jumbo v1, "like"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 829
    :cond_10
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->a:Z

    if-nez v0, :cond_11

    .line 830
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->s()V

    goto :goto_4

    .line 832
    :cond_11
    const-string/jumbo v0, "email_redirect_favorite"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method public a(Lcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 1869
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "tweet::retweet_dialog::dismiss"

    aput-object v3, v1, v2

    .line 1870
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1871
    return-void
.end method

.method public a(Lcom/twitter/model/core/ad;)V
    .locals 10

    .prologue
    .line 1471
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    if-eqz v0, :cond_0

    .line 1472
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "open_link"

    const-string/jumbo v2, "platform_card"

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1476
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v0

    .line 1477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "tweet::"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::open_link"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1478
    const-string/jumbo v0, "tweet::tweet::impression"

    .line 1480
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/client/BrowserDataSourceFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/client/BrowserDataSource;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/android/TweetFragment;->a_:J

    const-string/jumbo v7, "tweet::tweet::impression"

    .line 1481
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v8

    const/4 v9, 0x0

    move-object v3, p1

    .line 1480
    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 1482
    return-void
.end method

.method public a(Lcom/twitter/model/core/b;)V
    .locals 2

    .prologue
    .line 1448
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1449
    invoke-static {v0, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/b;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1450
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    invoke-virtual {v0, p1}, Lbxa;->a(Lcom/twitter/model/core/b;)V

    .line 1451
    return-void
.end method

.method public a(Lcom/twitter/model/core/h;)V
    .locals 2

    .prologue
    .line 1440
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1441
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/h;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1442
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    invoke-virtual {v0, p1}, Lbxa;->a(Lcom/twitter/model/core/h;)V

    .line 1443
    return-void
.end method

.method public a(Lcom/twitter/model/core/q;)V
    .locals 1

    .prologue
    .line 1456
    iget-object v0, p1, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->b(Ljava/lang/String;)V

    .line 1457
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    invoke-virtual {v0, p1}, Lbxa;->a(Lcom/twitter/model/core/q;)V

    .line 1458
    return-void
.end method

.method public a(Lcom/twitter/model/geo/TwitterPlace;)V
    .locals 1

    .prologue
    .line 1462
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1463
    if-eqz v0, :cond_0

    .line 1464
    invoke-static {v0, p1}, Lcom/twitter/android/geo/places/a;->a(Landroid/content/Context;Lcom/twitter/model/geo/TwitterPlace;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    .line 1466
    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 1

    .prologue
    .line 512
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Z)V

    .line 513
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/cp;

    invoke-virtual {v0}, Lcom/twitter/android/cp;->a()V

    .line 514
    return-void
.end method

.method public a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z
    .locals 6

    .prologue
    .line 1890
    iget-wide v2, p3, Lcom/twitter/model/core/Tweet;->C:J

    .line 1891
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 1892
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 1893
    if-eqz v4, :cond_0

    .line 1894
    new-instance v0, Lcom/twitter/android/TweetFragment$6;

    move-object v1, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/TweetFragment$6;-><init>(Lcom/twitter/android/TweetFragment;JLandroid/app/Activity;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v4, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1919
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public ae_()V
    .locals 3

    .prologue
    .line 1786
    new-instance v0, Lcom/twitter/android/bm$a;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/bm$a;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    const/16 v1, 0x65

    .line 1787
    invoke-virtual {v0, v1}, Lcom/twitter/android/bm$a;->a(I)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 1788
    invoke-virtual {v0, p0}, Lcom/twitter/android/bm$a;->a(Lcom/twitter/android/bm$b;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 1789
    invoke-virtual {v0, p0}, Lcom/twitter/android/bm$a;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 1790
    invoke-virtual {v0}, Lcom/twitter/android/bm$a;->a()Lcom/twitter/android/bm;

    move-result-object v0

    .line 1791
    invoke-virtual {v0}, Lcom/twitter/android/bm;->a()V

    .line 1792
    return-void
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/list/j;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lank;)Latn;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2582
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->b()V

    .line 2583
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-static {v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/content/Intent;Landroid/view/ViewGroup;)V

    .line 2584
    return-void
.end method

.method public b(J)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 1414
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    .line 1415
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 1416
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->t:J

    .line 1417
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v6, v0

    .line 1424
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1425
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v5

    move-wide v2, p1

    move-object v7, v4

    .line 1424
    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V

    .line 1426
    return-void

    .line 1419
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 1420
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->t:J

    .line 1421
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v6, v0

    goto :goto_0
.end method

.method public b(JLcom/twitter/model/core/Tweet;Z)V
    .locals 3

    .prologue
    .line 1859
    iget-wide v0, p3, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1860
    :goto_0
    if-eqz v0, :cond_0

    .line 1861
    const-string/jumbo v0, "self_quote"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1863
    :cond_0
    const-string/jumbo v0, "quote"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1864
    return-void

    .line 1859
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 1924
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1925
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->D()V

    .line 1927
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 1876
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "tweet::retweet_dialog::impression"

    aput-object v3, v1, v2

    .line 1877
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1876
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1878
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 1119
    iget v0, p0, Lcom/twitter/android/TweetFragment;->B:I

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/TweetFragment;->a(ZI)V

    .line 1120
    return-void
.end method

.method public b(Lcom/twitter/model/core/ad;)Z
    .locals 1

    .prologue
    .line 1486
    const/4 v0, 0x1

    return v0
.end method

.method protected synthetic c(Lank;)Lcom/twitter/app/common/abs/c;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lank;)Latn;

    move-result-object v0

    return-object v0
.end method

.method public c(J)V
    .locals 3

    .prologue
    .line 1430
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1431
    if-eqz v0, :cond_0

    .line 1432
    invoke-static {v0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->c(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1435
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 1330
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1331
    if-eqz p1, :cond_1

    .line 1332
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->D()V

    .line 1337
    :cond_0
    :goto_0
    return-void

    .line 1334
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    goto :goto_0
.end method

.method protected c_(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 526
    packed-switch p1, :pswitch_data_0

    .line 535
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->c_(I)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 528
    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->c_(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v2

    invoke-virtual {v2}, Latr;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    .line 529
    invoke-virtual {v2}, Lcom/twitter/android/TweetFragment$a;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->y()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 532
    :pswitch_1
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->c_(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->u()Latr;

    move-result-object v2

    invoke-virtual {v2}, Latr;->f()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 526
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected synthetic d(Lank;)Lann;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lank;)Latn;

    move-result-object v0

    return-object v0
.end method

.method public d(J)V
    .locals 3

    .prologue
    .line 1512
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;JLcom/twitter/library/widget/TweetView;)V

    .line 1513
    return-void
.end method

.method public e(J)V
    .locals 1

    .prologue
    .line 1882
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1883
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->D()V

    .line 1885
    :cond_0
    return-void
.end method

.method public f()Lcom/twitter/library/api/ActivitySummary;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    return-object v0
.end method

.method public f(J)V
    .locals 3

    .prologue
    .line 2463
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 2464
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 2466
    :cond_0
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 1286
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    .line 1287
    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getMeasuredHeight()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    .line 1288
    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getMeasuredHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 1296
    :cond_0
    :goto_0
    return-void

    .line 1291
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1292
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->z:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->z:Landroid/view/View;

    .line 1293
    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1294
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->z:Landroid/view/View;

    iget v3, p0, Lcom/twitter/android/TweetFragment;->A:I

    if-eqz v0, :cond_3

    .line 1295
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1294
    :goto_2
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/TweetFragment;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 1293
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1295
    :cond_3
    iget v0, p0, Lcom/twitter/android/TweetFragment;->A:I

    goto :goto_2
.end method

.method k()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1299
    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->k:Z

    if-nez v0, :cond_1

    .line 1321
    :cond_0
    :goto_0
    return-void

    .line 1303
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1304
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment$a;->notifyDataSetChanged()V

    .line 1305
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1306
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->z:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/ui/k;->a(Landroid/app/Activity;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->a(Landroid/view/View;I)V

    .line 1307
    iget v0, p0, Lcom/twitter/android/TweetFragment;->B:I

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/TweetFragment;->a(ZI)V

    .line 1309
    :cond_2
    iput-boolean v2, p0, Lcom/twitter/android/TweetFragment;->k:Z

    .line 1311
    const-string/jumbo v0, "urt_conv:complete"

    .line 1313
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v1

    sget-object v2, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 1312
    invoke-static {v0, v1, v2}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;

    move-result-object v0

    .line 1314
    iget-wide v2, p0, Lcom/twitter/android/TweetFragment;->a_:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/e;->b(J)V

    .line 1315
    invoke-virtual {v0}, Lcom/twitter/metrics/e;->j()V

    .line 1317
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    if-eqz v0, :cond_0

    .line 1318
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "impression"

    const-string/jumbo v2, "platform_card"

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 1325
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TweetDetailView;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->a(ZI)V

    .line 1326
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1519
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1520
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 1522
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v2}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v7

    .line 1523
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    .line 1524
    sparse-switch v1, :sswitch_data_0

    .line 1613
    :cond_0
    :goto_0
    return-void

    .line 1527
    :sswitch_0
    const v0, 0x7f13031a

    if-ne v1, v0, :cond_1

    .line 1528
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    invoke-virtual {v0}, Lbxa;->c()V

    .line 1532
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->s:J

    .line 1533
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->b(J)V

    goto :goto_0

    .line 1530
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    invoke-virtual {v0}, Lbxa;->d()V

    goto :goto_1

    .line 1537
    :sswitch_1
    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->m:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 1538
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->z()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1539
    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->b(J)V

    goto :goto_0

    .line 1541
    :cond_2
    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->b(J)V

    goto :goto_0

    .line 1546
    :sswitch_2
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1547
    check-cast p1, Lcom/twitter/ui/widget/ActionButton;

    .line 1548
    invoke-virtual {p1}, Lcom/twitter/ui/widget/ActionButton;->isChecked()Z

    move-result v1

    .line 1549
    if-eqz v1, :cond_3

    const-string/jumbo v0, "unfollow"

    .line 1551
    :goto_2
    if-eqz v1, :cond_4

    .line 1552
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iput v10, v1, Lcom/twitter/model/core/Tweet;->l:I

    .line 1553
    iget-object v8, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/client/p;

    new-instance v1, Lbhs;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v4, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1554
    invoke-virtual {v6}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 1553
    invoke-virtual {v8, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 1561
    :goto_3
    invoke-virtual {p1}, Lcom/twitter/ui/widget/ActionButton;->toggle()V

    .line 1562
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 1563
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v1, v2, v3, v9}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 1564
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v9}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 1566
    new-array v2, v11, [Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "tweet"

    aput-object v4, v3, v10

    aput-object v9, v3, v11

    const/4 v4, 0x2

    aput-object v7, v3, v4

    const/4 v4, 0x3

    aput-object v9, v3, v4

    aput-object v0, v3, v12

    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v10

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1549
    :cond_3
    const-string/jumbo v0, "follow"

    goto :goto_2

    .line 1556
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iput v11, v1, Lcom/twitter/model/core/Tweet;->l:I

    .line 1557
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 1558
    iget-object v8, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/client/p;

    new-instance v1, Lbhq;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v4, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1559
    invoke-virtual {v6}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 1558
    invoke-virtual {v8, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    goto :goto_3

    .line 1569
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1571
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v0

    .line 1569
    invoke-static {v1, v12, v0}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 1576
    :sswitch_3
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1577
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->c()V

    goto/16 :goto_0

    .line 1579
    :cond_6
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->F()V

    .line 1580
    const-string/jumbo v0, "translation_button"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1586
    :sswitch_4
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->v()Lcom/twitter/android/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/cn;->a()Lcom/twitter/model/av/h;

    move-result-object v0

    .line 1588
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    if-eqz v1, :cond_0

    .line 1589
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    new-instance v2, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$a;

    invoke-direct {v2, v0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$a;-><init>(Lcom/twitter/model/av/h;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;->c(Lako;)V

    goto/16 :goto_0

    .line 1596
    :sswitch_5
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    invoke-virtual {v0}, Lbxa;->a()V

    .line 1597
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1598
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    .line 1599
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->x:J

    .line 1601
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 1600
    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1602
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1603
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1604
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    .line 1605
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 1606
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1524
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f130003 -> :sswitch_2
        0x7f13005a -> :sswitch_0
        0x7f13027b -> :sswitch_5
        0x7f13031a -> :sswitch_0
        0x7f130527 -> :sswitch_1
        0x7f130821 -> :sswitch_3
        0x7f130826 -> :sswitch_4
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 360
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 361
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "tweet"

    .line 362
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 361
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 364
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v1

    .line 365
    const-string/jumbo v0, "source_association"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/i;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 366
    const-string/jumbo v0, "timeline_moment"

    .line 367
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/i;->i(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    sget-object v2, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->s:Lcom/twitter/model/moments/Moment;

    .line 369
    const-string/jumbo v0, "subbranch"

    .line 370
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/i;->i(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    sget-object v1, Lcom/twitter/model/timeline/ar;->a:Lcom/twitter/util/serialization/i;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ar;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/model/timeline/ar;

    .line 373
    if-eqz p1, :cond_0

    .line 374
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Landroid/os/Bundle;)V

    .line 376
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e03a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetFragment;->A:I

    .line 377
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetFragment;->B:I

    .line 378
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    .line 380
    new-instance v0, Lcom/twitter/android/TweetFragment$11;

    const-string/jumbo v1, "tweet:stream:tweet:link:open_link"

    .line 381
    invoke-static {v4, v1, v4, v4}, Lbxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbxc;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/TweetFragment$d;

    invoke-direct {v6, p0, p0, v3}, Lcom/twitter/android/TweetFragment$d;-><init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/TweetFragment$11;-><init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lcom/twitter/android/ck;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    .line 403
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/ct;->a(Z)V

    .line 404
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ct;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 406
    new-instance v0, Lbxa;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/TweetFragment$12;

    invoke-direct {v2, p0}, Lcom/twitter/android/TweetFragment$12;-><init>(Lcom/twitter/android/TweetFragment;)V

    .line 412
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    new-instance v5, Lcom/twitter/android/TweetFragment$13;

    invoke-direct {v5, p0}, Lcom/twitter/android/TweetFragment$13;-><init>(Lcom/twitter/android/TweetFragment;)V

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->Z:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct/range {v0 .. v6}, Lbxa;-><init>(Landroid/content/Context;Lcom/twitter/util/object/j;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/util/object/j;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lbxa;

    .line 420
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 422
    instance-of v1, v0, Lcom/twitter/android/TweetActivity;

    if-eqz v1, :cond_1

    .line 423
    new-instance v1, Lcom/twitter/android/TweetFragment$14;

    invoke-direct {v1, p0}, Lcom/twitter/android/TweetFragment$14;-><init>(Lcom/twitter/android/TweetFragment;)V

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->ai:Lakv;

    .line 450
    new-instance v1, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    check-cast v0, Lcom/twitter/android/TweetActivity;

    const/16 v2, 0x69

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    .line 452
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ai:Lakv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;->a(Lakv;)V

    .line 455
    :cond_1
    invoke-static {}, Lcom/twitter/android/cm;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 456
    new-instance v0, Lcom/twitter/android/cm;

    invoke-direct {v0}, Lcom/twitter/android/cm;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->al:Lcom/twitter/android/cm;

    .line 458
    :cond_2
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356
    packed-switch p1, :pswitch_data_0

    .line 1363
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1358
    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->E()Lapb;

    move-result-object v6

    .line 1359
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v6, Lapb;->d:Landroid/net/Uri;

    iget-object v3, v6, Lapb;->e:[Ljava/lang/String;

    iget-object v4, v6, Lapb;->a:Ljava/lang/String;

    iget-object v5, v6, Lapb;->b:[Ljava/lang/String;

    iget-object v6, v6, Lapb;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1356
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ag:Lrx/j;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ag:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ai:Lakv;

    if-eqz v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ai:Lakv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;->b(Lakv;)V

    .line 470
    :cond_1
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroy()V

    .line 471
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 217
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1370
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1634
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 1635
    const v3, 0x7f13081f

    if-ne v2, v3, :cond_1

    .line 1636
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "tweet"

    aput-object v4, v3, v1

    aput-object v6, v3, v0

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    .line 1637
    invoke-virtual {v5}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v6, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "copy"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 1636
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 1638
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 1639
    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v3}, Lcom/twitter/model/core/Tweet;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1640
    const v3, 0x7f0a021b

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1643
    :cond_0
    :goto_0
    return v0

    :cond_1
    const v3, 0x7f13027b

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/ct;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2, v3}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 654
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 655
    const-string/jumbo v0, "as"

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/api/ActivitySummary;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 656
    const-string/jumbo v0, "dw"

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 657
    const-string/jumbo v0, "fss"

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 659
    const-string/jumbo v0, "display_possibly_sensitive_media"

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 660
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/os/Bundle;)V

    .line 661
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 1252
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onStart()V

    .line 1253
    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->ad:Z

    if-eqz v0, :cond_0

    .line 1254
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->e()V

    .line 1255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->ad:Z

    .line 1257
    :cond_0
    invoke-static {p0}, Lcom/twitter/library/provider/o;->b(Lcom/twitter/library/provider/o$a;)V

    .line 1258
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    .line 1259
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->h(J)V

    .line 1261
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 1266
    invoke-static {p0}, Lcom/twitter/library/provider/o;->a(Lcom/twitter/library/provider/o$a;)V

    .line 1267
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 1268
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/cp;

    const-string/jumbo v3, "tweet::stream::results"

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/cp;->a(JLjava/lang/String;)V

    .line 1269
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/cp;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/cp;->a(J)V

    .line 1270
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->f()V

    .line 1271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->ad:Z

    .line 1272
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->aO()V

    .line 1273
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onStop()V

    .line 1274
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 548
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 549
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->s()V

    .line 550
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v0

    .line 551
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    .line 552
    invoke-virtual {v2, v5}, Lcom/twitter/library/widget/PageableListView;->setScrollingCacheEnabled(Z)V

    .line 553
    invoke-virtual {v2, v5}, Lcom/twitter/library/widget/PageableListView;->setCacheColorHint(I)V

    .line 554
    new-instance v1, Lcom/twitter/android/TweetFragment$15;

    invoke-direct {v1, p0, v2, v0}, Lcom/twitter/android/TweetFragment$15;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/PageableListView;Lcno;)V

    invoke-virtual {v2, v1}, Lcom/twitter/library/widget/PageableListView;->setOnPageScrollListener(Lcom/twitter/library/widget/PageableListView$a;)V

    .line 604
    invoke-virtual {v2}, Lcom/twitter/library/widget/PageableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 605
    const v0, 0x7f040411

    .line 606
    invoke-virtual {v3, v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetDetailView;

    .line 607
    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, p0}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 608
    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getNamePanel()Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 609
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetDetailView;->setOnMomentClickListener(Lwn;)V

    .line 611
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v4, 0x7f1302a2

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 612
    new-instance v4, Lcom/twitter/android/widget/TweetDetailView$c;

    invoke-direct {v4, v1, v0}, Lcom/twitter/android/widget/TweetDetailView$c;-><init>(Landroid/view/View;Lcom/twitter/android/widget/TweetDetailView;)V

    .line 614
    invoke-static {v1, v4}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 615
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->p:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v1, v4}, Lcom/twitter/library/widget/PageableListView;->setExploreByTouchHelper(Landroid/support/v4/widget/ExploreByTouchHelper;)V

    .line 617
    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getTweetTextView()Landroid/widget/TextView;

    move-result-object v1

    .line 618
    if-eqz v1, :cond_0

    .line 619
    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 622
    :cond_0
    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/ui/widget/BadgeView;

    invoke-virtual {v1, p0}, Lcom/twitter/ui/widget/BadgeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 623
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetDetailView;->setQuoteTweetClickListener(Landroid/view/View$OnClickListener;)V

    .line 624
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetDetailView;->setQuoteTweetLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 625
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetDetailView;->setTranslationButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 626
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetDetailView;->setOnMediaMonetizationClickListener(Landroid/view/View$OnClickListener;)V

    .line 627
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->setOnCaretClickListener(Landroid/view/View$OnClickListener;)V

    .line 628
    if-eqz p2, :cond_1

    .line 629
    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TweetDetailView;->b(Landroid/os/Bundle;)V

    .line 631
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->s:Lcom/twitter/model/moments/Moment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->setMoment(Lcom/twitter/model/moments/Moment;)V

    .line 632
    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    .line 633
    iput-boolean v5, p0, Lcom/twitter/android/TweetFragment;->ad:Z

    .line 635
    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->c:Lcom/twitter/android/widget/EngagementActionBar;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    .line 636
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v1}, Lcom/twitter/android/widget/EngagementActionBar;->b()V

    .line 637
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    const v4, 0x7f130032

    invoke-virtual {v1, v4}, Lcom/twitter/android/widget/EngagementActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/ToggleImageButton;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->C:Lcom/twitter/android/widget/ToggleImageButton;

    .line 638
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    const v4, 0x7f130071

    invoke-virtual {v1, v4}, Lcom/twitter/android/widget/EngagementActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/ToggleImageButton;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->D:Lcom/twitter/android/widget/ToggleImageButton;

    .line 639
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->E:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v1}, Lcom/twitter/android/widget/EngagementActionBar;->c()V

    .line 640
    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->a()V

    .line 641
    const v0, 0x7f040283

    invoke-virtual {v3, v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->z:Landroid/view/View;

    .line 643
    invoke-static {}, Lbpm;->a()Lbpm;

    move-result-object v0

    invoke-virtual {v0}, Lbpm;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 644
    new-instance v0, Lbpl;

    check-cast p1, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Lbpl;-><init>(Landroid/widget/FrameLayout;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->ak:Lbpl;

    .line 645
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ak:Lbpl;

    .line 646
    invoke-static {}, Lbpm;->a()Lbpm;

    move-result-object v1

    invoke-virtual {v1}, Lbpm;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbpl;->a(Ljava/util/List;)V

    .line 648
    :cond_2
    sget-object v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/cq;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/TweetFragment;->J:I

    .line 649
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    .line 648
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/cq;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/client/v;)Lcom/twitter/android/cp;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/cp;

    .line 650
    return-void
.end method

.method public q()V
    .locals 3

    .prologue
    .line 1796
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/model/core/Tweet;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Z)V

    .line 1797
    const-string/jumbo v0, "share"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    .line 1798
    return-void
.end method

.method public r()V
    .locals 2

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    if-eqz v0, :cond_0

    .line 2020
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/TweetFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment$a;->notifyDataSetChanged()V

    .line 2023
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    if-eqz v0, :cond_1

    .line 2024
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->g:Lcom/twitter/library/card/CardContext;

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/card/CardContext;)V

    .line 2026
    :cond_1
    return-void
.end method

.method protected s()V
    .locals 4

    .prologue
    .line 2546
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2547
    new-instance v1, Lcom/twitter/android/TweetFragment$8;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/TweetFragment$8;-><init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/FragmentActivity;)V

    .line 2564
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a099a

    .line 2565
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a044c

    .line 2566
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a00c3

    .line 2567
    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00f6

    const/4 v3, 0x0

    .line 2568
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 2569
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 2570
    new-instance v2, Lcom/twitter/android/TweetFragment$9;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/TweetFragment$9;-><init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/FragmentActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2577
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 2578
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 2721
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x66

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0993

    .line 2722
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0992

    .line 2723
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 2724
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 2725
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2726
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2727
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2728
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2729
    return-void
.end method
