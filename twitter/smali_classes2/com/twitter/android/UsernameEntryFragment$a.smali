.class public Lcom/twitter/android/UsernameEntryFragment$a;
.super Landroid/os/Handler;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/UsernameEntryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/UsernameEntryFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/UsernameEntryFragment;)V
    .locals 1

    .prologue
    .line 387
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 388
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/UsernameEntryFragment$a;->a:Ljava/lang/ref/WeakReference;

    .line 389
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 440
    invoke-virtual {p0, p1}, Lcom/twitter/android/UsernameEntryFragment$a;->removeMessages(I)V

    .line 441
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/UsernameEntryFragment$a;->sendEmptyMessageDelayed(IJ)Z

    .line 442
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 393
    iget-object v0, p0, Lcom/twitter/android/UsernameEntryFragment$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsernameEntryFragment;

    .line 395
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/UsernameEntryFragment;->Y()Z

    move-result v1

    if-nez v1, :cond_1

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    iget-object v1, v0, Lcom/twitter/android/UsernameEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    .line 401
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 403
    :pswitch_0
    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 404
    sget-object v2, Lcom/twitter/model/util/g;->d:Ljava/util/regex/Pattern;

    .line 405
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 406
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    .line 409
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    const/4 v4, 0x5

    if-ge v3, v4, :cond_2

    .line 410
    iget-object v1, v0, Lcom/twitter/android/UsernameEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    const v2, 0x7f0a07c3

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 411
    sget-object v2, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    .line 412
    sget-object v1, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    .line 430
    :goto_1
    invoke-static {v0}, Lcom/twitter/android/UsernameEntryFragment;->b(Lcom/twitter/android/UsernameEntryFragment;)Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    new-instance v3, Lcom/twitter/android/ValidationState;

    invoke-direct {v3, v2, v1}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v0, v3}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    goto :goto_0

    .line 413
    :cond_2
    sget-object v3, Lcom/twitter/model/util/g;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez v2, :cond_3

    .line 416
    invoke-virtual {v0}, Lcom/twitter/android/UsernameEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 417
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v4, 0x2

    .line 419
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-static {v2, v3, v4, v1}, Lbhl;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v3

    .line 421
    sget-object v2, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    .line 422
    sget-object v1, Lcom/twitter/android/ValidationState$Level;->b:Lcom/twitter/android/ValidationState$Level;

    .line 423
    invoke-virtual {v0, v3}, Lcom/twitter/android/UsernameEntryFragment;->a(Lcom/twitter/library/service/s;)V

    goto :goto_1

    .line 425
    :cond_3
    invoke-static {v1}, Lcom/twitter/android/UsernameEntryFragment;->a(Landroid/text/Editable;)I

    move-result v1

    .line 426
    iget-object v2, v0, Lcom/twitter/android/UsernameEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 427
    sget-object v2, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    .line 428
    sget-object v1, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    goto :goto_1

    .line 401
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
