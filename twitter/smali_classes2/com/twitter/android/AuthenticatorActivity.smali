.class public Lcom/twitter/android/AuthenticatorActivity;
.super Lcom/twitter/app/common/base/BaseAccountAuthenticatorActivity;
.source "Twttr"


# instance fields
.field a:Ljava/lang/Boolean;

.field private c:Landroid/accounts/AccountManager;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseAccountAuthenticatorActivity;-><init>()V

    return-void
.end method

.method private b(Lcom/twitter/model/account/OAuthToken;)V
    .locals 4

    .prologue
    .line 229
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->e:Ljava/lang/String;

    sget-object v2, Lcom/twitter/library/util/b;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->c:Landroid/accounts/AccountManager;

    .line 231
    const-string/jumbo v2, "com.twitter.android.oauth.token"

    iget-object v3, p1, Lcom/twitter/model/account/OAuthToken;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string/jumbo v2, "com.twitter.android.oauth.token.secret"

    iget-object v3, p1, Lcom/twitter/model/account/OAuthToken;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/model/account/OAuthToken;)V
    .locals 3

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/twitter/android/AuthenticatorActivity;->b(Lcom/twitter/model/account/OAuthToken;)V

    .line 203
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 204
    const-string/jumbo v1, "authAccount"

    iget-object v2, p0, Lcom/twitter/android/AuthenticatorActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    const-string/jumbo v1, "accountType"

    sget-object v2, Lcom/twitter/library/util/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->d:Ljava/lang/String;

    const-string/jumbo v2, "com.twitter.android.oauth.token"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 208
    const-string/jumbo v1, "authtoken"

    iget-object v2, p1, Lcom/twitter/model/account/OAuthToken;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthenticatorActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 214
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/AuthenticatorActivity;->setResult(ILandroid/content/Intent;)V

    .line 217
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    invoke-virtual {v0, p1}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/account/OAuthToken;)V

    .line 222
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/AuthenticatorActivity;->finish()V

    .line 223
    return-void

    .line 209
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->d:Ljava/lang/String;

    const-string/jumbo v2, "com.twitter.android.oauth.token.secret"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    const-string/jumbo v1, "authtoken"

    iget-object v2, p1, Lcom/twitter/model/account/OAuthToken;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method a(Lcom/twitter/model/account/OAuthToken;Z)V
    .locals 2

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/twitter/android/AuthenticatorActivity;->b(Lcom/twitter/model/account/OAuthToken;)V

    .line 186
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 187
    const-string/jumbo v1, "booleanResult"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 188
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthenticatorActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 189
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/AuthenticatorActivity;->setResult(ILandroid/content/Intent;)V

    .line 190
    invoke-virtual {p0}, Lcom/twitter/android/AuthenticatorActivity;->finish()V

    .line 191
    return-void
.end method

.method public onClickHandler(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/AuthenticatorActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    :goto_0
    return-void

    .line 123
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthenticatorActivity;->showDialog(I)V

    .line 125
    new-instance v1, Lcom/twitter/android/AuthenticatorActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/AuthenticatorActivity$1;-><init>(Lcom/twitter/android/AuthenticatorActivity;)V

    .line 152
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    .line 153
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 154
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v3

    new-instance v4, Lbat;

    iget-object v5, p0, Lcom/twitter/android/AuthenticatorActivity;->e:Ljava/lang/String;

    invoke-direct {v4, p0, v2, v5, v0}, Lbat;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v4, v1}, Lbat;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    .line 154
    invoke-virtual {v3, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 76
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseAccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0, v3}, Lcom/twitter/android/AuthenticatorActivity;->requestWindowFeature(I)Z

    .line 79
    const v0, 0x7f04018a

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthenticatorActivity;->setContentView(I)V

    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/AuthenticatorActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v3, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    .line 83
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AuthenticatorActivity;->c:Landroid/accounts/AccountManager;

    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/AuthenticatorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 86
    const-string/jumbo v1, "username"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->e:Ljava/lang/String;

    .line 87
    const-string/jumbo v1, "auth_token_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->d:Ljava/lang/String;

    .line 89
    const-string/jumbo v1, "confirm_credentials"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AuthenticatorActivity;->a:Ljava/lang/Boolean;

    .line 90
    iget-object v0, p0, Lcom/twitter/android/AuthenticatorActivity;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const v0, 0x7f0a008f

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 93
    invoke-virtual {p0}, Lcom/twitter/android/AuthenticatorActivity;->finish()V

    .line 101
    :goto_0
    return-void

    .line 97
    :cond_0
    const v0, 0x7f13046b

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthenticatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 98
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    const v0, 0x7f13046c

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthenticatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/AuthenticatorActivity;->f:Landroid/widget/EditText;

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 161
    packed-switch p1, :pswitch_data_0

    .line 171
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 163
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 164
    const v1, 0x7f0a0088

    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthenticatorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 165
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 166
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 106
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseAccountAuthenticatorActivity;->onStart()V

    .line 107
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login_dialog::::show"

    aput-object v3, v1, v2

    .line 108
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 107
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 109
    return-void
.end method
