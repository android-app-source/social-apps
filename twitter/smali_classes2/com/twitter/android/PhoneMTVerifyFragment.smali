.class public Lcom/twitter/android/PhoneMTVerifyFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/TextView;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Landroid/content/Context;

.field private g:Lcom/twitter/library/client/Session;

.field private h:Z

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->e:Z

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->h:Z

    .line 57
    new-instance v0, Lcom/twitter/android/PhoneMTVerifyFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/PhoneMTVerifyFragment$1;-><init>(Lcom/twitter/android/PhoneMTVerifyFragment;)V

    iput-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->i:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PhoneMTVerifyFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/PhoneMTVerifyFragment;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->e:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/PhoneMTVerifyFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->f:Landroid/content/Context;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->g:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lbci;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)Lbci;

    move-result-object v0

    .line 131
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/PhoneMTVerifyFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 132
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->c:Landroid/widget/TextView;

    const v1, 0x7f0a066f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 134
    :cond_0
    return-void
.end method

.method private f()V
    .locals 5

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    const-string/jumbo v1, "/"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->g:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->e:Z

    invoke-static {v1, v2, v3, v0, v4}, Lbcj;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Z)Lbcj;

    move-result-object v0

    .line 143
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/PhoneMTVerifyFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 144
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 76
    const v0, 0x7f04029b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 78
    invoke-virtual {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->f:Landroid/content/Context;

    .line 79
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->g:Lcom/twitter/library/client/Session;

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a0670

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 83
    invoke-virtual {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    .line 84
    const-string/jumbo v1, "verify_phone_number"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    .line 85
    const-string/jumbo v1, "is_verizon"

    invoke-virtual {v0, v1, v6}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->e:Z

    .line 87
    const v0, 0x7f130639

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->a:Landroid/widget/EditText;

    .line 88
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->a:Landroid/widget/EditText;

    new-instance v1, Lcom/twitter/android/PhoneMTVerifyFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/PhoneMTVerifyFragment$2;-><init>(Lcom/twitter/android/PhoneMTVerifyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 94
    const v0, 0x7f13063a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->b:Landroid/widget/Button;

    .line 95
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    const v0, 0x7f13063b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->c:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v0, 0x7f130638

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 100
    const v3, 0x7f0a066b

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string/jumbo v1, ""

    .line 101
    :goto_0
    aput-object v1, v4, v6

    .line 100
    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/PhoneMTVerifyFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 106
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v3, "phone_loggedin_mt"

    aput-object v3, v1, v6

    const-string/jumbo v3, "enter_code:::impression"

    aput-object v3, v1, v7

    .line 107
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 108
    return-object v2

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    .line 101
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 9
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x2

    const/4 v7, 0x1

    .line 151
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/abs/AbsFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 152
    if-ne p2, v7, :cond_4

    move-object v0, p1

    .line 153
    check-cast v0, Lbci;

    invoke-virtual {v0}, Lbci;->s()Lcbv;

    move-result-object v1

    move-object v0, p1

    .line 154
    check-cast v0, Lbci;

    invoke-virtual {v0}, Lbci;->e()[I

    move-result-object v2

    .line 155
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcbv;->a:Ljava/lang/String;

    .line 156
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    const/16 v0, 0x11d

    invoke-static {v2, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    const v0, 0x7f0a0664

    .line 164
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->f:Landroid/content/Context;

    invoke-static {v1, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->c:Landroid/widget/TextView;

    const v1, 0x7f0a066e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 195
    :cond_2
    :goto_1
    return-void

    .line 162
    :cond_3
    const v0, 0x7f0a0665

    goto :goto_0

    .line 169
    :cond_4
    if-ne p2, v1, :cond_2

    .line 170
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    .line 171
    if-eqz v3, :cond_6

    iget-wide v4, v3, Lcom/twitter/library/service/v;->c:J

    .line 172
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 173
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 174
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Lcom/twitter/library/util/v;

    move-result-object v0

    .line 175
    invoke-virtual {v0, v7, v7}, Lcom/twitter/library/util/v;->a(ZZ)V

    .line 176
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v6, "phone_loggedin_mt"

    aput-object v6, v1, v8

    const-string/jumbo v6, "enter_code:verify_code::success"

    aput-object v6, v1, v7

    .line 177
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 176
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 179
    if-eqz v3, :cond_5

    .line 180
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->S:Lcom/twitter/library/client/p;

    new-instance v1, Lbio;

    iget-object v6, v3, Lcom/twitter/library/service/v;->e:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lbio;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 184
    :cond_5
    instance-of v0, v2, Lcom/twitter/android/bb;

    if-eqz v0, :cond_2

    .line 185
    check-cast v2, Lcom/twitter/android/PhoneMTFlowActivity;

    invoke-virtual {v2}, Lcom/twitter/android/PhoneMTFlowActivity;->a()V

    goto :goto_1

    .line 171
    :cond_6
    const-wide/16 v4, 0x0

    goto :goto_2

    .line 188
    :cond_7
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "phone_loggedin_mt"

    aput-object v2, v1, v8

    const-string/jumbo v2, "enter_code:verify_code::failure"

    aput-object v2, v1, v7

    .line 189
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 188
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 190
    iget-object v0, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->f:Landroid/content/Context;

    const v1, 0x7f0a066d

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1
.end method

.method d()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 199
    iget-boolean v2, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->h:Z

    if-eqz v2, :cond_0

    .line 200
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "phone_loggedin_mt"

    aput-object v4, v3, v1

    const-string/jumbo v4, "enter_code::code:input"

    aput-object v4, v3, v0

    .line 201
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 200
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 202
    iput-boolean v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->h:Z

    .line 204
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->b:Landroid/widget/Button;

    iget-object v3, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 205
    return-void

    :cond_1
    move v0, v1

    .line 204
    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 115
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f13063a

    if-ne v0, v1, :cond_1

    .line 116
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "phone_loggedin_mt"

    aput-object v2, v1, v4

    const-string/jumbo v2, "enter_code::continue:click"

    aput-object v2, v1, v5

    .line 117
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 116
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 118
    invoke-virtual {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->a:Landroid/widget/EditText;

    invoke-static {v0, v1, v4}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 119
    invoke-direct {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->f()V

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f13063b

    if-ne v0, v1, :cond_0

    .line 121
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/PhoneMTVerifyFragment;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "phone_loggedin_mt"

    aput-object v2, v1, v4

    const-string/jumbo v2, "enter_code::resend:click"

    aput-object v2, v1, v5

    .line 122
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 123
    invoke-direct {p0}, Lcom/twitter/android/PhoneMTVerifyFragment;->e()V

    goto :goto_0
.end method
