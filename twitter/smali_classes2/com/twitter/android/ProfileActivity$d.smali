.class Lcom/twitter/android/ProfileActivity$d;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ProfileActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/ProfileActivity;)V
    .locals 0

    .prologue
    .line 2765
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity$d;->a:Lcom/twitter/android/ProfileActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/ProfileActivity$1;)V
    .locals 0

    .prologue
    .line 2765
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileActivity$d;-><init>(Lcom/twitter/android/ProfileActivity;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 2765
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileActivity$d;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 2768
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$d;->a:Lcom/twitter/android/ProfileActivity;

    invoke-static {v0}, Lcom/twitter/android/ProfileActivity;->d(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 2769
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 2770
    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/v;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_2

    instance-of v0, p1, Lcom/twitter/library/api/upload/k;

    if-eqz v0, :cond_2

    .line 2771
    check-cast p1, Lcom/twitter/library/api/upload/k;

    .line 2772
    iget-object v0, p1, Lcom/twitter/library/api/upload/k;->o:Landroid/os/Bundle;

    .line 2773
    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 2775
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity$d;->a:Lcom/twitter/android/ProfileActivity;

    iget-boolean v1, v1, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity$d;->a:Lcom/twitter/android/ProfileActivity;

    iget-wide v4, v1, Lcom/twitter/android/ProfileActivity;->A:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2776
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/k;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/k;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2777
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/k;->f()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/k;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2780
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity$d;->a:Lcom/twitter/android/ProfileActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 2784
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/k;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2785
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$d;->a:Lcom/twitter/android/ProfileActivity;

    invoke-static {v0}, Lcom/twitter/android/ProfileActivity;->e(Lcom/twitter/android/ProfileActivity;)V

    .line 2788
    :cond_2
    return-void
.end method
