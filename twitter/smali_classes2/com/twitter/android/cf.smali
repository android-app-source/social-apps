.class public Lcom/twitter/android/cf;
.super Lcom/twitter/android/y;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lcom/twitter/android/widget/ad;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/cf$d;,
        Lcom/twitter/android/cf$c;,
        Lcom/twitter/android/cf$e;,
        Lcom/twitter/android/cf$k;,
        Lcom/twitter/android/cf$i;,
        Lcom/twitter/android/cf$j;,
        Lcom/twitter/android/cf$h;,
        Lcom/twitter/android/cf$l;,
        Lcom/twitter/android/cf$a;,
        Lcom/twitter/android/cf$b;,
        Lcom/twitter/android/cf$f;,
        Lcom/twitter/android/cf$g;
    }
.end annotation


# instance fields
.field private A:Z

.field private final B:Z

.field private C:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final D:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final E:Lcom/twitter/android/timeline/av;

.field private final F:Lcom/twitter/android/timeline/cs;

.field private final G:Lcom/twitter/android/timeline/cu;

.field private final H:Lcom/twitter/android/timeline/cn;

.field private final I:Lcom/twitter/android/timeline/au;

.field private final J:Lcom/twitter/android/timeline/bv;

.field private final K:Lcom/twitter/android/revenue/c;

.field private final L:Lcom/twitter/android/revenue/e;

.field private final M:Lcom/twitter/android/timeline/b;

.field private final N:Z

.field private final O:Z

.field private final P:Z

.field private final Q:Lcom/twitter/android/ap;

.field private final R:Lcom/twitter/app/users/d;

.field private final S:Ladj;

.field private final T:Lcom/twitter/android/timeline/ch;

.field private final U:Lcom/twitter/android/timeline/t;

.field private final V:Lcom/twitter/android/timeline/ag;

.field private final W:Lckd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lckd",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation
.end field

.field private final i:I

.field private final j:Lcom/twitter/app/users/c;

.field private final k:Lcom/twitter/android/ch;

.field private final l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/twitter/android/av/t;

.field private final n:Z

.field private final o:Z

.field private p:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcgi;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/twitter/android/timeline/bn;

.field private final t:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/az;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Landroid/view/LayoutInflater;

.field private final v:Lcom/twitter/android/trends/g;

.field private final w:I

.field private final x:I

.field private final y:Lcom/twitter/android/timeline/w;

.field private z:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;IZLcom/twitter/library/view/d;Lcom/twitter/android/timeline/bn;Lcom/twitter/app/users/c;Lcom/twitter/android/ch;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLcom/twitter/android/timeline/cs;Lcom/twitter/android/timeline/cu;Lcom/twitter/android/timeline/av;Lcom/twitter/android/timeline/w;Lcom/twitter/android/ck;Lcom/twitter/ui/view/h;Lcom/twitter/android/av;Lcom/twitter/android/av;Lcom/twitter/android/av;Lcom/twitter/android/revenue/c;Lcom/twitter/android/revenue/e;Lcom/twitter/android/av/t;Lcom/twitter/android/ap;Lcom/twitter/app/users/d;Ladj;Lcom/twitter/android/timeline/ch;Lcom/twitter/android/timeline/t;Lcom/twitter/android/timeline/ag;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
            "IZ",
            "Lcom/twitter/library/view/d;",
            "Lcom/twitter/android/timeline/bn;",
            "Lcom/twitter/app/users/c;",
            "Lcom/twitter/android/ch;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Z",
            "Lcom/twitter/android/timeline/cs;",
            "Lcom/twitter/android/timeline/cu;",
            "Lcom/twitter/android/timeline/av;",
            "Lcom/twitter/android/timeline/w;",
            "Lcom/twitter/android/ck;",
            "Lcom/twitter/ui/view/h;",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/bk;",
            ">;",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/bk;",
            ">;",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/az;",
            ">;",
            "Lcom/twitter/android/revenue/c;",
            "Lcom/twitter/android/revenue/e;",
            "Lcom/twitter/android/av/t;",
            "Lcom/twitter/android/ap;",
            "Lcom/twitter/app/users/d;",
            "Ladj;",
            "Lcom/twitter/android/timeline/ch;",
            "Lcom/twitter/android/timeline/t;",
            "Lcom/twitter/android/timeline/ag;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p8

    move v6, p2

    move-object/from16 v7, p9

    move-object/from16 v8, p16

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/y;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/ui/view/h;)V

    .line 211
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/cf;->l:Ljava/util/HashMap;

    .line 288
    iput p2, p0, Lcom/twitter/android/cf;->i:I

    .line 289
    iput-object p6, p0, Lcom/twitter/android/cf;->j:Lcom/twitter/app/users/c;

    .line 290
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/twitter/android/cf;->k:Lcom/twitter/android/ch;

    .line 291
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    .line 292
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/cf;->x:I

    .line 293
    invoke-direct {p0}, Lcom/twitter/android/cf;->h()V

    .line 294
    new-instance v1, Lcom/twitter/android/trends/g;

    invoke-virtual {p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/android/trends/g;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    .line 295
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/twitter/android/cf;->B:Z

    .line 296
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/cf;->E:Lcom/twitter/android/timeline/av;

    .line 297
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/twitter/android/cf;->F:Lcom/twitter/android/timeline/cs;

    .line 298
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/cf;->G:Lcom/twitter/android/timeline/cu;

    .line 299
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/cf;->y:Lcom/twitter/android/timeline/w;

    .line 300
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/twitter/android/cf;->D:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 301
    const v1, 0x7f020840

    iput v1, p0, Lcom/twitter/android/cf;->w:I

    .line 302
    new-instance v1, Lcom/twitter/android/timeline/cn;

    const v2, 0x7f040264

    iget-object v3, p0, Lcom/twitter/android/cf;->j:Lcom/twitter/app/users/c;

    move-object/from16 v0, p8

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/android/timeline/cn;-><init>(ILcom/twitter/model/util/FriendshipCache;Lcom/twitter/app/users/c;)V

    iput-object v1, p0, Lcom/twitter/android/cf;->H:Lcom/twitter/android/timeline/cn;

    .line 304
    new-instance v1, Lcom/twitter/android/timeline/bv;

    iget-object v2, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    iget-object v3, p0, Lcom/twitter/android/cf;->d:Lcom/twitter/library/view/d;

    .line 305
    invoke-static {}, Lcom/twitter/android/revenue/k;->f()I

    move-result v6

    move-object/from16 v4, p15

    move-object/from16 v5, p8

    move-object/from16 v7, p16

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/timeline/bv;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/library/view/d;Lcom/twitter/android/ck;Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/ui/view/h;)V

    iput-object v1, p0, Lcom/twitter/android/cf;->J:Lcom/twitter/android/timeline/bv;

    .line 307
    iget-object v1, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/cf;->D:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p0, Lcom/twitter/android/cf;->c:Lcom/twitter/library/client/v;

    iget-object v4, p0, Lcom/twitter/android/cf;->E:Lcom/twitter/android/timeline/av;

    .line 308
    invoke-static {v1, v2, v3, v4}, Lcom/twitter/android/timeline/au;->a(Landroid/app/Activity;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/client/v;Lcom/twitter/android/timeline/av;)Lcom/twitter/android/timeline/au;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/cf;->I:Lcom/twitter/android/timeline/au;

    .line 310
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/android/cf;->q:Lcom/twitter/android/av;

    .line 311
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/android/cf;->r:Lcom/twitter/android/av;

    .line 312
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/twitter/android/cf;->t:Lcom/twitter/android/av;

    .line 313
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/twitter/android/cf;->K:Lcom/twitter/android/revenue/c;

    .line 314
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/twitter/android/cf;->L:Lcom/twitter/android/revenue/e;

    .line 315
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/twitter/android/cf;->m:Lcom/twitter/android/av/t;

    .line 316
    new-instance v1, Lcom/twitter/android/timeline/b;

    invoke-direct {v1}, Lcom/twitter/android/timeline/b;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/cf;->M:Lcom/twitter/android/timeline/b;

    .line 317
    iput-object p5, p0, Lcom/twitter/android/cf;->s:Lcom/twitter/android/timeline/bn;

    .line 318
    const-string/jumbo v1, "ad_formats_ad_slots_android_4189"

    const-string/jumbo v2, "ad_slots"

    invoke-static {v1, v2}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/cf;->N:Z

    .line 320
    const-string/jumbo v1, "ad_formats_no_screen_name_ads_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/cf;->O:Z

    .line 321
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/twitter/android/cf;->Q:Lcom/twitter/android/ap;

    .line 322
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/twitter/android/cf;->R:Lcom/twitter/app/users/d;

    .line 323
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/twitter/android/cf;->S:Ladj;

    .line 324
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/twitter/android/cf;->T:Lcom/twitter/android/timeline/ch;

    .line 325
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/twitter/android/cf;->U:Lcom/twitter/android/timeline/t;

    .line 326
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/twitter/android/cf;->V:Lcom/twitter/android/timeline/ag;

    .line 327
    const-string/jumbo v1, "timeline_header_avatars_in_wtf_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/cf;->n:Z

    .line 329
    const-string/jumbo v1, "timelines_headers_redesign_5491"

    invoke-static {v1}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/cf;->o:Z

    .line 330
    const-string/jumbo v1, "timeline_live_banner_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/cf;->P:Z

    .line 332
    new-instance v2, Lcjz;

    .line 333
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v3, Lcom/twitter/android/timeline/cg;

    .line 334
    move-object/from16 v0, p26

    invoke-virtual {v1, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v3, Lcom/twitter/android/timeline/ac;

    iget-object v4, p0, Lcom/twitter/android/cf;->S:Ladj;

    .line 335
    invoke-virtual {v1, v3, v4}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v3, Lcom/twitter/android/timeline/co;

    iget-object v4, p0, Lcom/twitter/android/cf;->R:Lcom/twitter/app/users/d;

    .line 336
    invoke-virtual {v1, v3, v4}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 337
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v2, v1}, Lcjz;-><init>(Ljava/util/Map;)V

    iput-object v2, p0, Lcom/twitter/android/cf;->W:Lckd;

    .line 338
    return-void
.end method

.method private a(Lcom/twitter/android/timeline/bk;Lcom/twitter/android/timeline/bg;)I
    .locals 3

    .prologue
    const/4 v0, 0x7

    const/4 v2, 0x1

    .line 1973
    instance-of v1, p1, Lcom/twitter/android/timeline/cq;

    if-eqz v1, :cond_0

    .line 1974
    check-cast p1, Lcom/twitter/android/timeline/cq;

    .line 1975
    iget-object v1, p1, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget v1, v1, Lcom/twitter/model/timeline/at$a;->b:I

    packed-switch v1, :pswitch_data_0

    .line 1990
    :cond_0
    :goto_0
    return v0

    .line 1977
    :pswitch_0
    const/16 v1, 0x14

    invoke-static {v2, p2, v0, v1}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v0

    goto :goto_0

    .line 1981
    :pswitch_1
    const/16 v0, 0x17

    const/16 v1, 0x15

    invoke-static {v2, p2, v0, v1}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v0

    goto :goto_0

    .line 1975
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(ZLcom/twitter/android/timeline/bg;II)I
    .locals 3

    .prologue
    const/4 v0, 0x7

    .line 527
    if-eqz p0, :cond_2

    .line 528
    iget v1, p1, Lcom/twitter/android/timeline/bg;->q:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 536
    :goto_0
    return p2

    .line 530
    :cond_0
    iget v1, p1, Lcom/twitter/android/timeline/bg;->q:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    move p2, v0

    .line 531
    goto :goto_0

    :cond_1
    move p2, p3

    .line 533
    goto :goto_0

    :cond_2
    move p2, v0

    .line 536
    goto :goto_0
.end method

.method private a(Lcom/twitter/model/topic/trends/a;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1564
    if-eqz p1, :cond_1

    .line 1565
    iget-object v0, p1, Lcom/twitter/model/topic/trends/a;->d:Lcom/twitter/model/topic/trends/a$b;

    .line 1566
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/topic/trends/a$b;->a()I

    move-result v0

    .line 1568
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/cf;->z:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 1570
    :goto_1
    return-object v0

    .line 1566
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1570
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 708
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040136

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/CarouselRowView;

    .line 710
    new-instance v1, Lcom/twitter/android/cf$a;

    new-instance v2, Lcom/twitter/android/widget/d;

    .line 711
    invoke-virtual {p0}, Lcom/twitter/android/cf;->j()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/cf;->J:Lcom/twitter/android/timeline/bv;

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/widget/d;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/e;)V

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/cf$a;-><init>(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/d;)V

    .line 712
    new-instance v2, Lcom/twitter/android/cf$g$a;

    invoke-direct {v2}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 713
    invoke-virtual {v2, v1}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$a;)Lcom/twitter/android/cf$g$a;

    move-result-object v2

    .line 714
    invoke-virtual {v2}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v2

    .line 715
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/CarouselRowView;->setTag(Ljava/lang/Object;)V

    .line 717
    new-instance v2, Lcom/twitter/android/widget/f;

    iget-object v3, v1, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    iget v4, p0, Lcom/twitter/android/cf;->x:I

    new-instance v5, Lcom/twitter/android/timeline/bu;

    invoke-direct {v5}, Lcom/twitter/android/timeline/bu;-><init>()V

    invoke-direct {v2, v3, p1, v4, v5}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/CarouselRowView;Landroid/view/ViewParent;ILcom/twitter/android/widget/f$a;)V

    .line 723
    iget-object v3, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v3

    invoke-virtual {v3}, Lcmt;->c()Lcmr;

    move-result-object v3

    instance-of v3, v3, Lcom/twitter/library/client/navigation/g;

    if-eqz v3, :cond_0

    .line 724
    new-instance v3, Lcom/twitter/android/widget/z;

    iget-object v4, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 725
    invoke-virtual {v4}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lcom/twitter/android/widget/z;-><init>(Landroid/support/v4/view/ViewPager$OnPageChangeListener;Lcmt;)V

    .line 727
    iget-object v1, v1, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    invoke-virtual {v1, v3}, Lcom/twitter/android/widget/CarouselRowView;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 732
    :goto_0
    return-object v0

    .line 729
    :cond_0
    iget-object v1, v1, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/CarouselRowView;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;Lcom/twitter/android/timeline/au;)Landroid/view/View;
    .locals 6

    .prologue
    .line 790
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040129

    const/4 v2, 0x0

    .line 791
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/AutoPlayableCarouselRowView;

    .line 792
    iget-object v1, p0, Lcom/twitter/android/cf;->m:Lcom/twitter/android/av/t;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AutoPlayableCarouselRowView;->setAutoPlayableItemPositionListener(Lcom/twitter/android/av/t;)V

    .line 794
    new-instance v1, Lcom/twitter/android/widget/as;

    .line 795
    invoke-virtual {p0}, Lcom/twitter/android/cf;->j()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/twitter/android/widget/as;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/e;)V

    .line 796
    invoke-virtual {p0}, Lcom/twitter/android/cf;->j()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/android/widget/at;->a(Lcom/twitter/android/widget/d;Landroid/content/Context;)V

    .line 797
    new-instance v2, Lcom/twitter/android/cf$a;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/cf$a;-><init>(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/d;)V

    .line 798
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 799
    invoke-virtual {v1, v2}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$a;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 800
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 801
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AutoPlayableCarouselRowView;->setTag(Ljava/lang/Object;)V

    .line 803
    iget-object v1, p0, Lcom/twitter/android/cf;->E:Lcom/twitter/android/timeline/av;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/android/timeline/as;

    iget-object v3, p0, Lcom/twitter/android/cf;->E:Lcom/twitter/android/timeline/av;

    invoke-direct {v1, v3}, Lcom/twitter/android/timeline/as;-><init>(Lcom/twitter/android/timeline/av;)V

    .line 806
    :goto_0
    new-instance v3, Lcom/twitter/android/widget/f;

    iget-object v4, v2, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    iget v5, p0, Lcom/twitter/android/cf;->x:I

    invoke-direct {v3, v4, p1, v5, v1}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/CarouselRowView;Landroid/view/ViewParent;ILcom/twitter/android/widget/f$a;)V

    .line 808
    iget-object v1, v2, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    invoke-virtual {v1, v3}, Lcom/twitter/android/widget/CarouselRowView;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 809
    iget-object v1, v2, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    iget-object v2, p0, Lcom/twitter/android/cf;->s:Lcom/twitter/android/timeline/bn;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/CarouselRowView;->setDismissClickListener(Landroid/view/View$OnClickListener;)V

    .line 810
    return-object v0

    .line 803
    :cond_0
    sget-object v1, Lcom/twitter/android/widget/f$a;->c:Lcom/twitter/android/widget/f$a;

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;Lcom/twitter/android/timeline/cn;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v4, 0x1

    .line 750
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040138

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/CarouselRowView;

    .line 757
    new-instance v1, Lcom/twitter/android/widget/d;

    .line 758
    invoke-virtual {p0}, Lcom/twitter/android/cf;->j()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/twitter/android/widget/d;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/e;)V

    .line 759
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f100001

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/d;->a(F)V

    .line 760
    new-instance v2, Lcom/twitter/android/cf$a;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/cf$a;-><init>(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/d;)V

    .line 761
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 762
    invoke-virtual {v1, v2}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$a;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 763
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 764
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/CarouselRowView;->setTag(Ljava/lang/Object;)V

    .line 766
    new-instance v1, Lcom/twitter/android/widget/f;

    iget-object v3, v2, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    iget v4, p0, Lcom/twitter/android/cf;->x:I

    new-instance v5, Lcom/twitter/android/timeline/cm;

    iget-object v6, p0, Lcom/twitter/android/cf;->G:Lcom/twitter/android/timeline/cu;

    iget-object v7, p0, Lcom/twitter/android/cf;->D:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v8, p0, Lcom/twitter/android/cf;->c:Lcom/twitter/library/client/v;

    .line 769
    invoke-virtual {v8}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/twitter/android/timeline/cm;-><init>(Lcom/twitter/android/timeline/cu;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;J)V

    invoke-direct {v1, v3, p1, v4, v5}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/CarouselRowView;Landroid/view/ViewParent;ILcom/twitter/android/widget/f$a;)V

    .line 773
    iget-object v3, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v3

    invoke-virtual {v3}, Lcmt;->c()Lcmr;

    move-result-object v3

    instance-of v3, v3, Lcom/twitter/library/client/navigation/g;

    if-eqz v3, :cond_0

    .line 774
    new-instance v3, Lcom/twitter/android/widget/z;

    iget-object v4, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 775
    invoke-virtual {v4}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/twitter/android/widget/z;-><init>(Landroid/support/v4/view/ViewPager$OnPageChangeListener;Lcmt;)V

    .line 777
    iget-object v1, v2, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    invoke-virtual {v1, v3}, Lcom/twitter/android/widget/CarouselRowView;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 782
    :goto_0
    iget-object v1, v2, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    iget-object v2, p0, Lcom/twitter/android/cf;->s:Lcom/twitter/android/timeline/bn;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/CarouselRowView;->setDismissClickListener(Landroid/view/View$OnClickListener;)V

    .line 784
    return-object v0

    .line 779
    :cond_0
    iget-object v3, v2, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    invoke-virtual {v3, v1}, Lcom/twitter/android/widget/CarouselRowView;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/v;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1581
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1582
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {v0, v1}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    .line 1584
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ILandroid/view/View;Lcgi;Ljava/lang/String;ZIZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/twitter/android/cf;->p:Lcom/twitter/android/av;

    if-eqz v0, :cond_0

    .line 1543
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1544
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1545
    const-string/jumbo v1, "entity_id"

    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1547
    const-string/jumbo v1, "isread"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1548
    const-string/jumbo v1, "changes"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1549
    const-string/jumbo v1, "description"

    .line 1550
    invoke-static {p7}, Lcom/twitter/model/topic/e;->a(Z)Ljava/lang/String;

    move-result-object v2

    .line 1549
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    const-string/jumbo v1, "trend_id"

    invoke-virtual {v0, v1, p8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    iget-object v1, p0, Lcom/twitter/android/cf;->p:Lcom/twitter/android/av;

    invoke-interface {v1, p2, p3, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 1554
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1528
    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1529
    invoke-static {v0}, Lcom/twitter/library/view/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1530
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1531
    new-instance v2, Lcom/twitter/library/view/b;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v2, v0, v3}, Lcom/twitter/library/view/b;-><init>(Ljava/lang/String;I)V

    .line 1532
    invoke-static {p1, v1, v2, p3, v4}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Landroid/view/View;Z)I

    .line 1533
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1537
    :goto_0
    return-void

    .line 1535
    :cond_0
    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/content/res/Resources;Landroid/view/View;Lcom/twitter/android/timeline/ax;Landroid/os/Bundle;I)V
    .locals 8

    .prologue
    .line 1872
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/cf$g;

    .line 1873
    iget-object v5, p3, Lcom/twitter/android/timeline/ax;->a:Lcbi;

    .line 1874
    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v1, v0, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    .line 1875
    const v0, 0x7f13007e

    invoke-virtual {v1, v0, p3}, Lcom/twitter/android/widget/CarouselRowView;->a(ILcom/twitter/android/timeline/bk;)V

    .line 1876
    invoke-static {p3}, Lcom/twitter/android/cf;->b(Lcom/twitter/android/timeline/bk;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/CarouselRowView;->setShowDismiss(Z)V

    .line 1877
    iget-object v0, p3, Lcom/twitter/android/timeline/ax;->b:Lcom/twitter/model/moments/v$b;

    iget-object v0, v0, Lcom/twitter/model/moments/v$b;->b:Ljava/lang/String;

    const v3, 0x7f0a093f

    .line 1880
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1878
    invoke-static {v0, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1877
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/CarouselRowView;->setTitleText(Ljava/lang/String;)V

    .line 1881
    iget-object v3, p3, Lcom/twitter/android/timeline/ax;->e:Lcom/twitter/model/timeline/r;

    invoke-virtual {p3}, Lcom/twitter/android/timeline/ax;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v4

    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v7, v0, Lcom/twitter/android/cf$a;->b:Lcom/twitter/android/widget/d;

    move-object v0, p0

    move-object v1, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/cf$g;Lcom/twitter/model/timeline/r;Lcom/twitter/android/timeline/bg;Lcbi;Landroid/os/Bundle;Lcom/twitter/android/widget/d;)V

    .line 1886
    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v0, v0, Lcom/twitter/android/cf$a;->b:Lcom/twitter/android/widget/d;

    invoke-virtual {v0}, Lcom/twitter/android/widget/d;->notifyDataSetChanged()V

    .line 1888
    iget-object v0, p0, Lcom/twitter/android/cf;->E:Lcom/twitter/android/timeline/av;

    if-eqz v0, :cond_0

    .line 1889
    iget-object v0, p0, Lcom/twitter/android/cf;->E:Lcom/twitter/android/timeline/av;

    .line 1890
    invoke-virtual {v0, p3, p5}, Lcom/twitter/android/timeline/av;->a(Lcom/twitter/android/timeline/ax;I)V

    .line 1892
    :cond_0
    return-void
.end method

.method static a(Landroid/view/View;Lcom/twitter/android/ap;Lcom/twitter/android/timeline/bk;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1619
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1620
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1621
    const v0, 0x7f13007e

    invoke-virtual {p0, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1622
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/cf$g;Lcom/twitter/model/timeline/r;Lcom/twitter/android/timeline/bg;Lcbi;Landroid/os/Bundle;Lcom/twitter/android/widget/d;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1786
    iget-object v3, p2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    .line 1787
    iget v4, v3, Lcom/twitter/android/cf$a;->e:I

    .line 1788
    iget-object v0, p4, Lcom/twitter/android/timeline/bg;->a:Ljava/lang/String;

    iget-object v5, v3, Lcom/twitter/android/cf$a;->c:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1789
    :goto_0
    iget v5, p4, Lcom/twitter/android/timeline/bg;->o:I

    if-eq v4, v5, :cond_2

    .line 1791
    :goto_1
    iget-object v4, v3, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    .line 1792
    if-nez v0, :cond_0

    if-eqz v1, :cond_4

    .line 1793
    :cond_0
    iget-object v1, p4, Lcom/twitter/android/timeline/bg;->a:Ljava/lang/String;

    iput-object v1, v3, Lcom/twitter/android/cf$a;->c:Ljava/lang/String;

    .line 1794
    iget v1, p4, Lcom/twitter/android/timeline/bg;->o:I

    iput v1, v3, Lcom/twitter/android/cf$a;->e:I

    .line 1797
    if-eqz v0, :cond_3

    .line 1802
    :goto_2
    invoke-virtual {p7, p5}, Lcom/twitter/android/widget/d;->a(Lcbi;)V

    .line 1803
    invoke-virtual {v4, p7}, Lcom/twitter/android/widget/CarouselRowView;->setCarouselAdapter(Lcom/twitter/android/widget/d;)V

    .line 1804
    invoke-virtual {v4, v2}, Lcom/twitter/android/widget/CarouselRowView;->setCurrentItemIndex(I)V

    .line 1813
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 1788
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1789
    goto :goto_1

    .line 1800
    :cond_3
    invoke-virtual {v4}, Lcom/twitter/android/widget/CarouselRowView;->getCurrentItemIndex()I

    move-result v2

    goto :goto_2

    .line 1807
    :cond_4
    invoke-virtual {v4}, Lcom/twitter/android/widget/CarouselRowView;->getCurrentItemIndex()I

    move-result v0

    .line 1808
    invoke-virtual {v4}, Lcom/twitter/android/widget/CarouselRowView;->getCarouselAdapter()Lcom/twitter/android/widget/d;

    move-result-object v1

    .line 1809
    invoke-virtual {v1}, Lcom/twitter/android/widget/d;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_5

    move v2, v0

    .line 1810
    :cond_5
    invoke-virtual {v1, p5}, Lcom/twitter/android/widget/d;->a(Lcbi;)V

    .line 1811
    invoke-virtual {v4, v2}, Lcom/twitter/android/widget/CarouselRowView;->setCurrentItemIndex(I)V

    goto :goto_3
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/bb;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1821
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/cf$g;

    .line 1822
    iget-object v3, p2, Lcom/twitter/android/timeline/bb;->e:Lcom/twitter/model/timeline/r;

    .line 1823
    invoke-virtual {p2}, Lcom/twitter/android/timeline/bb;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v4

    iget-object v5, p2, Lcom/twitter/android/timeline/bb;->a:Lcbi;

    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v7, v0, Lcom/twitter/android/cf$a;->b:Lcom/twitter/android/widget/d;

    move-object v0, p0

    move-object v1, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/cf$g;Lcom/twitter/model/timeline/r;Lcom/twitter/android/timeline/bg;Lcbi;Landroid/os/Bundle;Lcom/twitter/android/widget/d;)V

    .line 1825
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/bk;I)V
    .locals 2

    .prologue
    .line 1520
    iget-object v1, p0, Lcom/twitter/android/cf;->U:Lcom/twitter/android/timeline/t;

    move-object v0, p2

    check-cast v0, Lcom/twitter/android/timeline/az;

    invoke-static {p1, v1, v0, p3}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    .line 1522
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1523
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1524
    iget-object v1, p0, Lcom/twitter/android/cf;->t:Lcom/twitter/android/av;

    check-cast p2, Lcom/twitter/android/timeline/az;

    invoke-interface {v1, p1, p2, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 1525
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/bk;Ljava/lang/String;Lcom/twitter/model/timeline/j;I)V
    .locals 8
    .param p5    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1693
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cf$g;

    .line 1694
    iget-object v1, v0, Lcom/twitter/android/cf$g;->i:Lcom/twitter/android/z;

    .line 1695
    iget-object v0, v1, Lcom/twitter/android/z;->c:Landroid/widget/TextView;

    .line 1696
    iget-object v2, v1, Lcom/twitter/android/z;->b:Landroid/support/v7/widget/RecyclerView;

    .line 1698
    invoke-virtual {p0}, Lcom/twitter/android/cf;->j()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1699
    iget-boolean v4, p0, Lcom/twitter/android/cf;->o:Z

    if-eqz v4, :cond_2

    .line 1700
    const/4 v4, 0x0

    sget v5, Lcni;->a:F

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1701
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 1702
    const v4, 0x7f0e051a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 1703
    const v4, 0x7f0e0050

    .line 1704
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0e051b

    .line 1705
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const v6, 0x7f0e0050

    .line 1706
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v7, 0x7f0e051b

    .line 1707
    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1703
    invoke-virtual {v0, v4, v5, v6, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1709
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 1710
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLetterSpacing(F)V

    .line 1729
    :cond_0
    :goto_0
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1730
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1735
    :goto_1
    if-eqz v2, :cond_1

    .line 1736
    iget-boolean v0, p0, Lcom/twitter/android/cf;->n:Z

    if-eqz v0, :cond_4

    if-eqz p4, :cond_4

    iget-object v0, p4, Lcom/twitter/model/timeline/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1737
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1738
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/o;

    .line 1739
    invoke-static {p4}, Lcom/twitter/model/timeline/j$c;->a(Lcom/twitter/model/timeline/j;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/people/adapters/viewbinders/o;->a(Ljava/util/List;)V

    .line 1740
    invoke-virtual {v0}, Lcom/twitter/android/people/adapters/viewbinders/o;->notifyDataSetChanged()V

    .line 1746
    :cond_1
    :goto_2
    iget-object v0, v1, Lcom/twitter/android/z;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/cf;->Q:Lcom/twitter/android/ap;

    invoke-static {v0, v1, p2}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/ap;Lcom/twitter/android/timeline/bk;)V

    .line 1747
    return-void

    .line 1713
    :cond_2
    const/4 v4, 0x0

    const v5, 0x7f0e004a

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1714
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 1715
    const v4, 0x7f0e02ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 1716
    const v4, 0x7f0e0050

    .line 1717
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0e0051

    .line 1718
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const v6, 0x7f0e0050

    .line 1719
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v7, 0x7f0e0051

    .line 1720
    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 1716
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1722
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 1723
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 1724
    const v5, 0x7f0e04fc

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v4, v6}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 1725
    invoke-virtual {v4}, Landroid/util/TypedValue;->getFloat()F

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLetterSpacing(F)V

    goto/16 :goto_0

    .line 1732
    :cond_3
    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 1742
    :cond_4
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_2
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/bq;I)V
    .locals 3

    .prologue
    .line 1755
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cf$g;

    .line 1756
    iget-object v0, v0, Lcom/twitter/android/cf$g;->m:Lcom/twitter/android/cf$f;

    .line 1757
    iget-object v1, v0, Lcom/twitter/android/cf$f;->a:Lcom/twitter/library/widget/TimelineMessageView;

    iget-object v2, p2, Lcom/twitter/android/timeline/bq;->a:Lcom/twitter/model/timeline/ab;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TimelineMessageView;->a(Lcom/twitter/model/timeline/ab;)V

    .line 1758
    iget-object v0, v0, Lcom/twitter/android/cf$f;->a:Lcom/twitter/library/widget/TimelineMessageView;

    const v1, 0x7f13007e

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/widget/TimelineMessageView;->setTag(ILjava/lang/Object;)V

    .line 1759
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1760
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1761
    iget-object v1, p0, Lcom/twitter/android/cf;->r:Lcom/twitter/android/av;

    invoke-interface {v1, p1, p2, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 1762
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/cf;)V
    .locals 4

    .prologue
    .line 1750
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GapView;

    .line 1751
    invoke-virtual {p0}, Lcom/twitter/android/cf;->d()Ljava/util/List;

    move-result-object v1

    iget-wide v2, p2, Lcom/twitter/android/timeline/cf;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GapView;->setSpinnerActive(Z)V

    .line 1752
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/cq;Landroid/os/Bundle;I)V
    .locals 8

    .prologue
    .line 1833
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/cf$g;

    .line 1834
    iget-object v0, p2, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v3, v0, Lcom/twitter/model/timeline/at$a;->e:Lcom/twitter/model/timeline/r;

    .line 1835
    iget-object v5, p2, Lcom/twitter/android/timeline/cq;->a:Lcbi;

    .line 1836
    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v0, v0, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    .line 1837
    const v1, 0x7f13007e

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/widget/CarouselRowView;->a(ILcom/twitter/android/timeline/bk;)V

    .line 1838
    invoke-static {p2}, Lcom/twitter/android/cf;->b(Lcom/twitter/android/timeline/bk;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/CarouselRowView;->setShowDismiss(Z)V

    .line 1839
    iget-object v1, p2, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v1, v1, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1840
    iget-object v1, p2, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v1, v1, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/CarouselRowView;->setTitleText(Ljava/lang/String;)V

    .line 1842
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/android/timeline/cq;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v4

    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v7, v0, Lcom/twitter/android/cf$a;->b:Lcom/twitter/android/widget/d;

    move-object v0, p0

    move-object v1, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/cf$g;Lcom/twitter/model/timeline/r;Lcom/twitter/android/timeline/bg;Lcbi;Landroid/os/Bundle;Lcom/twitter/android/widget/d;)V

    .line 1844
    iget-object v0, p0, Lcom/twitter/android/cf;->F:Lcom/twitter/android/timeline/cs;

    if-eqz v0, :cond_1

    .line 1845
    iget-object v0, p0, Lcom/twitter/android/cf;->F:Lcom/twitter/android/timeline/cs;

    invoke-virtual {v0, p2, p4}, Lcom/twitter/android/timeline/cs;->a(Lcom/twitter/android/timeline/cq;I)V

    .line 1847
    :cond_1
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/x;I)V
    .locals 2

    .prologue
    .line 1765
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cf$g;

    .line 1766
    iget-object v0, v0, Lcom/twitter/android/cf$g;->n:Lcom/twitter/android/cf$d;

    .line 1767
    iget-object v0, v0, Lcom/twitter/android/cf$d;->a:Landroid/widget/TextView;

    .line 1768
    iget-object v1, p2, Lcom/twitter/android/timeline/x;->a:Lcom/twitter/model/timeline/m;

    iget-object v1, v1, Lcom/twitter/model/timeline/m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1769
    iget-object v0, p0, Lcom/twitter/android/cf;->y:Lcom/twitter/android/timeline/w;

    add-int/lit8 v1, p3, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/timeline/w;->a(Lcom/twitter/android/timeline/x;I)V

    .line 1770
    return-void
.end method

.method private varargs a(Landroid/view/View;[I)V
    .locals 4

    .prologue
    .line 2292
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget v2, p2, v0

    .line 2293
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2294
    if-eqz v2, :cond_0

    .line 2295
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2292
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2298
    :cond_1
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1895
    if-eqz p0, :cond_0

    .line 1896
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1897
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1899
    :cond_0
    return-void

    .line 1897
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/timeline/bk;Landroid/view/View;Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1590
    check-cast p1, Lcom/twitter/android/timeline/a;

    .line 1591
    iget-object v0, p0, Lcom/twitter/android/cf;->K:Lcom/twitter/android/revenue/c;

    iget-object v2, p1, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/android/revenue/c;->c(Ljava/lang/String;)Lcom/twitter/android/revenue/a;

    move-result-object v2

    .line 1592
    if-eqz v2, :cond_1

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "AdSlot "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " did not receive an ad when it should have."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/twitter/util/g;->a(ZLjava/lang/String;)Z

    .line 1593
    iget-object v0, p0, Lcom/twitter/android/cf;->M:Lcom/twitter/android/timeline/b;

    invoke-virtual {v0, p1, v2}, Lcom/twitter/android/timeline/b;->a(Lcom/twitter/android/timeline/a;Lcom/twitter/android/revenue/a;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    .line 1594
    if-eqz v0, :cond_0

    .line 1595
    invoke-super {p0, p2, p3, v0, p4}, Lcom/twitter/android/y;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V

    .line 1598
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cu;

    .line 1599
    iget-object v2, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2, v1}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    .line 1602
    iget-object v1, p0, Lcom/twitter/android/cf;->L:Lcom/twitter/android/revenue/e;

    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/revenue/e;->a(Lcom/twitter/library/widget/TweetView;)V

    .line 1604
    :cond_0
    return-void

    .line 1592
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/twitter/internal/android/widget/GroupedRowView;ZZ)V
    .locals 1

    .prologue
    .line 1910
    if-eqz p1, :cond_1

    .line 1913
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    .line 1914
    if-eqz p2, :cond_0

    .line 1916
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->a()V

    .line 1927
    :cond_0
    :goto_0
    return-void

    .line 1918
    :cond_1
    if-eqz p2, :cond_2

    .line 1921
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 1925
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/timeline/cd;)Z
    .locals 2

    .prologue
    .line 1939
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 1940
    iget v1, v0, Lcom/twitter/model/core/Tweet;->d:I

    invoke-static {v1}, Lbwi;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lbwi;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 814
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040406

    invoke-virtual {v0, v1, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 815
    iget-object v1, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v1}, Lcom/twitter/android/trends/g;->a()F

    move-result v1

    .line 816
    iget-object v2, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v2}, Lcom/twitter/android/trends/g;->c()F

    move-result v2

    .line 817
    new-instance v3, Lcom/twitter/android/cf$l;

    invoke-direct {v3, v0}, Lcom/twitter/android/cf$l;-><init>(Landroid/view/View;)V

    .line 819
    iget-object v4, v3, Lcom/twitter/android/cf$l;->e:Lcom/twitter/android/cf$h;

    iget-object v4, v4, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 821
    iget-object v4, v3, Lcom/twitter/android/cf$l;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 823
    iget-object v4, v3, Lcom/twitter/android/cf$l;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 824
    iget-object v1, v3, Lcom/twitter/android/cf$l;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 827
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 828
    invoke-virtual {v1, v3}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$l;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 829
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 830
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 831
    return-object v0
.end method

.method private b(Landroid/view/View;Lcom/twitter/android/timeline/bb;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1855
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/cf$g;

    .line 1856
    iget-object v3, p2, Lcom/twitter/android/timeline/bb;->e:Lcom/twitter/model/timeline/r;

    .line 1857
    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v0, v0, Lcom/twitter/android/cf$a;->a:Lcom/twitter/android/widget/CarouselRowView;

    .line 1858
    const v1, 0x7f13007e

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/widget/CarouselRowView;->a(ILcom/twitter/android/timeline/bk;)V

    .line 1859
    invoke-static {p2}, Lcom/twitter/android/cf;->b(Lcom/twitter/android/timeline/bk;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/CarouselRowView;->setShowDismiss(Z)V

    .line 1860
    iget-object v1, p2, Lcom/twitter/android/timeline/bb;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/CarouselRowView;->setTitleText(Ljava/lang/String;)V

    .line 1861
    invoke-virtual {p2}, Lcom/twitter/android/timeline/bb;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v4

    iget-object v5, p2, Lcom/twitter/android/timeline/bb;->a:Lcbi;

    iget-object v0, v2, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    iget-object v7, v0, Lcom/twitter/android/cf$a;->b:Lcom/twitter/android/widget/d;

    move-object v0, p0

    move-object v1, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/cf$g;Lcom/twitter/model/timeline/r;Lcom/twitter/android/timeline/bg;Lcbi;Landroid/os/Bundle;Lcom/twitter/android/widget/d;)V

    .line 1863
    return-void
.end method

.method static b(Lcom/twitter/android/timeline/bk;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1636
    invoke-static {p0}, Lcom/twitter/android/cf;->c(Lcom/twitter/android/timeline/bk;)Z

    move-result v0

    return v0
.end method

.method private c(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    .prologue
    .line 956
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 957
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 960
    new-instance v1, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-direct {v1, p1}, Lcom/twitter/internal/android/widget/GroupedRowView;-><init>(Landroid/content/Context;)V

    .line 961
    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->addView(Landroid/view/View;)V

    .line 964
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setClickable(Z)V

    .line 965
    return-object v1
.end method

.method private c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 835
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04040a

    invoke-virtual {v0, v1, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 837
    iget-object v1, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v1}, Lcom/twitter/android/trends/g;->a()F

    move-result v1

    .line 838
    iget-object v2, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v2}, Lcom/twitter/android/trends/g;->c()F

    move-result v2

    .line 839
    new-instance v3, Lcom/twitter/android/cf$k;

    invoke-direct {v3, v0}, Lcom/twitter/android/cf$k;-><init>(Landroid/view/View;)V

    .line 842
    iget-object v4, v3, Lcom/twitter/android/cf$k;->e:Lcom/twitter/android/cf$h;

    iget-object v4, v4, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 844
    iget-object v4, v3, Lcom/twitter/android/cf$k;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 846
    iget-object v1, v3, Lcom/twitter/android/cf$k;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 848
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 849
    invoke-virtual {v1, v3}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$k;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 850
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 851
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 852
    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 681
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cf$g;

    .line 682
    iget-object v0, v0, Lcom/twitter/android/cf$g;->i:Lcom/twitter/android/z;

    .line 684
    iget-object v6, v0, Lcom/twitter/android/z;->b:Landroid/support/v7/widget/RecyclerView;

    .line 685
    if-eqz v6, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/cf;->n:Z

    if-eqz v0, :cond_0

    .line 686
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 687
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 689
    const v1, 0x7f0e0519

    .line 690
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 691
    const v1, 0x7f0e0518

    .line 692
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    .line 693
    const v1, 0x7f0e0516

    .line 694
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 696
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    .line 697
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    invoke-direct {v1, v0, v9, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 696
    invoke-virtual {v6, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 698
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/o;

    sget-object v1, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    const/4 v2, 0x1

    const/high16 v8, 0x7f110000

    .line 700
    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/people/adapters/viewbinders/o;-><init>(Lcom/twitter/media/ui/image/config/g;ZIIF)V

    .line 701
    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 702
    new-instance v0, Lcom/twitter/internal/android/widget/d;

    invoke-direct {v0, v7, v9}, Lcom/twitter/internal/android/widget/d;-><init>(II)V

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 703
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 705
    :cond_0
    return-void
.end method

.method static c(Lcom/twitter/android/timeline/bk;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1688
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    iget v0, v0, Lcom/twitter/android/timeline/bg;->s:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)Landroid/view/View;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 929
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 930
    new-instance v1, Lcom/twitter/android/cb;

    invoke-direct {v1, v0}, Lcom/twitter/android/cb;-><init>(Landroid/view/View;)V

    .line 931
    new-instance v2, Lcom/twitter/android/cf$g$a;

    invoke-direct {v2}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 932
    invoke-virtual {v2, v1}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cb;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 933
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 934
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 935
    return-object v0
.end method

.method private d(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2281
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0402dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2284
    const v0, 0x7f13011b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2285
    invoke-static {}, Lcom/twitter/android/revenue/k;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2287
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/cf;->a(Landroid/view/View;[I)V

    .line 2288
    return-object v1

    .line 2287
    :array_0
    .array-data 4
        0x7f13011b
        0x7f130504
        0x7f130568
    .end array-data
.end method

.method private d(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 856
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040409

    invoke-virtual {v0, v1, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 858
    iget-object v1, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v1}, Lcom/twitter/android/trends/g;->a()F

    move-result v1

    .line 859
    new-instance v2, Lcom/twitter/android/cf$k;

    invoke-direct {v2, v0}, Lcom/twitter/android/cf$k;-><init>(Landroid/view/View;)V

    .line 862
    iget-object v3, v2, Lcom/twitter/android/cf$k;->e:Lcom/twitter/android/cf$h;

    iget-object v3, v3, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 864
    iget-object v3, v2, Lcom/twitter/android/cf$k;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 866
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 867
    invoke-virtual {v1, v2}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$k;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 868
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 869
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 870
    return-object v0
.end method

.method private static d(Lcom/twitter/android/timeline/bk;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1930
    instance-of v1, p0, Lcom/twitter/android/timeline/cd;

    if-eqz v1, :cond_0

    .line 1931
    check-cast p0, Lcom/twitter/android/timeline/cd;

    .line 1932
    iget-object v1, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 1933
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->t()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1935
    :cond_0
    return v0
.end method

.method private static e(Lcom/twitter/android/timeline/bk;)I
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1944
    instance-of v2, p0, Lcom/twitter/android/timeline/ca;

    if-eqz v2, :cond_7

    .line 1945
    check-cast p0, Lcom/twitter/android/timeline/ca;

    .line 1946
    iget-object v3, p0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    .line 1947
    iget-object v2, p0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    .line 1948
    iget-object v4, p0, Lcom/twitter/android/timeline/ca;->i:Ljava/lang/String;

    .line 1950
    if-eqz v2, :cond_0

    move v2, v0

    .line 1951
    :goto_0
    if-eqz v3, :cond_1

    iget-boolean v3, v3, Lcom/twitter/model/topic/e;->j:Z

    if-eqz v3, :cond_1

    .line 1952
    :goto_1
    invoke-static {v4}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 1954
    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 1955
    const/16 v0, 0xf

    .line 1969
    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 1950
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1951
    goto :goto_1

    .line 1956
    :cond_2
    if-eqz v0, :cond_3

    .line 1957
    const/16 v0, 0xe

    goto :goto_2

    .line 1958
    :cond_3
    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    .line 1959
    const/16 v0, 0x10

    goto :goto_2

    .line 1960
    :cond_4
    if-eqz v2, :cond_5

    .line 1961
    const/16 v0, 0xc

    goto :goto_2

    .line 1962
    :cond_5
    if-eqz v1, :cond_6

    .line 1963
    const/16 v0, 0xd

    goto :goto_2

    .line 1965
    :cond_6
    const/16 v0, 0x8

    goto :goto_2

    .line 1969
    :cond_7
    const/4 v0, 0x7

    goto :goto_2
.end method

.method private e(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 874
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f0403fc

    invoke-virtual {v0, v1, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 876
    iget-object v1, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v1}, Lcom/twitter/android/trends/g;->a()F

    move-result v1

    .line 877
    new-instance v2, Lcom/twitter/android/cf$l;

    invoke-direct {v2, v0}, Lcom/twitter/android/cf$l;-><init>(Landroid/view/View;)V

    .line 879
    iget-object v3, v2, Lcom/twitter/android/cf$l;->e:Lcom/twitter/android/cf$h;

    iget-object v3, v3, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 881
    iget-object v3, v2, Lcom/twitter/android/cf$l;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 883
    iget-object v3, v2, Lcom/twitter/android/cf$l;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 884
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 885
    invoke-virtual {v1, v2}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$l;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 886
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 887
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 888
    return-object v0
.end method

.method private f(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 892
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040403

    invoke-virtual {v0, v1, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 894
    iget-object v1, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v1}, Lcom/twitter/android/trends/g;->a()F

    move-result v1

    .line 895
    iget-object v2, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v2}, Lcom/twitter/android/trends/g;->b()F

    move-result v2

    .line 896
    new-instance v3, Lcom/twitter/android/cf$j;

    invoke-direct {v3, v0}, Lcom/twitter/android/cf$j;-><init>(Landroid/view/View;)V

    .line 898
    iget-object v4, v3, Lcom/twitter/android/cf$j;->b:Lcom/twitter/android/cf$h;

    iget-object v4, v4, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 900
    iget-object v2, v3, Lcom/twitter/android/cf$j;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 902
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 903
    invoke-virtual {v1, v3}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$j;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 904
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 905
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 906
    return-object v0
.end method

.method private g(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 910
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040404

    invoke-virtual {v0, v1, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 912
    iget-object v1, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v1}, Lcom/twitter/android/trends/g;->a()F

    move-result v1

    .line 913
    iget-object v2, p0, Lcom/twitter/android/cf;->v:Lcom/twitter/android/trends/g;

    invoke-virtual {v2}, Lcom/twitter/android/trends/g;->b()F

    move-result v2

    .line 914
    new-instance v3, Lcom/twitter/android/cf$i;

    invoke-direct {v3, v0}, Lcom/twitter/android/cf$i;-><init>(Landroid/view/View;)V

    .line 916
    iget-object v4, v3, Lcom/twitter/android/cf$i;->c:Lcom/twitter/android/cf$h;

    iget-object v4, v4, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 918
    iget-object v2, v3, Lcom/twitter/android/cf$i;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 920
    new-instance v1, Lcom/twitter/android/cf$g$a;

    invoke-direct {v1}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 921
    invoke-virtual {v1, v3}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$i;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 922
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 923
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 924
    return-object v0
.end method

.method private h(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 993
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040126

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 994
    const v0, 0x7f1303c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/InlineDismissView;

    .line 995
    new-instance v2, Lcom/twitter/android/ad;

    invoke-direct {v2, v0}, Lcom/twitter/android/ad;-><init>(Lcom/twitter/library/widget/InlineDismissView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 996
    return-object v1
.end method

.method private h()V
    .locals 2

    .prologue
    .line 356
    iget v0, p0, Lcom/twitter/android/cf;->i:I

    const/16 v1, 0x1c

    if-ne v0, v1, :cond_0

    .line 358
    iget-object v0, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-static {v0}, Lcom/twitter/android/trends/c;->a(Landroid/content/Context;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf;->z:Landroid/util/SparseArray;

    .line 360
    :cond_0
    return-void
.end method

.method private i()Landroid/view/View;
    .locals 3

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/twitter/android/cf;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040131

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 743
    const v1, 0x7f13014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 744
    return-object v0
.end method

.method private i(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040133

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1002
    const v0, 0x7f1303c9

    .line 1003
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TimelineMessageView;

    .line 1004
    new-instance v2, Lcom/twitter/android/cf$g$a;

    invoke-direct {v2}, Lcom/twitter/android/cf$g$a;-><init>()V

    new-instance v3, Lcom/twitter/android/cf$f;

    invoke-direct {v3, v0}, Lcom/twitter/android/cf$f;-><init>(Lcom/twitter/library/widget/TimelineMessageView;)V

    .line 1005
    invoke-virtual {v2, v3}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$f;)Lcom/twitter/android/cf$g$a;

    move-result-object v2

    .line 1006
    invoke-virtual {v2}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v2

    .line 1004
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1007
    iget-object v2, p0, Lcom/twitter/android/cf;->k:Lcom/twitter/android/ch;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TimelineMessageView;->a(Lcom/twitter/library/widget/TimelineMessageView$a;)Lcom/twitter/library/widget/TimelineMessageView;

    .line 1008
    return-object v1
.end method

.method private j(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040134

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1013
    new-instance v1, Lcom/twitter/android/cf$d;

    invoke-direct {v1, v0}, Lcom/twitter/android/cf$d;-><init>(Landroid/view/View;)V

    .line 1014
    new-instance v2, Lcom/twitter/android/cf$g$a;

    invoke-direct {v2}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 1015
    invoke-virtual {v2, v1}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$d;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 1016
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 1014
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1017
    return-object v0
.end method

.method private l()Landroid/view/View;
    .locals 3

    .prologue
    .line 950
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040120

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 951
    invoke-virtual {p0, v0}, Lcom/twitter/android/cf;->b(Landroid/view/View;)V

    .line 952
    return-object v0
.end method


# virtual methods
.method protected U_()Lcbp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbp",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    new-instance v0, Lcbn;

    invoke-static {}, Lcom/twitter/android/timeline/bj;->a()Lcbp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbn;-><init>(Lcbp;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/android/timeline/bk;)I
    .locals 8

    .prologue
    const/16 v2, 0x11

    const/16 v7, 0xb

    const/16 v0, 0x17

    const/4 v1, 0x7

    const/4 v6, 0x1

    .line 378
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v3

    .line 380
    iget v4, v3, Lcom/twitter/android/timeline/bg;->h:I

    .line 382
    iget v5, v3, Lcom/twitter/android/timeline/bg;->e:I

    .line 383
    packed-switch v5, :pswitch_data_0

    .line 521
    :cond_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/y;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v1

    :cond_1
    :goto_0
    return v1

    .line 385
    :pswitch_1
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->j(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 386
    invoke-static {v6, v3, v0, v7}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 388
    :cond_2
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->l(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 389
    invoke-static {v6, v3, v0, v7}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 391
    :cond_3
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->q(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 392
    invoke-static {v6, v3, v0, v7}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 394
    :cond_4
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->r(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 402
    :pswitch_2
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 403
    iget-object v0, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v0, v0, Lcom/twitter/model/timeline/ar;->c:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_5

    const/16 v0, 0x1d

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    .line 407
    :pswitch_3
    const/4 v0, 0x2

    invoke-static {v6, v3, v1, v0}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 411
    :pswitch_4
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->b(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 412
    const/4 v0, 0x3

    invoke-static {v6, v3, v1, v0}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 414
    :cond_6
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->c(I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 415
    const/4 v1, 0x4

    invoke-static {v6, v3, v0, v1}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 417
    :cond_7
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->k(I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 418
    const/16 v1, 0x13

    invoke-static {v6, v3, v0, v1}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 420
    :cond_8
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->q(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 421
    const/4 v1, 0x0

    invoke-static {v6, v3, v0, v1}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 423
    :cond_9
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->g(I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 424
    invoke-static {v6, v3, v0, v2}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto :goto_0

    .line 426
    :cond_a
    iget-boolean v3, p0, Lcom/twitter/android/cf;->B:Z

    if-eqz v3, :cond_b

    invoke-static {p1}, Lcom/twitter/android/cf;->d(Lcom/twitter/android/timeline/bk;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 427
    const/16 v1, 0x12

    goto/16 :goto_0

    .line 428
    :cond_b
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->o(I)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 429
    invoke-static {}, Lbsd;->l()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 431
    :cond_c
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v3

    iget v3, v3, Lcom/twitter/android/timeline/bg;->q:I

    if-ne v3, v6, :cond_d

    move v1, v0

    .line 432
    goto/16 :goto_0

    .line 433
    :cond_d
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->e(I)Z

    move-result v0

    if-eqz v0, :cond_10

    move-object v0, p1

    .line 435
    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 436
    iget-object v2, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v2

    if-eqz v2, :cond_e

    iget-boolean v2, p0, Lcom/twitter/android/cf;->O:Z

    if-eqz v2, :cond_1

    .line 441
    :cond_e
    iget-object v2, p0, Lcom/twitter/android/cf;->C:Ljava/util/Set;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/android/cf;->C:Ljava/util/Set;

    iget-object v3, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v3}, Lcom/twitter/model/core/Tweet;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 445
    :cond_f
    invoke-static {v0}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/timeline/cd;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 448
    :cond_10
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->r(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 449
    goto/16 :goto_0

    .line 454
    :pswitch_5
    invoke-static {p1}, Lcom/twitter/android/cf;->e(Lcom/twitter/android/timeline/bk;)I

    move-result v1

    goto/16 :goto_0

    .line 457
    :pswitch_6
    const/16 v1, 0x1f

    goto/16 :goto_0

    .line 460
    :pswitch_7
    const/16 v1, 0x21

    goto/16 :goto_0

    .line 463
    :pswitch_8
    const/16 v1, 0x18

    goto/16 :goto_0

    .line 466
    :pswitch_9
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->t(I)Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0xa

    :goto_2
    move v1, v0

    goto/16 :goto_0

    :cond_11
    const/16 v0, 0x9

    goto :goto_2

    .line 470
    :pswitch_a
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->m(I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 471
    invoke-direct {p0, p1, v3}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/android/timeline/bg;)I

    move-result v1

    goto/16 :goto_0

    .line 472
    :cond_12
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->q(I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 473
    const/16 v0, 0x1c

    invoke-static {v6, v3, v1, v0}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto/16 :goto_0

    .line 474
    :cond_13
    instance-of v0, p1, Lcom/twitter/android/timeline/bb;

    if-eqz v0, :cond_14

    .line 475
    const/16 v1, 0x15

    goto/16 :goto_0

    .line 476
    :cond_14
    instance-of v0, p1, Lcom/twitter/android/timeline/co;

    if-eqz v0, :cond_0

    .line 477
    const/16 v1, 0x1c

    goto/16 :goto_0

    .line 482
    :pswitch_b
    invoke-static {v4}, Lcom/twitter/model/timeline/z$a;->p(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    invoke-static {}, Lbsd;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v1, 0x16

    goto/16 :goto_0

    .line 489
    :pswitch_c
    invoke-static {}, Lbsd;->i()Z

    move-result v1

    const/16 v2, 0x1a

    invoke-static {v1, v3, v0, v2}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto/16 :goto_0

    .line 493
    :pswitch_d
    iget-boolean v0, p0, Lcom/twitter/android/cf;->N:Z

    if-eqz v0, :cond_1

    .line 494
    check-cast p1, Lcom/twitter/android/timeline/a;

    .line 495
    iget-object v0, p0, Lcom/twitter/android/cf;->K:Lcom/twitter/android/revenue/c;

    iget-object v1, p1, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 496
    const-string/jumbo v1, "tweet"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 497
    const/16 v0, 0x1e

    const/16 v1, 0x19

    invoke-static {v6, v3, v0, v1}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto/16 :goto_0

    .line 500
    :cond_15
    const/16 v1, 0x1b

    goto/16 :goto_0

    .line 507
    :pswitch_e
    const/4 v0, 0x5

    invoke-static {v6, v3, v1, v0}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto/16 :goto_0

    .line 511
    :pswitch_f
    iget-boolean v0, p0, Lcom/twitter/android/cf;->P:Z

    if-eqz v0, :cond_1

    .line 512
    const/16 v0, 0x20

    invoke-static {v6, v3, v1, v0}, Lcom/twitter/android/cf;->a(ZLcom/twitter/android/timeline/bg;II)I

    move-result v1

    goto/16 :goto_0

    .line 383
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_5
        :pswitch_a
        :pswitch_b
        :pswitch_8
        :pswitch_d
        :pswitch_c
        :pswitch_e
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_f
        :pswitch_7
    .end packed-switch
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 168
    check-cast p1, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    return v0
.end method

.method protected a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 969
    new-instance v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-direct {v0, p1}, Lcom/twitter/internal/android/widget/GroupedRowView;-><init>(Landroid/content/Context;)V

    .line 970
    new-instance v1, Lcom/twitter/android/UmfInlinePromptView;

    invoke-direct {v1, p1}, Lcom/twitter/android/UmfInlinePromptView;-><init>(Landroid/content/Context;)V

    .line 971
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/UmfInlinePromptView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 973
    invoke-virtual {v1}, Lcom/twitter/android/UmfInlinePromptView;->e()V

    .line 974
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->addView(Landroid/view/View;)V

    .line 975
    new-instance v2, Lcom/twitter/android/cf$c;

    invoke-direct {v2, v1}, Lcom/twitter/android/cf$c;-><init>(Lcom/twitter/android/UmfInlinePromptView;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    .line 976
    return-object v0
.end method

.method protected a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 981
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04013c

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 982
    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/WhoToFollowUsersView;

    .line 983
    iget-object v2, p0, Lcom/twitter/android/cf;->j:Lcom/twitter/app/users/c;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/WhoToFollowUsersView;->setTimelineUserClickListener(Lcom/twitter/app/users/c;)V

    .line 984
    new-instance v2, Lcom/twitter/android/cf$g$a;

    invoke-direct {v2}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 985
    invoke-virtual {v2, v0}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/widget/WhoToFollowUsersView;)Lcom/twitter/android/cf$g$a;

    move-result-object v0

    .line 986
    invoke-virtual {v0}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v0

    .line 987
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 988
    return-object v1
.end method

.method public a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 548
    invoke-virtual {p0, p2}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    .line 550
    packed-switch v0, :pswitch_data_0

    .line 667
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/cf;->W:Lckd;

    invoke-interface {v0, p2}, Lckd;->a(Ljava/lang/Object;)I

    move-result v0

    .line 668
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 669
    iget-object v1, p0, Lcom/twitter/android/cf;->W:Lckd;

    invoke-interface {v1, v0}, Lckd;->a(I)Lckb;

    move-result-object v0

    .line 670
    invoke-static {p3, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    .line 677
    :goto_0
    return-object v0

    .line 552
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04011d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 553
    new-instance v1, Lcom/twitter/android/cf$b;

    invoke-direct {v1, v0}, Lcom/twitter/android/cf$b;-><init>(Landroid/view/View;)V

    .line 555
    new-instance v2, Lcom/twitter/android/cf$g$a;

    invoke-direct {v2}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 556
    invoke-virtual {v2, v1}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$b;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 557
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 558
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 562
    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/android/cf;->l()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 566
    :pswitch_3
    invoke-direct {p0}, Lcom/twitter/android/cf;->l()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 570
    :pswitch_4
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->b(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 574
    :pswitch_5
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->c(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 578
    :pswitch_6
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->d(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 582
    :pswitch_7
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->e(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 586
    :pswitch_8
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->f(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 590
    :pswitch_9
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->g(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 594
    :pswitch_a
    iget-object v0, p0, Lcom/twitter/android/cf;->T:Lcom/twitter/android/timeline/ch;

    invoke-static {p3, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 598
    :pswitch_b
    const v0, 0x7f04012a

    invoke-direct {p0, v0}, Lcom/twitter/android/cf;->d(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 602
    :pswitch_c
    const v0, 0x7f040123

    invoke-virtual {p0, v0}, Lcom/twitter/android/cf;->b(I)Landroid/view/View;

    move-result-object v0

    .line 603
    invoke-direct {p0, v0}, Lcom/twitter/android/cf;->c(Landroid/view/View;)V

    goto :goto_0

    .line 608
    :pswitch_d
    invoke-direct {p0, p1}, Lcom/twitter/android/cf;->c(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 612
    :pswitch_e
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 617
    :pswitch_f
    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 621
    :pswitch_10
    invoke-direct {p0, p1}, Lcom/twitter/android/cf;->d(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 622
    invoke-virtual {p0, v0}, Lcom/twitter/android/cf;->b(Landroid/view/View;)V

    goto :goto_0

    .line 626
    :pswitch_11
    invoke-direct {p0}, Lcom/twitter/android/cf;->i()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 630
    :pswitch_12
    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/cf;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 634
    :pswitch_13
    iget-object v0, p0, Lcom/twitter/android/cf;->H:Lcom/twitter/android/timeline/cn;

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/cf;->a(Landroid/view/ViewGroup;Lcom/twitter/android/timeline/cn;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 638
    :pswitch_14
    iget-object v0, p0, Lcom/twitter/android/cf;->I:Lcom/twitter/android/timeline/au;

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/cf;->a(Landroid/view/ViewGroup;Lcom/twitter/android/timeline/au;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 643
    :pswitch_15
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->h(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 647
    :pswitch_16
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->i(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 651
    :pswitch_17
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/y;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 655
    :pswitch_18
    invoke-direct {p0, p3}, Lcom/twitter/android/cf;->j(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 659
    :pswitch_19
    iget-object v0, p0, Lcom/twitter/android/cf;->U:Lcom/twitter/android/timeline/t;

    invoke-static {p3, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 663
    :pswitch_1a
    iget-object v0, p0, Lcom/twitter/android/cf;->V:Lcom/twitter/android/timeline/ag;

    invoke-static {p3, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 672
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/y;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 550
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_18
        :pswitch_b
        :pswitch_d
        :pswitch_4
        :pswitch_f
        :pswitch_f
        :pswitch_c
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_6
        :pswitch_e
        :pswitch_10
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_14
        :pswitch_d
        :pswitch_0
        :pswitch_11
        :pswitch_15
        :pswitch_a
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 168
    check-cast p2, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/cf;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1093
    invoke-direct {p0, p1}, Lcom/twitter/android/cf;->c(Landroid/view/View;)V

    .line 1094
    return-void
.end method

.method public a(Landroid/view/View;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1098
    sub-int v0, p2, p3

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1099
    invoke-virtual {p0, v0}, Lcom/twitter/android/cf;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/timeline/bk;

    .line 1100
    instance-of v0, v2, Lcom/twitter/android/timeline/j;

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 1101
    check-cast v0, Lcom/twitter/android/timeline/j;

    invoke-interface {v0}, Lcom/twitter/android/timeline/j;->a()Lcom/twitter/model/timeline/o;

    move-result-object v0

    .line 1102
    iget-object v3, v0, Lcom/twitter/model/timeline/o;->c:Ljava/lang/String;

    const/4 v4, 0x0

    const v5, 0x7f0a0743

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/bk;Ljava/lang/String;Lcom/twitter/model/timeline/j;I)V

    .line 1116
    :goto_0
    return-void

    .line 1103
    :cond_0
    if-eqz v2, :cond_2

    iget-object v0, v2, Lcom/twitter/android/timeline/bk;->f:Lcha;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/twitter/android/timeline/bk;->f:Lcha;

    iget-boolean v0, v0, Lcha;->c:Z

    if-eqz v0, :cond_2

    .line 1105
    instance-of v0, v2, Lcom/twitter/android/timeline/cq;

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 1106
    check-cast v0, Lcom/twitter/android/timeline/cq;

    .line 1107
    iget-object v0, v0, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v4, v0, Lcom/twitter/model/timeline/at$a;->j:Lcom/twitter/model/timeline/j;

    .line 1111
    :goto_1
    iget-object v0, v2, Lcom/twitter/android/timeline/bk;->f:Lcha;

    iget-object v3, v0, Lcha;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/bk;Ljava/lang/String;Lcom/twitter/model/timeline/j;I)V

    goto :goto_0

    .line 1109
    :cond_1
    iget-object v0, v2, Lcom/twitter/android/timeline/bk;->f:Lcha;

    iget-object v4, v0, Lcha;->d:Lcom/twitter/model/timeline/j;

    goto :goto_1

    .line 1113
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Attempted to display a pinned header for an unsupported item type.\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 1144
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/cf;->b(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1148
    :goto_0
    return-void

    .line 1145
    :catch_0
    move-exception v0

    .line 1146
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 168
    check-cast p3, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/cf;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V

    return-void
.end method

.method protected a(Landroid/view/View;Lcom/twitter/android/timeline/n;)V
    .locals 3

    .prologue
    .line 1021
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cf$c;

    .line 1023
    iget-object v1, v0, Lcom/twitter/android/cf$c;->a:Lcom/twitter/android/UmfInlinePromptView;

    iget-object v1, v1, Lcom/twitter/android/UmfInlinePromptView;->a:Lcom/twitter/model/timeline/n;

    .line 1024
    if-eqz v1, :cond_0

    iget-object v2, p2, Lcom/twitter/android/timeline/n;->a:Lcom/twitter/model/timeline/n;

    invoke-virtual {v1, v2}, Lcom/twitter/model/timeline/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/cf$c;->a:Lcom/twitter/android/UmfInlinePromptView;

    .line 1025
    invoke-virtual {v1}, Lcom/twitter/android/UmfInlinePromptView;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1026
    iget-object v1, v0, Lcom/twitter/android/cf$c;->a:Lcom/twitter/android/UmfInlinePromptView;

    invoke-virtual {v1}, Lcom/twitter/android/UmfInlinePromptView;->c()V

    .line 1027
    iget-object v0, v0, Lcom/twitter/android/cf$c;->a:Lcom/twitter/android/UmfInlinePromptView;

    invoke-virtual {v0}, Lcom/twitter/android/UmfInlinePromptView;->e()V

    .line 1031
    :goto_0
    return-void

    .line 1029
    :cond_0
    iget-object v0, v0, Lcom/twitter/android/cf$c;->a:Lcom/twitter/android/UmfInlinePromptView;

    iget-object v1, p2, Lcom/twitter/android/timeline/n;->a:Lcom/twitter/model/timeline/n;

    invoke-virtual {v0, v1}, Lcom/twitter/android/UmfInlinePromptView;->a(Lcom/twitter/model/timeline/n;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/av;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcgi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1994
    iput-object p1, p0, Lcom/twitter/android/cf;->p:Lcom/twitter/android/av;

    .line 1995
    return-void
.end method

.method public a(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 341
    iput-object p1, p0, Lcom/twitter/android/cf;->C:Ljava/util/Set;

    .line 342
    return-void
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 541
    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->getItemViewType(I)I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(II)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1051
    .line 1052
    sub-int v3, p1, p2

    .line 1053
    invoke-virtual {p0}, Lcom/twitter/android/cf;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    .line 1054
    if-ltz v3, :cond_3

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1055
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bl;->c()I

    move-result v4

    .line 1056
    invoke-virtual {p0, v3}, Lcom/twitter/android/cf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/bk;

    .line 1057
    if-eqz v1, :cond_2

    .line 1058
    invoke-virtual {v1}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v1

    .line 1060
    iget v5, v1, Lcom/twitter/android/timeline/bg;->q:I

    if-nez v5, :cond_1

    .line 1061
    iget v5, v1, Lcom/twitter/android/timeline/bg;->u:I

    packed-switch v5, :pswitch_data_0

    .line 1076
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Invalid pinned header state "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v1, v1, Lcom/twitter/android/timeline/bg;->u:I

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcpd;->c(Ljava/lang/Throwable;)V

    move v1, v2

    .line 1084
    :goto_0
    if-eq v4, v3, :cond_0

    .line 1085
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/bl;->f_(I)Z

    :cond_0
    move v0, v1

    .line 1088
    :goto_1
    return v0

    :pswitch_0
    move v1, v2

    .line 1064
    goto :goto_0

    .line 1067
    :pswitch_1
    const/4 v2, 0x1

    move v1, v2

    .line 1068
    goto :goto_0

    .line 1071
    :pswitch_2
    const/4 v2, 0x2

    move v1, v2

    .line 1072
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1081
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 1061
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(I)Landroid/view/View;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 940
    iget-object v0, p0, Lcom/twitter/android/cf;->u:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 941
    new-instance v1, Lcom/twitter/android/z;

    invoke-direct {v1, v0}, Lcom/twitter/android/z;-><init>(Landroid/view/View;)V

    .line 942
    new-instance v2, Lcom/twitter/android/cf$g$a;

    invoke-direct {v2}, Lcom/twitter/android/cf$g$a;-><init>()V

    .line 943
    invoke-virtual {v2, v1}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/z;)Lcom/twitter/android/cf$g$a;

    move-result-object v1

    .line 944
    invoke-virtual {v1}, Lcom/twitter/android/cf$g$a;->a()Lcom/twitter/android/cf$g;

    move-result-object v1

    .line 945
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 946
    return-object v0
.end method

.method protected b(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V
    .locals 18

    .prologue
    .line 1154
    if-nez p4, :cond_0

    .line 1155
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/cf;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move/from16 v0, p4

    if-ne v0, v6, :cond_6

    const/4 v6, 0x1

    move v7, v6

    .line 1157
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v6

    .line 1158
    iget v8, v6, Lcom/twitter/android/timeline/bg;->d:I

    .line 1160
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v15

    .line 1161
    iget-boolean v0, v6, Lcom/twitter/android/timeline/bg;->m:Z

    move/from16 v16, v0

    .line 1162
    iget-boolean v0, v6, Lcom/twitter/android/timeline/bg;->n:Z

    move/from16 v17, v0

    .line 1163
    iget-boolean v8, v6, Lcom/twitter/android/timeline/bg;->k:Z

    .line 1164
    iget-boolean v9, v6, Lcom/twitter/android/timeline/bg;->l:Z

    .line 1165
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1166
    const-string/jumbo v6, "position"

    move/from16 v0, p4

    invoke-virtual {v10, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1168
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v6

    invoke-virtual {v6}, Lcof;->p()Z

    move-result v6

    if-eqz v6, :cond_2

    const/16 v6, 0xb

    if-eq v15, v6, :cond_2

    .line 1169
    invoke-virtual/range {p3 .. p4}, Lcom/twitter/android/timeline/bk;->a(I)Ljava/lang/String;

    move-result-object v11

    .line 1170
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    .line 1171
    instance-of v12, v6, Lcom/twitter/android/cu;

    if-eqz v12, :cond_1

    .line 1172
    check-cast v6, Lcom/twitter/android/cu;

    .line 1173
    iget-object v6, v6, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    const v12, 0x7f130092

    invoke-virtual {v6, v12, v11}, Lcom/twitter/library/widget/TweetView;->setTag(ILjava/lang/Object;)V

    .line 1175
    :cond_1
    const v6, 0x7f130092

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1178
    :cond_2
    packed-switch v15, :pswitch_data_0

    .line 1478
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->W:Lckd;

    move-object/from16 v0, p3

    invoke-interface {v6, v0}, Lckd;->a(Ljava/lang/Object;)I

    move-result v6

    .line 1479
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1a

    .line 1480
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/cf;->W:Lckd;

    invoke-interface {v7, v6}, Lckd;->a(I)Lckb;

    move-result-object v6

    .line 1481
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v6, v1, v2}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    .line 1504
    :cond_3
    :goto_1
    :pswitch_1
    const/16 v6, 0x11

    if-eq v6, v15, :cond_4

    const/16 v6, 0x15

    if-eq v6, v15, :cond_4

    const/16 v6, 0x1a

    if-eq v6, v15, :cond_4

    const/16 v6, 0x1e

    if-eq v6, v15, :cond_4

    const/16 v6, 0x17

    if-ne v6, v15, :cond_1c

    .line 1509
    :cond_4
    check-cast p1, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 1510
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    .line 1516
    :cond_5
    :goto_2
    return-void

    .line 1155
    :cond_6
    const/4 v6, 0x0

    move v7, v6

    goto/16 :goto_0

    .line 1180
    :pswitch_2
    check-cast p3, Lcom/twitter/android/timeline/g;

    .line 1181
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cf$g;

    .line 1182
    iget-object v7, v6, Lcom/twitter/android/cf$g;->a:Lcom/twitter/android/cf$b;

    .line 1184
    const-wide/16 v8, 0x0

    .line 1186
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/g;->a:Lcom/twitter/model/timeline/f$a;

    .line 1187
    if-eqz v6, :cond_7

    .line 1188
    iget-wide v8, v6, Lcom/twitter/model/timeline/f$a;->b:J

    .line 1189
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c0020

    iget v12, v6, Lcom/twitter/model/timeline/f$a;->c:I

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    iget v6, v6, Lcom/twitter/model/timeline/f$a;->c:I

    .line 1190
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v13, v14

    .line 1189
    invoke-virtual {v10, v11, v12, v13}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1194
    :goto_3
    iput-wide v8, v7, Lcom/twitter/android/cf$b;->c:J

    .line 1195
    iget-object v8, v7, Lcom/twitter/android/cf$b;->b:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1196
    iget-object v6, v7, Lcom/twitter/android/cf$b;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 1197
    invoke-static {}, Lbpi;->b()I

    move-result v8

    iput v8, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1198
    iget-object v6, v7, Lcom/twitter/android/cf$b;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->requestLayout()V

    goto :goto_1

    .line 1192
    :cond_7
    const v6, 0x7f0a0981

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 1203
    :pswitch_3
    if-nez v8, :cond_8

    const/4 v6, 0x1

    move v8, v6

    .line 1204
    :goto_4
    if-nez v9, :cond_9

    const/4 v6, 0x1

    move v7, v6

    .line 1205
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cu;

    .line 1206
    check-cast p3, Lcom/twitter/android/timeline/cd;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)Lcom/twitter/model/core/Tweet;

    .line 1207
    iget-object v9, v6, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v9, v8, v7}, Lcom/twitter/library/widget/TweetView;->a(ZZ)V

    .line 1208
    iget-object v6, v6, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    goto/16 :goto_1

    .line 1203
    :cond_8
    const/4 v6, 0x0

    move v8, v6

    goto :goto_4

    .line 1204
    :cond_9
    const/4 v6, 0x0

    move v7, v6

    goto :goto_5

    .line 1212
    :pswitch_4
    check-cast p3, Lcom/twitter/android/timeline/bb;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v10}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/bb;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1216
    :pswitch_5
    invoke-static/range {p3 .. p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/timeline/cf;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/cf;)V

    goto/16 :goto_1

    .line 1221
    :pswitch_6
    check-cast p3, Lcom/twitter/android/timeline/ca;

    .line 1222
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cf$g;

    .line 1223
    iget-object v8, v6, Lcom/twitter/android/cf$g;->d:Lcom/twitter/android/cf$k;

    .line 1225
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 1227
    iget-object v10, v8, Lcom/twitter/android/cf$k;->d:Landroid/view/View;

    if-eqz v7, :cond_b

    const/4 v6, 0x4

    :goto_6
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1228
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    iget-object v7, v8, Lcom/twitter/android/cf$k;->e:Lcom/twitter/android/cf$h;

    iget-object v7, v7, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v6, v7}, Lcom/twitter/android/cf;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/TextView;)V

    .line 1230
    iget-object v6, v8, Lcom/twitter/android/cf$k;->c:Landroid/widget/TextView;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/twitter/android/timeline/ca;->i:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/twitter/android/cf;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1232
    iget-object v6, v8, Lcom/twitter/android/cf$k;->b:Landroid/widget/TextView;

    const v7, 0x7f0a06f4

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    iget-object v12, v12, Lcgi;->g:Ljava/lang/String;

    aput-object v12, v10, v11

    .line 1233
    invoke-virtual {v9, v7, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1232
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1235
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    invoke-virtual {v6}, Lcgi;->b()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1236
    iget-object v6, v8, Lcom/twitter/android/cf$k;->a:Landroid/widget/ImageView;

    const v7, 0x7f020841

    .line 1237
    invoke-virtual {v9, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1236
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1242
    :goto_7
    iget-object v6, v8, Lcom/twitter/android/cf$k;->f:Lcom/twitter/android/trends/TrendBadgesView;

    if-eqz v6, :cond_a

    .line 1243
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    if-nez v6, :cond_d

    const/4 v6, 0x0

    .line 1244
    :goto_8
    iget-object v7, v8, Lcom/twitter/android/cf$k;->f:Lcom/twitter/android/trends/TrendBadgesView;

    invoke-virtual {v7, v6}, Lcom/twitter/android/trends/TrendBadgesView;->setBadges(Ljava/util/List;)V

    .line 1246
    :cond_a
    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/twitter/android/timeline/ca;->a:Ljava/lang/String;

    move-object/from16 v6, p0

    move/from16 v7, p4

    move-object/from16 v8, p1

    invoke-direct/range {v6 .. v14}, Lcom/twitter/android/cf;->a(ILandroid/view/View;Lcgi;Ljava/lang/String;ZIZLjava/lang/String;)V

    goto/16 :goto_1

    .line 1227
    :cond_b
    const/4 v6, 0x0

    goto :goto_6

    .line 1239
    :cond_c
    iget-object v6, v8, Lcom/twitter/android/cf$k;->a:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/cf;->w:I

    .line 1240
    invoke-virtual {v9, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1239
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    .line 1243
    :cond_d
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-object v6, v6, Lcom/twitter/model/topic/e;->l:Ljava/util/List;

    goto :goto_8

    .line 1251
    :pswitch_7
    check-cast p3, Lcom/twitter/android/timeline/ca;

    .line 1253
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cf$g;

    .line 1254
    iget-object v7, v6, Lcom/twitter/android/cf$g;->e:Lcom/twitter/android/cf$j;

    .line 1256
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    if-eqz v6, :cond_e

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-object v6, v6, Lcom/twitter/model/topic/e;->h:Ljava/lang/String;

    .line 1259
    :goto_9
    iget-object v8, v7, Lcom/twitter/android/cf$j;->b:Lcom/twitter/android/cf$h;

    iget-object v8, v8, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    .line 1260
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1261
    iget-object v7, v7, Lcom/twitter/android/cf$j;->a:Landroid/widget/TextView;

    invoke-static {v7, v6}, Lcom/twitter/android/cf;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1262
    const/4 v9, 0x0

    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/twitter/android/timeline/ca;->a:Ljava/lang/String;

    move-object/from16 v6, p0

    move/from16 v7, p4

    move-object/from16 v8, p1

    invoke-direct/range {v6 .. v14}, Lcom/twitter/android/cf;->a(ILandroid/view/View;Lcgi;Ljava/lang/String;ZIZLjava/lang/String;)V

    goto/16 :goto_1

    .line 1256
    :cond_e
    const/4 v6, 0x0

    goto :goto_9

    .line 1267
    :pswitch_8
    check-cast p3, Lcom/twitter/android/timeline/ca;

    .line 1269
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cf$g;

    .line 1270
    iget-object v6, v6, Lcom/twitter/android/cf$g;->f:Lcom/twitter/android/cf$i;

    .line 1272
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 1274
    iget-object v8, v6, Lcom/twitter/android/cf$i;->c:Lcom/twitter/android/cf$h;

    iget-object v8, v8, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277
    iget-object v8, v6, Lcom/twitter/android/cf$i;->b:Landroid/widget/TextView;

    const v9, 0x7f0a06f4

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    iget-object v12, v12, Lcgi;->g:Ljava/lang/String;

    aput-object v12, v10, v11

    .line 1278
    invoke-virtual {v7, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 1277
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1280
    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    invoke-virtual {v8}, Lcgi;->b()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 1281
    iget-object v6, v6, Lcom/twitter/android/cf$i;->a:Landroid/widget/ImageView;

    const v8, 0x7f020841

    .line 1282
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1281
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1287
    :goto_a
    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/twitter/android/timeline/ca;->a:Ljava/lang/String;

    move-object/from16 v6, p0

    move/from16 v7, p4

    move-object/from16 v8, p1

    invoke-direct/range {v6 .. v14}, Lcom/twitter/android/cf;->a(ILandroid/view/View;Lcgi;Ljava/lang/String;ZIZLjava/lang/String;)V

    goto/16 :goto_1

    .line 1284
    :cond_f
    iget-object v6, v6, Lcom/twitter/android/cf$i;->a:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/android/cf;->w:I

    .line 1285
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1284
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_a

    .line 1292
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->T:Lcom/twitter/android/timeline/ch;

    check-cast p3, Lcom/twitter/android/timeline/cg;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v6, v1, v2}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 1297
    :pswitch_a
    check-cast p3, Lcom/twitter/android/timeline/ca;

    .line 1298
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cf$g;

    .line 1299
    iget-object v11, v6, Lcom/twitter/android/cf$g;->c:Lcom/twitter/android/cf$l;

    .line 1307
    iget-object v8, v11, Lcom/twitter/android/cf$l;->h:Landroid/view/View;

    if-eqz v7, :cond_11

    const/4 v6, 0x4

    :goto_b
    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1309
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    if-eqz v6, :cond_12

    .line 1310
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget v8, v6, Lcom/twitter/model/topic/e;->i:I

    .line 1311
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-object v10, v6, Lcom/twitter/model/topic/e;->h:Ljava/lang/String;

    .line 1312
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-object v6, v6, Lcom/twitter/model/topic/e;->c:Lcom/twitter/model/topic/trends/a;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/twitter/android/cf;->a(Lcom/twitter/model/topic/trends/a;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 1313
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-object v6, v6, Lcom/twitter/model/topic/e;->f:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/twitter/android/cf;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 1314
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget-object v6, v6, Lcom/twitter/model/topic/e;->l:Ljava/util/List;

    .line 1315
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    iget v12, v12, Lcom/twitter/model/topic/e;->g:I

    .line 1325
    :goto_c
    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    iget-object v14, v11, Lcom/twitter/android/cf$l;->e:Lcom/twitter/android/cf$h;

    iget-object v14, v14, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v13, v14}, Lcom/twitter/android/cf;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/TextView;)V

    .line 1326
    iget-object v13, v11, Lcom/twitter/android/cf$l;->a:Landroid/widget/TextView;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v13, v8}, Lcom/twitter/android/cf;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1327
    iget-object v8, v11, Lcom/twitter/android/cf$l;->g:Landroid/widget/TextView;

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/twitter/android/timeline/ca;->i:Ljava/lang/String;

    invoke-static {v8, v13}, Lcom/twitter/android/cf;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1328
    iget-object v8, v11, Lcom/twitter/android/cf$l;->b:Landroid/widget/TextView;

    invoke-static {v8, v10}, Lcom/twitter/android/cf;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1329
    iget-object v8, v11, Lcom/twitter/android/cf$l;->e:Lcom/twitter/android/cf$h;

    iget-object v8, v8, Lcom/twitter/android/cf$h;->b:Landroid/widget/TextView;

    const/4 v10, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v8, v10, v13, v9, v14}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1331
    iget-object v8, v11, Lcom/twitter/android/cf$l;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, Lcom/twitter/library/media/widget/TweetMediaView;->a(Ljava/lang/Iterable;Ljava/lang/String;)V

    .line 1332
    iget-object v8, v11, Lcom/twitter/android/cf$l;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    const v9, 0x7f0a0964

    invoke-virtual {v8, v9}, Lcom/twitter/library/media/widget/TweetMediaView;->setBadgeText(I)V

    .line 1333
    iget-object v8, v11, Lcom/twitter/android/cf$l;->i:Lcom/twitter/android/trends/TrendBadgesView;

    if-eqz v8, :cond_10

    .line 1334
    iget-object v8, v11, Lcom/twitter/android/cf$l;->i:Lcom/twitter/android/trends/TrendBadgesView;

    invoke-virtual {v8, v6}, Lcom/twitter/android/trends/TrendBadgesView;->setBadges(Ljava/util/List;)V

    .line 1336
    :cond_10
    invoke-static {v7}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v6

    if-nez v6, :cond_13

    .line 1337
    iget-object v6, v11, Lcom/twitter/android/cf$l;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/twitter/library/media/widget/TweetMediaView;->setVisibility(I)V

    .line 1338
    iget-object v6, v11, Lcom/twitter/android/cf$l;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setVisibility(I)V

    .line 1339
    iget-object v6, v11, Lcom/twitter/android/cf$l;->f:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1345
    :goto_d
    const/4 v9, 0x0

    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-boolean v11, v0, Lcom/twitter/android/timeline/ca;->k:Z

    const/4 v13, 0x0

    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/twitter/android/timeline/ca;->a:Ljava/lang/String;

    move-object/from16 v6, p0

    move/from16 v7, p4

    move-object/from16 v8, p1

    invoke-direct/range {v6 .. v14}, Lcom/twitter/android/cf;->a(ILandroid/view/View;Lcgi;Ljava/lang/String;ZIZLjava/lang/String;)V

    goto/16 :goto_1

    .line 1307
    :cond_11
    const/4 v6, 0x0

    goto/16 :goto_b

    .line 1317
    :cond_12
    const/4 v8, -0x1

    .line 1318
    const/4 v10, 0x0

    .line 1319
    const/4 v9, 0x0

    .line 1320
    const/4 v7, 0x0

    .line 1321
    const/4 v6, 0x0

    .line 1322
    const/4 v12, 0x0

    goto/16 :goto_c

    .line 1341
    :cond_13
    iget-object v6, v11, Lcom/twitter/android/cf$l;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/twitter/library/media/widget/TweetMediaView;->setVisibility(I)V

    .line 1342
    iget-object v6, v11, Lcom/twitter/android/cf$l;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setVisibility(I)V

    .line 1343
    iget-object v6, v11, Lcom/twitter/android/cf$l;->f:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_d

    .line 1350
    :pswitch_b
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cf$g;

    .line 1351
    iget-object v6, v6, Lcom/twitter/android/cf$g;->h:Lcom/twitter/android/cb;

    .line 1354
    iget-object v6, v6, Lcom/twitter/android/cb;->c:Landroid/widget/TextView;

    const v7, 0x7f0a0107

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 1359
    :pswitch_c
    const/16 v6, 0xa

    if-ne v15, v6, :cond_14

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/android/cf;->A:Z

    if-nez v6, :cond_14

    .line 1360
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/twitter/android/cf;->A:Z

    .line 1362
    :cond_14
    check-cast p3, Lcom/twitter/android/timeline/n;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/n;)V

    goto/16 :goto_1

    .line 1369
    :pswitch_d
    move-object/from16 v0, p3

    instance-of v6, v0, Lcom/twitter/android/timeline/an;

    if-eqz v6, :cond_15

    move-object/from16 v6, p3

    .line 1370
    check-cast v6, Lcom/twitter/android/timeline/an;

    .line 1371
    iget-object v6, v6, Lcom/twitter/android/timeline/an;->a:Lcom/twitter/model/timeline/o;

    iget-object v9, v6, Lcom/twitter/model/timeline/o;->c:Ljava/lang/String;

    .line 1372
    const/4 v10, 0x0

    .line 1373
    const v11, 0x7f0a0743

    :goto_e
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p3

    .line 1389
    invoke-direct/range {v6 .. v11}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/bk;Ljava/lang/String;Lcom/twitter/model/timeline/j;I)V

    goto/16 :goto_1

    .line 1374
    :cond_15
    move-object/from16 v0, p3

    instance-of v6, v0, Lcom/twitter/android/timeline/cq;

    if-eqz v6, :cond_16

    move-object/from16 v6, p3

    .line 1375
    check-cast v6, Lcom/twitter/android/timeline/cq;

    .line 1376
    iget-object v7, v6, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v9, v7, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    .line 1377
    const v11, 0x7f0a0a3e

    .line 1378
    iget-object v6, v6, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v10, v6, Lcom/twitter/model/timeline/at$a;->j:Lcom/twitter/model/timeline/j;

    goto :goto_e

    .line 1379
    :cond_16
    move-object/from16 v0, p3

    instance-of v6, v0, Lcom/twitter/android/timeline/z;

    if-eqz v6, :cond_17

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/bk;->f:Lcha;

    if-eqz v6, :cond_17

    .line 1380
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/bk;->f:Lcha;

    iget-object v9, v6, Lcha;->b:Ljava/lang/String;

    .line 1381
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/bk;->f:Lcha;

    iget-object v10, v6, Lcha;->d:Lcom/twitter/model/timeline/j;

    .line 1384
    const/4 v11, 0x0

    goto :goto_e

    .line 1386
    :cond_17
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Attempting to bind an unexpected module header from TimelineItem: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1387
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1393
    :pswitch_e
    check-cast p3, Lcom/twitter/android/timeline/ap;

    .line 1394
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-virtual {v0, v1, v6, v2}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)Lcom/twitter/model/core/Tweet;

    .line 1395
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cu;

    .line 1396
    iget-object v6, v6, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    goto/16 :goto_1

    .line 1400
    :pswitch_f
    check-cast p3, Lcom/twitter/android/timeline/cd;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)Lcom/twitter/model/core/Tweet;

    .line 1401
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cu;

    .line 1402
    iget-object v6, v6, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    goto/16 :goto_1

    .line 1409
    :pswitch_10
    invoke-static/range {p3 .. p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/timeline/a;

    .line 1410
    const-string/jumbo v7, "ad_slot_id"

    iget-object v6, v6, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    invoke-virtual {v10, v7, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->g:Lcom/twitter/android/av;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-interface {v6, v0, v7, v10}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1417
    :pswitch_11
    check-cast p3, Lcom/twitter/android/timeline/cq;

    .line 1418
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cf$g;

    .line 1419
    iget-object v6, v6, Lcom/twitter/android/cf$g;->j:Lcom/twitter/android/widget/WhoToFollowUsersView;

    .line 1421
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/cf;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/cf;->G:Lcom/twitter/android/timeline/cu;

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v6, v0, v7, v1, v8}, Lcom/twitter/android/widget/WhoToFollowUsersView;->a(Lcom/twitter/android/timeline/cq;Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/android/timeline/cu;)V

    .line 1422
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->F:Lcom/twitter/android/timeline/cs;

    if-eqz v6, :cond_3

    .line 1423
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->F:Lcom/twitter/android/timeline/cs;

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v6, v0, v1}, Lcom/twitter/android/timeline/cs;->a(Lcom/twitter/android/timeline/cq;I)V

    goto/16 :goto_1

    .line 1428
    :pswitch_12
    move-object/from16 v0, p3

    instance-of v6, v0, Lcom/twitter/android/timeline/cq;

    if-eqz v6, :cond_18

    .line 1429
    check-cast p3, Lcom/twitter/android/timeline/cq;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v10, v3}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/cq;Landroid/os/Bundle;I)V

    goto/16 :goto_1

    .line 1430
    :cond_18
    move-object/from16 v0, p3

    instance-of v6, v0, Lcom/twitter/android/timeline/bb;

    if-eqz v6, :cond_3

    .line 1431
    check-cast p3, Lcom/twitter/android/timeline/bb;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v10}, Lcom/twitter/android/cf;->b(Landroid/view/View;Lcom/twitter/android/timeline/bb;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1436
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v6}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    move-object/from16 v9, p3

    .line 1437
    check-cast v9, Lcom/twitter/android/timeline/ax;

    move-object/from16 v6, p0

    move-object/from16 v8, p1

    move/from16 v11, p4

    invoke-direct/range {v6 .. v11}, Lcom/twitter/android/cf;->a(Landroid/content/res/Resources;Landroid/view/View;Lcom/twitter/android/timeline/ax;Landroid/os/Bundle;I)V

    goto/16 :goto_1

    .line 1441
    :pswitch_14
    check-cast p3, Lcom/twitter/android/timeline/a;

    .line 1442
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->K:Lcom/twitter/android/revenue/c;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/twitter/android/revenue/c;->a(Ljava/lang/String;)Lcom/twitter/android/revenue/a;

    move-result-object v7

    .line 1443
    if-eqz v7, :cond_19

    const/4 v6, 0x1

    :goto_f
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Dismissing filled ad slot: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " but AdPool is not returning valid ad anymore"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/twitter/util/g;->a(ZLjava/lang/String;)Z

    .line 1445
    if-eqz v7, :cond_3

    .line 1446
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->M:Lcom/twitter/android/timeline/b;

    move-object/from16 v0, p3

    invoke-virtual {v6, v0, v7}, Lcom/twitter/android/timeline/b;->a(Lcom/twitter/android/timeline/a;Lcom/twitter/android/revenue/a;)Lcom/twitter/android/timeline/bk;

    move-result-object v6

    .line 1447
    if-eqz v6, :cond_3

    .line 1448
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/cf;->q:Lcom/twitter/android/av;

    move-object/from16 v0, p1

    invoke-interface {v7, v0, v6, v10}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1443
    :cond_19
    const/4 v6, 0x0

    goto :goto_f

    .line 1454
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->q:Lcom/twitter/android/av;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-interface {v6, v0, v1, v10}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1458
    :pswitch_16
    check-cast p3, Lcom/twitter/android/timeline/bq;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/bq;I)V

    goto/16 :goto_1

    .line 1462
    :pswitch_17
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/timeline/bk;Landroid/view/View;Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 1466
    :pswitch_18
    check-cast p3, Lcom/twitter/android/timeline/x;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/x;I)V

    goto/16 :goto_1

    .line 1470
    :pswitch_19
    check-cast p3, Lcom/twitter/android/timeline/az;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/cf;->a(Landroid/view/View;Lcom/twitter/android/timeline/bk;I)V

    goto/16 :goto_1

    .line 1474
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/cf;->V:Lcom/twitter/android/timeline/ag;

    check-cast p3, Lcom/twitter/android/timeline/af;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v6, v1, v2}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 1485
    :cond_1a
    invoke-super/range {p0 .. p4}, Lcom/twitter/android/y;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V

    .line 1486
    move-object/from16 v0, p3

    instance-of v6, v0, Lcom/twitter/android/timeline/k;

    if-eqz v6, :cond_1b

    .line 1487
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/cu;

    .line 1488
    iget-object v6, v6, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    .line 1489
    invoke-virtual {v6}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    .line 1491
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    .line 1495
    :cond_1b
    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/cf;->i:I

    if-nez v6, :cond_3

    move-object/from16 v0, p3

    instance-of v6, v0, Lcom/twitter/android/timeline/aq;

    if-eqz v6, :cond_3

    .line 1496
    new-instance v6, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/cf;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v7}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string/jumbo v9, "home::gap::impression"

    aput-object v9, v7, v8

    .line 1497
    invoke-virtual {v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v6

    .line 1496
    invoke-static {v6}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_1

    .line 1511
    :cond_1c
    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v6, :cond_5

    .line 1512
    check-cast p1, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 1514
    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/twitter/android/cf;->a(Lcom/twitter/internal/android/widget/GroupedRowView;ZZ)V

    goto/16 :goto_2

    .line 1178
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_18
        :pswitch_b
        :pswitch_1
        :pswitch_a
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_6
        :pswitch_a
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_4
        :pswitch_f
        :pswitch_e
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_13
        :pswitch_10
        :pswitch_0
        :pswitch_5
        :pswitch_14
        :pswitch_9
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 1123
    iput-boolean p1, p0, Lcom/twitter/android/cf;->A:Z

    .line 1124
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1119
    iget-boolean v0, p0, Lcom/twitter/android/cf;->A:Z

    return v0
.end method

.method public c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1903
    invoke-virtual {p0}, Lcom/twitter/android/cf;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    .line 1904
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1905
    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bl;->i(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 370
    if-eqz v0, :cond_0

    .line 371
    invoke-virtual {p0, v0}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    .line 373
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1128
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/cf$c;

    if-eqz v0, :cond_0

    .line 1129
    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->getItemViewType(I)I

    move-result v0

    .line 1130
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    .line 1131
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "cursor moved"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 1132
    const/4 v0, 0x0

    invoke-super {p0, p1, v0, p3}, Lcom/twitter/android/y;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1136
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/y;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 364
    const/16 v0, 0x22

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    .line 1035
    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->getItemViewType(I)I

    move-result v0

    .line 1036
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    .line 1038
    :cond_0
    const/4 v0, 0x0

    .line 1046
    :goto_0
    return v0

    .line 1040
    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/cf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 1041
    iget-object v1, p0, Lcom/twitter/android/cf;->W:Lckd;

    invoke-interface {v1, v0}, Lckd;->a(Ljava/lang/Object;)I

    move-result v1

    .line 1042
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1043
    iget-object v2, p0, Lcom/twitter/android/cf;->W:Lckd;

    invoke-interface {v2, v1}, Lckd;->a(I)Lckb;

    move-result-object v1

    .line 1044
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lckb;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1046
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/android/y;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 352
    invoke-static {p1}, Lckc;->a(Landroid/view/View;)V

    .line 353
    return-void
.end method
