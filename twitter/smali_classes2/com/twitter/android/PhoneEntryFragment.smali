.class public Lcom/twitter/android/PhoneEntryFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/android/ba$a;
.implements Lcom/twitter/android/util/SpannableTextUtil$a;
.implements Lcom/twitter/android/util/s$b;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/ui/widget/TwitterSelection$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/PhoneEntryFragment$b;,
        Lcom/twitter/android/PhoneEntryFragment$a;
    }
.end annotation


# static fields
.field private static final b:[I

.field private static final c:[I


# instance fields
.field a:Lcom/twitter/ui/widget/TwitterEditText;

.field private d:Lcom/twitter/ui/widget/TwitterSelection;

.field private e:Lcom/twitter/android/o;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/twitter/ui/widget/PromptView;

.field private l:Landroid/widget/TextView;

.field private m:Lcom/twitter/android/ValidationState$a;

.field private n:Lcom/twitter/android/au;

.field private o:Lcom/twitter/android/PhoneEntryFragment$a;

.field private p:Lcom/twitter/android/by;

.field private q:Lcom/twitter/android/ba;

.field private r:Lcom/twitter/android/util/s;

.field private s:Lcom/twitter/android/PhoneEntryFragment$b;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;

.field private w:Landroid/view/View;

.field private x:Lcom/twitter/android/w;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/PhoneEntryFragment;->b:[I

    .line 64
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010471

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/android/PhoneEntryFragment;->c:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method

.method private a(Lcom/twitter/ui/widget/TwitterEditText;I)Lcom/twitter/android/ValidationState$State;
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/PhoneEntryFragment;->a(Lcom/twitter/ui/widget/TwitterEditText;Z)V

    .line 248
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 249
    invoke-virtual {p1}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    .line 250
    if-lt v0, p2, :cond_0

    .line 251
    sget-object v0, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    .line 253
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/android/ValidationState$State;->a:Lcom/twitter/android/ValidationState$State;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/PhoneEntryFragment;)Lcom/twitter/android/util/s;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    return-object v0
.end method

.method private a(Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->j()Ljava/lang/String;

    move-result-object v0

    .line 401
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    iget-object v2, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    .line 402
    invoke-interface {v2, v0}, Lcom/twitter/android/util/s;->b(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v0

    .line 401
    invoke-interface {v1, v0, p1}, Lcom/twitter/android/util/s;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/ui/widget/TwitterEditText;Z)V
    .locals 1

    .prologue
    .line 258
    if-eqz p2, :cond_0

    sget-object v0, Lcom/twitter/android/PhoneEntryFragment;->c:[I

    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    .line 259
    return-void

    .line 258
    :cond_0
    sget-object v0, Lcom/twitter/android/PhoneEntryFragment;->b:[I

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/PhoneEntryFragment;)Lcom/twitter/android/ValidationState$a;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    return-object v0
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 291
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->f:Z

    if-eqz v0, :cond_0

    .line 292
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v0

    .line 294
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p1, v1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 295
    iget-object v2, p0, Lcom/twitter/android/PhoneEntryFragment;->d:Lcom/twitter/ui/widget/TwitterSelection;

    iget-object v3, p0, Lcom/twitter/android/PhoneEntryFragment;->e:Lcom/twitter/android/o;

    .line 296
    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;->b()I

    move-result v4

    invoke-static {v4}, Lcom/twitter/android/util/d;->a(I)Lcom/twitter/android/n;

    move-result-object v4

    .line 295
    invoke-virtual {v3, v4}, Lcom/twitter/android/o;->a(Lcom/twitter/android/n;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TwitterSelection;->setSelectedPosition(I)V

    .line 298
    sget-object v2, Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->c:Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    invoke-virtual {v0, v1, v2}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/i18n/phonenumbers/NumberParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 304
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    .line 305
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0403ef

    invoke-direct {v1, v0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 308
    invoke-virtual {v1, p1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 309
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 310
    new-instance v1, Lcom/twitter/android/util/a;

    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-direct {v1, v0}, Lcom/twitter/android/util/a;-><init>(Lcom/twitter/internal/android/widget/PopupEditText;)V

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 314
    return-void

    .line 299
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/PhoneEntryFragment;)Lcom/twitter/android/au;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->n:Lcom/twitter/android/au;

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 142
    new-instance v0, Lcom/twitter/android/o;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/util/d;->a()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/o;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->e:Lcom/twitter/android/o;

    .line 143
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->d:Lcom/twitter/ui/widget/TwitterSelection;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->e:Lcom/twitter/android/o;

    .line 144
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/util/d;->a(Landroid/content/Context;)Lcom/twitter/android/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/o;->a(Lcom/twitter/android/n;)I

    move-result v1

    .line 143
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterSelection;->setSelectedPosition(I)V

    .line 145
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->d:Lcom/twitter/ui/widget/TwitterSelection;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->e:Lcom/twitter/android/o;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterSelection;->setSelectionAdapter(Lcom/twitter/ui/widget/g;)V

    .line 146
    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 231
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->j:Z

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "phone100_enter_phone"

    aput-object v3, v1, v2

    const-string/jumbo v2, "form"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 234
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    .line 235
    invoke-interface {v3}, Lcom/twitter/android/util/s;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "change"

    aput-object v3, v1, v2

    .line 233
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 232
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    .line 238
    invoke-direct {p0, v0, v4}, Lcom/twitter/android/PhoneEntryFragment;->a(Lcom/twitter/ui/widget/TwitterEditText;I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    .line 239
    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_1

    .line 240
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->s:Lcom/twitter/android/PhoneEntryFragment$b;

    invoke-virtual {v0, v4}, Lcom/twitter/android/PhoneEntryFragment$b;->a(I)V

    .line 244
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->s:Lcom/twitter/android/PhoneEntryFragment$b;

    invoke-virtual {v0, v4}, Lcom/twitter/android/PhoneEntryFragment$b;->removeMessages(I)V

    goto :goto_0
.end method

.method private m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->a:Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntryFragment;->a(Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 407
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/util/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 408
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    invoke-interface {v0}, Lcom/twitter/android/util/s;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a08cb

    .line 410
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/twitter/android/util/SpannableTextUtil;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v1

    .line 412
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v4}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v2, 0x7f0a08cc

    .line 413
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0616

    .line 414
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0335

    .line 415
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 416
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 417
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 418
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 419
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 420
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 422
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "phone100_enter_phone"

    aput-object v3, v1, v2

    const-string/jumbo v2, "sms_confirm_dialog"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 424
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 423
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 422
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 425
    return-void

    .line 408
    :cond_0
    const v0, 0x7f0a08ca

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->k:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/PromptView;->setTitle(I)V

    .line 323
    return-void
.end method

.method public a(IZ)V
    .locals 4

    .prologue
    const v3, 0x7f0d02d2

    .line 364
    if-eqz p2, :cond_0

    .line 365
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->u:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/android/util/SpannableTextUtil;->a(Landroid/content/Context;Landroid/widget/TextView;IZ)V

    .line 366
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 371
    :goto_0
    return-void

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 369
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    .prologue
    const/4 v0, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 450
    if-ne p2, v5, :cond_2

    .line 452
    if-ne p3, v0, :cond_1

    .line 453
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->o:Lcom/twitter/android/PhoneEntryFragment$a;

    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryFragment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/PhoneEntryFragment$a;->a(Ljava/lang/String;)V

    .line 454
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->o:Lcom/twitter/android/PhoneEntryFragment$a;

    invoke-interface {v0, v4}, Lcom/twitter/android/PhoneEntryFragment$a;->c(Z)V

    .line 455
    const-string/jumbo v0, "ok"

    .line 459
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "phone100_enter_phone"

    aput-object v3, v2, v4

    const-string/jumbo v3, "sms_confirm_dialog"

    aput-object v3, v2, v5

    iget-boolean v3, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 461
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    aput-object v7, v2, v3

    const/4 v3, 0x4

    aput-object v0, v2, v3

    .line 460
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 459
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 475
    :cond_0
    :goto_1
    return-void

    .line 457
    :cond_1
    const-string/jumbo v0, "cancel"

    goto :goto_0

    .line 462
    :cond_2
    if-ne p2, v6, :cond_0

    .line 464
    if-ne p3, v0, :cond_3

    .line 465
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->o:Lcom/twitter/android/PhoneEntryFragment$a;

    invoke-interface {v0, v7}, Lcom/twitter/android/PhoneEntryFragment$a;->a(Ljava/lang/String;)V

    .line 466
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->n:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->bl_()V

    .line 467
    const-string/jumbo v0, "ok"

    .line 471
    :goto_2
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "phone100_phone_optional"

    aput-object v3, v2, v4

    const-string/jumbo v3, "skip_confirm_dialog"

    aput-object v3, v2, v5

    iget-boolean v3, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 473
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    aput-object v7, v2, v3

    const/4 v3, 0x4

    aput-object v0, v2, v3

    .line 472
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 471
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    .line 469
    :cond_3
    const-string/jumbo v0, "cancel"

    goto :goto_2
.end method

.method public a(Lcom/twitter/android/w;)V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p1, Lcom/twitter/android/w;->c:Ljava/lang/String;

    .line 385
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->Y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 386
    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setError(Ljava/lang/CharSequence;)V

    .line 387
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->x:Lcom/twitter/android/w;

    .line 391
    :goto_0
    return-void

    .line 389
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/PhoneEntryFragment;->x:Lcom/twitter/android/w;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/n;)V
    .locals 6

    .prologue
    const v0, 0x7f0a08c4

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 510
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->Y()Z

    move-result v2

    if-nez v2, :cond_0

    .line 546
    :goto_0
    return-void

    .line 515
    :cond_0
    if-nez p1, :cond_1

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->g()Z

    move-result v2

    if-nez v2, :cond_1

    .line 516
    sget-object v2, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    .line 517
    const v0, 0x7f0a08c0

    .line 534
    :goto_1
    iget-object v5, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    sget-object v3, Lcom/twitter/android/ValidationState$State;->c:Lcom/twitter/android/ValidationState$State;

    if-ne v2, v3, :cond_8

    move v3, v4

    :goto_2
    invoke-direct {p0, v5, v3}, Lcom/twitter/android/PhoneEntryFragment;->a(Lcom/twitter/ui/widget/TwitterEditText;Z)V

    .line 535
    if-lez v0, :cond_9

    .line 536
    iget-object v3, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v3, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 541
    :goto_3
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_a

    .line 542
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    new-instance v1, Lcom/twitter/android/ValidationState;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->b:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    goto :goto_0

    .line 518
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/api/n;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 519
    :cond_2
    sget-object v0, Lcom/twitter/android/ValidationState$State;->c:Lcom/twitter/android/ValidationState$State;

    move-object v2, v0

    move v0, v1

    .line 520
    goto :goto_1

    .line 521
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/library/api/n;->c()Z

    move-result v2

    if-nez v2, :cond_4

    .line 522
    const v0, 0x7f0a08c3

    .line 523
    sget-object v2, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    goto :goto_1

    .line 524
    :cond_4
    invoke-virtual {p1}, Lcom/twitter/library/api/n;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 525
    iget-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->i:Z

    if-eqz v2, :cond_5

    .line 527
    :goto_4
    sget-object v2, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    goto :goto_1

    .line 525
    :cond_5
    const v0, 0x7f0a0664

    goto :goto_4

    .line 529
    :cond_6
    iget-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->i:Z

    if-eqz v2, :cond_7

    .line 531
    :goto_5
    sget-object v2, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    goto :goto_1

    .line 529
    :cond_7
    const v0, 0x7f0a0656

    goto :goto_5

    :cond_8
    move v3, v1

    .line 534
    goto :goto_2

    .line 538
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    goto :goto_3

    .line 544
    :cond_a
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->n:Lcom/twitter/android/au;

    sget-object v3, Lcom/twitter/android/ValidationState$State;->c:Lcom/twitter/android/ValidationState$State;

    if-ne v2, v3, :cond_b

    :goto_6
    invoke-interface {v0, v4}, Lcom/twitter/android/au;->a(Z)V

    goto :goto_0

    :cond_b
    move v4, v1

    goto :goto_6
.end method

.method public a(Lcom/twitter/ui/widget/TwitterSelection;I)V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryFragment;->l()V

    .line 227
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->k:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/PromptView;->setTitle(Ljava/lang/CharSequence;)V

    .line 328
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/PhoneEntryFragment;->b(Ljava/lang/String;Z)V

    .line 318
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 275
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->requestFocus()Z

    .line 277
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->i:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 279
    iput-boolean v4, p0, Lcom/twitter/android/PhoneEntryFragment;->j:Z

    .line 280
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "phone100_enter_phone"

    aput-object v3, v1, v2

    const-string/jumbo v2, "form"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 282
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    .line 283
    invoke-interface {v3}, Lcom/twitter/android/util/s;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "prefill"

    aput-object v3, v1, v2

    .line 281
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 280
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 286
    :cond_0
    invoke-direct {p0, p1, v4}, Lcom/twitter/android/PhoneEntryFragment;->b(Ljava/lang/String;Z)V

    .line 288
    :cond_1
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 353
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 360
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/util/Collection;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/PhoneEntryFragment;->l:Landroid/widget/TextView;

    invoke-static {v0, v1, v2, p0}, Lcom/twitter/android/util/SpannableTextUtil;->a(Landroid/content/Context;[ILandroid/widget/TextView;Lcom/twitter/android/util/SpannableTextUtil$a;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 375
    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->t:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 376
    return-void

    .line 375
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public a(ZII)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 337
    if-eqz p1, :cond_0

    .line 338
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->k:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/PromptView;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->v:Landroid/view/View;

    const v1, 0x7f1303a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 342
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->v:Landroid/view/View;

    const v1, 0x7f1307f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 343
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 344
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->k:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/PromptView;->setVisibility(I)V

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 220
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryFragment;->l()V

    .line 222
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 150
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 151
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->h:Z

    if-eqz v0, :cond_6

    .line 152
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 153
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->f:Z

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->d:Lcom/twitter/ui/widget/TwitterSelection;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterSelection;->setOnSelectionChangeListener(Lcom/twitter/ui/widget/TwitterSelection$c;)V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    invoke-interface {v0}, Lcom/twitter/android/ValidationState$a;->e()Lcom/twitter/android/ValidationState;

    move-result-object v0

    .line 162
    :goto_0
    if-eqz v0, :cond_5

    .line 163
    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 164
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/PhoneEntryFragment;->a(Lcom/twitter/ui/widget/TwitterEditText;Z)V

    .line 180
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->x:Lcom/twitter/android/w;

    if-eqz v0, :cond_2

    .line 181
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->x:Lcom/twitter/android/w;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryFragment;->a(Lcom/twitter/android/w;)V

    .line 183
    :cond_2
    return-void

    .line 160
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 165
    :cond_4
    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/PhoneEntryFragment;->a(Lcom/twitter/ui/widget/TwitterEditText;I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_1

    .line 167
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->s:Lcom/twitter/android/PhoneEntryFragment$b;

    invoke-virtual {v0, v2}, Lcom/twitter/android/PhoneEntryFragment$b;->a(I)V

    goto :goto_1

    .line 170
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->s:Lcom/twitter/android/PhoneEntryFragment$b;

    invoke-virtual {v0, v2}, Lcom/twitter/android/PhoneEntryFragment$b;->a(I)V

    goto :goto_1

    .line 173
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_7

    .line 174
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    new-instance v1, Lcom/twitter/android/ValidationState;

    sget-object v2, Lcom/twitter/android/ValidationState$State;->c:Lcom/twitter/android/ValidationState$State;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    goto :goto_1

    .line 177
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->n:Lcom/twitter/android/au;

    invoke-interface {v0, v2}, Lcom/twitter/android/au;->a(Z)V

    goto :goto_1
.end method

.method public b(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 494
    const v0, 0x7f0a0884

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 495
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "phone100_enter_phone"

    aput-object v2, v1, v4

    const-string/jumbo v2, "form"

    aput-object v2, v1, v5

    iget-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 496
    invoke-static {v2}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string/jumbo v2, "settings"

    aput-object v2, v1, v7

    const-string/jumbo v2, "click"

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 495
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 499
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->o:Lcom/twitter/android/PhoneEntryFragment$a;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->q:Lcom/twitter/android/ba;

    invoke-virtual {v1}, Lcom/twitter/android/ba;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/android/PhoneEntryFragment$a;->b(Z)V

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    const v0, 0x7f0a09cb

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "phone100_enter_phone"

    aput-object v2, v1, v4

    const-string/jumbo v2, "form"

    aput-object v2, v1, v5

    iget-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 503
    invoke-static {v2}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string/jumbo v2, "use_email_instead"

    aput-object v2, v1, v7

    const-string/jumbo v2, "click"

    aput-object v2, v1, v8

    .line 502
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 501
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 504
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->o:Lcom/twitter/android/PhoneEntryFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/PhoneEntryFragment$a;->O_()V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryFragment;->m()Ljava/lang/String;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 266
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryFragment;->n()V

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const v1, 0x7f0a08c3

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 332
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->w:Landroid/view/View;

    const v2, 0x7f0e0063

    invoke-static {v0, v1, v2}, Lcom/twitter/android/k;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 333
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 380
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 381
    return-void
.end method

.method protected g()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 429
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v4}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a036a

    .line 430
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0660

    .line 431
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a026a

    .line 432
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0269

    .line 433
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 434
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 435
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 436
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 437
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 439
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "phone100_phone_optional"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "skip_confirm_dialog"

    aput-object v3, v1, v2

    iget-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 441
    invoke-static {v2}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 440
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 439
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 442
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 478
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryFragment;->m()Ljava/lang/String;

    move-result-object v0

    .line 479
    if-eqz v0, :cond_1

    .line 480
    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 481
    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->o:Lcom/twitter/android/PhoneEntryFragment$a;

    invoke-interface {v1, v0}, Lcom/twitter/android/PhoneEntryFragment$a;->a(Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->p:Lcom/twitter/android/by;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->p:Lcom/twitter/android/by;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/twitter/android/by;->d(Z)V

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const v1, 0x7f0a0661

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0
.end method

.method protected i()Lcom/twitter/ui/widget/TwitterEditText;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    return-object v0
.end method

.method j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 554
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->f:Z

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->d:Lcom/twitter/ui/widget/TwitterSelection;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterSelection;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/n;

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/n;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 559
    :goto_1
    return-object v0

    .line 556
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    .line 559
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 196
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    .line 197
    check-cast v0, Lcom/twitter/android/PhoneEntryFragment$a;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->o:Lcom/twitter/android/PhoneEntryFragment$a;

    .line 199
    instance-of v0, p1, Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 200
    check-cast v0, Lcom/twitter/android/ValidationState$a;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    .line 202
    :cond_0
    instance-of v0, p1, Lcom/twitter/android/by;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 203
    check-cast v0, Lcom/twitter/android/by;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->p:Lcom/twitter/android/by;

    .line 206
    :cond_1
    check-cast p1, Lcom/twitter/android/au;

    iput-object p1, p0, Lcom/twitter/android/PhoneEntryFragment;->n:Lcom/twitter/android/au;

    .line 207
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    .line 99
    new-instance v0, Lcom/twitter/android/ba;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->r:Lcom/twitter/android/util/s;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ba;-><init>(Lcom/twitter/android/ba$a;Lcom/twitter/android/util/s;)V

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->q:Lcom/twitter/android/ba;

    .line 100
    new-instance v0, Lcom/twitter/android/PhoneEntryFragment$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/PhoneEntryFragment$b;-><init>(Lcom/twitter/android/PhoneEntryFragment;)V

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->s:Lcom/twitter/android/PhoneEntryFragment$b;

    .line 101
    const-string/jumbo v0, "mandatory_phone_signup_country_code_selection_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->f:Z

    .line 103
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const v3, 0x7f130631

    const/4 v4, 0x0

    .line 108
    const v0, 0x7f040297

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->w:Landroid/view/View;

    .line 109
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    .line 111
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->f:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 114
    const v0, 0x7f130632

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 115
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    .line 116
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    .line 117
    const v0, 0x7f130633

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterSelection;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->d:Lcom/twitter/ui/widget/TwitterSelection;

    .line 119
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryFragment;->k()V

    .line 122
    :cond_0
    const v0, 0x7f130630

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/PromptView;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->k:Lcom/twitter/ui/widget/PromptView;

    .line 123
    const v0, 0x7f1307f3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->v:Landroid/view/View;

    .line 125
    const v0, 0x7f130366

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->l:Landroid/widget/TextView;

    .line 127
    const v0, 0x7f13011d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->t:Landroid/view/View;

    .line 128
    const v0, 0x7f13016a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->u:Landroid/widget/TextView;

    .line 130
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    .line 131
    const-string/jumbo v2, "extra_inline_validation_enabled"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->h:Z

    .line 132
    const-string/jumbo v2, "is_phone_signup"

    invoke-virtual {v0, v2, v4}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->i:Z

    .line 133
    invoke-static {v0}, Lcom/twitter/android/bw;->a(Lcom/twitter/app/common/base/b;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/PhoneEntryFragment;->g:Z

    .line 134
    iget-object v2, p0, Lcom/twitter/android/PhoneEntryFragment;->q:Lcom/twitter/android/ba;

    invoke-virtual {v2, v0}, Lcom/twitter/android/ba;->a(Lcom/twitter/app/common/base/b;)V

    .line 136
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 138
    return-object v1
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 564
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    const v1, 0x7f130631

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->n:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->n_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->n:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->b()V

    .line 566
    const/4 v0, 0x1

    .line 568
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public q_()V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 188
    iget-boolean v0, p0, Lcom/twitter/android/PhoneEntryFragment;->h:Z

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/twitter/android/PhoneEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 191
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 192
    return-void
.end method
