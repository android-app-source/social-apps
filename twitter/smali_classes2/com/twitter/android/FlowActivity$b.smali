.class Lcom/twitter/android/FlowActivity$b;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/FlowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/FlowData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/FlowData;)V
    .locals 1

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 233
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/FlowActivity$b;->a:Ljava/lang/ref/WeakReference;

    .line 234
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 229
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/FlowActivity$b;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/twitter/android/FlowActivity$b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FlowData;

    .line 239
    if-nez v0, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    check-cast p1, Lbbh;

    .line 243
    invoke-virtual {p1}, Lbbh;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    invoke-virtual {p1}, Lbbh;->g()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    .line 245
    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lcbw;->c:Z

    if-eqz v1, :cond_0

    .line 246
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/FlowData;->f(Z)V

    goto :goto_0
.end method
