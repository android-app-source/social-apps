.class public Lcom/twitter/android/al;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method private static a(I)I
    .locals 1
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 242
    packed-switch p0, :pswitch_data_0

    .line 273
    :goto_0
    :pswitch_0
    return v0

    .line 249
    :pswitch_1
    const v0, 0x7f020465

    goto :goto_0

    .line 252
    :pswitch_2
    const v0, 0x7f020469

    goto :goto_0

    .line 256
    :pswitch_3
    const v0, 0x7f020468

    goto :goto_0

    .line 259
    :pswitch_4
    invoke-static {}, Lcmj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    const v0, 0x7f020467

    goto :goto_0

    .line 262
    :cond_0
    const v0, 0x7f020466

    goto :goto_0

    .line 266
    :pswitch_5
    const v0, 0x7f020463

    goto :goto_0

    .line 270
    :pswitch_6
    const v0, 0x7f020464

    goto :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/app/Activity;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    invoke-static {p0}, Lcom/twitter/android/al;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 142
    packed-switch p1, :pswitch_data_0

    .line 178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :pswitch_0
    const-string/jumbo v0, "favorite"

    .line 181
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":login_signup_dialog:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 148
    :pswitch_1
    const-string/jumbo v0, "share_via_dm"

    goto :goto_0

    .line 153
    :pswitch_2
    const-string/jumbo v0, "retweet"

    goto :goto_0

    .line 157
    :pswitch_3
    const-string/jumbo v0, "reply"

    goto :goto_0

    .line 161
    :pswitch_4
    const-string/jumbo v0, "follow"

    goto :goto_0

    .line 165
    :pswitch_5
    const-string/jumbo v0, "following"

    goto :goto_0

    .line 169
    :pswitch_6
    const-string/jumbo v0, "followers"

    goto :goto_0

    .line 174
    :pswitch_7
    const-string/jumbo v0, "favorites"

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_7
        :pswitch_1
    .end packed-switch
.end method

.method static a(Landroid/app/Activity;ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 187
    packed-switch p1, :pswitch_data_0

    .line 233
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :pswitch_1
    const v0, 0x7f0a04c1

    .line 237
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 193
    :pswitch_2
    const v0, 0x7f0a04c2

    .line 194
    goto :goto_0

    .line 197
    :pswitch_3
    const v0, 0x7f0a04c6

    .line 198
    goto :goto_0

    .line 201
    :pswitch_4
    const v0, 0x7f0a04c5

    .line 202
    goto :goto_0

    .line 205
    :pswitch_5
    const v0, 0x7f0a04c4

    .line 206
    goto :goto_0

    .line 209
    :pswitch_6
    const v0, 0x7f0a04be

    .line 210
    goto :goto_0

    .line 213
    :pswitch_7
    const v0, 0x7f0a04c0

    .line 214
    goto :goto_0

    .line 217
    :pswitch_8
    const v0, 0x7f0a04bf

    .line 218
    goto :goto_0

    .line 221
    :pswitch_9
    const v0, 0x7f0a04c3

    .line 222
    goto :goto_0

    .line 225
    :pswitch_a
    const v0, 0x7f0a04c8

    .line 226
    goto :goto_0

    .line 229
    :pswitch_b
    const v0, 0x7f0a04c7

    .line 230
    goto :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 319
    invoke-static {p0}, Lcom/twitter/android/al;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 321
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 322
    invoke-static {p0, v0}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 324
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 326
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 352
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "logged_out_landing"

    .line 353
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 354
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 355
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 95
    invoke-static {p0, p1, p2}, Lcom/twitter/android/al;->d(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)Lcom/twitter/android/dialog/LoggedOutDialogFragment;

    move-result-object v0

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/android/dialog/LoggedOutDialogFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "::impression"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/twitter/android/al;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "logged_out_dialog_fragment"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/dialog/LoggedOutDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method public static a(ZLandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 299
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const-string/jumbo v0, "is_landing_page"

    invoke-virtual {p1, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 302
    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->e()Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "is_landing_page"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 81
    :cond_0
    return v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 366
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "logged_out_landing"

    const-string/jumbo v2, "external"

    .line 367
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 366
    return-object v0
.end method

.method public static b(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 279
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 281
    new-instance v1, Lcom/twitter/android/v$a;

    invoke-direct {v1}, Lcom/twitter/android/v$a;-><init>()V

    .line 282
    invoke-virtual {v1, v2}, Lcom/twitter/android/v$a;->a(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    .line 283
    invoke-virtual {v1, v2}, Lcom/twitter/android/v$a;->b(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    const/4 v2, 0x1

    .line 284
    invoke-virtual {v1, v2}, Lcom/twitter/android/v$a;->c(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    .line 285
    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {v1, v0}, Lcom/twitter/android/v$a;->a(Landroid/content/Intent;)Lcom/twitter/android/v$a;

    .line 288
    :cond_0
    new-instance v2, Lcom/twitter/android/FlowActivity$a;

    invoke-direct {v2, p0}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lako;

    invoke-virtual {v2, v0}, Lcom/twitter/android/FlowActivity$a;->a(Lako;)V

    .line 289
    return-void
.end method

.method public static b(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 337
    invoke-static {p0}, Lcom/twitter/android/al;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "logged_out_landing"

    .line 339
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 340
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "external:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":::impression"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/al;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 344
    :cond_0
    return-void
.end method

.method public static b(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 111
    invoke-static {p0, p1, p2}, Lcom/twitter/android/al;->d(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)Lcom/twitter/android/dialog/LoggedOutDialogFragment;

    move-result-object v0

    .line 112
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/twitter/android/al$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/al$1;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/dialog/LoggedOutDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 119
    return-void
.end method

.method public static c(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 305
    invoke-static {p0}, Lcom/twitter/android/al;->d(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 306
    return-void
.end method

.method public static c(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 379
    const/4 v0, 0x0

    .line 380
    const/16 v1, 0xc

    if-ne p1, v1, :cond_1

    .line 381
    const/16 v0, 0x8

    .line 385
    :cond_0
    :goto_0
    invoke-static {p0, v0, p2}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    .line 386
    return-void

    .line 382
    :cond_1
    const/16 v1, 0xb

    if-ne p1, v1, :cond_0

    .line 383
    const/16 v0, 0x9

    goto :goto_0
.end method

.method public static d(Landroid/app/Activity;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 310
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 311
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 312
    if-eqz v1, :cond_0

    .line 313
    const-string/jumbo v2, "android.intent.extra.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 315
    :cond_0
    return-object v0
.end method

.method private static d(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)Lcom/twitter/android/dialog/LoggedOutDialogFragment;
    .locals 2

    .prologue
    .line 129
    new-instance v0, Lcom/twitter/android/dialog/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/dialog/c$a;-><init>(I)V

    .line 130
    invoke-static {p0, p1}, Lcom/twitter/android/al;->a(Landroid/app/Activity;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/c$a;->a(Ljava/lang/String;)Lcom/twitter/android/dialog/c$a;

    move-result-object v0

    const v1, 0x7f0a04bd

    .line 131
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/c$a;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/f$b;

    const v1, 0x7f0a04bc

    .line 132
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/f$b;->e(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/f$b;

    .line 133
    invoke-static {p0, p1, p2}, Lcom/twitter/android/al;->a(Landroid/app/Activity;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/f$b;->b(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/f$b;

    .line 134
    invoke-static {p1}, Lcom/twitter/android/al;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/f$b;->a(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/f$b;

    .line 135
    invoke-virtual {v0}, Lcom/twitter/android/dialog/f$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/LoggedOutDialogFragment;

    .line 129
    return-object v0
.end method
