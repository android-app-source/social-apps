.class public Lcom/twitter/android/ck;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbxb;


# static fields
.field private static final a:Z


# instance fields
.field private final b:Lcom/twitter/android/revenue/c;

.field protected final c:Lcom/twitter/library/client/v;

.field protected final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field protected final e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected final f:Landroid/content/Context;

.field protected final g:Lcom/twitter/library/client/p;

.field protected final h:Lcom/twitter/android/timeline/be;

.field private final i:I

.field private final j:Lcom/twitter/android/ar;

.field private k:Lwc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcrt;->a()Z

    move-result v0

    sput-boolean v0, Lcom/twitter/android/ck;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 111
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/revenue/c;Lcom/twitter/android/timeline/be;ILcom/twitter/android/ar;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/ar;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 116
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/revenue/c;Lcom/twitter/android/timeline/be;ILcom/twitter/android/ar;)V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/revenue/c;Lcom/twitter/android/timeline/be;ILcom/twitter/android/ar;)V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    .line 123
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ck;->g:Lcom/twitter/library/client/p;

    .line 124
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    .line 125
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ck;->d:Ljava/lang/ref/WeakReference;

    .line 126
    iput-object p2, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 127
    iput-object p3, p0, Lcom/twitter/android/ck;->b:Lcom/twitter/android/revenue/c;

    .line 128
    iput-object p4, p0, Lcom/twitter/android/ck;->h:Lcom/twitter/android/timeline/be;

    .line 129
    iput p5, p0, Lcom/twitter/android/ck;->i:I

    .line 130
    iput-object p6, p0, Lcom/twitter/android/ck;->j:Lcom/twitter/android/ar;

    .line 131
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 880
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 881
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 882
    const v2, 0x7f0e01a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 883
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 884
    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 885
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    .line 886
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 887
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string/jumbo v1, "OK"

    const/4 v2, 0x0

    .line 888
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 889
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 890
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 891
    return-void
.end method

.method private a(Landroid/support/v4/app/FragmentActivity;)V
    .locals 4

    .prologue
    .line 609
    const-string/jumbo v0, "ad_formats_ads_info_link"

    const-string/jumbo v1, "https://business.twitter.com/help/how-twitter-ads-work"

    invoke-static {v0, v1}, Lcoj;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 611
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 612
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 613
    invoke-virtual {p1, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 614
    return-void
.end method

.method private a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/r;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 696
    new-instance v0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;

    .line 697
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V

    .line 698
    invoke-virtual {v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 699
    invoke-virtual {v0, p2}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a(Lcom/twitter/model/core/r;)V

    .line 706
    :goto_0
    return-void

    .line 701
    :cond_0
    new-instance v0, Lcom/twitter/app/dm/r$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/r$a;-><init>()V

    .line 702
    invoke-virtual {v0, p2}, Lcom/twitter/app/dm/r$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    .line 703
    invoke-virtual {v0, p3}, Lcom/twitter/app/dm/r$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    .line 704
    invoke-virtual {v0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    .line 701
    invoke-static {p1, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/r;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/ck;JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/ck;->c(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ck;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/ck;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/r;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 595
    iget-object v0, p0, Lcom/twitter/android/ck;->g:Lcom/twitter/library/client/p;

    new-instance v1, Lbem;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    .line 596
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, p2, p1}, Lbem;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/library/api/PromotedEvent;)V

    .line 595
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 597
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 1

    .prologue
    .line 961
    new-instance v0, Lcom/twitter/android/ReportFlowWebViewActivity$a;

    invoke-direct {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    .line 962
    invoke-virtual {v0, p2}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 963
    const-string/jumbo v0, "report"

    invoke-virtual {p0, v0, p1, p3}, Lcom/twitter/android/ck;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 964
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 970
    new-instance v1, Lcom/twitter/android/bf;

    .line 971
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-direct {v1, p2, p3, v0, v3}, Lcom/twitter/android/bf;-><init>(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/p;Lcom/twitter/android/bf$a;)V

    .line 972
    const-string/jumbo v2, ""

    if-eqz p5, :cond_0

    const-string/jumbo v0, "pin"

    :goto_0
    invoke-virtual {p0, v2, v0, p1, v3}, Lcom/twitter/android/ck;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-virtual {v1, p1, p5, p4, v0}, Lcom/twitter/android/bf;->a(Lcom/twitter/model/core/Tweet;ZLcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 974
    return-void

    .line 972
    :cond_0
    const-string/jumbo v0, "unpin"

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;J)V
    .locals 3

    .prologue
    .line 950
    invoke-static {p2}, Laje$a;->a(Landroid/support/v4/app/FragmentActivity;)Laje$a;

    move-result-object v0

    .line 951
    invoke-virtual {v0, p3, p4}, Laje$a;->a(J)Laje$a;

    move-result-object v0

    .line 952
    invoke-virtual {v0, p1}, Laje$a;->a(Lcom/twitter/model/core/Tweet;)Laje$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 953
    invoke-virtual {v0, v1}, Laje$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Laje$a;

    move-result-object v0

    .line 954
    invoke-virtual {v0}, Laje$a;->a()Lajj;

    move-result-object v0

    .line 955
    invoke-interface {v0}, Lajj;->a()V

    .line 956
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 2

    .prologue
    .line 872
    const-string/jumbo v0, "tweet_analytics"

    const-string/jumbo v1, "click"

    invoke-virtual {p0, v0, v1, p1, p3}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 873
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->t:J

    invoke-static {p2, v0, v1}, Lcom/twitter/android/analytics/TweetAnalyticsWebViewActivity;->a(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 874
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 2

    .prologue
    .line 600
    const-string/jumbo v0, "dismiss_dialog"

    const-string/jumbo v1, "dismiss"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/timeline/bk;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 844
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    .line 845
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcgi;->c:Ljava/lang/String;

    move-object v6, v0

    .line 846
    :goto_0
    iget-object v8, p0, Lcom/twitter/android/ck;->g:Lcom/twitter/library/client/p;

    new-instance v0, Lbek;

    iget-object v1, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    .line 847
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget v3, p1, Lcom/twitter/model/core/Tweet;->P:I

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->O:J

    invoke-direct/range {v0 .. v5}, Lbek;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;IJ)V

    .line 848
    invoke-virtual {v0, v6}, Lbek;->a(Ljava/lang/String;)Lbej;

    move-result-object v0

    .line 849
    invoke-virtual {v0, p2}, Lbej;->a(Lcom/twitter/library/api/PromotedEvent;)Lbej;

    move-result-object v0

    .line 846
    invoke-virtual {v8, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 850
    invoke-virtual {p0, p3, p4, p1, v7}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 851
    const-string/jumbo v0, "unspecified"

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p6, :cond_0

    .line 852
    iget-object v0, p0, Lcom/twitter/android/ck;->g:Lcom/twitter/library/client/p;

    new-instance v1, Lbfo;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    .line 853
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, p6}, Lbfo;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;)V

    .line 852
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 855
    :cond_0
    return-void

    :cond_1
    move-object v6, v7

    .line 845
    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 979
    sget-object v0, Lcom/twitter/android/ck$2;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/core/TweetActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1014
    :goto_0
    :pswitch_0
    return-void

    .line 981
    :pswitch_1
    const/4 v0, 0x1

    .line 1013
    :goto_1
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p4, v0, v1}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_0

    .line 985
    :pswitch_2
    const/16 v0, 0xa

    .line 986
    goto :goto_1

    .line 989
    :pswitch_3
    const/4 v0, 0x2

    .line 990
    goto :goto_1

    .line 993
    :pswitch_4
    const/4 v0, 0x3

    .line 994
    goto :goto_1

    .line 997
    :pswitch_5
    const/4 v0, 0x4

    .line 998
    goto :goto_1

    .line 1001
    :pswitch_6
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/android/ck;->b(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 1005
    :pswitch_7
    invoke-virtual {p0, p2, p4}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/app/Activity;)V

    goto :goto_0

    .line 979
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;ZLcom/twitter/android/timeline/bk;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 897
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    move-object/from16 v7, p8

    .line 898
    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;)V

    .line 946
    :cond_0
    :goto_0
    return-void

    .line 899
    :cond_1
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_2

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p7

    move-object/from16 v7, p8

    .line 900
    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;)V

    goto :goto_0

    .line 901
    :cond_2
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->d:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_3

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p7

    .line 902
    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto :goto_0

    .line 903
    :cond_3
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->e:Lcom/twitter/model/core/TweetActionType;

    if-eq p1, v2, :cond_4

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->f:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_5

    :cond_4
    move-object v2, p0

    move-object v3, p2

    move-object v4, p6

    move-object v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p10

    .line 904
    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;Lcom/twitter/android/timeline/bk;)V

    goto :goto_0

    .line 905
    :cond_5
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->g:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_6

    .line 906
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 907
    :cond_6
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->h:Lcom/twitter/model/core/TweetActionType;

    if-eq p1, v2, :cond_7

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->C:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_8

    .line 908
    :cond_7
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/android/ck;->b(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 909
    :cond_8
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->n:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_9

    .line 910
    move/from16 v0, p9

    invoke-virtual {p0, p2, p4, p7, v0}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Z)V

    goto :goto_0

    .line 911
    :cond_9
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->j:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_a

    .line 912
    invoke-virtual {p0, p2, p6, p4, p5}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 913
    :cond_a
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->k:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_b

    .line 914
    invoke-virtual {p0, p2, p6, p4, p5}, Lcom/twitter/android/ck;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 915
    :cond_b
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->l:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_c

    move-object v2, p0

    move-object v3, p2

    move-object v4, p6

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p8

    .line 916
    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/widget/h;)V

    goto :goto_0

    .line 917
    :cond_c
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->m:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_d

    move-object v2, p0

    move-object v3, p2

    move-object v4, p6

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p8

    .line 918
    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/ck;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/widget/h;)V

    goto :goto_0

    .line 919
    :cond_d
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->s:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_e

    .line 920
    move-object/from16 v0, p11

    invoke-direct {p0, p4, v0}, Lcom/twitter/android/ck;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 921
    :cond_e
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->o:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_f

    .line 922
    invoke-direct {p0, p2, p4, p7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto/16 :goto_0

    .line 923
    :cond_f
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->q:Lcom/twitter/model/core/TweetActionType;

    if-eq p1, v2, :cond_10

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->r:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_12

    .line 924
    :cond_10
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->q:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_11

    const/4 v7, 0x1

    :goto_1
    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Z)V

    goto/16 :goto_0

    :cond_11
    const/4 v7, 0x0

    goto :goto_1

    .line 925
    :cond_12
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->t:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_13

    .line 926
    invoke-direct {p0, p2, p4, p7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto/16 :goto_0

    .line 927
    :cond_13
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->u:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_14

    .line 928
    invoke-virtual {p5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {p0, p2, p4, v2, v3}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;J)V

    goto/16 :goto_0

    .line 929
    :cond_14
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->v:Lcom/twitter/model/core/TweetActionType;

    if-eq p1, v2, :cond_15

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->D:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_16

    .line 930
    :cond_15
    invoke-virtual {p0, p2, p4}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 931
    :cond_16
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->w:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_17

    .line 932
    move-object/from16 v0, p10

    invoke-virtual {p0, v0}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/timeline/bk;)V

    goto/16 :goto_0

    .line 933
    :cond_17
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->x:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_18

    .line 934
    invoke-virtual {p0, p2, p4}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;)V

    goto/16 :goto_0

    .line 935
    :cond_18
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->y:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_19

    .line 936
    invoke-virtual {p0, p2, p4}, Lcom/twitter/android/ck;->b(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;)V

    goto/16 :goto_0

    .line 937
    :cond_19
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->z:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_1a

    .line 938
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->i:Lcom/twitter/library/api/PromotedEvent;

    move-object/from16 v0, p10

    invoke-virtual {p0, v0, p2, v2}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V

    goto/16 :goto_0

    .line 939
    :cond_1a
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->A:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_1b

    .line 940
    invoke-direct {p0, p4}, Lcom/twitter/android/ck;->a(Landroid/support/v4/app/FragmentActivity;)V

    goto/16 :goto_0

    .line 941
    :cond_1b
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->B:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_1c

    .line 942
    invoke-direct {p0, p2, p4, p7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto/16 :goto_0

    .line 943
    :cond_1c
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->E:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v2, :cond_0

    .line 944
    invoke-virtual {p5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/ck;->a(J)V

    goto/16 :goto_0
.end method

.method private a(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 368
    if-nez p2, :cond_0

    .line 370
    iget-object v0, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    const v1, 0x7f0a09ae

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 375
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 376
    return-void

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    if-eqz p1, :cond_1

    const v0, 0x7f0a09ad

    :goto_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0a03a6

    goto :goto_1
.end method

.method static synthetic b(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 0

    .prologue
    .line 94
    invoke-static {p0, p1, p2, p3}, Lcom/twitter/android/ck;->e(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/ck;JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/ck;->d(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V

    return-void
.end method

.method private c(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 395
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    invoke-virtual {p4, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->f(J)V

    .line 398
    :cond_0
    return-void
.end method

.method private d(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 417
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    invoke-virtual {p4, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->g(J)V

    .line 420
    :cond_0
    return-void
.end method

.method private static e(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 2

    .prologue
    .line 509
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    invoke-virtual {p3, p0, p1}, Lcom/twitter/model/util/FriendshipCache;->i(J)V

    .line 512
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lbes;
    .locals 8

    .prologue
    .line 459
    new-instance v1, Lbes;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    const/4 v7, 0x1

    move-object v3, p1

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    return-object v1
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 5

    .prologue
    .line 174
    invoke-static {p3}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 175
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 176
    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-virtual {p0, p3}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, p3, v3}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 177
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {v4, v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 178
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 179
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 177
    return-object v0
.end method

.method protected a(Lcom/twitter/android/bm$a;)Lcom/twitter/android/bm$a;
    .locals 0

    .prologue
    .line 319
    return-object p1
.end method

.method protected a(Landroid/app/Activity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    instance-of v0, p1, Lcom/twitter/android/TweetActivity;

    if-eqz v0, :cond_0

    .line 557
    const-string/jumbo v0, "non_focused_tweet"

    .line 559
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const-string/jumbo v0, "focal"

    .line 158
    :goto_0
    return-object v0

    .line 155
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    const-string/jumbo v0, "ancestor"

    goto :goto_0

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)V
    .locals 7

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/twitter/android/ck;->k:Lwc;

    if-nez v0, :cond_0

    .line 1081
    :goto_0
    return-void

    .line 1075
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ck;->k:Lwc;

    invoke-virtual {v0}, Lwc;->a()Ljava/lang/String;

    move-result-object v0

    .line 1076
    iget-object v1, p0, Lcom/twitter/android/ck;->k:Lwc;

    invoke-virtual {v1}, Lwc;->b()Ljava/lang/String;

    move-result-object v1

    .line 1077
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweet"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "dont_like_recommendation"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v0, v3, v4

    const/4 v0, 0x4

    const-string/jumbo v4, "click"

    aput-object v4, v3, v0

    .line 1078
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1079
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1080
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method public a(JLandroid/support/v4/app/FragmentActivity;)V
    .locals 3

    .prologue
    .line 710
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 711
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    .line 712
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    .line 713
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 714
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 715
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, p3, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 716
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 717
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 718
    invoke-virtual {p3, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 719
    return-void
.end method

.method protected a(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 465
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    .line 466
    invoke-virtual {p4, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->h(J)V

    .line 468
    :cond_0
    return-void
.end method

.method protected a(JZI)V
    .locals 0

    .prologue
    .line 1092
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/bk;)V
    .locals 2

    .prologue
    .line 1055
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/timeline/bk;II)Z

    .line 1056
    return-void
.end method

.method protected a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 3

    .prologue
    .line 581
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    .line 582
    if-eqz v0, :cond_0

    .line 584
    const/4 v1, 0x5

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v1, v2}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/timeline/bk;II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 585
    iget-object v0, v0, Lcgi;->c:Ljava/lang/String;

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/ck;->a(Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;)V

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Landroid/app/Activity;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1046
    const v0, 0x7f0a0c3c

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    aput-object v2, v1, v6

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1047
    invoke-static {p2, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1048
    const v0, 0x7f0a021b

    invoke-virtual {p2, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1049
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;)V
    .locals 9

    .prologue
    .line 240
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 243
    iget-object v8, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 244
    new-instance v0, Lcom/twitter/android/ck$4;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/ck$4;-><init>(Lcom/twitter/android/ck;Ljava/lang/ref/WeakReference;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 306
    new-instance v1, Lcom/twitter/android/bm$a;

    invoke-direct {v1, p3, p1}, Lcom/twitter/android/bm$a;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 307
    invoke-virtual {v1, v0}, Lcom/twitter/android/bm$a;->a(Lcom/twitter/android/bm$b;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 308
    invoke-virtual {v0, p2}, Lcom/twitter/android/bm$a;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 309
    invoke-virtual {p0, v0}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/bm$a;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Lcom/twitter/android/bm$a;->a()Lcom/twitter/android/bm;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Lcom/twitter/android/bm;->a()V

    .line 312
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 516
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0993

    .line 517
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0992

    .line 518
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 519
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 520
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 521
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 522
    invoke-virtual {v0, p2}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ck$9;

    invoke-direct {v1, p0, p4, p1}, Lcom/twitter/android/ck$9;-><init>(Lcom/twitter/android/ck;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)V

    .line 523
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 546
    invoke-virtual {p3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 547
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 2

    .prologue
    .line 333
    invoke-virtual {p0, p3}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "reply"

    invoke-virtual {p0, v0, v1, p1, p5}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 334
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 335
    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 336
    invoke-virtual {p4}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 337
    invoke-virtual {v0, p3}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 334
    invoke-virtual {p3, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 338
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;)V
    .locals 1

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/twitter/android/ck;->j:Lcom/twitter/android/ar;

    if-eqz v0, :cond_0

    .line 1060
    iget-object v0, p0, Lcom/twitter/android/ck;->j:Lcom/twitter/android/ar;

    invoke-virtual {v0, p2, p1}, Lcom/twitter/android/ar;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 1062
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Z)V
    .locals 7

    .prologue
    .line 227
    if-eqz p4, :cond_0

    .line 228
    invoke-virtual {p0, p2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "share_via_dm"

    invoke-virtual {p0, v0, v1, p1, p3}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 234
    :goto_0
    new-instance v0, Lcom/twitter/model/core/r;

    invoke-direct {v0, p1}, Lcom/twitter/model/core/r;-><init>(Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Lcom/twitter/android/ck;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/r;Ljava/lang/String;)V

    .line 235
    return-void

    .line 230
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "share_sheet"

    const-string/jumbo v5, "tweet"

    const-string/jumbo v6, "share_via_dm"

    .line 231
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 230
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;)V
    .locals 8

    .prologue
    .line 187
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 188
    :goto_0
    if-eqz p5, :cond_0

    .line 189
    invoke-interface {p5, v0}, Lcom/twitter/library/widget/h;->a(Z)V

    .line 192
    :cond_0
    if-eqz v0, :cond_2

    .line 193
    new-instance v1, Lbfm;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->u:J

    move-object v3, p3

    invoke-direct/range {v1 .. v7}, Lbfm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 196
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfm;->a(Lcgi;)Lbfm;

    move-result-object v0

    .line 197
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->m()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfm;->a(Ljava/lang/Boolean;)Lbfm;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/twitter/android/ck;->g:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/android/ck$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/ck$1;-><init>(Lcom/twitter/android/ck;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 207
    invoke-virtual {p0, p2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "favorite"

    invoke-virtual {p0, v0, v1, p1, p4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 222
    :goto_1
    return-void

    .line 187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 209
    :cond_2
    new-instance v1, Lbfp;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->u:J

    move-object v3, p3

    invoke-direct/range {v1 .. v7}, Lbfp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 211
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfp;->a(Lcgi;)Lbfp;

    move-result-object v0

    .line 212
    iget-object v1, p0, Lcom/twitter/android/ck;->g:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/android/ck$3;

    iget-object v3, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/ck$3;-><init>(Lcom/twitter/android/ck;Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 220
    invoke-virtual {p0, p2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unfavorite"

    invoke-virtual {p0, v0, v1, p1, p4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto :goto_1
.end method

.method a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 838
    const-string/jumbo v5, "unspecified"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/timeline/bk;)V

    .line 839
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    .line 381
    invoke-virtual {p0, p3}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mute_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 382
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbfa;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-direct {v1, v2, p4}, Lbfa;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->s:J

    .line 383
    invoke-virtual {v1, v2, v3}, Lbfa;->a(J)Lbeq;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ck$5;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/ck$5;-><init>(Lcom/twitter/android/ck;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;)V

    .line 382
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 391
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/widget/h;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 427
    invoke-virtual {p0, p3}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "block_user"

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 428
    const-string/jumbo v0, "block_dialog"

    const-string/jumbo v1, "impression"

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 430
    iget-object v6, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/library/view/e;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, -0x1

    .line 431
    invoke-virtual {p3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v9

    new-instance v0, Lcom/twitter/android/ck$7;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ck$7;-><init>(Lcom/twitter/android/ck;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/library/widget/h;)V

    .line 430
    invoke-static {v6, v7, v8, v9, v0}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Lcom/twitter/app/common/dialog/b$d;)V

    .line 456
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;Lcom/twitter/android/timeline/bk;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 344
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v1

    .line 345
    const/4 v0, 0x0

    .line 346
    if-eqz v1, :cond_0

    iget v2, p1, Lcom/twitter/model/core/Tweet;->P:I

    if-eq v2, v3, :cond_0

    .line 348
    invoke-virtual {p0, p5, v3, v3}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/timeline/bk;II)Z

    move-result v0

    .line 353
    :cond_0
    if-nez v0, :cond_1

    .line 354
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/ck;->a(ZLjava/lang/String;)V

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {v0, v1}, Lajk;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lajk;

    move-result-object v0

    .line 357
    iget-object v1, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-static {v1, v0}, Lajg$a;->a(Landroid/content/Context;Lajk;)Lajg$a;

    move-result-object v0

    .line 358
    invoke-virtual {v0, p1}, Lajg$a;->a(Lcom/twitter/model/core/Tweet;)Lajg$a;

    move-result-object v0

    .line 359
    invoke-virtual {v0, p2}, Lajg$a;->a(Lcom/twitter/model/util/FriendshipCache;)Lajg$a;

    move-result-object v0

    .line 360
    invoke-virtual {v0, p3}, Lajg$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lajg$a;

    move-result-object v0

    .line 361
    invoke-virtual {v0, p4}, Lajg$a;->a(Lcom/twitter/library/widget/h;)Lajg$a;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Lajg$a;->a()Lajj;

    move-result-object v0

    .line 363
    invoke-interface {v0}, Lajj;->a()V

    .line 364
    return-void
.end method

.method public a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;Lcom/twitter/android/timeline/bk;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1019
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;ZLcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 1021
    return-void
.end method

.method public a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;ZLcom/twitter/android/timeline/bk;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/twitter/android/ck;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/Fragment;

    .line 1027
    if-eqz v3, :cond_0

    .line 1028
    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 1029
    if-eqz v4, :cond_0

    .line 1030
    iget-object v0, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v5

    .line 1031
    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    .line 1032
    invoke-direct/range {v0 .. v11}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;ZLcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 1039
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 1035
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 1

    .prologue
    .line 164
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 165
    return-void
.end method

.method protected a(Lwc;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/twitter/android/ck;->k:Lwc;

    .line 150
    return-void
.end method

.method protected a(ZZLandroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 323
    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    .line 324
    if-eqz p2, :cond_1

    const v0, 0x7f0a0994

    .line 326
    :goto_0
    const/4 v1, 0x1

    invoke-static {p3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 328
    :cond_0
    return-void

    .line 324
    :cond_1
    const v0, 0x7f0a099d

    goto :goto_0
.end method

.method protected a(Lcom/twitter/android/timeline/bk;II)Z
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/twitter/android/ck;->h:Lcom/twitter/android/timeline/be;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 573
    iget-object v0, p0, Lcom/twitter/android/ck;->h:Lcom/twitter/android/timeline/be;

    iget v1, p0, Lcom/twitter/android/ck;->i:I

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/twitter/android/timeline/be;->a(Lcom/twitter/android/timeline/bk;III)V

    .line 574
    const/4 v0, 0x1

    .line 576
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/util/FriendshipCache;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1087
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v5, v4

    move-object v6, p3

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;Lcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 1088
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcom/twitter/model/core/Tweet;)V
    .locals 3

    .prologue
    .line 605
    const-string/jumbo v0, "caret"

    const-string/jumbo v1, "click"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 606
    return-void
.end method

.method protected b(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 551
    const/4 v0, 0x0

    invoke-static {p3, p1, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Z)V

    .line 552
    invoke-virtual {p0, p3}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "share"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 553
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/FragmentActivity;)V
    .locals 1

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/twitter/android/ck;->j:Lcom/twitter/android/ar;

    if-eqz v0, :cond_0

    .line 1066
    iget-object v0, p0, Lcom/twitter/android/ck;->j:Lcom/twitter/android/ar;

    invoke-virtual {v0, p2, p1}, Lcom/twitter/android/ar;->b(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 1068
    :cond_0
    return-void
.end method

.method protected b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    .line 403
    invoke-virtual {p0, p3}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unmute_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 404
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbff;

    iget-object v2, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-direct {v1, v2, p4}, Lbff;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->s:J

    .line 405
    invoke-virtual {v1, v2, v3}, Lbff;->a(J)Lbeq;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ck$6;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/ck$6;-><init>(Lcom/twitter/android/ck;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;)V

    .line 404
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 413
    return-void
.end method

.method protected b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/widget/h;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 475
    invoke-virtual {p0, p3}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unblock_user"

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 476
    const-string/jumbo v0, "unblock_dialog"

    const-string/jumbo v1, "impression"

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 477
    iget-object v6, p0, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-object v7, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    const/4 v8, -0x1

    .line 478
    invoke-virtual {p3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v9

    new-instance v0, Lcom/twitter/android/ck$8;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ck$8;-><init>(Lcom/twitter/android/ck;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/library/widget/h;)V

    .line 477
    invoke-static {v6, v7, v8, v9, v0}, Lcom/twitter/android/util/y;->b(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Lcom/twitter/app/common/dialog/b$d;)V

    .line 505
    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/ck;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 170
    return-void
.end method

.method public c(Lcom/twitter/model/core/Tweet;)Z
    .locals 14

    .prologue
    .line 621
    iget-object v0, p0, Lcom/twitter/android/ck;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 622
    if-nez v0, :cond_1

    const/4 v3, 0x0

    .line 623
    :goto_0
    if-eqz v3, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 624
    :cond_0
    const/4 v0, 0x0

    .line 691
    :goto_1
    return v0

    .line 622
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    goto :goto_0

    .line 627
    :cond_2
    iget-object v4, p1, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    .line 628
    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->x:J

    .line 629
    iget-object v0, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 630
    iget-object v0, p0, Lcom/twitter/android/ck;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    .line 632
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v10, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v11, p0, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 634
    invoke-virtual {v11}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "quoted_tweet"

    const-string/jumbo v13, "long_press"

    .line 633
    invoke-static {v10, v11, v12, v13}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 637
    new-instance v2, Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-direct {v2, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 638
    new-instance v1, Ljava/util/ArrayList;

    const/4 v5, 0x3

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 639
    if-eqz v0, :cond_3

    .line 640
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    sget-object v5, Lcom/twitter/model/core/TweetActionType;->n:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v2, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 641
    const v0, 0x7f0a07fa

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    :cond_3
    invoke-static {v4, v8, v9}, Lbxd;->a(Lcom/twitter/model/core/r;J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 644
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    sget-object v5, Lcom/twitter/model/core/TweetActionType;->h:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v2, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 645
    const v0, 0x7f0a08aa

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 647
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    sget-object v5, Lcom/twitter/model/core/TweetActionType;->p:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v2, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 648
    const v0, 0x7f0a0a26

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 650
    new-instance v5, Lcom/twitter/android/widget/aj$b;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 651
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v5, v0}, Lcom/twitter/android/widget/aj$b;->a([Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 652
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 653
    new-instance v0, Lcom/twitter/android/ck$10;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/ck$10;-><init>(Lcom/twitter/android/ck;Landroid/util/SparseArray;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/r;Lcom/twitter/model/core/Tweet;JJ)V

    invoke-virtual {v10, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 689
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->setRetainInstance(Z)V

    .line 690
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 691
    const/4 v0, 0x1

    goto/16 :goto_1
.end method
