.class Lcom/twitter/android/AuthenticatorActivity$1;
.super Lcom/twitter/library/service/w;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/AuthenticatorActivity;->onClickHandler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/w",
        "<",
        "Landroid/os/Bundle;",
        "Lbat;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/AuthenticatorActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/w;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbat;)V
    .locals 5

    .prologue
    const v4, 0x7f130197

    const v3, 0x7f0a008a

    const/4 v2, 0x1

    .line 128
    invoke-virtual {p1}, Lbat;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/AuthenticatorActivity;->removeDialog(I)V

    .line 132
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    invoke-virtual {p1}, Lbat;->s()Lcom/twitter/model/account/LoginResponse;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/twitter/model/account/LoginResponse;->a:Lcom/twitter/model/account/OAuthToken;

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    iget-object v1, v1, Lcom/twitter/android/AuthenticatorActivity;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    iget-object v0, v0, Lcom/twitter/model/account/LoginResponse;->a:Lcom/twitter/model/account/OAuthToken;

    invoke-virtual {v1, v0}, Lcom/twitter/android/AuthenticatorActivity;->a(Lcom/twitter/model/account/OAuthToken;)V

    .line 150
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    iget-object v0, v0, Lcom/twitter/model/account/LoginResponse;->a:Lcom/twitter/model/account/OAuthToken;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/AuthenticatorActivity;->a(Lcom/twitter/model/account/OAuthToken;Z)V

    goto :goto_0

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/AuthenticatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v1, v3}, Lcom/twitter/android/AuthenticatorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/AuthenticatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/AuthenticatorActivity$1;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v1, v3}, Lcom/twitter/android/AuthenticatorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 125
    check-cast p1, Lbat;

    invoke-virtual {p0, p1}, Lcom/twitter/android/AuthenticatorActivity$1;->a(Lbat;)V

    return-void
.end method
