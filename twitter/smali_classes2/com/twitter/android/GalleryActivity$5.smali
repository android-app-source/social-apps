.class Lcom/twitter/android/GalleryActivity$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/GalleryActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/GalleryActivity;)V
    .locals 0

    .prologue
    .line 700
    iput-object p1, p0, Lcom/twitter/android/GalleryActivity$5;->a:Lcom/twitter/android/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity$5;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->c(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/widget/SlidingPanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 704
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity$5;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->c(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/widget/SlidingPanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->a()Z

    .line 705
    const/4 v0, 0x0

    return v0
.end method
