.class Lcom/twitter/android/cf$k;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/cf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "k"
.end annotation


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/view/View;

.field public final e:Lcom/twitter/android/cf$h;

.field public final f:Lcom/twitter/android/trends/TrendBadgesView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2242
    const v0, 0x7f1306b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/cf$k;->a:Landroid/widget/ImageView;

    .line 2243
    const v0, 0x7f1306b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cf$k;->b:Landroid/widget/TextView;

    .line 2244
    const v0, 0x7f1301e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cf$k;->c:Landroid/widget/TextView;

    .line 2245
    new-instance v0, Lcom/twitter/android/cf$h;

    invoke-direct {v0, p1}, Lcom/twitter/android/cf$h;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/cf$k;->e:Lcom/twitter/android/cf$h;

    .line 2246
    const v0, 0x7f130815

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$k;->d:Landroid/view/View;

    .line 2247
    const v0, 0x7f13052e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/trends/TrendBadgesView;

    iput-object v0, p0, Lcom/twitter/android/cf$k;->f:Lcom/twitter/android/trends/TrendBadgesView;

    .line 2248
    return-void
.end method
