.class public Lcom/twitter/android/cn;
.super Lcom/twitter/android/e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/e",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lpv;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/android/e;-><init>(Lpv;)V

    .line 27
    return-void
.end method

.method private static b(Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lcom/twitter/util/collection/k;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/model/core/Tweet;",
            ")",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 42
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->O()Lcom/twitter/model/core/MediaEntity;

    move-result-object v2

    .line 44
    invoke-static {}, Lcom/twitter/android/av/y;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v3, v4, v0

    if-nez v3, :cond_0

    .line 47
    invoke-static {p1, v2}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;)J

    move-result-wide v4

    cmp-long v0, v4, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 49
    :goto_0
    if-eqz v0, :cond_1

    iget-wide v0, v2, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    :goto_1
    return-object v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected a(Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/model/core/Tweet;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    invoke-static {p1, p2}, Lcom/twitter/android/cn;->b(Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/twitter/util/collection/k;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/cn;->a:Lpv;

    .line 35
    invoke-virtual {v0}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, p1, v2, v3}, Lpv;->a(Lcom/twitter/library/client/Session;J)Lrx/c;

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(Lcom/twitter/library/client/Session;Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 21
    check-cast p2, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/cn;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
