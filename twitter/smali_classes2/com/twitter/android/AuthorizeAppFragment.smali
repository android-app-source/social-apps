.class public Lcom/twitter/android/AuthorizeAppFragment;
.super Lcom/twitter/app/common/base/BaseFragment;
.source "Twttr"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/AuthorizeAppFragment$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/AuthorizeAppFragment$a;

.field private b:Lcom/twitter/model/account/UserAccount;

.field private c:Lcom/twitter/ui/user/UserView;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:[Lakm;

.field private g:Lcom/twitter/library/client/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragment;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 233
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->g:Lcom/twitter/library/client/v;

    .line 234
    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "sso_sdk:::"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 235
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 236
    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 237
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 239
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 240
    return-void
.end method

.method private d()[Lakm;
    .locals 2

    .prologue
    .line 220
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    .line 221
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lakm;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lakm;

    .line 222
    sget-object v1, Lakm;->a:Lakm$a;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 223
    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 104
    const v0, 0x7f04003c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/AuthorizeAppFragment$a;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/AuthorizeAppFragment$a;

    .line 153
    return-void
.end method

.method public a(Lcom/twitter/model/account/UserAccount;)V
    .locals 2

    .prologue
    .line 156
    if-eqz p1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/ui/user/UserView;

    iget-object v1, p1, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 159
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/model/account/UserAccount;

    .line 160
    return-void
.end method

.method public aP_()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->aP_()V

    .line 142
    const-string/jumbo v0, "impression"

    invoke-direct {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->b(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-direct {p0}, Lcom/twitter/android/AuthorizeAppFragment;->d()[Lakm;

    move-result-object v3

    .line 198
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->f:[Lakm;

    invoke-static {v3, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    array-length v0, v3

    if-nez v0, :cond_1

    .line 201
    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/model/account/UserAccount;)V

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget-object v4, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/model/account/UserAccount;

    .line 206
    array-length v5, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_5

    aget-object v0, v3, v2

    .line 207
    if-eqz v4, :cond_2

    iget-object v6, v4, Lcom/twitter/model/account/UserAccount;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v6}, Lakm;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 212
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 213
    new-instance v1, Lcom/twitter/model/account/UserAccount;

    invoke-virtual {v0}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/account/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/model/core/TwitterUser;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/model/account/UserAccount;)V

    .line 215
    :cond_3
    iput-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->f:[Lakm;

    goto :goto_0

    .line 206
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 133
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/model/account/UserAccount;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/model/account/UserAccount;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/model/account/UserAccount;)V

    .line 136
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 165
    const-string/jumbo v0, "account"

    .line 166
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 167
    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/model/account/UserAccount;)V

    .line 169
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 174
    const v1, 0x7f130195

    if-eq v0, v1, :cond_0

    const v1, 0x7f130193

    if-ne v0, v1, :cond_2

    .line 175
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "authorize_account"

    const/4 v2, 0x1

    .line 176
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    .line 175
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/AuthorizeAppFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 191
    :cond_1
    :goto_0
    return-void

    .line 178
    :cond_2
    const v1, 0x7f130199

    if-ne v0, v1, :cond_3

    .line 179
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/AuthorizeAppFragment$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/model/account/UserAccount;

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/AuthorizeAppFragment$a;

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/model/account/UserAccount;

    iget-object v1, v1, Lcom/twitter/model/account/UserAccount;->a:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/twitter/android/AuthorizeAppFragment$a;->a(Landroid/accounts/Account;)V

    .line 181
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 182
    check-cast p1, Landroid/widget/Button;

    const v0, 0x7f0a0088

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    .line 183
    const-string/jumbo v0, "success"

    invoke-direct {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :cond_3
    const v1, 0x7f130198

    if-ne v0, v1, :cond_1

    .line 186
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/AuthorizeAppFragment$a;

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/AuthorizeAppFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/AuthorizeAppFragment$a;->a()V

    .line 188
    const-string/jumbo v0, "cancel"

    invoke-direct {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 63
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 65
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 66
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v3

    .line 67
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v2

    .line 68
    new-instance v4, Lcom/twitter/model/account/UserAccount;

    invoke-virtual {v2}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-direct {v4, v2, v3}, Lcom/twitter/model/account/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/model/core/TwitterUser;)V

    iput-object v4, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/model/account/UserAccount;

    .line 71
    :cond_0
    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->g:Lcom/twitter/library/client/v;

    .line 73
    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v1

    .line 74
    const-string/jumbo v2, "app_name"

    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    .line 75
    const-string/jumbo v2, "app_consumer_key"

    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    .line 76
    if-eqz p1, :cond_2

    .line 77
    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    .line 78
    const-string/jumbo v1, "app_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    .line 81
    const-string/jumbo v1, "app_consumer_key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    .line 85
    :cond_2
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 86
    invoke-direct {p0}, Lcom/twitter/android/AuthorizeAppFragment;->d()[Lakm;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->f:[Lakm;

    .line 88
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 89
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->onDestroy()V

    .line 148
    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 149
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 95
    const-string/jumbo v0, "app_name"

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 98
    const-string/jumbo v0, "app_consumer_key"

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 100
    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 109
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 111
    const v0, 0x7f130199

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 112
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    const v0, 0x7f130198

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 115
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 118
    const v0, 0x7f13011b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    const v1, 0x7f0a0092

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/AuthorizeAppFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    const v0, 0x7f130197

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 122
    const v1, 0x7f0a0091

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/AuthorizeAppFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :cond_0
    const v0, 0x7f130194

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserView;

    iput-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/ui/user/UserView;

    .line 126
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/ui/user/UserView;

    invoke-virtual {v0, v4}, Lcom/twitter/ui/user/UserView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const v0, 0x7f130193

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    return-void
.end method
