.class public Lcom/twitter/android/card/h;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/card/h;


# instance fields
.field private b:J

.field private c:Ljava/lang/Integer;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/android/card/h;

    invoke-direct {v0}, Lcom/twitter/android/card/h;-><init>()V

    sput-object v0, Lcom/twitter/android/card/h;->a:Lcom/twitter/android/card/h;

    .line 18
    const-class v0, Lcom/twitter/android/card/h;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 19
    return-void
.end method

.method public static b()Lcom/twitter/android/card/h;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/twitter/android/card/h;->a:Lcom/twitter/android/card/h;

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 1

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/twitter/android/card/h;->b:J

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/card/h;->c:Ljava/lang/Integer;

    .line 43
    return-void
.end method

.method public a(JI)V
    .locals 3

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/twitter/android/card/h;->b:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 47
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/h;->c:Ljava/lang/Integer;

    .line 49
    :cond_0
    return-void
.end method

.method public b(J)Ljava/lang/Integer;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-wide v2, p0, Lcom/twitter/android/card/h;->b:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/h;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/twitter/android/card/h;->c:Ljava/lang/Integer;

    .line 55
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/twitter/android/card/h;->b:J

    .line 56
    iput-object v1, p0, Lcom/twitter/android/card/h;->c:Ljava/lang/Integer;

    .line 60
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 58
    goto :goto_0
.end method
