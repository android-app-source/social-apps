.class public Lcom/twitter/android/card/w;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Lcom/twitter/model/card/property/Vector2F;

.field private f:J

.field private g:J

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-object v0, Lcom/twitter/model/card/property/Vector2F;->b:Lcom/twitter/model/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/android/card/w;->e:Lcom/twitter/model/card/property/Vector2F;

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/card/w;->f:J

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/card/w;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcar;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x1

    .line 46
    iget-object v0, p0, Lcom/twitter/android/card/w;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 47
    const-string/jumbo v0, "api"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/w;->a:Ljava/lang/String;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/w;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 50
    const-string/jumbo v0, "id"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/w;->c:Ljava/lang/String;

    .line 52
    :cond_1
    iget-wide v2, p0, Lcom/twitter/android/card/w;->f:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 53
    const-string/jumbo v0, "broadcaster_twitter_id"

    .line 54
    invoke-static {v0, p1}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    .line 55
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 54
    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/card/w;->f:J

    .line 57
    :cond_2
    const-string/jumbo v0, "is_360"

    invoke-static {v0, p1}, Lcom/twitter/library/card/g;->a(Ljava/lang/String;Lcar;)Ljava/lang/Boolean;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/card/w;->d:Z

    .line 59
    iget-object v0, p0, Lcom/twitter/android/card/w;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/card/w;->c:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 60
    :cond_3
    iput-boolean v1, p0, Lcom/twitter/android/card/w;->h:Z

    .line 62
    :cond_4
    const-string/jumbo v0, "player_width"

    invoke-static {v0, p1}, Lcom/twitter/library/card/x;->a(Ljava/lang/String;Lcar;)Ljava/lang/Integer;

    move-result-object v0

    .line 63
    const-string/jumbo v1, "player_height"

    invoke-static {v1, p1}, Lcom/twitter/library/card/x;->a(Ljava/lang/String;Lcar;)Ljava/lang/Integer;

    move-result-object v1

    .line 64
    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    .line 65
    new-instance v2, Lcom/twitter/model/card/property/Vector2F;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v2, v0, v1}, Lcom/twitter/model/card/property/Vector2F;-><init>(FF)V

    iput-object v2, p0, Lcom/twitter/android/card/w;->e:Lcom/twitter/model/card/property/Vector2F;

    .line 69
    :goto_1
    const-string/jumbo v0, "timecode"

    invoke-static {v0, p1}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/card/w;->g:J

    .line 70
    return-void

    .line 58
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_6
    sget-object v0, Lcom/twitter/model/card/property/Vector2F;->b:Lcom/twitter/model/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/android/card/w;->e:Lcom/twitter/model/card/property/Vector2F;

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/card/CardContext;)V
    .locals 2

    .prologue
    .line 36
    if-eqz p1, :cond_2

    .line 37
    invoke-virtual {p1}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v0

    move-object v1, v0

    .line 38
    :goto_0
    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcax;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/card/w;->h:Z

    .line 39
    iget-boolean v0, p0, Lcom/twitter/android/card/w;->h:Z

    if-nez v0, :cond_1

    .line 40
    invoke-virtual {v1}, Lcax;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/w;->b:Ljava/lang/String;

    .line 42
    :cond_1
    return-void

    .line 37
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 38
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/card/w;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/twitter/model/card/property/Vector2F;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/card/w;->e:Lcom/twitter/model/card/property/Vector2F;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/card/w;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/twitter/android/card/w;->d:Z

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/twitter/android/card/w;->f:J

    return-wide v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/twitter/android/card/w;->g:J

    return-wide v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/twitter/android/card/w;->h:Z

    return v0
.end method
