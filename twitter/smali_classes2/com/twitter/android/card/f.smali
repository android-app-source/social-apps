.class public Lcom/twitter/android/card/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/card/d;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/revenue/f;

.field private final c:J

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/library/card/CardContext;

.field private h:Lcar;

.field private i:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 59
    new-instance v0, Lcom/twitter/android/revenue/f;

    invoke-direct {v0}, Lcom/twitter/android/revenue/f;-><init>()V

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;Lcom/twitter/android/revenue/f;J)V

    .line 60
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/revenue/f;J)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/twitter/android/card/f;->b:Lcom/twitter/android/revenue/f;

    .line 65
    iput-wide p3, p0, Lcom/twitter/android/card/f;->c:J

    .line 66
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 402
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/card/f;->c:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "profile"

    aput-object v3, v1, v2

    const/4 v2, 0x0

    aput-object v2, v1, v6

    const/4 v2, 0x2

    const-string/jumbo v3, "spotlight"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "platform_card"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    aput-object p1, v1, v2

    .line 403
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/String;

    .line 406
    invoke-virtual {v0, v1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 407
    invoke-virtual {p0}, Lcom/twitter/android/card/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->n(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 408
    iget-object v1, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    const-string/jumbo v2, "app"

    iget-object v3, p0, Lcom/twitter/android/card/f;->h:Lcar;

    iget-wide v4, p0, Lcom/twitter/android/card/f;->i:J

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Ljava/lang/String;Lcar;J)V

    .line 410
    iget-wide v2, p0, Lcom/twitter/android/card/f;->i:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 411
    iget-wide v2, p0, Lcom/twitter/android/card/f;->i:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 412
    invoke-virtual {v0, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 414
    :cond_0
    return-object v0
.end method

.method private a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 460
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    const/4 v0, 0x0

    .line 467
    :goto_0
    return-object v0

    .line 463
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/NativeCardEvent;

    iget-object v1, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/NativeCardEvent;-><init>(Ljava/lang/String;)V

    .line 464
    if-eqz p1, :cond_1

    .line 465
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 467
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object p1

    .line 474
    :cond_0
    return-object p1
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/twitter/android/card/f;->h:Lcar;

    if-eqz v0, :cond_0

    .line 378
    invoke-direct {p0, p1, p3}, Lcom/twitter/android/card/f;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 380
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/card/f;->g(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    goto :goto_0
.end method

.method private g(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 387
    const-string/jumbo v0, "tweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/card/f;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 388
    iget-object v0, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 389
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 391
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    .line 392
    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->f()Lcom/twitter/library/scribe/ScribeItemsProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/scribe/ScribeItemsProvider;->a()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 393
    :goto_1
    iget-wide v0, p0, Lcom/twitter/android/card/f;->c:J

    iget-object v2, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    iget-object v4, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/card/g;->a(JLandroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    const/4 v2, 0x1

    aput-object v6, v1, v2

    const/4 v2, 0x2

    aput-object v7, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const/4 v2, 0x4

    aput-object p1, v1, v2

    .line 394
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 395
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/String;

    .line 396
    invoke-virtual {v0, v1, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 397
    invoke-virtual {p0}, Lcom/twitter/android/card/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->n(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 393
    return-object v0

    .line 389
    :cond_0
    const-string/jumbo v0, ""

    move-object v6, v0

    goto :goto_0

    :cond_1
    move-object v7, v5

    .line 392
    goto :goto_1
.end method

.method private h(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 423
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/card/f;->h(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    return-object v0
.end method

.method private h(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 428
    iget-wide v0, p0, Lcom/twitter/android/card/f;->c:J

    iget-object v2, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    iget-object v4, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/card/g;->a(JLandroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "tweet::tweet"

    aput-object v3, v2, v6

    aput-object p2, v2, v7

    const/4 v3, 0x2

    aput-object p1, v2, v3

    .line 429
    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v1, "tweet::tweet::impression"

    .line 430
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/card/f;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 431
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/String;

    .line 432
    invoke-virtual {v0, v1, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 433
    invoke-virtual {p0}, Lcom/twitter/android/card/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->n(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 428
    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 80
    iput-wide p1, p0, Lcom/twitter/android/card/f;->i:J

    .line 81
    return-void
.end method

.method public a(Lcar;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/android/card/f;->h:Lcar;

    .line 76
    return-void
.end method

.method protected a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 0

    .prologue
    .line 482
    invoke-virtual {p0, p1}, Lcom/twitter/android/card/f;->a(Lcpk;)V

    .line 483
    return-void
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 86
    return-void
.end method

.method public a(Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    .prologue
    .line 444
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 445
    return-void
.end method

.method public a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->i()Lcgi;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_0

    .line 453
    invoke-static {p1, v0}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsq$a;->c(Ljava/lang/String;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 454
    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcpk;)V

    .line 457
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/CardContext;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    .line 102
    return-void
.end method

.method protected a(Lcpk;)V
    .locals 0

    .prologue
    .line 493
    invoke-static {p1}, Lcpm;->a(Lcpk;)V

    .line 494
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/card/f;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 171
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 160
    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 161
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 240
    const-string/jumbo v0, "2586390716:message_me"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/card/f;->d(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 245
    :goto_0
    return-void

    .line 243
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/card/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 5

    .prologue
    .line 135
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v0

    .line 136
    sget-object v1, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v1}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v1

    .line 137
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 142
    invoke-direct {p0, p2, p3}, Lcom/twitter/android/card/f;->g(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v3

    .line 143
    const-string/jumbo v4, "app_download_client_event"

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 144
    const-string/jumbo v4, "4"

    invoke-virtual {v3, v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 145
    if-eqz v2, :cond_0

    .line 146
    const-string/jumbo v0, "3"

    invoke-virtual {v3, v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 148
    :cond_0
    if-eqz v1, :cond_1

    .line 149
    const-string/jumbo v0, "6"

    invoke-virtual {v1}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 150
    invoke-virtual {v1}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 152
    :cond_1
    return-object v3
.end method

.method public b()Lcom/twitter/library/card/CardContext;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    return-object v0
.end method

.method public b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/twitter/android/card/f;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 91
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 351
    iget-wide v0, p0, Lcom/twitter/android/card/f;->c:J

    iget-object v2, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    iget-object v4, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 352
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/card/g;->a(JLandroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "tweet:::platform_promotion_card:open_link"

    aput-object v3, v1, v2

    .line 353
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 354
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0, p1, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 355
    invoke-virtual {p0}, Lcom/twitter/android/card/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->n(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 351
    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 356
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/card/f;->e(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 260
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 7

    .prologue
    .line 341
    const-string/jumbo v0, "tweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/card/f;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 342
    iget-wide v0, p0, Lcom/twitter/android/card/f;->c:J

    iget-object v2, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    iget-object v4, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v5, p1

    .line 343
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/card/g;->a(JLandroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "::tweet:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "open_web_view_card"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 344
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/String;

    .line 345
    invoke-virtual {v0, v1, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/f;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 346
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 342
    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 347
    return-void
.end method

.method c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "2"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "1"

    goto :goto_0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 294
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/card/f;->h(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 296
    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 297
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 298
    if-nez v0, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 302
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v1

    .line 303
    sget-object v2, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v2}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v2

    .line 304
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 305
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/card/f;->h(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v3

    .line 310
    const-string/jumbo v4, "app_download_client_event"

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 311
    const-string/jumbo v4, "4"

    invoke-virtual {v3, v4, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 314
    invoke-static {v0, v1}, Lcom/twitter/library/util/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 315
    const-string/jumbo v1, "3"

    invoke-virtual {v3, v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 318
    if-eqz v2, :cond_2

    .line 319
    const-string/jumbo v0, "6"

    .line 320
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 319
    invoke-virtual {v3, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 321
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 323
    :cond_2
    invoke-virtual {p0, v3}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 5

    .prologue
    .line 181
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 183
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 185
    if-nez v0, :cond_0

    .line 214
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 191
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v2

    .line 192
    sget-object v3, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v3}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v3

    .line 193
    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_1

    .line 195
    const-string/jumbo v4, "3"

    invoke-virtual {v1, v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 197
    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 202
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 204
    const-string/jumbo v4, "app_download_client_event"

    invoke-virtual {v1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 205
    const-string/jumbo v4, "4"

    invoke-virtual {v1, v4, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 206
    if-eqz v0, :cond_2

    .line 207
    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 209
    :cond_2
    if-eqz v3, :cond_3

    .line 210
    const-string/jumbo v0, "6"

    invoke-virtual {v3}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 211
    invoke-virtual {v3}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 213
    :cond_3
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i()Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 360
    const-string/jumbo v0, "installed_app"

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/card/f;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v3

    .line 362
    const-string/jumbo v0, "installed_app"

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/card/f;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v4

    .line 364
    const-string/jumbo v0, "post_installed_logging_timeframe"

    const v1, 0x1b7740

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v8, v0

    .line 366
    const-string/jumbo v0, "post_installed_logging_polling_interval"

    const v1, 0x927c0

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v10, v0

    .line 370
    iget-object v0, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/f;->g:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->i()Lcgi;

    move-result-object v5

    .line 371
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/card/f;->b:Lcom/twitter/android/revenue/f;

    iget-object v1, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    .line 372
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v6

    move-object v2, p1

    .line 371
    invoke-virtual/range {v0 .. v11}, Lcom/twitter/android/revenue/f;->b(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;Lcgi;JJJ)V

    .line 373
    return-void

    .line 370
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 4

    .prologue
    .line 250
    new-instance v0, Lcom/twitter/analytics/model/ScribeKeyValue;

    const-string/jumbo v1, "viewing_user_id"

    iget-wide v2, p0, Lcom/twitter/android/card/f;->c:J

    .line 252
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/analytics/model/ScribeKeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 253
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    invoke-direct {v3, v0}, Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;-><init>(Ljava/util/List;)V

    .line 254
    invoke-virtual {v1, v2, p3, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 253
    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 255
    return-void
.end method

.method e(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 3

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/card/f;->g(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 113
    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 125
    :goto_0
    return-object v0

    .line 118
    :cond_0
    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 119
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_1

    .line 122
    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    :cond_1
    move-object v0, v1

    .line 125
    goto :goto_0
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 1

    .prologue
    .line 265
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/card/f;->f(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 267
    invoke-virtual {p0, v0}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 268
    return-void
.end method

.method f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    :cond_0
    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1, p2}, Lcom/twitter/library/util/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
