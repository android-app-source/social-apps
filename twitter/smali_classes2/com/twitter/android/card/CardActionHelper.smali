.class public Lcom/twitter/android/card/CardActionHelper;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/card/CardActionHelper$AppStatus;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/card/b;

.field private final b:Lcom/twitter/android/card/d;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/android/card/b;Lcom/twitter/android/card/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    .line 27
    iput-object p2, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    .line 28
    iput-object p3, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/android/card/CardActionHelper$AppStatus;
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lcom/twitter/android/card/CardActionHelper$AppStatus;->a:Lcom/twitter/android/card/CardActionHelper$AppStatus;

    .line 44
    :goto_0
    return-object v0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    invoke-interface {v0, p1}, Lcom/twitter/android/card/b;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    sget-object v0, Lcom/twitter/android/card/CardActionHelper$AppStatus;->b:Lcom/twitter/android/card/CardActionHelper$AppStatus;

    goto :goto_0

    .line 44
    :cond_1
    sget-object v0, Lcom/twitter/android/card/CardActionHelper$AppStatus;->c:Lcom/twitter/android/card/CardActionHelper$AppStatus;

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    invoke-interface {v0, p2, v1, v2}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 129
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    invoke-interface {v0, p1}, Lcom/twitter/android/card/b;->a(Landroid/content/Intent;)V

    .line 131
    return-void
.end method

.method public a(Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "show"

    iget-object v2, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p2}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 117
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->g:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v0, v1, p2}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 118
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    iget-object v1, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    invoke-interface {v1}, Lcom/twitter/android/card/d;->a()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/twitter/android/card/b;->a(Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 119
    return-void
.end method

.method public a(Lcom/twitter/library/card/e;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/twitter/library/card/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/card/e;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/twitter/android/card/CardActionHelper;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 51
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/card/e;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 3

    .prologue
    .line 94
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "open_link"

    iget-object v2, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p2}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v0, v1, p2}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    invoke-interface {v0}, Lcom/twitter/android/card/d;->b()Lcom/twitter/library/card/CardContext;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 103
    iget-object v2, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    invoke-interface {v2, p1}, Lcom/twitter/android/card/d;->b(Ljava/lang/String;)V

    .line 104
    iget-object v2, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    invoke-interface {v2, v1, v0, p1}, Lcom/twitter/android/card/b;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/card/CardContext;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    invoke-interface {v1, p1, v0}, Lcom/twitter/android/card/b;->a(Ljava/lang/String;Lcom/twitter/library/card/CardContext;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p0, p2}, Lcom/twitter/android/card/CardActionHelper;->a(Ljava/lang/String;)Lcom/twitter/android/card/CardActionHelper$AppStatus;

    move-result-object v0

    .line 56
    sget-object v1, Lcom/twitter/android/card/CardActionHelper$1;->a:[I

    invoke-virtual {v0}, Lcom/twitter/android/card/CardActionHelper$AppStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 83
    invoke-virtual {p0, p3, p4}, Lcom/twitter/android/card/CardActionHelper;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 59
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "open_app"

    iget-object v2, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p4}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 60
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->q:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v0, v1, p4}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/card/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "install_app"

    iget-object v2, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p4}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->r:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v0, v1, p4}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 70
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->a:Lcom/twitter/android/card/b;

    invoke-interface {v0, p2}, Lcom/twitter/android/card/b;->b(Ljava/lang/String;)Z

    move-result v0

    .line 71
    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "open_link"

    iget-object v2, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p4}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 76
    :cond_1
    const-string/jumbo v0, "post_installed_logging_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/twitter/android/card/CardActionHelper;->b:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/card/CardActionHelper;->c:Ljava/lang/String;

    invoke-interface {v0, p2, v1}, Lcom/twitter/android/card/d;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/card/CardActionHelper;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 91
    return-void
.end method
