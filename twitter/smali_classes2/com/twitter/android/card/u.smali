.class public Lcom/twitter/android/card/u;
.super Lcom/twitter/android/card/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/ad$a;
.implements Lcom/twitter/library/card/af$a;
.implements Lcom/twitter/library/card/q$a;
.implements Lcom/twitter/library/widget/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/card/u$b;,
        Lcom/twitter/android/card/u$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/card/ad;

.field private final b:Lcom/twitter/android/card/w;

.field private final c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

.field private final d:Lcom/twitter/android/card/t;

.field private final e:Lcom/twitter/android/card/u$b;

.field private final f:Landroid/view/ViewGroup;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcom/twitter/android/card/w;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Lcom/twitter/android/card/t;Lcom/twitter/android/card/u$a;Lcom/twitter/android/card/u$b;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 49
    new-instance v0, Lcom/twitter/library/card/ad;

    invoke-direct {v0, p0}, Lcom/twitter/library/card/ad;-><init>(Lcom/twitter/library/card/ad$a;)V

    iput-object v0, p0, Lcom/twitter/android/card/u;->a:Lcom/twitter/library/card/ad;

    .line 64
    iput-object p5, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    .line 65
    iput-object p6, p0, Lcom/twitter/android/card/u;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 66
    iput-object p7, p0, Lcom/twitter/android/card/u;->d:Lcom/twitter/android/card/t;

    .line 68
    invoke-virtual {p8}, Lcom/twitter/android/card/u$a;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/u;->f:Landroid/view/ViewGroup;

    .line 70
    iput-object p9, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    iget-object v1, p0, Lcom/twitter/android/card/u;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/u$b;->a(Landroid/view/ViewGroup;)V

    .line 72
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 100
    invoke-super {p0}, Lcom/twitter/android/card/n;->a()V

    .line 101
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->e()V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/card/u;->a:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->b()V

    .line 103
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/card/u;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/card/u;->d:Lcom/twitter/android/card/t;

    invoke-virtual {v0}, Lcom/twitter/android/card/t;->b()V

    .line 109
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 167
    const-string/jumbo v0, "PeriscopeCard"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Capi error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->a()V

    .line 169
    iget v0, p0, Lcom/twitter/android/card/u;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/card/u;->g:I

    .line 177
    return-void
.end method

.method public a(ILcar;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v0}, Lcom/twitter/android/card/w;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/u;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->a(Lcar;)V

    .line 157
    iget-object v0, p0, Lcom/twitter/android/card/u;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->c()V

    .line 159
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->d()V

    goto :goto_0

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->a()V

    goto :goto_0
.end method

.method public a(JLcar;)V
    .locals 9

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v0, p3}, Lcom/twitter/android/card/w;->a(Lcar;)V

    .line 133
    iget-object v0, p0, Lcom/twitter/android/card/u;->w:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/Tweet;

    .line 134
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    new-instance v1, Lcom/twitter/library/av/playback/PeriscopeDataSource;

    iget-object v3, p0, Lcom/twitter/android/card/u;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    iget-object v4, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    .line 135
    invoke-virtual {v4}, Lcom/twitter/android/card/w;->c()Lcom/twitter/model/card/property/Vector2F;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    .line 136
    invoke-virtual {v5}, Lcom/twitter/android/card/w;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v6}, Lcom/twitter/android/card/w;->g()J

    move-result-wide v6

    iget-object v8, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    .line 137
    invoke-virtual {v8}, Lcom/twitter/android/card/w;->e()Z

    move-result v8

    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/av/playback/PeriscopeDataSource;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Lcom/twitter/model/card/property/Vector2F;Ljava/lang/String;JZ)V

    .line 134
    invoke-virtual {v0, v1}, Lcom/twitter/android/card/u$b;->a(Lcom/twitter/library/av/playback/PeriscopeDataSource;)V

    .line 139
    iget-object v0, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v0}, Lcom/twitter/android/card/w;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->c()V

    .line 141
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->d()V

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->a()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 87
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    iget-object v1, p0, Lcom/twitter/android/card/u;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/u$b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    iget-object v1, p0, Lcom/twitter/android/card/u;->w:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/w;->a(Lcom/twitter/library/card/CardContext;)V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/card/u;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v0}, Lcom/twitter/android/card/w;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/twitter/android/card/u;->d:Lcom/twitter/android/card/t;

    iget-wide v2, p0, Lcom/twitter/android/card/u;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/android/card/t;->a(JLcom/twitter/library/card/af$a;)V

    .line 91
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/card/u;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/twitter/android/card/u;->a:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->a()V

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->b()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/u;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Z)V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/card/u$b;->a(Z)V

    .line 128
    return-void
.end method

.method public aj_()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Lcom/twitter/android/card/n;->aj_()V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/card/u;->a:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->d()V

    .line 115
    return-void
.end method

.method public as_()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/twitter/android/card/u;->d:Lcom/twitter/android/card/t;

    invoke-virtual {v0}, Lcom/twitter/android/card/t;->a()V

    .line 192
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->i()V

    .line 202
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->j()V

    .line 207
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->f()V

    .line 77
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->h()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Lcom/twitter/android/card/n;->d()V

    .line 120
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->g()V

    .line 121
    iget-object v0, p0, Lcom/twitter/android/card/u;->a:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->c()V

    .line 122
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/card/u;->f:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/android/card/u;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->a()I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/android/card/u;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/card/u;->g:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/u$b;

    invoke-virtual {v0}, Lcom/twitter/android/card/u$b;->k()V

    .line 212
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/card/u;->e()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
