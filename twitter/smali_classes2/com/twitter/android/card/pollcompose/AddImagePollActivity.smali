.class public Lcom/twitter/android/card/pollcompose/AddImagePollActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/imageeditor/EditImageFragment$b;
.implements Lcom/twitter/app/common/dialog/b$a;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/card/pollcompose/AddImagePollActivity$ImagePickerSelectDialog;,
        Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 67
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->a:Ljava/util/List;

    .line 66
    return-void
.end method

.method private a(Lrx/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<",
            "Lcom/twitter/media/model/MediaFile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 179
    new-instance v0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$3;-><init>(Lcom/twitter/android/card/pollcompose/AddImagePollActivity;)V

    invoke-virtual {p1, v0}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 184
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 185
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$2;-><init>(Lcom/twitter/android/card/pollcompose/AddImagePollActivity;)V

    .line 186
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 192
    return-void
.end method

.method private a(Ljava/util/List;)[Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;",
            ">;)[",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 108
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/CharSequence;

    .line 109
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 110
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;

    iget v0, v0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;->mStringResourceId:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 109
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 112
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 74
    invoke-virtual {p0, v0, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->overridePendingTransition(II)V

    .line 75
    const v0, 0x7f0400da

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 76
    return-object p2
.end method

.method public a(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    .line 203
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;

    .line 208
    sget-object v1, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$4;->a:[I

    invoke-virtual {v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 228
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    .line 232
    :goto_0
    return-void

    .line 210
    :pswitch_0
    const/4 v0, 0x1

    const-string/jumbo v1, ":composition:poll_compose:twitter_camera"

    .line 211
    invoke-static {p0, v0, v1}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x201

    .line 210
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 219
    :pswitch_1
    const/16 v0, 0x202

    invoke-static {p0, v0}, Lbrv;->a(Landroid/app/Activity;I)Z

    goto :goto_0

    .line 223
    :pswitch_2
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->setResult(ILandroid/content/Intent;)V

    .line 224
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    goto :goto_0

    .line 208
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 83
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 86
    const-string/jumbo v2, "has_image"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 87
    if-eqz v1, :cond_0

    .line 88
    sget-object v1, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;->c:Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 91
    :cond_0
    invoke-static {p0}, Lcom/twitter/android/util/b;->a(Landroid/content/Context;)Z

    move-result v1

    .line 92
    if-eqz v1, :cond_1

    .line 93
    sget-object v1, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;->a:Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 96
    :cond_1
    sget-object v1, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;->b:Lcom/twitter/android/card/pollcompose/AddImagePollActivity$DialogChoiceOption;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 97
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->a:Ljava/util/List;

    .line 99
    iget-object v0, p0, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->a:Ljava/util/List;

    .line 100
    invoke-direct {p0, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->a(Ljava/util/List;)[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$ImagePickerSelectDialog;->a([Ljava/lang/CharSequence;)Lcom/twitter/android/card/pollcompose/AddImagePollActivity$ImagePickerSelectDialog;

    move-result-object v0

    .line 101
    invoke-virtual {v0, p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$ImagePickerSelectDialog;->a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 102
    invoke-virtual {v0, p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$ImagePickerSelectDialog;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$ImagePickerSelectDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 104
    return-void
.end method

.method a(Lcom/twitter/model/media/EditableImage;)V
    .locals 4

    .prologue
    .line 160
    .line 161
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "image_edit"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 163
    if-nez v0, :cond_0

    .line 164
    new-instance v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    invoke-direct {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;-><init>()V

    .line 165
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "composition"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v0

    const/4 v1, 0x2

    .line 166
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(I)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 167
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(Z)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 168
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(F)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a()Lcom/twitter/android/media/imageeditor/EditImageFragment;

    move-result-object v0

    .line 170
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f1302e4

    const-string/jumbo v3, "image_edit"

    .line 171
    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 172
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 174
    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableImage;)V

    .line 175
    invoke-virtual {v0, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment$b;)V

    .line 176
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 236
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 237
    const-string/jumbo v1, "extra_editable_image"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 238
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->setResult(ILandroid/content/Intent;)V

    .line 239
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    .line 240
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->setResult(I)V

    .line 245
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    .line 246
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 196
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 197
    invoke-virtual {p0, v0, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->overridePendingTransition(II)V

    .line 198
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 117
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 118
    packed-switch p1, :pswitch_data_0

    .line 154
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    .line 157
    :goto_0
    return-void

    .line 120
    :pswitch_0
    if-ne p2, v0, :cond_0

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const/4 v0, 0x1

    .line 122
    invoke-static {p0, v0, v1, v1}, Lcom/twitter/android/media/camera/CameraActivity;->a(Landroid/content/Context;IZZ)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x203

    .line 121
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    goto :goto_0

    .line 130
    :pswitch_1
    if-ne p2, v0, :cond_1

    .line 131
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity$1;-><init>(Lcom/twitter/android/card/pollcompose/AddImagePollActivity;)V

    .line 132
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 138
    invoke-direct {p0, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->a(Lrx/c;)V

    goto :goto_0

    .line 140
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    goto :goto_0

    .line 145
    :pswitch_2
    if-ne p2, v0, :cond_2

    .line 146
    const-string/jumbo v0, "media_file"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 147
    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->a(Lrx/c;)V

    goto :goto_0

    .line 149
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/card/pollcompose/AddImagePollActivity;->finish()V

    goto :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
