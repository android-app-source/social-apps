.class public Lcom/twitter/android/card/r;
.super Lcom/twitter/android/card/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/ak$a;
.implements Lcom/twitter/library/card/q$a;
.implements Lcom/twitter/library/widget/a;


# instance fields
.field private final a:Lcom/twitter/android/moments/viewmodels/ab;

.field private final b:Lcom/twitter/android/moments/ui/card/f;

.field private c:Lcom/twitter/android/moments/viewmodels/aa;

.field private d:Lcom/twitter/model/moments/a;

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcom/twitter/android/moments/viewmodels/ab;Lcom/twitter/android/moments/ui/card/f;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 47
    iput-object p6, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    .line 48
    iput-object p5, p0, Lcom/twitter/android/card/r;->a:Lcom/twitter/android/moments/viewmodels/ab;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/moments/viewmodels/ab;Lcom/twitter/android/moments/ui/card/f;)V
    .locals 7

    .prologue
    .line 37
    new-instance v3, Lcom/twitter/android/card/f;

    invoke-direct {v3, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/twitter/android/card/c;

    invoke-direct {v4, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/card/r;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcom/twitter/android/moments/viewmodels/ab;Lcom/twitter/android/moments/ui/card/f;)V

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/card/r;)Lcom/twitter/android/moments/viewmodels/aa;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/card/r;->c:Lcom/twitter/android/moments/viewmodels/aa;

    return-object v0
.end method

.method private q()V
    .locals 4

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/twitter/android/card/r;->f:Z

    if-nez v0, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/card/r;->j()Lcom/twitter/library/card/q;

    move-result-object v0

    .line 160
    iget-wide v2, p0, Lcom/twitter/android/card/r;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/card/r;->f:Z

    .line 163
    :cond_0
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/twitter/android/card/r;->f:Z

    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/twitter/android/card/r;->j()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/card/r;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/card/r;->f:Z

    .line 170
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 90
    invoke-super {p0}, Lcom/twitter/android/card/n;->a()V

    .line 91
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->c()V

    .line 92
    invoke-direct {p0}, Lcom/twitter/android/card/r;->s()V

    .line 93
    invoke-virtual {p0}, Lcom/twitter/android/card/r;->k()Lcom/twitter/library/card/ak;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/card/r;->e:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->b(JLjava/lang/Object;)V

    .line 94
    return-void
.end method

.method public a(JLcar;)V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/card/r;->a:Lcom/twitter/android/moments/viewmodels/ab;

    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/viewmodels/ab;->a(Lcar;)Lcom/twitter/android/moments/viewmodels/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/r;->c:Lcom/twitter/android/moments/viewmodels/aa;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    iget-object v1, p0, Lcom/twitter/android/card/r;->c:Lcom/twitter/android/moments/viewmodels/aa;

    iget-object v2, p0, Lcom/twitter/android/card/r;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/moments/ui/card/f;->a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-virtual {p0}, Lcom/twitter/android/card/r;->p()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/moments/ui/card/f;->a(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method

.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 77
    invoke-static {p3}, Lcom/twitter/android/moments/viewmodels/aa;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/moments/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/r;->d:Lcom/twitter/model/moments/a;

    .line 78
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0, p3}, Lcom/twitter/android/moments/ui/card/f;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 79
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    iget-object v1, p0, Lcom/twitter/android/card/r;->d:Lcom/twitter/model/moments/a;

    invoke-interface {v0, v1}, Lcom/twitter/android/moments/ui/card/f;->a(Lcom/twitter/model/moments/a;)V

    .line 80
    invoke-direct {p0}, Lcom/twitter/android/card/r;->q()V

    .line 81
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 55
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/card/r;->y:J

    .line 56
    iget-object v0, p1, Lcom/twitter/library/card/z$a;->c:Lcar;

    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/aa;->a(Lcar;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/card/r;->e:J

    .line 59
    iget-wide v0, p0, Lcom/twitter/android/card/r;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/card/r;->k()Lcom/twitter/library/card/ak;

    move-result-object v0

    .line 61
    iget-wide v2, p0, Lcom/twitter/android/card/r;->e:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->a(JLjava/lang/Object;)V

    .line 66
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/twitter/android/moments/ui/card/f;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 64
    invoke-direct {p0}, Lcom/twitter/android/card/r;->q()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/r;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->d()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->at_()V

    .line 139
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->d()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 144
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->b()V

    .line 86
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->d()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->c()Z

    move-result v0

    return v0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->e()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->d()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->h()V

    .line 149
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/card/r;->b:Lcom/twitter/android/moments/ui/card/f;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/f;->d()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected j()Lcom/twitter/library/card/q;
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    return-object v0
.end method

.method protected k()Lcom/twitter/library/card/ak;
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    return-object v0
.end method

.method p()Landroid/view/View$OnClickListener;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/twitter/android/card/r$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/r$1;-><init>(Lcom/twitter/android/card/r;)V

    return-object v0
.end method
