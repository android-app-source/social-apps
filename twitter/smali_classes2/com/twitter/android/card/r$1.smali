.class Lcom/twitter/android/card/r$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/card/r;->p()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/card/r;


# direct methods
.method constructor <init>(Lcom/twitter/android/card/r;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    invoke-static {v0}, Lcom/twitter/android/card/r;->a(Lcom/twitter/android/card/r;)Lcom/twitter/android/moments/viewmodels/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    invoke-virtual {v0}, Lcom/twitter/android/card/r;->l()Landroid/app/Activity;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    invoke-static {v1}, Lcom/twitter/android/card/r;->a(Lcom/twitter/android/card/r;)Lcom/twitter/android/moments/viewmodels/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/aa;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 120
    iget-object v1, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    iget-object v1, v1, Lcom/twitter/android/card/r;->v:Lcom/twitter/android/card/CardActionHelper;

    iget-object v2, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    .line 122
    invoke-static {v2}, Lcom/twitter/android/card/r;->a(Lcom/twitter/android/card/r;)Lcom/twitter/android/moments/viewmodels/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/moments/viewmodels/aa;->b()J

    move-result-wide v2

    .line 121
    invoke-static {v0, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->c(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "open_link"

    .line 120
    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/card/CardActionHelper;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    iget-object v0, v0, Lcom/twitter/android/card/r;->v:Lcom/twitter/android/card/CardActionHelper;

    iget-object v1, p0, Lcom/twitter/android/card/r$1;->a:Lcom/twitter/android/card/r;

    invoke-static {v1}, Lcom/twitter/android/card/r;->a(Lcom/twitter/android/card/r;)Lcom/twitter/android/moments/viewmodels/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/aa;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/CardActionHelper;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
