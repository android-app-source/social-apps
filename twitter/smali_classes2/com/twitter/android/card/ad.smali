.class public Lcom/twitter/android/card/ad;
.super Lcom/twitter/library/card/aa;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/library/card/aa;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/z;
    .locals 3

    .prologue
    .line 29
    invoke-static {p2}, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    new-instance v1, Lcom/twitter/android/card/q;

    invoke-direct {v1}, Lcom/twitter/android/card/q;-><init>()V

    .line 31
    new-instance v0, Lcom/twitter/android/card/p;

    invoke-direct {v0}, Lcom/twitter/android/card/p;-><init>()V

    .line 37
    :goto_0
    sget-object v2, Lcom/twitter/android/revenue/card/ah;->a:Ljava/util/List;

    invoke-static {p3, v2}, Lcom/twitter/android/revenue/k;->a(Lcar;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "website_cardsize_downgrade_5436"

    .line 38
    invoke-static {v2}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 39
    :cond_0
    new-instance v2, Lcom/twitter/android/revenue/card/ai;

    invoke-direct {v2, p1, p2, v1, v0}, Lcom/twitter/android/revenue/card/ai;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    move-object v0, v2

    .line 42
    :goto_1
    return-object v0

    .line 33
    :cond_1
    new-instance v1, Lcom/twitter/android/card/f;

    invoke-direct {v1, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    .line 34
    new-instance v0, Lcom/twitter/android/card/c;

    invoke-direct {v0, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 42
    :cond_2
    new-instance v2, Lcom/twitter/android/revenue/card/ah;

    invoke-direct {v2, p1, p2, v1, v0}, Lcom/twitter/android/revenue/card/ah;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    move-object v0, v2

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x1

    return v0
.end method
