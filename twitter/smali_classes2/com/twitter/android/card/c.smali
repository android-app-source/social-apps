.class public Lcom/twitter/android/card/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/card/b;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    .line 60
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    .line 61
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/card/c;->c:J

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/card/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    return-object v0
.end method

.method private e(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 248
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 256
    :goto_0
    return v0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 252
    if-nez v0, :cond_1

    move v0, v1

    .line 253
    goto :goto_0

    .line 255
    :cond_1
    iget-wide v2, p0, Lcom/twitter/android/card/c;->c:J

    invoke-static {v0, p1, v2, v3}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;J)V

    .line 256
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(JLcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 166
    invoke-static {p3}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    .line 167
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    invoke-static {p1, p2, v1, p4, v0}, Lcom/twitter/android/profiles/v;->a(JLcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 390
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 391
    iget-object v0, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 392
    return-void
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/card/CardContext;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 292
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 293
    if-nez v1, :cond_0

    .line 300
    :goto_0
    return-void

    .line 297
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 298
    invoke-static {p2}, Lcom/twitter/library/client/BrowserDataSourceFactory;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/library/client/BrowserDataSource;

    move-result-object v2

    move-object v3, p3

    move-object v7, v6

    move-object v8, p1

    invoke-static/range {v1 .. v8}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 4

    .prologue
    .line 238
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 239
    if-nez v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 242
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "tw"

    .line 243
    invoke-static {p1}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    .line 244
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 242
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/card/CardContext;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 262
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    .line 263
    if-nez v5, :cond_0

    .line 287
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-static {}, Lcom/twitter/android/client/OpenUriHelper;->a()Lcom/twitter/android/client/OpenUriHelper;

    move-result-object v6

    new-instance v0, Lcom/twitter/android/card/c$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/card/c$2;-><init>(Lcom/twitter/android/card/c;Lcom/twitter/library/client/Session;Lcom/twitter/library/card/CardContext;Ljava/lang/String;Landroid/app/Activity;)V

    invoke-virtual {v6, v5, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 67
    if-nez v0, :cond_0

    .line 75
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 71
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v2

    const/4 v3, 0x0

    .line 72
    invoke-virtual {v2, p1, v3}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 73
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 74
    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;JLcax;Lcgi;I)V
    .locals 8

    .prologue
    .line 367
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 368
    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p4, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 372
    invoke-static {}, Lcom/twitter/android/card/h;->b()Lcom/twitter/android/card/h;

    move-result-object v2

    .line 373
    invoke-virtual {v2, p2, p3}, Lcom/twitter/android/card/h;->a(J)V

    .line 374
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 376
    invoke-static {p6}, Lbso;->a(I)Lbso;

    move-result-object v3

    invoke-static {v3}, Lbsr;->a(Lbsr$a;)Ljava/lang/String;

    move-result-object v3

    .line 377
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 378
    invoke-virtual {p4}, Lcax;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 379
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 380
    invoke-virtual {v1, p2, p3}, Lcom/twitter/android/composer/a;->c(J)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 381
    invoke-virtual {v1, p5}, Lcom/twitter/android/composer/a;->a(Lcgi;)Lcom/twitter/android/composer/a;

    move-result-object v1

    const/4 v2, 0x1

    .line 382
    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/a;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/composer/a;

    .line 383
    invoke-virtual {v1, p4}, Lcom/twitter/android/composer/a;->a(Lcax;)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 384
    invoke-virtual {v1, v3}, Lcom/twitter/android/composer/a;->d(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 385
    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 377
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/card/CardContext;)V
    .locals 4

    .prologue
    .line 318
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 319
    if-nez v0, :cond_0

    .line 323
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-wide v2, p0, Lcom/twitter/android/card/c;->c:J

    invoke-static {p2}, Lcom/twitter/library/client/BrowserDataSourceFactory;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/library/client/BrowserDataSource;

    move-result-object v1

    invoke-static {v0, p1, v2, v3, v1}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;JLcom/twitter/library/client/BrowserDataSource;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 80
    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 83
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "text/plain"

    .line 84
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.extra.TEXT"

    .line 85
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 86
    invoke-static {v1, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/twitter/library/card/CardContext;)V
    .locals 10

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 94
    if-nez v3, :cond_0

    .line 132
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-static {}, Lcom/twitter/android/client/OpenUriHelper;->a()Lcom/twitter/android/client/OpenUriHelper;

    move-result-object v9

    new-instance v0, Lcom/twitter/android/card/c$1;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p1

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/card/c$1;-><init>(Lcom/twitter/android/card/c;Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;ZZLjava/lang/String;Lcom/twitter/library/card/CardContext;)V

    invoke-virtual {v9, v3, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;I",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 152
    if-nez v0, :cond_0

    .line 160
    :goto_0
    return-void

    .line 155
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "association"

    .line 156
    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "li"

    .line 157
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_starting_index"

    .line 158
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 159
    const/16 v2, 0x23bf

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/e;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p1}, Lcom/twitter/library/card/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/card/e;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/card/c;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 202
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/card/e;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 180
    iget-object v1, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/twitter/util/u;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 181
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 182
    iget-object v3, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 183
    invoke-virtual {v3, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 184
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 185
    invoke-direct {p0, v1}, Lcom/twitter/android/card/c;->e(Ljava/lang/String;)Z

    move-result v1

    .line 186
    if-eqz v1, :cond_0

    .line 187
    const/4 v0, 0x1

    .line 190
    :cond_0
    return v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 212
    iget-object v0, p0, Lcom/twitter/android/card/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 213
    if-nez v0, :cond_0

    move v0, v1

    .line 232
    :goto_0
    return v0

    .line 217
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/card/c;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 218
    invoke-direct {p0, p1}, Lcom/twitter/android/card/c;->e(Ljava/lang/String;)Z

    move-result v2

    .line 219
    if-eqz v2, :cond_1

    .line 220
    const/4 v0, 0x1

    goto :goto_0

    .line 224
    :cond_1
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 225
    iget-object v2, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 226
    invoke-virtual {v2, p2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 227
    if-eqz v2, :cond_2

    .line 228
    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    move v0, v1

    .line 232
    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/card/c;->a(Ljava/lang/String;Lcom/twitter/library/card/CardContext;)V

    .line 309
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 332
    invoke-static {p2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v0

    .line 335
    :cond_1
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 336
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 337
    iget-object v2, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 338
    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 340
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/twitter/util/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 354
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    const/4 v0, 0x0

    .line 357
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/c;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/util/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
