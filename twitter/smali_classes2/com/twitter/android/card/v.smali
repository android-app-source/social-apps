.class public Lcom/twitter/android/card/v;
.super Lcom/twitter/library/card/aa;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/card/v$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/card/aa;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/library/card/aa;-><init>()V

    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 24
    new-instance v0, Lcom/twitter/android/card/v$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/card/v$a;-><init>(Lcom/twitter/android/card/v$1;)V

    iput-object v0, p0, Lcom/twitter/android/card/v;->a:Lcom/twitter/library/card/aa;

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    new-instance v0, Lcom/twitter/android/card/ad;

    invoke-direct {v0}, Lcom/twitter/android/card/ad;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/card/v;->a:Lcom/twitter/library/card/aa;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/z;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/card/v;->a:Lcom/twitter/library/card/aa;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/library/card/aa;->a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/z;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/card/v;->a:Lcom/twitter/library/card/aa;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/card/aa;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v0

    return v0
.end method
