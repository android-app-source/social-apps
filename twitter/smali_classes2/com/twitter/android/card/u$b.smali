.class Lcom/twitter/android/card/u$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/card/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/android/card/w;

.field private final c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

.field private final d:Lcom/twitter/android/av/video/e$b;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/view/ViewGroup;

.field private g:Landroid/view/View;

.field private h:Lcom/twitter/android/av/video/e;

.field private i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private j:Lcom/twitter/android/av/PeriscopeBadge;

.field private k:Landroid/widget/ImageView;

.field private l:Lcom/twitter/library/av/playback/PeriscopeDataSource;

.field private m:Ljava/lang/String;

.field private n:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/android/card/w;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Lcom/twitter/android/av/video/e$b;)V
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->m:Ljava/lang/String;

    .line 254
    iput-object p1, p0, Lcom/twitter/android/card/u$b;->a:Landroid/app/Activity;

    .line 255
    iput-object p2, p0, Lcom/twitter/android/card/u$b;->b:Lcom/twitter/android/card/w;

    .line 256
    iput-object p3, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 257
    iput-object p4, p0, Lcom/twitter/android/card/u$b;->d:Lcom/twitter/android/av/video/e$b;

    .line 258
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_0

    .line 345
    invoke-direct {p0}, Lcom/twitter/android/card/u$b;->m()V

    .line 350
    :goto_0
    return-void

    .line 348
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/card/u$b;->n()V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 353
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->f:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 358
    :cond_0
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 361
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->f:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 364
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->f:Landroid/view/ViewGroup;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lcom/twitter/util/e;->b(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    .line 366
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/twitter/android/card/u$b;->l()V

    .line 262
    const-string/jumbo v0, "PeriscopeCard"

    const-string/jumbo v1, "onCardData error"

    invoke-static {v0, v1}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 271
    const v0, 0x7f130396

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    .line 272
    const v0, 0x7f1305a0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->f:Landroid/view/ViewGroup;

    .line 273
    const v0, 0x7f130594

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->g:Landroid/view/View;

    .line 274
    const v0, 0x7f1305a1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/twitter/android/card/u$b;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 286
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/PeriscopeDataSource;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/twitter/android/card/u$b;->l:Lcom/twitter/library/av/playback/PeriscopeDataSource;

    .line 282
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->d()V

    .line 392
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/twitter/android/card/u$b;->l()V

    .line 267
    const-string/jumbo v0, "PeriscopeCard"

    const-string/jumbo v1, "onAttach error"

    invoke-static {v0, v1}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    return-void
.end method

.method public c()V
    .locals 10

    .prologue
    .line 289
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->l:Lcom/twitter/library/av/playback/PeriscopeDataSource;

    if-eqz v0, :cond_4

    .line 297
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->m:Ljava/lang/String;

    const-string/jumbo v1, "RUNNING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 298
    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 300
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->l:Lcom/twitter/library/av/playback/PeriscopeDataSource;

    .line 301
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/PeriscopeDataSource;->b()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 303
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->d:Lcom/twitter/android/av/video/e$b;

    iget-object v1, p0, Lcom/twitter/android/card/u$b;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    new-instance v3, Lcom/twitter/android/av/aa;

    iget-object v4, p0, Lcom/twitter/android/card/u$b;->b:Lcom/twitter/android/card/w;

    iget-object v5, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-direct {v3, v4, v5}, Lcom/twitter/android/av/aa;-><init>(Lcom/twitter/android/card/w;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;)V

    new-instance v4, Lcom/twitter/android/av/ah;

    invoke-direct {v4}, Lcom/twitter/android/av/ah;-><init>()V

    .line 309
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/av/u;

    invoke-direct {v6}, Lcom/twitter/android/av/u;-><init>()V

    iget-object v7, p0, Lcom/twitter/android/card/u$b;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v8, p0, Lcom/twitter/android/card/u$b;->l:Lcom/twitter/library/av/playback/PeriscopeDataSource;

    const/4 v9, 0x0

    .line 306
    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/av/video/e$b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/av/ad;Lcom/twitter/android/av/ah;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/u;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    .line 311
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    sget-object v1, Lbyo;->c:Lbyf;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->j:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/video/e;->a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 312
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    const v1, 0x7f13061f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/PeriscopeBadge;

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->j:Lcom/twitter/android/av/PeriscopeBadge;

    .line 313
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    const v1, 0x7f130624

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->k:Landroid/widget/ImageView;

    .line 314
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->e:Landroid/view/ViewGroup;

    const v1, 0x7f130622

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_2

    .line 316
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/card/u$b;->n:Z

    if-eqz v0, :cond_3

    .line 322
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 326
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->m:Ljava/lang/String;

    .line 328
    :cond_4
    return-void

    .line 298
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->k:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->j:Lcom/twitter/android/av/PeriscopeBadge;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/card/u$b;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020715

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 334
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->j:Lcom/twitter/android/av/PeriscopeBadge;

    iget-object v1, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->e()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/card/u$b;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v2}, Lcom/twitter/android/card/w;->e()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/PeriscopeBadge;->a(IZ)V

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/card/u$b;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020714

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 337
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->j:Lcom/twitter/android/av/PeriscopeBadge;

    iget-object v1, p0, Lcom/twitter/android/card/u$b;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->e()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/card/u$b;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v2}, Lcom/twitter/android/card/w;->e()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/PeriscopeBadge;->b(IZ)V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 371
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    .line 373
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/card/u$b;->n:Z

    .line 377
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 380
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->b()V

    .line 386
    :cond_0
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/card/u$b;->b:Lcom/twitter/android/card/w;

    invoke-virtual {v0}, Lcom/twitter/android/card/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "video_threesixty_inline_enabled"

    .line 412
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 411
    :goto_0
    return v0

    .line 412
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->at_()V

    .line 419
    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->au_()V

    .line 425
    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/twitter/android/card/u$b;->h:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->h()V

    .line 431
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 396
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 408
    :goto_0
    return-void

    .line 399
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/card/u$b;->a:Landroid/app/Activity;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "search_type"

    const/16 v2, 0xc

    .line 400
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 401
    iget-object v1, p0, Lcom/twitter/android/card/u$b;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 396
    :sswitch_data_0
    .sparse-switch
        0x7f1305a1 -> :sswitch_0
        0x7f130622 -> :sswitch_0
    .end sparse-switch
.end method
