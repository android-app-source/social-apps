.class public Lcom/twitter/android/card/af;
.super Lcom/twitter/android/card/a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/card/af$a;
    }
.end annotation


# instance fields
.field final z:Lcom/twitter/android/av/video/VideoContainerHost;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/android/card/f;

    invoke-direct {v0, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/twitter/android/card/c;

    invoke-direct {v1, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/card/af;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 44
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V
    .locals 7

    .prologue
    .line 48
    .line 49
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04024a

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    .line 50
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    new-instance v6, Lcom/twitter/android/card/af$a;

    invoke-direct {v6}, Lcom/twitter/android/card/af$a;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 48
    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/card/af;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Landroid/widget/LinearLayout;Lcom/twitter/android/card/a$a;)V

    .line 53
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Landroid/widget/LinearLayout;Lcom/twitter/android/card/a$a;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/card/a;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Landroid/widget/LinearLayout;Lcom/twitter/android/card/a$a;)V

    .line 59
    iget-object v0, p0, Lcom/twitter/android/card/af;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f130107

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/video/VideoContainerHost;

    iput-object v0, p0, Lcom/twitter/android/card/af;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    .line 60
    return-void
.end method


# virtual methods
.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/card/a;->a(JLcom/twitter/model/core/TwitterUser;)V

    .line 77
    iget-object v0, p0, Lcom/twitter/android/card/af;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/twitter/android/card/af;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 80
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 6

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/twitter/android/card/a;->a(Lcom/twitter/library/card/z$a;)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/android/card/af;->l()Landroid/app/Activity;

    move-result-object v1

    .line 66
    iget-object v0, p0, Lcom/twitter/android/card/af;->w:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/twitter/model/core/Tweet;

    .line 67
    if-eqz v1, :cond_0

    .line 68
    new-instance v0, Lcom/twitter/android/av/video/f;

    new-instance v1, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v1, v2}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    iget-object v2, p0, Lcom/twitter/android/card/af;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    sget-object v3, Lbyo;->c:Lbyf;

    sget-object v4, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/av/video/f;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v1, p0, Lcom/twitter/android/card/af;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/video/VideoContainerHost;->setVideoContainerConfig(Lcom/twitter/android/av/video/f;)V

    .line 72
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/af;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/card/af;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->at_()V

    .line 90
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/android/card/af;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 95
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/card/af;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->c()Z

    move-result v0

    return v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/twitter/android/card/af;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->h()V

    .line 100
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/twitter/android/card/af;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method j()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/card/af;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    return-object v0
.end method
