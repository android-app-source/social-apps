.class public interface abstract Lcom/twitter/android/card/b;
.super Ljava/lang/Object;
.source "Twttr"


# virtual methods
.method public abstract a(JLcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
.end method

.method public abstract a(Landroid/content/Intent;)V
.end method

.method public abstract a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/card/CardContext;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
.end method

.method public abstract a(Lcom/twitter/library/client/Session;Lcom/twitter/library/card/CardContext;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;JLcax;Lcgi;I)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/twitter/library/card/CardContext;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/twitter/library/card/CardContext;)V
.end method

.method public abstract a(Ljava/util/ArrayList;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;I",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/twitter/library/card/e;Ljava/lang/String;)Z
.end method

.method public abstract b(Ljava/lang/String;)Z
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract c(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract d(Ljava/lang/String;)Z
.end method
