.class public Lcom/twitter/android/card/g;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(JLandroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 31
    if-eqz p3, :cond_1

    .line 32
    invoke-virtual {p3}, Lcom/twitter/library/card/CardContext;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    invoke-virtual {p3}, Lcom/twitter/library/card/CardContext;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 36
    :cond_0
    invoke-virtual {p3}, Lcom/twitter/library/card/CardContext;->f()Lcom/twitter/library/scribe/ScribeItemsProvider;

    move-result-object v1

    .line 35
    invoke-static {v0, p2, v1, p4, p5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/library/scribe/ScribeItemsProvider;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 38
    :cond_1
    return-object v0
.end method

.method public static a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, p0, :cond_0

    const-string/jumbo v0, "platform_forward_card"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "platform_card"

    goto :goto_0
.end method
