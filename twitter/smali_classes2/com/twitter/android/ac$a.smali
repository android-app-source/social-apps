.class Lcom/twitter/android/ac$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/InlineDismissView$a;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/ac;

.field private final b:Lcom/twitter/library/widget/InlineDismissView$a;


# direct methods
.method constructor <init>(Lcom/twitter/android/ac;Lcom/twitter/library/widget/InlineDismissView$a;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/twitter/android/ac$a;->a:Lcom/twitter/android/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    iput-object p2, p0, Lcom/twitter/android/ac$a;->b:Lcom/twitter/library/widget/InlineDismissView$a;

    .line 405
    return-void
.end method

.method private a(Lcom/twitter/library/widget/InlineDismissView;)Lcom/twitter/android/timeline/bk;
    .locals 1

    .prologue
    .line 450
    const v0, 0x7f13007e

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineDismissView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 4

    .prologue
    .line 409
    invoke-direct {p0, p1}, Lcom/twitter/android/ac$a;->a(Lcom/twitter/library/widget/InlineDismissView;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    .line 410
    if-eqz v0, :cond_0

    .line 411
    iget-object v1, p0, Lcom/twitter/android/ac$a;->a:Lcom/twitter/android/ac;

    iget-wide v2, v0, Lcom/twitter/android/timeline/bk;->d:J

    invoke-virtual {v1, v2, v3, p2}, Lcom/twitter/android/ac;->a(JLcom/twitter/model/timeline/g;)Lcom/twitter/model/timeline/g;

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ac$a;->b:Lcom/twitter/library/widget/InlineDismissView$a;

    if-eqz v0, :cond_1

    .line 414
    iget-object v0, p0, Lcom/twitter/android/ac$a;->b:Lcom/twitter/library/widget/InlineDismissView$a;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/widget/InlineDismissView$a;->a(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V

    .line 416
    :cond_1
    return-void
.end method

.method public b(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 4

    .prologue
    .line 420
    invoke-direct {p0, p1}, Lcom/twitter/android/ac$a;->a(Lcom/twitter/library/widget/InlineDismissView;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_0

    .line 422
    iget-object v1, p0, Lcom/twitter/android/ac$a;->a:Lcom/twitter/android/ac;

    iget-wide v2, v0, Lcom/twitter/android/timeline/bk;->d:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/ac;->f(J)Lcom/twitter/model/timeline/g;

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ac$a;->b:Lcom/twitter/library/widget/InlineDismissView$a;

    if-eqz v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/twitter/android/ac$a;->b:Lcom/twitter/library/widget/InlineDismissView$a;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/widget/InlineDismissView$a;->b(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V

    .line 427
    :cond_1
    return-void
.end method

.method public c(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 3

    .prologue
    .line 431
    invoke-direct {p0, p1}, Lcom/twitter/android/ac$a;->a(Lcom/twitter/library/widget/InlineDismissView;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    .line 432
    if-eqz v0, :cond_0

    .line 433
    iget-wide v0, v0, Lcom/twitter/android/timeline/bk;->d:J

    .line 434
    iget-object v2, p0, Lcom/twitter/android/ac$a;->a:Lcom/twitter/android/ac;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/ac;->f(J)Lcom/twitter/model/timeline/g;

    .line 435
    iget-object v2, p0, Lcom/twitter/android/ac$a;->a:Lcom/twitter/android/ac;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/ac;->d(J)Z

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ac$a;->b:Lcom/twitter/library/widget/InlineDismissView$a;

    if-eqz v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/twitter/android/ac$a;->b:Lcom/twitter/library/widget/InlineDismissView$a;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/widget/InlineDismissView$a;->c(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V

    .line 440
    :cond_1
    return-void
.end method
