.class public Lcom/twitter/android/r;
.super Lcom/twitter/android/AbsPagesAdapter;
.source "Twttr"


# instance fields
.field private final g:Lcom/twitter/android/q;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;Lcom/twitter/android/q;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/internal/android/widget/HorizontalListView;",
            "Lcom/twitter/android/at;",
            "Lcom/twitter/android/q;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/AbsPagesAdapter;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    .line 32
    iput-object p6, p0, Lcom/twitter/android/r;->g:Lcom/twitter/android/q;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;Lcom/twitter/internal/android/widget/DockLayout;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/internal/android/widget/HorizontalListView;",
            "Lcom/twitter/android/at;",
            "Lcom/twitter/internal/android/widget/DockLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v6, Lcom/twitter/android/q;

    invoke-direct {v6, p6}, Lcom/twitter/android/q;-><init>(Lcom/twitter/internal/android/widget/DockLayout;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/r;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;Lcom/twitter/android/q;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/app/common/list/TwitterListFragment;I)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsPagesAdapter;->a(Lcom/twitter/app/common/base/BaseFragment;I)V

    .line 46
    new-instance v0, Lcno$a;

    invoke-direct {v0}, Lcno$a;-><init>()V

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/r;->g:Lcom/twitter/android/q;

    .line 47
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/refresh/widget/RefreshableListView$e;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 48
    return-void
.end method

.method d(I)V
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/twitter/android/r;->f:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/r;->c(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/r;->a(Lcom/twitter/library/client/m;)Z

    .line 58
    invoke-virtual {p0, p1}, Lcom/twitter/android/r;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    .line 59
    invoke-virtual {p0, v0}, Lcom/twitter/android/r;->b(Lcom/twitter/library/client/m;)Z

    .line 60
    iput p1, p0, Lcom/twitter/android/r;->f:I

    .line 61
    return-void
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    .line 38
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsPagesAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/TwitterListFragment;

    .line 39
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/r;->a(Lcom/twitter/app/common/list/TwitterListFragment;I)V

    .line 40
    return-object v0
.end method

.method public onPageSelected(I)V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageSelected(I)V

    .line 53
    invoke-virtual {p0, p1}, Lcom/twitter/android/r;->d(I)V

    .line 54
    return-void
.end method
