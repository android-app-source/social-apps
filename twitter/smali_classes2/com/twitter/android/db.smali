.class public Lcom/twitter/android/db;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final c:Lcom/twitter/ui/user/BaseUserView;

.field public d:J

.field public e:J

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/ui/user/BaseUserView;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/twitter/ui/user/BaseUserView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/db;->c:Lcom/twitter/ui/user/BaseUserView;

    .line 26
    iput-object p2, p0, Lcom/twitter/android/db;->j:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iput-wide v0, p0, Lcom/twitter/android/db;->e:J

    .line 31
    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    .line 32
    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/db;->h:Ljava/lang/String;

    .line 33
    return-void
.end method
