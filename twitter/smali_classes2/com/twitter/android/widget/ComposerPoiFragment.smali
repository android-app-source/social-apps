.class public Lcom/twitter/android/widget/ComposerPoiFragment;
.super Lcom/twitter/android/widget/ComposerLocationFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/android/geo/e$a;
.implements Lcom/twitter/android/widget/au$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/ComposerPoiFragment$b;,
        Lcom/twitter/android/widget/ComposerPoiFragment$a;,
        Lcom/twitter/android/widget/ComposerPoiFragment$c;
    }
.end annotation


# instance fields
.field private A:Z

.field private final B:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/widget/ComposerPoiFragment$b;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

.field private E:Lcom/twitter/util/ui/d;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/ProgressBar;

.field private m:Landroid/widget/ListView;

.field private n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

.field private o:Lcom/twitter/android/geo/c;

.field private p:Lcom/twitter/android/geo/e;

.field private q:Landroid/widget/EditText;

.field private r:Landroid/widget/TextSwitcher;

.field private s:I

.field private t:I

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;-><init>()V

    .line 113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->B:Ljava/util/Set;

    return-void
.end method

.method private a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 875
    const v0, 0x7f0402b1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 876
    const v0, 0x7f130662

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 877
    invoke-virtual {v0, p2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 878
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 879
    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/widget/ComposerPoiFragment;Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/ComposerPoiFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    return-object v0
.end method

.method private a(Lcom/twitter/model/geo/TwitterPlace;Ljava/lang/String;)V
    .locals 20

    .prologue
    .line 732
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->g:Z

    if-eqz v2, :cond_0

    .line 734
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v2

    .line 735
    invoke-virtual {v2}, Lcom/twitter/android/geo/GeoTagState;->d()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz p1, :cond_2

    .line 736
    new-instance v2, Lcom/twitter/android/geo/GeoTagState;

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->p()Lcom/twitter/model/geo/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->w:Z

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    .line 737
    invoke-virtual {v3}, Lcom/twitter/android/geo/e;->c()Z

    move-result v8

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/geo/GeoTagState;-><init>(Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/model/geo/b;Ljava/lang/String;ZZZ)V

    .line 736
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/geo/GeoTagState;)V

    .line 738
    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string/jumbo v5, "compose:poi:poi_list:location:select"

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    const-wide/high16 v8, 0x7ff8000000000000L    # NaN

    const-wide/high16 v10, 0x7ff8000000000000L    # NaN

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->w:Z

    if-eqz v2, :cond_1

    const-string/jumbo v12, "auto_default"

    :goto_0
    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    .line 742
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/TwitterPlace;)I

    move-result v15

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->n()Ljava/lang/String;

    move-result-object v16

    const-string/jumbo v17, "geotag"

    .line 743
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v18

    move-object/from16 v2, p0

    .line 738
    invoke-direct/range {v2 .. v19}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(ZZLjava/lang/String;Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)V

    .line 753
    :cond_0
    :goto_1
    return-void

    .line 738
    :cond_1
    const-string/jumbo v12, "default"

    goto :goto_0

    .line 744
    :cond_2
    invoke-virtual {v2}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iget-object v3, v3, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 745
    invoke-virtual {v2}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v2

    .line 746
    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string/jumbo v5, "compose:poi:poi_list:location:select"

    iget-object v6, v2, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    iget-object v7, v2, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    const-wide/high16 v8, 0x7ff8000000000000L    # NaN

    const-wide/high16 v10, 0x7ff8000000000000L    # NaN

    const-string/jumbo v12, "default"

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    .line 749
    invoke-virtual {v15, v2}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/TwitterPlace;)I

    move-result v15

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->n()Ljava/lang/String;

    move-result-object v16

    const-string/jumbo v17, "geotag"

    .line 750
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v18

    move-object/from16 v2, p0

    .line 746
    invoke-direct/range {v2 .. v19}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(ZZLjava/lang/String;Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_1
.end method

.method private a(ZZLjava/lang/String;Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)V
    .locals 20

    .prologue
    .line 853
    if-eqz p1, :cond_0

    .line 854
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iget-object v2, v2, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->c:Ljava/util/List;

    .line 855
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 856
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    const-string/jumbo v3, "removed"

    iput-object v3, v2, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->j:Ljava/lang/String;

    .line 859
    :cond_0
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v18

    .line 860
    if-eqz p2, :cond_1

    .line 861
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-wide/from16 v16, p16

    invoke-virtual/range {v3 .. v17}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->a(Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    move-result-object v2

    .line 864
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iget-object v3, v3, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 869
    :goto_0
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->d:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    .line 870
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 871
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 869
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 872
    return-void

    .line 866
    :cond_1
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-wide/from16 v16, p16

    invoke-virtual/range {v3 .. v17}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->a(Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/widget/ComposerPoiFragment;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->A:Z

    return p1
.end method

.method private a(Lcom/twitter/library/api/geo/a;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 611
    iget-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->v:Z

    if-nez v2, :cond_0

    .line 612
    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->v:Z

    .line 613
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    .line 614
    invoke-virtual {p0, p1, p2, v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 617
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/widget/ComposerPoiFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/ComposerPoiFragment;Z)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/ComposerPoiFragment;->e(Z)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/widget/ComposerPoiFragment;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->B:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/ComposerPoiFragment;)Lcom/twitter/android/geo/c;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    return-object v0
.end method

.method private e(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 366
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    .line 367
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368
    invoke-direct {p0, v7}, Lcom/twitter/android/widget/ComposerPoiFragment;->h(Z)V

    .line 369
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 370
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    iget-object v3, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    .line 371
    invoke-virtual {v3}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a()Lcom/twitter/android/geo/d;

    move-result-object v3

    .line 370
    invoke-static {v3, v0}, Lcom/twitter/android/geo/d;->a(Lcom/twitter/android/geo/d;Ljava/lang/String;)Lcom/twitter/android/geo/d;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a(Lcom/twitter/android/geo/d;)V

    .line 373
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v2}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->A:Z

    if-nez v2, :cond_1

    .line 374
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    invoke-virtual {v0}, Lcom/twitter/android/geo/c;->a()V

    .line 391
    :cond_0
    :goto_0
    invoke-direct {p0, v6}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    .line 392
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->o()V

    .line 393
    invoke-virtual {v1, v6, v6}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 394
    return-void

    .line 375
    :cond_1
    iget-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->A:Z

    if-eqz v2, :cond_0

    .line 376
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    const v3, 0x7f0a06a9

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/widget/ComposerPoiFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/geo/c;->a(Ljava/lang/CharSequence;)V

    .line 377
    if-eqz p1, :cond_0

    .line 378
    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 379
    iput-object v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    .line 380
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->d:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v4, "compose:poi:poi_list::filter"

    aput-object v4, v3, v6

    .line 381
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 382
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 380
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 386
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    new-instance v2, Lcom/twitter/android/geo/d;

    iget-object v3, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    sget-object v4, Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;->a:Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/geo/d;-><init>(Lcom/twitter/android/geo/PlacePickerModel;Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a(Lcom/twitter/android/geo/d;)V

    .line 388
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    invoke-virtual {v0}, Lcom/twitter/android/geo/c;->a()V

    .line 389
    invoke-direct {p0, v6}, Lcom/twitter/android/widget/ComposerPoiFragment;->h(Z)V

    goto :goto_0
.end method

.method private f(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 677
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->l:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 699
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    .line 681
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    .line 682
    if-eqz p1, :cond_2

    .line 683
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 684
    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 685
    invoke-direct {p0, v3}, Lcom/twitter/android/widget/ComposerPoiFragment;->g(Z)V

    .line 686
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 687
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 688
    :cond_2
    iget-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->v:Z

    if-nez v2, :cond_0

    .line 689
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 690
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v2}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->A:Z

    if-eqz v2, :cond_4

    .line 691
    :cond_3
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 692
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 697
    :goto_1
    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 694
    :cond_4
    invoke-direct {p0, v5}, Lcom/twitter/android/widget/ComposerPoiFragment;->g(Z)V

    .line 695
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private g(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 707
    if-nez p1, :cond_1

    .line 708
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 709
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 724
    :cond_0
    :goto_0
    return-void

    .line 710
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->k()Z

    move-result v0

    if-nez v0, :cond_3

    .line 715
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->j:Landroid/widget/TextView;

    const v1, 0x7f0a05e4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 716
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->k:Landroid/widget/TextView;

    const v1, 0x7f0a05e5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 717
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 722
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 718
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 719
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->j:Landroid/widget/TextView;

    const v1, 0x7f0a05e8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 720
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private h(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 810
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 811
    if-nez p1, :cond_1

    .line 812
    iget v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->s:I

    if-ne v1, v4, :cond_0

    .line 813
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    const v2, 0x7f050038

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/content/Context;I)V

    .line 814
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    const v2, 0x7f050037

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/content/Context;I)V

    .line 815
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    const v1, 0x7f0a06a5

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 816
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->s:I

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 819
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v1

    .line 820
    iget v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->s:I

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 821
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    const v3, 0x7f05003a

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/content/Context;I)V

    .line 822
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    const v3, 0x7f050039

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/content/Context;I)V

    .line 823
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 824
    iput v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->s:I

    goto :goto_0
.end method

.method private k()Z
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 757
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iput v1, v2, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->b:I

    .line 758
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 759
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 760
    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->A:Z

    .line 761
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 762
    new-instance v4, Lcom/twitter/library/api/geo/a;

    iget-object v5, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->c:Lcom/twitter/library/client/Session;

    invoke-direct {v4, v3, v5}, Lcom/twitter/library/api/geo/a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v5, "tweet_compose_location"

    .line 763
    invoke-virtual {v4, v5}, Lcom/twitter/library/api/geo/a;->a(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v4

    .line 764
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/library/api/geo/a;->b(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v4

    .line 765
    invoke-static {v3}, Lcom/twitter/library/util/ai;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/twitter/library/api/geo/a;->a(Ljava/util/List;)Lcom/twitter/library/api/geo/a;

    move-result-object v3

    .line 766
    invoke-direct {p0, v3, v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/library/api/geo/a;I)Z

    .line 767
    new-instance v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 768
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    .line 769
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->d:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v4, v1, [Ljava/lang/String;

    const-string/jumbo v5, "compose:poi:poi_list::search"

    aput-object v5, v4, v0

    .line 770
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 771
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 769
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v1

    .line 774
    :cond_0
    return v0
.end method

.method private m()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 779
    iput-boolean v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->y:Z

    .line 780
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->B:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    :goto_0
    return-void

    .line 783
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->d:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "compose:poi:poi_list:location:results"

    aput-object v2, v1, v4

    .line 784
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 785
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->B:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/ComposerPoiFragment$b;

    .line 786
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v3

    .line 787
    new-instance v4, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;-><init>()V

    .line 788
    iget-object v5, v1, Lcom/twitter/android/widget/ComposerPoiFragment$b;->a:Ljava/lang/String;

    iput-object v5, v4, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->a:Ljava/lang/String;

    .line 789
    iget-object v5, v1, Lcom/twitter/android/widget/ComposerPoiFragment$b;->b:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    invoke-virtual {v5}, Lcom/twitter/model/geo/TwitterPlace$PlaceType;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->b:Ljava/lang/String;

    .line 790
    iget-object v5, v1, Lcom/twitter/android/widget/ComposerPoiFragment$b;->d:Ljava/lang/String;

    iput-object v5, v4, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->e:Ljava/lang/String;

    .line 791
    iget v5, v1, Lcom/twitter/android/widget/ComposerPoiFragment$b;->f:I

    iput v5, v4, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->g:I

    .line 792
    iget v5, v1, Lcom/twitter/android/widget/ComposerPoiFragment$b;->e:I

    iput v5, v4, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->h:I

    .line 793
    iget-object v1, v1, Lcom/twitter/android/widget/ComposerPoiFragment$b;->c:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->i:Ljava/lang/String;

    .line 794
    iget-object v1, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iget-object v1, v1, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->c:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 795
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 797
    :cond_1
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 798
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->B:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 802
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 803
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 804
    const/4 v0, 0x0

    .line 806
    :cond_0
    return-object v0
.end method

.method private o()V
    .locals 5

    .prologue
    const v4, 0x7f0205ea

    const/4 v1, 0x0

    .line 830
    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    .line 831
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 834
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 840
    :goto_0
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 841
    invoke-virtual {v2, v0, v1, v4, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 847
    :goto_1
    return-void

    .line 837
    :cond_0
    const v0, 0x7f0205bb

    goto :goto_0

    .line 844
    :cond_1
    invoke-virtual {v2, v4, v1, v0, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1
.end method

.method private p()Lcom/twitter/model/geo/b;
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->b:Lbqn;

    invoke-virtual {v0}, Lbqn;->a()Landroid/location/Location;

    move-result-object v0

    .line 885
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/twitter/model/geo/b;->a(Landroid/location/Location;)Lcom/twitter/model/geo/b;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    .line 136
    const v0, 0x7f0402b0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 138
    const v0, 0x1020004

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->i:Landroid/view/View;

    .line 139
    invoke-virtual {v7, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 140
    const v0, 0x7f13065f

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->j:Landroid/widget/TextView;

    .line 141
    const v0, 0x7f130660

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->k:Landroid/widget/TextView;

    .line 142
    const v0, 0x7f13004e

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->l:Landroid/widget/ProgressBar;

    .line 144
    const v0, 0x7f13065e

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 145
    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 146
    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 147
    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 150
    if-eqz p2, :cond_0

    .line 151
    const-string/jumbo v0, "has_search_text_changed_since_last_search"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->A:Z

    .line 153
    const-string/jumbo v0, "place_picker_presenter_source"

    .line 154
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    move-object v1, v0

    .line 159
    :goto_0
    new-instance v0, Lcom/twitter/android/geo/c;

    invoke-virtual {v6}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v6}, Lcom/twitter/android/geo/c;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    .line 160
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/geo/c;->a(Landroid/view/View$OnClickListener;)V

    .line 162
    new-instance v2, Lcom/twitter/android/geo/e;

    const v0, 0x7f130661

    .line 163
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v2, v0, p0}, Lcom/twitter/android/geo/e;-><init>(Landroid/view/ViewGroup;Lcom/twitter/android/geo/e$a;)V

    iput-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    .line 164
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    invoke-virtual {v0}, Lcom/twitter/android/geo/e;->b()V

    .line 168
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    invoke-virtual {v0}, Lcom/twitter/android/geo/c;->b()Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "poi_footer_tag"

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 171
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 172
    new-instance v0, Lcom/twitter/android/widget/ComposerPoiFragment$a;

    const v2, 0x7f0402b5

    const v3, 0x7f0402b6

    iget-object v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    new-instance v5, Lcom/twitter/android/geo/d;

    iget-object v9, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-direct {v5, v9, v1}, Lcom/twitter/android/geo/d;-><init>(Lcom/twitter/android/geo/PlacePickerModel;Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/ComposerPoiFragment$a;-><init>(Lcom/twitter/android/widget/ComposerPoiFragment;IILcom/twitter/android/geo/PlacePickerModel;Lcom/twitter/android/geo/d;)V

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    .line 175
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 176
    iput-object v6, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    .line 178
    const v0, 0x7f13065c

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextSwitcher;

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    .line 179
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->r:Landroid/widget/TextSwitcher;

    const v1, 0x7f0a06a5

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    .line 180
    const v0, 0x7f13065b

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 181
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    const v0, 0x7f13065d

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 185
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 186
    new-instance v1, Lcom/twitter/android/widget/au;

    invoke-direct {v1, v0, p0}, Lcom/twitter/android/widget/au;-><init>(Landroid/widget/TextView;Lcom/twitter/android/widget/au$a;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 187
    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    .line 189
    return-object v7

    .line 156
    :cond_0
    sget-object v0, Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;->a:Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    move-object v1, v0

    goto/16 :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 470
    if-eqz p1, :cond_0

    .line 471
    invoke-static {p1}, Lcom/twitter/model/geo/b;->a(Landroid/location/Location;)Lcom/twitter/model/geo/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    .line 472
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/geo/e;->a(Lcom/twitter/model/geo/b;)V

    .line 473
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    invoke-virtual {v0}, Lcom/twitter/android/geo/e;->a()V

    .line 474
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->i()Z

    .line 476
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 487
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->L()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 488
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/widget/ComposerLocationFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    check-cast p1, Lcom/twitter/library/api/geo/a;

    .line 492
    iput-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->v:Z

    .line 493
    invoke-virtual {p1}, Lcom/twitter/library/api/geo/a;->h()Lcom/twitter/library/api/geo/b;

    move-result-object v3

    .line 494
    if-nez v3, :cond_3

    .line 495
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 496
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    invoke-virtual {v0}, Lcom/twitter/android/geo/c;->a()V

    .line 498
    :cond_2
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    goto :goto_0

    .line 501
    :cond_3
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 503
    :pswitch_0
    invoke-virtual {p1}, Lcom/twitter/library/api/geo/a;->g()Lcom/twitter/model/geo/b;

    move-result-object v0

    .line 504
    if-eqz v0, :cond_8

    .line 508
    :goto_1
    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v4, v0}, Lcom/twitter/android/geo/PlacePickerModel;->b(Lcom/twitter/model/geo/b;)Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    if-nez v1, :cond_6

    iget-object v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    iget-object v5, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    .line 509
    invoke-virtual {v4, v5}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/b;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 510
    :cond_5
    new-instance v4, Lcom/twitter/android/geo/b;

    .line 511
    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->a()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->d()Ljava/util/List;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/twitter/android/geo/b;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    .line 513
    iget-object v5, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    if-eqz v1, :cond_9

    :goto_2
    invoke-virtual {v5, v0, v4}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/b;Lcom/twitter/android/geo/b;)Lcom/twitter/android/geo/PlacePickerModel;

    move-result-object v0

    .line 514
    invoke-virtual {v0, v1}, Lcom/twitter/android/geo/PlacePickerModel;->a(Z)Lcom/twitter/android/geo/PlacePickerModel;

    .line 515
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    new-instance v4, Lcom/twitter/android/geo/d;

    iget-object v5, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    sget-object v6, Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;->a:Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    invoke-direct {v4, v5, v6}, Lcom/twitter/android/geo/d;-><init>(Lcom/twitter/android/geo/PlacePickerModel;Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;)V

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a(Lcom/twitter/android/geo/d;)V

    .line 518
    if-eqz v1, :cond_a

    .line 519
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->d()Z

    move-result v0

    if-nez v0, :cond_6

    .line 520
    invoke-static {}, Lcom/twitter/android/geo/GeoTagState;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/geo/GeoTagState;)V

    .line 527
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->h:Z

    if-nez v0, :cond_0

    .line 528
    :cond_7
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 504
    goto :goto_1

    .line 513
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    goto :goto_2

    .line 523
    :cond_a
    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->b()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/model/geo/TwitterPlace;Ljava/lang/String;)V

    goto :goto_3

    .line 535
    :pswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 536
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    new-instance v4, Lcom/twitter/android/geo/b;

    .line 537
    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/twitter/library/api/geo/b;->d()Ljava/util/List;

    move-result-object v3

    invoke-direct {v4, v0, v5, v3}, Lcom/twitter/android/geo/b;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    .line 536
    invoke-virtual {v1, v4}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/android/geo/b;)Lcom/twitter/android/geo/PlacePickerModel;

    .line 538
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 539
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->o:Lcom/twitter/android/geo/c;

    invoke-virtual {v0}, Lcom/twitter/android/geo/c;->a()V

    .line 540
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    new-instance v1, Lcom/twitter/android/geo/d;

    iget-object v3, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    sget-object v4, Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;->b:Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    invoke-direct {v1, v3, v4}, Lcom/twitter/android/geo/d;-><init>(Lcom/twitter/android/geo/PlacePickerModel;Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a(Lcom/twitter/android/geo/d;)V

    .line 543
    :cond_b
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    goto/16 :goto_0

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 896
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v0}, Lcom/twitter/android/geo/PlacePickerModel;->c()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    .line 897
    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 898
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v0, p1}, Lcom/twitter/android/geo/GeoTagState;->a(Z)Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/android/geo/GeoTagState;)Lcom/twitter/android/geo/PlacePickerModel;

    .line 900
    :cond_0
    return-void
.end method

.method public a(ZI)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 630
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->w:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->g:Z

    if-eqz v0, :cond_1

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iput v8, v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->a:I

    .line 632
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->C:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    iget-wide v4, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->d:J

    iput-wide v4, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 635
    if-nez p1, :cond_2

    .line 636
    const-string/jumbo v0, "composition"

    move-object v1, v2

    move-object v3, v0

    .line 643
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 663
    :cond_1
    :goto_1
    :pswitch_0
    return-void

    .line 639
    :cond_2
    const-string/jumbo v1, "drafts"

    .line 640
    const-string/jumbo v0, "composition"

    move-object v3, v1

    move-object v1, v0

    goto :goto_0

    .line 645
    :pswitch_1
    const-string/jumbo v0, "discard_tweet_geo_interaction"

    .line 659
    :goto_2
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v6, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->d:J

    invoke-direct {v4, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    aput-object v3, v5, v8

    const/4 v3, 0x2

    aput-object v1, v5, v3

    const/4 v1, 0x3

    aput-object v2, v5, v1

    const/4 v1, 0x4

    aput-object v0, v5, v1

    .line 660
    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->C:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 661
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 659
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    .line 649
    :pswitch_2
    const-string/jumbo v0, "send_tweet_geo_interaction"

    goto :goto_2

    .line 643
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/geo/b;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 598
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v1, p1}, Lcom/twitter/android/geo/PlacePickerModel;->b(Lcom/twitter/model/geo/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 599
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 600
    if-eqz v1, :cond_0

    .line 601
    new-instance v2, Lcom/twitter/library/api/geo/a;

    iget-object v3, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->c:Lcom/twitter/library/client/Session;

    invoke-direct {v2, v1, v3}, Lcom/twitter/library/api/geo/a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v1, "tweet_compose_location"

    .line 602
    invoke-virtual {v2, v1}, Lcom/twitter/library/api/geo/a;->a(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    .line 603
    invoke-virtual {v1, p1}, Lcom/twitter/library/api/geo/a;->a(Lcom/twitter/model/geo/b;)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    .line 604
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/library/api/geo/a;I)Z

    move-result v0

    .line 607
    :cond_0
    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 194
    invoke-super {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->b()V

    .line 195
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->x:Z

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v0, :cond_0

    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->u:Z

    .line 198
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    invoke-interface {v0, v2}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->b(Z)V

    .line 199
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->a(Ljava/lang/String;)V

    .line 204
    :cond_0
    iput-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->x:Z

    .line 206
    :cond_1
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->e(Z)V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->E:Lcom/twitter/util/ui/d;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 208
    return-void
.end method

.method public b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 429
    packed-switch p1, :pswitch_data_0

    .line 462
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 431
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 432
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->y:Z

    goto :goto_0

    .line 436
    :pswitch_2
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->z:Z

    if-eqz v0, :cond_2

    .line 437
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->notifyDataSetChanged()V

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 441
    iput-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->z:Z

    .line 443
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 444
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->v:Z

    if-eqz v0, :cond_3

    .line 445
    iput-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->v:Z

    .line 446
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    .line 448
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 450
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->u:Z

    if-eqz v0, :cond_5

    .line 451
    iput-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->u:Z

    .line 453
    :cond_5
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->y:Z

    if-eqz v0, :cond_0

    .line 454
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->m()V

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 480
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ComposerLocationFragment;->b(Landroid/location/Location;)V

    .line 482
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    .line 483
    return-void
.end method

.method protected b(Z)V
    .locals 2

    .prologue
    .line 667
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ComposerLocationFragment;->b(Z)V

    .line 668
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->g:Z

    if-nez v0, :cond_0

    .line 669
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    .line 670
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 674
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 554
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->g:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v0, :cond_0

    .line 555
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->d:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "compose:poi::poi_tag:click"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 556
    invoke-virtual {p0, v4}, Lcom/twitter/android/widget/ComposerPoiFragment;->d(Z)V

    .line 558
    :cond_0
    return-void
.end method

.method public c(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 414
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 417
    :goto_0
    if-ne p1, v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 420
    :cond_0
    return v1

    .line 414
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public d(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 569
    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->w:Z

    .line 570
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v1}, Lcom/twitter/android/geo/PlacePickerModel;->c()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 571
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    invoke-virtual {v1}, Lcom/twitter/android/geo/e;->a()V

    .line 574
    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->b(Z)V

    .line 575
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v1}, Lcom/twitter/android/geo/PlacePickerModel;->b()Lcom/twitter/model/geo/b;

    move-result-object v1

    if-nez v1, :cond_2

    .line 576
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->f(Z)V

    .line 579
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v1, :cond_3

    .line 580
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-nez p1, :cond_4

    :goto_0
    invoke-interface {v1, v0}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->b(Z)V

    .line 582
    :cond_3
    return-void

    .line 580
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    if-nez v0, :cond_0

    .line 562
    const/4 v0, 0x0

    .line 565
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    invoke-virtual {v0}, Lcom/twitter/android/geo/e;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public i()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 585
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->f:Lcom/twitter/model/geo/b;

    invoke-virtual {v1, v2}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 586
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 587
    if-eqz v1, :cond_0

    .line 588
    new-instance v2, Lcom/twitter/library/api/geo/a;

    iget-object v3, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->c:Lcom/twitter/library/client/Session;

    invoke-direct {v2, v1, v3}, Lcom/twitter/library/api/geo/a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v3, "tweet_compose_location"

    .line 589
    invoke-virtual {v2, v3}, Lcom/twitter/library/api/geo/a;->a(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v2

    .line 590
    invoke-static {v1}, Lcom/twitter/library/util/ai;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/api/geo/a;->a(Ljava/util/List;)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    .line 591
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/library/api/geo/a;I)Z

    move-result v0

    .line 594
    :cond_0
    return v0
.end method

.method public j()Lcom/twitter/android/geo/PlacePickerModel;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 20

    .prologue
    .line 223
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 252
    :cond_0
    :goto_0
    const-string/jumbo v2, "footer_text_tag"

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 253
    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->l()Z

    .line 255
    :cond_1
    return-void

    .line 225
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v2, :cond_0

    .line 226
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->u:Z

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->b(Z)V

    goto :goto_0

    .line 232
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v2

    .line 233
    invoke-virtual {v2}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 234
    invoke-virtual {v2}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v2

    .line 235
    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string/jumbo v5, "compose:poi:poi_list:location:deselect"

    iget-object v6, v2, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    iget-object v7, v2, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    const-wide/high16 v8, 0x7ff8000000000000L    # NaN

    const-wide/high16 v10, 0x7ff8000000000000L    # NaN

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    .line 237
    invoke-virtual {v12, v2}, Lcom/twitter/android/geo/PlacePickerModel;->b(Lcom/twitter/model/geo/TwitterPlace;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    .line 238
    invoke-virtual {v15, v2}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/TwitterPlace;)I

    move-result v15

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->n()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    const-wide/16 v18, -0x1

    move-object/from16 v2, p0

    .line 235
    invoke-direct/range {v2 .. v19}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(ZZLjava/lang/String;Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)V

    .line 240
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->b(Z)V

    .line 241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v2, :cond_0

    .line 242
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->b(Z)V

    goto :goto_0

    .line 223
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f13065b -> :sswitch_0
        0x7f130668 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ComposerLocationFragment;->onCreate(Landroid/os/Bundle;)V

    .line 121
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->C:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 122
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->C:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    .line 123
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iput v1, v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->a:I

    .line 124
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->D:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iput v1, v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->b:I

    .line 125
    new-instance v0, Lcom/twitter/android/widget/ComposerPoiFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/ComposerPoiFragment$1;-><init>(Lcom/twitter/android/widget/ComposerPoiFragment;)V

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->E:Lcom/twitter/util/ui/d;

    .line 132
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v0}, Lcom/twitter/android/geo/PlacePickerModel;->unregisterAll()V

    .line 891
    invoke-super {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->onDestroy()V

    .line 892
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 403
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->l()Z

    move-result v0

    .line 405
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    sub-int v14, p3, v2

    .line 317
    if-ltz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v2}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->getCount()I

    move-result v2

    if-lt v14, v2, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    invoke-virtual {v2, v14}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a(I)Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v3

    .line 322
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v2

    .line 323
    invoke-virtual {v2}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/model/geo/TwitterPlace;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v2, :cond_2

    .line 324
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->u:Z

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->b(Z)V

    goto :goto_0

    .line 329
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    .line 330
    invoke-virtual {v4}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a()Lcom/twitter/android/geo/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/android/geo/d;->b()Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    move-result-object v4

    .line 329
    invoke-virtual {v2, v4}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;)Lcom/twitter/android/geo/b;

    move-result-object v5

    .line 331
    if-nez v5, :cond_4

    .line 332
    const-string/jumbo v2, "PlaceList cannot be null here"

    invoke-static {v2}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 340
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v2, :cond_3

    .line 341
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->u:Z

    .line 342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->b(Z)V

    .line 344
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->z:Z

    .line 345
    const/16 v20, 0x1

    const/4 v4, 0x1

    const-string/jumbo v5, "compose:poi:poi_list:location:select"

    iget-object v6, v3, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    iget-object v7, v3, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    const-wide/high16 v8, 0x7ff8000000000000L    # NaN

    const-wide/high16 v10, 0x7ff8000000000000L    # NaN

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    .line 347
    invoke-virtual {v2, v3}, Lcom/twitter/android/geo/PlacePickerModel;->b(Lcom/twitter/model/geo/TwitterPlace;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    .line 348
    invoke-virtual {v2, v3}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/TwitterPlace;)I

    move-result v15

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->n()Ljava/lang/String;

    move-result-object v16

    const-string/jumbo v17, "geotag"

    .line 349
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v18

    move-object/from16 v2, p0

    move/from16 v3, v20

    .line 345
    invoke-direct/range {v2 .. v19}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(ZZLjava/lang/String;Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 334
    :cond_4
    new-instance v2, Lcom/twitter/android/geo/GeoTagState;

    .line 335
    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->p()Lcom/twitter/model/geo/b;

    move-result-object v4

    .line 336
    invoke-virtual {v5}, Lcom/twitter/android/geo/b;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    .line 337
    invoke-virtual {v8}, Lcom/twitter/android/geo/e;->c()Z

    move-result v8

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/geo/GeoTagState;-><init>(Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/model/geo/b;Ljava/lang/String;ZZZ)V

    .line 334
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/geo/GeoTagState;)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 354
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ComposerLocationFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 355
    const-string/jumbo v0, "has_search_text_changed_since_last_search"

    iget-boolean v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 357
    const-string/jumbo v0, "place_picker_presenter_source"

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->n:Lcom/twitter/android/widget/ComposerPoiFragment$a;

    .line 358
    invoke-virtual {v1}, Lcom/twitter/android/widget/ComposerPoiFragment$a;->a()Lcom/twitter/android/geo/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/geo/d;->b()Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    move-result-object v1

    .line 357
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 359
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 289
    iget v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->t:I

    if-le v0, p2, :cond_1

    .line 290
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    invoke-virtual {v0}, Lcom/twitter/android/geo/e;->a()V

    .line 295
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 306
    :goto_1
    return-void

    .line 291
    :cond_1
    iget v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->t:I

    if-ge v0, p2, :cond_0

    .line 292
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->p:Lcom/twitter/android/geo/e;

    invoke-virtual {v0}, Lcom/twitter/android/geo/e;->b()V

    goto :goto_0

    .line 298
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    if-nez v0, :cond_3

    move v0, v1

    .line 299
    :goto_2
    sub-int v0, p2, v0

    if-lez v0, :cond_4

    .line 300
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->h(Z)V

    .line 305
    :goto_3
    iput p2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->t:I

    goto :goto_1

    .line 298
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->m:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    goto :goto_2

    .line 302
    :cond_4
    invoke-direct {p0, v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->h(Z)V

    goto :goto_3
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    iget-boolean v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->u:Z

    if-eqz v2, :cond_0

    .line 274
    :goto_0
    return v0

    .line 266
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 267
    const v3, 0x7f13065e

    if-ne v2, v3, :cond_2

    .line 268
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 269
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerPoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    invoke-static {v0, v2, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    :cond_1
    move v0, v1

    .line 274
    goto :goto_0

    .line 270
    :cond_2
    const v3, 0x7f130659

    if-ne v2, v3, :cond_1

    goto :goto_0
.end method

.method public q_()V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->q:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerPoiFragment;->E:Lcom/twitter/util/ui/d;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 213
    invoke-super {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->q_()V

    .line 214
    return-void
.end method
