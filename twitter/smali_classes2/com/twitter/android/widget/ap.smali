.class public Lcom/twitter/android/widget/ap;
.super Landroid/widget/BaseAdapter;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ad;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/ap$a;
    }
.end annotation


# instance fields
.field a:Z

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/BaseAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:[I

.field private e:I

.field private f:Lcom/twitter/android/widget/ad;


# direct methods
.method public constructor <init>([I[Landroid/widget/BaseAdapter;II)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ap;->a:Z

    .line 49
    invoke-static {p2}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    .line 50
    iput p3, p0, Lcom/twitter/android/widget/ap;->c:I

    .line 51
    iput-object p1, p0, Lcom/twitter/android/widget/ap;->d:[I

    .line 52
    iput p4, p0, Lcom/twitter/android/widget/ap;->e:I

    .line 54
    new-instance v1, Lcom/twitter/android/widget/ap$a;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/ap$a;-><init>(Lcom/twitter/android/widget/ap;)V

    .line 55
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    .line 56
    invoke-virtual {v3, v1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method public constructor <init>([Landroid/widget/BaseAdapter;)V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x0

    const v1, 0x7f0403a9

    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/twitter/android/widget/ap;-><init>([I[Landroid/widget/BaseAdapter;II)V

    .line 37
    return-void
.end method

.method public constructor <init>([Landroid/widget/BaseAdapter;I)V
    .locals 2

    .prologue
    .line 40
    const/4 v0, 0x0

    const v1, 0x7f0403a9

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/twitter/android/widget/ap;-><init>([I[Landroid/widget/BaseAdapter;II)V

    .line 41
    return-void
.end method

.method private c(I)I
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 90
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    .line 91
    if-lez v0, :cond_0

    .line 92
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    add-int/lit8 v0, v0, 0x1

    .line 96
    :cond_0
    return v0
.end method


# virtual methods
.method protected a(Landroid/widget/BaseAdapter;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->d:[I

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->d:[I

    aget v0, v0, p3

    .line 203
    :goto_0
    if-nez v0, :cond_1

    .line 205
    invoke-virtual {p1, p4, p5, p6}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 208
    :goto_1
    return-object v0

    .line 201
    :cond_0
    iget v0, p0, Lcom/twitter/android/widget/ap;->e:I

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {p1, p4, p5, p6}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->f:Lcom/twitter/android/widget/ad;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->f:Lcom/twitter/android/widget/ad;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/ad;->a(Landroid/view/View;)V

    .line 356
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->f:Lcom/twitter/android/widget/ad;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->f:Lcom/twitter/android/widget/ad;

    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/android/widget/ad;->a(Landroid/view/View;II)V

    .line 368
    :cond_0
    return-void
.end method

.method public a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    move v2, v1

    move v3, v1

    .line 72
    :goto_0
    if-ge v2, p1, :cond_0

    .line 73
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/ap;->c(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 72
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 75
    :cond_0
    if-nez v3, :cond_1

    .line 84
    :goto_1
    return v1

    .line 79
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/widget/ap;->d:[I

    if-eqz v2, :cond_2

    .line 80
    iget-object v2, p0, Lcom/twitter/android/widget/ap;->d:[I

    aget v2, v2, p1

    .line 84
    :goto_2
    if-ne v2, v0, :cond_3

    :goto_3
    move v1, v0

    goto :goto_1

    .line 82
    :cond_2
    iget v2, p0, Lcom/twitter/android/widget/ap;->e:I

    goto :goto_2

    :cond_3
    move v0, v1

    .line 84
    goto :goto_3
.end method

.method public a_(II)I
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->f:Lcom/twitter/android/widget/ad;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->f:Lcom/twitter/android/widget/ad;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/widget/ad;->a_(II)I

    move-result v0

    .line 347
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 214
    .line 215
    iget-object v2, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 216
    :goto_0
    if-ge v2, v3, :cond_2

    .line 217
    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v0

    .line 222
    :goto_1
    if-nez v2, :cond_1

    invoke-super {p0}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_2
    return v0

    .line 216
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 222
    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public b(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 293
    .line 295
    iget-object v1, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 296
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    .line 297
    if-lez v0, :cond_3

    .line 298
    add-int/2addr v0, v1

    .line 299
    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    add-int/lit8 v0, v0, 0x1

    .line 303
    :cond_0
    :goto_1
    if-ge p1, v0, :cond_2

    .line 308
    :cond_1
    return v2

    .line 306
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    .line 307
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public getCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 101
    move v1, v0

    .line 102
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 103
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/ap;->c(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_0
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 111
    const/4 v0, 0x0

    .line 112
    iget-object v1, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 113
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    .line 114
    if-lez v2, :cond_3

    .line 115
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 116
    add-int/lit8 v2, v2, 0x1

    .line 117
    if-nez p1, :cond_0

    move-object v0, v3

    .line 131
    :goto_1
    return-object v0

    .line 119
    :cond_0
    if-ge p1, v2, :cond_2

    .line 120
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 123
    :cond_1
    if-ge p1, v2, :cond_2

    .line 124
    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 127
    sub-int/2addr p1, v0

    .line 129
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 130
    goto :goto_0

    :cond_4
    move-object v0, v3

    .line 131
    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 137
    const/4 v0, 0x0

    .line 138
    iget-object v1, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 139
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    .line 140
    if-lez v2, :cond_3

    .line 141
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 142
    add-int/lit8 v2, v2, 0x1

    .line 143
    if-nez p1, :cond_0

    move-wide v0, v4

    .line 157
    :goto_1
    return-wide v0

    .line 145
    :cond_0
    if-ge p1, v2, :cond_2

    .line 146
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_1

    .line 149
    :cond_1
    if-ge p1, v2, :cond_2

    .line 150
    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 153
    sub-int/2addr p1, v0

    .line 155
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 156
    goto :goto_0

    :cond_4
    move-wide v0, v4

    .line 157
    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 253
    const/4 v0, 0x1

    .line 255
    iget-object v1, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 256
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v4

    .line 257
    if-lez v4, :cond_3

    .line 258
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 259
    add-int/lit8 v4, v4, 0x1

    .line 260
    if-nez p1, :cond_0

    .line 275
    :goto_1
    return v2

    .line 262
    :cond_0
    if-ge p1, v4, :cond_2

    .line 263
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    add-int v2, v0, v3

    goto :goto_1

    .line 266
    :cond_1
    if-ge p1, v4, :cond_2

    .line 267
    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    add-int v2, v0, v3

    goto :goto_1

    .line 270
    :cond_2
    sub-int/2addr p1, v4

    .line 272
    :cond_3
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v0

    add-int/2addr v3, v0

    .line 273
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 274
    goto :goto_0

    .line 275
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v2

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 162
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 168
    iget-object v1, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v0

    move v3, v0

    move v4, p1

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/BaseAdapter;

    .line 169
    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v5

    .line 170
    if-lez v5, :cond_4

    .line 171
    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 172
    add-int/lit8 v5, v5, 0x1

    .line 173
    if-nez v4, :cond_1

    .line 174
    if-nez p2, :cond_0

    .line 175
    iget v1, p0, Lcom/twitter/android/widget/ap;->c:I

    invoke-virtual {v6, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 192
    :cond_0
    :goto_1
    return-object p2

    .line 178
    :cond_1
    if-ge v4, v5, :cond_3

    .line 179
    add-int/lit8 v4, v4, -0x1

    move-object v0, p0

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/widget/ap;->a(Landroid/widget/BaseAdapter;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    .line 182
    :cond_2
    if-ge v4, v5, :cond_3

    move-object v0, p0

    move-object v5, p2

    move-object v6, p3

    .line 183
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/widget/ap;->a(Landroid/widget/BaseAdapter;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    :cond_3
    move v1, v5

    .line 186
    sub-int/2addr v4, v1

    .line 187
    add-int/lit8 v2, v2, 0x1

    .line 189
    :cond_4
    add-int/lit8 v3, v3, 0x1

    .line 190
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 3

    .prologue
    .line 280
    const/4 v0, 0x0

    .line 281
    iget-object v1, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 282
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 283
    goto :goto_0

    .line 285
    :cond_0
    add-int/lit8 v0, v1, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 227
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 228
    iget-object v0, p0, Lcom/twitter/android/widget/ap;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 229
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v3

    .line 230
    if-lez v3, :cond_3

    .line 231
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/ap;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 232
    add-int/lit8 v3, v3, 0x1

    .line 233
    if-nez p1, :cond_0

    .line 246
    :goto_1
    return v2

    .line 235
    :cond_0
    if-ge p1, v3, :cond_2

    .line 236
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v2

    goto :goto_1

    .line 239
    :cond_1
    if-ge p1, v3, :cond_2

    .line 240
    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v2

    goto :goto_1

    :cond_2
    move v0, v3

    .line 243
    sub-int/2addr p1, v0

    .line 227
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 246
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v2

    goto :goto_1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 334
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ap;->a:Z

    .line 336
    return-void
.end method
