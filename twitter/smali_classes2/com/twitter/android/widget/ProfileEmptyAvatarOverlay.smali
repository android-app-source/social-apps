.class public Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"

# interfaces
.implements Lbrp;
.implements Lcom/twitter/android/media/selection/a;


# instance fields
.field private c:Lcom/twitter/android/media/selection/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 62
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 63
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 64
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "profile_overlay"

    .line 65
    invoke-virtual {v1, v2, v0}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 64
    :cond_0
    return v0
.end method

.method public static b(Landroid/support/v4/app/FragmentActivity;)V
    .locals 2

    .prologue
    .line 40
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 44
    invoke-static {p0, v0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 48
    :cond_0
    new-instance v0, Lcom/twitter/android/widget/ah$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ah$a;-><init>(I)V

    const v1, 0x7f0a06ca

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ah$a;->b(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a0358

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a0353

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->e(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0204db

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->a(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 53
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;

    .line 54
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->a(Landroid/support/v4/app/FragmentActivity;)V

    goto :goto_0
.end method

.method private m()V
    .locals 9

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/android/media/selection/c;

    .line 70
    invoke-virtual {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v3, "profile"

    sget-object v4, Lcom/twitter/media/model/MediaType;->g:Ljava/util/EnumSet;

    const/4 v5, 0x1

    sget-object v6, Lcom/twitter/android/composer/ComposerType;->d:Lcom/twitter/android/composer/ComposerType;

    .line 76
    invoke-virtual {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->l()Lcom/twitter/library/client/Session;

    move-result-object v7

    move-object v2, p0

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/k;)V

    iput-object v0, p0, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->c:Lcom/twitter/android/media/selection/c;

    .line 78
    return-void
.end method


# virtual methods
.method protected a(Landroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->a(Landroid/app/Dialog;Landroid/os/Bundle;)V

    .line 95
    invoke-direct {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->m()V

    .line 96
    return-void
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 167
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->startActivityForResult(Landroid/content/Intent;I)V

    .line 168
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 105
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->c()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 106
    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 111
    iget v2, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    packed-switch v2, :pswitch_data_0

    .line 125
    if-eqz v1, :cond_0

    .line 126
    const v0, 0x7f0a06c6

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 114
    :pswitch_0
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 116
    if-eqz v1, :cond_2

    .line 117
    new-instance v2, Lcom/twitter/library/client/n;

    iget-object v0, v0, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v4}, Lcom/twitter/library/client/n;-><init>(Lcom/twitter/media/model/MediaFile;Lcom/twitter/media/model/MediaFile;Z)V

    .line 118
    invoke-virtual {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v1, v0, v2}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;)Ljava/lang/String;

    .line 120
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->dismiss()V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method protected h()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 83
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->h()V

    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 86
    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "profile_overlay"

    .line 87
    invoke-virtual {v0, v1, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 89
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "profile::empty_state::impression"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->a([Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public i()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "profile::empty_state:camera:click"

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->a([Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->c:Lcom/twitter/android/media/selection/c;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->c:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/media/selection/c;->a(ZI)V

    .line 147
    :cond_0
    return-void
.end method

.method public j()V
    .locals 3

    .prologue
    .line 152
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "profile::empty_state:photo:click"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->a([Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->c:Lcom/twitter/android/media/selection/c;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->c:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->b()V

    .line 156
    :cond_0
    return-void
.end method

.method public k()V
    .locals 3

    .prologue
    .line 136
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->k()V

    .line 137
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "profile::empty_state::dismiss"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->a([Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 161
    iget-object v0, p0, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->c:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/twitter/android/media/selection/c;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 162
    return-void
.end method
