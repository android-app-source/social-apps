.class Lcom/twitter/android/widget/ap$a;
.super Landroid/database/DataSetObserver;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/widget/ap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/ap;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/ap;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/twitter/android/widget/ap$a;->a:Lcom/twitter/android/widget/ap;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/twitter/android/widget/ap$a;->a:Lcom/twitter/android/widget/ap;

    iget-boolean v0, v0, Lcom/twitter/android/widget/ap;->a:Z

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/twitter/android/widget/ap$a;->a:Lcom/twitter/android/widget/ap;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ap;->notifyDataSetChanged()V

    .line 381
    :cond_0
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/twitter/android/widget/ap$a;->a:Lcom/twitter/android/widget/ap;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ap;->notifyDataSetInvalidated()V

    .line 374
    return-void
.end method
