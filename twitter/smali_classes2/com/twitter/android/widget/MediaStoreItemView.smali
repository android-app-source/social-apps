.class public Lcom/twitter/android/widget/MediaStoreItemView;
.super Lcom/twitter/media/ui/image/BaseMediaImageView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/MediaStoreItemView$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/media/ui/image/BaseMediaImageView",
        "<",
        "Lcom/twitter/android/widget/MediaStoreItemView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Lcom/twitter/android/widget/MediaBadgeOverlayView;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Lcom/twitter/media/model/b;

.field private r:Lcom/twitter/model/media/EditableMedia;

.field private s:Z

.field private t:Lcom/twitter/model/media/MediaSource;

.field private u:Landroid/view/View;

.field private v:Z

.field private w:Lcom/twitter/android/widget/MediaStoreItemView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 62
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/widget/MediaStoreItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/MediaStoreItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/media/ui/image/BaseMediaImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->a:I

    .line 72
    const v0, 0x7f020697

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->setErrorDrawableId(I)V

    .line 73
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->a(Landroid/view/LayoutInflater;)V

    .line 74
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 223
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 224
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 225
    iget v2, p0, Lcom/twitter/android/widget/MediaStoreItemView;->a:I

    sub-int v2, v1, v2

    .line 226
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 227
    iget v4, p0, Lcom/twitter/android/widget/MediaStoreItemView;->a:I

    sub-int v4, v3, v4

    .line 228
    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 229
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/MediaStoreItemView;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/MediaStoreItemView;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/MediaStoreItemView;)Lcom/twitter/android/widget/MediaStoreItemView$a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->w:Lcom/twitter/android/widget/MediaStoreItemView$a;

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;)V
    .locals 2

    .prologue
    .line 77
    const v0, 0x7f0401ae

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 79
    const v0, 0x7f13049f

    .line 80
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaBadgeOverlayView;

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->n:Lcom/twitter/android/widget/MediaBadgeOverlayView;

    .line 81
    const v0, 0x7f13049d

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->j:Landroid/widget/ImageView;

    .line 82
    const v0, 0x7f1304a1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->k:Landroid/widget/ImageView;

    .line 83
    const v0, 0x7f1304a2

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->l:Landroid/view/View;

    .line 84
    const v0, 0x7f1304a4

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->m:Landroid/view/View;

    .line 85
    const v0, 0x7f1304a3

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    .line 86
    const v0, 0x7f1304a0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->p:Landroid/view/View;

    .line 87
    const v0, 0x7f13049e

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->u:Landroid/view/View;

    .line 88
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/widget/MediaStoreItemView$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/MediaStoreItemView$1;-><init>(Lcom/twitter/android/widget/MediaStoreItemView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/MediaStoreItemView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/widget/MediaStoreItemView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->p:Landroid/view/View;

    return-object v0
.end method

.method private d(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 281
    if-eqz p1, :cond_0

    .line 282
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 284
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->p:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 294
    :goto_0
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 295
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 296
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->p:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v2, [F

    fill-array-data v2, :array_1

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 287
    new-instance v1, Lcom/twitter/android/widget/MediaStoreItemView$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/MediaStoreItemView$3;-><init>(Lcom/twitter/android/widget/MediaStoreItemView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 284
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 286
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private setEditableMedia(Lcom/twitter/model/media/EditableMedia;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 176
    iput-object p1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->r:Lcom/twitter/model/media/EditableMedia;

    .line 178
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->n:Lcom/twitter/android/widget/MediaBadgeOverlayView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 181
    if-eqz p1, :cond_0

    .line 182
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    .line 183
    sget-object v1, Lcom/twitter/android/widget/MediaStoreItemView$4;->a:[I

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 203
    :goto_0
    :pswitch_0
    iget-boolean v1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->s:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 205
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    if-eqz v1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 208
    if-eqz v1, :cond_3

    .line 209
    new-instance v1, Lcom/twitter/android/widget/MediaStoreItemView$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView$2;-><init>(Lcom/twitter/android/widget/MediaStoreItemView;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 219
    :goto_3
    return-void

    .line 185
    :pswitch_1
    iget-object v1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->u:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 192
    :pswitch_2
    iget-object v1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->u:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 200
    :cond_0
    sget-object v0, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    goto :goto_0

    :cond_1
    move v1, v2

    .line 203
    goto :goto_1

    .line 205
    :cond_2
    const/4 v2, 0x4

    goto :goto_2

    .line 217
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_3

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(I)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->n:Lcom/twitter/android/widget/MediaBadgeOverlayView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a()V

    .line 117
    return-void
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    return-void
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 157
    invoke-super {p0, p1, p2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V

    .line 158
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    .line 160
    iget-object v2, p0, Lcom/twitter/android/widget/MediaStoreItemView;->t:Lcom/twitter/model/media/MediaSource;

    invoke-static {v0, v2}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/twitter/android/widget/MediaStoreItemView;->setEditableMedia(Lcom/twitter/model/media/EditableMedia;)V

    .line 161
    instance-of v2, v0, Lcom/twitter/media/model/AnimatedGifFile;

    if-eqz v2, :cond_0

    .line 162
    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {}, Lwe;->a()I

    move-result v2

    if-le v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->v:Z

    .line 163
    iget-boolean v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->v:Z

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 162
    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 244
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    .line 245
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 246
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/widget/MediaStoreItemView;->k:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 248
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/MediaStoreItemView;->d(Z)V

    .line 250
    :cond_0
    return-void

    .line 245
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 253
    iget-object v1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->l:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 254
    return-void

    .line 253
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 257
    iget-object v1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->m:Landroid/view/View;

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->v:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 258
    return-void

    .line 257
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public getEditableMedia()Lcom/twitter/model/media/EditableMedia;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->r:Lcom/twitter/model/media/EditableMedia;

    return-object v0
.end method

.method public getImageSize()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 105
    invoke-static {p0}, Lcom/twitter/util/math/Size;->a(Landroid/view/View;)Lcom/twitter/util/math/Size;

    move-result-object v0

    return-object v0
.end method

.method public getImageViewAnimator()Landroid/view/ViewPropertyAnimator;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public getMediaStoreItem()Lcom/twitter/media/model/b;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->q:Lcom/twitter/media/model/b;

    return-object v0
.end method

.method public getMediaType()Lcom/twitter/media/model/MediaType;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->q:Lcom/twitter/media/model/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->q:Lcom/twitter/media/model/b;

    iget-object v0, v0, Lcom/twitter/media/model/b;->d:Lcom/twitter/media/model/MediaType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 2

    .prologue
    .line 239
    invoke-super {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->r:Lcom/twitter/model/media/EditableMedia;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->r:Lcom/twitter/model/media/EditableMedia;

    .line 240
    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 239
    :goto_0
    return v0

    .line 240
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 300
    invoke-super {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->onDetachedFromWindow()V

    .line 301
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->w:Lcom/twitter/android/widget/MediaStoreItemView$a;

    invoke-interface {v0, p0}, Lcom/twitter/android/widget/MediaStoreItemView$a;->b(Lcom/twitter/android/widget/MediaStoreItemView;)V

    .line 302
    return-void
.end method

.method public setCallback(Lcom/twitter/android/widget/MediaStoreItemView$a;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->w:Lcom/twitter/android/widget/MediaStoreItemView$a;

    .line 127
    return-void
.end method

.method public setMediaStoreItem(Lcom/twitter/media/model/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 130
    iput-object p1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->q:Lcom/twitter/media/model/b;

    .line 131
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->n:Lcom/twitter/android/widget/MediaBadgeOverlayView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a()V

    .line 133
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->v:Z

    .line 136
    if-nez p1, :cond_0

    .line 137
    invoke-direct {p0, v1}, Lcom/twitter/android/widget/MediaStoreItemView;->setEditableMedia(Lcom/twitter/model/media/EditableMedia;)V

    .line 138
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/MediaStoreItemView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-static {p1}, Lcom/twitter/library/media/manager/d;->a(Lcom/twitter/media/model/b;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 141
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Landroid/graphics/Bitmap$Config;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 140
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->b(Lcom/twitter/media/request/a$a;)Z

    goto :goto_0
.end method

.method public setShowExpand(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 261
    iget-boolean v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->s:Z

    if-eq v0, p1, :cond_0

    .line 262
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 263
    iput-boolean p1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->s:Z

    .line 264
    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->r:Lcom/twitter/model/media/EditableMedia;

    if-eqz v1, :cond_3

    .line 265
    iget-object v1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->r:Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {v1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    .line 266
    sget-object v4, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    if-eq v1, v4, :cond_1

    sget-object v4, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-eq v1, v4, :cond_1

    const/4 v1, 0x1

    .line 267
    :goto_0
    iget-object v4, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 268
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTouchDelegate()Landroid/view/TouchDelegate;

    move-result-object v1

    if-nez v1, :cond_0

    .line 269
    new-instance v1, Landroid/view/TouchDelegate;

    .line 270
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    invoke-direct {v1, v2, v3}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 269
    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 277
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v1, v2

    .line 266
    goto :goto_0

    :cond_2
    move v2, v3

    .line 267
    goto :goto_1

    .line 273
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 274
    iget-object v0, p0, Lcom/twitter/android/widget/MediaStoreItemView;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public setSource(Lcom/twitter/model/media/MediaSource;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/android/widget/MediaStoreItemView;->t:Lcom/twitter/model/media/MediaSource;

    .line 100
    return-void
.end method
