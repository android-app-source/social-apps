.class public Lcom/twitter/android/widget/l;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lcom/twitter/android/widget/MediaStoreItemView$a;
.implements Lcom/twitter/media/ui/image/BaseMediaImageView$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/l$c;,
        Lcom/twitter/android/widget/l$a;,
        Lcom/twitter/android/widget/l$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/widget/CursorAdapter;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Lcom/twitter/android/widget/MediaStoreItemView$a;",
        "Lcom/twitter/media/ui/image/BaseMediaImageView$b",
        "<",
        "Lcom/twitter/android/widget/MediaStoreItemView;",
        ">;"
    }
.end annotation


# instance fields
.field protected final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/util/collection/ReferenceMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/ReferenceMap",
            "<",
            "Landroid/net/Uri;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z

.field private final e:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private final f:I

.field private g:Z

.field private h:Z

.field private i:Lcom/twitter/android/widget/l$b;

.field private j:Z

.field private k:Lcom/twitter/android/widget/l$a;

.field private l:Lcom/twitter/android/widget/l$c;


# direct methods
.method public constructor <init>(Landroid/content/Context;IZI)V
    .locals 3
    .param p4    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/l;->a:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    .line 42
    invoke-static {}, Lcom/twitter/util/collection/ReferenceMap;->b()Lcom/twitter/util/collection/ReferenceMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    .line 58
    iput p2, p0, Lcom/twitter/android/widget/l;->f:I

    .line 59
    iput-boolean p3, p0, Lcom/twitter/android/widget/l;->d:Z

    .line 60
    iput p4, p0, Lcom/twitter/android/widget/l;->e:I

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x7f0d01bc

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 62
    invoke-static {}, Lbpo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x7f0d01bd

    .line 64
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 66
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/l;)Lcom/twitter/android/widget/l$a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/widget/l;->k:Lcom/twitter/android/widget/l$a;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/l;Lcom/twitter/android/widget/MediaStoreItemView;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/l;->c(Lcom/twitter/android/widget/MediaStoreItemView;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/l;)Lcom/twitter/android/widget/l$c;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/widget/l;->l:Lcom/twitter/android/widget/l$c;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceMap;->h()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 257
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/l;->d(Landroid/view/View;)V

    .line 258
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/l;->c(Landroid/view/View;)V

    goto :goto_0

    .line 260
    :cond_0
    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    .line 174
    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaStoreItemView;->isSelected()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/android/widget/l;->j:Z

    if-eqz v1, :cond_0

    .line 175
    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaStoreItemView;->getMediaType()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    sget-object v4, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne v1, v4, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/widget/l;->h:Z

    if-eqz v1, :cond_2

    :cond_1
    move v1, v2

    .line 176
    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaStoreItemView;->c(Z)V

    .line 177
    if-nez v1, :cond_3

    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 178
    return-void

    :cond_2
    move v1, v3

    .line 175
    goto :goto_0

    :cond_3
    move v2, v3

    .line 177
    goto :goto_1
.end method

.method private c(Lcom/twitter/android/widget/MediaStoreItemView;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 204
    invoke-virtual {p1, v1}, Lcom/twitter/android/widget/MediaStoreItemView;->a(Z)V

    .line 205
    iget-boolean v0, p0, Lcom/twitter/android/widget/l;->d:Z

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->setShowExpand(Z)V

    .line 206
    invoke-virtual {p1, v1}, Lcom/twitter/android/widget/MediaStoreItemView;->b(Z)V

    .line 207
    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 181
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    .line 182
    iget-object v1, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    invoke-static {v0}, Lcom/twitter/android/widget/l;->e(Lcom/twitter/android/widget/MediaStoreItemView;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    iget-object v1, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 184
    invoke-static {}, Lbpo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    new-instance v1, Lcom/twitter/android/widget/l$3;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/widget/l$3;-><init>(Lcom/twitter/android/widget/l;Lcom/twitter/android/widget/MediaStoreItemView;)V

    const-wide/16 v2, 0x7d

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/widget/MediaStoreItemView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 201
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/l;->c(Lcom/twitter/android/widget/MediaStoreItemView;)V

    goto :goto_0

    .line 195
    :cond_1
    invoke-static {}, Lbpo;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/MediaStoreItemView;->setShowExpand(Z)V

    .line 198
    :cond_2
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/MediaStoreItemView;->a(Z)V

    .line 199
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/MediaStoreItemView;->b(Z)V

    goto :goto_0
.end method

.method private d(Lcom/twitter/android/widget/MediaStoreItemView;)V
    .locals 2

    .prologue
    .line 319
    invoke-static {p1}, Lcom/twitter/android/widget/l;->e(Lcom/twitter/android/widget/MediaStoreItemView;)Landroid/net/Uri;

    move-result-object v1

    .line 320
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 321
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 322
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/ReferenceMap;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/MediaStoreItemView;->setMediaStoreItem(Lcom/twitter/media/model/b;)V

    .line 325
    return-void
.end method

.method private static e(Lcom/twitter/android/widget/MediaStoreItemView;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaStoreItemView;->getMediaStoreItem()Lcom/twitter/media/model/b;

    move-result-object v0

    .line 329
    if-nez v0, :cond_0

    .line 330
    const/4 v0, 0x0

    .line 332
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/twitter/media/model/b;->c:Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)Landroid/view/View;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 242
    if-eqz v0, :cond_0

    .line 243
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/l;->d(Landroid/view/View;)V

    .line 245
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/twitter/android/widget/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    return-void
.end method

.method public a(Lcom/twitter/android/widget/MediaStoreItemView;)V
    .locals 2

    .prologue
    .line 303
    invoke-virtual {p1}, Lcom/twitter/android/widget/MediaStoreItemView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/l;->i:Lcom/twitter/android/widget/l$b;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/twitter/android/widget/l;->i:Lcom/twitter/android/widget/l$b;

    invoke-virtual {p1}, Lcom/twitter/android/widget/MediaStoreItemView;->getEditableMedia()Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/l$b;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 306
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/widget/MediaStoreItemView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 4

    .prologue
    .line 338
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/l;->c(Landroid/view/View;)V

    .line 339
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->b()Lcom/twitter/media/request/ImageResponse$Error;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/request/ImageResponse$Error;->a:Lcom/twitter/media/request/ImageResponse$Error;

    if-ne v0, v1, :cond_0

    .line 340
    invoke-virtual {p1}, Lcom/twitter/android/widget/MediaStoreItemView;->getMediaStoreItem()Lcom/twitter/media/model/b;

    move-result-object v0

    .line 341
    if-eqz v0, :cond_0

    .line 342
    iget-object v1, p0, Lcom/twitter/android/widget/l;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/twitter/media/model/b;->c:Landroid/net/Uri;

    .line 344
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x0

    new-instance v3, Lcom/twitter/android/widget/l$4;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/l$4;-><init>(Lcom/twitter/android/widget/l;)V

    .line 342
    invoke-static {v1, v2, v0, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 353
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/widget/l$a;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/android/widget/l;->k:Lcom/twitter/android/widget/l$a;

    .line 70
    return-void
.end method

.method public a(Lcom/twitter/android/widget/l$b;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/twitter/android/widget/l;->i:Lcom/twitter/android/widget/l$b;

    .line 278
    return-void
.end method

.method public a(Lcom/twitter/android/widget/l$c;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/twitter/android/widget/l;->l:Lcom/twitter/android/widget/l$c;

    .line 74
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lcom/twitter/android/widget/MediaStoreItemView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/l;->a(Lcom/twitter/android/widget/MediaStoreItemView;Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/twitter/android/widget/l;->h:Z

    if-eq v0, p1, :cond_0

    .line 264
    iput-boolean p1, p0, Lcom/twitter/android/widget/l;->h:Z

    .line 265
    invoke-direct {p0}, Lcom/twitter/android/widget/l;->c()V

    .line 267
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableImage;)Z
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableImage;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/l;->a(Lcom/twitter/model/media/EditableMedia;)Landroid/view/View;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/twitter/android/widget/MediaStoreItemView;

    if-eqz v1, :cond_0

    .line 159
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaStoreItemView;->getEditableMedia()Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 160
    if-eqz v0, :cond_0

    .line 161
    iget-boolean v1, p1, Lcom/twitter/model/media/EditableImage;->b:Z

    iput-boolean v1, v0, Lcom/twitter/model/media/EditableImage;->b:Z

    .line 162
    iget v1, p1, Lcom/twitter/model/media/EditableImage;->c:I

    iput v1, v0, Lcom/twitter/model/media/EditableImage;->c:I

    .line 163
    iget v1, p1, Lcom/twitter/model/media/EditableImage;->d:F

    iput v1, v0, Lcom/twitter/model/media/EditableImage;->d:F

    .line 164
    iget-object v1, p1, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    iput-object v1, v0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 165
    iget v1, p1, Lcom/twitter/model/media/EditableImage;->e:I

    iput v1, v0, Lcom/twitter/model/media/EditableImage;->e:I

    .line 166
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceMap;->h()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 296
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/MediaStoreItemView;->setMediaStoreItem(Lcom/twitter/media/model/b;)V

    goto :goto_0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceMap;->c()V

    .line 299
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/twitter/android/widget/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    new-instance v0, Lcom/twitter/android/widget/ao;

    invoke-direct {v0, p1}, Lcom/twitter/android/widget/ao;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 228
    return-void
.end method

.method public b(Lcom/twitter/android/widget/MediaStoreItemView;)V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/l;->d(Lcom/twitter/android/widget/MediaStoreItemView;)V

    .line 311
    return-void
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;)V
    .locals 2

    .prologue
    .line 231
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->d()Landroid/net/Uri;

    move-result-object v0

    .line 232
    iget-object v1, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v1, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 234
    if-eqz v0, :cond_0

    .line 235
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/l;->d(Landroid/view/View;)V

    .line 237
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/twitter/android/widget/l;->j:Z

    if-eq v0, p1, :cond_0

    .line 271
    iput-boolean p1, p0, Lcom/twitter/android/widget/l;->j:Z

    .line 272
    invoke-direct {p0}, Lcom/twitter/android/widget/l;->c()V

    .line 274
    :cond_0
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    .line 212
    iget-object v1, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-static {v0}, Lcom/twitter/android/widget/l;->e(Lcom/twitter/android/widget/MediaStoreItemView;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/ReferenceMap;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    new-instance v1, Lcom/twitter/media/model/b;

    invoke-direct {v1, p3}, Lcom/twitter/media/model/b;-><init>(Landroid/database/Cursor;)V

    .line 214
    iget-object v2, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    iget-object v3, v1, Lcom/twitter/media/model/b;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3, p1}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaStoreItemView;->setMediaStoreItem(Lcom/twitter/media/model/b;)V

    .line 216
    iget-boolean v1, p0, Lcom/twitter/android/widget/l;->g:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaStoreItemView;->setFromMemoryOnly(Z)V

    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/l;->d(Landroid/view/View;)V

    .line 218
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/l;->c(Landroid/view/View;)V

    .line 219
    return-void
.end method

.method c(Z)V
    .locals 2

    .prologue
    .line 281
    iget-boolean v0, p0, Lcom/twitter/android/widget/l;->g:Z

    if-eq v0, p1, :cond_0

    .line 282
    iput-boolean p1, p0, Lcom/twitter/android/widget/l;->g:Z

    .line 283
    iget-object v0, p0, Lcom/twitter/android/widget/l;->c:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceMap;->h()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 284
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/MediaStoreItemView;->setFromMemoryOnly(Z)V

    goto :goto_0

    .line 287
    :cond_0
    return-void
.end method

.method public c(Lcom/twitter/model/media/EditableMedia;)Z
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/twitter/android/widget/l;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/l;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/widget/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/widget/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 90
    if-ge p1, v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/twitter/android/widget/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 95
    :goto_0
    return-object v0

    .line 94
    :cond_0
    instance-of v1, p2, Lcom/twitter/android/widget/MediaStoreItemView;

    if-eqz v1, :cond_1

    .line 95
    :goto_1
    sub-int v0, p1, v0

    invoke-super {p0, v0, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_1
    const/4 p2, 0x0

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 103
    iget v0, p0, Lcom/twitter/android/widget/l;->e:I

    if-eqz v0, :cond_1

    .line 104
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/widget/l;->e:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 105
    const v0, 0x7f130050

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    .line 111
    :goto_0
    sget-object v2, Lcom/twitter/model/media/MediaSource;->c:Lcom/twitter/model/media/MediaSource;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/MediaStoreItemView;->setSource(Lcom/twitter/model/media/MediaSource;)V

    .line 112
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/MediaStoreItemView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 114
    iget v2, p0, Lcom/twitter/android/widget/l;->f:I

    if-lez v2, :cond_0

    .line 115
    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaStoreItemView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 116
    iget v3, p0, Lcom/twitter/android/widget/l;->f:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 117
    iget v3, p0, Lcom/twitter/android/widget/l;->f:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 120
    :cond_0
    new-instance v2, Lcom/twitter/android/widget/l$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/widget/l$1;-><init>(Lcom/twitter/android/widget/l;Lcom/twitter/android/widget/MediaStoreItemView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    new-instance v2, Lcom/twitter/android/widget/l$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/widget/l$2;-><init>(Lcom/twitter/android/widget/l;Lcom/twitter/android/widget/MediaStoreItemView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 140
    new-instance v2, Lcom/twitter/android/widget/ao;

    invoke-direct {v2, v1}, Lcom/twitter/android/widget/ao;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 142
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/MediaStoreItemView;->setCallback(Lcom/twitter/android/widget/MediaStoreItemView$a;)V

    .line 143
    iget-boolean v2, p0, Lcom/twitter/android/widget/l;->d:Z

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/MediaStoreItemView;->setShowExpand(Z)V

    .line 145
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 146
    return-object v1

    .line 107
    :cond_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04010b

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 108
    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    goto :goto_0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaStoreItemView;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/l;->d(Lcom/twitter/android/widget/MediaStoreItemView;)V

    .line 292
    return-void
.end method
