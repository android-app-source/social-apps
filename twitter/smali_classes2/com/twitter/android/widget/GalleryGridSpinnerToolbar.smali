.class public Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;
.super Lcom/twitter/android/widget/GalleryGridToolbar;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;,
        Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/Spinner;

.field private b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

.field private c:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/GalleryGridToolbar;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->d:I

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->e:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/GalleryGridToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->d:I

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->e:Z

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/GalleryGridToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->d:I

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->e:Z

    .line 45
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 112
    const v1, 0x7f040108

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 113
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setCustomView(Landroid/view/View;)V

    .line 114
    const v1, 0x7f1303a2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->a:Landroid/widget/Spinner;

    .line 115
    new-instance v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    .line 116
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    iget-boolean v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->e:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(Z)V

    .line 117
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->a:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 118
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 119
    return-void
.end method

.method protected a(Lcom/twitter/media/model/MediaStoreBucket;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->c:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->c:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;->a(Lcom/twitter/media/model/MediaStoreBucket;)V

    .line 125
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->c:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->e:Z

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->c:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;

    invoke-interface {v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;->f()V

    .line 131
    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;)I

    move-result v0

    if-ne p3, v0, :cond_1

    .line 88
    iget v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->d:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setSelectedMediaBucket(I)V

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b()V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    if-nez p3, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/model/MediaStoreBucket;->a(Landroid/content/res/Resources;)Lcom/twitter/media/model/MediaStoreBucket;

    move-result-object v0

    .line 99
    :goto_1
    iget v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->d:I

    if-eq p3, v1, :cond_0

    .line 100
    iput p3, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->d:I

    .line 101
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaStoreBucket;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->a(Lcom/twitter/media/model/MediaStoreBucket;)V

    goto :goto_0

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    add-int/lit8 v1, p3, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaStoreBucket;

    goto :goto_1
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 107
    return-void
.end method

.method public setMediaBucketCursor(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(Landroid/database/Cursor;)V

    .line 70
    return-void
.end method

.method public setMoreEnabled(Z)V
    .locals 1

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->e:Z

    .line 78
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(Z)V

    .line 80
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->notifyDataSetChanged()V

    .line 82
    :cond_0
    return-void
.end method

.method public setSelectedMediaBucket(I)V
    .locals 2

    .prologue
    .line 48
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->a:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/Spinner;->setSelection(IZ)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iput p1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->d:I

    goto :goto_0
.end method

.method public setSelectedMediaBucket(Lcom/twitter/media/model/MediaStoreBucket;)V
    .locals 6

    .prologue
    .line 56
    if-nez p1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-virtual {v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->b:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->getItemId(I)J

    move-result-wide v2

    .line 61
    invoke-virtual {p1}, Lcom/twitter/media/model/MediaStoreBucket;->b()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 62
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setSelectedMediaBucket(I)V

    goto :goto_0

    .line 59
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setSpinnerActionListener(Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->c:Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;

    .line 74
    return-void
.end method
