.class public Lcom/twitter/android/widget/WhoToFollowUsersView;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# instance fields
.field protected a:I

.field protected b:Landroid/view/LayoutInflater;

.field private c:Lcom/twitter/app/users/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/WhoToFollowUsersView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/WhoToFollowUsersView;->b:Landroid/view/LayoutInflater;

    .line 56
    sget-object v0, Lcom/twitter/android/bi$a;->WhoToFollowUsersView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    const/4 v1, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/WhoToFollowUsersView;->a:I

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method private a()Lcom/twitter/ui/user/UserSocialView;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/widget/WhoToFollowUsersView;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0403f3

    const/4 v2, 0x0

    .line 72
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    .line 73
    invoke-virtual {p0}, Lcom/twitter/android/widget/WhoToFollowUsersView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/android/widget/WhoToFollowUsersView;->a(Landroid/content/Context;Lcom/twitter/ui/user/UserSocialView;)V

    .line 74
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/WhoToFollowUsersView;->addView(Landroid/view/View;)V

    .line 75
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/ui/user/UserSocialView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 144
    sget v0, Lcni;->a:F

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setContentSize(F)V

    .line 145
    invoke-static {}, Lbpi;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setUserImageSize(I)V

    .line 147
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 148
    const v1, 0x7f110177

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/UserSocialView;->setScreenNameColor(I)V

    .line 149
    const v1, 0x7f0200b1

    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/UserSocialView;->setFollowBackgroundResource(I)V

    .line 151
    const v1, 0x7f0a03a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, Lcom/twitter/ui/user/UserSocialView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const v0, 0x7f0200b0

    invoke-virtual {p1, v0, v3}, Lcom/twitter/ui/user/UserSocialView;->a(ILcom/twitter/ui/user/BaseUserView$a;)V

    .line 154
    invoke-virtual {p1, v2}, Lcom/twitter/ui/user/UserSocialView;->a(Z)V

    .line 155
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setProfileDescriptionMaxLines(I)V

    .line 156
    invoke-virtual {p1, v2}, Lcom/twitter/ui/user/UserSocialView;->c(Z)V

    .line 157
    invoke-virtual {p1, v2}, Lcom/twitter/ui/user/UserSocialView;->setShowIconOnFollowButton(Z)V

    .line 158
    return-void
.end method

.method private a(Lcom/twitter/ui/user/UserSocialView;Lcom/twitter/android/timeline/co;Lcom/twitter/model/core/TwitterSocialProof;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/model/timeline/r;IILcom/twitter/android/timeline/cu;Lcom/twitter/model/timeline/at$a;)V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p2, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    .line 115
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 116
    iput p6, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 117
    iput p7, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 118
    if-eqz p5, :cond_0

    .line 119
    iput-object p5, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 120
    iget-object v2, p5, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/twitter/ui/user/UserSocialView;->setScribeComponent(Ljava/lang/String;)V

    .line 122
    :cond_0
    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/UserSocialView;->setScribeItem(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 124
    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 125
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iget-object v2, v0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    invoke-virtual {p1, v1, v2}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 126
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    invoke-virtual {p1, p3, v1}, Lcom/twitter/ui/user/UserSocialView;->a(Lcom/twitter/model/core/TwitterSocialProof;Z)V

    .line 127
    invoke-virtual {p4, v0}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 129
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    invoke-virtual {p4, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/widget/WhoToFollowUsersView;->c:Lcom/twitter/app/users/c;

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setActionButtonClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/widget/WhoToFollowUsersView;->c:Lcom/twitter/app/users/c;

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    if-eqz p8, :cond_1

    .line 138
    invoke-virtual {p8, p2, p6}, Lcom/twitter/android/timeline/cu;->a(Lcom/twitter/android/timeline/co;I)V

    .line 140
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/cq;Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/android/timeline/cu;)V
    .locals 12

    .prologue
    .line 80
    iget-object v10, p1, Lcom/twitter/android/timeline/cq;->a:Lcbi;

    .line 81
    iget-object v9, p1, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    .line 84
    invoke-virtual {v10}, Lcbi;->be_()I

    move-result v0

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/widget/WhoToFollowUsersView;->getChildCount()I

    move-result v1

    .line 88
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 90
    const/4 v6, 0x0

    :goto_0
    if-ge v6, v11, :cond_1

    .line 91
    invoke-virtual {v10, v6}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/timeline/co;

    .line 92
    invoke-virtual {p0, v6}, Lcom/twitter/android/widget/WhoToFollowUsersView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    .line 93
    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 94
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setVisibility(I)V

    .line 90
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 96
    :cond_0
    iget-object v1, v2, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    .line 97
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->b()Ljava/lang/String;

    move-result-object v4

    .line 98
    iget-object v1, v9, Lcom/twitter/model/timeline/at$a;->f:Ljava/util/Map;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/TwitterSocialProof;

    .line 99
    if-nez v0, :cond_2

    .line 100
    invoke-direct {p0}, Lcom/twitter/android/widget/WhoToFollowUsersView;->a()Lcom/twitter/ui/user/UserSocialView;

    move-result-object v1

    .line 102
    :goto_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/twitter/ui/user/UserSocialView;->setVisibility(I)V

    .line 103
    iget-object v0, v9, Lcom/twitter/model/timeline/at$a;->g:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/model/timeline/r;

    move-object v0, p0

    move-object v4, p2

    move v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/widget/WhoToFollowUsersView;->a(Lcom/twitter/ui/user/UserSocialView;Lcom/twitter/android/timeline/co;Lcom/twitter/model/core/TwitterSocialProof;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/model/timeline/r;IILcom/twitter/android/timeline/cu;Lcom/twitter/model/timeline/at$a;)V

    goto :goto_1

    .line 107
    :cond_1
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_2
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 64
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/twitter/android/widget/WhoToFollowUsersView;->a:I

    if-ge v0, v1, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/twitter/android/widget/WhoToFollowUsersView;->a()Lcom/twitter/ui/user/UserSocialView;

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method public setTimelineUserClickListener(Lcom/twitter/app/users/c;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/twitter/android/widget/WhoToFollowUsersView;->c:Lcom/twitter/app/users/c;

    .line 162
    return-void
.end method
