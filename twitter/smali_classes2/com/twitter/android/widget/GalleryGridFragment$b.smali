.class Lcom/twitter/android/widget/GalleryGridFragment$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/widget/GalleryGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcom/twitter/android/widget/GalleryGridFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 704
    const-class v0, Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/widget/GalleryGridFragment$b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/twitter/android/widget/GalleryGridFragment;)V
    .locals 0

    .prologue
    .line 709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 710
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    .line 711
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 759
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    .line 760
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 739
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    if-nez v0, :cond_0

    .line 741
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "delivers data to destroyed GalleryGridFragment: id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 742
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 743
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 744
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 748
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 749
    :cond_1
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 715
    sget-boolean v0, Lcom/twitter/android/widget/GalleryGridFragment$b;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 716
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 734
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 718
    :pswitch_0
    const-string/jumbo v0, "media_bucket"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaStoreBucket;

    .line 719
    if-eqz v0, :cond_1

    .line 720
    new-instance v1, Lcom/twitter/media/util/n;

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v2}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 721
    invoke-static {}, Lcom/twitter/media/util/t;->a()Z

    move-result v3

    invoke-direct {v1, v2, v4, v3, v0}, Lcom/twitter/media/util/n;-><init>(Landroid/content/Context;ZZLcom/twitter/media/model/MediaStoreBucket;)V

    move-object v0, v1

    .line 720
    goto :goto_0

    .line 723
    :cond_1
    new-instance v0, Lcom/twitter/media/util/n;

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v1}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 724
    invoke-static {}, Lcom/twitter/media/util/t;->a()Z

    move-result v2

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/media/util/n;-><init>(Landroid/content/Context;ZZ)V

    goto :goto_0

    .line 728
    :pswitch_1
    new-instance v0, Lcom/twitter/media/util/m;

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v1}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/media/util/m;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 716
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 704
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/GalleryGridFragment$b;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 753
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment$b;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->g()V

    .line 756
    :cond_0
    return-void
.end method
