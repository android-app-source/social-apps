.class public Lcom/twitter/android/widget/MuteConversationEducationOverlay;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/app/common/dialog/b$d;)V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/android/widget/x$a;

    invoke-direct {v0}, Lcom/twitter/android/widget/x$a;-><init>()V

    .line 49
    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/x$a;->a(Ljava/lang/String;)Lcom/twitter/android/widget/x$a;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/x$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/widget/x$a;

    move-result-object v0

    const v1, 0x7f0d0194

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/x$a;->i(I)Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a05ac

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->b(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a05ad

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a05ae

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->e(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f020822

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->a(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 56
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 57
    invoke-virtual {v0, p3}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 58
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 59
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->c(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->m()V

    .line 119
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->c:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->l()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v5, "mute_conversation_prompt"

    .line 128
    invoke-static {v4, v0, v5, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 127
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 130
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "mute_conversation_prompt"

    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->l()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 123
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/android/widget/x;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/x;->a(Landroid/os/Bundle;)Lcom/twitter/android/widget/x;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->a(Landroid/app/Dialog;Landroid/os/Bundle;)V

    .line 73
    const v0, 0x7f13001d

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 75
    const v0, 0x7f13001e

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    if-eqz v0, :cond_0

    .line 77
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0bca

    const v5, 0x7f1100c9

    const v6, 0x7f1100c8

    const-class v7, Lcom/twitter/android/WebViewActivity;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;IIILjava/lang/Class;)Lcom/twitter/ui/view/a;

    move-result-object v3

    aput-object v3, v1, v2

    .line 81
    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 82
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "{{}}"

    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_0
    return-void
.end method

.method public synthetic b()Lcom/twitter/android/dialog/g;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->a()Lcom/twitter/android/widget/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/android/dialog/f;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->a()Lcom/twitter/android/widget/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->a()Lcom/twitter/android/widget/x;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->g()V

    .line 113
    const-string/jumbo v0, "dismiss"

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->b(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->h()V

    .line 91
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->setCancelable(Z)V

    .line 92
    const-string/jumbo v0, "impression"

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->c(Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->i()V

    .line 99
    const-string/jumbo v0, "mute"

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->b(Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->j()V

    .line 106
    const-string/jumbo v0, "cancel"

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->b(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->a()Lcom/twitter/android/widget/x;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/twitter/android/widget/x;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->c:Ljava/lang/String;

    .line 66
    invoke-virtual {v0}, Lcom/twitter/android/widget/x;->w()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 67
    return-void
.end method
