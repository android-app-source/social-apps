.class public Lcom/twitter/android/widget/GalleryGridFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;
.implements Lcom/twitter/android/widget/GalleryGridToolbar$a;
.implements Lcom/twitter/android/widget/l$a;
.implements Lcom/twitter/android/widget/l$b;
.implements Lcom/twitter/android/widget/l$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/GalleryGridFragment$a;,
        Lcom/twitter/android/widget/GalleryGridFragment$b;
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/android/widget/l;

.field private final b:Lcom/twitter/android/widget/GalleryGridFragment$b;

.field private c:I

.field private d:I

.field private e:I

.field private f:[Landroid/view/View;

.field private g:Landroid/widget/GridView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Lcom/twitter/android/widget/GalleryGridFragment$a;

.field private k:Lcom/twitter/android/widget/ab;

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field private o:I

.field private p:Z

.field private q:Lcom/twitter/model/media/EditableImage;

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/twitter/android/media/selection/d;

.field private t:Z

.field private u:Lcom/twitter/android/widget/GalleryGridToolbar;

.field private v:Lcom/twitter/media/model/MediaStoreBucket;

.field private w:Landroid/database/Cursor;

.field private x:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 76
    new-instance v0, Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/GalleryGridFragment$b;-><init>(Lcom/twitter/android/widget/GalleryGridFragment;)V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->r:Ljava/util/Map;

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->x:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/GalleryGridFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Landroid/widget/GridView;

    return-object v0
.end method

.method public static a(IZI)Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 3

    .prologue
    .line 114
    new-instance v0, Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-direct {v0}, Lcom/twitter/android/widget/GalleryGridFragment;-><init>()V

    .line 115
    new-instance v1, Lcom/twitter/app/common/base/b$a;

    invoke-direct {v1}, Lcom/twitter/app/common/base/b$a;-><init>()V

    const-string/jumbo v2, "header"

    .line 116
    invoke-virtual {v1, v2, p2}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    const-string/jumbo v2, "scroll_header"

    .line 117
    invoke-virtual {v1, v2, p0}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    const-string/jumbo v2, "show_expand"

    .line 118
    invoke-virtual {v1, v2, p1}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    .line 119
    invoke-virtual {v1}, Lcom/twitter/app/common/base/b$a;->c()Lcom/twitter/app/common/base/b;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 120
    return-object v0
.end method

.method private a(II)V
    .locals 5

    .prologue
    .line 613
    if-lez p2, :cond_1

    .line 614
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {v1, v0, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 616
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 617
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    .line 618
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 619
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 620
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 621
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 623
    :cond_0
    iget-object v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v4, v3}, Lcom/twitter/android/widget/l;->a(Landroid/view/View;)V

    .line 617
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 626
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 124
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 600
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method protected static b(Lcom/twitter/media/model/MediaStoreBucket;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 699
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 700
    const-string/jumbo v1, "media_bucket"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 701
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/GalleryGridFragment;)Lcom/twitter/android/media/selection/d;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    return-object v0
.end method

.method private b(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040105

    const/4 v2, 0x0

    .line 377
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    .line 378
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    const v1, 0x7f13029f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 379
    new-instance v1, Lcom/twitter/android/widget/GalleryGridFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/GalleryGridFragment$2;-><init>(Lcom/twitter/android/widget/GalleryGridFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 389
    iget v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->c:I

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setTextColor(I)V

    .line 390
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    const v1, 0x7f1303a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Landroid/widget/TextView;

    .line 391
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Landroid/widget/TextView;

    iget v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 392
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 393
    return-void
.end method

.method private b([Landroid/view/View;)V
    .locals 4

    .prologue
    .line 629
    if-eqz p1, :cond_0

    .line 630
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 631
    iget-object v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v3, v2}, Lcom/twitter/android/widget/l;->b(Landroid/view/View;)V

    .line 630
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 634
    :cond_0
    return-void
.end method

.method private c(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 396
    invoke-static {}, Lbpo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040107

    .line 398
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    .line 403
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    const v1, 0x7f1303a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GalleryGridToolbar;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    .line 404
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->setListener(Lcom/twitter/android/widget/GalleryGridToolbar$a;)V

    .line 405
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    instance-of v0, v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    check-cast v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    .line 407
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setSpinnerActionListener(Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$b;)V

    .line 408
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 409
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setMediaBucketCursor(Landroid/database/Cursor;)V

    .line 410
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setSelectedMediaBucket(Lcom/twitter/media/model/MediaStoreBucket;)V

    .line 411
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    .line 416
    :cond_0
    :goto_1
    return-void

    .line 400
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040106

    .line 401
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    goto :goto_0

    .line 412
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->m()V

    goto :goto_1
.end method

.method private c(Lcom/twitter/model/media/EditableMedia;)Z
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 435
    :goto_0
    return v0

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/l;->c(Lcom/twitter/model/media/EditableMedia;)Z

    move-result v0

    goto :goto_0

    .line 435
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 637
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    if-eqz v0, :cond_1

    .line 640
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0}, Lcom/twitter/android/widget/l;->a()I

    move-result v0

    .line 647
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 648
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 650
    if-nez v0, :cond_3

    const v0, 0x7f0a03c4

    .line 651
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 653
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 658
    :cond_0
    :goto_2
    return-void

    .line 641
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 642
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 644
    goto :goto_0

    .line 651
    :cond_3
    const v3, 0x7f0c0025

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 652
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 654
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    if-eqz v1, :cond_0

    .line 655
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/GalleryGridToolbar;->setSelectedCount(I)V

    goto :goto_2
.end method

.method private l()V
    .locals 1

    .prologue
    .line 661
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/g;->f()Lcom/twitter/library/media/manager/e;

    move-result-object v0

    .line 662
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e;->a()Lcom/twitter/util/collection/g;

    move-result-object v0

    .line 663
    if-eqz v0, :cond_0

    .line 664
    invoke-interface {v0}, Lcom/twitter/util/collection/g;->a()V

    .line 666
    :cond_0
    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 686
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    instance-of v0, v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    if-eqz v0, :cond_0

    .line 687
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 691
    :goto_0
    iput-boolean v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->t:Z

    .line 692
    return-void

    .line 689
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    invoke-static {v2}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Lcom/twitter/media/model/MediaStoreBucket;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 201
    const v0, 0x7f040103

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 203
    const v0, 0x7f13039c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 204
    iget v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->d:I

    if-lez v1, :cond_0

    .line 205
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 206
    iget v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->d:I

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 207
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    :goto_0
    const v0, 0x7f13039d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 213
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 214
    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 216
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x7f010031

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v1, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 218
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setBackgroundResource(I)V

    .line 220
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0e017d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 222
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 223
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 225
    invoke-virtual {v0}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 226
    iput v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 227
    iput v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 229
    const v1, 0x7f130020

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 232
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Landroid/content/Context;)I

    move-result v1

    .line 233
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 234
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    .line 236
    add-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    div-int v1, v3, v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setColumnWidth(I)V

    .line 237
    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Landroid/widget/GridView;

    .line 238
    return-object v2

    .line 209
    :cond_0
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    if-nez v0, :cond_0

    .line 365
    invoke-static {}, Lbpo;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->c(Landroid/view/ViewGroup;)V

    .line 371
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->k()V

    .line 372
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Landroid/view/View;

    return-object v0

    .line 368
    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public a(F)V
    .locals 9

    .prologue
    const v6, 0x7fffffff

    const/4 v1, 0x0

    .line 542
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    .line 544
    float-to-int v4, p1

    const/high16 v7, -0x80000000

    move v2, v1

    move v3, v1

    move v5, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 546
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalY()I

    move-result v2

    neg-int v2, v2

    .line 547
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 546
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 548
    invoke-virtual {v0}, Landroid/widget/Scroller;->getDuration()I

    move-result v0

    .line 546
    invoke-virtual {v1, v2, v0}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 549
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    if-nez v0, :cond_1

    .line 516
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->k()V

    .line 523
    return-void

    .line 520
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/l;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 298
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 300
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/l;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 302
    iget v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->o:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->o:I

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    .line 303
    invoke-virtual {v1}, Lcom/twitter/android/widget/l;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 304
    iget v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->o:I

    .line 305
    iput v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->o:I

    .line 306
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Landroid/widget/GridView;

    new-instance v2, Lcom/twitter/android/widget/GalleryGridFragment$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/widget/GalleryGridFragment$1;-><init>(Lcom/twitter/android/widget/GalleryGridFragment;I)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    .line 314
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 317
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "composition::photo_gallery::load_finished"

    aput-object v1, v0, v3

    .line 318
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 319
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 317
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 325
    :pswitch_1
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326
    iput-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    .line 327
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-virtual {v0, v4, v2, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 330
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    instance-of v0, v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    if-eqz v0, :cond_3

    .line 331
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    check-cast v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    .line 332
    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setMediaBucketCursor(Landroid/database/Cursor;)V

    .line 333
    iget-boolean v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->x:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setMoreEnabled(Z)V

    .line 334
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setSelectedMediaBucket(Lcom/twitter/media/model/MediaStoreBucket;)V

    goto :goto_0

    .line 335
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    if-nez v0, :cond_0

    .line 336
    iput-object p2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    goto/16 :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/View;Lcom/twitter/model/media/EditableMedia;)V
    .locals 1

    .prologue
    .line 420
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    if-eqz v0, :cond_0

    .line 421
    invoke-direct {p0, p2}, Lcom/twitter/android/widget/GalleryGridFragment;->c(Lcom/twitter/model/media/EditableMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    invoke-interface {v0, p2}, Lcom/twitter/android/media/selection/d;->b(Lcom/twitter/model/media/EditableMedia;)V

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    invoke-interface {v0, p2}, Lcom/twitter/android/media/selection/d;->a(Lcom/twitter/model/media/EditableMedia;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/media/selection/d;)V
    .locals 0

    .prologue
    .line 695
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    .line 696
    return-void
.end method

.method public a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 357
    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDrawerDraggable(Z)V

    .line 358
    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDispatchDragToChildren(Z)V

    .line 359
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setFullScreenHeaderView(Landroid/view/View;)V

    .line 360
    return-void
.end method

.method public a(Lcom/twitter/android/widget/GalleryGridFragment$a;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Lcom/twitter/android/widget/GalleryGridFragment$a;

    .line 494
    return-void
.end method

.method public a(Lcom/twitter/android/widget/ab;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->k:Lcom/twitter/android/widget/ab;

    .line 498
    return-void
.end method

.method public a(Lcom/twitter/media/model/MediaStoreBucket;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 277
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    .line 278
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Lcom/twitter/media/model/MediaStoreBucket;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 280
    invoke-virtual {p1}, Lcom/twitter/media/model/MediaStoreBucket;->b()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 281
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v4

    const-string/jumbo v2, ""

    aput-object v2, v1, v5

    const-string/jumbo v2, "photo_gallery"

    aput-object v2, v1, v6

    const-string/jumbo v2, "album_spinner"

    aput-object v2, v1, v7

    const-string/jumbo v2, "more"

    aput-object v2, v1, v8

    .line 282
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 281
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/media/model/MediaStoreBucket;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 284
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v4

    const-string/jumbo v2, ""

    aput-object v2, v1, v5

    const-string/jumbo v2, "photo_gallery"

    aput-object v2, v1, v6

    const-string/jumbo v2, "album_spinner"

    aput-object v2, v1, v7

    const-string/jumbo v2, "selected"

    aput-object v2, v1, v8

    .line 285
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 284
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/media/EditableImage;)V
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/l;->a(Lcom/twitter/model/media/EditableImage;)Z

    .line 683
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Lcom/twitter/android/widget/GalleryGridFragment$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->q:Lcom/twitter/model/media/EditableImage;

    if-nez v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->o:I

    .line 453
    invoke-static {}, Lbpo;->b()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->p:Z

    .line 454
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/l;->a(Lcom/twitter/model/media/EditableMedia;)Landroid/view/View;

    move-result-object v1

    .line 455
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 456
    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->q:Lcom/twitter/model/media/EditableImage;

    .line 458
    :cond_0
    if-eqz v1, :cond_1

    .line 459
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Lcom/twitter/android/widget/GalleryGridFragment$a;

    invoke-interface {v0, p1, v1}, Lcom/twitter/android/widget/GalleryGridFragment$a;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    .line 462
    :cond_1
    return-void

    .line 453
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    if-nez v0, :cond_1

    .line 527
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->m:Z

    .line 531
    :goto_1
    return-void

    .line 527
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/l;->a(Z)V

    goto :goto_1
.end method

.method public a([Landroid/view/View;)V
    .locals 0

    .prologue
    .line 581
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:[Landroid/view/View;

    .line 582
    return-void
.end method

.method public b(Landroid/view/View;Lcom/twitter/model/media/EditableMedia;)V
    .locals 4

    .prologue
    .line 441
    if-eqz p2, :cond_0

    .line 442
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "photo_gallery"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "thumbnail"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "long_press"

    aput-object v3, v1, v2

    .line 443
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 442
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 444
    invoke-virtual {p0, p2}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 446
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;)V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    if-nez v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 505
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->k()V

    .line 512
    return-void

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/l;->b(Lcom/twitter/model/media/EditableMedia;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    if-nez v0, :cond_0

    .line 535
    iput-boolean p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->n:Z

    .line 539
    :goto_0
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/l;->b(Z)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 593
    iput-boolean p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->x:Z

    .line 594
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    instance-of v0, v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    if-eqz v0, :cond_0

    .line 595
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    check-cast v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setMoreEnabled(Z)V

    .line 597
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    invoke-interface {v0}, Lcom/twitter/android/media/selection/d;->b()V

    .line 265
    :cond_0
    return-void
.end method

.method protected d(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 604
    new-instance v0, Lcom/twitter/android/widget/l;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v2, p1, v2}, Lcom/twitter/android/widget/l;-><init>(Landroid/content/Context;IZI)V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    .line 605
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->s:Lcom/twitter/android/media/selection/d;

    invoke-interface {v0}, Lcom/twitter/android/media/selection/d;->a()V

    .line 272
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Lcom/twitter/android/widget/GalleryGridFragment$a;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Lcom/twitter/android/widget/GalleryGridFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/widget/GalleryGridFragment$a;->aL_()V

    .line 295
    :cond_0
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 347
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/l;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 348
    iput-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->w:Landroid/database/Cursor;

    .line 349
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    instance-of v0, v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->u:Lcom/twitter/android/widget/GalleryGridToolbar;

    check-cast v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;

    .line 351
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setMediaBucketCursor(Landroid/database/Cursor;)V

    .line 352
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;->setSelectedMediaBucket(I)V

    .line 354
    :cond_0
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 481
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->t:Z

    if-eqz v0, :cond_2

    .line 482
    invoke-static {}, Lbpo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 485
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    .line 486
    invoke-static {v2}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Lcom/twitter/media/model/MediaStoreBucket;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 490
    :cond_1
    :goto_0
    return-void

    .line 487
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->m()V

    goto :goto_0
.end method

.method public i()Lcom/twitter/model/media/EditableImage;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->q:Lcom/twitter/model/media/EditableImage;

    return-object v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 674
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->q:Lcom/twitter/model/media/EditableImage;

    .line 675
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 129
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 130
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v1

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 133
    const v2, 0x7f0d01c2

    invoke-virtual {v0, v2, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 135
    invoke-static {}, Lbpo;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    const v2, 0x7f0d01c1

    invoke-virtual {v0, v2, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 139
    :cond_0
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 140
    const v3, 0x7f010032

    invoke-virtual {v0, v3, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 141
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->c:I

    .line 143
    const-string/jumbo v0, "header"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->d:I

    .line 144
    const-string/jumbo v0, "scroll_header"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->e:I

    .line 146
    if-eqz p1, :cond_1

    .line 147
    const-string/jumbo v0, "first_visible_position"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->o:I

    .line 150
    const-string/jumbo v0, "disable_grid_reload"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->p:Z

    .line 151
    const-string/jumbo v0, "expanded_image"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->q:Lcom/twitter/model/media/EditableImage;

    .line 152
    const-string/jumbo v0, "editable_images"

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/model/media/EditableMedia;->j:Lcom/twitter/util/serialization/l;

    .line 153
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 152
    invoke-static {p1, v0, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->r:Ljava/util/Map;

    .line 154
    const-string/jumbo v0, "current_bucket"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaStoreBucket;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    .line 162
    :goto_0
    const-string/jumbo v0, "show_expand"

    invoke-virtual {v1, v0, v4}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 163
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->d(Z)V

    .line 165
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/l;->a(Lcom/twitter/android/widget/l$a;)V

    .line 166
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/l;->a(Lcom/twitter/android/widget/l$c;)V

    .line 168
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 169
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 170
    iget-object v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {v3, v0}, Lcom/twitter/android/widget/l;->b(Lcom/twitter/model/media/EditableMedia;)V

    goto :goto_1

    .line 156
    :cond_1
    iput v6, p0, Lcom/twitter/android/widget/GalleryGridFragment;->o:I

    .line 157
    iput-boolean v5, p0, Lcom/twitter/android/widget/GalleryGridFragment;->p:Z

    .line 158
    iput-object v7, p0, Lcom/twitter/android/widget/GalleryGridFragment;->q:Lcom/twitter/model/media/EditableImage;

    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/model/MediaStoreBucket;->a(Landroid/content/res/Resources;)Lcom/twitter/media/model/MediaStoreBucket;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    goto :goto_0

    .line 172
    :cond_2
    iput-object v7, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Ljava/util/Map;

    .line 174
    :cond_3
    if-eqz v1, :cond_4

    .line 175
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/l;->a(Lcom/twitter/android/widget/l$b;)V

    .line 178
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->m:Z

    if-eqz v0, :cond_5

    .line 179
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/l;->a(Z)V

    .line 180
    iput-boolean v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->m:Z

    .line 183
    :cond_5
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->n:Z

    if-eqz v0, :cond_6

    .line 184
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/l;->b(Z)V

    .line 185
    iput-boolean v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->n:Z

    .line 188
    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 189
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->m()V

    .line 193
    :goto_2
    return-void

    .line 191
    :cond_7
    iput-boolean v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->t:Z

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 571
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onDestroy()V

    .line 572
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 573
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->l()V

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:Lcom/twitter/android/widget/GalleryGridFragment$b;

    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridFragment$b;->a()V

    .line 577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    .line 578
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 564
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onDestroyView()V

    .line 566
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    invoke-virtual {v0}, Lcom/twitter/android/widget/l;->b()V

    .line 567
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 251
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 252
    const-string/jumbo v0, "first_visible_position"

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 253
    const-string/jumbo v1, "disable_grid_reload"

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getChangingConfigurations()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 254
    const-string/jumbo v0, "expanded_image"

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->q:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 255
    const-string/jumbo v0, "current_bucket"

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->v:Lcom/twitter/media/model/MediaStoreBucket;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 256
    const-string/jumbo v0, "editable_images"

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->r:Ljava/util/Map;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/model/media/EditableMedia;->j:Lcom/twitter/util/serialization/l;

    .line 257
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 256
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 258
    return-void

    .line 253
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 473
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->k:Lcom/twitter/android/widget/ab;

    if-eqz v1, :cond_1

    .line 474
    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 475
    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->k:Lcom/twitter/android/widget/ab;

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v2, v0}, Lcom/twitter/android/widget/ab;->a(Z)V

    .line 477
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 466
    if-eq p2, v0, :cond_0

    if-nez p2, :cond_1

    .line 467
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:Lcom/twitter/android/widget/l;

    if-ne p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/l;->c(Z)V

    .line 469
    :cond_1
    return-void

    .line 467
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 553
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onStart()V

    .line 555
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->p:Z

    if-eqz v0, :cond_0

    .line 556
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->p:Z

    .line 560
    :goto_0
    return-void

    .line 558
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->h()V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 243
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 244
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Landroid/content/Context;)I

    move-result v0

    .line 245
    iget v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->e:I

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(II)V

    .line 246
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:[Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->b([Landroid/view/View;)V

    .line 247
    return-void
.end method
