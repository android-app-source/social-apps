.class public Lcom/twitter/android/widget/u;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/widget/av;

.field private final b:F

.field private final c:Landroid/view/ViewGroup;

.field private d:Landroid/view/View;

.field private e:F

.field private f:J

.field private g:F


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;F)V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/android/widget/av;

    invoke-direct {v0}, Lcom/twitter/android/widget/av;-><init>()V

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/android/widget/u;-><init>(Landroid/view/ViewGroup;Lcom/twitter/android/widget/av;F)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/twitter/android/widget/av;F)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/u;->e:F

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/widget/u;->f:J

    .line 38
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/twitter/android/widget/u;->g:F

    .line 51
    iput-object p1, p0, Lcom/twitter/android/widget/u;->c:Landroid/view/ViewGroup;

    .line 52
    iput-object p2, p0, Lcom/twitter/android/widget/u;->a:Lcom/twitter/android/widget/av;

    .line 53
    iput p3, p0, Lcom/twitter/android/widget/u;->b:F

    .line 54
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 61
    iget-object v0, p0, Lcom/twitter/android/widget/u;->a:Lcom/twitter/android/widget/av;

    invoke-virtual {v0}, Lcom/twitter/android/widget/av;->a()J

    move-result-wide v4

    .line 62
    iget-wide v0, p0, Lcom/twitter/android/widget/u;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    move-wide v0, v2

    .line 63
    :goto_0
    iput-wide v4, p0, Lcom/twitter/android/widget/u;->f:J

    .line 64
    invoke-virtual {p0}, Lcom/twitter/android/widget/u;->e()Landroid/view/View;

    move-result-object v4

    .line 65
    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    if-eqz v4, :cond_0

    .line 66
    iget-object v2, p0, Lcom/twitter/android/widget/u;->d:Landroid/view/View;

    if-ne v4, v2, :cond_2

    .line 67
    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v2

    iget v3, p0, Lcom/twitter/android/widget/u;->e:F

    sub-float/2addr v2, v3

    long-to-float v0, v0

    const v1, 0x3a83126f    # 0.001f

    mul-float/2addr v0, v1

    div-float v0, v2, v0

    iput v0, p0, Lcom/twitter/android/widget/u;->g:F

    .line 74
    :cond_0
    :goto_1
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v0

    :goto_2
    iput v0, p0, Lcom/twitter/android/widget/u;->e:F

    .line 75
    iput-object v4, p0, Lcom/twitter/android/widget/u;->d:Landroid/view/View;

    .line 76
    return-void

    .line 62
    :cond_1
    iget-wide v0, p0, Lcom/twitter/android/widget/u;->f:J

    sub-long v0, v4, v0

    goto :goto_0

    .line 71
    :cond_2
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/twitter/android/widget/u;->g:F

    goto :goto_1

    .line 74
    :cond_3
    iget v0, p0, Lcom/twitter/android/widget/u;->e:F

    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/widget/u;->f:J

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/u;->d:Landroid/view/View;

    .line 84
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/twitter/android/widget/u;->g:F

    .line 85
    return-void
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 92
    iget v0, p0, Lcom/twitter/android/widget/u;->g:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/u;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/android/widget/u;->b:F

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/widget/u;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public e()Landroid/view/View;
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/widget/u;->c:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
