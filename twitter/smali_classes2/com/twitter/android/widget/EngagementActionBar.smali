.class public Lcom/twitter/android/widget/EngagementActionBar;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/twitter/model/core/Tweet;

.field private d:Landroid/view/View$OnClickListener;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/widget/EngagementActionBar;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f130071
        0x7f130032
        0x7f13014a
        0x7f130149
        0x7f13006e
        0x7f13001f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/widget/EngagementActionBar;->a:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->b:Ljava/util/Map;

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/widget/EngagementActionBar;->a:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->b:Ljava/util/Map;

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/widget/EngagementActionBar;->a:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->b:Ljava/util/Map;

    .line 72
    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 121
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_1

    .line 167
    :cond_0
    return-void

    .line 125
    :cond_1
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v6

    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    move v1, v2

    .line 127
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->u:J

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-lez v0, :cond_3

    move v4, v2

    .line 129
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 130
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto :goto_2

    .line 144
    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->e:Landroid/view/View;

    check-cast v0, Lcom/twitter/android/widget/ToggleImageButton;

    .line 145
    if-nez v4, :cond_7

    .line 146
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/ToggleImageButton;->setEnabled(Z)V

    goto :goto_2

    :cond_2
    move v1, v3

    .line 126
    goto :goto_0

    :cond_3
    move v4, v3

    .line 127
    goto :goto_1

    .line 132
    :sswitch_1
    check-cast v0, Lcom/twitter/android/widget/ToggleImageButton;

    .line 133
    if-nez v4, :cond_4

    .line 135
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/ToggleImageButton;->setEnabled(Z)V

    goto :goto_2

    .line 137
    :cond_4
    iget-object v5, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v5, v5, Lcom/twitter/model/core/Tweet;->c:Z

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 138
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/ToggleImageButton;->setVisibility(I)V

    .line 139
    iget-object v5, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v5, v5, Lcom/twitter/model/core/Tweet;->F:Z

    if-eqz v5, :cond_5

    if-eqz v1, :cond_6

    :cond_5
    move v5, v2

    :goto_3
    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/ToggleImageButton;->setEnabled(Z)V

    goto :goto_2

    :cond_6
    move v5, v3

    goto :goto_3

    .line 148
    :cond_7
    iget-object v5, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v5, v5, Lcom/twitter/model/core/Tweet;->a:Z

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    goto :goto_2

    .line 153
    :sswitch_2
    iget-object v5, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v5, v5, Lcom/twitter/model/core/Tweet;->F:Z

    if-nez v5, :cond_8

    move v5, v2

    :goto_4
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    :cond_8
    move v5, v3

    goto :goto_4

    .line 157
    :sswitch_3
    iget-object v5, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-static {v5, v8, v9}, Lbxd;->b(Lcom/twitter/model/core/Tweet;J)Z

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    .line 161
    :sswitch_4
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    .line 130
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f130032 -> :sswitch_0
        0x7f13006e -> :sswitch_4
        0x7f130071 -> :sswitch_1
        0x7f130149 -> :sswitch_2
        0x7f13014a -> :sswitch_3
    .end sparse-switch
.end method

.method public b()V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 174
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    goto :goto_0

    .line 176
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 211
    return-void
.end method

.method public getFavoriteButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->e:Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    .line 76
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 78
    const v0, 0x7f13014a

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EngagementActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->f:Landroid/view/View;

    .line 79
    sget-object v1, Lcom/twitter/android/widget/EngagementActionBar;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 80
    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/EngagementActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 81
    if-eqz v4, :cond_0

    .line 82
    iget-object v5, p0, Lcom/twitter/android/widget/EngagementActionBar;->b:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const v5, 0x7f130032

    if-ne v3, v5, :cond_0

    .line 85
    iput-object v4, p0, Lcom/twitter/android/widget/EngagementActionBar;->e:Landroid/view/View;

    .line 79
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_1
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/widget/EngagementActionBar;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 112
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 114
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/widget/EngagementActionBar;->d:Landroid/view/View$OnClickListener;

    .line 115
    return-void
.end method

.method public setTweet(Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/twitter/android/widget/EngagementActionBar;->c:Lcom/twitter/model/core/Tweet;

    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/widget/EngagementActionBar;->a()V

    .line 102
    return-void
.end method
