.class public Lcom/twitter/android/widget/GalleryGridToolbar;
.super Lcom/twitter/internal/android/widget/ToolBar;
.source "Twttr"

# interfaces
.implements Lcmr$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/GalleryGridToolbar$a;
    }
.end annotation


# instance fields
.field private a:Lazv;

.field private b:Lcom/twitter/android/widget/GalleryGridToolbar$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 21
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/widget/GalleryGridToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/GalleryGridToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/internal/android/widget/ToolBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->b()V

    .line 31
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->a()V

    .line 35
    new-instance v0, Lazu;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lazu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridToolbar;->a(Lazu;)V

    .line 36
    invoke-virtual {p0, p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->setOnToolBarItemSelectedListener(Lcmr$a;)V

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridToolbar;->setSelectedCount(I)V

    .line 38
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a03c4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridToolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 42
    return-void
.end method

.method protected a(Lazu;)V
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f140010

    invoke-virtual {p1, v0, p0}, Lazu;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    .line 46
    const v0, 0x7f13089a

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridToolbar;->a(I)Lazv;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->a:Lazv;

    .line 47
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 64
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 77
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 66
    :sswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->d()V

    goto :goto_0

    .line 70
    :sswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->c()V

    goto :goto_0

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x7f130043 -> :sswitch_1
        0x7f13089a -> :sswitch_0
    .end sparse-switch
.end method

.method public a_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->b:Lcom/twitter/android/widget/GalleryGridToolbar$a;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->b:Lcom/twitter/android/widget/GalleryGridToolbar$a;

    invoke-interface {v0}, Lcom/twitter/android/widget/GalleryGridToolbar$a;->d()V

    .line 89
    :cond_0
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->b:Lcom/twitter/android/widget/GalleryGridToolbar$a;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->b:Lcom/twitter/android/widget/GalleryGridToolbar$a;

    invoke-interface {v0}, Lcom/twitter/android/widget/GalleryGridToolbar$a;->e()V

    .line 95
    :cond_0
    return-void
.end method

.method public setListener(Lcom/twitter/android/widget/GalleryGridToolbar$a;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->b:Lcom/twitter/android/widget/GalleryGridToolbar$a;

    .line 60
    return-void
.end method

.method public setSelectedCount(I)V
    .locals 6

    .prologue
    .line 50
    if-nez p1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->a:Lazv;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a03c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lazv;->a(Ljava/lang/CharSequence;)Lazv;

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridToolbar;->a:Lazv;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 54
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 53
    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lazv;->a(Ljava/lang/CharSequence;)Lazv;

    goto :goto_0
.end method
