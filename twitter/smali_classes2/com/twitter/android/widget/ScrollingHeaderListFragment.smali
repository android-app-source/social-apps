.class public abstract Lcom/twitter/android/widget/ScrollingHeaderListFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"

# interfaces
.implements Lcno$c;
.implements Lcom/twitter/android/ScrollingHeaderActivity$b;
.implements Lcom/twitter/library/widget/ObservableScrollView$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A:",
        "Lcjr",
        "<TT;>;>",
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<TT;TA;>;",
        "Lcno$c;",
        "Lcom/twitter/android/ScrollingHeaderActivity$b;",
        "Lcom/twitter/library/widget/ObservableScrollView$a;"
    }
.end annotation


# instance fields
.field protected L:I

.field protected M:I

.field protected N:I

.field protected O:I

.field protected P:I

.field protected Q:Z

.field private final a:Z

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Lcom/twitter/library/widget/ObservableScrollView;

.field private g:I

.field private h:Z

.field private i:Z

.field private final j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    .line 62
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->ap_()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    .line 72
    new-instance v0, Lcom/twitter/android/widget/ScrollingHeaderListFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment$1;-><init>(Lcom/twitter/android/widget/ScrollingHeaderListFragment;)V

    iput-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->k:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/ScrollingHeaderListFragment;)Lcom/twitter/library/widget/ObservableScrollView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    return-object v0
.end method

.method private c(I)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 227
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-nez v0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    iget v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    if-eq v0, p1, :cond_4

    .line 232
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v2, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 238
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->d:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    :cond_3
    iput p1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    .line 244
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    .line 245
    :goto_1
    if-eqz v0, :cond_0

    .line 246
    iget v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->P:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 244
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected L_()I
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0367

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected R()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method protected S()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 169
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->h:Z

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ObservableScrollView;->getHeight()I

    move-result v0

    .line 173
    iget v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    sub-int/2addr v1, v2

    .line 178
    iget-object v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v2, v4}, Lcom/twitter/library/widget/ObservableScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 182
    iget-object v3, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 183
    add-int/2addr v0, v3

    add-int/2addr v0, v1

    sub-int/2addr v0, v2

    .line 184
    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 185
    if-eq v3, v0, :cond_0

    .line 186
    iget-object v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 187
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    new-instance v1, Lcom/twitter/android/widget/ScrollingHeaderListFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment$2;-><init>(Lcom/twitter/android/widget/ScrollingHeaderListFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ObservableScrollView;->post(Ljava/lang/Runnable;)Z

    .line 197
    return-void
.end method

.method protected T()V
    .locals 2

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ObservableScrollView;->getHeight()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ObservableScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_2

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ObservableScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 213
    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->i:Z

    .line 214
    return-void

    .line 210
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->S()V

    goto :goto_0
.end method

.method protected U()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, -0x1

    const/4 v1, 0x1

    .line 328
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->ay()Z

    move-result v2

    if-nez v2, :cond_0

    .line 349
    :goto_0
    return v0

    .line 331
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 332
    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v3

    .line 333
    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    .line 334
    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v5

    .line 335
    invoke-virtual {v2}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v6

    .line 337
    if-lez v6, :cond_1

    if-le v3, v1, :cond_1

    sub-int v5, v6, v5

    add-int/lit8 v5, v5, 0x1

    if-lt v5, v4, :cond_1

    .line 339
    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 341
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/lit8 v2, v3, -0x2

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 342
    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->g:I

    sub-int v0, v2, v0

    .line 343
    iget-object v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->c:Landroid/view/View;

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v3, v7, v0, v7}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v1

    .line 348
    :cond_1
    iput-boolean v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->Q:Z

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-eqz v0, :cond_1

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    .line 133
    iget-object v0, v2, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    .line 134
    if-eqz v0, :cond_0

    .line 136
    instance-of v3, v0, Lcom/twitter/library/widget/ObservableScrollView;

    if-eqz v3, :cond_2

    check-cast v0, Lcom/twitter/library/widget/ObservableScrollView;

    .line 137
    :goto_0
    iput-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    .line 138
    const v0, 0x7f130555

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->d:Landroid/view/View;

    .line 141
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->d:Landroid/view/View;

    .line 142
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 143
    iget v3, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 145
    const v0, 0x7f130556

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->e:Landroid/view/View;

    .line 146
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ObservableScrollView;->setObservableScrollViewListener(Lcom/twitter/library/widget/ObservableScrollView$a;)V

    .line 147
    iget-object v0, v2, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->O:I

    .line 150
    :cond_0
    invoke-virtual {v2, p0}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 152
    :cond_1
    return-object v1

    .line 136
    :cond_2
    const v0, 0x7f130554

    .line 137
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ObservableScrollView;

    goto :goto_0
.end method

.method public a(II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 273
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-nez v0, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iput p2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->P:I

    .line 278
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->c(I)V

    .line 280
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    .line 285
    iget-object v3, v2, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 286
    invoke-virtual {v3}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    invoke-virtual {v2}, Lcom/twitter/app/common/list/l;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 291
    neg-int v0, p2

    .line 292
    iget-boolean v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->i:Z

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    iget-object v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/ObservableScrollView;->getScrollX()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/widget/ObservableScrollView;->scrollTo(II)V

    goto :goto_0

    .line 298
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->Q:Z

    if-nez v0, :cond_3

    .line 299
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->U()Z

    move-result v0

    .line 300
    if-eqz v0, :cond_3

    .line 301
    add-int v0, p2, p1

    invoke-virtual {v2, v1, v0}, Lcom/twitter/app/common/list/l;->a(II)V

    goto :goto_0

    .line 306
    :cond_3
    add-int v0, p2, p1

    iget v4, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    if-le v0, v4, :cond_4

    .line 308
    add-int v0, p2, p1

    invoke-virtual {v2, v1, v0}, Lcom/twitter/app/common/list/l;->a(II)V

    goto :goto_0

    .line 313
    :cond_4
    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 314
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 315
    if-lt v0, v1, :cond_5

    if-eqz v3, :cond_5

    .line 317
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v1

    .line 323
    :goto_1
    invoke-virtual {v2, v0, v1}, Lcom/twitter/app/common/list/l;->a(II)V

    goto :goto_0

    .line 320
    :cond_5
    iget v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_1
.end method

.method public a(Lcno;)V
    .locals 0

    .prologue
    .line 354
    return-void
.end method

.method public a(Lcno;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x1

    .line 366
    iget v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->k:I

    if-eq v0, v3, :cond_1

    if-nez p2, :cond_1

    .line 368
    invoke-interface {p1}, Lcno;->c()Lcnm;

    move-result-object v0

    iget v0, v0, Lcnm;->c:I

    if-le v0, v1, :cond_2

    move v0, v1

    .line 369
    :goto_0
    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 372
    :cond_0
    iput v3, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->k:I

    .line 374
    :cond_1
    return-void

    .line 368
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcno;IIIZ)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 379
    const/4 v1, 0x1

    if-le p2, v1, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    iget-boolean v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-eqz v1, :cond_0

    if-lez p3, :cond_0

    iget v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    if-lez v1, :cond_0

    .line 386
    if-nez p2, :cond_4

    .line 387
    invoke-interface {p1}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 388
    if-eqz v1, :cond_2

    .line 389
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 390
    neg-int v2, v1

    if-nez v1, :cond_3

    :goto_1
    sub-int v0, v2, v0

    .line 398
    :cond_2
    :goto_2
    iget v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    sub-int/2addr v1, v2

    .line 399
    neg-int v0, v0

    neg-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 400
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 401
    instance-of v2, v0, Lcom/twitter/android/ScrollingHeaderActivity;

    if-eqz v2, :cond_0

    .line 402
    iget-boolean v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->Q:Z

    if-eqz v2, :cond_5

    .line 403
    iput v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->P:I

    .line 404
    check-cast v0, Lcom/twitter/android/ScrollingHeaderActivity;

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->N:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ScrollingHeaderActivity;->b(II)V

    goto :goto_0

    .line 390
    :cond_3
    iget v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->O:I

    goto :goto_1

    .line 395
    :cond_4
    iget v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    goto :goto_2

    .line 406
    :cond_5
    invoke-interface {p1}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/widget/ScrollingHeaderListFragment$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment$3;-><init>(Lcom/twitter/android/widget/ScrollingHeaderListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 3

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 113
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->R()Z

    move-result v1

    .line 115
    if-eqz v1, :cond_1

    const v0, 0x7f040398

    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v2

    .line 116
    invoke-virtual {p1}, Lcom/twitter/app/common/list/l$d;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f040221

    :goto_1
    invoke-virtual {v2, v0}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 118
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    if-eqz v1, :cond_3

    const v0, 0x7f0403df

    :goto_2
    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 121
    if-nez v1, :cond_0

    .line 122
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 126
    :cond_0
    return-void

    .line 115
    :cond_1
    const v0, 0x7f040396

    goto :goto_0

    .line 116
    :cond_2
    const v0, 0x7f040397

    goto :goto_1

    .line 119
    :cond_3
    const v0, 0x7f0403e0

    goto :goto_2
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 0

    .prologue
    .line 201
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 202
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->T()V

    .line 203
    return-void
.end method

.method public a(Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 0

    .prologue
    .line 433
    return-void
.end method

.method public a(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
    .locals 3

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    iget v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    iget v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    sub-int/2addr v0, v1

    .line 422
    neg-int v1, p3

    neg-int v0, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 423
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 424
    instance-of v2, v0, Lcom/twitter/android/ScrollingHeaderActivity;

    if-eqz v2, :cond_0

    .line 425
    iput v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->P:I

    .line 426
    check-cast v0, Lcom/twitter/android/ScrollingHeaderActivity;

    iget v2, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->N:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ScrollingHeaderActivity;->b(II)V

    .line 429
    :cond_0
    return-void
.end method

.method protected ap_()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public aq_()Landroid/view/View;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f:Lcom/twitter/library/widget/ObservableScrollView;

    return-object v0
.end method

.method public b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 252
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    .line 256
    iget-object v1, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 257
    if-lt v1, v3, :cond_0

    .line 258
    const/16 v2, 0xf

    if-le v1, v2, :cond_2

    .line 259
    invoke-virtual {v0, v3, p1}, Lcom/twitter/app/common/list/l;->a(II)V

    goto :goto_0

    .line 261
    :cond_2
    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3, p1}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 266
    iput p1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->k:I

    goto :goto_0
.end method

.method public b(Lcno;)V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method public b(Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 0

    .prologue
    .line 437
    return-void
.end method

.method public b(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
    .locals 0

    .prologue
    .line 441
    return-void
.end method

.method public c(Lcno;)V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 103
    iput p1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->N:I

    .line 104
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 84
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 85
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 87
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L_()I

    move-result v3

    iput v3, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    .line 90
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->g:I

    .line 91
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b:Landroid/view/View;

    .line 92
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->c:Landroid/view/View;

    .line 93
    iget-object v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b:Landroid/view/View;

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    iget v4, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->L:I

    iget v5, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->M:I

    .line 94
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-direct {v3, v6, v4, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    .line 93
    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    const-string/jumbo v1, "fragment_page_number"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->N:I

    .line 98
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->h:Z

    .line 100
    :cond_0
    return-void

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 218
    iget-boolean v0, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a:Z

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 220
    iget-object v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b:Landroid/view/View;

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 221
    iget-object v1, p0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 223
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/list/TwitterListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 224
    return-void
.end method
