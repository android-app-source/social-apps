.class Lcom/twitter/android/widget/ao$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/widget/ao;->a(Landroid/view/View;)Landroid/view/GestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/widget/ao;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/ao;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/twitter/android/widget/ao$1;->b:Lcom/twitter/android/widget/ao;

    iput-object p2, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performLongClick()Z

    .line 56
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->b:Lcom/twitter/android/widget/ao;

    invoke-static {v0}, Lcom/twitter/android/widget/ao;->a(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/ao$1;->b:Lcom/twitter/android/widget/ao;

    invoke-static {v1}, Lcom/twitter/android/widget/ao;->a(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->b:Lcom/twitter/android/widget/ao;

    invoke-static {v0}, Lcom/twitter/android/widget/ao;->b(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/ao$1;->b:Lcom/twitter/android/widget/ao;

    invoke-static {v1}, Lcom/twitter/android/widget/ao;->b(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    const/4 v0, 0x0

    .line 47
    :goto_0
    return v0

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 44
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->b:Lcom/twitter/android/widget/ao;

    invoke-static {v0}, Lcom/twitter/android/widget/ao;->a(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/twitter/android/widget/ao$1;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/ao$1;->b:Lcom/twitter/android/widget/ao;

    invoke-static {v1}, Lcom/twitter/android/widget/ao;->a(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 47
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
