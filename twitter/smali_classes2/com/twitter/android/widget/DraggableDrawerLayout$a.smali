.class Lcom/twitter/android/widget/DraggableDrawerLayout$a;
.super Landroid/view/animation/Animation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/widget/DraggableDrawerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public final a:F

.field public final b:F


# direct methods
.method constructor <init>(FFF)V
    .locals 2

    .prologue
    .line 685
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 686
    invoke-static {p2, p1}, Ljava/lang/Math;->copySign(FF)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->a:F

    .line 687
    invoke-static {p3, p1}, Ljava/lang/Math;->copySign(FF)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->b:F

    .line 688
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->a:F

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->b:F

    invoke-static {p1, v0, v1}, Lcom/twitter/util/math/b;->c(FFF)F

    move-result v0

    float-to-int v0, v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->setDuration(J)V

    .line 690
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 694
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->getDuration()J

    move-result-wide v0

    long-to-float v0, v0

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->a:F

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout$a;->b:F

    invoke-static {v0, v1, v2}, Lcom/twitter/util/math/b;->b(FFF)F

    move-result v0

    .line 696
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 697
    return-void
.end method
