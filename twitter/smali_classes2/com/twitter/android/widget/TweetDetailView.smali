.class public Lcom/twitter/android/widget/TweetDetailView;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Lbxf;
.implements Lcom/twitter/library/media/widget/TweetMediaView$b;
.implements Lcom/twitter/library/revenue/a$a;
.implements Lcom/twitter/library/widget/b;
.implements Lcom/twitter/media/ui/image/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/TweetDetailView$c;,
        Lcom/twitter/android/widget/TweetDetailView$b;,
        Lcom/twitter/android/widget/TweetDetailView$a;,
        Lcom/twitter/android/widget/TweetDetailView$d;
    }
.end annotation


# instance fields
.field private A:Lcom/twitter/library/widget/renderablecontent/d;

.field private B:Ljava/lang/CharSequence;

.field private C:Ljava/lang/CharSequence;

.field private D:Landroid/view/View;

.field private E:Lcom/twitter/ui/widget/TypefacesTextView;

.field private F:Lcom/twitter/ui/widget/TypefacesTextView;

.field private G:Lcom/twitter/model/core/Translation;

.field private H:Lcom/twitter/library/view/QuoteView;

.field private I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

.field private J:Landroid/view/View;

.field private K:Landroid/widget/ImageView;

.field private L:Landroid/view/ViewGroup;

.field private M:Z

.field private N:Lcom/twitter/android/widget/aw;

.field private O:Lcom/twitter/ui/widget/ActionButton;

.field private P:Lcom/twitter/model/core/TwitterUser;

.field private Q:Lcom/twitter/library/revenue/a;

.field private R:Lbxq;

.field private S:Lcom/twitter/model/moments/Moment;

.field private T:Lwn;

.field public a:Lcom/twitter/media/ui/image/UserImageView;

.field public b:Lcom/twitter/ui/widget/BadgeView;

.field public c:Lcom/twitter/android/widget/EngagementActionBar;

.field private d:Lcom/twitter/library/api/ActivitySummary;

.field private e:Lcom/twitter/android/widget/TweetDetailView$b;

.field private f:Lcom/twitter/model/core/Tweet;

.field private g:Lcne;

.field private h:Lcom/twitter/android/cc;

.field private i:Ljava/text/SimpleDateFormat;

.field private final j:Lcom/twitter/media/ui/image/b;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/RelativeLayout;

.field private n:Landroid/widget/TextView;

.field private o:Lcom/twitter/ui/socialproof/SocialBylineView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/ImageView;

.field private t:Landroid/view/View;

.field private u:Landroid/view/ViewGroup;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/ViewGroup;

.field private y:Landroid/view/ViewGroup;

.field private z:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TweetDetailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/TweetDetailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 196
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 143
    new-instance v0, Lcom/twitter/media/ui/image/b;

    invoke-direct {v0}, Lcom/twitter/media/ui/image/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Lcom/twitter/media/ui/image/b;

    .line 197
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/TweetDetailView;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method private a(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->i:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 1053
    new-instance v0, Ljava/text/SimpleDateFormat;

    .line 1054
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0b74

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->i:Ljava/text/SimpleDateFormat;

    .line 1056
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->i:Ljava/text/SimpleDateFormat;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;III)V
    .locals 7

    .prologue
    .line 675
    const/16 v0, 0xe

    if-eq p1, v0, :cond_0

    const/16 v0, 0xf

    if-eq p1, v0, :cond_0

    const/16 v0, 0x11

    if-eq p1, v0, :cond_0

    const/16 v0, 0x13

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-eq p1, v0, :cond_0

    .line 680
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    .line 681
    invoke-static/range {v0 .. v6}, Lcom/twitter/ui/socialproof/a;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v1

    .line 683
    if-nez v1, :cond_1

    .line 684
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    .line 697
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    .line 688
    invoke-static/range {v0 .. v6}, Lcom/twitter/ui/socialproof/a;->b(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    .line 690
    if-eqz v0, :cond_2

    .line 691
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 693
    :cond_2
    invoke-static {p1}, Lcom/twitter/ui/socialproof/a;->a(I)I

    move-result v0

    .line 694
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIcon(I)V

    .line 695
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 847
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 848
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 851
    const/4 v2, 0x3

    const v3, 0x7f1303f2

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 852
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 853
    const v1, 0x7f13008a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    .line 855
    const v1, 0x7f13048a

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 856
    const v1, 0x7f13048c

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 859
    invoke-virtual {v0, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 860
    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    .line 861
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->b(Landroid/view/View;)V

    .line 862
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 733
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    .line 734
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    const v1, 0x7f130083

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 736
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 737
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 738
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 740
    invoke-static {v0, v1}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    .line 741
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbxs;->a(Landroid/view/View;Lcax;Ljava/util/List;)V

    .line 746
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->b(Landroid/view/View;)V

    .line 747
    return-void
.end method

.method private a(Landroid/widget/TextView;Z)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v0, 0x3

    .line 1061
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_1

    .line 1062
    if-eqz p2, :cond_0

    const/4 v0, 0x4

    :cond_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 1066
    :goto_0
    return-void

    .line 1064
    :cond_1
    if-eqz p2, :cond_2

    const/4 v0, 0x5

    :cond_2
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method private a(Lcgi;)V
    .locals 2

    .prologue
    .line 662
    if-eqz p1, :cond_0

    .line 663
    iget-object v0, p1, Lcgi;->f:Ljava/lang/String;

    .line 664
    if-eqz v0, :cond_1

    .line 665
    invoke-virtual {p1}, Lcgi;->f()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->b(ILjava/lang/String;)V

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 667
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/api/ActivitySummary;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 722
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/aw;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    .line 723
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/aw;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/twitter/android/widget/aw;->a(Landroid/content/res/Resources;Lcom/twitter/library/api/ActivitySummary;)V

    .line 724
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/aw;

    iget-object v1, v1, Lcom/twitter/android/widget/aw;->a:Landroid/view/View;

    .line 725
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_0

    .line 726
    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, v1, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 727
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v2}, Lbxd;->j(Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 730
    :cond_0
    return-void

    .line 727
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/Tweet;J)V
    .locals 2

    .prologue
    .line 706
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->b:J

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    .line 707
    const/16 v0, 0xd

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->b(ILjava/lang/String;)V

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 708
    :cond_1
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v0, :cond_0

    .line 709
    const/16 v0, 0x2c

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 946
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 700
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    .line 701
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v0, p2}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIcon(I)V

    .line 702
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    .line 703
    return-void
.end method

.method static a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;J)V
    .locals 6

    .prologue
    .line 968
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 969
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 970
    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 971
    new-instance v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;-><init>()V

    .line 972
    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    iget-object v2, v2, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->a:Ljava/lang/String;

    .line 973
    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    iget-object v2, v2, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    invoke-virtual {v2}, Lcom/twitter/model/geo/TwitterPlace$PlaceType;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->b:Ljava/lang/String;

    .line 974
    iget-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iget-object v2, v2, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 975
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "tweet::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":place_tag:click"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 976
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 977
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 975
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 979
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 618
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 619
    invoke-static {p1, p2}, Lbxt;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 621
    const v2, 0x7f0a096c

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v1, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 622
    const-string/jumbo v3, "$b"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 623
    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0202a1

    invoke-direct {v4, v5, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;I)V

    .line 624
    invoke-virtual {v4}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 625
    invoke-virtual {v4}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 626
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f110016

    invoke-static {v6, v7}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 629
    :cond_0
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 630
    add-int/lit8 v2, v3, 0x2

    invoke-virtual {v5, v4, v3, v2, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 631
    new-instance v2, Lcom/twitter/android/widget/TweetDetailView$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/TweetDetailView$3;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    .line 655
    add-int/lit8 v4, v3, 0x2

    invoke-virtual {v5, v2, v3, v4, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 657
    iput-object v5, p0, Lcom/twitter/android/widget/TweetDetailView;->C:Ljava/lang/CharSequence;

    .line 658
    const v2, 0x7f0a096e

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v1, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Ljava/lang/CharSequence;

    .line 659
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 513
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbxd;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/widget/TweetDetailView;)Lcne;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->g:Lcne;

    return-object v0
.end method

.method private b(ILjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 714
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/widget/TweetDetailView;->a(ILjava/lang/String;Ljava/lang/String;III)V

    .line 715
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1044
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->setVisibility(I)V

    .line 1045
    if-eqz p1, :cond_0

    .line 1046
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1047
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1049
    :cond_0
    return-void
.end method

.method private b(Lcom/twitter/model/core/Tweet;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 918
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    .line 919
    if-nez v0, :cond_0

    .line 942
    :goto_0
    return-void

    .line 922
    :cond_0
    iget-object v1, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->f:Ljava/lang/String;

    .line 923
    :goto_1
    iget-boolean v1, p1, Lcom/twitter/model/core/Tweet;->T:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 924
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 925
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 922
    :cond_2
    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    goto :goto_1

    .line 928
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0982

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 929
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->o()Z

    move-result v2

    if-nez v2, :cond_4

    .line 930
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 931
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 934
    :cond_4
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 935
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 937
    new-instance v0, Lcom/twitter/android/widget/TweetDetailView$d;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->getOwnerId()J

    move-result-wide v4

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/TweetDetailView$d;-><init>(Lcom/twitter/android/widget/TweetDetailView;Landroid/content/Context;Lcom/twitter/model/core/Tweet;J)V

    .line 938
    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v1

    const/16 v2, 0x11

    .line 937
    invoke-virtual {v7, v0, v6, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 939
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 940
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 941
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/widget/TweetDetailView;)Lcom/twitter/model/moments/Moment;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/model/moments/Moment;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/TweetDetailView;)Lwn;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->T:Lwn;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/widget/TweetDetailView;)J
    .locals 2

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->getOwnerId()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic f(Lcom/twitter/android/widget/TweetDetailView;)Lcom/twitter/library/api/ActivitySummary;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->d:Lcom/twitter/library/api/ActivitySummary;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/widget/TweetDetailView;)Lcom/twitter/android/widget/TweetDetailView$b;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Lcom/twitter/android/widget/TweetDetailView$b;

    return-object v0
.end method

.method private getContentContainer()Lcom/twitter/library/widget/renderablecontent/c;
    .locals 1

    .prologue
    .line 766
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->e()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->e()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    .line 769
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/c;->B:Lcom/twitter/library/widget/renderablecontent/c;

    goto :goto_0
.end method

.method private getOwnerId()J
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lcom/twitter/android/widget/TweetDetailView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/widget/TweetDetailView;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 517
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/model/core/TwitterUser;

    .line 518
    invoke-static {v0}, Lbad;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 523
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 526
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    .line 527
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->M()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 528
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 529
    sget-object v2, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 530
    invoke-static {v0, v2}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    .line 531
    invoke-static {v0}, Lcom/twitter/model/util/d;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 533
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f020837

    invoke-static {v2, v0, v3}, Lbrw;->b(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 534
    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 536
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    .line 537
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 538
    const v2, 0x7f0e0547

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 540
    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 541
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestLayout()V

    .line 542
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 546
    :goto_0
    return-void

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 718
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;)V

    .line 719
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 773
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    .line 775
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    .line 776
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bh_()V

    .line 777
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    .line 778
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->d()V

    .line 780
    :cond_0
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bg_()V

    .line 785
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->g()V

    .line 786
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->c()V

    .line 788
    :cond_0
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 885
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/aw;

    if-nez v0, :cond_0

    .line 886
    new-instance v0, Lcom/twitter/android/widget/aw;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040416

    const/4 v3, 0x0

    .line 887
    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/TweetDetailView$a;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/TweetDetailView$a;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/aw;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/aw;

    .line 889
    :cond_0
    return-void
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 914
    const-string/jumbo v0, "poi_place_pivot_tweet_detail"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private p()V
    .locals 5

    .prologue
    .line 982
    iget-boolean v0, p0, Lcom/twitter/android/widget/TweetDetailView;->M:Z

    if-eqz v0, :cond_1

    .line 983
    const/4 v0, 0x0

    .line 984
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->L:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 985
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->L:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v0

    .line 987
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 988
    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {v2}, Lcom/twitter/util/ui/k;->e(Landroid/view/View;)I

    move-result v2

    div-int/lit8 v3, v1, 0x2

    sub-int/2addr v2, v3

    .line 989
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {v3, p0}, Lcom/twitter/util/ui/k;->b(Landroid/view/View;Landroid/view/View;)I

    move-result v3

    .line 990
    iget-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Landroid/view/View;

    add-int/2addr v1, v2

    invoke-virtual {v4, v2, v0, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 992
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->O:Lcom/twitter/ui/widget/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 509
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 510
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/ui/widget/BadgeView;

    if-eqz v0, :cond_0

    .line 958
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/ui/widget/BadgeView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/ui/widget/BadgeView;->a(ILjava/lang/String;)V

    .line 960
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    if-eqz v0, :cond_0

    .line 598
    const-string/jumbo v0, "translated_tweet"

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 599
    const-string/jumbo v1, "show_translation"

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 600
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TypefacesTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 599
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 602
    :cond_0
    return-void

    .line 600
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    .line 832
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 833
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401a2

    const/4 v3, 0x0

    .line 834
    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 836
    invoke-static {v2}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    invoke-virtual {v1}, Lbaa;->f()Ljava/lang/String;

    move-result-object v3

    .line 837
    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 838
    const v1, 0x7f130489

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 839
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 840
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 842
    :cond_0
    invoke-direct {p0, v2, p1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/ViewGroup;)V

    .line 843
    return-void
.end method

.method public a(Lcom/twitter/library/api/ActivitySummary;Lcom/twitter/android/widget/TweetDetailView$b;)V
    .locals 0

    .prologue
    .line 899
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->d:Lcom/twitter/library/api/ActivitySummary;

    .line 900
    iput-object p2, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Lcom/twitter/android/widget/TweetDetailView$b;

    .line 901
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->n()V

    .line 902
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;)V

    .line 903
    return-void
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcax;)V
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->g:Lcne;

    invoke-interface {v0, p2}, Lcne;->a(Lcax;)V

    .line 819
    return-void
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcom/twitter/model/core/MediaEntity;)V
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->g:Lcne;

    invoke-interface {v0, p2}, Lcne;->a(Lcom/twitter/model/core/MediaEntity;)V

    .line 824
    return-void
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcom/twitter/model/media/EditableMedia;)V
    .locals 0

    .prologue
    .line 829
    return-void
.end method

.method public a(Lcom/twitter/library/widget/PossiblySensitiveWarningView$a;ZZ)V
    .locals 2

    .prologue
    .line 866
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    .line 867
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->setVisibility(I)V

    .line 868
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->setListener(Lcom/twitter/library/widget/PossiblySensitiveWarningView$a;)V

    .line 869
    if-eqz p2, :cond_0

    .line 870
    if-eqz p3, :cond_1

    .line 871
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->g()V

    .line 876
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->requestLayout()V

    .line 877
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->d()V

    .line 878
    return-void

    .line 873
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->h()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/av/h;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 489
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 490
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->w:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/twitter/model/av/h;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0a0515

    .line 491
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 490
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_1

    .line 496
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setScaleY(F)V

    .line 497
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setPivotY(F)V

    .line 498
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 499
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 500
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 504
    :goto_1
    return-void

    .line 491
    :cond_0
    const v2, 0x7f0a0516

    .line 492
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcne;Laji;Lcom/twitter/android/cc;Lbxm$a;ZZ)V
    .locals 10

    .prologue
    .line 327
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    .line 328
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lbxd;->j(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 329
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Lcom/twitter/android/widget/EngagementActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->setVisibility(I)V

    .line 335
    :goto_0
    iput-object p2, p0, Lcom/twitter/android/widget/TweetDetailView;->g:Lcne;

    .line 336
    iput-object p4, p0, Lcom/twitter/android/widget/TweetDetailView;->h:Lcom/twitter/android/cc;

    .line 337
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 338
    if-eqz p3, :cond_7

    invoke-virtual {p3}, Laji;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    move v2, v0

    .line 339
    :goto_1
    if-eqz v2, :cond_8

    iget-boolean v0, p3, Laji;->d:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    .line 342
    :goto_2
    invoke-static {p1}, Lcom/twitter/model/util/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 343
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 344
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->an()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/util/a;->b(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 345
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/util/a;->d(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 346
    invoke-static {p1}, Lbwr;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/util/a;->f(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 347
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/util/a;->c(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 348
    invoke-virtual {v1}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v3

    .line 350
    const/4 v1, 0x0

    .line 351
    iget-object v4, v3, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 352
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 366
    :cond_0
    :goto_3
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 367
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->o()Z

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/widget/TextView;Z)V

    .line 368
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 369
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    :goto_4
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x40

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0x8

    :goto_5
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->O:Lcom/twitter/ui/widget/ActionButton;

    iget-object v3, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/twitter/ui/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    .line 377
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->Q:Lcom/twitter/library/revenue/a;

    invoke-virtual {v1, p1, v7}, Lcom/twitter/library/revenue/a;->a(Lcom/twitter/model/core/Tweet;Landroid/content/res/Resources;)V

    .line 380
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->R:Lbxq;

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->getOwnerId()J

    move-result-wide v4

    invoke-virtual {v1, p1, v4, v5, p5}, Lbxq;->a(Lcom/twitter/model/core/Tweet;JLbxm$a;)V

    .line 382
    if-eqz p7, :cond_c

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x1

    :goto_6
    iput-boolean v1, p0, Lcom/twitter/android/widget/TweetDetailView;->M:Z

    .line 383
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Landroid/view/View;

    iget-boolean v1, p0, Lcom/twitter/android/widget/TweetDetailView;->M:Z

    if-eqz v1, :cond_d

    const/4 v1, 0x0

    :goto_7
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 385
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->h:Lcom/twitter/android/cc;

    invoke-virtual {v1, v3}, Lcom/twitter/ui/socialproof/SocialBylineView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    iget-boolean v1, p1, Lcom/twitter/model/core/Tweet;->c:Z

    .line 388
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->t()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 389
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->getOwnerId()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/Tweet;J)V

    .line 420
    :cond_1
    :goto_8
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 421
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    :goto_9
    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Z)V

    .line 423
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->L:Z

    if-eqz v0, :cond_19

    .line 424
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    const v1, 0x7f020699

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 425
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    const v1, 0x7f110018

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 426
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 435
    :goto_a
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->q:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 437
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->b(Lcom/twitter/model/core/Tweet;)V

    .line 438
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;)V

    .line 439
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->k()V

    .line 442
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    if-nez v0, :cond_1b

    .line 443
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, v1}, Lbxt;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 444
    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 446
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 445
    invoke-static {v0}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 447
    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->R:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->D:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 457
    :cond_2
    :goto_b
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    .line 458
    invoke-virtual {v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 459
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-static {v1, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 461
    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    iget-object v5, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 462
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 463
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p1, Lcom/twitter/model/core/Tweet;->q:J

    move-object v1, p0

    .line 461
    invoke-static/range {v1 .. v9}, Lbxs;->a(Landroid/view/View;Lcax;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 464
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->j()V

    .line 468
    invoke-static {p1}, Lbwr;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 469
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 471
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    .line 472
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v4

    .line 471
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 474
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 477
    :cond_5
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 478
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setQuoteData(Lcom/twitter/model/core/r;)V

    .line 479
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setVisibility(I)V

    .line 484
    :goto_c
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->i()V

    .line 485
    return-void

    .line 332
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Lcom/twitter/android/widget/EngagementActionBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 338
    :cond_7
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_1

    .line 339
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 354
    :cond_9
    iget-object v1, v3, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    invoke-static {v1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v1

    iget-object v4, v3, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    .line 355
    invoke-virtual {v1, v4}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v1

    .line 356
    invoke-virtual {v1, p2}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v1

    const v4, 0x7f1100c9

    .line 357
    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcnf;->b(I)Lcnf;

    move-result-object v1

    .line 358
    invoke-virtual {v1}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 360
    invoke-static {}, Lcom/twitter/library/view/b;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->n()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 361
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v3, v3, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    iget-object v3, v3, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    const/4 v5, 0x1

    invoke-static {v4, v3, v1, v5}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    goto/16 :goto_3

    .line 371
    :cond_a
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 375
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 382
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 383
    :cond_d
    const/16 v1, 0x8

    goto/16 :goto_7

    .line 390
    :cond_e
    iget-boolean v3, p1, Lcom/twitter/model/core/Tweet;->K:Z

    if-eqz v3, :cond_f

    .line 391
    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->b(ILjava/lang/String;)V

    goto/16 :goto_8

    .line 392
    :cond_f
    iget-boolean v3, p1, Lcom/twitter/model/core/Tweet;->ab:Z

    if-eqz v3, :cond_10

    .line 393
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->b(ILjava/lang/String;)V

    goto/16 :goto_8

    .line 394
    :cond_10
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->s()Z

    move-result v3

    if-nez v3, :cond_11

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v3

    if-eqz v3, :cond_12

    :cond_11
    if-nez v1, :cond_12

    .line 395
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcgi;)V

    goto/16 :goto_8

    .line 396
    :cond_12
    if-eqz v0, :cond_13

    if-nez p6, :cond_13

    .line 397
    iget-object v0, p3, Laji;->b:Ljava/lang/String;

    iget v1, p3, Laji;->c:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 398
    :cond_13
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 399
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->getOwnerId()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/Tweet;J)V

    goto/16 :goto_8

    .line 400
    :cond_14
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/model/moments/Moment;

    if-eqz v0, :cond_15

    .line 401
    const/16 v0, 0x2b

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/model/moments/Moment;

    iget-object v1, v1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->b(ILjava/lang/String;)V

    .line 402
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->T:Lwn;

    if-eqz v0, :cond_1

    .line 403
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    new-instance v1, Lcom/twitter/android/widget/TweetDetailView$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/TweetDetailView$2;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_8

    .line 410
    :cond_15
    iget v0, p1, Lcom/twitter/model/core/Tweet;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_16

    .line 411
    iget v1, p1, Lcom/twitter/model/core/Tweet;->f:I

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/core/Tweet;->Z:Ljava/lang/String;

    iget v4, p1, Lcom/twitter/model/core/Tweet;->W:I

    iget v5, p1, Lcom/twitter/model/core/Tweet;->Y:I

    iget v6, p1, Lcom/twitter/model/core/Tweet;->X:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/widget/TweetDetailView;->a(ILjava/lang/String;Ljava/lang/String;III)V

    goto/16 :goto_8

    .line 413
    :cond_16
    if-eqz v2, :cond_17

    if-nez p6, :cond_17

    .line 414
    iget-object v0, p3, Laji;->b:Ljava/lang/String;

    iget v1, p3, Laji;->c:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 416
    :cond_17
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/ui/widget/BadgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/BadgeView;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    goto/16 :goto_8

    .line 421
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 427
    :cond_19
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->F:Z

    if-eqz v0, :cond_1a

    .line 428
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    const v1, 0x7f020461

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 429
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    const v1, 0x7f110012

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 430
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    .line 432
    :cond_1a
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    .line 451
    :cond_1b
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    iget-object v0, v0, Lcom/twitter/model/core/Translation;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    iget-object v1, v1, Lcom/twitter/model/core/Translation;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 452
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/Translation;)Z

    .line 453
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->D:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_b

    .line 455
    :cond_1c
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->D:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_b

    .line 481
    :cond_1d
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setVisibility(I)V

    goto/16 :goto_c
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V
    .locals 2

    .prologue
    .line 312
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/model/core/TwitterUser;

    .line 313
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    if-eqz p2, :cond_0

    iget-boolean v0, p2, Lcom/twitter/model/account/UserSettings;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/view/QuoteView;->setDisplaySensitiveMedia(Z)V

    .line 315
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->i()V

    .line 316
    return-void

    .line 313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Translation;)Z
    .locals 3

    .prologue
    .line 558
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/Translation;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/Translation;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->D:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 560
    const/4 v0, 0x0

    .line 575
    :goto_0
    return v0

    .line 564
    :cond_1
    iget-object v0, p1, Lcom/twitter/model/core/Translation;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->R:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 565
    iget-object v0, p1, Lcom/twitter/model/core/Translation;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/Translation;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 568
    iget-object v1, p1, Lcom/twitter/model/core/Translation;->d:Ljava/lang/String;

    invoke-static {v1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/model/core/Translation;->e:Lcom/twitter/model/core/v;

    .line 569
    invoke-virtual {v1, v2}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->g:Lcne;

    .line 570
    invoke-virtual {v1, v2}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v1

    const v2, 0x7f1100c9

    .line 571
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcnf;->b(I)Lcnf;

    move-result-object v0

    .line 572
    invoke-virtual {v0}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 573
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 574
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    .line 575
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 605
    const-string/jumbo v0, "translated_tweet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Translation;

    .line 606
    const-string/jumbo v2, "show_translation"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 607
    if-eqz v0, :cond_0

    .line 608
    iget-object v3, v0, Lcom/twitter/model/core/Translation;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/model/core/Translation;->c:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    .line 612
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    if-eqz v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 613
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/ui/widget/TypefacesTextView;

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->C:Ljava/lang/CharSequence;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    :cond_0
    return-void

    .line 612
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 613
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Ljava/lang/CharSequence;

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Lcom/twitter/model/core/Translation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TypefacesTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 589
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 594
    :goto_0
    return-void

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->C:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 750
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 753
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/view/View;

    .line 755
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->k()V

    .line 756
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Lcom/twitter/media/ui/image/b;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/b;->e()V

    .line 288
    return-void
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 950
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/ui/widget/BadgeView;

    if-eqz v0, :cond_0

    .line 951
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/ui/widget/BadgeView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/BadgeView;->setVisibility(I)V

    .line 953
    :cond_0
    return-void

    .line 951
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Lcom/twitter/media/ui/image/b;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/b;->f()V

    .line 293
    return-void
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/View;)V

    .line 794
    :cond_0
    return-void
.end method

.method public getActionButton()Lcom/twitter/ui/widget/ActionButton;
    .locals 1

    .prologue
    .line 809
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->O:Lcom/twitter/ui/widget/ActionButton;

    return-object v0
.end method

.method public getAutoPlayableItem()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContentContainer()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/widget/c;->a(Lcom/twitter/library/widget/renderablecontent/c;)Lcom/twitter/library/widget/a;

    move-result-object v0

    return-object v0
.end method

.method public getNamePanel()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getTweetTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->h()V

    .line 882
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 208
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 209
    const v0, 0x7f13051a

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 210
    const v1, 0x7f1300fa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/ImageView;

    .line 211
    const v1, 0x7f13040f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/TextView;

    .line 212
    const v1, 0x7f130058

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    .line 213
    const v1, 0x7f13005a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    .line 214
    invoke-static {}, Lbpi;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    .line 216
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 215
    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 217
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/TextView;

    .line 218
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 217
    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 220
    :cond_0
    const v1, 0x7f13031a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    .line 221
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Lcom/twitter/media/ui/image/b;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/b;->a(Lcom/twitter/media/ui/image/a;)V

    .line 223
    const v0, 0x7f13081f

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    .line 224
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/ui/widget/i;->e:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 225
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 226
    const v0, 0x7f13007a

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/socialproof/SocialBylineView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    .line 227
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setRenderRTL(Z)V

    .line 228
    const v0, 0x7f130824

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Landroid/widget/TextView;

    .line 229
    const v0, 0x7f130825

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    .line 230
    const v0, 0x7f130644

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    .line 231
    const v0, 0x7f130003

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->O:Lcom/twitter/ui/widget/ActionButton;

    .line 232
    const v0, 0x7f130758

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Landroid/view/ViewGroup;

    .line 233
    const v0, 0x7f130826

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    .line 234
    const v0, 0x7f130827

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->w:Landroid/widget/TextView;

    .line 235
    const v0, 0x7f130828

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/view/ViewGroup;

    .line 236
    const v0, 0x7f130488

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/view/ViewGroup;

    .line 237
    const v0, 0x7f1303f2

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/view/ViewGroup;

    .line 238
    const v0, 0x7f13081e

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Landroid/widget/ImageView;

    .line 239
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020306

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 240
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v0, v1}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 241
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 243
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/android/widget/TweetDetailView$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/TweetDetailView$1;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    const v0, 0x7f130527

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/BadgeView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/ui/widget/BadgeView;

    .line 252
    new-instance v0, Lcom/twitter/library/revenue/a;

    const v1, 0x7f020841

    const v2, 0x7f020840

    const v3, 0x7f020809

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/library/revenue/a;-><init>(Lcom/twitter/library/revenue/a$a;III)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->Q:Lcom/twitter/library/revenue/a;

    .line 254
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 255
    new-instance v2, Lbxq;

    const v0, 0x7f130276

    .line 256
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f1100c9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v0, v1, v3}, Lbxq;-><init>(Landroid/widget/TextView;Landroid/content/res/Resources;I)V

    iput-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->R:Lbxq;

    .line 258
    const v0, 0x7f130070

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EngagementActionBar;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Lcom/twitter/android/widget/EngagementActionBar;

    .line 260
    const v0, 0x7f130821

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 261
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 262
    const v0, 0x7f130822

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 263
    const v0, 0x7f130820

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->D:Landroid/view/View;

    .line 264
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 266
    const v0, 0x7f13027b

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/QuoteView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    .line 267
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setRenderRtl(Z)V

    .line 268
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Lcom/twitter/media/ui/image/b;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/b;->a(Lcom/twitter/media/ui/image/a;)V

    .line 270
    const v0, 0x7f130823

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    .line 271
    const v0, 0x7f13081a

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Landroid/view/View;

    .line 273
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-static {}, Lbpi;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setMinIconWidth(I)V

    .line 274
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {}, Lbpi;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 276
    const v0, 0x7f13081b

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->L:Landroid/view/ViewGroup;

    .line 277
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 281
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 282
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->p()V

    .line 283
    return-void
.end method

.method public setContentHost(Lcom/twitter/library/widget/renderablecontent/d;)V
    .locals 0

    .prologue
    .line 759
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->l()V

    .line 760
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Lcom/twitter/library/widget/renderablecontent/d;

    .line 761
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->m()V

    .line 762
    return-void
.end method

.method public setMoment(Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 963
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/model/moments/Moment;

    .line 964
    return-void
.end method

.method public setOnCaretClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 802
    return-void
.end method

.method public setOnMediaMonetizationClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    return-void
.end method

.method public setOnMomentClickListener(Lwn;)V
    .locals 0

    .prologue
    .line 797
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->T:Lwn;

    .line 798
    return-void
.end method

.method public setOnTweetAnalyticsClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    return-void
.end method

.method public setQuoteTweetClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/view/QuoteView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    return-void
.end method

.method public setQuoteTweetLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/view/QuoteView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 309
    return-void
.end method

.method public setTranslationButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TypefacesTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 580
    return-void
.end method
