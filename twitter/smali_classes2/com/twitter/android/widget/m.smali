.class public Lcom/twitter/android/widget/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/m$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    const v1, 0x7f13003c

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setId(I)V

    .line 85
    invoke-static {}, Lbpo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    const v1, 0x7f02080e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setIcon(I)V

    .line 87
    const v1, 0x7f0a03c7

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabel(I)V

    .line 88
    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setBackgroundResource(I)V

    .line 94
    :goto_0
    return-object v0

    .line 90
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabelVisibility(I)V

    .line 91
    const v1, 0x7f020110

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setIcon(I)V

    .line 92
    const v1, 0x7f11018a

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public static a([ILandroid/support/v4/app/FragmentActivity;Lcom/twitter/android/media/selection/c;Lcom/twitter/android/widget/m$a;Z)[Landroid/view/View;
    .locals 6

    .prologue
    .line 24
    new-instance v2, Lcom/twitter/android/widget/m$1;

    invoke-direct {v2, p2, p3}, Lcom/twitter/android/widget/m$1;-><init>(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/widget/m$a;)V

    .line 46
    array-length v0, p0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 47
    array-length v4, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    aget v5, p0, v1

    .line 48
    const/4 v0, 0x0

    .line 49
    packed-switch v5, :pswitch_data_0

    .line 69
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 70
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 47
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 51
    :pswitch_0
    invoke-static {p1}, Lcom/twitter/android/widget/m;->a(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    move-result-object v0

    goto :goto_1

    .line 55
    :pswitch_1
    invoke-static {p1}, Lcom/twitter/android/widget/m;->b(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    move-result-object v0

    goto :goto_1

    .line 59
    :pswitch_2
    invoke-static {}, Lbpo;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    if-nez p4, :cond_0

    .line 60
    :cond_2
    invoke-static {p1}, Lcom/twitter/android/widget/m;->c(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    move-result-object v0

    goto :goto_1

    .line 75
    :cond_3
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 76
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    return-object v0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;-><init>(Landroid/content/Context;)V

    .line 100
    const v1, 0x7f13003e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setId(I)V

    .line 101
    const v1, 0x7f0a00cc

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 102
    invoke-static {}, Lbpo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    const v1, 0x7f020811

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setIcon(I)V

    .line 104
    const v1, 0x7f0a03c8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabel(I)V

    .line 105
    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setBackgroundResource(I)V

    .line 111
    :goto_0
    return-object v0

    .line 107
    :cond_0
    const v1, 0x7f020111

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setIcon(I)V

    .line 108
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabelVisibility(I)V

    .line 109
    const v1, 0x7f11018a

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;-><init>(Landroid/content/Context;)V

    .line 117
    const v1, 0x7f13003b

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setId(I)V

    .line 118
    const v1, 0x7f0a00c7

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 119
    invoke-static {}, Lbpo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    const v1, 0x7f020839

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setIcon(I)V

    .line 121
    const v1, 0x7f0a03c5

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabel(I)V

    .line 122
    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setBackgroundResource(I)V

    .line 128
    :goto_0
    return-object v0

    .line 124
    :cond_0
    const v1, 0x7f02010e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setIcon(I)V

    .line 125
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabelVisibility(I)V

    .line 126
    const v1, 0x7f11018a

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setBackgroundResource(I)V

    goto :goto_0
.end method
