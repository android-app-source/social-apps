.class public Lcom/twitter/android/widget/v;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcoi$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string/jumbo v0, "video_autoplay_scroll_threshold_5373"

    invoke-static {v0}, Lcoi;->f(Ljava/lang/String;)Lcoi$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/v;->a:Lcoi$a;

    .line 18
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/android/widget/u;
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/widget/v;->a:Lcoi$a;

    invoke-virtual {v0}, Lcoi$a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 25
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 43
    const v0, 0x3ecccccd    # 0.4f

    .line 47
    :goto_1
    new-instance v1, Lcom/twitter/android/widget/u;

    invoke-direct {v1, p1, v0}, Lcom/twitter/android/widget/u;-><init>(Landroid/view/ViewGroup;F)V

    return-object v1

    .line 25
    :sswitch_0
    const-string/jumbo v2, "30"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "50"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v2, "70"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v2, "90"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 27
    :pswitch_0
    const v0, 0x3e99999a    # 0.3f

    .line 28
    goto :goto_1

    .line 31
    :pswitch_1
    const/high16 v0, 0x3f000000    # 0.5f

    .line 32
    goto :goto_1

    .line 35
    :pswitch_2
    const v0, 0x3f333333    # 0.7f

    .line 36
    goto :goto_1

    .line 39
    :pswitch_3
    const v0, 0x3f666666    # 0.9f

    .line 40
    goto :goto_1

    .line 25
    :sswitch_data_0
    .sparse-switch
        0x65d -> :sswitch_0
        0x69b -> :sswitch_1
        0x6d9 -> :sswitch_2
        0x717 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/widget/v;->a:Lcoi$a;

    invoke-virtual {v0}, Lcoi$a;->close()V

    .line 52
    return-void
.end method
