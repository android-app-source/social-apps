.class public Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method public static b(Landroid/support/v4/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/android/widget/y$a;

    invoke-direct {v0}, Lcom/twitter/android/widget/y$a;-><init>()V

    const v1, 0x7f0d0194

    .line 29
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/y$a;->i(I)Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a05be

    .line 30
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->b(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a05bf

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f020823

    .line 32
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->a(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 33
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 34
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 35
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/android/widget/y;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/y;->a(Landroid/os/Bundle;)Lcom/twitter/android/widget/y;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->a(Landroid/app/Dialog;Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f13001d

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 43
    const v0, 0x7f13001e

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 44
    if-eqz v0, :cond_0

    .line 45
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0bca

    const v5, 0x7f1100c9

    const v6, 0x7f1100c8

    const-class v7, Lcom/twitter/android/WebViewActivity;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;IIILjava/lang/Class;)Lcom/twitter/ui/view/a;

    move-result-object v3

    aput-object v3, v1, v2

    .line 50
    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 51
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "{{}}"

    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :cond_0
    return-void
.end method

.method public synthetic b()Lcom/twitter/android/dialog/g;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->a()Lcom/twitter/android/widget/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/android/dialog/f;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->a()Lcom/twitter/android/widget/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->a()Lcom/twitter/android/widget/y;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->h()V

    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->setCancelable(Z)V

    .line 60
    return-void
.end method
