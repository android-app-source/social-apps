.class public Lcom/twitter/android/widget/ag;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/view/View;)Lcom/twitter/android/widget/af;
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f13005f

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/af;

    return-object v0
.end method

.method public static a(Landroid/view/View;Lcom/twitter/android/widget/af;)V
    .locals 1

    .prologue
    .line 22
    const v0, 0x7f13005f

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 23
    return-void
.end method

.method public static a(Lcom/twitter/android/widget/ad;Landroid/view/ViewGroup;Landroid/view/View;Lcom/twitter/android/widget/ae;)V
    .locals 2

    .prologue
    .line 45
    instance-of v0, p2, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;

    if-eqz v0, :cond_1

    move-object v0, p2

    .line 46
    check-cast v0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;

    .line 47
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->setAdapter(Lcom/twitter/android/widget/ad;)V

    .line 48
    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->setListView(Landroid/view/ViewGroup;)V

    .line 49
    invoke-static {p1, v0}, Lcom/twitter/android/widget/ag;->a(Landroid/view/View;Lcom/twitter/android/widget/af;)V

    .line 50
    const v0, 0x7f130423

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 51
    if-eqz p3, :cond_0

    .line 52
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-interface {p3, v0}, Lcom/twitter/android/widget/ae;->a(Landroid/view/View;)V

    .line 55
    :cond_0
    invoke-interface {p0, v0}, Lcom/twitter/android/widget/ad;->a(Landroid/view/View;)V

    .line 59
    return-void

    .line 57
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Expecting PinnedHeaderListViewContainer as parent of listview!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
