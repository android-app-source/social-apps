.class public Lcom/twitter/android/widget/k;
.super Lcom/twitter/android/widget/o;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/j;


# instance fields
.field private final c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;Landroid/view/View;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;)V
    .locals 10
    .param p5    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 29
    .line 31
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0298

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    .line 29
    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/widget/o;-><init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;IZ)V

    .line 32
    iput-object p4, p0, Lcom/twitter/android/widget/k;->c:Landroid/view/View;

    .line 33
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/widget/k;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/widget/k;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 43
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/widget/k;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    return-void
.end method
