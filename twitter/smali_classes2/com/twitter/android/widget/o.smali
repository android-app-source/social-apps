.class public Lcom/twitter/android/widget/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/n;


# instance fields
.field private final c:Lcom/twitter/android/widget/GalleryGridFragment;

.field private final d:Landroid/support/v4/app/FragmentActivity;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;ZILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/media/selection/d;[Landroid/view/View;I)V
    .locals 1
    .param p3    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/twitter/android/widget/o;->d:Landroid/support/v4/app/FragmentActivity;

    .line 71
    invoke-virtual {p0, p3, p2, p7}, Lcom/twitter/android/widget/o;->a(IZI)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/o;->c:Lcom/twitter/android/widget/GalleryGridFragment;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/widget/o;->c:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p6}, Lcom/twitter/android/widget/GalleryGridFragment;->a([Landroid/view/View;)V

    .line 73
    if-eqz p4, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/android/widget/o;->c:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p4}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/GalleryGridFragment$a;)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/widget/o;->c:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p5}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/media/selection/d;)V

    .line 77
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;IZ)V
    .locals 11
    .param p4    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 31
    sget-object v8, Lcom/twitter/android/widget/o;->a:[I

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/widget/o;-><init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;[IIZ)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;ZIZ)V
    .locals 11
    .param p4    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 45
    if-eqz p8, :cond_0

    sget-object v8, Lcom/twitter/android/widget/o;->a:[I

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/widget/o;-><init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;[IIZ)V

    .line 48
    return-void

    .line 45
    :cond_0
    sget-object v8, Lcom/twitter/android/widget/o;->b:[I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;[IIZ)V
    .locals 11
    .param p4    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 59
    .line 60
    move-object/from16 v0, p8

    move-object/from16 v1, p6

    move/from16 v2, p10

    invoke-static {v0, p1, p3, v1, v2}, Lcom/twitter/android/widget/m;->a([ILandroid/support/v4/app/FragmentActivity;Lcom/twitter/android/media/selection/c;Lcom/twitter/android/widget/m$a;Z)[Landroid/view/View;

    move-result-object v9

    move-object v3, p0

    move-object v4, p1

    move v5, p2

    move v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move/from16 v10, p9

    .line 59
    invoke-direct/range {v3 .. v10}, Lcom/twitter/android/widget/o;-><init>(Landroid/support/v4/app/FragmentActivity;ZILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/media/selection/d;[Landroid/view/View;I)V

    .line 62
    return-void
.end method


# virtual methods
.method a(II)Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 118
    invoke-static {p2, v0, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(IZI)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/twitter/android/widget/o;->d:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const-string/jumbo v3, "gallery"

    .line 122
    invoke-virtual {v2, p1, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 124
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 126
    return-object v0
.end method

.method a(IZI)Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 106
    if-eqz p2, :cond_1

    .line 107
    iget-object v0, p0, Lcom/twitter/android/widget/o;->d:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "gallery"

    .line 108
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GalleryGridFragment;

    .line 112
    :goto_0
    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/widget/o;->a(II)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    .line 112
    :cond_0
    return-object v0

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/widget/o;->c:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 99
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/widget/o;->c:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->h()V

    .line 94
    return-void
.end method
