.class public Lcom/twitter/android/widget/ao;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:Landroid/view/animation/Animation;

.field private final b:Landroid/view/animation/Animation;

.field private final c:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/ao;->a(Landroid/view/View;)Landroid/view/GestureDetector;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/ao;->c:Landroid/view/GestureDetector;

    .line 21
    const v1, 0x7f050057

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/ao;->a:Landroid/view/animation/Animation;

    .line 22
    const v1, 0x7f050058

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ao;->b:Landroid/view/animation/Animation;

    .line 23
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/view/GestureDetector;
    .locals 3

    .prologue
    .line 36
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/ao$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/widget/ao$1;-><init>(Lcom/twitter/android/widget/ao;Landroid/view/View;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/twitter/android/widget/ao;->b:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/ao;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/twitter/android/widget/ao;->a:Landroid/view/animation/Animation;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/widget/ao;->c:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 28
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 29
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    if-nez v0, :cond_1

    .line 30
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 32
    :cond_1
    return v0
.end method
