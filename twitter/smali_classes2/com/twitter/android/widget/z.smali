.class public Lcom/twitter/android/widget/z;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private final a:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private final b:Lcmt;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/support/v4/view/ViewPager$OnPageChangeListener;Lcmt;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/widget/z;->a:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 23
    iput-object p2, p0, Lcom/twitter/android/widget/z;->b:Lcmt;

    .line 24
    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/widget/z;->a:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 46
    iput p1, p0, Lcom/twitter/android/widget/z;->c:I

    .line 47
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 28
    iget-object v2, p0, Lcom/twitter/android/widget/z;->a:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v2, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 29
    iget v2, p0, Lcom/twitter/android/widget/z;->c:I

    if-ne v2, v0, :cond_0

    .line 30
    if-nez p1, :cond_1

    move v2, v0

    .line 31
    :goto_0
    if-nez p3, :cond_2

    .line 32
    :goto_1
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/twitter/android/widget/z;->b:Lcmt;

    invoke-virtual {v0}, Lcmt;->f()Z

    .line 36
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 30
    goto :goto_0

    :cond_2
    move v0, v1

    .line 31
    goto :goto_1
.end method

.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/widget/z;->a:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 41
    return-void
.end method
