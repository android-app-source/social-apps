.class public Lcom/twitter/android/widget/GalleryGridHeaderIconView;
.super Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f04010c

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 36
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p4    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p4, p0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    const v0, 0x7f130044

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->a:Landroid/widget/ImageView;

    .line 45
    const v0, 0x7f1303a4

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->b:Landroid/widget/TextView;

    .line 47
    sget-object v0, Lcom/twitter/android/bi$a;->GalleryGridHeaderIconView:[I

    .line 48
    invoke-virtual {p1, p2, v0, v2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabel(Ljava/lang/String;)V

    .line 50
    const/16 v1, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabelVisibility(I)V

    .line 51
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 52
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setAspectRatio(F)V

    .line 53
    const v1, 0x7f02013a

    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 55
    return-void
.end method


# virtual methods
.method public setIcon(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 76
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 80
    return-void
.end method

.method public setLabel(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabel(Ljava/lang/String;)V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabel(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    if-nez p1, :cond_0

    .line 68
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabelVisibility(I)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->setLabelVisibility(I)V

    goto :goto_0
.end method

.method public setLabelVisibility(I)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridHeaderIconView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    return-void
.end method
