.class Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/widget/GalleryGridSpinnerToolbar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/twitter/media/model/MediaStoreBucket;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 147
    invoke-direct {p0, p1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->c:Z

    .line 148
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 149
    const v1, 0x7f0a03ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a:Ljava/lang/String;

    .line 150
    const v1, 0x7f0a03cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->b:Ljava/lang/String;

    .line 151
    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->setNotifyOnChange(Z)V

    .line 152
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;)I
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a()I

    move-result v0

    return v0
.end method

.method private a(ILandroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 236
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;

    .line 238
    if-nez p1, :cond_0

    .line 239
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;->a(Ljava/lang/String;)V

    .line 240
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;->a(Z)V

    .line 249
    :goto_0
    return-object p2

    .line 241
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;->a(Ljava/lang/String;)V

    .line 243
    iget-boolean v1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->c:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;->a(Z)V

    goto :goto_0

    .line 245
    :cond_1
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/model/MediaStoreBucket;

    .line 246
    invoke-virtual {v1}, Lcom/twitter/media/model/MediaStoreBucket;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;->a(Ljava/lang/String;)V

    .line 247
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;->a(Z)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 227
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    .line 228
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 229
    new-instance v1, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a$a;-><init>(Landroid/view/View;)V

    .line 230
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 231
    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->clear()V

    .line 200
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    :cond_0
    invoke-static {p1}, Lcom/twitter/media/model/MediaStoreBucket;->a(Landroid/database/Cursor;)Lcom/twitter/media/model/MediaStoreBucket;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_1

    .line 204
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->add(Ljava/lang/Object;)V

    .line 206
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->notifyDataSetChanged()V

    .line 209
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->c:Z

    .line 196
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 184
    if-nez p2, :cond_0

    .line 185
    const v0, 0x7f040109

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 187
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 161
    if-nez p1, :cond_0

    move-wide v0, v2

    .line 171
    :goto_0
    return-wide v0

    .line 164
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 165
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 167
    :cond_1
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaStoreBucket;

    .line 168
    if-eqz v0, :cond_2

    .line 169
    invoke-virtual {v0}, Lcom/twitter/media/model/MediaStoreBucket;->b()J

    move-result-wide v0

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 171
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 176
    if-nez p2, :cond_0

    .line 177
    const v0, 0x7f04010a

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 179
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 216
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridSpinnerToolbar$a;->c:Z

    .line 218
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
