.class public Lcom/twitter/android/widget/ComposerSelectionFragment;
.super Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/ComposerSelectionFragment$a;,
        Lcom/twitter/android/widget/ComposerSelectionFragment$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment",
        "<",
        "Lnk;",
        "Lcom/twitter/android/provider/e;",
        ">;"
    }
.end annotation


# instance fields
.field private e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/ComposerSelectionFragment;)Lcom/twitter/android/widget/ComposerSelectionFragment$b;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 32
    const v0, 0x7f04008b

    invoke-super {p0, p1, v0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a(Landroid/view/LayoutInflater;I)Landroid/view/View;

    move-result-object v0

    .line 33
    const v1, 0x7f13029d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->f:Landroid/view/View;

    .line 34
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->f:Landroid/view/View;

    const v2, 0x7f13029f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/ComposerSelectionFragment$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/ComposerSelectionFragment$1;-><init>(Lcom/twitter/android/widget/ComposerSelectionFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->f:Landroid/view/View;

    invoke-static {v1}, Lcom/twitter/util/ui/k;->c(Landroid/view/View;)Z

    .line 47
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->b:Landroid/widget/ListView;

    const v2, 0x1020004

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 48
    return-object v0
.end method

.method public a(Lcom/twitter/android/widget/ComposerSelectionFragment$b;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    .line 124
    return-void
.end method

.method public a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 77
    invoke-virtual {p1, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDrawerDraggable(Z)V

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDraggableBelowUpPosition(Z)V

    .line 79
    invoke-virtual {p1, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDispatchDragToChildren(Z)V

    .line 80
    invoke-virtual {p1, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setAllowDrawerUpPositionIfKeyboard(Z)V

    .line 81
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->f:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setFullScreenHeaderView(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcbi;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lnk;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Lnk;Lcbi;)V

    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->c:Lmq;

    instance-of v0, v0, Lml;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->c:Lmq;

    check-cast v0, Lml;

    invoke-virtual {v0, p1}, Lml;->a(Ljava/util/Collection;)V

    .line 74
    :cond_0
    return-void
.end method

.method public a(Lnk;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 127
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    if-eqz v0, :cond_0

    .line 128
    if-eqz p1, :cond_1

    .line 130
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->d:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0}, Lcom/twitter/android/suggestionselection/a;->c()Lnh;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ComposerSelectionFragment$a;

    iget v1, p1, Lnk;->b:I

    .line 131
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerSelectionFragment$a;->a(I)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->d:Lcom/twitter/android/suggestionselection/a;

    iget-object v1, p1, Lnk;->a:Ljava/lang/String;

    iget-object v2, p1, Lnk;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/lang/CharSequence;I)V

    .line 138
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->b()V

    .line 140
    :cond_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->d:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0}, Lcom/twitter/android/suggestionselection/a;->c()Lnh;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ComposerSelectionFragment$a;

    .line 135
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ComposerSelectionFragment$a;->a(I)V

    .line 136
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->d:Lcom/twitter/android/suggestionselection/a;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/suggestionselection/a;->a(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public a(Lnk;Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnk;",
            "Lcbi",
            "<",
            "Lcom/twitter/android/provider/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a(Ljava/lang/Object;Lcbi;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/widget/ComposerSelectionFragment$b;->a(Lnk;Lcbi;)V

    .line 107
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;JLjava/lang/Object;I)Z
    .locals 6

    .prologue
    .line 25
    move-object v1, p1

    check-cast v1, Lnk;

    move-object v4, p4

    check-cast v4, Lcom/twitter/android/provider/e;

    move-object v0, p0

    move-wide v2, p2

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Lnk;JLcom/twitter/android/provider/e;I)Z

    move-result v0

    return v0
.end method

.method public a(Lnk;JLcom/twitter/android/provider/e;I)Z
    .locals 2

    .prologue
    .line 87
    iget v0, p1, Lnk;->b:I

    invoke-static {v0, p4}, Lml;->a(ILcom/twitter/android/provider/e;)Ljava/lang/String;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->clearFocus()V

    .line 89
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    invoke-interface {v1, v0, p1, p5}, Lcom/twitter/android/widget/ComposerSelectionFragment$b;->a(Ljava/lang/String;Lnk;I)V

    .line 92
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->d()V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->e:Lcom/twitter/android/widget/ComposerSelectionFragment$b;

    invoke-interface {v0}, Lcom/twitter/android/widget/ComposerSelectionFragment$b;->b()V

    .line 115
    :cond_0
    return-void
.end method

.method protected e()Lna;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lna",
            "<",
            "Lnk;",
            "Lcom/twitter/android/provider/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lmv;

    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lnd;

    .line 55
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v4

    const-string/jumbo v5, "compose"

    invoke-direct {v2, v3, v4, v5}, Lnd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lmv;-><init>(Landroid/content/Context;Lnd;)V

    .line 54
    return-object v0
.end method

.method protected f()Lnh;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnh",
            "<",
            "Lnk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    new-instance v0, Lcom/twitter/android/widget/ComposerSelectionFragment$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ComposerSelectionFragment$a;-><init>(Lcom/twitter/android/widget/ComposerSelectionFragment$1;)V

    return-object v0
.end method

.method protected g()Lmq;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmq",
            "<",
            "Lnk;",
            "Lcom/twitter/android/provider/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Lml;

    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lml;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public m()V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->requestFocus()Z

    .line 119
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerSelectionFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 120
    return-void
.end method
