.class public Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/widget/MomentsCardCarouselItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lrx/f;

.field private b:Lrx/functions/a;

.field private c:Lrx/j;


# direct methods
.method public constructor <init>(Lrx/functions/a;Lrx/f;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object p1, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->b:Lrx/functions/a;

    .line 151
    iput-object p2, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->a:Lrx/f;

    .line 152
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;)Lrx/functions/a;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->b:Lrx/functions/a;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->c:Lrx/j;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->c:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 186
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->e()V

    .line 156
    const-wide/16 v0, 0x3e8

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->a:Lrx/f;

    invoke-static {v0, v1, v2, v3}, Lrx/c;->a(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    .line 157
    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a$1;-><init>(Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;)V

    .line 158
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->c:Lrx/j;

    .line 164
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->e()V

    .line 169
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->b:Lrx/functions/a;

    invoke-interface {v0}, Lrx/functions/a;->a()V

    .line 174
    invoke-direct {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->e()V

    .line 175
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->e()V

    .line 180
    return-void
.end method
