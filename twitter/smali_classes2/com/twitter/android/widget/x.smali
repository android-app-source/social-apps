.class public Lcom/twitter/android/widget/x;
.super Lcom/twitter/android/dialog/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/x$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/android/dialog/g;-><init>(Landroid/os/Bundle;)V

    .line 18
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/android/widget/x;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/android/widget/x;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/x;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/widget/x;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_component"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method w()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/widget/x;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_association"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method
