.class Lcom/twitter/android/widget/TweetCarouselView$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbxz;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/model/core/Tweet;Lbxy;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/TweetCarouselView;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/TweetCarouselView;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcax;)V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v0}, Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/android/ct;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v0}, Lcom/twitter/android/widget/TweetCarouselView;->b(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_0

    .line 183
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v1}, Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/android/ct;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v2}, Lcom/twitter/android/widget/TweetCarouselView;->b(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/model/core/Tweet;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcax;Lcom/twitter/library/widget/TweetView;)V

    .line 186
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/MediaEntity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 169
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v0}, Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/android/ct;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v1, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v0, v1, :cond_1

    .line 171
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v0}, Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/android/ct;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v1}, Lcom/twitter/android/widget/TweetCarouselView;->b(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;)V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v0}, Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/android/ct;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-static {v1}, Lcom/twitter/android/widget/TweetCarouselView;->b(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method
