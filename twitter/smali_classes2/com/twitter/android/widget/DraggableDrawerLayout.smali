.class public Lcom/twitter/android/widget/DraggableDrawerLayout;
.super Landroid/view/ViewGroup;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/DraggableDrawerLayout$a;,
        Lcom/twitter/android/widget/DraggableDrawerLayout$b;
    }
.end annotation


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/widget/ImageView;

.field private C:F

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:Landroid/widget/Scroller;

.field private K:Z

.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Z

.field private final f:F

.field private final g:F

.field private final h:F

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:F

.field private final o:Landroid/view/VelocityTracker;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Recycle"
        }
    .end annotation
.end field

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Lcom/twitter/android/widget/DraggableDrawerLayout$b;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 144
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 106
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    .line 112
    iput-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->t:Z

    .line 124
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->D:I

    .line 125
    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    .line 146
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->a:I

    .line 148
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:I

    .line 149
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->c:I

    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 152
    iget v4, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    const v5, 0x7f0e0514

    .line 153
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->d:I

    .line 154
    iget v4, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v6

    iput v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:F

    .line 155
    const v4, 0x3c23d70a    # 0.01f

    iget v5, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:F

    .line 156
    const v4, 0x3ba3d70a    # 0.005f

    iget v5, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:F

    .line 157
    sget-object v4, Lcom/twitter/android/bi$a;->DraggableDrawerLayout:[I

    .line 158
    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 160
    invoke-virtual {v4, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:I

    .line 161
    const/4 v5, 0x2

    .line 162
    invoke-virtual {v4, v5, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->j:I

    .line 163
    const/4 v5, 0x3

    .line 164
    invoke-virtual {v4, v5, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->k:I

    .line 165
    invoke-virtual {v4, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->l:I

    .line 166
    const v5, 0x7f0e01f3

    .line 167
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:I

    .line 170
    const/4 v0, 0x4

    .line 171
    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 172
    iget v5, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v5, v3, :cond_0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:Z

    .line 175
    const/4 v0, 0x5

    const/high16 v3, 0x3f000000    # 0.5f

    .line 176
    invoke-virtual {v4, v0, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    .line 178
    cmpg-float v3, v0, v6

    if-gez v3, :cond_1

    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    :goto_1
    const-string/jumbo v2, "Invalid ratio."

    invoke-static {v1, v2}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 180
    sub-float v0, v6, v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->n:F

    .line 181
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 182
    return-void

    :cond_0
    move v0, v2

    .line 172
    goto :goto_0

    :cond_1
    move v1, v2

    .line 178
    goto :goto_1
.end method

.method private static a(II)I
    .locals 1

    .prologue
    .line 385
    if-lez p0, :cond_0

    move p1, p0

    :cond_0
    const/4 v0, -0x2

    if-ne p0, v0, :cond_1

    const/high16 v0, -0x80000000

    :goto_0
    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/widget/DraggableDrawerLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    return-object v0
.end method

.method private a(FJ)V
    .locals 4

    .prologue
    .line 456
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    .line 457
    if-eqz v0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-lez v1, :cond_0

    .line 458
    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v1

    .line 459
    cmpl-float v2, v1, p1

    if-eqz v2, :cond_0

    .line 460
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 461
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v1, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 462
    invoke-virtual {v2, p2, p3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 463
    new-instance v1, Lcom/twitter/android/widget/DraggableDrawerLayout$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout$2;-><init>(Lcom/twitter/android/widget/DraggableDrawerLayout;Landroid/view/View;F)V

    invoke-virtual {v2, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 469
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 470
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 471
    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 474
    :cond_0
    return-void
.end method

.method private a(IFFF)V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 443
    :cond_0
    new-instance v0, Lcom/twitter/android/widget/DraggableDrawerLayout$a;

    int-to-float v1, p1

    invoke-direct {v0, v1, p2, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout$a;-><init>(FFF)V

    .line 444
    new-instance v1, Lcom/twitter/android/widget/DraggableDrawerLayout$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/DraggableDrawerLayout$1;-><init>(Lcom/twitter/android/widget/DraggableDrawerLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 450
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Z

    .line 451
    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 452
    invoke-virtual {v0}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v0

    invoke-direct {p0, p4, v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(FJ)V

    .line 453
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 669
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 670
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 671
    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 672
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 673
    return-void
.end method

.method private static a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 373
    if-eqz v0, :cond_0

    .line 374
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr p1, v1

    .line 375
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr p2, v0

    .line 380
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p1

    .line 381
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p2

    .line 380
    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 382
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/DraggableDrawerLayout;Z)Z
    .locals 0

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Z

    return p1
.end method

.method private static b(Landroid/view/View;II)I
    .locals 4

    .prologue
    .line 391
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 392
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    .line 393
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    .line 394
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    sub-int v1, p1, v1

    invoke-static {v3, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(II)I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    sub-int v3, p2, v2

    .line 395
    invoke-static {v0, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(II)I

    move-result v0

    .line 394
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->measure(II)V

    .line 396
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/DraggableDrawerLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    return-object v0
.end method

.method private b(Z)V
    .locals 0

    .prologue
    .line 400
    if-eqz p1, :cond_0

    .line 401
    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->h()V

    .line 405
    :goto_0
    return-void

    .line 403
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->i()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/widget/DraggableDrawerLayout;)Lcom/twitter/android/widget/DraggableDrawerLayout$b;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:Lcom/twitter/android/widget/DraggableDrawerLayout$b;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/DraggableDrawerLayout;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/widget/DraggableDrawerLayout;)Landroid/widget/Scroller;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->J:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/widget/DraggableDrawerLayout;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:I

    return v0
.end method

.method private h()V
    .locals 9

    .prologue
    const v6, 0x7fffffff

    const/4 v1, 0x0

    .line 408
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->c:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 409
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    .line 410
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->J:Landroid/widget/Scroller;

    if-nez v0, :cond_0

    .line 411
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->J:Landroid/widget/Scroller;

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->J:Landroid/widget/Scroller;

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v3

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/widget/Scroller;->setFriction(F)V

    .line 414
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->J:Landroid/widget/Scroller;

    float-to-int v4, v2

    const/high16 v7, -0x80000000

    move v2, v1

    move v3, v1

    move v5, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 417
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->F:I

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 418
    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:F

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:F

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IFFF)V

    .line 419
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    .line 422
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, v0, v1

    .line 423
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 424
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    .line 425
    if-lez v1, :cond_0

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:F

    .line 426
    :goto_0
    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IFFF)V

    .line 427
    return-void

    .line 425
    :cond_0
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:F

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 431
    if-gez v0, :cond_0

    .line 432
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->g()V

    .line 436
    :goto_0
    return-void

    .line 434
    :cond_0
    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:F

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:F

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IFFF)V

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 286
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    .line 288
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 289
    if-eqz p1, :cond_1

    .line 290
    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->j()V

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->g()V

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_2

    :cond_0
    const/4 v0, 0x2

    .line 275
    :goto_0
    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-eq v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 276
    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    .line 277
    if-eqz p1, :cond_3

    .line 278
    invoke-direct {p0, p2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(Z)V

    .line 283
    :cond_1
    :goto_1
    return-void

    .line 273
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 280
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->g()V

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->K:Z

    return v0
.end method

.method f()V
    .locals 1

    .prologue
    .line 479
    new-instance v0, Lcom/twitter/android/widget/DraggableDrawerLayout$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/DraggableDrawerLayout$3;-><init>(Lcom/twitter/android/widget/DraggableDrawerLayout;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->post(Ljava/lang/Runnable;)Z

    .line 505
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 509
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 511
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 512
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:Lcom/twitter/android/widget/DraggableDrawerLayout$b;

    if-eqz v0, :cond_1

    .line 513
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:Lcom/twitter/android/widget/DraggableDrawerLayout$b;

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout$b;->b(I)V

    .line 515
    :cond_1
    return-void
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 299
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getDrawerPosition()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 187
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    .line 188
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 192
    :cond_0
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->j:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->A:Landroid/view/View;

    .line 193
    const v0, 0x7f130285

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    .line 194
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    const-string/jumbo v1, "drawer_header"

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:Landroid/view/View;

    .line 195
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->k:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    .line 196
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 519
    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Z

    if-eqz v2, :cond_1

    .line 527
    :cond_0
    :goto_0
    return v0

    .line 521
    :cond_1
    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->r:Z

    if-eqz v2, :cond_2

    move v0, v1

    .line 522
    goto :goto_0

    .line 526
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 527
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->G:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->l:I

    add-int/2addr v1, p3

    invoke-static {v0, p2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/View;II)V

    .line 357
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->A:Landroid/view/View;

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->l:I

    add-int/2addr v1, p3

    invoke-static {v0, p2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/View;II)V

    .line 359
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    add-int/2addr v1, p3

    invoke-static {v0, p2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/View;II)V

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 365
    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 368
    :cond_2
    return-void

    .line 365
    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 304
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 305
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getMeasuredWidth()I

    move-result v4

    .line 306
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getMeasuredHeight()I

    move-result v3

    .line 308
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    .line 309
    invoke-static {v0, v4, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(Landroid/view/View;II)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->F:I

    .line 310
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:Landroid/view/View;

    .line 311
    invoke-static {v0, v4, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(Landroid/view/View;II)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->G:I

    .line 314
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->K:Z

    .line 315
    iget v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->d:I

    if-ge v3, v5, :cond_0

    move v1, v2

    :cond_0
    iput-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->K:Z

    .line 316
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->b()I

    move-result v1

    .line 317
    iget-boolean v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->K:Z

    if-eqz v5, :cond_6

    .line 318
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->G:I

    sub-int v0, v3, v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    .line 319
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->v:Z

    if-eqz v0, :cond_1

    .line 320
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    int-to-float v0, v0

    iget v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->n:F

    mul-float/2addr v0, v5

    float-to-int v0, v0

    .line 321
    iget v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:I

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    .line 327
    :cond_1
    :goto_2
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->K:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v0, v2, :cond_8

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->v:Z

    if-eqz v0, :cond_8

    .line 328
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    .line 339
    :goto_3
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-eq v0, v6, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->r:Z

    if-eqz v0, :cond_d

    :cond_2
    move v0, v3

    .line 341
    :goto_4
    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v1, v6, :cond_e

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    .line 343
    :goto_5
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->isInEditMode()Z

    move-result v2

    if-nez v2, :cond_3

    .line 344
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->A:Landroid/view/View;

    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->l:I

    sub-int/2addr v1, v3

    invoke-static {v2, v4, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(Landroid/view/View;II)I

    .line 345
    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    sub-int/2addr v0, v2

    invoke-static {v1, v4, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(Landroid/view/View;II)I

    .line 347
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 309
    goto :goto_0

    :cond_5
    move v0, v1

    .line 311
    goto :goto_1

    .line 323
    :cond_6
    iget v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    if-eqz v5, :cond_7

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->K:Z

    if-nez v0, :cond_1

    .line 324
    :cond_7
    int-to-float v0, v1

    iget v5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->n:F

    mul-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    goto :goto_2

    .line 329
    :cond_8
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->K:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v0, v6, :cond_a

    :cond_9
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-nez v0, :cond_b

    .line 331
    :cond_a
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->G:I

    sub-int v0, v3, v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    goto :goto_3

    .line 332
    :cond_b
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v0, v2, :cond_c

    .line 333
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    goto :goto_3

    .line 335
    :cond_c
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->F:I

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    goto :goto_3

    .line 339
    :cond_d
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    add-int/2addr v0, v1

    goto :goto_4

    .line 341
    :cond_e
    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->I:I

    goto :goto_5
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 532
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 533
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    .line 534
    if-lt v4, v5, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    if-gt v4, v0, :cond_1

    move v0, v1

    .line 536
    :goto_0
    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Z

    if-eqz v3, :cond_2

    .line 665
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 534
    goto :goto_0

    .line 538
    :cond_2
    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->r:Z

    if-eqz v3, :cond_3

    .line 539
    if-eqz v0, :cond_0

    .line 540
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 545
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_1

    .line 549
    :pswitch_0
    if-eqz v0, :cond_4

    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    if-nez v3, :cond_4

    .line 550
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    .line 553
    :cond_4
    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->s:Z

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    .line 554
    int-to-float v0, v4

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->C:F

    .line 555
    iput v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->D:I

    .line 556
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 558
    :cond_5
    iput-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    .line 559
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_1

    .line 565
    :pswitch_1
    iput v9, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->D:I

    .line 566
    const/high16 v3, -0x31000000

    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->C:F

    .line 567
    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    if-eqz v3, :cond_7

    .line 568
    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 569
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v3, v7, :cond_b

    .line 570
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->F:I

    add-int/lit8 v3, v3, 0xa

    if-lt v5, v3, :cond_6

    .line 571
    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:Z

    if-eqz v3, :cond_a

    move v3, v2

    :goto_2
    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    .line 576
    :cond_6
    :goto_3
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-nez v3, :cond_c

    .line 577
    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->j()V

    .line 583
    :cond_7
    :goto_4
    if-eqz v0, :cond_9

    .line 586
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    if-eqz v0, :cond_8

    .line 587
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 589
    :cond_8
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    .line 591
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 592
    iput-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    goto :goto_1

    :cond_a
    move v3, v1

    .line 571
    goto :goto_2

    .line 573
    :cond_b
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    add-int/lit8 v3, v3, -0x14

    if-gt v5, v3, :cond_6

    .line 574
    iput v7, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    goto :goto_3

    .line 579
    :cond_c
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v3, v7, :cond_d

    move v3, v1

    :goto_5
    invoke-direct {p0, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(Z)V

    goto :goto_4

    :cond_d
    move v3, v2

    goto :goto_5

    .line 596
    :pswitch_2
    int-to-float v0, v4

    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->C:F

    sub-float/2addr v0, v3

    float-to-int v6, v0

    .line 597
    int-to-float v0, v4

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->C:F

    .line 599
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->D:I

    if-eq v0, v9, :cond_14

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->D:I

    sub-int/2addr v0, v4

    .line 600
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->a:I

    if-le v0, v3, :cond_14

    move v3, v1

    .line 601
    :goto_6
    if-le v4, v5, :cond_15

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v0, v1, :cond_e

    if-ltz v6, :cond_f

    :cond_e
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    if-ne v0, v7, :cond_15

    if-lez v6, :cond_15

    :cond_f
    move v0, v1

    .line 604
    :goto_7
    if-eqz v3, :cond_12

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->s:Z

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    if-nez v0, :cond_12

    .line 605
    iput-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    .line 606
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    if-eqz v0, :cond_10

    .line 607
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 609
    :cond_10
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:Lcom/twitter/android/widget/DraggableDrawerLayout$b;

    if-eqz v0, :cond_11

    .line 610
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:Lcom/twitter/android/widget/DraggableDrawerLayout$b;

    invoke-interface {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout$b;->r()V

    .line 612
    :cond_11
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 613
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    .line 616
    :cond_12
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    if-eqz v0, :cond_19

    .line 617
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 618
    add-int v0, v5, v6

    .line 619
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->F:I

    if-gt v0, v3, :cond_16

    .line 622
    iput v7, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->E:I

    .line 623
    iput-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Z

    .line 624
    iput-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->r:Z

    .line 625
    iput v9, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->D:I

    .line 626
    const/high16 v0, -0x31000000

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->C:F

    .line 627
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->g()V

    .line 628
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Z

    if-eqz v0, :cond_13

    .line 630
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 631
    invoke-static {p0, v0}, Lcom/twitter/util/ui/k;->b(Landroid/view/View;Landroid/view/View;)I

    move-result v1

    .line 632
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 633
    invoke-virtual {v3, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 634
    int-to-float v4, v1

    invoke-static {}, Lcom/twitter/util/z;->e()F

    move-result v5

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    invoke-virtual {v3, v8, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 635
    invoke-virtual {v0, v3}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 636
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 637
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 638
    invoke-virtual {v3, v7}, Landroid/view/MotionEvent;->setAction(I)V

    .line 639
    int-to-float v1, v1

    invoke-virtual {v3, v8, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 640
    invoke-virtual {v0, v3}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 641
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    :cond_13
    move v1, v2

    .line 643
    goto/16 :goto_1

    :cond_14
    move v3, v2

    .line 600
    goto/16 :goto_6

    :cond_15
    move v0, v2

    .line 601
    goto/16 :goto_7

    .line 645
    :cond_16
    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    if-eqz v3, :cond_17

    .line 646
    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 647
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:Landroid/widget/ImageView;

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v4, v0

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    .line 648
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    const/high16 v4, 0x3f400000    # 0.75f

    .line 647
    invoke-static {v3, v8, v4}, Lcom/twitter/util/math/b;->a(FFF)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 651
    :cond_17
    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->t:Z

    if-nez v2, :cond_18

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->H:I

    if-gt v0, v2, :cond_0

    .line 652
    :cond_18
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto/16 :goto_1

    .line 656
    :cond_19
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 545
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setAllowDrawerUpPositionIfKeyboard(Z)V
    .locals 0

    .prologue
    .line 269
    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->v:Z

    .line 270
    return-void
.end method

.method public setDispatchDragToChildren(Z)V
    .locals 0

    .prologue
    .line 240
    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Z

    .line 241
    return-void
.end method

.method public setDraggableBelowUpPosition(Z)V
    .locals 0

    .prologue
    .line 232
    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->t:Z

    .line 233
    return-void
.end method

.method public setDrawerDraggable(Z)V
    .locals 0

    .prologue
    .line 228
    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->s:Z

    .line 229
    return-void
.end method

.method public setDrawerLayoutListener(Lcom/twitter/android/widget/DraggableDrawerLayout$b;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:Lcom/twitter/android/widget/DraggableDrawerLayout$b;

    .line 266
    return-void
.end method

.method public setFullScreenHeaderView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    if-eq v0, p1, :cond_1

    .line 245
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    .line 246
    iput-object p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Landroid/view/View;

    .line 247
    if-eqz v0, :cond_0

    .line 248
    invoke-static {v0}, Lcom/twitter/util/ui/k;->c(Landroid/view/View;)Z

    .line 250
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->F:I

    .line 251
    if-eqz p1, :cond_1

    .line 252
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 254
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 255
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->addView(Landroid/view/View;)V

    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getMeasuredHeight()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(Landroid/view/View;II)I

    .line 258
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->F:I

    .line 262
    :cond_1
    return-void
.end method

.method public setLocked(Z)V
    .locals 0

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->r:Z

    .line 237
    return-void
.end method
