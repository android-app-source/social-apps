.class public Lcom/twitter/android/widget/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/widget/Button;

.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Lcom/twitter/android/widget/b;

.field private final i:J

.field private j:Landroid/app/AlertDialog;

.field private final k:Lcom/twitter/android/widget/b$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/twitter/android/widget/a$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/a$1;-><init>(Lcom/twitter/android/widget/a;)V

    iput-object v0, p0, Lcom/twitter/android/widget/a;->k:Lcom/twitter/android/widget/b$a;

    .line 86
    iput-object p1, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    .line 87
    iput-wide p2, p0, Lcom/twitter/android/widget/a;->i:J

    .line 88
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/widget/a;->j:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/a;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/twitter/android/widget/a;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 94
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/a;->j:Landroid/app/AlertDialog;

    .line 95
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 139
    const v0, 0x7f13017a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/a;->f:Landroid/widget/TextView;

    .line 140
    const v0, 0x7f13017b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/a;->g:Landroid/widget/TextView;

    .line 141
    const v0, 0x7f13017c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/widget/a;->b:Landroid/widget/Button;

    .line 142
    const v0, 0x7f13017d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/widget/a;->c:Landroid/widget/Button;

    .line 143
    const v0, 0x7f13017e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/widget/a;->e:Landroid/widget/Button;

    .line 144
    const v0, 0x7f13017f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/widget/a;->d:Landroid/widget/Button;

    .line 145
    new-instance v1, Lcom/twitter/android/widget/b;

    iget-object v2, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    const v0, 0x7f130174

    .line 146
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/twitter/android/widget/a;->k:Lcom/twitter/android/widget/b$a;

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/android/widget/b;-><init>(Landroid/content/Context;Landroid/widget/LinearLayout;Lcom/twitter/android/widget/b$a;)V

    iput-object v1, p0, Lcom/twitter/android/widget/a;->h:Lcom/twitter/android/widget/b;

    .line 149
    iget-object v0, p0, Lcom/twitter/android/widget/a;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/widget/a;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v0, p0, Lcom/twitter/android/widget/a;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    iget-object v0, p0, Lcom/twitter/android/widget/a;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    return-void
.end method

.method protected a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;)V
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/twitter/android/widget/a;->i:J

    invoke-static {p1, v0, v1}, Lcom/twitter/android/util/AppRatingPromptHelper;->b(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;J)V

    .line 99
    return-void
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 102
    sget-object v0, Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;->b:Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/a;->a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;)V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/AppRatingPromptHelper;->a(Landroid/content/Context;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "market://details?id=com.twitter.android"

    .line 105
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 104
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 106
    return-void
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;->c:Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/a;->a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;)V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/AppRatingPromptHelper;->a(Landroid/content/Context;)V

    .line 111
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;->d:Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/a;->a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;)V

    .line 115
    iget-object v0, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/AppRatingPromptHelper;->b(Landroid/content/Context;)V

    .line 116
    return-void
.end method

.method protected e()Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 123
    sget-object v0, Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;->a:Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/a;->a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;)V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    const-string/jumbo v1, "layout_inflater"

    .line 125
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 126
    invoke-virtual {p0}, Lcom/twitter/android/widget/a;->g()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/util/AppRatingPromptHelper;->b(Landroid/content/Context;)V

    .line 129
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/a;->a(Landroid/view/View;)V

    .line 130
    invoke-virtual {p0}, Lcom/twitter/android/widget/a;->e()Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/a;->j:Landroid/app/AlertDialog;

    .line 131
    iget-object v0, p0, Lcom/twitter/android/widget/a;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 132
    return-void
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 135
    const v0, 0x7f040038

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 157
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 190
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/a;->a()V

    .line 191
    return-void

    .line 159
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/a;->b()V

    goto :goto_0

    .line 163
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/a;->d()V

    goto :goto_0

    .line 167
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/a;->c()V

    goto :goto_0

    .line 171
    :pswitch_3
    sget-object v0, Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;->e:Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/a;->a(Lcom/twitter/android/util/AppRatingPromptHelper$AppRatingEvent;)V

    .line 172
    iget-object v0, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/AppRatingPromptHelper;->a(Landroid/content/Context;)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/widget/a;->h:Lcom/twitter/android/widget/b;

    invoke-virtual {v0}, Lcom/twitter/android/widget/b;->a()I

    move-result v0

    .line 174
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SENDTO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 175
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mailto:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    const v4, 0x7f0a0068

    .line 176
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 177
    const-string/jumbo v2, "android.intent.extra.SUBJECT"

    iget-object v3, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    const v4, 0x7f0a006a

    .line 178
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 177
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const-string/jumbo v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    const v4, 0x7f0a0069

    .line 180
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    .line 181
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    .line 180
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    iget-object v0, p0, Lcom/twitter/android/widget/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x7f13017c
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
