.class public Lcom/twitter/android/widget/MediaBadgeOverlayView;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Lcom/twitter/library/media/widget/VideoDurationView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/MediaBadgeOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/MediaBadgeOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401ad

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 37
    const v0, 0x7f13049c

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaBadgeOverlayView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a:Landroid/widget/ImageView;

    .line 38
    const v0, 0x7f13009c

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MediaBadgeOverlayView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/widget/VideoDurationView;

    iput-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->b:Lcom/twitter/library/media/widget/VideoDurationView;

    .line 39
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 74
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->b:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/VideoDurationView;->setVisibility(I)V

    .line 76
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 43
    if-eqz p1, :cond_0

    .line 44
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/twitter/android/widget/MediaBadgeOverlayView$1;->a:[I

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 71
    :goto_0
    return-void

    .line 47
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 48
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->b:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/widget/VideoDurationView;->setVisibility(I)V

    goto :goto_0

    .line 52
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->b:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/widget/VideoDurationView;->setVisibility(I)V

    goto :goto_0

    .line 57
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    check-cast p1, Lcom/twitter/model/media/EditableVideo;

    .line 59
    iget-object v1, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->b:Lcom/twitter/library/media/widget/VideoDurationView;

    iget-object v0, p1, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    iget v0, v0, Lcom/twitter/media/model/VideoFile;->h:I

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/VideoDurationView;->setDuration(I)V

    .line 60
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->b:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, v3}, Lcom/twitter/library/media/widget/VideoDurationView;->setVisibility(I)V

    goto :goto_0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/widget/MediaBadgeOverlayView;->b:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/widget/VideoDurationView;->setVisibility(I)V

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
