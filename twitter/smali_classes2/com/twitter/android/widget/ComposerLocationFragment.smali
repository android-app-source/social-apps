.class public abstract Lcom/twitter/android/widget/ComposerLocationFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Lbqk;
.implements Lcom/twitter/android/geo/a$a;
.implements Lcom/twitter/android/widget/DraggableDrawerLayout$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/ComposerLocationFragment$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/android/geo/PlacePickerModel;

.field protected b:Lbqn;

.field protected c:Lcom/twitter/library/client/Session;

.field protected d:J

.field protected e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

.field protected f:Lcom/twitter/model/geo/b;

.field protected g:Z

.field protected h:Z

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 34
    new-instance v0, Lcom/twitter/android/geo/PlacePickerModel;

    invoke-direct {v0}, Lcom/twitter/android/geo/PlacePickerModel;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->i:Z

    .line 167
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public a(Lcom/twitter/android/geo/GeoTagState;)V
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerLocationFragment;->b(Z)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v0, p1}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/android/geo/GeoTagState;)Lcom/twitter/android/geo/PlacePickerModel;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v0, :cond_2

    .line 113
    const/4 v0, 0x0

    .line 114
    invoke-virtual {p1}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    invoke-virtual {p1}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    invoke-interface {v1, v0}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->a(Ljava/lang/String;)V

    .line 119
    :cond_2
    return-void
.end method

.method public a(Lcom/twitter/android/widget/ComposerLocationFragment$a;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    .line 104
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 72
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->g()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerLocationFragment;->b(Z)V

    .line 73
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method public b(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->h:Z

    .line 88
    return-void
.end method

.method protected b(Z)V
    .locals 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->g:Z

    if-eq v0, p1, :cond_0

    .line 128
    iput-boolean p1, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->g:Z

    .line 129
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1, p1}, Lbqg;->a(Lcom/twitter/library/client/Session;Z)V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->e:Lcom/twitter/android/widget/ComposerLocationFragment$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/ComposerLocationFragment$a;->a(Z)V

    .line 134
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->g:Z

    if-eqz v0, :cond_1

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->h:Z

    .line 136
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->b:Lbqn;

    invoke-virtual {v0, p0}, Lbqn;->a(Lbqk;)V

    .line 143
    :goto_0
    return-void

    .line 138
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->h:Z

    .line 139
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->b:Lbqn;

    invoke-virtual {v0, p0}, Lbqn;->b(Lbqk;)V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->f:Lcom/twitter/model/geo/b;

    .line 141
    invoke-static {}, Lcom/twitter/android/geo/GeoTagState;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerLocationFragment;->a(Lcom/twitter/android/geo/GeoTagState;)V

    goto :goto_0
.end method

.method public d()Lcom/twitter/android/geo/GeoTagState;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->a:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v0}, Lcom/twitter/android/geo/PlacePickerModel;->c()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    .line 154
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "geoSelectedPlaceId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nisGeoAutoTagEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 155
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    return-object v0

    .line 152
    :cond_0
    const-string/jumbo v0, "none"

    goto :goto_0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 159
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->c:Lcom/twitter/library/client/Session;

    .line 160
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->d:J

    .line 161
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->g()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerLocationFragment;->b(Z)V

    .line 162
    return-void
.end method

.method protected g()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 175
    iget-boolean v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->i:Z

    if-nez v0, :cond_0

    .line 176
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v3}, Lbqg;->c(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 177
    :goto_0
    iput-boolean v1, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->i:Z

    .line 179
    if-eqz v0, :cond_2

    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    .line 180
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 179
    invoke-virtual {v0, v3}, Lbqg;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 176
    goto :goto_0

    :cond_2
    move v2, v1

    .line 179
    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    .line 49
    invoke-static {p1}, Lbqn;->a(Landroid/content/Context;)Lbqn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->b:Lbqn;

    .line 50
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->c:Lcom/twitter/library/client/Session;

    .line 51
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->d:J

    .line 52
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 57
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-super {p0, v4}, Lcom/twitter/app/common/abs/AbsFragment;->setRetainInstance(Z)V

    .line 61
    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerLocationFragment;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string/jumbo v0, "compose:::autotag:enabled"

    .line 66
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->d:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 67
    return-void

    .line 64
    :cond_0
    const-string/jumbo v0, "compose:::autotag:disabled"

    goto :goto_0
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLocationFragment;->b:Lbqn;

    invoke-virtual {v0, p0}, Lbqn;->b(Lbqk;)V

    .line 78
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 79
    return-void
.end method

.method public r()V
    .locals 0

    .prologue
    .line 100
    return-void
.end method
