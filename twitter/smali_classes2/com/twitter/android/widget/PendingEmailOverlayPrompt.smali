.class public Lcom/twitter/android/widget/PendingEmailOverlayPrompt;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    const v0, 0x7f0a0040

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/twitter/android/widget/ac$a;

    invoke-direct {v1, v2}, Lcom/twitter/android/widget/ac$a;-><init>(I)V

    .line 23
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ac$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ac$a;

    const v1, 0x7f0a0041

    .line 24
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ac$a;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ac$a;

    .line 25
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/ac$a;->a(Ljava/lang/String;)Lcom/twitter/android/widget/ac$a;

    move-result-object v0

    const v1, 0x7f020805

    .line 26
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ac$a;->a(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ac$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ac$a;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 27
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 28
    return-void
.end method

.method private m()V
    .locals 6

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->a()Lcom/twitter/android/widget/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/ac;->a()Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->l()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 46
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/twitter/android/settings/AccountActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "pending_email"

    .line 47
    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_account_id"

    .line 48
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 49
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->startActivity(Landroid/content/Intent;)V

    .line 50
    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 51
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/android/widget/ac;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/ac;->a(Landroid/os/Bundle;)Lcom/twitter/android/widget/ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lcom/twitter/android/dialog/g;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->a()Lcom/twitter/android/widget/ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/android/dialog/f;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->a()Lcom/twitter/android/widget/ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->a()Lcom/twitter/android/widget/ac;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 32
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->i()V

    .line 33
    invoke-direct {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->m()V

    .line 34
    return-void
.end method

.method protected k()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->k()V

    .line 39
    invoke-direct {p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->m()V

    .line 40
    return-void
.end method
