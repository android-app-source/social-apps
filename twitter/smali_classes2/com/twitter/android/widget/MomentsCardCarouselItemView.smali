.class public Lcom/twitter/android/widget/MomentsCardCarouselItemView;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/widget/MomentsCardCarouselItemView$b;,
        Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/moments/ui/card/h;

.field private b:Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$b;

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$b;-><init>(Lrx/f;)V

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/MomentsCardCarouselItemView$b;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/widget/MomentsCardCarouselItemView$b;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-direct {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a()Lrx/functions/a;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$b;->a(Lrx/functions/a;)Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->b:Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;

    .line 46
    return-void
.end method

.method private a()Lrx/functions/a;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/android/widget/MomentsCardCarouselItemView$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$1;-><init>(Lcom/twitter/android/widget/MomentsCardCarouselItemView;)V

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->removeAllViews()V

    .line 116
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->addView(Landroid/view/View;)V

    .line 117
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/MomentsCardCarouselItemView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->b()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/h;->c()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    .line 124
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/card/h;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->b:Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->d()V

    .line 97
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    if-ne v0, p1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 101
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->b()V

    .line 102
    iput-object p1, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    .line 103
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/h;->e()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public getAutoPlayableItem()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    .line 110
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/h;->d()Lcom/twitter/library/widget/a;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 110
    :cond_0
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    goto :goto_0
.end method

.method public getBoundMomentId()J
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a:Lcom/twitter/android/moments/ui/card/h;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/card/h;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 51
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->b:Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->c()V

    .line 52
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishTemporaryDetach()V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->b:Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->b()V

    .line 58
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroid/widget/FrameLayout;->onStartTemporaryDetach()V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->b:Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView$a;->a()V

    .line 76
    return-void
.end method
