.class public Lcom/twitter/android/widget/x$a;
.super Lcom/twitter/android/dialog/g$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/widget/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/dialog/g$b;-><init>(I)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/widget/x$a;
    .locals 2

    .prologue
    .line 56
    if-eqz p1, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/android/widget/x$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_association"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/widget/x$a;
    .locals 2

    .prologue
    .line 48
    if-eqz p1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/twitter/android/widget/x$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_component"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    return-object p0
.end method

.method protected b()Lcom/twitter/android/widget/MuteConversationEducationOverlay;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/android/widget/MuteConversationEducationOverlay;

    invoke-direct {v0}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;-><init>()V

    return-object v0
.end method

.method protected synthetic c()Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/twitter/android/widget/x$a;->b()Lcom/twitter/android/widget/MuteConversationEducationOverlay;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Lcom/twitter/android/dialog/TakeoverDialogFragment;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/twitter/android/widget/x$a;->b()Lcom/twitter/android/widget/MuteConversationEducationOverlay;

    move-result-object v0

    return-object v0
.end method
