.class public Lcom/twitter/android/widget/TweetCarouselView;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Lbxf;
.implements Lcom/twitter/library/revenue/a$a;
.implements Lcom/twitter/library/widget/InlineActionBar$b;
.implements Lcom/twitter/library/widget/h;


# static fields
.field private static final a:Landroid/text/TextPaint;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/view/ViewGroup;

.field private d:Lcom/twitter/media/ui/image/UserImageView;

.field private e:Lcom/twitter/ui/widget/TweetHeaderView;

.field private f:Lcom/twitter/library/widget/TextContentView;

.field private g:Lcom/twitter/library/widget/InlineActionBar;

.field private h:Lcom/twitter/ui/widget/BadgeView;

.field private i:Lcom/twitter/model/core/Tweet;

.field private j:Lcom/twitter/library/widget/renderablecontent/d;

.field private k:Landroid/view/ViewGroup;

.field private l:Lcom/twitter/android/ct;

.field private m:Lcom/twitter/android/ck;

.field private n:Lcom/twitter/model/util/FriendshipCache;

.field private o:Lcom/twitter/library/revenue/a;

.field private p:F

.field private q:F

.field private r:I

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/android/widget/TweetCarouselView;->a:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TweetCarouselView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetCarouselView;->a()V

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 98
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetCarouselView;->a()V

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/android/ct;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->l:Lcom/twitter/android/ct;

    return-object v0
.end method

.method private a(Lcom/twitter/model/core/Tweet;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 248
    new-instance v0, Lcom/twitter/android/widget/TweetCarouselView$3;

    iget v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->s:I

    iget v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->r:I

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/TweetCarouselView$3;-><init>(Lcom/twitter/android/widget/TweetCarouselView;IIZLcom/twitter/model/core/Tweet;)V

    .line 257
    const v1, 0x7f0a0362

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 258
    const v2, 0x7f0a024f

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 259
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 260
    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 261
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    const/16 v2, 0x21

    invoke-virtual {v3, v0, v4, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 264
    return-object v3
.end method

.method private a(Ljava/lang/String;Lcom/twitter/model/core/v;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 268
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    .line 271
    invoke-virtual {p2}, Lcom/twitter/model/core/v;->a()Z

    move-result v0

    .line 272
    if-eqz v0, :cond_2

    .line 273
    new-instance v0, Lcom/twitter/android/widget/TweetCarouselView$4;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/widget/TweetCarouselView$4;-><init>(Lcom/twitter/android/widget/TweetCarouselView;Lcom/twitter/model/core/Tweet;)V

    .line 282
    invoke-static {p1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v2

    .line 283
    invoke-virtual {v2, p2}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v2

    .line 284
    invoke-virtual {v2, v0}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->s:I

    .line 285
    invoke-virtual {v0, v2}, Lcnf;->b(I)Lcnf;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->r:I

    .line 286
    invoke-virtual {v0, v2}, Lcnf;->a(I)Lcnf;

    move-result-object v0

    .line 287
    invoke-virtual {v0, v3}, Lcnf;->a(Z)Lcnf;

    move-result-object v0

    .line 288
    invoke-virtual {v0, v3}, Lcnf;->b(Z)Lcnf;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 294
    :goto_0
    invoke-static {}, Lcom/twitter/library/view/b;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->b:Landroid/content/Context;

    iget-object v2, p2, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 299
    :cond_0
    if-eqz v0, :cond_1

    .line 300
    sget-object v1, Lcom/twitter/android/widget/TweetCarouselView;->a:Landroid/text/TextPaint;

    .line 301
    iget v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->p:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 302
    iget-object v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 305
    :cond_1
    return-object v0

    .line 291
    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetCarouselView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->b:Landroid/content/Context;

    .line 104
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetCarouselView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 105
    const v1, 0x7f1100c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->r:I

    .line 106
    const v1, 0x7f1100c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->s:I

    .line 108
    sget v0, Lcni;->a:F

    iput v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->p:F

    .line 109
    iget v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->p:F

    invoke-static {v0}, Lcni;->a(F)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->q:F

    .line 110
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, -0x2

    .line 350
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bg_()V

    .line 352
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v1

    .line 353
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetCarouselView;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetCarouselView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04005c

    iget-object v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 356
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->c:Landroid/view/ViewGroup;

    const v2, 0x7f1301d9

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->k:Landroid/view/ViewGroup;

    .line 357
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->k:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 368
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->c()V

    .line 370
    :cond_1
    return-void

    .line 359
    :cond_2
    if-eqz v1, :cond_0

    .line 360
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 363
    const/4 v2, 0x3

    const v3, 0x7f130019

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 364
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 365
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private b(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 392
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/widget/TweetCarouselView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->b:Landroid/content/Context;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 373
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    .line 375
    iput-object v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    .line 377
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->k:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 378
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->k:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 379
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->k:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 380
    iput-object v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->k:Landroid/view/ViewGroup;

    .line 387
    :cond_0
    :goto_0
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bh_()V

    .line 389
    :cond_1
    return-void

    .line 382
    :cond_2
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v1

    .line 383
    if-eqz v1, :cond_0

    .line 384
    iget-object v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/widget/TweetCarouselView;)Lcom/twitter/android/ck;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->m:Lcom/twitter/android/ck;

    return-object v0
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->h:Lcom/twitter/ui/widget/BadgeView;

    iget v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->q:F

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/BadgeView;->setContentSize(F)V

    .line 403
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->h:Lcom/twitter/ui/widget/BadgeView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/ui/widget/BadgeView;->a(ILjava/lang/String;)V

    .line 404
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lbxy;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 156
    if-nez p1, :cond_0

    .line 236
    :goto_0
    return-void

    .line 159
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    .line 161
    const v0, 0x7f11000b

    .line 163
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 161
    invoke-virtual {p2, v6, v0}, Lbxy;->a(ILjava/lang/Object;)V

    .line 164
    const/4 v0, 0x4

    new-instance v1, Lcom/twitter/android/widget/TweetCarouselView$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/TweetCarouselView$1;-><init>(Lcom/twitter/android/widget/TweetCarouselView;)V

    invoke-virtual {p2, v0, v1}, Lbxy;->a(ILjava/lang/Object;)V

    .line 195
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->d:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 196
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->d:Lcom/twitter/media/ui/image/UserImageView;

    new-instance v1, Lcom/twitter/android/widget/TweetCarouselView$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/widget/TweetCarouselView$2;-><init>(Lcom/twitter/android/widget/TweetCarouselView;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->e:Lcom/twitter/ui/widget/TweetHeaderView;

    iget v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->p:F

    iget v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->q:F

    iget v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->q:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/ui/widget/TweetHeaderView;->a(FFF)V

    .line 208
    invoke-static {}, Lcmj;->a()Z

    move-result v4

    .line 209
    invoke-static {}, Lcmj;->b()Z

    move-result v7

    .line 210
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->e:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/twitter/library/view/e;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-boolean v8, p1, Lcom/twitter/model/core/Tweet;->L:Z

    if-eqz v8, :cond_2

    if-eqz v4, :cond_2

    move v4, v6

    :goto_1
    iget-boolean v8, p1, Lcom/twitter/model/core/Tweet;->F:Z

    if-eqz v8, :cond_1

    if-eqz v7, :cond_1

    move v5, v6

    :cond_1
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 214
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->f:Lcom/twitter/library/widget/TextContentView;

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TextContentView;->setTruncateText(Ljava/lang/CharSequence;)V

    .line 216
    invoke-static {p1}, Lcom/twitter/model/util/a;->b(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 217
    invoke-virtual {v0, v6}, Lcom/twitter/model/util/a;->b(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 218
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetCarouselView;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 219
    invoke-static {p1}, Lbwr;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/util/a;->f(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->f:Lcom/twitter/library/widget/TextContentView;

    iget-object v2, v0, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/widget/TweetCarouselView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->o()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/widget/TextContentView;->b(Ljava/lang/CharSequence;Z)V

    .line 224
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetCarouselView;->c()V

    .line 225
    invoke-virtual {p2}, Lbxy;->a()Lcom/twitter/library/widget/renderablecontent/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->j:Lcom/twitter/library/widget/renderablecontent/d;

    .line 226
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetCarouselView;->b()V

    .line 228
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->o:Lcom/twitter/library/revenue/a;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetCarouselView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/library/revenue/a;->a(Lcom/twitter/model/core/Tweet;Landroid/content/res/Resources;)V

    .line 231
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->n:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionBar;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 232
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/InlineActionBar;->setOnInlineActionClickListener(Lcom/twitter/library/widget/InlineActionBar$b;)V

    .line 233
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/InlineActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 235
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetCarouselView;->invalidate()V

    goto/16 :goto_0

    :cond_2
    move v4, v5

    .line 210
    goto :goto_1
.end method

.method public a(Lcom/twitter/model/core/TweetActionType;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 310
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->m:Lcom/twitter/android/ck;

    if-nez v0, :cond_0

    .line 315
    :goto_0
    return-void

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->m:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->n:Lcom/twitter/model/util/FriendshipCache;

    move-object v1, p1

    move-object v5, p0

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;Lcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 330
    if-eqz p1, :cond_0

    .line 331
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 332
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    iget v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    .line 338
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionBar;->b()V

    .line 339
    return-void

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    iput-boolean v2, v0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 335
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    goto :goto_0
.end method

.method public b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 343
    iget-object v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/twitter/model/core/Tweet;->c:Z

    .line 344
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    iget v3, v3, Lcom/twitter/model/core/Tweet;->k:I

    if-eqz p1, :cond_0

    const/4 v1, -0x1

    :cond_0
    add-int/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->k:I

    .line 346
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionBar;->b()V

    .line 347
    return-void

    :cond_1
    move v0, v2

    .line 343
    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionBar;->b()V

    .line 322
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 326
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 408
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 409
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0, p1, p0}, Lcom/twitter/library/widget/InlineActionBar;->a(Landroid/graphics/Canvas;Landroid/view/ViewGroup;)V

    .line 410
    return-void
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 397
    iget-object v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->h:Lcom/twitter/ui/widget/BadgeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/BadgeView;->setVisibility(I)V

    .line 398
    return-void

    .line 397
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final getTweet()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->i:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 138
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 139
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetCarouselView;->c()V

    .line 140
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 114
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 116
    const v0, 0x7f1304b2

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetCarouselView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->c:Landroid/view/ViewGroup;

    .line 118
    const v0, 0x7f13031a

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetCarouselView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->d:Lcom/twitter/media/ui/image/UserImageView;

    .line 120
    const v0, 0x7f130041

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetCarouselView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TweetHeaderView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->e:Lcom/twitter/ui/widget/TweetHeaderView;

    .line 121
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->e:Lcom/twitter/ui/widget/TweetHeaderView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TweetHeaderView;->b(Z)V

    .line 122
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->e:Lcom/twitter/ui/widget/TweetHeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TweetHeaderView;->setShowTimestamp(Z)V

    .line 124
    const v0, 0x7f130019

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetCarouselView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TextContentView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->f:Lcom/twitter/library/widget/TextContentView;

    .line 125
    iget-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->f:Lcom/twitter/library/widget/TextContentView;

    iget v1, p0, Lcom/twitter/android/widget/TweetCarouselView;->p:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TextContentView;->setContentSize(F)V

    .line 127
    const v0, 0x7f1301d8

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetCarouselView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/InlineActionBar;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->g:Lcom/twitter/library/widget/InlineActionBar;

    .line 129
    const v0, 0x7f13051a

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetCarouselView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/BadgeView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->h:Lcom/twitter/ui/widget/BadgeView;

    .line 131
    new-instance v0, Lcom/twitter/library/revenue/a;

    const v1, 0x7f020841

    const v2, 0x7f020840

    const v3, 0x7f020809

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/library/revenue/a;-><init>(Lcom/twitter/library/revenue/a$a;III)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetCarouselView;->o:Lcom/twitter/library/revenue/a;

    .line 134
    return-void
.end method

.method public setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/twitter/android/widget/TweetCarouselView;->n:Lcom/twitter/model/util/FriendshipCache;

    .line 144
    return-void
.end method

.method public setTweetActionsHandler(Lcom/twitter/android/ck;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/twitter/android/widget/TweetCarouselView;->m:Lcom/twitter/android/ck;

    .line 152
    return-void
.end method

.method public setTweetViewClickHandler(Lcom/twitter/android/ct;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/twitter/android/widget/TweetCarouselView;->l:Lcom/twitter/android/ct;

    .line 148
    return-void
.end method
