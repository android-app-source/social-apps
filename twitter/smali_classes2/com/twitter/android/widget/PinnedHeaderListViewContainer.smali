.class public Lcom/twitter/android/widget/PinnedHeaderListViewContainer;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/af;


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private b:Landroid/view/View;

.field private c:Lcom/twitter/android/widget/ad;

.field private final d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->e:I

    .line 51
    sget-object v0, Lcom/twitter/android/bi$a;->PinnedHeaderListViewContainer:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->d:I

    .line 55
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->c:Lcom/twitter/android/widget/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->a:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 93
    iget v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->d:I

    neg-int v0, v0

    if-gt v3, v0, :cond_8

    move v0, v2

    .line 94
    :goto_1
    iget-object v5, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->a:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v0, v5, :cond_8

    .line 95
    iget-object v5, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->a:Landroid/view/ViewGroup;

    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 96
    if-nez v5, :cond_3

    move v0, v3

    .line 105
    :goto_2
    iget-object v3, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->c:Lcom/twitter/android/widget/ad;

    invoke-interface {v3, p1, p2}, Lcom/twitter/android/widget/ad;->a_(II)I

    move-result v3

    .line 106
    iget v5, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->e:I

    if-eq p1, v5, :cond_2

    if-eqz v3, :cond_2

    .line 107
    iget-object v5, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->c:Lcom/twitter/android/widget/ad;

    iget-object v6, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-interface {v5, v6, p1, p2}, Lcom/twitter/android/widget/ad;->a(Landroid/view/View;II)V

    .line 109
    :cond_2
    iput p1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->e:I

    .line 111
    iget-object v5, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTranslationY()F

    move-result v5

    .line 112
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 148
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 98
    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v6

    if-lez v6, :cond_4

    .line 99
    add-int/2addr p1, v0

    .line 100
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_2

    .line 94
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 118
    :pswitch_1
    instance-of v0, v1, Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v0, :cond_5

    move-object v0, v1

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 120
    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getStyle()I

    move-result v0

    if-ne v0, v2, :cond_5

    .line 121
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->d:I

    neg-int v1, v1

    if-lt v0, v1, :cond_5

    .line 122
    :goto_3
    if-eqz v2, :cond_6

    .line 123
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move v2, v4

    .line 121
    goto :goto_3

    .line 126
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 127
    cmpl-float v0, v5, v7

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_0

    .line 133
    :pswitch_2
    iget-object v1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 136
    if-lez v1, :cond_7

    if-ge v0, v1, :cond_7

    .line 138
    sub-int/2addr v0, v1

    .line 142
    :goto_4
    int-to-float v1, v0

    cmpl-float v1, v5, v1

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_0

    :cond_7
    move v0, v4

    .line 140
    goto :goto_4

    :cond_8
    move v0, v3

    goto/16 :goto_2

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 61
    const v0, 0x7f130423

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    return-void
.end method

.method public setAdapter(Lcom/twitter/android/widget/ad;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->c:Lcom/twitter/android/widget/ad;

    .line 68
    return-void
.end method

.method public setListView(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/twitter/android/widget/PinnedHeaderListViewContainer;->a:Landroid/view/ViewGroup;

    .line 73
    return-void
.end method
