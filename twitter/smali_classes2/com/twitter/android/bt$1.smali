.class Lcom/twitter/android/bt$1;
.super Lcom/twitter/library/view/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/bt;->b(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/internal/android/widget/GroupedRowView;

.field final synthetic c:Lcom/twitter/android/widget/PipView;

.field final synthetic d:Lcom/twitter/android/bt;


# direct methods
.method constructor <init>(Lcom/twitter/android/bt;Landroid/view/ViewParent;IILcom/twitter/internal/android/widget/GroupedRowView;Lcom/twitter/android/widget/PipView;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/twitter/android/bt$1;->d:Lcom/twitter/android/bt;

    iput p4, p0, Lcom/twitter/android/bt$1;->a:I

    iput-object p5, p0, Lcom/twitter/android/bt$1;->b:Lcom/twitter/internal/android/widget/GroupedRowView;

    iput-object p6, p0, Lcom/twitter/android/bt$1;->c:Lcom/twitter/android/widget/PipView;

    invoke-direct {p0, p2, p3}, Lcom/twitter/library/view/c;-><init>(Landroid/view/ViewParent;I)V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 4

    .prologue
    .line 375
    iget-object v0, p0, Lcom/twitter/android/bt$1;->d:Lcom/twitter/android/bt;

    invoke-static {v0}, Lcom/twitter/android/bt;->a(Lcom/twitter/android/bt;)Lcom/twitter/android/av;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 376
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 377
    const-string/jumbo v1, "position"

    iget v2, p0, Lcom/twitter/android/bt$1;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 378
    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 379
    iget-object v1, p0, Lcom/twitter/android/bt$1;->d:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->a(Lcom/twitter/android/bt;)Lcom/twitter/android/av;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/bt$1;->b:Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/bt$1;->c:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    .line 382
    invoke-super {p0, p1}, Lcom/twitter/library/view/c;->onPageSelected(I)V

    .line 383
    return-void
.end method
