.class Lcom/twitter/android/ap$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ap;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/ap;


# direct methods
.method constructor <init>(Lcom/twitter/android/ap;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/twitter/android/ap$1;->b:Lcom/twitter/android/ap;

    iput-object p2, p0, Lcom/twitter/android/ap$1;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/twitter/android/ap$1;->a:Landroid/view/View;

    const v1, 0x7f13007e

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 47
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string/jumbo v4, "Instance of TimelineItem is null!"

    invoke-static {v1, v4}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 50
    if-nez v0, :cond_1

    .line 51
    const/4 v1, -0x1

    .line 62
    :goto_1
    iget-object v4, p0, Lcom/twitter/android/ap$1;->b:Lcom/twitter/android/ap;

    invoke-static {v4}, Lcom/twitter/android/ap;->a(Lcom/twitter/android/ap;)Lcom/twitter/android/timeline/be;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/twitter/android/timeline/be;->a(Lcom/twitter/android/timeline/bk;III)V

    .line 64
    return-void

    :cond_0
    move v1, v2

    .line 47
    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v1

    .line 55
    iget v1, v1, Lcom/twitter/android/timeline/bg;->g:I

    move v2, v3

    .line 60
    goto :goto_1
.end method
