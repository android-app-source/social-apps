.class public Lcom/twitter/android/TweetFragment$d;
.super Lcom/twitter/android/ck;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/TweetFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 4

    .prologue
    .line 2503
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$d;->a:Lcom/twitter/android/TweetFragment;

    .line 2504
    new-instance v0, Lcom/twitter/android/ar;

    .line 2505
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/cr;

    invoke-direct {v2}, Lcom/twitter/android/cr;-><init>()V

    .line 2506
    invoke-static {p1}, Lcom/twitter/android/TweetFragment;->N(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/as;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 2504
    invoke-direct {p0, p2, p3, v0}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/ar;)V

    .line 2507
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2538
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$d;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2539
    if-eqz v0, :cond_0

    .line 2540
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2542
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 3

    .prologue
    .line 2530
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/ck;->a(JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V

    .line 2531
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$d;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 2533
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$d;->a()V

    .line 2535
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2514
    invoke-virtual {p0, p2, p3, v0, v0}, Lcom/twitter/android/TweetFragment$d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 2515
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$d;->a()V

    .line 2516
    return-void
.end method

.method protected a(ZZLandroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 2520
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/ck;->a(ZZLandroid/content/Context;Z)V

    .line 2521
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p4, :cond_0

    .line 2524
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$d;->a()V

    .line 2526
    :cond_0
    return-void
.end method
