.class Lcom/twitter/android/cy$b;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/cy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/cy;

.field private final b:J


# direct methods
.method constructor <init>(Lcom/twitter/android/cy;J)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/twitter/android/cy$b;->a:Lcom/twitter/android/cy;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 380
    iput-wide p2, p0, Lcom/twitter/android/cy$b;->b:J

    .line 381
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 376
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/cy$b;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 385
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 386
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/twitter/android/cy$b;->a:Lcom/twitter/android/cy;

    invoke-static {v0}, Lcom/twitter/android/cy;->d(Lcom/twitter/android/cy;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/cy$b;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 388
    iget-object v0, p0, Lcom/twitter/android/cy$b;->a:Lcom/twitter/android/cy;

    invoke-static {v0}, Lcom/twitter/android/cy;->e(Lcom/twitter/android/cy;)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 390
    :cond_0
    return-void
.end method
