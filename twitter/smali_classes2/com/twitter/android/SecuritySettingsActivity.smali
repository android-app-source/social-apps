.class public Lcom/twitter/android/SecuritySettingsActivity;
.super Lcom/twitter/android/client/TwitterPreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/SecuritySettingsActivity$b;,
        Lcom/twitter/android/SecuritySettingsActivity$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/twitter/android/SecuritySettingsActivity$b;

.field private c:Z

.field private e:Z

.field private f:Z

.field private g:Landroid/preference/Preference;

.field private h:Landroid/preference/Preference;

.field private i:Landroid/preference/Preference;

.field private j:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;-><init>()V

    return-void
.end method

.method private static a([I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 788
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget v0, p0, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 468
    invoke-virtual {p0, p1}, Lcom/twitter/android/SecuritySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 469
    return-void
.end method

.method private a(Ljava/lang/String;I[I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 802
    const-string/jumbo v0, "settings:login_verification:"

    .line 803
    invoke-static {p3}, Lcom/twitter/android/SecuritySettingsActivity;->a([I)I

    move-result v1

    .line 804
    const/16 v0, 0x58

    if-ne v1, v0, :cond_0

    .line 805
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "settings:login_verification:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "::rate_limit"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 807
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "settings:login_verification:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "::failure"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 808
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 809
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 810
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 811
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 812
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SecuritySettingsActivity;Lcom/twitter/library/service/s;I)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SecuritySettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/SecuritySettingsActivity;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/twitter/android/SecuritySettingsActivity;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 409
    if-eqz p1, :cond_0

    .line 410
    const v0, 0x7f0a04e2

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;)V

    .line 412
    :cond_0
    new-instance v0, Lbai;

    .line 413
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    .line 414
    invoke-static {p0, v2, v3}, Lbaw;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lbai;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/4 v1, 0x3

    .line 412
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 416
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/SecuritySettingsActivity;Lcom/twitter/library/service/s;I)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SecuritySettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 488
    .line 489
    new-instance v0, Lcom/twitter/android/SecuritySettingsActivity$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/SecuritySettingsActivity$1;-><init>(Lcom/twitter/android/SecuritySettingsActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 339
    const-string/jumbo v1, "login_verification_totp_code"

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "login_verification_totp_generator_enabled"

    .line 340
    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->j:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 343
    :cond_0
    const-string/jumbo v1, "login_verification_generate_code"

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-nez v1, :cond_1

    .line 344
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 346
    :cond_1
    const-string/jumbo v1, "temporary_app_password"

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-nez v1, :cond_2

    .line 347
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->i:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 349
    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/SecuritySettingsActivity;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->g()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method private d(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 524
    .line 525
    new-instance v0, Lcom/twitter/android/SecuritySettingsActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/SecuritySettingsActivity$2;-><init>(Lcom/twitter/android/SecuritySettingsActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 353
    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-static {p0, v2, v3}, Lbaw;->i(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 354
    const-string/jumbo v2, "login_verification_totp_code"

    invoke-virtual {p0, v2}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "login_verification_totp_generator_enabled"

    .line 355
    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 357
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->j:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 359
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 362
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 363
    const-string/jumbo v1, "login_verification_check_requests"

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-nez v1, :cond_0

    .line 364
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->h:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 366
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->c()V

    .line 367
    return-void
.end method

.method static synthetic f(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 371
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->h:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 372
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 373
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->i:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 374
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->j:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 375
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 472
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    .line 473
    const-string/jumbo v0, "native_mobile_sms_2fa_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    new-instance v0, Lbam;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 475
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbam;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x4

    .line 474
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 483
    :goto_0
    return-void

    .line 479
    :cond_0
    new-instance v0, Lcom/twitter/android/SecuritySettingsActivity$a;

    .line 480
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/SecuritySettingsActivity$a;-><init>(Lcom/twitter/android/SecuritySettingsActivity;Landroid/content/Context;Ljava/lang/String;)V

    .line 481
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic h(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method static synthetic i(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method private i()Ljava/lang/String;
    .locals 4

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 816
    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-static {v0, v2, v3}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 817
    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-static {v0, v2, v3}, Lbaw;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 819
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method static synthetic k(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method static synthetic l(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method static synthetic m(Lcom/twitter/android/SecuritySettingsActivity;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    return-wide v0
.end method

.method static synthetic n(Lcom/twitter/android/SecuritySettingsActivity;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    return v0
.end method

.method static synthetic o(Lcom/twitter/android/SecuritySettingsActivity;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->c:Z

    return v0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 877
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 878
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 798
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->removeDialog(I)V

    .line 799
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/16 v7, 0xb

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 159
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 160
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 161
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v1

    iget v2, v1, Lcom/twitter/network/l;->a:I

    .line 163
    packed-switch p2, :pswitch_data_0

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 165
    :pswitch_0
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/twitter/library/network/ab;->a(Landroid/os/Bundle;)[I

    move-result-object v0

    .line 166
    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->a([I)I

    move-result v1

    .line 168
    iput-boolean v5, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    .line 169
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    .line 171
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_2

    .line 172
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:set_enabled_for::success"

    aput-object v2, v1, v5

    .line 173
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 172
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 174
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:enroll::success"

    aput-object v2, v1, v5

    .line 175
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 174
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 176
    const v0, 0x7f0a04e7

    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(I)V

    .line 177
    const-string/jumbo v0, "login_verification"

    .line 178
    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 179
    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 181
    const-string/jumbo v0, "native_mobile_sms_2fa_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->c()V

    .line 189
    :goto_1
    iput-boolean v5, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    .line 190
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/BackupCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "show_welcome"

    .line 191
    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "bc_account_id"

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    .line 192
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 190
    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 186
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->e()V

    goto :goto_1

    .line 193
    :cond_2
    const/16 v3, 0x190

    if-ne v2, v3, :cond_3

    const/16 v3, 0xf7

    if-ne v1, v3, :cond_3

    .line 195
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:set_enabled_for::failure"

    aput-object v2, v1, v5

    .line 196
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 195
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 197
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:enroll::success"

    aput-object v2, v1, v5

    .line 198
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 197
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 199
    const v0, 0x7f0a04e7

    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(I)V

    .line 200
    const-string/jumbo v0, "login_verification"

    .line 201
    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 202
    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 203
    iput-boolean v5, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    .line 204
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 206
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-static {v1, v4, v5}, Lbau;->b(Landroid/content/Context;J)Z

    .line 207
    const-string/jumbo v1, "enroll"

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;I[I)V

    .line 208
    const v0, 0x7f0a04e4

    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(I)V

    goto/16 :goto_0

    .line 213
    :pswitch_1
    iget-object v1, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    invoke-static {v1}, Lcom/twitter/library/network/ab;->a(Landroid/os/Bundle;)[I

    move-result-object v1

    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    .line 215
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 216
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:unenroll::success"

    aput-object v2, v1, v5

    .line 217
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 216
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 218
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-static {v0, v2, v3}, Lbau;->b(Landroid/content/Context;J)Z

    .line 219
    const-string/jumbo v0, "login_verification"

    .line 220
    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 221
    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 222
    iput-boolean v5, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    .line 223
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->f()V

    goto/16 :goto_0

    .line 225
    :cond_4
    const-string/jumbo v0, "unenroll"

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;I[I)V

    .line 226
    const v0, 0x7f0a04e4

    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(I)V

    goto/16 :goto_0

    :pswitch_2
    move-object v1, p1

    .line 231
    check-cast v1, Lbai;

    invoke-virtual {v1}, Lbai;->g()[I

    move-result-object v1

    .line 232
    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->a([I)I

    move-result v3

    .line 233
    iget-boolean v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    if-nez v4, :cond_5

    .line 234
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    .line 236
    :cond_5
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 237
    check-cast p1, Lbai;

    .line 238
    invoke-virtual {p1}, Lbai;->e()Lcom/twitter/model/account/LvEligibilityResponse;

    move-result-object v1

    .line 239
    iget-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    if-nez v0, :cond_6

    .line 240
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "settings:login_verification:eligibility::success"

    aput-object v3, v2, v5

    .line 241
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 240
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 243
    :cond_6
    const-string/jumbo v0, "sms"

    invoke-virtual {v1}, Lcom/twitter/model/account/LvEligibilityResponse;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 244
    const-string/jumbo v0, "native_mobile_sms_2fa_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const-string/jumbo v0, "login_verification"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 247
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 248
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->c()V

    goto/16 :goto_0

    .line 253
    :cond_7
    iget-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    if-eqz v0, :cond_8

    const-string/jumbo v0, "push"

    .line 254
    invoke-virtual {v1}, Lcom/twitter/model/account/LvEligibilityResponse;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 255
    iput-boolean v5, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    .line 258
    :cond_8
    iget-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    if-nez v0, :cond_9

    .line 259
    const-string/jumbo v0, "login_verification"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 260
    const-string/jumbo v2, "push"

    .line 261
    invoke-virtual {v1}, Lcom/twitter/model/account/LvEligibilityResponse;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 263
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 264
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->e()V

    .line 271
    :cond_9
    :goto_2
    invoke-virtual {v1}, Lcom/twitter/model/account/LvEligibilityResponse;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 272
    iput-boolean v6, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    .line 273
    invoke-virtual {p0, v7}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 267
    :cond_a
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 268
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->f()V

    goto :goto_2

    .line 275
    :cond_b
    iput-boolean v5, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    goto/16 :goto_0

    .line 279
    :cond_c
    const-string/jumbo v0, "eligibility"

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;I[I)V

    .line 280
    const-string/jumbo v0, "native_mobile_sms_2fa_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    packed-switch v3, :pswitch_data_1

    .line 300
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-static {v0, v2, v3}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v0

    .line 301
    if-nez v0, :cond_0

    .line 302
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 283
    :pswitch_3
    invoke-virtual {p0, v8}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 289
    :pswitch_4
    invoke-virtual {p0, v7}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 293
    :pswitch_5
    invoke-virtual {p0, v9}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 312
    :pswitch_6
    check-cast p1, Lbam;

    invoke-virtual {p1}, Lbam;->e()[I

    move-result-object v1

    .line 313
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    .line 314
    invoke-static {v0}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/library/service/u;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    if-eqz v1, :cond_d

    const/16 v0, 0xe8

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 317
    invoke-virtual {p0, v8}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 318
    :cond_d
    if-eqz v1, :cond_e

    const/16 v0, 0xe9

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 320
    invoke-virtual {p0, v7}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 321
    :cond_e
    if-eqz v1, :cond_f

    const/16 v0, 0xea

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 323
    invoke-virtual {p0, v9}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 325
    :cond_f
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6
    .end packed-switch

    .line 281
    :pswitch_data_1
    .packed-switch 0xe8
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 792
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 793
    const-string/jumbo v1, "msg"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 795
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    const v0, 0x7f0a088c

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->setTitle(I)V

    .line 104
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "SecuritySettingsActivity_account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Ljava/lang/String;

    .line 105
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "SecuritySettingsActivity_account_id"

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    .line 107
    if-eqz p1, :cond_1

    .line 108
    const-string/jumbo v0, "enrolling"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    .line 113
    :goto_0
    const v0, 0x7f08001e

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->addPreferencesFromResource(I)V

    .line 116
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    .line 115
    invoke-static {v0, v2, v3}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v2

    .line 118
    const-string/jumbo v0, "login_verification"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 119
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    move-object v0, v1

    .line 120
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 121
    const-string/jumbo v0, "native_mobile_sms_2fa_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    const v0, 0x7f0a0842

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 125
    :cond_0
    const-string/jumbo v0, "login_verification_generate_code"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->g:Landroid/preference/Preference;

    .line 126
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->g:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 127
    const-string/jumbo v0, "login_verification_check_requests"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->h:Landroid/preference/Preference;

    .line 128
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->h:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 129
    const-string/jumbo v0, "temporary_app_password"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->i:Landroid/preference/Preference;

    .line 130
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->i:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 131
    const-string/jumbo v0, "login_verification_totp_code"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->j:Landroid/preference/Preference;

    .line 132
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->j:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 134
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->f()V

    .line 136
    new-instance v0, Lcom/twitter/android/SecuritySettingsActivity$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/SecuritySettingsActivity$b;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Lcom/twitter/android/SecuritySettingsActivity$b;

    .line 137
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 138
    sget-object v1, Lcom/twitter/library/platform/notifications/PushRegistration;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 139
    sget-object v1, Lcom/twitter/library/platform/notifications/PushRegistration;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Lcom/twitter/android/SecuritySettingsActivity$b;

    sget-object v2, Lcom/twitter/database/schema/a;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/twitter/android/SecuritySettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 142
    iput-boolean v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->c:Z

    .line 143
    iput-boolean v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    .line 144
    return-void

    .line 110
    :cond_1
    iput-boolean v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10

    .prologue
    const v9, 0x104000a

    const v8, 0x1040009

    const/4 v7, 0x0

    const v5, 0x7f0a04eb

    const v6, 0x7f0a00f6

    .line 550
    .line 552
    new-instance v0, Lcom/twitter/android/SecuritySettingsActivity$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/SecuritySettingsActivity$3;-><init>(Lcom/twitter/android/SecuritySettingsActivity;I)V

    .line 582
    new-instance v1, Lcom/twitter/android/SecuritySettingsActivity$4;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/SecuritySettingsActivity$4;-><init>(Lcom/twitter/android/SecuritySettingsActivity;I)V

    .line 614
    new-instance v2, Lcom/twitter/android/SecuritySettingsActivity$5;

    invoke-direct {v2, p0}, Lcom/twitter/android/SecuritySettingsActivity$5;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    .line 622
    new-instance v3, Lcom/twitter/android/SecuritySettingsActivity$6;

    invoke-direct {v3, p0}, Lcom/twitter/android/SecuritySettingsActivity$6;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    .line 630
    new-instance v4, Lcom/twitter/android/SecuritySettingsActivity$7;

    invoke-direct {v4, p0}, Lcom/twitter/android/SecuritySettingsActivity$7;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    .line 638
    packed-switch p1, :pswitch_data_0

    .line 750
    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 640
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a04e5

    .line 641
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a04e4

    .line 642
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 643
    invoke-virtual {v0, v9, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 644
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 645
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 648
    :pswitch_2
    new-instance v0, Lcom/twitter/android/SecuritySettingsActivity$8;

    invoke-direct {v0, p0}, Lcom/twitter/android/SecuritySettingsActivity$8;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    .line 661
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 662
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v5, 0x7f0a04df

    .line 663
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v5, 0x7f0a0042

    .line 664
    invoke-virtual {v1, v5, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 665
    invoke-virtual {v0, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 666
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 667
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 668
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 672
    :pswitch_3
    new-instance v0, Lcom/twitter/android/SecuritySettingsActivity$9;

    invoke-direct {v0, p0}, Lcom/twitter/android/SecuritySettingsActivity$9;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    .line 682
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 683
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a04ee

    .line 684
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0a04

    .line 685
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 686
    invoke-virtual {v0, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 687
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 688
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 691
    :pswitch_4
    const-string/jumbo v0, "settings:login_verification:switch:ok:click"

    .line 692
    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->c(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    .line 693
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 694
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a04e8

    .line 695
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0948

    .line 696
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 697
    invoke-virtual {v0, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 698
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 699
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 702
    :pswitch_5
    const-string/jumbo v2, "settings:login_verification:enroll:ok:click"

    .line 703
    invoke-direct {p0, v2}, Lcom/twitter/android/SecuritySettingsActivity;->c(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 705
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a04e3

    .line 706
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0b78

    .line 707
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013

    .line 708
    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 709
    invoke-virtual {v2, v8, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 710
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 711
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 715
    :pswitch_6
    const-string/jumbo v2, "settings:login_verification:unenroll:ok:click"

    .line 716
    invoke-direct {p0, v2}, Lcom/twitter/android/SecuritySettingsActivity;->d(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 717
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0276

    .line 718
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0b77

    .line 719
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013

    .line 720
    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 721
    invoke-virtual {v2, v8, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 722
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 723
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 726
    :pswitch_7
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string/jumbo v3, ""

    .line 727
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a04e6

    .line 728
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027

    .line 729
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 730
    invoke-virtual {v2, v9, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 731
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 732
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 735
    :pswitch_8
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a04ed

    .line 736
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a04ec

    .line 737
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0616

    .line 738
    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 739
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 740
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 743
    :pswitch_9
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 744
    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 745
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 746
    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto/16 :goto_0

    .line 638
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_8
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 402
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onDestroy()V

    .line 403
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Lcom/twitter/android/SecuritySettingsActivity$b;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Lcom/twitter/android/SecuritySettingsActivity$b;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 406
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 422
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 424
    const-string/jumbo v3, "login_verification"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 425
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 426
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:::select"

    aput-object v4, v3, v0

    .line 427
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 426
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 429
    iget-boolean v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    if-nez v2, :cond_0

    .line 430
    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    .line 441
    :goto_0
    return v0

    .line 432
    :cond_0
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    .line 435
    :cond_1
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v3, "settings:login_verification:::deselect"

    aput-object v3, v1, v0

    .line 436
    invoke-virtual {v2, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 435
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 437
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 441
    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 446
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 447
    const-string/jumbo v2, "login_verification_generate_code"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 448
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/BackupCodeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "bc_account_id"

    iget-wide v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    .line 449
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    .line 448
    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 464
    :goto_0
    return v0

    .line 451
    :cond_0
    const-string/jumbo v2, "login_verification_check_requests"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/LoginVerificationActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "lv_account_name"

    iget-object v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Ljava/lang/String;

    .line 453
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 452
    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 455
    :cond_1
    const-string/jumbo v2, "temporary_app_password"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 456
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TemporaryAppPasswordActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_id"

    iget-wide v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    .line 457
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    .line 456
    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 459
    :cond_2
    const-string/jumbo v2, "login_verification_totp_code"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 460
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TotpGeneratorActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "TotpGeneratorActivity_account_id"

    iget-wide v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    .line 461
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    .line 460
    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 464
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 759
    sparse-switch p1, :sswitch_data_0

    .line 781
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 785
    :goto_0
    return-void

    .line 761
    :sswitch_0
    check-cast p2, Landroid/app/ProgressDialog;

    .line 762
    const-string/jumbo v0, "msg"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 767
    :sswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:enroll::impression"

    aput-object v2, v1, v4

    .line 768
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 767
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 769
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    goto :goto_0

    .line 774
    :sswitch_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:unenroll::impression"

    aput-object v2, v1, v4

    .line 775
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 774
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 776
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    goto :goto_0

    .line 759
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 380
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onResume()V

    .line 381
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 382
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 383
    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 392
    :goto_0
    return-void

    .line 386
    :cond_0
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/SecuritySettingsActivity;->G:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:::impression"

    aput-object v4, v3, v1

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 387
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 388
    iget-boolean v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    if-nez v2, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->b(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 390
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 396
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 397
    const-string/jumbo v0, "enrolling"

    iget-boolean v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 398
    return-void
.end method
