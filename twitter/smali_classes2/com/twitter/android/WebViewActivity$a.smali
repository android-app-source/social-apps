.class Lcom/twitter/android/WebViewActivity$a;
.super Lcom/twitter/android/client/f$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/WebViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/twitter/android/client/f$a;-><init>()V

    .line 309
    iput-object p1, p0, Lcom/twitter/android/WebViewActivity$a;->a:Landroid/content/Context;

    .line 310
    iput-object p2, p0, Lcom/twitter/android/WebViewActivity$a;->b:Ljava/lang/String;

    .line 311
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/WebViewActivity$a;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 315
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x1

    return v0
.end method
