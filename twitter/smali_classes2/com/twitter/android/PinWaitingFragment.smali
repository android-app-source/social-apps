.class public Lcom/twitter/android/PinWaitingFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/client/u$a;


# instance fields
.field private a:Lcom/twitter/android/bd;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    const v0, 0x7f040447

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 46
    const v0, 0x7f13086e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const v0, 0x7f13086f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    .line 48
    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    invoke-virtual {p0}, Lcom/twitter/android/PinWaitingFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v4

    .line 51
    const-string/jumbo v5, "is_phone100_flow"

    invoke-virtual {v4, v5, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 52
    const v5, 0x7f0a067d

    invoke-virtual {v0, v5}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 53
    const v0, 0x7f13086c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    const v5, 0x7f0a068d

    .line 54
    invoke-virtual {p0, v5}, Lcom/twitter/android/PinWaitingFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/twitter/android/PinWaitingFragment;->a:Lcom/twitter/android/bd;

    .line 55
    invoke-interface {v7}, Lcom/twitter/android/bd;->r()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 54
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 53
    invoke-virtual {v0, v5}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    const v0, 0x7f13086d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    const v5, 0x7f0a0689

    invoke-virtual {v0, v5}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    .line 59
    :cond_0
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v5, p0, Lcom/twitter/android/PinWaitingFragment;->T:Landroid/content/Context;

    new-array v6, v1, [Ljava/lang/String;

    const-string/jumbo v7, "android.permission.RECEIVE_SMS"

    aput-object v7, v6, v2

    invoke-virtual {v0, v5, v6}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    .line 60
    const-string/jumbo v5, "should_intercept_pin"

    invoke-virtual {v4, v5, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/PinWaitingFragment;->b:Z

    .line 62
    return-object v3

    :cond_1
    move v0, v2

    .line 60
    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/PinWaitingFragment;->a:Lcom/twitter/android/bd;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/twitter/android/bd;->a(Ljava/lang/String;I)V

    .line 91
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 29
    iget-boolean v0, p0, Lcom/twitter/android/PinWaitingFragment;->b:Z

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/twitter/android/PinWaitingFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/u;->a(Landroid/content/Context;)Lcom/twitter/android/client/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/u;->a(Lcom/twitter/android/client/u$a;)V

    .line 32
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    .line 68
    check-cast p1, Lcom/twitter/android/bd;

    iput-object p1, p0, Lcom/twitter/android/PinWaitingFragment;->a:Lcom/twitter/android/bd;

    .line 69
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    return-void

    .line 75
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/PinWaitingFragment;->a:Lcom/twitter/android/bd;

    invoke-interface {v0}, Lcom/twitter/android/bd;->o()V

    goto :goto_0

    .line 79
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/PinWaitingFragment;->a:Lcom/twitter/android/bd;

    invoke-interface {v0}, Lcom/twitter/android/bd;->q()V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x7f13086e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/twitter/android/PinWaitingFragment;->b:Z

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/android/PinWaitingFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/u;->a(Landroid/content/Context;)Lcom/twitter/android/client/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/u;->b()V

    .line 39
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 40
    return-void
.end method
