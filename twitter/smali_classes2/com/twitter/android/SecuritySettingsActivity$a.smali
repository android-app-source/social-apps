.class public Lcom/twitter/android/SecuritySettingsActivity$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/SecuritySettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lbax;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/SecuritySettingsActivity;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/twitter/android/SecuritySettingsActivity;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 829
    iput-object p1, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 830
    iput-object p2, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->b:Landroid/content/Context;

    .line 831
    iput-object p3, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->c:Ljava/lang/String;

    .line 832
    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Lbax;
    .locals 4

    .prologue
    .line 841
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->m(Lcom/twitter/android/SecuritySettingsActivity;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lbau;->a(Landroid/content/Context;J)Lbax;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbax;)V
    .locals 5

    .prologue
    .line 846
    if-nez p1, :cond_1

    .line 847
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    .line 848
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "login_verification"

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 849
    check-cast v0, Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 850
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 854
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->n(Lcom/twitter/android/SecuritySettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    new-instance v1, Lbal;

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    .line 856
    invoke-virtual {v3}, Lcom/twitter/android/SecuritySettingsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1}, Lbal;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbax;)V

    const/4 v2, 0x1

    .line 855
    invoke-static {v0, v1, v2}, Lcom/twitter/android/SecuritySettingsActivity;->b(Lcom/twitter/android/SecuritySettingsActivity;Lcom/twitter/library/service/s;I)Z

    goto :goto_0
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 823
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/SecuritySettingsActivity$a;->a([Ljava/lang/Void;)Lbax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 823
    check-cast p1, Lbax;

    invoke-virtual {p0, p1}, Lcom/twitter/android/SecuritySettingsActivity$a;->a(Lbax;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 836
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity$a;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const v2, 0x7f0a04ea

    invoke-virtual {v1, v2}, Lcom/twitter/android/SecuritySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;)V

    .line 837
    return-void
.end method
