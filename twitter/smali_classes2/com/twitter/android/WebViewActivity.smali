.class public Lcom/twitter/android/WebViewActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/WebViewActivity$a;
    }
.end annotation


# instance fields
.field protected a:Landroid/webkit/WebView;

.field protected b:Z

.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/internal/android/widget/ToolBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/WebViewActivity;->b:Z

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x6

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 80
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 81
    const v0, 0x7f04044d

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 83
    return-object p2
.end method

.method a(Lcom/twitter/model/account/OAuthToken;Landroid/net/Uri;ZLjava/util/Map;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/account/OAuthToken;",
            "Landroid/net/Uri;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/ac;->a(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_1

    .line 237
    invoke-static {p0}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/ab;->a(Ljava/net/URI;)Ljava/util/HashMap;

    move-result-object v6

    .line 239
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 240
    new-instance v0, Lcom/twitter/library/network/t;

    invoke-direct {v0, p1}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 241
    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 242
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/ac;->a(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 241
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/network/t;->a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/apache/e;J)Ljava/lang/String;

    move-result-object v0

    .line 243
    const-string/jumbo v1, "Authorization"

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    if-eqz p4, :cond_0

    .line 245
    invoke-interface {v6, p4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    move-object v0, v6

    .line 250
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 4

    .prologue
    .line 255
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    .line 256
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 257
    iput-object v0, p0, Lcom/twitter/android/WebViewActivity;->d:Lcom/twitter/internal/android/widget/ToolBar;

    .line 258
    iget-object v1, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 259
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 260
    const v2, 0x7f0401b6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 262
    new-instance v2, Lazv;

    invoke-direct {v2, v0}, Lazv;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 263
    invoke-virtual {v2, v1}, Lazv;->a(Landroid/view/View;)Lazv;

    .line 264
    const/4 v1, 0x2

    invoke-virtual {v2, v1}, Lazv;->c(I)Lazv;

    .line 265
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 266
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;)V

    .line 269
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 275
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 276
    const v1, 0x7f13088d

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    invoke-virtual {v1, v2}, Lazv;->b(Z)Lazv;

    .line 277
    const v1, 0x7f13088c

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-virtual {v0, v2}, Lazv;->b(Z)Lazv;

    .line 278
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Lcmr;)I

    move-result v0

    return v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/WebViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 99
    invoke-virtual {p0}, Lcom/twitter/android/WebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 101
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 103
    const-string/jumbo v4, "token"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/account/OAuthToken;

    .line 104
    const-string/jumbo v5, "headers"

    .line 105
    invoke-virtual {v0, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 106
    const-string/jumbo v6, "com.twitter.android.EXTRA_POST_PARAMS"

    .line 107
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/HashMap;

    .line 109
    const v0, 0x7f13055f

    invoke-virtual {p0, v0}, Lcom/twitter/android/WebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    .line 110
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v9

    .line 112
    invoke-virtual {v9, v7}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 114
    if-eqz v1, :cond_0

    const-string/jumbo v0, "set_disable_javascript"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v8

    :goto_0
    invoke-virtual {v9, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 115
    invoke-virtual {v9, v7}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 117
    iget-object v9, p0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    new-instance v0, Lcom/twitter/android/WebViewActivity$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/WebViewActivity$1;-><init>(Lcom/twitter/android/WebViewActivity;Landroid/content/res/Resources;Landroid/net/Uri;Lcom/twitter/model/account/OAuthToken;Ljava/util/HashMap;)V

    invoke-virtual {v9, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 189
    if-nez p1, :cond_5

    .line 190
    if-eqz v6, :cond_4

    .line 191
    iput-boolean v8, p0, Lcom/twitter/android/WebViewActivity;->b:Z

    .line 192
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 193
    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 194
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 195
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    new-instance v5, Lcom/twitter/network/apache/message/BasicNameValuePair;

    invoke-direct {v5, v1, v0}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    const-string/jumbo v5, "url"

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 198
    iput-object v0, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move v0, v7

    .line 114
    goto :goto_0

    .line 201
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Lcom/twitter/library/util/af;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 202
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 201
    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/webkit/WebView;Ljava/lang/String;[B)V

    .line 214
    :goto_2
    return-void

    .line 204
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/WebViewActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->i()V

    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/WebViewActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 206
    iput-boolean v7, p0, Lcom/twitter/android/WebViewActivity;->b:Z

    .line 207
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    .line 209
    invoke-static {v2}, Lcom/twitter/android/util/al;->a(Ljava/lang/String;)Z

    move-result v2

    .line 208
    invoke-virtual {p0, v4, v3, v2, v5}, Lcom/twitter/android/WebViewActivity;->a(Lcom/twitter/model/account/OAuthToken;Landroid/net/Uri;ZLjava/util/Map;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_2

    .line 212
    :cond_5
    const-string/jumbo v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    goto :goto_2
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 294
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 295
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 296
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 89
    iget-boolean v0, p0, Lcom/twitter/android/WebViewActivity;->b:Z

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->d:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->i()V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->d:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->h()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 300
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 301
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 302
    const-string/jumbo v0, "url"

    iget-object v1, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public openBrowser(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 283
    new-instance v0, Lcom/twitter/android/WebViewActivity$a;

    iget-object v1, p0, Lcom/twitter/android/WebViewActivity;->c:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/WebViewActivity$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 284
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    invoke-virtual {v1}, Lbaa;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 285
    invoke-static {p0, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/android/client/f;)V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    invoke-interface {v0}, Lcom/twitter/android/client/f;->a()V

    goto :goto_0
.end method
