.class Lcom/twitter/android/LoginActivity$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/v$b;
.implements Lcom/twitter/library/client/v$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/LoginActivity;)V
    .locals 0

    .prologue
    .line 812
    iput-object p1, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813
    return-void
.end method

.method private b(Lcom/twitter/library/client/Session;I[I)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 894
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->f_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 971
    :cond_0
    :goto_0
    return-void

    .line 897
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, v7}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    .line 899
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 901
    if-ne p2, v8, :cond_4

    .line 902
    const v1, 0x7f0a094b

    .line 903
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    iget-boolean v0, v0, Lcom/twitter/android/LoginActivity;->a:Z

    if-eqz v0, :cond_2

    .line 904
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "accountAuthenticatorResponse"

    .line 905
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    .line 907
    if-eqz v0, :cond_2

    .line 908
    const/16 v3, 0x190

    iget-object v6, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v6, v1}, Lcom/twitter/android/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    .line 911
    :cond_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v6, "login::::failure"

    aput-object v6, v3, v2

    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v1

    .line 956
    :goto_1
    if-eqz v0, :cond_3

    .line 957
    iget-object v1, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v1, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 960
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->l(Lcom/twitter/android/LoginActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 961
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0, v2}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;I)I

    .line 962
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v8}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a04d4

    .line 963
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 964
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 965
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 966
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    .line 967
    invoke-virtual {v1}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 968
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v3, "login::forgot_password_prompt::impression"

    aput-object v3, v1, v2

    .line 969
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 968
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 913
    :cond_4
    if-eqz p3, :cond_5

    array-length v0, p3

    if-nez v0, :cond_6

    :cond_5
    move v0, v2

    .line 915
    :goto_2
    sparse-switch v0, :sswitch_data_0

    .line 945
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 946
    const v0, 0x7f0a04cd

    goto :goto_1

    .line 913
    :cond_6
    aget v0, p3, v2

    goto :goto_2

    .line 918
    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    .line 919
    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->f(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 918
    invoke-static {v4, v5, v0}, Lcom/twitter/android/am;->a(JLjava/lang/String;)I

    move-result v0

    .line 921
    iget-object v1, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginActivity;->h(Lcom/twitter/android/LoginActivity;)I

    goto/16 :goto_1

    .line 925
    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->i(Lcom/twitter/android/LoginActivity;)V

    .line 926
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v3, "login:form::identifier:ambiguous"

    aput-object v3, v1, v2

    .line 927
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 931
    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->j(Lcom/twitter/android/LoginActivity;)V

    goto/16 :goto_0

    .line 935
    :sswitch_3
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->k(Lcom/twitter/android/LoginActivity;)V

    goto/16 :goto_0

    .line 939
    :sswitch_4
    const v0, 0x7f0a04d3

    .line 940
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v6, "login:form::identifier:shared_email"

    aput-object v6, v3, v2

    .line 941
    invoke-virtual {v1, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 940
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_1

    .line 948
    :cond_7
    const v0, 0x7f0a04d2

    goto/16 :goto_1

    .line 915
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0xe5 -> :sswitch_1
        0xe7 -> :sswitch_2
        0xf4 -> :sswitch_3
        0x10b -> :sswitch_0
        0x131 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;II[IZ)V
    .locals 2

    .prologue
    .line 992
    invoke-direct {p0, p1, p2, p4}, Lcom/twitter/android/LoginActivity$a;->b(Lcom/twitter/library/client/Session;I[I)V

    .line 993
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;Z)Z

    .line 994
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;I[I)V
    .locals 0

    .prologue
    .line 975
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/LoginActivity$a;->b(Lcom/twitter/library/client/Session;I[I)V

    .line 976
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginVerificationRequiredResponse;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 841
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->f_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 871
    :goto_0
    return-void

    .line 846
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, v5}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    .line 851
    const-string/jumbo v0, "two_factor_challenge_in_web_view_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 852
    const-class v2, Lcom/twitter/android/LoginChallengeActivity;

    .line 853
    const-string/jumbo v1, "login_challenge_required_response"

    .line 854
    const/4 v0, 0x3

    .line 861
    :goto_1
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 862
    invoke-virtual {v3, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "username"

    iget-object v3, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    .line 863
    invoke-static {v3}, Lcom/twitter/android/LoginActivity;->f(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "session_id"

    .line 864
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 865
    iget-object v2, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v2}, Lcom/twitter/android/LoginActivity;->g(Lcom/twitter/android/LoginActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v2}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "android.intent.extra.INTENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 866
    const-string/jumbo v2, "start_main"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 869
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;Z)Z

    .line 870
    iget-object v2, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 856
    :cond_2
    const-class v2, Lcom/twitter/android/VerifyLoginActivity;

    .line 857
    const-string/jumbo v1, "login_verification_required_response"

    .line 858
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 830
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->f_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 836
    :goto_0
    return-void

    .line 834
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    .line 835
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 980
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->f_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 987
    :goto_0
    return-void

    .line 984
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    .line 985
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 986
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;Z)Z

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginVerificationRequiredResponse;)V
    .locals 4

    .prologue
    .line 876
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->f_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 889
    :goto_0
    return-void

    .line 881
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    .line 883
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    const-class v3, Lcom/twitter/android/LoginChallengeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "login_challenge_required_response"

    .line 884
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "username"

    iget-object v3, p0, Lcom/twitter/android/LoginActivity$a;->a:Lcom/twitter/android/LoginActivity;

    .line 886
    invoke-static {v3}, Lcom/twitter/android/LoginActivity;->f(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "session_id"

    .line 887
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x3

    .line 883
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
