.class Lcom/twitter/android/ar$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/aq$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ar;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/ref/WeakReference;

.field final synthetic b:Z

.field final synthetic c:Lcom/twitter/model/core/Tweet;

.field final synthetic d:Lcom/twitter/android/ar;


# direct methods
.method constructor <init>(Lcom/twitter/android/ar;Ljava/lang/ref/WeakReference;ZLcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/twitter/android/ar$3;->d:Lcom/twitter/android/ar;

    iput-object p2, p0, Lcom/twitter/android/ar$3;->a:Ljava/lang/ref/WeakReference;

    iput-boolean p3, p0, Lcom/twitter/android/ar$3;->b:Z

    iput-object p4, p0, Lcom/twitter/android/ar$3;->c:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/twitter/android/ar$3;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 165
    if-eqz v0, :cond_1

    .line 166
    if-eqz p1, :cond_2

    .line 167
    const/4 v1, 0x0

    .line 168
    iget-boolean v2, p0, Lcom/twitter/android/ar$3;->b:Z

    if-eqz v2, :cond_0

    .line 169
    new-instance v1, Lcom/twitter/android/ar$3$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/ar$3$1;-><init>(Lcom/twitter/android/ar$3;Landroid/support/v4/app/FragmentActivity;)V

    .line 176
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ar$3;->d:Lcom/twitter/android/ar;

    invoke-static {v2}, Lcom/twitter/android/ar;->a(Lcom/twitter/android/ar;)Lcom/twitter/android/as;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/twitter/android/as;->b(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V

    .line 181
    :cond_1
    :goto_0
    return-void

    .line 178
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/ar$3;->d:Lcom/twitter/android/ar;

    invoke-static {v1}, Lcom/twitter/android/ar;->a(Lcom/twitter/android/ar;)Lcom/twitter/android/as;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/twitter/android/as;->b(Landroid/app/Activity;)V

    goto :goto_0
.end method
