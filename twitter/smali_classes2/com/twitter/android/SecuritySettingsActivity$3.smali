.class Lcom/twitter/android/SecuritySettingsActivity$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/SecuritySettingsActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/android/SecuritySettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/SecuritySettingsActivity;I)V
    .locals 0

    .prologue
    .line 553
    iput-object p1, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->b:Lcom/twitter/android/SecuritySettingsActivity;

    iput p2, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 556
    iget v0, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 578
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->b:Lcom/twitter/android/SecuritySettingsActivity;

    iget v1, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->a:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->removeDialog(I)V

    .line 579
    return-void

    .line 558
    :sswitch_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->b:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->g(Lcom/twitter/android/SecuritySettingsActivity;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:enroll:cancel:click"

    aput-object v2, v1, v5

    .line 559
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 558
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 563
    :sswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->b:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->h(Lcom/twitter/android/SecuritySettingsActivity;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "settings:login_verification:unenroll:cancel:click"

    aput-object v2, v1, v5

    .line 564
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 563
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 568
    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->b:Lcom/twitter/android/SecuritySettingsActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->b:Lcom/twitter/android/SecuritySettingsActivity;

    const-class v3, Lcom/twitter/android/BackupCodeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "show_welcome"

    .line 570
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "bc_account_id"

    iget-object v3, p0, Lcom/twitter/android/SecuritySettingsActivity$3;->b:Lcom/twitter/android/SecuritySettingsActivity;

    .line 571
    invoke-static {v3}, Lcom/twitter/android/SecuritySettingsActivity;->i(Lcom/twitter/android/SecuritySettingsActivity;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    .line 568
    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 556
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xc -> :sswitch_2
    .end sparse-switch
.end method
