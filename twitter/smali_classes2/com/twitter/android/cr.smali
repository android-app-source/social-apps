.class public Lcom/twitter/android/cr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/as;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/app/Activity;IIILandroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 49
    const v0, 0x7f1302e4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0, v0, p1, p2}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 51
    if-eqz p4, :cond_0

    .line 52
    invoke-virtual {v0, p3, p4}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 54
    :cond_0
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 55
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 26
    const v0, 0x7f0a05af

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 27
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 21
    const v0, 0x7f0a05b2

    const/4 v1, 0x0

    const v2, 0x7f0a05b3

    invoke-static {p1, v0, v1, v2, p2}, Lcom/twitter/android/cr;->a(Landroid/app/Activity;IIILandroid/view/View$OnClickListener;)V

    .line 23
    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 36
    const v0, 0x7f0a09b8

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 37
    return-void
.end method

.method public b(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 31
    const v0, 0x7f0a09b9

    const/4 v1, 0x0

    const v2, 0x7f0a05b3

    invoke-static {p1, v0, v1, v2, p2}, Lcom/twitter/android/cr;->a(Landroid/app/Activity;IIILandroid/view/View$OnClickListener;)V

    .line 33
    return-void
.end method
