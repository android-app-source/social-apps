.class Lcom/twitter/android/cy$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/cy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/cy;


# direct methods
.method private constructor <init>(Lcom/twitter/android/cy;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/twitter/android/cy$d;->a:Lcom/twitter/android/cy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/cy;Lcom/twitter/android/cy$1;)V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0, p1}, Lcom/twitter/android/cy$d;-><init>(Lcom/twitter/android/cy;)V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 339
    iget-object v0, p0, Lcom/twitter/android/cy$d;->a:Lcom/twitter/android/cy;

    invoke-static {v0}, Lcom/twitter/android/cy;->a(Lcom/twitter/android/cy;)Lcom/twitter/library/widget/SlidingPanel;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lcom/twitter/android/cy$d;->a:Lcom/twitter/android/cy;

    invoke-static {v1}, Lcom/twitter/android/cy;->b(Lcom/twitter/android/cy;)Landroid/widget/ListView;

    move-result-object v1

    .line 341
    iget-object v2, p0, Lcom/twitter/android/cy$d;->a:Lcom/twitter/android/cy;

    .line 342
    invoke-static {v2}, Lcom/twitter/android/cy;->c(Lcom/twitter/android/cy;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 345
    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getHeader()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 347
    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 348
    add-int/2addr v1, v3

    .line 349
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setPanelPreviewHeight(I)V

    .line 350
    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->b()Z

    .line 351
    iget-object v1, p0, Lcom/twitter/android/cy$d;->a:Lcom/twitter/android/cy;

    iput-boolean v6, v1, Lcom/twitter/android/cy;->b:Z

    .line 354
    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 355
    return v6
.end method
