.class public Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;
.super Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;
.source "Twttr"


# instance fields
.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/ui/widget/TwitterButton;",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

.field private o:Lcom/twitter/android/card/b;

.field private p:Landroid/view/View;

.field private q:Lcom/twitter/android/card/d;

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;-><init>(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method private F()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const v0, 0x7f13011b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 125
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    sget-object v1, Lcom/twitter/android/revenue/card/h;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 127
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    :cond_0
    new-instance v4, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView$1;

    invoke-direct {v4, p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView$1;-><init>(Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;)V

    .line 137
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 138
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->m:Ljava/util/Map;

    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->j:Landroid/view/View;

    const v1, 0x7f130853

    .line 140
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    const/4 v1, 0x3

    new-array v2, v1, [Lcom/twitter/ui/widget/TwitterButton;

    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->j:Landroid/view/View;

    const v6, 0x7f130854

    .line 141
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/TwitterButton;

    aput-object v1, v2, v3

    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->j:Landroid/view/View;

    const v6, 0x7f130855

    .line 142
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/TwitterButton;

    aput-object v1, v2, v10

    const/4 v6, 0x2

    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->j:Landroid/view/View;

    const v7, 0x7f130856

    .line 143
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/TwitterButton;

    aput-object v1, v2, v6

    .line 139
    invoke-static {v0, v2}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 144
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v7

    move v2, v3

    .line 145
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 146
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    .line 147
    new-instance v1, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView$2;-><init>(Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 153
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 155
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 156
    const v8, 0x7f0a0212

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v1, v9, v3

    invoke-virtual {v5, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 157
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v1, v8, v9}, Lcom/twitter/android/revenue/i;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/text/Spannable;

    move-result-object v1

    .line 158
    sget-object v8, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v8}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 159
    invoke-virtual {v7, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 161
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->m:Ljava/util/Map;

    iget-object v8, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v8, v8, Lcom/twitter/android/av/revenue/VideoConversationCardData;->b:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v8

    invoke-interface {v1, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 163
    :cond_1
    invoke-virtual {v7}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->s:Ljava/util/List;

    .line 164
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    const v1, 0x7f0a06c0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 165
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 166
    return-void
.end method

.method private G()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 182
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->a(Ljava/lang/String;I)V

    .line 187
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->H()V

    goto :goto_0
.end method

.method private H()V
    .locals 3

    .prologue
    .line 190
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0213

    .line 191
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->s:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->s:Ljava/util/List;

    .line 192
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v2, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView$3;-><init>(Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 200
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;)Lcom/twitter/android/card/d;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->q:Lcom/twitter/android/card/d;

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->p:Landroid/view/View;

    const/4 v1, 0x0

    .line 170
    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->q:Lcom/twitter/android/card/d;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->o:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 175
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->a(Ljava/lang/String;I)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->G()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;)Lcom/twitter/android/av/revenue/VideoConversationCardData;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    return-object v0
.end method


# virtual methods
.method protected D()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method protected E()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public a(Lcom/twitter/android/av/revenue/VideoConversationPlayerCanvasView;Lcom/twitter/android/av/revenue/VideoConversationCardData;Lcom/twitter/android/card/b;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 83
    iput-object p1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->p:Landroid/view/View;

    .line 84
    iput-object p2, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    .line 85
    iput-object p3, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->o:Lcom/twitter/android/card/b;

    .line 86
    new-instance v0, Lcom/twitter/android/card/f;

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->q:Lcom/twitter/android/card/d;

    .line 87
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->q:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->h:Lcom/twitter/model/core/Tweet;

    invoke-static {v1}, Lcom/twitter/library/card/CardContextFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/card/CardContext;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/card/CardContext;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-boolean v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->i:Z

    iput-boolean v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->k:Z

    .line 89
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-boolean v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->i:Z

    iput-boolean v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->l:Z

    .line 90
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->c:Z

    if-eqz v0, :cond_3

    .line 94
    invoke-super {p0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->B()V

    .line 98
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->h:Lcom/twitter/model/core/Tweet;

    .line 100
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    const v0, 0x7f13058b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    .line 102
    iget-object v2, v1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 104
    const v0, 0x7f130848

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 105
    iget-object v2, v1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    const v0, 0x7f130849

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 108
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    const v0, 0x7f130847

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 111
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 114
    :cond_1
    const v0, 0x7f13084a

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 115
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    const v0, 0x7f13084b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 119
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->F()V

    goto/16 :goto_0

    .line 96
    :cond_3
    invoke-super {p0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->A()V

    goto/16 :goto_1
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 203
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->h:Lcom/twitter/model/core/Tweet;

    .line 204
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->o:Lcom/twitter/android/card/b;

    iget-object v2, p0, Lcom/twitter/android/av/FullscreenConversationCardCanvasChromeView;->n:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-wide v2, v2, Lcom/twitter/android/av/revenue/VideoConversationCardData;->g:J

    .line 205
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v4

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v5

    move-object v1, p1

    move v6, p2

    .line 204
    invoke-interface/range {v0 .. v6}, Lcom/twitter/android/card/b;->a(Ljava/lang/String;JLcax;Lcgi;I)V

    .line 206
    return-void
.end method

.method protected b(Landroid/content/Context;)Lcom/twitter/ui/widget/TwitterButton;
    .locals 3

    .prologue
    .line 72
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040439

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    return-object v0
.end method

.method protected c(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 78
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04043f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
