.class public Lcom/twitter/android/av/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/m$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/android/av/m;


# instance fields
.field private final b:Lcom/twitter/android/av/m$a;


# direct methods
.method private constructor <init>(Lcom/twitter/android/av/m$a;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/twitter/android/av/m;->b:Lcom/twitter/android/av/m$a;

    .line 72
    return-void
.end method

.method public static declared-synchronized a()I
    .locals 4

    .prologue
    .line 56
    const-class v1, Lcom/twitter/android/av/m;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/av/m;->a:Lcom/twitter/android/av/m;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lcpb;

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "AutoPlayPreferences#getAutoPlayEnabled called before initialize"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcpd;->c(Lcpb;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    const/4 v0, 0x0

    .line 61
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/twitter/android/av/m;->a:Lcom/twitter/android/av/m;

    invoke-virtual {v0}, Lcom/twitter/android/av/m;->b()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Lcom/twitter/android/av/m$a;)V
    .locals 2

    .prologue
    .line 38
    const-class v1, Lcom/twitter/android/av/m;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/twitter/android/av/m;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/m;-><init>(Lcom/twitter/android/av/m$a;)V

    sput-object v0, Lcom/twitter/android/av/m;->a:Lcom/twitter/android/av/m;

    .line 39
    const-class v0, Lcom/twitter/android/av/m;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit v1

    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public b()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/av/m;->b:Lcom/twitter/android/av/m$a;

    invoke-interface {v0}, Lcom/twitter/android/av/m$a;->a()Z

    move-result v0

    return v0
.end method
