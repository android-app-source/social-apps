.class public abstract Lcom/twitter/android/av/AVCardCanvasActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Lcom/twitter/android/av/ExternalActionButton$a;
.implements Lcom/twitter/android/widget/ExpandableViewHost$a;
.implements Lcom/twitter/library/av/playback/AVPlayer$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/AVCardCanvasActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TCardCanvasView::",
        "Lcom/twitter/android/av/c;",
        ">",
        "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Lcom/twitter/android/av/ExternalActionButton$a;",
        "Lcom/twitter/android/widget/ExpandableViewHost$a;",
        "Lcom/twitter/library/av/playback/AVPlayer$b;"
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/android/av/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTCardCanvasView;"
        }
    .end annotation
.end field

.field protected b:Z

.field protected c:Lcom/twitter/android/widget/ExpandableViewHost;

.field protected d:Lcom/twitter/android/av/TweetEngagementView;

.field protected e:Lcom/twitter/model/core/Tweet;

.field protected f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected g:Landroid/graphics/PointF;

.field protected h:Landroid/graphics/PointF;

.field protected i:Landroid/graphics/PointF;

.field protected j:Landroid/graphics/PointF;

.field protected k:Lcom/twitter/library/av/playback/AVDataSource;

.field protected l:Lcom/twitter/library/av/playback/u;

.field protected m:Ljava/lang/String;

.field protected n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field protected o:Lcom/twitter/library/av/playback/AVPlayer;

.field protected final p:Lcom/twitter/library/av/playback/q;

.field private final q:Lcom/twitter/library/av/playback/p;

.field private r:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 88
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->p:Lcom/twitter/library/av/playback/q;

    .line 89
    new-instance v0, Lcom/twitter/library/av/playback/p;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/p;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->q:Lcom/twitter/library/av/playback/p;

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->r:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/AVCardCanvasActivity;)V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    return-void
.end method

.method public static b(Landroid/os/Bundle;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 2

    .prologue
    .line 211
    const-string/jumbo v0, "association"

    .line 212
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 213
    if-nez v0, :cond_0

    .line 214
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 216
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 96
    invoke-virtual {p0, v2, v2}, Lcom/twitter/android/av/AVCardCanvasActivity;->overridePendingTransition(II)V

    .line 97
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 100
    :cond_0
    return-object v0
.end method

.method protected abstract a(Landroid/os/Bundle;)Lcom/twitter/library/av/playback/u;
.end method

.method protected a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 283
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 289
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/TweetEngagementView;->setVisibility(I)V

    .line 297
    :goto_0
    return-void

    .line 285
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/TweetEngagementView;->setVisibility(I)V

    goto :goto_0

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/TweetEngagementView;->setVisibility(I)V

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 107
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 108
    const-string/jumbo v0, "av_data_source"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVDataSource;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->k:Lcom/twitter/library/av/playback/AVDataSource;

    .line 109
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->k:Lcom/twitter/library/av/playback/AVDataSource;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->k:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    .line 116
    :goto_0
    const-string/jumbo v0, "initial_top_left_coords"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->g:Landroid/graphics/PointF;

    .line 117
    const-string/jumbo v0, "initial_size"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->h:Landroid/graphics/PointF;

    .line 118
    const-string/jumbo v0, "return_top_left_coords"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->i:Landroid/graphics/PointF;

    .line 119
    const-string/jumbo v0, "return_size"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->j:Landroid/graphics/PointF;

    .line 120
    const-string/jumbo v0, "media_source_url"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->m:Ljava/lang/String;

    .line 122
    const v0, 0x7f130190

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/TweetEngagementView;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    .line 123
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/content/res/Configuration;)V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/TweetEngagementView;->setContext(Landroid/content/Context;)V

    .line 126
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/TweetEngagementView;->setFragmentActivity(Lcom/twitter/app/common/base/TwitterFragmentActivity;)V

    .line 128
    invoke-static {v1}, Lcom/twitter/android/av/AVCardCanvasActivity;->b(Landroid/os/Bundle;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 129
    invoke-virtual {p0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/os/Bundle;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->l:Lcom/twitter/library/av/playback/u;

    .line 130
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->l:Lcom/twitter/library/av/playback/u;

    if-nez v0, :cond_1

    .line 165
    :goto_1
    return-void

    .line 113
    :cond_0
    const-string/jumbo v0, "tweet"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    goto :goto_0

    .line 133
    :cond_1
    new-instance v0, Lcom/twitter/library/av/playback/r;

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->p:Lcom/twitter/library/av/playback/q;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/r;-><init>(Lcom/twitter/library/av/playback/q;)V

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->l:Lcom/twitter/library/av/playback/u;

    .line 134
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 135
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->l()Lbyf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lbyf;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 137
    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/r;->a(Landroid/content/Context;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    const/4 v1, 0x1

    .line 138
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->b(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    const/4 v1, 0x0

    .line 139
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/r;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 141
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    .line 143
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/av/TweetEngagementView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVPlayer;Ljava/lang/String;)V

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 149
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->p:Lcom/twitter/library/av/playback/q;

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 150
    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v1

    .line 149
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/u;)V

    .line 153
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/client/v;)V

    .line 155
    const v0, 0x7f13018f

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/c;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    .line 156
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->b()V

    .line 157
    const v0, 0x7f13018e

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ExpandableViewHost;

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    .line 158
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/ExpandableViewHost;->setListener(Lcom/twitter/android/widget/ExpandableViewHost$a;)V

    .line 159
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 161
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->j()V

    goto/16 :goto_1

    .line 163
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->i()V

    goto/16 :goto_1
.end method

.method public a(Lcom/twitter/android/widget/ExpandableViewHost;)V
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->r:I

    .line 303
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->finish()V

    .line 304
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 0

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->finish()V

    .line 356
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 369
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    if-nez v0, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->j()Landroid/os/Bundle;

    move-result-object v0

    .line 376
    const-string/jumbo v1, "impression_scribed"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 377
    const-string/jumbo v2, "cta_availability"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 380
    if-eqz v1, :cond_2

    if-eq v2, p1, :cond_0

    .line 381
    :cond_2
    const-string/jumbo v1, "impression_scribed"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 382
    const-string/jumbo v1, "cta_availability"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 384
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 385
    if-eqz v1, :cond_0

    .line 386
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v2

    if-eqz p1, :cond_3

    new-instance v0, Lbjf;

    invoke-direct {v0, v1}, Lbjf;-><init>(Lcom/twitter/model/av/AVMedia;)V

    :goto_1
    invoke-virtual {v2, v0}, Lbix;->a(Lbiw;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lbjg;

    invoke-direct {v0, v1}, Lbjg;-><init>(Lcom/twitter/model/av/AVMedia;)V

    goto :goto_1
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public b(Lcom/twitter/android/widget/ExpandableViewHost;)V
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->r:I

    .line 309
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->finish()V

    .line 310
    return-void
.end method

.method public b(Z)V
    .locals 3

    .prologue
    .line 397
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 399
    if-eqz v1, :cond_0

    .line 400
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v2

    if-eqz p1, :cond_1

    new-instance v0, Lbje;

    invoke-direct {v0, v1}, Lbje;-><init>(Lcom/twitter/model/av/AVMedia;)V

    :goto_0
    invoke-virtual {v2, v0}, Lbix;->a(Lbiw;)V

    .line 404
    :cond_0
    return-void

    .line 400
    :cond_1
    new-instance v0, Lbjd;

    invoke-direct {v0, v1}, Lbjd;-><init>(Lcom/twitter/model/av/AVMedia;)V

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 257
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 258
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->q:Lcom/twitter/library/av/playback/p;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    iget-object v3, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->p:Lcom/twitter/library/av/playback/q;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/p;->a(ZLcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/playback/q;)V

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    .line 260
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    invoke-virtual {v0}, Lcom/twitter/android/av/TweetEngagementView;->a()V

    .line 263
    :cond_0
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 325
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->b:Z

    if-nez v0, :cond_2

    .line 326
    new-instance v0, Lcom/twitter/android/av/AVCardCanvasActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/AVCardCanvasActivity$1;-><init>(Lcom/twitter/android/av/AVCardCanvasActivity;)V

    .line 333
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    if-eqz v1, :cond_0

    .line 334
    iget v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->r:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 335
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ExpandableViewHost;->a(Ljava/lang/Runnable;)V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ExpandableViewHost;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 341
    :cond_2
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    goto :goto_0
.end method

.method protected abstract h()Ljava/lang/String;
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVPlayer$b;)V

    .line 190
    :cond_0
    return-void
.end method

.method protected j()V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    if-nez v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->m()V

    .line 202
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/av/c;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;Landroid/content/res/Configuration;)V

    .line 204
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->l:Lcom/twitter/library/av/playback/u;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVDataSource;->j()Lcom/twitter/model/av/Partner;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/twitter/android/av/c;->setPartner(Lcom/twitter/model/av/Partner;)V

    .line 205
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    invoke-interface {v1}, Lcom/twitter/android/av/c;->getContentView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 206
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected l()Lbyf;
    .locals 1

    .prologue
    .line 252
    sget-object v0, Lbyo;->c:Lbyf;

    return-object v0
.end method

.method public n()V
    .locals 0

    .prologue
    .line 350
    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->j()V

    .line 351
    return-void
.end method

.method public o()V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->r:I

    .line 315
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 316
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 267
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 268
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/content/res/Configuration;)V

    .line 269
    return-void
.end method

.method public onGlobalLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 221
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ExpandableViewHost;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->h:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->g:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->g:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->h:Landroid/graphics/PointF;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/widget/ExpandableViewHost;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Ljava/lang/Runnable;)V

    .line 228
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ExpandableViewHost;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 229
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/ExpandableViewHost;->c(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 233
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 234
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 237
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 241
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 242
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->j()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 245
    :cond_0
    return-void
.end method
