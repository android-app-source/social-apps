.class public abstract Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;
.super Lcom/twitter/android/av/BaseVideoPlayerChromeView;
.source "Twttr"


# instance fields
.field protected i:Lcom/twitter/ui/widget/TwitterButton;

.field protected j:Landroid/view/View;

.field protected k:Z

.field protected l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;-><init>(Landroid/content/Context;)V

    .line 20
    iput-boolean v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->k:Z

    .line 21
    iput-boolean v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->l:Z

    .line 25
    return-void
.end method


# virtual methods
.method protected A()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 89
    invoke-super {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->A()V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 91
    iget-boolean v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->l:Z

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 97
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :cond_1
    return-void

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0
.end method

.method protected B()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 104
    invoke-super {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->B()V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 106
    iget-boolean v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->k:Z

    if-eqz v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 112
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 115
    :cond_1
    return-void

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected abstract D()V
.end method

.method protected abstract E()V
.end method

.method public a()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a()V

    .line 152
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->B()V

    .line 153
    return-void
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a(Lcom/twitter/model/av/AVMedia;)V

    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->p()V

    .line 85
    return-void
.end method

.method protected abstract b(Landroid/content/Context;)Lcom/twitter/ui/widget/TwitterButton;
.end method

.method protected abstract c(Landroid/content/Context;)Landroid/view/View;
.end method

.method public i()V
    .locals 0

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->u()V

    .line 120
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->addView(Landroid/view/View;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->addView(Landroid/view/View;)V

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 65
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->addView(Landroid/view/View;)V

    .line 67
    :cond_2
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 132
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->onLayout(ZIIII)V

    .line 133
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    iget-object v1, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v1}, Lcom/twitter/library/av/control/VideoControlView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, p5, v1

    invoke-virtual {v0, p2, v1, p4, p5}, Lcom/twitter/library/av/control/VideoControlView;->layout(IIII)V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->e:Lcom/twitter/android/av/ab;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->e:Lcom/twitter/android/av/ab;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/av/ab;->a(ZIIII)V

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 140
    add-int v0, p4, p2

    shr-int/lit8 v0, v0, 0x1

    .line 141
    add-int v1, p5, p3

    shr-int/lit8 v1, v1, 0x1

    .line 142
    iget-object v2, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    shr-int/lit8 v2, v2, 0x1

    .line 143
    iget-object v3, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    .line 144
    iget-object v4, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    sub-int v5, v0, v2

    sub-int v6, v1, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 147
    :cond_2
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->p()V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->l:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 128
    :cond_0
    return-void
.end method

.method protected setupInternalViews(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->a(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/control/VideoControlView;->setListener(Lcom/twitter/library/av/control/VideoControlView$a;)V

    .line 39
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/control/VideoControlView;->setIsFullScreenToggleAllowed(Z)V

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    if-nez v0, :cond_2

    .line 42
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->b(Landroid/content/Context;)Lcom/twitter/ui/widget/TwitterButton;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->i:Lcom/twitter/ui/widget/TwitterButton;

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    if-nez v0, :cond_3

    .line 45
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->c(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->j:Landroid/view/View;

    .line 48
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->D()V

    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenRevenueCardCanvasChromeView;->E()V

    .line 50
    return-void
.end method
