.class public abstract Lcom/twitter/android/av/AVCardCanvasActivity$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/AVCardCanvasActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/twitter/model/core/Tweet;

.field private b:Lcom/twitter/library/av/playback/AVDataSource;

.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private e:Landroid/graphics/PointF;

.field private f:Landroid/graphics/PointF;

.field private g:Landroid/graphics/PointF;

.field private h:Landroid/graphics/PointF;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a:Lcom/twitter/model/core/Tweet;

    .line 411
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 412
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->c:Ljava/lang/String;

    .line 413
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 414
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->e:Landroid/graphics/PointF;

    .line 415
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->f:Landroid/graphics/PointF;

    .line 416
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->g:Landroid/graphics/PointF;

    .line 417
    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->h:Landroid/graphics/PointF;

    .line 418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->i:Z

    return-void
.end method

.method private c(Landroid/content/Context;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 508
    new-instance v0, Lcom/twitter/android/av/AVCardCanvasActivity$a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity$a$1;-><init>(Lcom/twitter/android/av/AVCardCanvasActivity$a;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 474
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 475
    const-string/jumbo v1, "tweet"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 476
    const-string/jumbo v1, "av_data_source"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->b:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 477
    const-string/jumbo v1, "media_source_url"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v1, :cond_0

    .line 480
    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 483
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->e:Landroid/graphics/PointF;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->f:Landroid/graphics/PointF;

    if-eqz v1, :cond_1

    .line 484
    const-string/jumbo v1, "initial_top_left_coords"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->e:Landroid/graphics/PointF;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 485
    const-string/jumbo v1, "initial_size"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->f:Landroid/graphics/PointF;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 488
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->h:Landroid/graphics/PointF;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->g:Landroid/graphics/PointF;

    if-eqz v1, :cond_2

    .line 489
    const-string/jumbo v1, "return_top_left_coords"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->g:Landroid/graphics/PointF;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 490
    const-string/jumbo v1, "return_size"

    iget-object v2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->h:Landroid/graphics/PointF;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 493
    :cond_2
    return-object v0
.end method

.method public a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Lcom/twitter/android/av/AVCardCanvasActivity$a;
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->e:Landroid/graphics/PointF;

    .line 444
    iput-object p2, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->f:Landroid/graphics/PointF;

    .line 445
    return-object p0
.end method

.method public a(Landroid/view/View;)Lcom/twitter/android/av/AVCardCanvasActivity$a;
    .locals 4

    .prologue
    .line 449
    if-eqz p1, :cond_0

    .line 452
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 453
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 454
    new-instance v1, Landroid/graphics/PointF;

    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-float v2, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    int-to-float v0, v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->e:Landroid/graphics/PointF;

    .line 455
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->f:Landroid/graphics/PointF;

    .line 457
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/av/AVCardCanvasActivity$a;
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 438
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/android/av/AVCardCanvasActivity$a;
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 427
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/av/AVCardCanvasActivity$a;
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a:Lcom/twitter/model/core/Tweet;

    .line 422
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/av/AVCardCanvasActivity$a;
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->c:Ljava/lang/String;

    .line 432
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/av/AVCardCanvasActivity$a;
    .locals 0

    .prologue
    .line 469
    iput-boolean p1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->i:Z

    .line 470
    return-object p0
.end method

.method protected abstract a()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/android/av/AVCardCanvasActivity;",
            ">;"
        }
    .end annotation
.end method

.method public b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 499
    invoke-direct {p0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->c(Landroid/content/Context;)Ljava/lang/Runnable;

    move-result-object v0

    .line 500
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/av/AVCardCanvasActivity$a;->i:Z

    if-eqz v1, :cond_0

    .line 501
    invoke-static {}, Lcom/twitter/android/client/OpenUriHelper;->a()Lcom/twitter/android/client/OpenUriHelper;

    move-result-object v1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 505
    :goto_0
    return-void

    .line 503
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
