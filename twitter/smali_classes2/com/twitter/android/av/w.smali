.class public Lcom/twitter/android/av/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/w$b;,
        Lcom/twitter/android/av/w$c;,
        Lcom/twitter/android/av/w$a;
    }
.end annotation


# instance fields
.field a:Lcom/twitter/model/core/Tweet;

.field private final b:Lcom/twitter/android/widget/EngagementActionBar;

.field private final c:Lcom/twitter/library/client/Session;

.field private d:Landroid/content/Context;

.field private final e:Lcom/twitter/android/widget/ToggleImageButton;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Lcom/twitter/android/av/w$a;

.field private final j:Lcom/twitter/android/av/w$c;

.field private final k:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/support/v4/app/FragmentActivity;

.field private m:Lbpl;

.field private n:Lcom/twitter/android/av/v;

.field private final o:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;)V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/twitter/util/p;

    invoke-direct {v0}, Lcom/twitter/util/p;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/av/w;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lcom/twitter/util/p;Lcom/twitter/android/av/v;)V

    .line 77
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/util/p;Lcom/twitter/android/av/v;Lcom/twitter/android/av/w$a;Lcom/twitter/android/av/w$c;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/android/widget/EngagementActionBar;",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;",
            "Lcom/twitter/android/av/v;",
            "Lcom/twitter/android/av/w$a;",
            "Lcom/twitter/android/av/w$c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/twitter/android/av/w$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/w$1;-><init>(Lcom/twitter/android/av/w;)V

    iput-object v0, p0, Lcom/twitter/android/av/w;->o:Lcom/twitter/util/q;

    .line 110
    iput-object p1, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    .line 111
    iput-object p2, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    .line 112
    iput-object p5, p0, Lcom/twitter/android/av/w;->k:Lcom/twitter/util/p;

    .line 113
    iget-object v0, p0, Lcom/twitter/android/av/w;->k:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/av/w;->o:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 114
    iput-object p6, p0, Lcom/twitter/android/av/w;->n:Lcom/twitter/android/av/v;

    .line 115
    iput-object p7, p0, Lcom/twitter/android/av/w;->i:Lcom/twitter/android/av/w$a;

    .line 116
    iput-object p8, p0, Lcom/twitter/android/av/w;->j:Lcom/twitter/android/av/w$c;

    .line 117
    iput-object p3, p0, Lcom/twitter/android/av/w;->d:Landroid/content/Context;

    .line 118
    iput-object p4, p0, Lcom/twitter/android/av/w;->c:Lcom/twitter/library/client/Session;

    .line 119
    iget-object v0, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    const v1, 0x7f13006f

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/w;->f:Landroid/widget/TextView;

    .line 120
    iget-object v0, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    const v1, 0x7f130033

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/w;->g:Landroid/widget/TextView;

    .line 121
    iget-object v0, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0}, Lcom/twitter/android/widget/EngagementActionBar;->getFavoriteButton()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ToggleImageButton;

    iput-object v0, p0, Lcom/twitter/android/av/w;->e:Lcom/twitter/android/widget/ToggleImageButton;

    .line 122
    iget-object v0, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    const v1, 0x7f130072

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/w;->h:Landroid/widget/TextView;

    .line 123
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lcom/twitter/android/av/v;)V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/twitter/util/p;

    invoke-direct {v0}, Lcom/twitter/util/p;-><init>()V

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/android/av/w;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lcom/twitter/util/p;Lcom/twitter/android/av/v;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lcom/twitter/android/av/v;Lbpl;)V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/twitter/util/p;

    invoke-direct {v0}, Lcom/twitter/util/p;-><init>()V

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/android/av/w;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lcom/twitter/util/p;Lcom/twitter/android/av/v;)V

    .line 97
    iput-object p4, p0, Lcom/twitter/android/av/w;->m:Lbpl;

    .line 98
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lcom/twitter/util/p;Lcom/twitter/android/av/v;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/android/widget/EngagementActionBar;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;",
            "Lcom/twitter/android/av/v;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p2}, Lcom/twitter/android/widget/EngagementActionBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v4

    new-instance v7, Lcom/twitter/android/av/w$a;

    invoke-direct {v7, p3, p4}, Lcom/twitter/android/av/w$a;-><init>(Lcom/twitter/util/p;Lcom/twitter/android/av/v;)V

    new-instance v8, Lcom/twitter/android/av/w$c;

    invoke-direct {v8}, Lcom/twitter/android/av/w$c;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/av/w;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/util/p;Lcom/twitter/android/av/v;Lcom/twitter/android/av/w$a;Lcom/twitter/android/av/w$c;)V

    .line 104
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 8

    .prologue
    const v1, 0x7f110195

    .line 212
    iget-object v0, p0, Lcom/twitter/android/av/w;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 213
    iget-object v0, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    iget v0, v0, Lcom/twitter/model/core/Tweet;->n:I

    .line 214
    iget-object v3, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    iget v3, v3, Lcom/twitter/model/core/Tweet;->k:I

    .line 215
    iget-object v4, p0, Lcom/twitter/android/av/w;->f:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-static {v4, v5}, Lcom/twitter/library/view/e;->a(Landroid/widget/TextView;Lcom/twitter/model/core/Tweet;)V

    .line 216
    iget-object v4, p0, Lcom/twitter/android/av/w;->g:Landroid/widget/TextView;

    if-lez v0, :cond_1

    int-to-long v6, v0

    .line 217
    invoke-static {v2, v6, v7}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 216
    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v4, p0, Lcom/twitter/android/av/w;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->a:Z

    if-eqz v0, :cond_2

    const v0, 0x7f1100e4

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 220
    iget-object v4, p0, Lcom/twitter/android/av/w;->h:Landroid/widget/TextView;

    if-lez v3, :cond_3

    int-to-long v6, v3

    .line 221
    invoke-static {v2, v6, v7}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 220
    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcom/twitter/android/av/w;->h:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    iget-boolean v3, v3, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v3, :cond_0

    const v1, 0x7f1100de

    :cond_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 225
    return-void

    .line 217
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_2
    move v0, v1

    .line 218
    goto :goto_1

    .line 221
    :cond_3
    const-string/jumbo v0, ""

    goto :goto_2
.end method

.method public a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/twitter/android/av/w;->d:Landroid/content/Context;

    .line 127
    return-void
.end method

.method public a(Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/twitter/android/av/w;->l:Landroid/support/v4/app/FragmentActivity;

    .line 131
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/w;->b(Lcom/twitter/model/core/Tweet;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/EngagementActionBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/av/v;)V
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/w;->a(Lcom/twitter/model/core/Tweet;)V

    .line 140
    iget-object v0, p0, Lcom/twitter/android/av/w;->i:Lcom/twitter/android/av/w$a;

    invoke-virtual {v0, p2}, Lcom/twitter/android/av/w$a;->a(Lcom/twitter/android/av/v;)V

    .line 141
    return-void
.end method

.method protected b(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 144
    iput-object p1, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    .line 145
    iget-object v0, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    iget-object v1, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 146
    invoke-virtual {p0}, Lcom/twitter/android/av/w;->a()V

    .line 147
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/w;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 160
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 161
    const v1, 0x7f13006e

    if-ne v0, v1, :cond_3

    .line 162
    iget-object v0, p0, Lcom/twitter/android/av/w;->i:Lcom/twitter/android/av/w$a;

    iget-object v1, p0, Lcom/twitter/android/av/w;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/android/av/w;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/av/w$a;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;)V

    .line 171
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/av/w;->b:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0}, Lcom/twitter/android/widget/EngagementActionBar;->a()V

    .line 172
    invoke-virtual {p0}, Lcom/twitter/android/av/w;->a()V

    goto :goto_0

    .line 163
    :cond_3
    const v1, 0x7f130032

    if-ne v0, v1, :cond_4

    .line 164
    iget-object v0, p0, Lcom/twitter/android/av/w;->i:Lcom/twitter/android/av/w$a;

    iget-object v1, p0, Lcom/twitter/android/av/w;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/android/av/w;->c:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/av/w;->m:Lbpl;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/av/w$a;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;Lbpl;Landroid/view/View;)V

    goto :goto_1

    .line 165
    :cond_4
    const v1, 0x7f130071

    if-ne v0, v1, :cond_5

    .line 166
    iget-object v0, p0, Lcom/twitter/android/av/w;->i:Lcom/twitter/android/av/w$a;

    iget-object v1, p0, Lcom/twitter/android/av/w;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/w$a;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V

    goto :goto_1

    .line 167
    :cond_5
    const v1, 0x7f13014a

    if-ne v0, v1, :cond_2

    .line 168
    iget-object v0, p0, Lcom/twitter/android/av/w;->i:Lcom/twitter/android/av/w$a;

    iget-object v1, p0, Lcom/twitter/android/av/w;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/w$a;->b(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V

    goto :goto_1

    .line 174
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 184
    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/av/w;->j:Lcom/twitter/android/av/w$c;

    iget-object v1, p0, Lcom/twitter/android/av/w;->l:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/w$c;->c(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/twitter/android/av/w;->e:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ToggleImageButton;->a()V

    goto :goto_0

    .line 176
    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/av/w;->j:Lcom/twitter/android/av/w$c;

    iget-object v1, p0, Lcom/twitter/android/av/w;->l:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/w$c;->a(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/av/w;->j:Lcom/twitter/android/av/w$c;

    iget-object v1, p0, Lcom/twitter/android/av/w;->l:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/w$c;->b(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 191
    :sswitch_3
    iget-object v0, p0, Lcom/twitter/android/av/w;->j:Lcom/twitter/android/av/w$c;

    iget-object v1, p0, Lcom/twitter/android/av/w;->l:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/w$c;->d(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 195
    :sswitch_4
    iget-object v0, p0, Lcom/twitter/android/av/w;->j:Lcom/twitter/android/av/w$c;

    iget-object v1, p0, Lcom/twitter/android/av/w;->l:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/w$c;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 196
    iget-object v0, p0, Lcom/twitter/android/av/w;->n:Lcom/twitter/android/av/v;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/twitter/android/av/w;->n:Lcom/twitter/android/av/v;

    iget-object v1, p0, Lcom/twitter/android/av/w;->a:Lcom/twitter/model/core/Tweet;

    const-string/jumbo v2, "share"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/v;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    :sswitch_data_0
    .sparse-switch
        0x7f130032 -> :sswitch_0
        0x7f13006e -> :sswitch_1
        0x7f130071 -> :sswitch_2
        0x7f130149 -> :sswitch_3
        0x7f13014a -> :sswitch_4
    .end sparse-switch
.end method
