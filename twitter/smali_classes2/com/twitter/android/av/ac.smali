.class public Lcom/twitter/android/av/ac;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/f;


# static fields
.field public static final a:Lcom/twitter/library/av/playback/g;


# instance fields
.field private final c:Lcom/twitter/android/av/b;

.field private final d:Z

.field private final e:Lcom/twitter/android/av/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/av/ac$1;

    invoke-direct {v0}, Lcom/twitter/android/av/ac$1;-><init>()V

    sput-object v0, Lcom/twitter/android/av/ac;->a:Lcom/twitter/library/av/playback/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/u;)V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/android/av/e;

    invoke-direct {v0, p2}, Lcom/twitter/android/av/e;-><init>(Lcom/twitter/library/av/playback/u;)V

    new-instance v1, Lcom/twitter/android/av/b;

    invoke-direct {v1, p1, p2}, Lcom/twitter/android/av/b;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/u;)V

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/av/ac;-><init>(Lcom/twitter/android/av/e;Lcom/twitter/android/av/b;)V

    .line 40
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/av/e;Lcom/twitter/android/av/b;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p2, p0, Lcom/twitter/android/av/ac;->c:Lcom/twitter/android/av/b;

    .line 45
    const-string/jumbo v0, "android_media_playback_async_logging"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/av/ac;->d:Z

    .line 47
    invoke-virtual {p1}, Lcom/twitter/android/av/e;->a()Lcom/twitter/android/av/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/ac;->e:Lcom/twitter/android/av/d;

    .line 48
    return-void
.end method

.method private c(Ljava/lang/String;Lcom/twitter/library/av/m;)Lrx/a;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/twitter/android/av/ac$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/av/ac$2;-><init>(Lcom/twitter/android/av/ac;Ljava/lang/String;Lcom/twitter/library/av/m;)V

    invoke-static {v0}, Lcre;->a(Lrx/functions/a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/twitter/library/av/m;)V
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/twitter/android/av/ac;->d:Z

    if-eqz v0, :cond_0

    .line 59
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/av/ac;->c(Ljava/lang/String;Lcom/twitter/library/av/m;)Lrx/a;

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/av/ac;->b(Ljava/lang/String;Lcom/twitter/library/av/m;)V

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;Lcom/twitter/library/av/m;)V
    .locals 2

    .prologue
    .line 72
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 84
    :goto_1
    return-void

    .line 72
    :sswitch_0
    const-string/jumbo v1, "AVPlayer.EVENT_PROMOTED_LOGGING_EVENT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "AVPlayer.EVENT_LOG_ANALYTICS_EVENT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 74
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/av/ac;->e:Lcom/twitter/android/av/d;

    invoke-interface {v0, p2}, Lcom/twitter/android/av/d;->a(Lcom/twitter/library/av/m;)V

    goto :goto_1

    .line 78
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/av/ac;->c:Lcom/twitter/android/av/b;

    invoke-virtual {v0, p2}, Lcom/twitter/android/av/b;->a(Lcom/twitter/library/av/m;)V

    goto :goto_1

    .line 72
    :sswitch_data_0
    .sparse-switch
        -0x5a91a477 -> :sswitch_1
        -0x57680804 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
