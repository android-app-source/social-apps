.class public Lcom/twitter/android/av/w$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/bm$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/android/av/v;


# direct methods
.method constructor <init>(Lcom/twitter/util/p;Lcom/twitter/android/av/v;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;",
            "Lcom/twitter/android/av/v;",
            ")V"
        }
    .end annotation

    .prologue
    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    iput-object p1, p0, Lcom/twitter/android/av/w$a;->a:Lcom/twitter/util/p;

    .line 239
    iput-object p2, p0, Lcom/twitter/android/av/w$a;->b:Lcom/twitter/android/av/v;

    .line 240
    return-void
.end method

.method private c(Lcom/twitter/model/core/Tweet;Z)V
    .locals 2

    .prologue
    .line 351
    iput-boolean p2, p1, Lcom/twitter/model/core/Tweet;->a:Z

    .line 352
    iget v1, p1, Lcom/twitter/model/core/Tweet;->n:I

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p1, Lcom/twitter/model/core/Tweet;->n:I

    .line 353
    iget-object v0, p0, Lcom/twitter/android/av/w$a;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 354
    return-void

    .line 352
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private d(Lcom/twitter/model/core/Tweet;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 357
    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p1, Lcom/twitter/model/core/Tweet;->c:Z

    .line 358
    iget v0, p1, Lcom/twitter/model/core/Tweet;->k:I

    if-eqz p2, :cond_0

    const/4 v1, -0x1

    :cond_0
    add-int/2addr v0, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p1, Lcom/twitter/model/core/Tweet;->k:I

    .line 359
    iget-object v0, p0, Lcom/twitter/android/av/w$a;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 360
    return-void

    :cond_1
    move v0, v2

    .line 357
    goto :goto_0
.end method


# virtual methods
.method public a(JLcom/twitter/model/core/Tweet;Z)V
    .locals 1

    .prologue
    .line 326
    invoke-direct {p0, p3, p4}, Lcom/twitter/android/av/w$a;->d(Lcom/twitter/model/core/Tweet;Z)V

    .line 327
    if-eqz p4, :cond_0

    const-string/jumbo v0, "unretweet"

    :goto_0
    invoke-virtual {p0, p3, v0}, Lcom/twitter/android/av/w$a;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 328
    return-void

    .line 327
    :cond_0
    const-string/jumbo v0, "retweet"

    goto :goto_0
.end method

.method public a(JZZZ)V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 304
    instance-of v0, p1, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 305
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .line 306
    new-instance v0, Lcom/twitter/android/bm$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/bm$a;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 307
    invoke-virtual {v0, p0}, Lcom/twitter/android/bm$a;->a(Lcom/twitter/android/bm$b;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Lcom/twitter/android/bm$a;->a()Lcom/twitter/android/bm;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lcom/twitter/android/bm;->a()V

    .line 311
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 253
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 254
    invoke-virtual {v0, p2}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 255
    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 256
    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 253
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 257
    const-string/jumbo v0, "reply"

    invoke-virtual {p0, p2, v0}, Lcom/twitter/android/av/w$a;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;Lbpl;Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 269
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v8

    .line 270
    iget-boolean v0, p2, Lcom/twitter/model/core/Tweet;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 271
    :goto_0
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/av/w$a;->c(Lcom/twitter/model/core/Tweet;Z)V

    .line 273
    if-nez v0, :cond_2

    .line 274
    if-eqz p4, :cond_0

    .line 275
    invoke-virtual {p4, p5}, Lbpl;->c(Landroid/view/View;)V

    .line 277
    :cond_0
    new-instance v1, Lbfp;

    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, p2, Lcom/twitter/model/core/Tweet;->u:J

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v1 .. v7}, Lbfp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 279
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfp;->a(Lcgi;)Lbfp;

    move-result-object v0

    .line 277
    invoke-virtual {v8, v0, v9}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 281
    const-string/jumbo v0, "unfavorite"

    invoke-virtual {p0, p2, v0}, Lcom/twitter/android/av/w$a;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 296
    :goto_1
    return-void

    .line 270
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 283
    :cond_2
    if-eqz p4, :cond_3

    .line 284
    invoke-virtual {p4, p5}, Lbpl;->b(Landroid/view/View;)V

    .line 286
    :cond_3
    new-instance v1, Lbfm;

    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, p2, Lcom/twitter/model/core/Tweet;->u:J

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v1 .. v7}, Lbfm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 291
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfm;->a(Lcgi;)Lbfm;

    move-result-object v0

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->m()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfm;->a(Ljava/lang/Boolean;)Lbfm;

    move-result-object v0

    .line 286
    invoke-virtual {v8, v0, v9}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 294
    const-string/jumbo v0, "favorite"

    invoke-virtual {p0, p2, v0}, Lcom/twitter/android/av/w$a;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/av/v;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/twitter/android/av/w$a;->b:Lcom/twitter/android/av/v;

    .line 244
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 364
    iget-object v0, p0, Lcom/twitter/android/av/w$a;->b:Lcom/twitter/android/av/v;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/twitter/android/av/w$a;->b:Lcom/twitter/android/av/v;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/av/v;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 367
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Z)V
    .locals 0

    .prologue
    .line 344
    return-void
.end method

.method public b(JLcom/twitter/model/core/Tweet;Z)V
    .locals 1

    .prologue
    .line 337
    if-nez p4, :cond_0

    .line 338
    const-string/jumbo v0, "quote"

    invoke-virtual {p0, p3, v0}, Lcom/twitter/android/av/w$a;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 340
    :cond_0
    return-void
.end method

.method public b(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Z)V

    .line 321
    const-string/jumbo v0, "share"

    invoke-virtual {p0, p2, v0}, Lcom/twitter/android/av/w$a;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 322
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;Z)V
    .locals 0

    .prologue
    .line 348
    return-void
.end method
