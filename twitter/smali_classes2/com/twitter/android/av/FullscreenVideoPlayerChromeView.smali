.class public Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;
.super Lcom/twitter/android/av/BaseVideoPlayerChromeView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/ExternalActionButton$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;
    }
.end annotation


# instance fields
.field protected final i:Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;

.field protected j:Lcom/twitter/android/widget/EngagementActionBar;

.field protected k:Lcom/twitter/android/av/ExternalActionButton;

.field protected l:Lcom/twitter/library/av/model/d;

.field protected m:Ljava/lang/String;

.field protected n:Landroid/view/ViewGroup;

.field protected o:Landroid/view/View;

.field protected p:Landroid/view/View;

.field protected q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/twitter/library/av/control/d;

    invoke-direct {v0}, Lcom/twitter/library/av/control/d;-><init>()V

    new-instance v1, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;

    invoke-direct {v1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/twitter/library/av/control/d;

    invoke-direct {v0}, Lcom/twitter/library/av/control/d;-><init>()V

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;)V

    .line 75
    iput-object p4, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->i:Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;

    .line 76
    invoke-direct {p0, p1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->setupEngagementActionBar(Landroid/content/Context;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V

    .line 68
    return-void
.end method

.method private I()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    .line 171
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->G()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->E()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 173
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->G()Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private J()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 298
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 297
    :goto_0
    return v0

    .line 298
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->E()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->c:Z

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v0}, Lcom/twitter/library/av/model/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setVisibility(I)V

    .line 158
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->D()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setVisibility(I)V

    .line 161
    :cond_2
    return-void
.end method

.method private setupEngagementActionBar(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->i:Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;

    invoke-virtual {v0, p1, p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/widget/EngagementActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 131
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/EngagementActionBar;->setVisibility(I)V

    .line 132
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/EngagementActionBar;->setBackgroundColor(I)V

    .line 134
    :cond_0
    return-void
.end method


# virtual methods
.method protected D()Z
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->E()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method E()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method F()Z
    .locals 1

    .prologue
    .line 188
    const-string/jumbo v0, "video_call_to_action_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected G()Z
    .locals 2

    .prologue
    .line 293
    invoke-direct {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->i:Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method H()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 318
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->getAVPlayer()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v1

    .line 319
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 321
    :goto_0
    if-eqz v1, :cond_0

    .line 322
    invoke-interface {v1}, Lcom/twitter/model/av/AVMedia;->g()Lcom/twitter/model/av/a;

    move-result-object v0

    .line 323
    invoke-interface {v1}, Lcom/twitter/model/av/AVMedia;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->m:Ljava/lang/String;

    .line 328
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(Lcom/twitter/model/av/a;)V

    .line 329
    return-void

    :cond_1
    move-object v1, v0

    .line 319
    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->g:Lcom/twitter/library/av/control/d;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/d;->b(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;

    move-result-object v0

    return-object v0
.end method

.method protected a(IIII)V
    .locals 4

    .prologue
    .line 236
    .line 238
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v0}, Lcom/twitter/android/av/ExternalActionButton;->getPaddingBottom()I

    move-result v0

    sub-int v0, p4, v0

    .line 239
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v1}, Lcom/twitter/android/av/ExternalActionButton;->getHeight()I

    move-result v1

    sub-int v1, v0, v1

    .line 240
    sub-int v2, p3, p1

    iget-object v3, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v3}, Lcom/twitter/android/av/ExternalActionButton;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 241
    if-lez v2, :cond_0

    .line 242
    add-int/2addr p1, v2

    .line 243
    sub-int/2addr p3, v2

    .line 245
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v2, p1, v1, p3, v0}, Lcom/twitter/android/av/ExternalActionButton;->layout(IIII)V

    .line 246
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 0

    .prologue
    .line 303
    invoke-super {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 304
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->H()V

    .line 305
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->i:Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 99
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->H()V

    .line 100
    return-void
.end method

.method protected a(Lcom/twitter/model/av/a;)V
    .locals 5

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b(Lcom/twitter/model/av/a;)Lcom/twitter/library/av/model/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    .line 334
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v0}, Lcom/twitter/library/av/model/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setVisibility(I)V

    .line 352
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v1}, Lcom/twitter/library/av/model/d;->c()Landroid/net/Uri;

    move-result-object v1

    .line 341
    iget-object v2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v2, v0}, Lcom/twitter/library/av/model/d;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    .line 343
    if-eqz v1, :cond_1

    .line 344
    iget-object v3, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    iget-object v4, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v4, v0}, Lcom/twitter/library/av/model/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/android/av/ExternalActionButton;->setFallbackText(Ljava/lang/String;)V

    .line 345
    iget-object v3, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v3, v2}, Lcom/twitter/android/av/ExternalActionButton;->setFallbackUri(Landroid/net/Uri;)V

    .line 346
    iget-object v2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    iget-object v3, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v3, v0}, Lcom/twitter/library/av/model/d;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/av/ExternalActionButton;->setActionText(Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setExternalUri(Landroid/net/Uri;)V

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ExternalActionButton;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->getAVPlayer()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 368
    if-nez v0, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 372
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v2}, Lcom/twitter/library/av/model/d;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 373
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v2, Lbjk;

    invoke-direct {v2, v1, p1}, Lbjk;-><init>(Lcom/twitter/model/av/AVMedia;Z)V

    invoke-virtual {v0, v2}, Lbix;->a(Lbiw;)V

    goto :goto_0
.end method

.method protected a(ZIIII)V
    .locals 6

    .prologue
    .line 214
    .line 216
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0}, Lcom/twitter/android/widget/EngagementActionBar;->getHeight()I

    move-result v0

    sub-int v0, p5, v0

    .line 218
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v1, p2, v0, p4, p5}, Lcom/twitter/android/widget/EngagementActionBar;->layout(IIII)V

    .line 222
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v1}, Lcom/twitter/library/av/control/VideoControlView;->getHeight()I

    move-result v1

    sub-int v1, v0, v1

    .line 224
    iget-object v2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v2, p2, v1, p4, v0}, Lcom/twitter/library/av/control/VideoControlView;->layout(IIII)V

    move v0, v1

    .line 228
    :cond_0
    invoke-virtual {p0, p2, p3, p4, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(IIII)V

    .line 230
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/av/ab;->a(ZIIII)V

    .line 233
    :cond_1
    return-void

    :cond_2
    move v0, p5

    goto :goto_0
.end method

.method protected b(Lcom/twitter/model/av/a;)Lcom/twitter/library/av/model/d;
    .locals 1

    .prologue
    .line 356
    new-instance v0, Lcom/twitter/library/av/model/d;

    invoke-direct {v0, p1}, Lcom/twitter/library/av/model/d;-><init>(Lcom/twitter/model/av/a;)V

    return-object v0
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->getAVPlayer()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 383
    if-nez v0, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 387
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    if-eqz v2, :cond_0

    .line 388
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v2

    new-instance v3, Lbjj;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v3, v1, v0}, Lbjj;-><init>(Lcom/twitter/model/av/AVMedia;Z)V

    invoke-virtual {v2, v3}, Lbix;->a(Lbiw;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected b(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->n:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->n:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->addView(Landroid/view/View;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->addView(Landroid/view/View;)V

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_2

    .line 88
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->addView(Landroid/view/View;)V

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 91
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->addView(Landroid/view/View;)V

    .line 93
    :cond_3
    return-void
.end method

.method protected o()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 250
    invoke-super {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->o()V

    .line 252
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 253
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->G()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-static {v1}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->F()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eq v0, v2, :cond_2

    .line 259
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-static {v0}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 263
    :cond_1
    :goto_0
    return-void

    .line 260
    :cond_2
    if-eq v0, v2, :cond_1

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 146
    invoke-direct {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->I()V

    .line 147
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(I)V

    .line 148
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 197
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->onLayout(ZIIII)V

    .line 198
    invoke-virtual/range {p0 .. p5}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(ZIIII)V

    .line 199
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 267
    invoke-super {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->p()V

    .line 269
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->j:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->b(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->l:Lcom/twitter/library/av/model/d;

    invoke-virtual {v0}, Lcom/twitter/library/av/model/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 276
    :cond_1
    return-void
.end method

.method public setShouldPlayPauseOnTap(Z)V
    .locals 0

    .prologue
    .line 397
    iput-boolean p1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->q:Z

    .line 398
    return-void
.end method

.method protected setupInternalViews(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 104
    invoke-super {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->setupInternalViews(Landroid/content/Context;)V

    .line 106
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 107
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    if-nez v0, :cond_0

    .line 108
    const v0, 0x7f040046

    invoke-virtual {v1, v0, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/ExternalActionButton;

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    .line 110
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v0, v2}, Lcom/twitter/android/av/ExternalActionButton;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/ExternalActionButton;->setEventListener(Lcom/twitter/android/av/ExternalActionButton$a;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->n:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 115
    const v0, 0x7f040047

    invoke-virtual {v1, v0, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->n:Landroid/view/ViewGroup;

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->n:Landroid/view/ViewGroup;

    const v1, 0x7f1301ab

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->o:Landroid/view/View;

    .line 118
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->n:Landroid/view/ViewGroup;

    const v1, 0x7f1301ac

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->p:Landroid/view/View;

    .line 120
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 121
    return-void
.end method
