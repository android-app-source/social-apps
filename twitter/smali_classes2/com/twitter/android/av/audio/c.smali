.class public Lcom/twitter/android/av/audio/c;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/widget/TextView;Lcom/twitter/android/av/audio/e;)V
    .locals 4

    .prologue
    .line 79
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0ba4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 80
    invoke-virtual {p1}, Lcom/twitter/android/av/audio/e;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/twitter/android/av/audio/e;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 79
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method

.method public static a(Lcom/twitter/android/av/ExternalActionButton;Lcom/twitter/android/av/audio/e;Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 2

    .prologue
    .line 27
    invoke-interface {p2}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    invoke-static {p0, p1, v0}, Lcom/twitter/android/av/audio/c;->a(Lcom/twitter/android/av/ExternalActionButton;Lcom/twitter/android/av/audio/e;Lcom/twitter/model/core/Tweet;)V

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    instance-of v0, p2, Lcom/twitter/library/av/playback/CardAVDataSource;

    if-eqz v0, :cond_0

    .line 31
    invoke-interface {p2}, Lcom/twitter/library/av/playback/AVDataSource;->j()Lcom/twitter/model/av/Partner;

    move-result-object v0

    .line 32
    check-cast p2, Lcom/twitter/library/av/playback/CardAVDataSource;

    invoke-virtual {p2}, Lcom/twitter/library/av/playback/CardAVDataSource;->r()Ljava/lang/String;

    move-result-object v1

    .line 33
    invoke-static {p0, p1, v0, v1}, Lcom/twitter/android/av/audio/c;->a(Lcom/twitter/android/av/ExternalActionButton;Lcom/twitter/android/av/audio/e;Lcom/twitter/model/av/Partner;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/av/ExternalActionButton;Lcom/twitter/android/av/audio/e;Lcom/twitter/model/av/Partner;Ljava/lang/String;)V
    .locals 7

    .prologue
    const v0, 0x7f0a0083

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/av/ExternalActionButton;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 49
    const-string/jumbo v1, "learn"

    invoke-virtual {p1}, Lcom/twitter/android/av/audio/e;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v0

    .line 61
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/android/av/audio/e;->e()Ljava/lang/String;

    move-result-object v3

    .line 62
    if-eqz v3, :cond_0

    .line 63
    new-array v4, v6, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {v2, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setActionText(Ljava/lang/String;)V

    .line 64
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setExternalUri(Landroid/net/Uri;)V

    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android-app://"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setReferrerUri(Landroid/net/Uri;)V

    .line 68
    :cond_0
    new-array v1, v6, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/ExternalActionButton;->setFallbackText(Ljava/lang/String;)V

    .line 69
    if-eqz p3, :cond_1

    .line 71
    invoke-static {v2, p3}, Lcom/twitter/util/u;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    .line 73
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 74
    invoke-virtual {p0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setFallbackIntent(Landroid/content/Intent;)V

    .line 76
    :cond_1
    return-void

    .line 57
    :cond_2
    const v1, 0x7f0a0082

    .line 58
    const v0, 0x7f0a0084

    goto :goto_0
.end method

.method public static a(Lcom/twitter/android/av/ExternalActionButton;Lcom/twitter/android/av/audio/e;Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 39
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->l(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/av/Partner;

    move-result-object v0

    .line 40
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->at()Ljava/lang/String;

    move-result-object v1

    .line 41
    invoke-static {p0, p1, v0, v1}, Lcom/twitter/android/av/audio/c;->a(Lcom/twitter/android/av/ExternalActionButton;Lcom/twitter/android/av/audio/e;Lcom/twitter/model/av/Partner;Ljava/lang/String;)V

    .line 42
    return-void
.end method
