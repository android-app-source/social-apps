.class public Lcom/twitter/android/av/audio/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/support/v4/app/FragmentManager;

.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/twitter/util/android/g;

.field private d:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Lcom/twitter/util/android/g;

    invoke-direct {v1, p2}, Lcom/twitter/util/android/g;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/av/audio/a;-><init>(Landroid/support/v4/app/FragmentManager;Landroid/content/res/Resources;Lcom/twitter/util/android/g;)V

    .line 32
    return-void
.end method

.method constructor <init>(Landroid/support/v4/app/FragmentManager;Landroid/content/res/Resources;Lcom/twitter/util/android/g;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/twitter/android/av/audio/a;->a:Landroid/support/v4/app/FragmentManager;

    .line 45
    iput-object p2, p0, Lcom/twitter/android/av/audio/a;->b:Landroid/content/res/Resources;

    .line 46
    iput-object p3, p0, Lcom/twitter/android/av/audio/a;->c:Lcom/twitter/util/android/g;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/av/c;)V
    .locals 3

    .prologue
    .line 56
    invoke-static {p1}, Lcom/twitter/android/av/audio/AudioCardError;->a(Lcom/twitter/model/av/c;)Lcom/twitter/android/av/audio/AudioCardError;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Lcom/twitter/android/av/audio/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/twitter/android/av/audio/AudioCardError;->statusCode:I

    const/16 v2, 0x193

    if-ne v1, v2, :cond_0

    .line 59
    iget-object v0, p0, Lcom/twitter/android/av/audio/a;->b:Landroid/content/res/Resources;

    .line 60
    invoke-static {v0, p1}, Lcom/twitter/android/av/audio/AudioCardErrorDialog;->a(Landroid/content/res/Resources;Lcom/twitter/model/av/c;)Lcom/twitter/android/av/audio/AudioCardErrorDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/av/audio/a;->a:Landroid/support/v4/app/FragmentManager;

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/audio/AudioCardErrorDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 71
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/av/audio/a;->b:Landroid/content/res/Resources;

    sget-object v2, Lcom/twitter/android/av/audio/d;->a:Lcom/twitter/android/av/audio/d;

    .line 64
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/audio/AudioCardError;->a(Landroid/content/res/Resources;Lcom/twitter/android/av/audio/d;)Ljava/lang/String;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/twitter/android/av/audio/a;->d:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    .line 66
    iget-object v1, p0, Lcom/twitter/android/av/audio/a;->d:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/av/audio/a;->c:Lcom/twitter/util/android/g;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/util/android/g;->a(Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/a;->d:Landroid/widget/Toast;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/av/audio/a;->d:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/av/audio/a;->a:Landroid/support/v4/app/FragmentManager;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
