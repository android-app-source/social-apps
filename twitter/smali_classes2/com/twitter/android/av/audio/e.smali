.class public Lcom/twitter/android/av/audio/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/audio/e$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/android/av/audio/e$a;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    invoke-static {p1}, Lcom/twitter/android/av/audio/e$a;->a(Lcom/twitter/android/av/audio/e$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/e;->a:Ljava/lang/String;

    .line 86
    invoke-static {p1}, Lcom/twitter/android/av/audio/e$a;->b(Lcom/twitter/android/av/audio/e$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/e;->b:Ljava/lang/String;

    .line 87
    invoke-static {p1}, Lcom/twitter/android/av/audio/e$a;->c(Lcom/twitter/android/av/audio/e$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/e;->c:Ljava/lang/String;

    .line 88
    invoke-static {p1}, Lcom/twitter/android/av/audio/e$a;->d(Lcom/twitter/android/av/audio/e$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/e;->d:Ljava/lang/String;

    .line 89
    invoke-static {p1}, Lcom/twitter/android/av/audio/e$a;->e(Lcom/twitter/android/av/audio/e$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/e;->e:Ljava/lang/String;

    .line 90
    return-void
.end method

.method private static a(Lcax;)Lcom/twitter/android/av/audio/e;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 62
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcax;->K()Lcar;

    move-result-object v0

    .line 64
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string/jumbo v2, "app_url"

    const-string/jumbo v3, "app_url_resolved"

    .line 67
    invoke-static {v2, v3, v0}, Lcom/twitter/library/card/e;->a(Ljava/lang/String;Ljava/lang/String;Lcar;)Lcom/twitter/library/card/e;

    move-result-object v3

    .line 68
    const-string/jumbo v2, "player_image"

    invoke-static {v2, v0}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v2

    .line 69
    const-string/jumbo v4, "artist_name"

    invoke-static {v4, v0}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v4

    .line 70
    if-eqz v2, :cond_1

    iget-object v2, v2, Lcas;->a:Ljava/lang/String;

    .line 71
    :goto_1
    const-string/jumbo v5, "title"

    invoke-static {v5, v0}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v5

    .line 72
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/twitter/library/card/e;->b()Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_2
    new-instance v3, Lcom/twitter/android/av/audio/e$a;

    invoke-direct {v3}, Lcom/twitter/android/av/audio/e$a;-><init>()V

    .line 76
    invoke-virtual {v3, v4}, Lcom/twitter/android/av/audio/e$a;->a(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v3

    .line 77
    invoke-virtual {v3, v2}, Lcom/twitter/android/av/audio/e$a;->b(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v2

    .line 78
    invoke-virtual {v2, v1}, Lcom/twitter/android/av/audio/e$a;->d(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v0}, Lcom/twitter/android/av/audio/e$a;->e(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v0

    .line 80
    invoke-virtual {v0, v5}, Lcom/twitter/android/av/audio/e$a;->c(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/twitter/android/av/audio/e$a;->a()Lcom/twitter/android/av/audio/e;

    move-result-object v0

    .line 75
    return-object v0

    :cond_0
    move-object v0, v1

    .line 62
    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 70
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 72
    goto :goto_2
.end method

.method public static a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/android/av/audio/e;
    .locals 1

    .prologue
    .line 51
    invoke-interface {p0}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    invoke-static {v0}, Lcom/twitter/android/av/audio/e;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/av/audio/e;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    .line 54
    :cond_0
    instance-of v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;

    if-eqz v0, :cond_1

    .line 55
    check-cast p0, Lcom/twitter/library/av/playback/CardAVDataSource;

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/CardAVDataSource;->q()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/av/audio/e;->a(Lcax;)Lcom/twitter/android/av/audio/e;

    move-result-object v0

    goto :goto_0

    .line 57
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/av/Audio;)Lcom/twitter/android/av/audio/e;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/android/av/audio/e$a;

    invoke-direct {v0}, Lcom/twitter/android/av/audio/e$a;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/twitter/model/av/Audio;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/audio/e$a;->a(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/twitter/model/av/Audio;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/audio/e$a;->b(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Lcom/twitter/model/av/Audio;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/audio/e$a;->d(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v0

    .line 39
    invoke-virtual {p0}, Lcom/twitter/model/av/Audio;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/audio/e$a;->e(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/twitter/model/av/Audio;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/audio/e$a;->c(Ljava/lang/String;)Lcom/twitter/android/av/audio/e$a;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/twitter/android/av/audio/e$a;->a()Lcom/twitter/android/av/audio/e;

    move-result-object v0

    .line 35
    return-object v0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/av/audio/e;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/av/audio/e;->a(Lcax;)Lcom/twitter/android/av/audio/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/av/audio/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/av/audio/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/android/av/audio/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/av/audio/e;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/av/audio/e;->e:Ljava/lang/String;

    return-object v0
.end method
