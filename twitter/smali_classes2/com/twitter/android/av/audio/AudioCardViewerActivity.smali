.class public Lcom/twitter/android/av/audio/AudioCardViewerActivity;
.super Lcom/twitter/android/av/AVCardCanvasActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/audio/AudioCardViewerActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/av/AVCardCanvasActivity",
        "<",
        "Lcom/twitter/android/av/audio/AudioCardPlayerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final q:Lcom/twitter/android/av/ab;

.field private r:Z

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcom/twitter/model/av/Partner;

.field private final u:Ljava/lang/Runnable;

.field private final v:Landroid/os/Handler;

.field private w:Lcom/twitter/android/av/audio/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;-><init>()V

    .line 44
    new-instance v0, Lcom/twitter/android/av/ab;

    invoke-direct {v0}, Lcom/twitter/android/av/ab;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->q:Lcom/twitter/android/av/ab;

    .line 46
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->s:Ljava/util/Map;

    .line 47
    sget-object v0, Lcom/twitter/model/av/Partner;->a:Lcom/twitter/model/av/Partner;

    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->t:Lcom/twitter/model/av/Partner;

    .line 53
    new-instance v0, Lcom/twitter/android/av/audio/AudioCardViewerActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/audio/AudioCardViewerActivity$1;-><init>(Lcom/twitter/android/av/audio/AudioCardViewerActivity;)V

    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->u:Ljava/lang/Runnable;

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->v:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/audio/AudioCardViewerActivity;)Lcom/twitter/android/widget/ExpandableViewHost;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/av/audio/AudioCardViewerActivity;Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->a(Landroid/content/res/Configuration;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/audio/AudioCardViewerActivity;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/av/audio/AudioCardViewerActivity;)Lcom/twitter/android/widget/ExpandableViewHost;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/av/audio/AudioCardViewerActivity;)Lcom/twitter/android/av/ab;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->q:Lcom/twitter/android/av/ab;

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->s()V

    .line 113
    :cond_0
    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_1

    .line 155
    iget-object v1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->k:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v1}, Lcom/twitter/library/av/playback/AVDataSource;->j()Lcom/twitter/model/av/Partner;

    move-result-object v1

    .line 156
    const-string/jumbo v2, "extra_metadata"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 157
    if-eqz v1, :cond_0

    .line 158
    iput-object v1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->t:Lcom/twitter/model/av/Partner;

    .line 160
    :cond_0
    if-eqz v0, :cond_1

    .line 161
    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->s:Ljava/util/Map;

    .line 164
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 73
    const v0, 0x7f04003a

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 74
    return-object p2
.end method

.method protected a(Landroid/os/Bundle;)Lcom/twitter/library/av/playback/u;
    .locals 2

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->r()V

    .line 149
    new-instance v1, Lcom/twitter/library/av/playback/v;

    invoke-direct {v1}, Lcom/twitter/library/av/playback/v;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->k:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/twitter/android/av/audio/a;

    invoke-virtual {p0}, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/twitter/android/av/audio/a;-><init>(Landroid/support/v4/app/FragmentManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->w:Lcom/twitter/android/av/audio/a;

    .line 80
    invoke-super {p0, p1, p2}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 81
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v1, Lbjc;

    invoke-direct {v1}, Lbjc;-><init>()V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 83
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->q:Lcom/twitter/android/av/ab;

    iget-object v1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ab;->a(Landroid/view/ViewGroup;)Z

    .line 169
    invoke-virtual {p0}, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->j()V

    .line 170
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->w:Lcom/twitter/android/av/audio/a;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->w:Lcom/twitter/android/av/audio/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/audio/a;->a(Lcom/twitter/model/av/c;)V

    .line 173
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->v:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 187
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->d()V

    .line 188
    return-void
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    const-string/jumbo v0, "platform_audio_card"

    return-object v0
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->v:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->u:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 126
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->d:Lcom/twitter/android/av/TweetEngagementView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/TweetEngagementView;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/audio/AudioCardPlayerView;

    invoke-virtual {v0}, Lcom/twitter/android/av/audio/AudioCardPlayerView;->getContentView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->i()V

    .line 129
    return-void
.end method

.method protected j()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->v:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 135
    iget-boolean v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->r:Z

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ExpandableViewHost;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ExpandableViewHost;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 139
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->r:Z

    .line 140
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/audio/AudioCardPlayerView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/audio/AudioCardPlayerView;->setCallToActionListener(Lcom/twitter/android/av/ExternalActionButton$a;)V

    .line 142
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->j()V

    .line 143
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->q:Lcom/twitter/android/av/ab;

    iget-object v1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ab;->a(Landroid/view/ViewGroup;)Z

    .line 181
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->n()V

    .line 182
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->onPause()V

    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    .line 97
    invoke-direct {p0}, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->q()V

    .line 99
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->onResume()V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->d(Z)V

    .line 91
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->onStop()V

    .line 104
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/twitter/android/av/audio/AudioCardViewerActivity;->q()V

    .line 107
    :cond_0
    return-void
.end method
