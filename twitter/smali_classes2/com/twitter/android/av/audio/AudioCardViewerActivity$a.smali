.class public Lcom/twitter/android/av/audio/AudioCardViewerActivity$a;
.super Lcom/twitter/android/av/AVCardCanvasActivity$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/audio/AudioCardViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/twitter/android/av/AVCardCanvasActivity$a;-><init>()V

    .line 196
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity$a;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 211
    invoke-super {p0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 212
    const-string/jumbo v1, "extra_metadata"

    iget-object v2, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity$a;->a:Ljava/util/Map;

    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/Map;)Ljava/io/Serializable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 213
    return-object v0
.end method

.method public a(Ljava/util/Map;)Lcom/twitter/android/av/audio/AudioCardViewerActivity$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/android/av/audio/AudioCardViewerActivity$a;"
        }
    .end annotation

    .prologue
    .line 205
    iput-object p1, p0, Lcom/twitter/android/av/audio/AudioCardViewerActivity$a;->a:Ljava/util/Map;

    .line 206
    return-object p0
.end method

.method protected a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/android/av/AVCardCanvasActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    const-class v0, Lcom/twitter/android/av/audio/AudioCardViewerActivity;

    return-object v0
.end method
