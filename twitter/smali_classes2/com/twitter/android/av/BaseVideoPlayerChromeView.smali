.class public abstract Lcom/twitter/android/av/BaseVideoPlayerChromeView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/aj$a;
.implements Lcom/twitter/library/av/control/VideoControlView$a;
.implements Lcom/twitter/library/av/control/e;


# instance fields
.field protected a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field protected b:Lcom/twitter/library/av/control/VideoControlView;

.field protected c:Z

.field protected d:Z

.field protected e:Lcom/twitter/android/av/ab;

.field protected f:Z

.field protected final g:Lcom/twitter/library/av/control/d;

.field protected final h:Lcom/twitter/android/av/ag;

.field private i:Lcom/twitter/library/av/control/VideoControlView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/library/av/control/d;

    invoke-direct {v0}, Lcom/twitter/library/av/control/d;-><init>()V

    new-instance v1, Lcom/twitter/android/av/ag;

    invoke-direct {v1, p1}, Lcom/twitter/android/av/ag;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/ag;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;)V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/twitter/android/av/ag;

    invoke-direct {v0, p1}, Lcom/twitter/android/av/ag;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/ag;)V

    .line 68
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/ag;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->f:Z

    .line 74
    iput-object p3, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->g:Lcom/twitter/library/av/control/d;

    .line 75
    iput-object p4, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->h:Lcom/twitter/android/av/ag;

    .line 76
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->setupInternalViews(Landroid/content/Context;)V

    .line 77
    return-void
.end method


# virtual methods
.method protected A()V
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->setBackgroundColor(I)V

    .line 457
    return-void
.end method

.method protected B()V
    .locals 1

    .prologue
    .line 460
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->getPlaylistCompleteOverlayBackgroundColor()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->setBackgroundColor(I)V

    .line 461
    return-void
.end method

.method protected C()Z
    .locals 1

    .prologue
    .line 482
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->g:Lcom/twitter/library/av/control/d;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/d;->a(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/av/ad;)V
    .locals 2

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->A()V

    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->u()V

    .line 299
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    iget-object v1, p2, Lcom/twitter/library/av/ad;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/library/av/control/VideoControlView;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 303
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 222
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->u()V

    .line 223
    iput-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->c:Z

    .line 224
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->A()V

    .line 225
    iget-object v1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v1}, Lcom/twitter/library/av/control/VideoControlView;->b()V

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->v()V

    .line 233
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 236
    const-string/jumbo v2, "video_disable_control_hiding"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 240
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->d:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    if-eq p1, v1, :cond_2

    if-eqz v0, :cond_3

    .line 241
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->p()V

    .line 243
    :cond_3
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 2

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->setWillNotDraw(Z)V

    .line 105
    iput-object p1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 106
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->h:Lcom/twitter/android/av/ag;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/ag;->a(Lcom/twitter/android/av/aj$a;)V

    .line 108
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->k()V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 111
    iget-object v1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/av/control/VideoControlView;->a(Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 112
    iget-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->f:Z

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->f()V

    .line 116
    :cond_0
    return-void

    .line 111
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/VideoControlView;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 468
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 326
    iput-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->d:Z

    .line 327
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->u()V

    .line 328
    iget-object v1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v1, :cond_1

    .line 329
    iget-object v1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/twitter/model/av/AVMedia;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/av/control/VideoControlView;->b(Z)V

    .line 331
    :cond_1
    return-void

    .line 329
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 471
    return-void
.end method

.method public a(ZJ)V
    .locals 2

    .prologue
    .line 283
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->c:Z

    if-eqz v0, :cond_1

    .line 284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->c:Z

    .line 285
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->A()V

    .line 290
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/library/av/control/VideoControlView$a;->a(ZJ)V

    .line 293
    :cond_0
    return-void

    .line 287
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->v()V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1, p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a(Landroid/view/View;Landroid/view/ViewGroup;)Z

    move-result v0

    return v0
.end method

.method protected a(Landroid/view/View;Landroid/view/ViewGroup;)Z
    .locals 1

    .prologue
    .line 84
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 261
    iput-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->d:Z

    .line 262
    iput-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->c:Z

    .line 263
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->u()V

    .line 264
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/VideoControlView;->a(Z)V

    .line 267
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->p()V

    .line 268
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->B()V

    .line 269
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method public b_(Z)V
    .locals 0

    .prologue
    .line 450
    if-eqz p1, :cond_0

    .line 451
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->p()V

    .line 453
    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->t()V

    .line 395
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->u()V

    .line 390
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->d:Z

    .line 336
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->getAVPlayer()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 337
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->t()V

    .line 340
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 368
    const/4 v0, 0x0

    .line 369
    iget-boolean v1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v1, :cond_1

    .line 370
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 371
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->p()V

    .line 375
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 377
    :cond_1
    return v0

    .line 372
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->o()V

    goto :goto_0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method protected getAVPlayer()Lcom/twitter/library/av/playback/AVPlayer;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 90
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getControls()Landroid/view/View;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    return-object v0
.end method

.method public getPlaylistCompleteOverlayBackgroundColor()I
    .locals 2
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 475
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 95
    return-object p0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->p()V

    .line 215
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->A()V

    .line 274
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->u()V

    .line 275
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->o()V

    .line 276
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 279
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->addView(Landroid/view/View;)V

    .line 128
    :cond_0
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->requestLayout()V

    .line 358
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->a()V

    .line 361
    :cond_0
    return-void
.end method

.method protected m()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->C()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/control/VideoControlView;->setIsFullScreenToggleAllowed(Z)V

    .line 157
    :cond_0
    return-void
.end method

.method protected n()Lcom/twitter/android/av/ab;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/twitter/android/av/ab;

    invoke-direct {v0}, Lcom/twitter/android/av/ab;-><init>()V

    return-object v0
.end method

.method protected o()V
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->f()V

    .line 184
    :cond_0
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->g()V

    .line 196
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->c:Z

    if-nez v0, :cond_1

    .line 197
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->v()V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->h:Lcom/twitter/android/av/ag;

    invoke-virtual {v0}, Lcom/twitter/android/av/ag;->a()V

    goto :goto_0
.end method

.method public q()V
    .locals 0

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->o()V

    .line 210
    return-void
.end method

.method protected r()Z
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x1

    return v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setControlsListener(Lcom/twitter/library/av/control/VideoControlView$a;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    .line 322
    return-void
.end method

.method public setIsFullscreen(Z)V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/VideoControlView;->c(Z)V

    .line 492
    :cond_0
    return-void
.end method

.method public setShouldShowControls(Z)V
    .locals 2

    .prologue
    .line 312
    iput-boolean p1, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->f:Z

    .line 313
    if-eqz p1, :cond_0

    .line 314
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/control/VideoControlView;->setVisibility(I)V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/control/VideoControlView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setupInternalViews(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-nez v0, :cond_0

    .line 138
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->a(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->b:Lcom/twitter/library/av/control/VideoControlView;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/control/VideoControlView;->setListener(Lcom/twitter/library/av/control/VideoControlView$a;)V

    .line 141
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->m()V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    if-nez v0, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->n()Lcom/twitter/android/av/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    .line 148
    :cond_1
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/av/ab;->a(Landroid/view/ViewGroup;Landroid/content/Context;)Z

    .line 401
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->o()V

    .line 402
    return-void
.end method

.method public u()V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->e:Lcom/twitter/android/av/ab;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/ab;->a(Landroid/view/ViewGroup;)Z

    .line 408
    :cond_0
    return-void
.end method

.method protected v()V
    .locals 4

    .prologue
    .line 411
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->h:Lcom/twitter/android/av/ag;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/av/ag;->a(J)V

    .line 412
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->v()V

    .line 418
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->w()V

    .line 421
    :cond_0
    return-void
.end method

.method public x()V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->x()V

    .line 428
    :cond_0
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->v()V

    .line 434
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->y()V

    .line 437
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->h:Lcom/twitter/android/av/ag;

    invoke-virtual {v0}, Lcom/twitter/android/av/ag;->a()V

    .line 443
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/twitter/android/av/BaseVideoPlayerChromeView;->i:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->z()V

    .line 446
    :cond_0
    return-void
.end method
