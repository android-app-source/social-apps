.class public abstract Lcom/twitter/android/av/r;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private final b:Landroid/graphics/Rect;

.field private final c:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Float;",
            "Lcom/twitter/library/widget/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/widget/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/r;->a:Landroid/graphics/Rect;

    .line 47
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/r;->b:Landroid/graphics/Rect;

    .line 48
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->b()Ljava/util/SortedMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/r;->c:Ljava/util/SortedMap;

    .line 49
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/r;->d:Ljava/util/List;

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/r;->e:Z

    return-void
.end method

.method public static a(I)Lcom/twitter/android/av/r;
    .locals 1

    .prologue
    .line 151
    packed-switch p0, :pswitch_data_0

    .line 169
    new-instance v0, Lcom/twitter/android/av/q;

    invoke-direct {v0}, Lcom/twitter/android/av/q;-><init>()V

    :goto_0
    return-object v0

    .line 153
    :pswitch_0
    new-instance v0, Lcom/twitter/android/av/s;

    invoke-direct {v0}, Lcom/twitter/android/av/s;-><init>()V

    goto :goto_0

    .line 156
    :pswitch_1
    new-instance v0, Lcom/twitter/android/av/q;

    invoke-direct {v0}, Lcom/twitter/android/av/q;-><init>()V

    goto :goto_0

    .line 159
    :pswitch_2
    new-instance v0, Lcom/twitter/android/av/o;

    invoke-direct {v0}, Lcom/twitter/android/av/o;-><init>()V

    goto :goto_0

    .line 162
    :pswitch_3
    new-instance v0, Lcom/twitter/android/av/p;

    invoke-direct {v0}, Lcom/twitter/android/av/p;-><init>()V

    goto :goto_0

    .line 165
    :pswitch_4
    new-instance v0, Lcom/twitter/android/av/n;

    invoke-direct {v0}, Lcom/twitter/android/av/n;-><init>()V

    goto :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private c()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/av/r;->c:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    .line 142
    iget-object v0, p0, Lcom/twitter/android/av/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 143
    return-void
.end method


# virtual methods
.method abstract a()D
.end method

.method protected abstract a(Landroid/graphics/Rect;Landroid/graphics/Rect;)F
.end method

.method public a(Landroid/view/ViewGroup;Ljava/util/List;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/widget/a;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/widget/a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lcom/twitter/android/av/r;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 89
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_4

    .line 90
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/a;

    .line 91
    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v4

    .line 93
    iget-boolean v2, p0, Lcom/twitter/android/av/r;->e:Z

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    .line 94
    :goto_1
    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    .line 95
    iget-object v2, p0, Lcom/twitter/android/av/r;->b:Landroid/graphics/Rect;

    invoke-virtual {v4, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    .line 96
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/av/r;->b:Landroid/graphics/Rect;

    .line 97
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p0, v2, v5, v4}, Lcom/twitter/android/av/r;->a(Landroid/graphics/Rect;II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    iget-object v2, p0, Lcom/twitter/android/av/r;->a:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/twitter/android/av/r;->b:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v4}, Lcom/twitter/android/av/r;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)F

    move-result v2

    .line 102
    iget-object v4, p0, Lcom/twitter/android/av/r;->c:Ljava/util/SortedMap;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 103
    iget-object v4, p0, Lcom/twitter/android/av/r;->c:Ljava/util/SortedMap;

    const v5, 0x38d1b717    # 1.0E-4f

    add-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v4, v2, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_1
    :goto_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v2, v1

    .line 93
    goto :goto_1

    .line 105
    :cond_3
    iget-object v4, p0, Lcom/twitter/android/av/r;->c:Ljava/util/SortedMap;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v4, v2, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 111
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/av/r;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/android/av/r;->c:Ljava/util/SortedMap;

    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 112
    iget-object v0, p0, Lcom/twitter/android/av/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/av/r;->b()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 113
    if-lez v2, :cond_5

    .line 114
    invoke-static {v2}, Lcom/twitter/util/collection/MutableSet;->a(I)Ljava/util/Set;

    move-result-object v0

    .line 115
    :goto_3
    if-ge v1, v2, :cond_6

    .line 116
    iget-object v3, p0, Lcom/twitter/android/av/r;->d:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 114
    :cond_5
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    goto :goto_3

    .line 118
    :cond_6
    invoke-direct {p0}, Lcom/twitter/android/av/r;->c()V

    .line 119
    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/twitter/android/av/r;->e:Z

    .line 132
    return-void
.end method

.method protected a(Landroid/graphics/Rect;II)Z
    .locals 6

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/twitter/android/av/r;->a()D

    move-result-wide v0

    .line 70
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    iget v3, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    int-to-double v4, p2

    mul-double/2addr v4, v0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_0

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    int-to-double v4, p3

    mul-double/2addr v0, v4

    cmpl-double v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b()I
.end method
