.class public Lcom/twitter/android/av/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/b$b;,
        Lcom/twitter/android/av/b$a;
    }
.end annotation


# static fields
.field static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Lnm;

.field private final e:Lcom/twitter/library/av/c;

.field private final f:Lcom/twitter/android/av/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 61
    const-string/jumbo v0, "playback_start"

    const/16 v1, 0x15

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "playback_25"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "playback_50"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "playback_75"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "playback_95"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "playback_complete"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "replay"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "loop"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "play"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "rewind"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "pause"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "dock"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "undock"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "playback_lapse"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "error"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "reply"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "unfavorite"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "favorite"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "share"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "unretweet"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "retweet"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "quote"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/b;->a:Ljava/util/Set;

    .line 86
    const-string/jumbo v0, "show"

    invoke-static {v0}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/b;->b:Ljava/util/Set;

    .line 94
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 95
    const-string/jumbo v1, "resume"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/b;->c:Ljava/util/Set;

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/u;)V
    .locals 4

    .prologue
    .line 104
    new-instance v0, Lcom/twitter/android/av/a;

    invoke-direct {v0, p2}, Lcom/twitter/android/av/a;-><init>(Lcom/twitter/library/av/playback/u;)V

    new-instance v1, Lnm;

    invoke-direct {v1, p1}, Lnm;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/twitter/android/av/b$b;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/twitter/android/av/b$b;-><init>(Lcom/twitter/android/av/b$1;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/av/b;-><init>(Lcom/twitter/android/av/a;Lnm;Lcom/twitter/library/av/c;)V

    .line 106
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/av/a;Lnm;Lcom/twitter/library/av/c;)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p2, p0, Lcom/twitter/android/av/b;->d:Lnm;

    .line 117
    iput-object p3, p0, Lcom/twitter/android/av/b;->e:Lcom/twitter/library/av/c;

    .line 118
    invoke-virtual {p1}, Lcom/twitter/android/av/a;->a()Lcom/twitter/android/av/b$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/b;->f:Lcom/twitter/android/av/b$a;

    .line 119
    return-void
.end method

.method static a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 269
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method static a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 284
    if-eqz p0, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c()Ljava/lang/String;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_0

    .line 290
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "tweet"

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method static a(Lcom/twitter/library/av/playback/AVDataSource;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    invoke-interface {p0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    .line 308
    packed-switch v0, :pswitch_data_0

    .line 335
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 310
    :pswitch_0
    const-string/jumbo v0, "platform_audio_card"

    goto :goto_0

    .line 313
    :pswitch_1
    const-string/jumbo v0, "platform_amplify_card"

    goto :goto_0

    .line 316
    :pswitch_2
    const-string/jumbo v0, "gif_player"

    goto :goto_0

    .line 320
    :pswitch_3
    const-string/jumbo v0, "video_player"

    goto :goto_0

    .line 323
    :pswitch_4
    const-string/jumbo v0, "live_video_player"

    goto :goto_0

    .line 326
    :pswitch_5
    const-string/jumbo v0, "video_app_card_canvas"

    goto :goto_0

    .line 329
    :pswitch_6
    const-string/jumbo v0, "vine_player"

    goto :goto_0

    .line 332
    :pswitch_7
    const-string/jumbo v0, "periscope_player"

    goto :goto_0

    .line 308
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_6
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_7
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method static b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 302
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "tweet"

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/m;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p1, Lcom/twitter/library/av/m;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 150
    if-eqz v0, :cond_6

    .line 151
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->k()Lcom/twitter/library/av/playback/ax;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/ax;->c()Ljava/lang/String;

    move-result-object v0

    .line 152
    :goto_0
    iget-object v2, p1, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p1, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/av/b;->f:Lcom/twitter/android/av/b$a;

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/twitter/android/av/b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/av/b$a;)[Ljava/lang/String;

    move-result-object v5

    .line 156
    iget-object v0, p1, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/av/b;->f:Lcom/twitter/android/av/b$a;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/av/b;->a(Ljava/lang/String;Lcom/twitter/android/av/b$a;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 157
    iget v0, p1, Lcom/twitter/library/av/m;->h:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    const-string/jumbo v0, "2"

    move-object v2, v0

    .line 161
    :goto_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 163
    iget-object v3, p0, Lcom/twitter/android/av/b;->f:Lcom/twitter/android/av/b$a;

    invoke-virtual {v3, p1}, Lcom/twitter/android/av/b$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v3

    .line 165
    iget-object v4, p1, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 167
    iget-object v4, p1, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    invoke-static {v4}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    :goto_2
    iput-object v1, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->J:Ljava/lang/String;

    .line 170
    :cond_0
    iget-object v1, p1, Lcom/twitter/library/av/m;->s:Ljava/lang/String;

    iput-object v1, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->K:Ljava/lang/String;

    .line 172
    iget-object v1, p1, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 174
    iget-object v1, p1, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    .line 177
    :cond_1
    iget-object v1, p1, Lcom/twitter/library/av/m;->p:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 178
    iget-object v1, p1, Lcom/twitter/library/av/m;->p:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->N:J

    .line 181
    :cond_2
    iget-object v1, p1, Lcom/twitter/library/av/m;->m:Ljava/lang/String;

    iput-object v1, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->P:Ljava/lang/String;

    .line 183
    iget-object v1, p1, Lcom/twitter/library/av/m;->u:Lbyd;

    if-eqz v1, :cond_3

    .line 184
    iget-object v1, p1, Lcom/twitter/library/av/m;->u:Lbyd;

    iget v1, v1, Lbyd;->d:I

    iput v1, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ag:I

    .line 185
    iget-object v1, p1, Lcom/twitter/library/av/m;->u:Lbyd;

    iget-wide v6, v1, Lbyd;->e:J

    iput-wide v6, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ah:J

    .line 187
    iget-object v1, p1, Lcom/twitter/library/av/m;->u:Lbyd;

    instance-of v1, v1, Lbiu;

    if-eqz v1, :cond_3

    .line 189
    iget-object v1, p1, Lcom/twitter/library/av/m;->u:Lbyd;

    check-cast v1, Lbiu;

    .line 190
    iget-wide v6, v1, Lbiu;->a:J

    iput-wide v6, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ae:J

    .line 191
    iget-wide v6, v1, Lbiu;->b:J

    iput-wide v6, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->af:J

    .line 192
    new-instance v4, Lcom/twitter/analytics/feature/model/d$a;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/d$a;-><init>()V

    iget-object v1, v1, Lbiu;->c:Ljava/util/List;

    .line 194
    invoke-virtual {v4, v1}, Lcom/twitter/analytics/feature/model/d$a;->a(Ljava/util/List;)Lcom/twitter/analytics/feature/model/d$a;

    move-result-object v1

    .line 195
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/d$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/d;

    iput-object v1, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->at:Lcom/twitter/analytics/feature/model/d;

    .line 196
    const-string/jumbo v1, "live_video_heartbeat_event"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 197
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i()Lcom/twitter/analytics/model/ScribeLog;

    .line 201
    :cond_3
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 202
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->n(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 204
    iget-object v1, p0, Lcom/twitter/android/av/b;->e:Lcom/twitter/library/av/c;

    iget-object v2, p1, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/twitter/library/av/c;->a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;)V

    .line 207
    :cond_4
    iget-object v0, p1, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/av/b;->f:Lcom/twitter/android/av/b$a;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/av/b;->b(Ljava/lang/String;Lcom/twitter/android/av/b$a;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 208
    iget-object v0, p1, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    if-eqz v0, :cond_9

    .line 211
    iget-object v0, p0, Lcom/twitter/android/av/b;->d:Lnm;

    iget-object v1, p1, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    iget-object v4, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual/range {v0 .. v5}, Lnm;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/model/av/AVMediaPlaylist;Lcom/twitter/model/av/AVMedia;Ljava/lang/String;)V

    .line 221
    :cond_5
    :goto_3
    return-void

    :cond_6
    move-object v0, v1

    .line 151
    goto/16 :goto_0

    .line 157
    :cond_7
    const-string/jumbo v0, "1"

    move-object v2, v0

    goto/16 :goto_1

    .line 167
    :cond_8
    iget-object v1, p1, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    goto/16 :goto_2

    .line 212
    :cond_9
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The playlist cannot be null when sending a beacon."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_a
    new-instance v0, Lcpb;

    invoke-direct {v0}, Lcpb;-><init>()V

    const-string/jumbo v1, "data.event"

    iget-object v2, p1, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    .line 216
    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "The playlist cannot be null when sending a beacon."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 215
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_3
.end method

.method protected a(Ljava/lang/String;Lcom/twitter/android/av/b$a;)Z
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/twitter/android/av/b;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2, p1}, Lcom/twitter/android/av/b$a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/av/b$a;)[Ljava/lang/String;
    .locals 9
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 250
    invoke-static {p1}, Lcom/twitter/android/av/b;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Ljava/lang/String;

    move-result-object v0

    .line 251
    invoke-static {p1}, Lcom/twitter/android/av/b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-static {p1, p2}, Lcom/twitter/android/av/b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 255
    const-string/jumbo v3, ":"

    .line 256
    invoke-virtual {p3, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "%s:%s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p4}, Lcom/twitter/android/av/b$a;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object p3, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 257
    :cond_0
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    aput-object v2, v3, v8

    const/4 v0, 0x3

    aput-object p3, v3, v0

    return-object v3
.end method

.method protected b(Ljava/lang/String;Lcom/twitter/android/av/b$a;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 138
    sget-object v1, Lcom/twitter/android/av/b;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/twitter/android/av/b;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/twitter/android/av/b;->c:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 142
    invoke-virtual {p2, p1}, Lcom/twitter/android/av/b$a;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
