.class public Lcom/twitter/android/av/TweetEngagementView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lcom/twitter/media/ui/image/UserImageView;

.field private final c:Lcom/twitter/android/av/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const v0, 0x7f04003b

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    const v0, 0x7f130192

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/TweetEngagementView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/TweetEngagementView;->a:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f130191

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/TweetEngagementView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/av/TweetEngagementView;->b:Lcom/twitter/media/ui/image/UserImageView;

    .line 34
    const v0, 0x7f130145

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/TweetEngagementView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EngagementActionBar;

    .line 35
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->setBackgroundColor(I)V

    .line 36
    new-instance v1, Lcom/twitter/android/av/w;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/av/w;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;)V

    iput-object v1, p0, Lcom/twitter/android/av/TweetEngagementView;->c:Lcom/twitter/android/av/w;

    .line 37
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVPlayer;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/av/TweetEngagementView;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/av/TweetEngagementView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0085

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 67
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 66
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/av/TweetEngagementView;->b:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 69
    iget-object v0, p0, Lcom/twitter/android/av/TweetEngagementView;->c:Lcom/twitter/android/av/w;

    new-instance v1, Lcom/twitter/android/av/v;

    .line 70
    invoke-virtual {p0}, Lcom/twitter/android/av/TweetEngagementView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2}, Lcom/twitter/library/av/playback/AVPlayer;->R()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {v1, v2, v3, p3}, Lcom/twitter/android/av/v;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 69
    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/av/w;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/av/v;)V

    .line 71
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/av/TweetEngagementView;->c:Lcom/twitter/android/av/w;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/w;->a(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public setFragmentActivity(Lcom/twitter/app/common/base/TwitterFragmentActivity;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/av/TweetEngagementView;->c:Lcom/twitter/android/av/w;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/w;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 59
    return-void
.end method
