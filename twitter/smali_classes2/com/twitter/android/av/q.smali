.class public Lcom/twitter/android/av/q;
.super Lcom/twitter/android/av/r;
.source "Twttr"


# instance fields
.field private final a:D


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/android/av/r;-><init>()V

    .line 22
    const-string/jumbo v0, "media_autoplay_start_player_min_visible_fraction_5368"

    invoke-static {v0}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 24
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 54
    const-wide/high16 v0, 0x3fd0000000000000L    # 0.25

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    .line 57
    :goto_1
    return-void

    .line 24
    :sswitch_0
    const-string/jumbo v2, "01"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "15"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v2, "35"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v2, "50"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string/jumbo v2, "65"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string/jumbo v2, "80"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string/jumbo v2, "100"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    .line 26
    :pswitch_0
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    goto :goto_1

    .line 30
    :pswitch_1
    const-wide v0, 0x3fc3333333333333L    # 0.15

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    goto :goto_1

    .line 34
    :pswitch_2
    const-wide v0, 0x3fd6666666666666L    # 0.35

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    goto :goto_1

    .line 38
    :pswitch_3
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    goto :goto_1

    .line 42
    :pswitch_4
    const-wide v0, 0x3fe4cccccccccccdL    # 0.65

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    goto :goto_1

    .line 46
    :pswitch_5
    const-wide v0, 0x3fe999999999999aL    # 0.8

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    goto :goto_1

    .line 50
    :pswitch_6
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    goto :goto_1

    .line 24
    nop

    :sswitch_data_0
    .sparse-switch
        0x601 -> :sswitch_0
        0x624 -> :sswitch_1
        0x662 -> :sswitch_2
        0x69b -> :sswitch_3
        0x6bf -> :sswitch_4
        0x6f8 -> :sswitch_5
        0xbdf1 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method a()D
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/twitter/android/av/q;->a:D

    return-wide v0
.end method

.method protected a(Landroid/graphics/Rect;Landroid/graphics/Rect;)F
    .locals 1

    .prologue
    .line 66
    invoke-static {p2, p1}, Lcom/twitter/util/math/b;->b(Landroid/graphics/Rect;Landroid/graphics/Rect;)F

    move-result v0

    return v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method
