.class public Lcom/twitter/android/av/aa;
.super Lcom/twitter/android/av/ad;
.source "Twttr"


# instance fields
.field private final k:Lcom/twitter/android/card/w;

.field private final l:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

.field private final m:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/card/w;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/android/av/ad;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/android/av/aa;->k:Lcom/twitter/android/card/w;

    .line 33
    iput-object p2, p0, Lcom/twitter/android/av/aa;->l:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 34
    const-string/jumbo v0, "android_periscope_auth_broadcasting_v2_5514"

    const-string/jumbo v1, "enabled"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/av/aa;->m:Z

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/app/common/base/h;

    invoke-direct {v0}, Lcom/twitter/app/common/base/h;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/android/av/aa;->f:Z

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    iget-boolean v0, p0, Lcom/twitter/android/av/aa;->m:Z

    if-eqz v0, :cond_0

    const-class v0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 43
    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 44
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/aa;->a(Landroid/content/Intent;)V

    .line 45
    return-object v0

    .line 42
    :cond_0
    const-class v0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    goto :goto_0
.end method

.method a(Landroid/content/Intent;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 50
    const-string/jumbo v1, "tw"

    iget-object v2, p0, Lcom/twitter/android/av/aa;->j:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 51
    const-string/jumbo v1, "statusId"

    iget-object v2, p0, Lcom/twitter/android/av/aa;->j:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->G:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 52
    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/av/aa;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 53
    const-string/jumbo v1, "is_from_dock"

    iget-boolean v2, p0, Lcom/twitter/android/av/aa;->c:Z

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 54
    const-string/jumbo v1, "is_from_inline"

    iget-boolean v2, p0, Lcom/twitter/android/av/aa;->d:Z

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 55
    const-string/jumbo v1, "show_tw"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 56
    const-string/jumbo v1, "broadcast_id"

    iget-object v2, p0, Lcom/twitter/android/av/aa;->k:Lcom/twitter/android/card/w;

    invoke-virtual {v2}, Lcom/twitter/android/card/w;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    const-string/jumbo v1, "broadcaster_twitter_user_id"

    iget-object v2, p0, Lcom/twitter/android/av/aa;->k:Lcom/twitter/android/card/w;

    invoke-virtual {v2}, Lcom/twitter/android/card/w;->f()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 58
    const-string/jumbo v1, "is_live"

    iget-object v2, p0, Lcom/twitter/android/av/aa;->l:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v2}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->k()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 59
    const-string/jumbo v1, "is_360"

    iget-object v2, p0, Lcom/twitter/android/av/aa;->k:Lcom/twitter/android/card/w;

    invoke-virtual {v2}, Lcom/twitter/android/card/w;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "video_threesixty_enabled"

    .line 60
    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 59
    :cond_0
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    return-void
.end method
