.class public abstract Lcom/twitter/android/av/ExternalActionButton;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/ExternalActionButton$a;
    }
.end annotation


# instance fields
.field protected a:Landroid/widget/TextView;

.field protected b:Landroid/widget/ProgressBar;

.field protected c:Z

.field private d:Landroid/view/View$OnClickListener;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Landroid/content/Intent;

.field private i:Landroid/content/Intent;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Lcom/twitter/android/av/ExternalActionButton$a;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->d:Landroid/view/View$OnClickListener;

    .line 33
    iput-boolean v2, p0, Lcom/twitter/android/av/ExternalActionButton;->e:Z

    .line 34
    iput-boolean v2, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    .line 35
    iput-boolean v2, p0, Lcom/twitter/android/av/ExternalActionButton;->g:Z

    .line 36
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    .line 37
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->i:Landroid/content/Intent;

    .line 38
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->j:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->k:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    .line 44
    invoke-direct {p0, p1, v1, v2}, Lcom/twitter/android/av/ExternalActionButton;->b(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    iput-object v2, p0, Lcom/twitter/android/av/ExternalActionButton;->d:Landroid/view/View$OnClickListener;

    .line 33
    iput-boolean v1, p0, Lcom/twitter/android/av/ExternalActionButton;->e:Z

    .line 34
    iput-boolean v1, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    .line 35
    iput-boolean v1, p0, Lcom/twitter/android/av/ExternalActionButton;->g:Z

    .line 36
    iput-object v2, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    .line 37
    iput-object v2, p0, Lcom/twitter/android/av/ExternalActionButton;->i:Landroid/content/Intent;

    .line 38
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->j:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->k:Ljava/lang/String;

    .line 40
    iput-object v2, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    .line 49
    invoke-direct {p0, p1, p2, v1}, Lcom/twitter/android/av/ExternalActionButton;->b(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->d:Landroid/view/View$OnClickListener;

    .line 33
    iput-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->e:Z

    .line 34
    iput-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    .line 35
    iput-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->g:Z

    .line 36
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    .line 37
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->i:Landroid/content/Intent;

    .line 38
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->j:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->k:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/av/ExternalActionButton;->b(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/ExternalActionButton;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 135
    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->c:Z

    if-eqz v0, :cond_3

    .line 136
    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    if-nez v0, :cond_1

    .line 148
    iget-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->a:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->j:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    :cond_1
    return-void

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 143
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->g:Z

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0, v1}, Lcom/twitter/android/av/ExternalActionButton;->setVisibility(I)V

    goto :goto_0

    .line 148
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->k:Ljava/lang/String;

    goto :goto_1
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 126
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/av/ExternalActionButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 129
    invoke-super {p0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    .line 155
    invoke-direct {p0}, Lcom/twitter/android/av/ExternalActionButton;->b()V

    .line 157
    invoke-virtual {p0}, Lcom/twitter/android/av/ExternalActionButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 158
    new-instance v1, Lcom/twitter/android/av/ExternalActionButton$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/av/ExternalActionButton$2;-><init>(Lcom/twitter/android/av/ExternalActionButton;Landroid/content/pm/PackageManager;)V

    invoke-static {v1}, Lcre;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 164
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/av/ExternalActionButton$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/av/ExternalActionButton$1;-><init>(Lcom/twitter/android/av/ExternalActionButton;)V

    .line 165
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 172
    :cond_0
    return-void
.end method

.method protected abstract a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->c:Z

    if-eqz v0, :cond_0

    .line 230
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/ExternalActionButton;->setVisibility(I)V

    .line 232
    :cond_0
    return-void

    .line 230
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 1

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/twitter/android/av/ExternalActionButton;->e:Z

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->g:Z

    .line 221
    invoke-direct {p0}, Lcom/twitter/android/av/ExternalActionButton;->b()V

    .line 223
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/av/ExternalActionButton$a;->a(Z)V

    .line 226
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->d:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->d:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 194
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/av/ExternalActionButton;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    .line 196
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/av/ExternalActionButton;->f:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_3

    .line 214
    :cond_1
    :goto_1
    return-void

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->i:Landroid/content/Intent;

    goto :goto_0

    .line 200
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    if-eqz v1, :cond_4

    .line 201
    iget-object v2, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    iget-boolean v1, p0, Lcom/twitter/android/av/ExternalActionButton;->e:Z

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    invoke-interface {v2, v1}, Lcom/twitter/android/av/ExternalActionButton$a;->b(Z)V

    .line 205
    :cond_4
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/av/ExternalActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 206
    :catch_0
    move-exception v0

    .line 212
    :goto_3
    invoke-virtual {p0}, Lcom/twitter/android/av/ExternalActionButton;->a()V

    goto :goto_1

    .line 201
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 206
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 176
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 177
    if-eqz p1, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/twitter/android/av/ExternalActionButton;->a()V

    .line 181
    :cond_0
    return-void
.end method

.method public setActionText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/android/av/ExternalActionButton;->j:Ljava/lang/String;

    .line 100
    invoke-direct {p0}, Lcom/twitter/android/av/ExternalActionButton;->b()V

    .line 101
    return-void
.end method

.method public setEventListener(Lcom/twitter/android/av/ExternalActionButton$a;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/av/ExternalActionButton;->l:Lcom/twitter/android/av/ExternalActionButton$a;

    .line 59
    return-void
.end method

.method public setExternalUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    .line 66
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/av/ExternalActionButton;->a()V

    .line 68
    return-void
.end method

.method public setFallbackIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/twitter/android/av/ExternalActionButton;->i:Landroid/content/Intent;

    .line 83
    return-void
.end method

.method public setFallbackText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/twitter/android/av/ExternalActionButton;->k:Ljava/lang/String;

    .line 109
    invoke-direct {p0}, Lcom/twitter/android/av/ExternalActionButton;->b()V

    .line 110
    return-void
.end method

.method public setFallbackUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->i:Landroid/content/Intent;

    .line 91
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->i:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 92
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/twitter/android/av/ExternalActionButton;->d:Landroid/view/View$OnClickListener;

    .line 186
    return-void
.end method

.method public setReferrerUri(Landroid/net/Uri;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 73
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->h:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.REFERRER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 75
    :cond_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/av/ExternalActionButton;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 122
    return-void
.end method
