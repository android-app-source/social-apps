.class Lcom/twitter/android/av/aj$b;
.super Landroid/os/Handler;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/av/aj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/android/av/aj;)V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 78
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/av/aj$b;->a:Ljava/lang/ref/WeakReference;

    .line 79
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/av/aj;Lcom/twitter/android/av/aj$1;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/twitter/android/av/aj$b;-><init>(Lcom/twitter/android/av/aj;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/av/aj$b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/aj;

    .line 84
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->c()Lcom/twitter/android/av/aj$a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 85
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 87
    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->c()Lcom/twitter/android/av/aj$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/av/aj$a;->q()V

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
