.class public abstract Lcom/twitter/android/av/AVBaseCardCanvasView;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TButtonView:",
        "Landroid/view/View;",
        ">",
        "Landroid/widget/LinearLayout;",
        "Lcom/twitter/android/av/c;"
    }
.end annotation


# instance fields
.field protected final a:Landroid/view/View;

.field protected final b:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTButtonView;"
        }
    .end annotation
.end field

.field protected final c:Landroid/widget/ProgressBar;

.field protected final d:Landroid/widget/ImageView;

.field protected e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field private f:Lcom/twitter/library/av/control/a;

.field private h:Z

.field private final i:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/av/AVBaseCardCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    invoke-virtual {p0, v3}, Lcom/twitter/android/av/AVBaseCardCanvasView;->setOrientation(I)V

    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->getLayoutId()I

    move-result v0

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 60
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/AVBaseCardCanvasView;->a(Landroid/content/Context;)V

    .line 61
    const v0, 0x7f130183

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->b:Landroid/view/View;

    .line 62
    const v0, 0x7f130182

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->a:Landroid/view/View;

    .line 63
    const v0, 0x7f130189

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->c:Landroid/widget/ProgressBar;

    .line 64
    const v0, 0x7f130188

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    .line 66
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 67
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->b:Landroid/view/View;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->a:Landroid/view/View;

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->i:Ljava/lang/Iterable;

    .line 70
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/res/Configuration;)Lbyf;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lbyo;->c:Lbyf;

    return-object v0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    return-void
.end method

.method public a(D)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public a(IIZZ)V
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->b()V

    .line 151
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 152
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->f:Lcom/twitter/library/av/control/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->f:Lcom/twitter/library/av/control/a;

    iget-object v1, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->A()Lcom/twitter/library/av/playback/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/control/a;->b(Lcom/twitter/library/av/playback/aa;)V

    .line 155
    :cond_0
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->b()V

    .line 174
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public a(Lcom/twitter/library/av/o;)V
    .locals 0

    .prologue
    .line 250
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->h:Z

    .line 185
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->b()V

    .line 186
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->g()V

    .line 187
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 188
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->setKeepScreenOn(Z)V

    .line 189
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 90
    iput-object p1, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 91
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->a()V

    .line 92
    new-instance v0, Lcom/twitter/library/av/control/a;

    iget-object v1, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v1

    invoke-direct {v0, p0, v1, p0}, Lcom/twitter/library/av/control/a;-><init>(Landroid/view/View;Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/control/a$a;)V

    iput-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->f:Lcom/twitter/library/av/control/a;

    .line 93
    invoke-virtual {p1, p0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Lcom/twitter/library/av/k;)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 94
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->g()V

    .line 97
    invoke-virtual {p0, p2}, Lcom/twitter/android/av/AVBaseCardCanvasView;->a(Landroid/content/res/Configuration;)Lbyf;

    move-result-object v0

    .line 98
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->b()Lbyf;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 99
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Attachment playback mode does not match the expected mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 100
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->b()Lbyf;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->f:Lcom/twitter/library/av/control/a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/a;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 242
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 0

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->b()V

    .line 179
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 230
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->m()V

    .line 233
    :cond_0
    return-void
.end method

.method public a(ZJ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->h:Z

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 138
    iput-boolean v1, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->h:Z

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 141
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    return-void
.end method

.method public b(II)V
    .locals 1

    .prologue
    .line 211
    const/16 v0, 0x2bd

    if-ne v0, p1, :cond_1

    .line 212
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->a()V

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    const/16 v0, 0x2be

    if-ne v0, p1, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->b()V

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->a()V

    .line 160
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 161
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->h:Z

    .line 166
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->setKeepScreenOn(Z)V

    .line 168
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->b()V

    .line 169
    return-void
.end method

.method protected g()V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method public getContentView()Landroid/view/View;
    .locals 0

    .prologue
    .line 307
    return-object p0
.end method

.method public getHideableViews()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->i:Ljava/lang/Iterable;

    return-object v0
.end method

.method protected abstract getLayoutId()I
.end method

.method public h()V
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->b()V

    .line 203
    :cond_0
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 221
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->setKeepScreenOn(Z)V

    .line 222
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public l()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->h:Z

    if-eqz v0, :cond_2

    .line 266
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->n()V

    .line 273
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    goto :goto_0

    .line 267
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 268
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->m()V

    goto :goto_1

    .line 270
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    goto :goto_1
.end method

.method protected n()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->f:Lcom/twitter/library/av/control/a;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/a;->a()V

    .line 279
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->h:Z

    .line 281
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->o()V

    .line 282
    return-void
.end method

.method protected o()V
    .locals 3

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->h:Z

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    const v1, 0x7f0206bf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 290
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a075c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 298
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->e:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    const v1, 0x7f0206bd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 293
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0633

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    const v1, 0x7f0206be

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 296
    iget-object v0, p0, Lcom/twitter/android/av/AVBaseCardCanvasView;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 254
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 255
    const v1, 0x7f130188

    if-ne v1, v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/av/AVBaseCardCanvasView;->m()V

    .line 258
    :cond_0
    return-void
.end method

.method public setPartner(Lcom/twitter/model/av/Partner;)V
    .locals 0

    .prologue
    .line 110
    return-void
.end method
