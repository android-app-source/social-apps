.class public Lcom/twitter/android/av/ai;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/av/ai;


# instance fields
.field private final b:I


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {}, Lcom/twitter/android/av/ai;->c()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/av/ai;->b:I

    .line 61
    return-void
.end method

.method public static a()Lcom/twitter/android/av/ai;
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 39
    sget-object v0, Lcom/twitter/android/av/ai;->a:Lcom/twitter/android/av/ai;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/twitter/android/av/ai;

    invoke-direct {v0}, Lcom/twitter/android/av/ai;-><init>()V

    sput-object v0, Lcom/twitter/android/av/ai;->a:Lcom/twitter/android/av/ai;

    .line 41
    const-class v0, Lcom/twitter/android/av/ai;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 43
    :cond_0
    sget-object v0, Lcom/twitter/android/av/ai;->a:Lcom/twitter/android/av/ai;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Landroid/content/Context;J)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 107
    sget v0, Lazw$k;->av_view_counts_text:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-static {p0, p2, p3, v2}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v1

    .line 109
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)Z
    .locals 2

    .prologue
    .line 91
    invoke-static {}, Lcom/twitter/android/av/ai;->a()Lcom/twitter/android/av/ai;

    move-result-object v0

    iget v0, v0, Lcom/twitter/android/av/ai;->b:I

    int-to-long v0, v0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/av/playback/AVDataSource;)Z
    .locals 2

    .prologue
    .line 78
    invoke-static {}, Lcom/twitter/android/av/ai;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    invoke-interface {p0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 80
    :cond_0
    invoke-interface {p0}, Lcom/twitter/library/av/playback/AVDataSource;->p()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/android/av/ai;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 78
    :goto_0
    return v0

    .line 80
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 67
    const-string/jumbo v0, "video_view_counts_android_4840"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static c()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 116
    const-string/jumbo v1, "video_view_counts_android_4840"

    invoke-static {v1}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 134
    const v0, 0x7fffffff

    :goto_1
    :pswitch_0
    return v0

    .line 117
    :sswitch_0
    const-string/jumbo v3, "threshold_1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v3, "threshold_10"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_2
    const-string/jumbo v3, "threshold_25"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v3, "threshold_100"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string/jumbo v3, "threshold_1000"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 122
    :pswitch_1
    const/16 v0, 0xa

    goto :goto_1

    .line 125
    :pswitch_2
    const/16 v0, 0x19

    goto :goto_1

    .line 128
    :pswitch_3
    const/16 v0, 0x64

    goto :goto_1

    .line 131
    :pswitch_4
    const/16 v0, 0x3e8

    goto :goto_1

    .line 117
    :sswitch_data_0
    .sparse-switch
        0x2c55aa73 -> :sswitch_1
        0x2c55aa97 -> :sswitch_2
        0x32fa815d -> :sswitch_0
        0x5e5fa41d -> :sswitch_3
        0x6d94dfb3 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
