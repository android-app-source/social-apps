.class public abstract Lcom/twitter/android/av/b$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# instance fields
.field protected final a:Lcom/twitter/library/av/playback/ax;

.field protected final b:Z

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 1

    .prologue
    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349
    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->k()Lcom/twitter/library/av/playback/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/b$a;->a:Lcom/twitter/library/av/playback/ax;

    .line 350
    invoke-static {p1}, Lcom/twitter/android/av/b;->a(Lcom/twitter/library/av/playback/AVDataSource;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/b$a;->c:Ljava/lang/String;

    .line 351
    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->n()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/av/b$a;->b:Z

    .line 352
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/twitter/library/av/m;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/twitter/android/av/b$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract a(Ljava/lang/String;)Z
.end method

.method protected abstract b(Ljava/lang/String;)Z
.end method
