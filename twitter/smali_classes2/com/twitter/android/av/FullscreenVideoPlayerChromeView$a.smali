.class public Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/twitter/android/widget/EngagementActionBar;

.field private b:Lcom/twitter/android/av/w;

.field private c:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/widget/EngagementActionBar;
    .locals 3

    .prologue
    .line 422
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 423
    const v1, 0x7f0401a0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EngagementActionBar;

    iput-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->a:Lcom/twitter/android/widget/EngagementActionBar;

    .line 427
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->a:Lcom/twitter/android/widget/EngagementActionBar;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 437
    iput-object p2, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 438
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->c:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->c:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    move-object v2, v0

    .line 439
    :goto_0
    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 440
    :goto_1
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 441
    new-instance v1, Lcom/twitter/android/av/w;

    iget-object v3, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->a:Lcom/twitter/android/widget/EngagementActionBar;

    new-instance v4, Lcom/twitter/android/av/v;

    iget-object v5, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 442
    invoke-virtual {v5}, Lcom/twitter/library/av/playback/AVPlayer;->R()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    .line 443
    invoke-static {v2}, Lcom/twitter/android/av/b;->a(Lcom/twitter/library/av/playback/AVDataSource;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p1, v5, v2}, Lcom/twitter/android/av/v;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    invoke-direct {v1, v0, v3, v4}, Lcom/twitter/android/av/w;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lcom/twitter/android/av/v;)V

    iput-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->b:Lcom/twitter/android/av/w;

    .line 444
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->b:Lcom/twitter/android/av/w;

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/w;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 450
    :cond_0
    :goto_2
    return-void

    :cond_1
    move-object v2, v1

    .line 438
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 439
    goto :goto_1

    .line 448
    :cond_3
    iput-object v1, p0, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;->b:Lcom/twitter/android/av/w;

    goto :goto_2
.end method

.method public a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 458
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 460
    const-string/jumbo v1, "amplify_fullscreen_engagement_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
