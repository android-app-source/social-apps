.class public Lcom/twitter/android/av/g;
.super Lcom/twitter/android/av/b$a;
.source "Twttr"


# static fields
.field static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 28
    const-string/jumbo v1, "playlist_start"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 29
    const-string/jumbo v1, "click"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 30
    const-string/jumbo v1, "cta_impression_signup"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 31
    const-string/jumbo v1, "cta_impression_open"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 32
    const-string/jumbo v1, "cta_click_signup"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    const-string/jumbo v1, "cta_click_open"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/g;->c:Ljava/util/Set;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 37
    const-string/jumbo v1, "close"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    const-string/jumbo v1, "checkpoint"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/g;->d:Ljava/util/Set;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/twitter/android/av/b$a;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/m;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/av/g;->a:Lcom/twitter/library/av/playback/ax;

    iget-object v1, p1, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/av/playback/ax;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 59
    iget-object v0, p0, Lcom/twitter/android/av/g;->a:Lcom/twitter/library/av/playback/ax;

    invoke-interface {v0}, Lcom/twitter/library/av/playback/ax;->a()Lbis;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/c;->a(Lbis;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/av/AVMedia;)V

    .line 62
    iget-object v0, p1, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    instance-of v0, v0, Lcom/twitter/model/av/AudioPlaylist;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p1, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    check-cast v0, Lcom/twitter/model/av/AudioPlaylist;

    .line 64
    invoke-virtual {v0}, Lcom/twitter/model/av/AudioPlaylist;->m()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/av/g;->a(Ljava/util/Map;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 66
    iget-object v0, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    instance-of v0, v0, Lcom/twitter/model/av/Audio;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    check-cast v0, Lcom/twitter/model/av/Audio;

    .line 69
    invoke-virtual {v0}, Lcom/twitter/model/av/Audio;->m()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->V:Ljava/lang/String;

    .line 70
    invoke-virtual {v0}, Lcom/twitter/model/av/Audio;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->X:Ljava/lang/String;

    .line 73
    :cond_0
    iget-object v0, p1, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    check-cast v0, Lcom/twitter/model/av/AudioPlaylist;

    invoke-virtual {v0}, Lcom/twitter/model/av/AudioPlaylist;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->W:Ljava/lang/String;

    .line 78
    :goto_0
    return-object v1

    .line 75
    :cond_1
    iget-object v0, p1, Lcom/twitter/library/av/m;->g:Ljava/util/Map;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/av/g;->a(Ljava/util/Map;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto :goto_0
.end method

.method protected a(Ljava/util/Map;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    if-eqz p1, :cond_0

    .line 89
    const-string/jumbo v0, "playlist_url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    .line 90
    const-string/jumbo v0, "artist_name"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->R:Ljava/lang/String;

    .line 91
    const-string/jumbo v0, "integration_partner"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->S:Ljava/lang/String;

    .line 92
    const-string/jumbo v0, "image_url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->U:Ljava/lang/String;

    .line 93
    const-string/jumbo v0, "card_title"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->T:Ljava/lang/String;

    .line 95
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/twitter/android/av/g;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/android/av/g;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
