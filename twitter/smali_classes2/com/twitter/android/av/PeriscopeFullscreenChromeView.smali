.class public Lcom/twitter/android/av/PeriscopeFullscreenChromeView;
.super Lcom/twitter/android/av/GalleryVideoChromeView;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/player/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/PeriscopeFullscreenChromeView$a;
    }
.end annotation


# instance fields
.field private A:Lcom/twitter/android/av/PeriscopeFullscreenChromeView$a;

.field private s:Landroid/view/View;

.field private t:Lcom/twitter/media/ui/image/UserImageView;

.field private u:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field private v:Z

.field private w:Lcom/twitter/android/periscope/f;

.field private x:Landroid/view/ViewGroup;

.field private y:Lcom/twitter/android/periscope/c;

.field private z:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/android/av/GalleryVideoChromeView;-><init>(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/av/GalleryVideoChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method private X()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->x:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->x:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->removeView(Landroid/view/View;)V

    .line 239
    :cond_0
    return-void
.end method

.method private Y()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 306
    iget-boolean v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->v:Z

    .line 307
    iput-boolean v1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->v:Z

    .line 308
    if-eqz v0, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->H()V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->x:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 314
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04028c

    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->x:Landroid/view/ViewGroup;

    .line 315
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->x:Landroid/view/ViewGroup;

    const v2, 0x7f130625

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->z:Landroid/view/ViewGroup;

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->y:Lcom/twitter/android/periscope/c;

    if-eqz v0, :cond_2

    .line 318
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->y:Lcom/twitter/android/periscope/c;

    new-instance v2, Lcom/twitter/android/av/PeriscopeFullscreenChromeView$2;

    invoke-direct {v2, p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView$2;-><init>(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V

    invoke-interface {v0, v2}, Lcom/twitter/android/periscope/c;->a(Lcom/twitter/android/periscope/c$a;)V

    .line 326
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 327
    :goto_0
    if-eqz v0, :cond_3

    .line 328
    invoke-direct {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->Z()V

    .line 329
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->x:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->addView(Landroid/view/View;)V

    .line 331
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 326
    goto :goto_0
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->z:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 336
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 337
    iget-object v1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->z:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 338
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 339
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->z:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 341
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)Lcom/twitter/android/av/PeriscopeFullscreenChromeView$a;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->A:Lcom/twitter/android/av/PeriscopeFullscreenChromeView$a;

    return-object v0
.end method

.method private a(Lcom/twitter/media/ui/image/UserImageView;)V
    .locals 4

    .prologue
    .line 68
    const v0, 0x7f0e03a6

    const v1, 0x7f0e03a5

    const/high16 v2, 0x7f110000

    sget-object v3, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/media/ui/image/UserImageView;IIILcom/twitter/media/ui/image/config/g;)V

    .line 72
    new-instance v0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView$1;-><init>(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->z:Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method protected D()Z
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public I()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setVisibility(I)V

    .line 139
    :cond_0
    return-void
.end method

.method public J()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setVisibility(I)V

    .line 145
    :cond_0
    return-void
.end method

.method K()V
    .locals 2

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->v:Z

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setHeartsMarginFactor(I)V

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setHeartsMarginFactor(I)V

    goto :goto_0
.end method

.method public L()V
    .locals 0

    .prologue
    .line 269
    return-void
.end method

.method public M()V
    .locals 0

    .prologue
    .line 272
    return-void
.end method

.method public N()V
    .locals 0

    .prologue
    .line 280
    return-void
.end method

.method public O()V
    .locals 0

    .prologue
    .line 286
    return-void
.end method

.method public P()Z
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    return v0
.end method

.method public Q()V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->w:Lcom/twitter/android/periscope/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/f;->a(Z)V

    .line 346
    invoke-direct {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->X()V

    .line 347
    return-void
.end method

.method public R()V
    .locals 0

    .prologue
    .line 356
    return-void
.end method

.method public S()V
    .locals 0

    .prologue
    .line 359
    return-void
.end method

.method public T()V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public U()V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public V()V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public W()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 350
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v0}, Lcom/twitter/android/av/ExternalActionButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    .line 123
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 125
    :cond_0
    return-void
.end method

.method public a(IF)V
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    .line 157
    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    .line 158
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 159
    invoke-virtual {v0, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    .line 160
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 161
    return-void
.end method

.method public a(II)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x96

    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    mul-int/lit8 v2, p2, -0x1

    int-to-float v2, v2

    .line 103
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 104
    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 105
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 106
    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->getRightAlignedViews()[Landroid/view/View;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 107
    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    mul-int/lit8 v4, p1, -0x1

    int-to-float v4, v4

    .line 108
    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 109
    invoke-virtual {v3, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 110
    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_0
    return-void
.end method

.method protected a(IIII)V
    .locals 4

    .prologue
    .line 165
    .line 167
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v0}, Lcom/twitter/android/av/ExternalActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 168
    if-eqz v0, :cond_1

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 169
    :goto_0
    add-int/2addr v0, p2

    .line 170
    iget-object v1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v1}, Lcom/twitter/android/av/ExternalActionButton;->getHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 171
    sub-int v2, p3, p1

    iget-object v3, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v3}, Lcom/twitter/android/av/ExternalActionButton;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 172
    if-lez v2, :cond_0

    .line 173
    add-int/2addr p1, v2

    .line 174
    sub-int/2addr p3, v2

    .line 176
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {v2, p1, v0, p3, v1}, Lcom/twitter/android/av/ExternalActionButton;->layout(IIII)V

    .line 177
    return-void

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->X()V

    .line 213
    invoke-super {p0, p1}, Lcom/twitter/android/av/GalleryVideoChromeView;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 214
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public a(ZJ)V
    .locals 2

    .prologue
    .line 218
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/av/GalleryVideoChromeView;->a(ZJ)V

    .line 219
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i()V

    .line 220
    return-void
.end method

.method public a(IJ)Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method

.method protected b(Lcom/twitter/model/av/a;)Lcom/twitter/library/av/model/d;
    .locals 2

    .prologue
    .line 207
    new-instance v0, Lcom/twitter/android/av/z;

    check-cast p1, Lcom/twitter/library/av/model/c;

    iget-boolean v1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->v:Z

    invoke-direct {v0, p1, v1}, Lcom/twitter/android/av/z;-><init>(Lcom/twitter/library/av/model/c;Z)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->w:Lcom/twitter/android/periscope/f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/f;->a(Z)V

    .line 300
    invoke-direct {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->Y()V

    .line 302
    :cond_0
    return-void
.end method

.method public getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->u:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/ChatRoomView;

    return-object v0
.end method

.method public getPreview()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextureView()Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    return-object v0
.end method

.method protected k()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->s:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->s:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->addView(Landroid/view/View;I)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->k:Lcom/twitter/android/av/ExternalActionButton;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->addView(Landroid/view/View;)V

    .line 90
    :cond_1
    return-void
.end method

.method public setAppCardViewProvider(Lcom/twitter/android/periscope/c;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->y:Lcom/twitter/android/periscope/c;

    .line 378
    return-void
.end method

.method public setBroadcastProgress(Lcom/twitter/android/periscope/f;)V
    .locals 1

    .prologue
    .line 381
    iput-object p1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->w:Lcom/twitter/android/periscope/f;

    .line 382
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->w:Lcom/twitter/android/periscope/f;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    invoke-direct {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->Y()V

    .line 387
    :goto_0
    return-void

    .line 385
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->X()V

    goto :goto_0
.end method

.method public setIsLive(Z)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->w:Lcom/twitter/android/periscope/f;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/f;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-boolean v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->v:Z

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    .line 188
    :goto_0
    iput-boolean p1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->v:Z

    .line 189
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->K()V

    .line 190
    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->H()V

    .line 194
    :cond_0
    return-void

    .line 187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPlayPauseAlpha(F)V
    .locals 0

    .prologue
    .line 374
    return-void
.end method

.method public setPlayPauseClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method public setProfileClickListener(Lcom/twitter/android/av/PeriscopeFullscreenChromeView$a;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->A:Lcom/twitter/android/av/PeriscopeFullscreenChromeView$a;

    .line 129
    return-void
.end method

.method public setProfileUser(Lcom/twitter/model/core/TwitterUser;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 133
    return-void
.end method

.method public setTextureView(Landroid/view/TextureView;)V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public setThumbImageUrlLoader(Ldae;)V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method public setThumbnail(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 353
    return-void
.end method

.method protected setupInternalViews(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/twitter/android/av/GalleryVideoChromeView;->setupInternalViews(Landroid/content/Context;)V

    .line 61
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04028f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->s:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->s:Landroid/view/View;

    const v1, 0x7f130629

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iput-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->u:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 63
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->s:Landroid/view/View;

    const v1, 0x7f130628

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    .line 64
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->t:Lcom/twitter/media/ui/image/UserImageView;

    invoke-direct {p0, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->a(Lcom/twitter/media/ui/image/UserImageView;)V

    .line 65
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 230
    invoke-super {p0}, Lcom/twitter/android/av/GalleryVideoChromeView;->w()V

    .line 231
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i()V

    .line 232
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->Q()V

    .line 233
    return-void
.end method

.method public z()V
    .locals 0

    .prologue
    .line 224
    invoke-super {p0}, Lcom/twitter/android/av/GalleryVideoChromeView;->z()V

    .line 225
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->Q()V

    .line 226
    return-void
.end method
