.class public Lcom/twitter/android/av/PeriscopeBadge;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method private a(IIZ)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 73
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 74
    if-eqz p3, :cond_0

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0bea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/av/PeriscopeBadge;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    return-void
.end method


# virtual methods
.method public a(IZ)V
    .locals 6

    .prologue
    const v5, 0x7f0a0a83

    const/4 v4, 0x0

    .line 42
    if-lez p1, :cond_0

    .line 44
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->b:Landroid/widget/TextView;

    const v1, 0x7f0206c2

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    int-to-long v2, p1

    invoke-static {v0, v2, v3, v4}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/av/PeriscopeBadge;->a(Ljava/lang/String;)V

    .line 46
    const v0, 0x7f02008f

    invoke-direct {p0, v0, v5, p2}, Lcom/twitter/android/av/PeriscopeBadge;->a(IIZ)V

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 49
    const v0, 0x7f020090

    invoke-direct {p0, v0, v5, p2}, Lcom/twitter/android/av/PeriscopeBadge;->a(IIZ)V

    goto :goto_0
.end method

.method public b(IZ)V
    .locals 6

    .prologue
    const v5, 0x7f0a0ad0

    const/4 v4, 0x0

    .line 54
    if-lez p1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 57
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    int-to-long v2, p1

    invoke-static {v0, v2, v3, v4}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Lcom/twitter/android/av/PeriscopeBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0649

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/av/PeriscopeBadge;->a(Ljava/lang/String;)V

    .line 59
    const v0, 0x7f02008c

    invoke-direct {p0, v0, v5, p2}, Lcom/twitter/android/av/PeriscopeBadge;->a(IIZ)V

    .line 64
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    const v0, 0x7f02008e

    invoke-direct {p0, v0, v5, p2}, Lcom/twitter/android/av/PeriscopeBadge;->a(IIZ)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    const v0, 0x7f130620

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/PeriscopeBadge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->a:Landroid/widget/TextView;

    .line 38
    const v0, 0x7f130621

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/PeriscopeBadge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/PeriscopeBadge;->b:Landroid/widget/TextView;

    .line 39
    return-void
.end method
