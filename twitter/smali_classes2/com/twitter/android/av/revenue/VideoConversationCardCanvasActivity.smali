.class public Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;
.super Lcom/twitter/android/av/RevenueCardCanvasActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;
    }
.end annotation


# instance fields
.field private q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/widget/Button;",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

.field private s:Lcom/twitter/android/card/b;

.field private t:Lcom/twitter/android/card/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    .line 156
    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->t:Lcom/twitter/android/card/d;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->o:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 159
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 161
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->a(Ljava/lang/String;I)V

    .line 163
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)Lcom/twitter/library/av/playback/u;
    .locals 2

    .prologue
    .line 187
    const-string/jumbo v0, "avdatasource"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVDataSource;

    .line 188
    if-eqz v0, :cond_0

    new-instance v1, Lcom/twitter/library/av/playback/u;

    invoke-direct {v1, v0}, Lcom/twitter/library/av/playback/u;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->a(Landroid/os/Bundle;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->s:Lcom/twitter/android/card/b;

    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-wide v2, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->g:J

    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    .line 167
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v4

    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v5

    move-object v1, p1

    move v6, p2

    .line 166
    invoke-interface/range {v0 .. v6}, Lcom/twitter/android/card/b;->a(Ljava/lang/String;JLcax;Lcgi;I)V

    .line 168
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->b()V

    .line 174
    invoke-virtual {p0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 175
    const-string/jumbo v1, "video_conversation_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    .line 176
    new-instance v0, Lcom/twitter/android/card/c;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->s:Lcom/twitter/android/card/b;

    .line 177
    new-instance v0, Lcom/twitter/android/card/f;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->t:Lcom/twitter/android/card/d;

    .line 179
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/revenue/VideoConversationCardView;

    .line 180
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/revenue/VideoConversationCardView;->setVideoConversationCardData(Lcom/twitter/android/av/revenue/VideoConversationCardData;)V

    .line 181
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->s:Lcom/twitter/android/card/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/revenue/VideoConversationCardView;->setActionHandler(Lcom/twitter/android/card/b;)V

    .line 182
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-boolean v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->i:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/revenue/VideoConversationCardView;->setBottomDivider(Z)V

    .line 183
    return-void
.end method

.method protected b(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 57
    new-instance v5, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$1;

    invoke-direct {v5, p0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$1;-><init>(Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const v0, 0x7f13058b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    .line 67
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 69
    const v0, 0x7f130848

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const v0, 0x7f130849

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "@"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    iget-object v3, v3, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    const v0, 0x7f130847

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 76
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 79
    :cond_0
    const v0, 0x7f13084a

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 80
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    const v0, 0x7f13084b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 83
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-boolean v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->i:Z

    if-eqz v0, :cond_6

    .line 87
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    const v0, 0x7f13011b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    sget-object v1, Lcom/twitter/android/revenue/card/h;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 91
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    const v0, 0x7f13084c

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 93
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 96
    :cond_2
    const v0, 0x7f130183

    .line 97
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    move-object v1, v2

    check-cast v1, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/twitter/util/collection/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/j;

    move-result-object v1

    const/4 v0, 0x3

    new-array v3, v0, [Lcom/twitter/util/collection/j;

    const v0, 0x7f13084e

    .line 99
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v6, 0x7f13084d

    .line 100
    invoke-virtual {p0, v6}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 98
    invoke-static {v0, v6}, Lcom/twitter/util/collection/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/j;

    move-result-object v0

    aput-object v0, v3, v4

    const v0, 0x7f130850

    .line 102
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v6, 0x7f13084f

    .line 103
    invoke-virtual {p0, v6}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 101
    invoke-static {v0, v6}, Lcom/twitter/util/collection/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/j;

    move-result-object v0

    aput-object v0, v3, v10

    const/4 v6, 0x2

    const v0, 0x7f130852

    .line 105
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v7, 0x7f130851

    .line 106
    invoke-virtual {p0, v7}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 104
    invoke-static {v0, v7}, Lcom/twitter/util/collection/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/j;

    move-result-object v0

    aput-object v0, v3, v6

    .line 96
    invoke-static {v1, v3}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->q:Ljava/util/Map;

    move v3, v4

    .line 108
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 109
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/j;

    invoke-virtual {v0}, Lcom/twitter/util/collection/j;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 111
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 113
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 114
    invoke-virtual {p0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 115
    const v8, 0x7f0a0212

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v1, v9, v4

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 117
    invoke-virtual {p0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->getApplication()Landroid/app/Application;

    move-result-object v8

    invoke-static {v1, v7, v8}, Lcom/twitter/android/revenue/i;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/text/Spannable;

    move-result-object v1

    sget-object v7, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    .line 116
    invoke-virtual {v0, v1, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 120
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->q:Ljava/util/Map;

    iget-object v7, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v7, v7, Lcom/twitter/android/av/revenue/VideoConversationCardData;->b:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v7

    invoke-interface {v1, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoConversationCardData;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_3

    .line 124
    const v1, 0x7f0200cc

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 127
    :cond_3
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 128
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/util/collection/j;

    invoke-virtual {v1}, Lcom/twitter/util/collection/j;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 129
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/util/collection/j;

    invoke-virtual {v1}, Lcom/twitter/util/collection/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 132
    :cond_4
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v1, v7, :cond_5

    .line 133
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setStateListAnimator(Landroid/animation/StateListAnimator;)V

    .line 108
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 137
    :cond_6
    return-void
.end method

.method protected q()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;->r:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoConversationCardData;->f:Ljava/lang/String;

    return-object v0
.end method

.method protected s()I
    .locals 1

    .prologue
    .line 151
    const v0, 0x7f040440

    return v0
.end method
