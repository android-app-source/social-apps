.class public Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;
.super Lcom/twitter/android/av/RevenueCardCanvasActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/revenue/VideoCardCanvasActivity$a;
    }
.end annotation


# instance fields
.field private q:Lcom/twitter/android/av/revenue/VideoAppCardData;

.field private r:Lcom/twitter/library/util/s;

.field private s:Lcom/twitter/android/card/CardActionHelper;

.field private t:Landroid/widget/Button;

.field private u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    .line 123
    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->s:Lcom/twitter/android/card/CardActionHelper;

    iget-object v2, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v2, v2, Lcom/twitter/android/av/revenue/VideoAppCardData;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->u:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v4, v4, Lcom/twitter/android/av/revenue/VideoAppCardData;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/card/CardActionHelper;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 126
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method


# virtual methods
.method protected b()V
    .locals 4

    .prologue
    .line 130
    invoke-super {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->b()V

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    const-string/jumbo v1, "video_canvas_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/revenue/VideoAppCardData;

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    .line 134
    new-instance v0, Lcom/twitter/android/card/c;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    .line 135
    new-instance v1, Lcom/twitter/android/card/f;

    invoke-direct {v1, p0}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    .line 136
    iget-object v2, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v1, v2}, Lcom/twitter/android/card/d;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 137
    iget-object v2, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    invoke-static {v2}, Lcom/twitter/library/card/CardContextFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/card/CardContext;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/card/CardContext;)V

    .line 138
    new-instance v2, Lcom/twitter/android/card/CardActionHelper;

    const-string/jumbo v3, "video_app_card_canvas"

    invoke-direct {v2, v0, v1, v3}, Lcom/twitter/android/card/CardActionHelper;-><init>(Lcom/twitter/android/card/b;Lcom/twitter/android/card/d;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->s:Lcom/twitter/android/card/CardActionHelper;

    .line 141
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/revenue/VideoAppCardView;

    .line 142
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/revenue/VideoAppCardView;->setVideoAppCardData(Lcom/twitter/android/av/revenue/VideoAppCardData;)V

    .line 143
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->s:Lcom/twitter/android/card/CardActionHelper;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/revenue/VideoAppCardView;->setActionHelper(Lcom/twitter/android/card/CardActionHelper;)V

    .line 144
    return-void
.end method

.method protected b(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 10

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoAppCardData;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->u:Ljava/lang/String;

    .line 46
    const v0, 0x7f130844

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    .line 47
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoAppCardData;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 48
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v1, v1, Lcom/twitter/android/av/revenue/VideoAppCardData;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 53
    :goto_0
    const v0, 0x7f130183

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->t:Landroid/widget/Button;

    .line 54
    const v0, 0x7f13011b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 55
    const v1, 0x7f1307fe

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/TextView;

    .line 56
    const v1, 0x7f130843

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 57
    const v1, 0x7f1307fd

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 59
    iget-object v2, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v2, v2, Lcom/twitter/android/av/revenue/VideoAppCardData;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoAppCardData;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    const/4 v6, 0x0

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoAppCardData;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 66
    :goto_1
    const v2, 0x7f020655

    const v3, 0x7f020659

    const v4, 0x7f020657

    .line 69
    invoke-virtual {p0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0e04fe

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    const/high16 v7, 0x40a00000    # 5.0f

    move-object v0, p0

    .line 66
    invoke-static/range {v0 .. v7}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Landroid/widget/LinearLayout;IIIIFF)V

    .line 73
    :cond_0
    const v0, 0x7f1305c7

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 74
    invoke-virtual {p0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0a0c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v5, v5, Lcom/twitter/android/av/revenue/VideoAppCardData;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoAppCardData;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoAppCardData;->c:Ljava/lang/String;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :cond_1
    new-instance v0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity$1;-><init>(Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;)V

    iput-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->r:Lcom/twitter/library/util/s;

    .line 87
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->r:Lcom/twitter/library/util/s;

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->t:Landroid/widget/Button;

    instance-of v0, v0, Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_3

    .line 91
    new-instance v1, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity$2;

    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->t:Landroid/widget/Button;

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity$2;-><init>(Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;Lcom/twitter/ui/widget/TwitterButton;)V

    move-object v0, v1

    .line 101
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->t:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoAppCardData;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/revenue/i;->b(Ljava/lang/String;)I

    move-result v0

    .line 103
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->t:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 104
    return-void

    .line 50
    :cond_2
    const v1, 0x7f0203f7

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 99
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->r:Lcom/twitter/library/util/s;

    goto :goto_2

    .line 65
    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method protected q()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/av/revenue/VideoCardCanvasActivity;->q:Lcom/twitter/android/av/revenue/VideoAppCardData;

    iget-object v0, v0, Lcom/twitter/android/av/revenue/VideoAppCardData;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected s()I
    .locals 1

    .prologue
    .line 118
    const v0, 0x7f040438

    return v0
.end method
