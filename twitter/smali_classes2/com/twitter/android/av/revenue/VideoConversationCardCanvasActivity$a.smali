.class public Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;
.super Lcom/twitter/android/av/AVCardCanvasActivity$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/av/revenue/VideoConversationCardData;

.field private final b:Lcom/twitter/library/av/playback/AVDataSource;


# direct methods
.method public constructor <init>(Lcom/twitter/android/av/revenue/VideoConversationCardData;)V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;-><init>(Lcom/twitter/android/av/revenue/VideoConversationCardData;Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 201
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/av/revenue/VideoConversationCardData;Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/twitter/android/av/AVCardCanvasActivity$a;-><init>()V

    .line 205
    iput-object p1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;->a:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    .line 206
    iput-object p2, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 207
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 211
    invoke-super {p0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 212
    const-string/jumbo v1, "video_conversation_data"

    iget-object v2, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;->a:Lcom/twitter/android/av/revenue/VideoConversationCardData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 213
    iget-object v1, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;->b:Lcom/twitter/library/av/playback/AVDataSource;

    if-eqz v1, :cond_0

    .line 214
    const-string/jumbo v1, "avdatasource"

    iget-object v2, p0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;->b:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 216
    :cond_0
    return-object v0
.end method

.method protected a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/android/av/AVCardCanvasActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    const-class v0, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity;

    return-object v0
.end method
