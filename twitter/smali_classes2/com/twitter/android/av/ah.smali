.class public Lcom/twitter/android/av/ah;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)Lcom/twitter/library/av/VideoPlayerView;
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/twitter/android/av/ah$1;->a:[I

    invoke-virtual {p3}, Lcom/twitter/library/av/VideoPlayerView$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 55
    new-instance v0, Lcom/twitter/library/av/VideoPlayerView;

    invoke-direct {v0, p1, p2, p3}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    :goto_0
    return-object v0

    .line 34
    :pswitch_0
    new-instance v0, Lcom/twitter/library/av/PeriscopeFullscreenVideoPlayerView;

    new-instance v1, Lcom/twitter/library/av/w;

    invoke-direct {v1}, Lcom/twitter/library/av/w;-><init>()V

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/twitter/library/av/PeriscopeFullscreenVideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    goto :goto_0

    .line 38
    :pswitch_1
    new-instance v0, Lcom/twitter/library/av/VideoPlayerView;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bq;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/fullscreen/bq;-><init>()V

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    goto :goto_0

    .line 42
    :pswitch_2
    new-instance v0, Lcom/twitter/library/av/VideoPlayerView;

    new-instance v1, Lcom/twitter/library/av/x;

    invoke-direct {v1}, Lcom/twitter/library/av/x;-><init>()V

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    goto :goto_0

    .line 47
    :pswitch_3
    new-instance v0, Lcom/twitter/library/av/VideoPlayerView;

    new-instance v1, Lcom/twitter/library/av/w;

    invoke-direct {v1}, Lcom/twitter/library/av/w;-><init>()V

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    goto :goto_0

    .line 52
    :pswitch_4
    new-instance v0, Lcom/twitter/library/av/LiveVideoPlayerView;

    invoke-direct {v0, p1, p2, p3}, Lcom/twitter/library/av/LiveVideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
