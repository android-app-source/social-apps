.class public Lcom/twitter/android/av/watchmode/g;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/av/watchmode/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/twitter/android/av/watchmode/g;
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 34
    sget-object v0, Lcom/twitter/android/av/watchmode/g;->a:Lcom/twitter/android/av/watchmode/g;

    if-nez v0, :cond_0

    .line 35
    const-class v0, Lcom/twitter/android/av/watchmode/g;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 36
    new-instance v0, Lcom/twitter/android/av/watchmode/g;

    invoke-direct {v0}, Lcom/twitter/android/av/watchmode/g;-><init>()V

    sput-object v0, Lcom/twitter/android/av/watchmode/g;->a:Lcom/twitter/android/av/watchmode/g;

    .line 39
    :cond_0
    sget-object v0, Lcom/twitter/android/av/watchmode/g;->a:Lcom/twitter/android/av/watchmode/g;

    return-object v0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 91
    instance-of v0, p0, Lcom/twitter/app/main/MainActivity;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/twitter/android/TweetActivity;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVDataSource;)Z
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/twitter/android/av/watchmode/g;->a()Lcom/twitter/android/av/watchmode/g;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/twitter/android/av/watchmode/g;->b(Landroid/content/Context;Lcom/twitter/library/av/playback/AVDataSource;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 99
    instance-of v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeRecyclerView;

    if-nez v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v1

    .line 103
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 105
    instance-of v2, v0, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v2, :cond_0

    .line 109
    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .line 110
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v2

    .line 113
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    if-nez v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 83
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public b(Landroid/content/Context;Lcom/twitter/library/av/playback/AVDataSource;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 69
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v1

    .line 71
    :goto_0
    if-eqz v1, :cond_0

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    if-eq v1, v0, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 73
    :cond_0
    invoke-interface {p2}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/av/watchmode/g;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 74
    invoke-static {p1}, Lcom/twitter/android/av/watchmode/g;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "immersive_video_android_4464"

    .line 75
    invoke-static {v1}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 71
    :goto_1
    return v0

    .line 69
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 75
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
