.class public Lcom/twitter/android/av/watchmode/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/watchmode/b;


# instance fields
.field private final a:Lqi;

.field private final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final d:Lcom/twitter/library/av/playback/AVDataSource;

.field private final e:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lqm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lqi;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/c;->a:Lqi;

    .line 46
    iput-object p2, p0, Lcom/twitter/android/av/watchmode/c;->d:Lcom/twitter/library/av/playback/AVDataSource;

    .line 47
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/c;->a:Lqi;

    invoke-virtual {v0, p2}, Lqi;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/c;->e:Lrx/c;

    .line 48
    const-string/jumbo v0, "watch_mode"

    invoke-virtual {p3, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 49
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, p3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const-string/jumbo v1, "original_video"

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/c;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 51
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, p3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const-string/jumbo v1, "suggested_video"

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/c;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 53
    return-void
.end method

.method private a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/android/av/watchmode/c;->c(Lcom/twitter/library/av/playback/AVDataSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/c;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 69
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/c;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    goto :goto_0
.end method

.method private b(Lcom/twitter/library/av/playback/AVDataSource;)Lbyf;
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/twitter/android/av/watchmode/c;->c(Lcom/twitter/library/av/playback/AVDataSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    sget-object v0, Lbyo;->b:Lbyf;

    .line 104
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lbyo;->h:Lbyf;

    goto :goto_0
.end method

.method private c(Lcom/twitter/library/av/playback/AVDataSource;)Z
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/c;->d:Lcom/twitter/library/av/playback/AVDataSource;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/android/av/video/f;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/twitter/android/av/video/f$a;

    invoke-direct {v0}, Lcom/twitter/android/av/video/f$a;-><init>()V

    .line 78
    invoke-virtual {v0, p2}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->i:Lcom/twitter/library/av/VideoPlayerView$Mode;

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/VideoPlayerView$Mode;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    .line 80
    invoke-static {p2}, Lcom/twitter/library/av/model/b;->b(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/model/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/model/b;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    .line 81
    invoke-virtual {p1}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getChromeView()Lcom/twitter/library/av/control/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/control/e;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    .line 82
    invoke-direct {p0, p2}, Lcom/twitter/android/av/watchmode/c;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    .line 83
    invoke-direct {p0, p2}, Lcom/twitter/android/av/watchmode/c;->b(Lcom/twitter/library/av/playback/AVDataSource;)Lbyf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lbyf;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/twitter/android/av/video/f$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/video/f;

    return-object v0
.end method

.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lqm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/c;->e:Lrx/c;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/c;->a:Lqi;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 92
    :cond_0
    return-void
.end method
