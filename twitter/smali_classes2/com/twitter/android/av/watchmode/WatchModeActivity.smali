.class public Lcom/twitter/android/av/watchmode/WatchModeActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/twitter/android/av/watchmode/WatchModeActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".seed_video"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/watchmode/WatchModeActivity;->a:Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/twitter/android/av/watchmode/WatchModeActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".scribe_association"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/watchmode/WatchModeActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 38
    if-nez p2, :cond_0

    new-instance p2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {p2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    .line 40
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/av/watchmode/WatchModeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/av/watchmode/WatchModeActivity;->a:Ljava/lang/String;

    .line 41
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/av/watchmode/WatchModeActivity;->b:Ljava/lang/String;

    .line 42
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 40
    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 54
    return-object v0
.end method

.method protected synthetic a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->h(Lank;)Lqx;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->h(Lank;)Lqx;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->h(Lank;)Lqx;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lqr;
    .locals 5

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 61
    sget-object v0, Lcom/twitter/android/av/watchmode/WatchModeActivity;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVDataSource;

    .line 62
    sget-object v2, Lcom/twitter/android/av/watchmode/WatchModeActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 63
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 64
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 67
    :cond_1
    invoke-static {}, Lqq;->a()Lqq$a;

    move-result-object v2

    .line 68
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lqq$a;->a(Lamu;)Lqq$a;

    move-result-object v2

    new-instance v3, Lano;

    new-instance v4, Lqs;

    invoke-direct {v4, v0, v1}, Lqs;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    invoke-direct {v3, v4}, Lano;-><init>(Lanr;)V

    .line 69
    invoke-virtual {v2, v3}, Lqq$a;->a(Lano;)Lqq$a;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lqq$a;->a()Lqr;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->d(Lank;)Lqr;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->d(Lank;)Lqr;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->d(Lank;)Lqr;

    move-result-object v0

    return-object v0
.end method

.method protected h(Lank;)Lqx;
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/WatchModeActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lqr;

    .line 78
    new-instance v1, Lant;

    invoke-direct {v1, p0, p1}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    invoke-interface {v0, v1}, Lqr;->a(Lant;)Lqx;

    move-result-object v0

    return-object v0
.end method
