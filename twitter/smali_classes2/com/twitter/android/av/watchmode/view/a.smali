.class public Lcom/twitter/android/av/watchmode/view/a;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "Twttr"

# interfaces
.implements Lane;
.implements Lcom/twitter/android/av/aj$a;
.implements Lcom/twitter/android/av/watchmode/view/i;
.implements Lpi$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/watchmode/view/a$a;
    }
.end annotation


# instance fields
.field a:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field b:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field c:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field d:Lpi;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field e:Lbix;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final f:Lcom/twitter/android/widget/NewItemBannerView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final g:Lcom/twitter/android/av/aj;

.field private final h:Landroid/support/v7/widget/RecyclerView;

.field private final i:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/NewItemBannerView;Landroid/support/v7/widget/RecyclerView;Landroid/os/Handler;Lcom/twitter/android/av/aj;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 56
    iput-object p3, p0, Lcom/twitter/android/av/watchmode/view/a;->i:Landroid/os/Handler;

    .line 57
    iput-object p2, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    .line 58
    iput-object p4, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    .line 59
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/aj;->a(Lcom/twitter/android/av/aj$a;)V

    .line 61
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    const v1, 0x7f0a059e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setText(I)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setShouldThrottleShowing(Z)V

    .line 64
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->a()V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    new-instance v1, Lcom/twitter/android/av/watchmode/view/a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/av/watchmode/view/a$1;-><init>(Lcom/twitter/android/av/watchmode/view/a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/watchmode/view/a;)Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/av/aj;->a(J)V

    .line 125
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 159
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 160
    iput-boolean v2, p0, Lcom/twitter/android/av/watchmode/view/a;->a:Z

    .line 161
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    iput-boolean v3, p0, Lcom/twitter/android/av/watchmode/view/a;->a:Z

    .line 163
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->a()V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iput-boolean v3, p0, Lcom/twitter/android/av/watchmode/view/a;->a:Z

    .line 166
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->b()Z

    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/a;->a:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    if-eqz v0, :cond_0

    .line 169
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->c()Z

    .line 170
    iput-boolean v2, p0, Lcom/twitter/android/av/watchmode/view/a;->a:Z

    .line 171
    iput-boolean v2, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;

    iget-boolean v0, v0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->t:Z

    if-eqz v0, :cond_1

    .line 135
    iput-boolean v2, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->i:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/av/watchmode/view/a$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/av/watchmode/view/a$2;-><init>(Lcom/twitter/android/av/watchmode/view/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    .line 145
    invoke-static {v0}, Lcom/twitter/android/av/watchmode/g;->a(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iput-boolean v2, p0, Lcom/twitter/android/av/watchmode/view/a;->a:Z

    goto :goto_0
.end method

.method c()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 82
    instance-of v1, v0, Landroid/support/v7/widget/LinearLayoutManager;

    if-nez v1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .line 86
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v1

    .line 87
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    .line 89
    instance-of v1, v0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;

    if-eqz v1, :cond_0

    .line 90
    check-cast v0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;

    iget-object v0, v0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c:Lcom/twitter/android/av/video/VideoContainerHost;

    .line 91
    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->getAVPlayerAttachment()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->e:Lbix;

    .line 94
    new-instance v0, Lpi;

    invoke-direct {v0, p0}, Lpi;-><init>(Lpi$a;)V

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->d:Lpi;

    .line 95
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->e:Lbix;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/a;->d:Lpi;

    invoke-virtual {v0, v1}, Lbix;->a(Lbiy;)Lbix;

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    .line 193
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iput-boolean v1, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    .line 195
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->b()Z

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->a()V

    .line 200
    iput-boolean v1, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    .line 202
    :cond_1
    return-void
.end method

.method e()Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 107
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/a;->h:Landroid/support/v7/widget/RecyclerView;

    .line 108
    invoke-static {v1}, Lcom/twitter/android/av/watchmode/g;->a(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/av/watchmode/view/a;->c:Z

    if-nez v1, :cond_0

    .line 107
    :goto_0
    return v0

    .line 108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->c()Z

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/a;->b:Z

    .line 214
    :cond_0
    return-void
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->e:Lbix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->d:Lpi;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->e:Lbix;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/a;->d:Lpi;

    invoke-virtual {v0, v1}, Lbix;->b(Lbiy;)Lbix;

    .line 224
    :cond_0
    return-void
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 113
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/a;->c:Z

    .line 115
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->g:Lcom/twitter/android/av/aj;

    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->a()V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->b()Z

    .line 120
    :cond_0
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/a;->f:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->c()Z

    .line 183
    :cond_0
    return-void
.end method
