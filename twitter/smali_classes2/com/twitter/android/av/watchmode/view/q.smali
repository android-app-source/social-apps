.class public Lcom/twitter/android/av/watchmode/view/q;
.super Lcom/twitter/android/client/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/client/p",
        "<",
        "Lcom/twitter/library/av/playback/AVDataSource;",
        "Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lcom/twitter/android/av;

.field private final c:Lcom/twitter/android/av/watchmode/b;

.field private d:Lcom/twitter/android/av/watchmode/view/k;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/av/watchmode/view/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/av/watchmode/b;Lcom/twitter/android/av;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/android/client/p;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/q;->a:Landroid/view/LayoutInflater;

    .line 35
    iput-object p2, p0, Lcom/twitter/android/av/watchmode/view/q;->c:Lcom/twitter/android/av/watchmode/b;

    .line 36
    iput-object p3, p0, Lcom/twitter/android/av/watchmode/view/q;->b:Lcom/twitter/android/av;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Lcbi;)Lcbi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ">;)",
            "Lcbi",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 76
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/q;->getItemCount()I

    move-result v0

    if-ne v0, v2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcbi;->be_()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 77
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/p;->a(Lcbi;)Lcbi;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 80
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/client/p;->b(Lcbi;)Lcbi;

    move-result-object v0

    .line 81
    invoke-virtual {p1}, Lcbi;->be_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/twitter/android/av/watchmode/view/q;->notifyItemRangeInserted(II)V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;I)Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/q;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f040448

    const/4 v2, 0x0

    .line 42
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;

    .line 43
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/q;->d:Lcom/twitter/android/av/watchmode/view/k;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setScrollClickListener(Lcom/twitter/android/av/watchmode/view/k;)V

    .line 44
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/q;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setFullscreenListeners(Ljava/util/List;)V

    .line 45
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;

    check-cast p2, Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/av/watchmode/view/q;->a(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;Lcom/twitter/library/av/playback/AVDataSource;I)V

    return-void
.end method

.method protected a(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;Lcom/twitter/library/av/playback/AVDataSource;I)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/q;->c:Lcom/twitter/android/av/watchmode/b;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/av/watchmode/b;->a(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/android/av/video/f;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setVideoContainerConfig(Lcom/twitter/android/av/video/f;)V

    .line 51
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/q;->b:Lcom/twitter/android/av;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v0, p1, p2, v1}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method protected a(Lcom/twitter/android/av/watchmode/view/k;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/q;->d:Lcom/twitter/android/av/watchmode/view/k;

    .line 59
    return-void
.end method

.method public varargs a([Lcom/twitter/android/av/watchmode/view/i;)V
    .locals 1

    .prologue
    .line 65
    invoke-static {p1}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/q;->e:Ljava/util/List;

    .line 66
    return-void
.end method

.method public synthetic b(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/av/watchmode/view/q;->a(Landroid/view/ViewGroup;I)Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;

    move-result-object v0

    return-object v0
.end method
