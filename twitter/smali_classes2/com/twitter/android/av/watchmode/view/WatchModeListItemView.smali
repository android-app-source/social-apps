.class public Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;
.implements Lcom/twitter/android/av/watchmode/view/p;
.implements Lcom/twitter/library/av/control/VideoControlView$a;
.implements Lcom/twitter/library/widget/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;
    }
.end annotation


# instance fields
.field A:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field B:F
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final C:Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;

.field private final D:Lcom/twitter/android/av/watchmode/view/h;

.field private E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/av/watchmode/view/i;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lcom/twitter/android/av/watchmode/view/e;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field b:Lcom/twitter/android/av/watchmode/view/e;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field c:Lcom/twitter/android/av/video/VideoContainerHost;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field e:Lcom/twitter/android/av/AutoPlayBadgeView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field f:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field g:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field i:Landroid/view/ViewGroup;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field k:Lcom/twitter/android/av/watchmode/view/WatchModeScalingFrameLayout;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field l:Landroid/widget/RelativeLayout$LayoutParams;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field m:Landroid/widget/RelativeLayout$LayoutParams;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field n:Landroid/widget/RelativeLayout$LayoutParams;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field o:F
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field p:F
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field q:Lcom/twitter/library/av/playback/AVDataSource;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field r:Landroid/content/res/ColorStateList;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field s:Landroid/content/res/ColorStateList;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field t:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field u:Lcom/twitter/android/av/video/f;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field v:Landroid/widget/FrameLayout;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field w:Lbpl;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field x:Lcom/twitter/android/av/ViewCountBadgeView;

.field y:Landroid/view/View;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 198
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 202
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 205
    new-instance v4, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;

    invoke-direct {v4}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;-><init>()V

    new-instance v5, Lcom/twitter/android/av/watchmode/view/h$b;

    invoke-direct {v5}, Lcom/twitter/android/av/watchmode/view/h$b;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;Lcom/twitter/android/av/watchmode/view/h$b;)V

    .line 207
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;Lcom/twitter/android/av/watchmode/view/h$b;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 213
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 153
    iput v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->o:F

    .line 155
    iput v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    .line 187
    const v0, 0x3f0ccccd    # 0.55f

    iput v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->B:F

    .line 214
    iput-object p4, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->C:Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;

    .line 215
    invoke-virtual {p5, p0}, Lcom/twitter/android/av/watchmode/view/h$b;->a(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;)Lcom/twitter/android/av/watchmode/view/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->D:Lcom/twitter/android/av/watchmode/view/h;

    .line 216
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->E:Ljava/util/List;

    return-object v0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 622
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->E:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 623
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->E:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/watchmode/view/i;

    .line 624
    if-eqz v0, :cond_0

    .line 625
    if-eqz p1, :cond_1

    .line 626
    invoke-interface {v0}, Lcom/twitter/android/av/watchmode/view/i;->d()V

    goto :goto_0

    .line 628
    :cond_1
    invoke-interface {v0}, Lcom/twitter/android/av/watchmode/view/i;->f()V

    goto :goto_0

    .line 633
    :cond_2
    return-void
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->u:Lcom/twitter/android/av/video/f;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->u:Lcom/twitter/android/av/video/f;

    iget-object v0, v0, Lcom/twitter/android/av/video/f;->f:Lcom/twitter/library/av/model/b;

    invoke-virtual {v0}, Lcom/twitter/library/av/model/b;->c()Z

    move-result v0

    .line 471
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    .line 521
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a(F)V

    .line 522
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 725
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->b(Z)V

    .line 726
    return-void
.end method


# virtual methods
.method a()V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->t:Z

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->d()V

    .line 341
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->D:Lcom/twitter/android/av/watchmode/view/h;

    invoke-virtual {v0}, Lcom/twitter/android/av/watchmode/view/h;->a()V

    .line 342
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a(Z)V

    .line 344
    :cond_0
    return-void
.end method

.method a(F)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 539
    iget v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->o:F

    div-float v0, p1, v0

    .line 540
    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    mul-float/2addr v0, v1

    .line 544
    div-float v0, p1, v0

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 545
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 546
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 645
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->t:Z

    if-eqz v0, :cond_1

    .line 646
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a:Lcom/twitter/android/av/watchmode/view/e;

    if-eqz v0, :cond_1

    .line 647
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a:Lcom/twitter/android/av/watchmode/view/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/watchmode/view/e;->a()V

    .line 650
    :cond_1
    return-void
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 659
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->t:Z

    if-eqz v0, :cond_1

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a:Lcom/twitter/android/av/watchmode/view/e;

    if-eqz v0, :cond_1

    .line 662
    if-eqz p2, :cond_2

    .line 663
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a:Lcom/twitter/android/av/watchmode/view/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/watchmode/view/e;->c()V

    .line 669
    :cond_1
    :goto_0
    return-void

    .line 665
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a:Lcom/twitter/android/av/watchmode/view/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/watchmode/view/e;->b()V

    goto :goto_0
.end method

.method a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/16 v3, 0x8

    .line 348
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 349
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 350
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    invoke-virtual {p2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 353
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 354
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 361
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method a(Z)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 554
    iput-boolean p1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->t:Z

    .line 555
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setIsFullscreen(Z)V

    .line 556
    return-void
.end method

.method public a(ZJ)V
    .locals 0

    .prologue
    .line 637
    return-void
.end method

.method b()V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const v2, 0x3f0ccccd    # 0.55f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 420
    iget v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 424
    iget v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v2

    div-float/2addr v0, v1

    .line 429
    :goto_0
    sub-float v0, v2, v0

    .line 430
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 431
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->r:Landroid/content/res/ColorStateList;

    invoke-virtual {v3, v1}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 432
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->s:Landroid/content/res/ColorStateList;

    invoke-virtual {v3, v1}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 434
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->e:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAlpha(F)V

    .line 435
    return-void

    .line 426
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Z)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 737
    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 738
    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->B:F

    .line 739
    if-eqz p1, :cond_0

    const/16 v0, 0x190

    .line 745
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->b:Lcom/twitter/android/av/watchmode/view/e;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/av/watchmode/view/e;->a(FI)V

    .line 746
    return-void

    .line 741
    :cond_1
    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->B:F

    iget v2, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    iget v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->B:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 742
    goto :goto_0
.end method

.method c()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->g()V

    .line 449
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->l:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setTweetSummaryViewContainerLayoutParams(Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 450
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setShouldShowFullscreenButton(Z)V

    .line 451
    return-void
.end method

.method d()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->i()V

    .line 459
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->m:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setTweetSummaryViewContainerLayoutParams(Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 460
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    invoke-direct {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setShouldShowFullscreenButton(Z)V

    .line 461
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 462
    return-void
.end method

.method e()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->g()V

    .line 481
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->n:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setTweetSummaryViewContainerLayoutParams(Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 482
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setShouldShowFullscreenButton(Z)V

    .line 483
    return-void
.end method

.method f()V
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0xc

    const/4 v4, 0x3

    const/4 v3, -0x1

    .line 502
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    invoke-virtual {v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 503
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->l:Landroid/widget/RelativeLayout$LayoutParams;

    .line 504
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->l:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 505
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->l:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 507
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->m:Landroid/widget/RelativeLayout$LayoutParams;

    .line 508
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->m:Landroid/widget/RelativeLayout$LayoutParams;

    const v2, 0x7f130870

    invoke-virtual {v1, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 509
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->m:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 511
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->n:Landroid/widget/RelativeLayout$LayoutParams;

    .line 512
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->n:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 513
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->n:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 514
    return-void
.end method

.method g()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 530
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 531
    return-void
.end method

.method public getAutoPlayableItem()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    return-object v0
.end method

.method public getChromeView()Lcom/twitter/library/av/control/e;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    return-object v0
.end method

.method public getExpandedHeight()I
    .locals 3

    .prologue
    .line 697
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    .line 698
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    invoke-virtual {v1}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->getPreferredHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->o:F

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 384
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 386
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a()V

    .line 389
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 718
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getChromeView()Lcom/twitter/library/av/control/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->f()Z

    .line 719
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 321
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 322
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->A:Z

    .line 323
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a()V

    .line 325
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c()V

    .line 330
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->requestLayout()V

    .line 331
    return-void

    .line 328
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->d()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 225
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 227
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 228
    const v0, 0x7f130870

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 229
    const v0, 0x7f130872

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/video/VideoContainerHost;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c:Lcom/twitter/android/av/video/VideoContainerHost;

    .line 230
    const v0, 0x7f130874

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->i:Landroid/view/ViewGroup;

    .line 231
    invoke-static {}, Lcom/twitter/android/av/ai;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->z:Z

    .line 233
    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->z:Z

    if-eqz v0, :cond_0

    .line 234
    const v0, 0x7f040040

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->i:Landroid/view/ViewGroup;

    invoke-static {v1, v0, v2}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 238
    :goto_0
    const v0, 0x7f13019c

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/AutoPlayBadgeView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->e:Lcom/twitter/android/av/AutoPlayBadgeView;

    .line 239
    const v0, 0x7f1301a2

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/ViewCountBadgeView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->x:Lcom/twitter/android/av/ViewCountBadgeView;

    .line 240
    const v0, 0x7f1301a1

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->y:Landroid/view/View;

    .line 242
    const v0, 0x7f130875

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->f:Landroid/widget/TextView;

    .line 243
    const v0, 0x7f130876

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->g:Landroid/widget/TextView;

    .line 244
    const v0, 0x7f13076e

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    .line 245
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 246
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->s:Landroid/content/res/ColorStateList;

    .line 247
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->r:Landroid/content/res/ColorStateList;

    .line 248
    const v0, 0x7f13051f

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    .line 249
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setOnChromeVisibilityChangeListener(Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;)V

    .line 250
    const v0, 0x7f130871

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/watchmode/view/WatchModeScalingFrameLayout;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->k:Lcom/twitter/android/av/watchmode/view/WatchModeScalingFrameLayout;

    .line 251
    new-instance v0, Lcom/twitter/android/av/watchmode/view/e;

    const v1, 0x7f130873

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/av/watchmode/view/e;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->b:Lcom/twitter/android/av/watchmode/view/e;

    .line 252
    const v0, 0x7f130877

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    .line 253
    new-instance v0, Lbpl;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    invoke-direct {v0, v1}, Lbpl;-><init>(Landroid/widget/FrameLayout;)V

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->w:Lbpl;

    .line 254
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->w:Lbpl;

    invoke-static {}, Lbpm;->a()Lbpm;

    move-result-object v1

    invoke-virtual {v1}, Lbpm;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbpl;->a(Ljava/util/List;)V

    .line 255
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->w:Lbpl;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->setHeartAnimationOverlay(Lbpl;)V

    .line 256
    new-instance v0, Lcom/twitter/android/av/watchmode/view/e;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    invoke-direct {v0, v1}, Lcom/twitter/android/av/watchmode/view/e;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a:Lcom/twitter/android/av/watchmode/view/e;

    .line 258
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->f()V

    .line 259
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->b()V

    .line 260
    invoke-direct {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j()V

    .line 261
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setControlsListener(Lcom/twitter/library/av/control/VideoControlView$a;)V

    .line 262
    invoke-virtual {p0, p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    return-void

    .line 236
    :cond_0
    const v0, 0x7f04003d

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->i:Landroid/view/ViewGroup;

    invoke-static {v1, v0, v2}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->C:Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;

    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c()V

    .line 367
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->C:Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;

    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$a;->a(Landroid/content/Context;)I

    move-result v0

    .line 368
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 374
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    iget-object v0, v0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->e:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0}, Lcom/twitter/android/widget/EngagementActionBar;->getFavoriteButton()Landroid/view/View;

    move-result-object v0

    .line 375
    if-eqz v0, :cond_1

    .line 376
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->w:Lbpl;

    invoke-virtual {v1, v0}, Lbpl;->a(Landroid/view/View;)V

    .line 379
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 380
    return-void

    .line 369
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->t:Z

    if-nez v0, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->d()V

    goto :goto_0
.end method

.method public setActive(Z)V
    .locals 2

    .prologue
    .line 706
    if-eqz p1, :cond_1

    const v0, 0x3f0ccccd    # 0.55f

    .line 707
    :goto_0
    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->B:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 708
    iput v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->B:F

    .line 709
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->b(Z)V

    .line 711
    :cond_0
    return-void

    .line 706
    :cond_1
    const v0, 0x3f333333    # 0.7f

    goto :goto_0
.end method

.method public setExpandedFraction(F)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 393
    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 394
    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_1

    .line 395
    iput v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    .line 397
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->b()V

    .line 398
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->h:Lcom/twitter/android/av/watchmode/view/TweetSummaryView;

    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->setExpandedFraction(F)V

    .line 399
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->k:Lcom/twitter/android/av/watchmode/view/WatchModeScalingFrameLayout;

    iget v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/WatchModeScalingFrameLayout;->setExpandedFraction(F)V

    .line 401
    iget v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    .line 402
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a()V

    .line 403
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->o()V

    .line 406
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j()V

    .line 407
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->requestLayout()V

    .line 410
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    iget v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->p:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setCollapsed(Z)V

    .line 411
    return-void

    .line 410
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setFullscreenListeners(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/av/watchmode/view/i;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 689
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->E:Ljava/util/List;

    .line 690
    return-void
.end method

.method protected setScrollClickListener(Lcom/twitter/android/av/watchmode/view/k;)V
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setScrollClickListener(Lcom/twitter/android/av/watchmode/view/k;)V

    .line 683
    return-void
.end method

.method setTweetSummaryViewContainerLayoutParams(Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 492
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 493
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 495
    :cond_0
    return-void
.end method

.method public setVideoContainerConfig(Lcom/twitter/android/av/video/f;)V
    .locals 8

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 266
    if-eqz p1, :cond_4

    .line 267
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->u:Lcom/twitter/android/av/video/f;

    .line 268
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c:Lcom/twitter/android/av/video/VideoContainerHost;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->u:Lcom/twitter/android/av/video/f;

    invoke-virtual {v0, v3}, Lcom/twitter/android/av/video/VideoContainerHost;->a(Lcom/twitter/android/av/video/f;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 269
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c:Lcom/twitter/android/av/video/VideoContainerHost;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->u:Lcom/twitter/android/av/video/f;

    invoke-virtual {v0, v3}, Lcom/twitter/android/av/video/VideoContainerHost;->setVideoContainerConfig(Lcom/twitter/android/av/video/f;)V

    .line 270
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->requestLayout()V

    .line 279
    :goto_0
    if-eqz p1, :cond_5

    iget-object v0, p1, Lcom/twitter/android/av/video/f;->a:Lcom/twitter/library/av/playback/AVDataSource;

    :goto_1
    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    .line 281
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    if-eqz v0, :cond_2

    .line 282
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-static {v0}, Lcom/twitter/library/av/playback/d;->a(Lcom/twitter/library/av/playback/AVDataSource;)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->o:F

    .line 283
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 284
    if-eqz v0, :cond_6

    move v3, v2

    .line 285
    :goto_2
    if-eqz v0, :cond_0

    .line 286
    iget-object v5, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v5, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->g:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "@"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v5, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->e(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v5, v0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setShouldShowControls(Z)V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->e:Lcom/twitter/android/av/AutoPlayBadgeView;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {v0, v3}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAVDataSource(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 295
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->x:Lcom/twitter/android/av/ViewCountBadgeView;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-static {v0}, Lcom/twitter/android/av/ai;->a(Lcom/twitter/library/av/playback/AVDataSource;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 297
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->x:Lcom/twitter/android/av/ViewCountBadgeView;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {v0, v3}, Lcom/twitter/android/av/ViewCountBadgeView;->setAVDataSource(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 298
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->y:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->y:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 301
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020058

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 300
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 313
    :cond_1
    :goto_4
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v3}, Lcom/twitter/android/av/video/VideoContainerHost;->getAVPlayerAttachment()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 314
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->j:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->q:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v3}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_9

    :goto_5
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->setShouldPlayPauseOnTap(Z)V

    .line 316
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->setExpandedFraction(F)V

    .line 317
    :cond_3
    return-void

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->a()V

    goto/16 :goto_0

    .line 279
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    move v3, v4

    .line 284
    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 288
    goto :goto_3

    .line 304
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->x:Lcom/twitter/android/av/ViewCountBadgeView;

    invoke-virtual {v0, v4}, Lcom/twitter/android/av/ViewCountBadgeView;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->y:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->y:Landroid/view/View;

    .line 308
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020075

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 307
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    :cond_9
    move v1, v2

    .line 314
    goto :goto_5
.end method

.method public w()V
    .locals 0

    .prologue
    .line 560
    return-void
.end method

.method public x()V
    .locals 3

    .prologue
    .line 572
    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->A:Z

    if-eqz v0, :cond_0

    .line 615
    :goto_0
    return-void

    .line 577
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->t:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 578
    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->c(Z)V

    .line 580
    if-eqz v0, :cond_2

    .line 581
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->D:Lcom/twitter/android/av/watchmode/view/h;

    new-instance v2, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$1;-><init>(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;Z)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/av/watchmode/view/h;->b(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 577
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 594
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;->D:Lcom/twitter/android/av/watchmode/view/h;

    new-instance v2, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/av/watchmode/view/WatchModeListItemView$2;-><init>(Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;Z)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/av/watchmode/view/h;->a(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public y()V
    .locals 0

    .prologue
    .line 564
    return-void
.end method

.method public z()V
    .locals 0

    .prologue
    .line 568
    return-void
.end method
