.class public Lcom/twitter/android/av/watchmode/view/TweetSummaryView;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;
    }
.end annotation


# instance fields
.field final a:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final b:Lcom/twitter/ui/widget/TypefacesTextView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final c:Lcom/twitter/ui/widget/TypefacesTextView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final d:Lcom/twitter/library/widget/TextContentView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field e:Lcom/twitter/android/widget/EngagementActionBar;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field f:Landroid/graphics/drawable/Drawable;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field g:I
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final h:Lcom/twitter/android/av/w$b;

.field private i:Lcom/twitter/android/av/w;

.field private final j:Landroid/content/res/Resources;

.field private final k:Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;

.field private l:Lcom/twitter/model/core/Tweet;

.field private m:F

.field private n:Lbpl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 88
    new-instance v4, Lcom/twitter/android/av/w$b;

    invoke-direct {v4}, Lcom/twitter/android/av/w$b;-><init>()V

    new-instance v5, Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;

    invoke-direct {v5}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/android/av/w$b;Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;)V

    .line 89
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/android/av/w$b;Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;)V
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->f:Landroid/graphics/drawable/Drawable;

    .line 75
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->m:F

    .line 96
    invoke-virtual {p0, v4}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->setOrientation(I)V

    .line 97
    const v0, 0x7f04044a

    invoke-static {p1, v0, p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 99
    const v0, 0x7f130879

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->a:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f13087a

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->b:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 101
    const v0, 0x7f13087b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 102
    const v0, 0x7f13087c

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TextContentView;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->d:Lcom/twitter/library/widget/TextContentView;

    .line 103
    const v0, 0x7f130145

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EngagementActionBar;

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->e:Lcom/twitter/android/widget/EngagementActionBar;

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->j:Landroid/content/res/Resources;

    .line 106
    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->b:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v2, v0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TypefacesTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 108
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v0, v0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 109
    iput-object p4, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->h:Lcom/twitter/android/av/w$b;

    .line 110
    iput-object p5, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->k:Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;

    .line 111
    invoke-virtual {p0, p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v4, [I

    const v2, 0x101030e

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 116
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->f:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 123
    return-void

    .line 121
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method static a(FLcom/twitter/android/av/watchmode/view/TweetSummaryView;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 183
    const/high16 v0, 0x3f400000    # 0.75f

    sub-float v0, p0, v0

    const/high16 v1, 0x3e800000    # 0.25f

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->setAlpha(F)V

    .line 184
    return-void
.end method


# virtual methods
.method a(Landroid/view/View;Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->e:Lcom/twitter/android/widget/EngagementActionBar;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->l:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    instance-of v0, p2, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 212
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "tw"

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->l:Lcom/twitter/model/core/Tweet;

    .line 213
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 212
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 215
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/model/core/Tweet;Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 136
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->g:I

    .line 137
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->l:Lcom/twitter/model/core/Tweet;

    .line 138
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->e:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/EngagementActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->a:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->b:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->j:Landroid/content/res/Resources;

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->q:J

    invoke-static {v1, v2, v3}, Lcom/twitter/util/aa;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v1

    .line 143
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->k:Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;

    invoke-virtual {v1, p1}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 146
    invoke-virtual {v1, v4}, Lcom/twitter/model/util/a;->f(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 147
    invoke-virtual {v1, v4}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    .line 149
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->d:Lcom/twitter/library/widget/TextContentView;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->o()Z

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/twitter/library/widget/TextContentView;->b(Ljava/lang/CharSequence;Z)V

    .line 150
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    instance-of v0, p2, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 153
    check-cast p2, Landroid/support/v4/app/FragmentActivity;

    .line 155
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->h:Lcom/twitter/android/av/w$b;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->l:Lcom/twitter/model/core/Tweet;

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->e:Lcom/twitter/android/widget/EngagementActionBar;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->n:Lbpl;

    new-instance v4, Lcom/twitter/android/av/v;

    .line 156
    invoke-virtual {p3}, Lcom/twitter/library/av/playback/AVPlayer;->R()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    const-string/jumbo v6, "tweet"

    invoke-direct {v4, p2, v5, v6}, Lcom/twitter/android/av/v;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/av/w$b;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/widget/EngagementActionBar;Lbpl;Lcom/twitter/android/av/v;)Lcom/twitter/android/av/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->i:Lcom/twitter/android/av/w;

    .line 158
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->i:Lcom/twitter/android/av/w;

    invoke-virtual {v0, p2}, Lcom/twitter/android/av/w;->a(Landroid/content/Context;)V

    .line 159
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->i:Lcom/twitter/android/av/w;

    invoke-virtual {v0, p2}, Lcom/twitter/android/av/w;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 160
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->i:Lcom/twitter/android/av/w;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/w;->a(Lcom/twitter/model/core/Tweet;)V

    .line 162
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->a(Lcom/twitter/model/core/Tweet;Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 132
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 268
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->j:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->l:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->q:J

    .line 269
    invoke-static {v0, v2, v3}, Lcom/twitter/util/aa;->b(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 268
    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 270
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 272
    const-string/jumbo v0, ""

    .line 273
    if-eqz p1, :cond_0

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 277
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->l:Lcom/twitter/model/core/Tweet;

    iget-object v3, v3, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->j:Landroid/content/res/Resources;

    const v3, 0x7f0a095c

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object p2, v4, v0

    const/4 v0, 0x2

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string/jumbo v1, ""

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string/jumbo v1, ""

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 280
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 281
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 220
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 221
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->invalidate()V

    .line 222
    return-void
.end method

.method protected getPreferredHeight()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->g:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 207
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 232
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 233
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 234
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 188
    iget v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->g:I

    if-nez v0, :cond_0

    .line 190
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 191
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->g:I

    .line 194
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->m:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->g:I

    if-nez v0, :cond_2

    .line 195
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 202
    :goto_0
    return-void

    .line 199
    :cond_2
    iget v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->m:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 200
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 226
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 227
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 228
    return-void
.end method

.method public setExpandedFraction(F)V
    .locals 0

    .prologue
    .line 169
    iput p1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->m:F

    .line 170
    invoke-static {p1, p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->a(FLcom/twitter/android/av/watchmode/view/TweetSummaryView;)V

    .line 171
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->requestLayout()V

    .line 172
    return-void
.end method

.method public setHeartAnimationOverlay(Lbpl;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/TweetSummaryView;->n:Lbpl;

    .line 252
    return-void
.end method
