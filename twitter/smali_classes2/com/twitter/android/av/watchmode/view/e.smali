.class public Lcom/twitter/android/av/watchmode/view/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/e;->a:Landroid/view/View;

    .line 20
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/e;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 24
    return-void
.end method

.method public a(FI)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/e;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 42
    if-lez p2, :cond_0

    .line 43
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/e;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/util/e;->b(Landroid/view/View;FILandroid/view/animation/Interpolator;)Landroid/view/ViewPropertyAnimator;

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/e;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/e;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 28
    return-void
.end method

.method public c()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/e;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 33
    return-void
.end method
