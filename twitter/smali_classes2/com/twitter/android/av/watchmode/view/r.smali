.class public Lcom/twitter/android/av/watchmode/view/r;
.super Lcom/twitter/android/client/o;
.source "Twttr"

# interfaces
.implements Lala;
.implements Lcom/twitter/android/av/watchmode/WatchModeLayoutManager$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/client/o",
        "<",
        "Lcom/twitter/library/av/playback/AVDataSource;",
        "Lcom/twitter/android/av/watchmode/view/WatchModeListItemView;",
        ">;",
        "Lala;",
        "Lcom/twitter/android/av/watchmode/WatchModeLayoutManager$a;"
    }
.end annotation


# instance fields
.field a:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final b:Lrx/j;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final d:Lcom/twitter/android/av/watchmode/b;

.field private final e:Lcom/twitter/android/av/j;

.field private final f:Lcom/twitter/android/av/watchmode/view/t;

.field private final g:Landroid/os/Handler;

.field private final h:Lqp;

.field private final i:Lcom/twitter/android/av/aj;

.field private final j:Lcom/twitter/android/av/watchmode/e;

.field private final k:Lcom/twitter/android/av/watchmode/view/g;

.field private final l:Lcom/twitter/library/av/b;

.field private m:Lcom/twitter/android/av/watchmode/view/a;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;Lcom/twitter/android/av/watchmode/b;Lcom/twitter/android/av/watchmode/e;Landroid/os/Handler;Lcom/twitter/android/av/watchmode/view/m;Laon;)V
    .locals 20

    .prologue
    .line 75
    new-instance v8, Lcom/twitter/android/av/j;

    .line 79
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    .line 80
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v5

    invoke-virtual {v5}, Lalc;->n()Lcom/twitter/app/common/util/m;

    move-result-object v5

    invoke-direct {v8, v3, v4, v5}, Lcom/twitter/android/av/j;-><init>(Landroid/content/Context;ZLcom/twitter/app/common/util/m;)V

    new-instance v9, Lcom/twitter/android/av/watchmode/view/q;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v9, v0, v1, v2}, Lcom/twitter/android/av/watchmode/view/q;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/av/watchmode/b;Lcom/twitter/android/av;)V

    new-instance v10, Lcom/twitter/android/av/watchmode/view/t;

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v10, v3}, Lcom/twitter/android/av/watchmode/view/t;-><init>(Landroid/content/Context;)V

    new-instance v11, Lcom/twitter/android/av/watchmode/view/o;

    .line 83
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v11, v3}, Lcom/twitter/android/av/watchmode/view/o;-><init>(Landroid/content/Context;)V

    new-instance v13, Lcom/twitter/android/av/aj;

    .line 85
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v13, v3}, Lcom/twitter/android/av/aj;-><init>(Landroid/content/Context;)V

    new-instance v15, Lcom/twitter/android/av/watchmode/view/g$a;

    invoke-direct {v15}, Lcom/twitter/android/av/watchmode/view/g$a;-><init>()V

    new-instance v17, Lcom/twitter/library/av/b;

    .line 89
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lcom/twitter/library/av/b;-><init>(Landroid/content/Context;)V

    new-instance v18, Lcom/twitter/android/av/watchmode/view/a$a;

    invoke-direct/range {v18 .. v18}, Lcom/twitter/android/av/watchmode/view/a$a;-><init>()V

    .line 91
    invoke-static {}, Lcom/twitter/android/av/watchmode/view/j;->a()Z

    move-result v19

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v12, p5

    move-object/from16 v14, p6

    move-object/from16 v16, p7

    .line 75
    invoke-direct/range {v3 .. v19}, Lcom/twitter/android/av/watchmode/view/r;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;Lcom/twitter/android/av/watchmode/b;Lcom/twitter/android/av/watchmode/e;Lcom/twitter/android/av/j;Lcom/twitter/android/av/watchmode/view/q;Lcom/twitter/android/av/watchmode/view/t;Lcom/twitter/android/av/watchmode/view/o;Landroid/os/Handler;Lcom/twitter/android/av/aj;Lcom/twitter/android/av/watchmode/view/m;Lcom/twitter/android/av/watchmode/view/g$a;Laon;Lcom/twitter/library/av/b;Lcom/twitter/android/av/watchmode/view/a$a;Z)V

    .line 92
    return-void
.end method

.method constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;Lcom/twitter/android/av/watchmode/b;Lcom/twitter/android/av/watchmode/e;Lcom/twitter/android/av/j;Lcom/twitter/android/av/watchmode/view/q;Lcom/twitter/android/av/watchmode/view/t;Lcom/twitter/android/av/watchmode/view/o;Landroid/os/Handler;Lcom/twitter/android/av/aj;Lcom/twitter/android/av/watchmode/view/m;Lcom/twitter/android/av/watchmode/view/g$a;Laon;Lcom/twitter/library/av/b;Lcom/twitter/android/av/watchmode/view/a$a;Z)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/o;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;)V

    .line 48
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/android/av/watchmode/view/r;->a:Z

    .line 107
    invoke-virtual {p0, p6}, Lcom/twitter/android/av/watchmode/view/r;->a(Lcom/twitter/android/client/p;)V

    .line 108
    new-instance v1, Lcom/twitter/android/av/watchmode/view/k;

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {v1, v2}, Lcom/twitter/android/av/watchmode/view/k;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {p6, v1}, Lcom/twitter/android/av/watchmode/view/q;->a(Lcom/twitter/android/av/watchmode/view/k;)V

    .line 109
    iput-object p5, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    .line 110
    const/4 v1, 0x0

    .line 111
    invoke-static {v1}, Lcom/twitter/android/av/r;->a(I)Lcom/twitter/android/av/r;

    move-result-object v1

    .line 112
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/av/r;->a(Z)V

    .line 113
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    invoke-virtual {v2, v1}, Lcom/twitter/android/av/j;->a(Lcom/twitter/android/av/r;)V

    .line 114
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    invoke-virtual {v1, p0}, Lcom/twitter/android/av/j;->a(Lcom/twitter/android/client/o;)V

    .line 115
    iput-object p7, p0, Lcom/twitter/android/av/watchmode/view/r;->f:Lcom/twitter/android/av/watchmode/view/t;

    .line 116
    iput-object p3, p0, Lcom/twitter/android/av/watchmode/view/r;->d:Lcom/twitter/android/av/watchmode/b;

    .line 117
    iput-object p9, p0, Lcom/twitter/android/av/watchmode/view/r;->g:Landroid/os/Handler;

    .line 118
    iput-object p10, p0, Lcom/twitter/android/av/watchmode/view/r;->i:Lcom/twitter/android/av/aj;

    .line 119
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->l:Lcom/twitter/library/av/b;

    .line 121
    new-instance v1, Lqp;

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {v1, v2, p1}, Lqp;-><init>(Landroid/support/v7/widget/RecyclerView;Landroid/view/LayoutInflater;)V

    iput-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->h:Lqp;

    .line 122
    iput-object p4, p0, Lcom/twitter/android/av/watchmode/view/r;->j:Lcom/twitter/android/av/watchmode/e;

    .line 123
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->h:Lqp;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 125
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/r;->aN_()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f130011

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/NewItemBannerView;

    .line 126
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/r;->g:Landroid/os/Handler;

    move-object/from16 v0, p15

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/av/watchmode/view/a$a;->a(Lcom/twitter/android/widget/NewItemBannerView;Landroid/support/v7/widget/RecyclerView;Landroid/os/Handler;)Lcom/twitter/android/av/watchmode/view/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->m:Lcom/twitter/android/av/watchmode/view/a;

    .line 128
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->d:Lcom/twitter/android/av/watchmode/b;

    invoke-interface {v1}, Lcom/twitter/android/av/watchmode/b;->a()Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/av/watchmode/view/r$1;

    move/from16 v0, p16

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/av/watchmode/view/r$1;-><init>(Lcom/twitter/android/av/watchmode/view/r;Z)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->b:Lrx/j;

    .line 159
    if-eqz p16, :cond_1

    .line 160
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/twitter/android/av/watchmode/view/i;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/av/watchmode/view/r;->m:Lcom/twitter/android/av/watchmode/view/a;

    aput-object v3, v1, v2

    invoke-virtual {p6, v1}, Lcom/twitter/android/av/watchmode/view/q;->a([Lcom/twitter/android/av/watchmode/view/i;)V

    .line 165
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/r;->aN_()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f130857

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 166
    new-instance v2, Lcom/twitter/android/av/watchmode/view/r$2;

    move-object/from16 v0, p13

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/av/watchmode/view/r$2;-><init>(Lcom/twitter/android/av/watchmode/view/r;Laon;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->i:Lcom/twitter/android/av/aj;

    move-object/from16 v0, p12

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/watchmode/view/g$a;->a(Landroid/support/v7/widget/RecyclerView;Lcom/twitter/android/av/aj;)Lcom/twitter/android/av/watchmode/view/g;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->k:Lcom/twitter/android/av/watchmode/view/g;

    .line 173
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->k:Lcom/twitter/android/av/watchmode/view/g;

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/watchmode/view/r;->a(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 174
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->j:Lcom/twitter/android/av/watchmode/e;

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/watchmode/view/r;->a(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 176
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->f:Lcom/twitter/android/av/watchmode/view/t;

    invoke-virtual {p0, v1}, Lcom/twitter/android/av/watchmode/view/r;->a(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 177
    invoke-virtual {p0, p8}, Lcom/twitter/android/av/watchmode/view/r;->a(Landroid/support/v7/widget/RecyclerView$OnItemTouchListener;)V

    .line 179
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v1

    instance-of v1, v1, Lcom/twitter/android/av/watchmode/WatchModeLayoutManager;

    if-eqz v1, :cond_0

    .line 180
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/av/watchmode/WatchModeLayoutManager;

    invoke-virtual {v1, p0}, Lcom/twitter/android/av/watchmode/WatchModeLayoutManager;->a(Lcom/twitter/android/av/watchmode/WatchModeLayoutManager$a;)V

    .line 183
    :cond_0
    invoke-virtual {p0, p11}, Lcom/twitter/android/av/watchmode/view/r;->a(Landroid/support/v7/widget/RecyclerView$OnItemTouchListener;)V

    .line 185
    invoke-virtual {p0}, Lcom/twitter/android/av/watchmode/view/r;->o()Lanh;

    move-result-object v1

    .line 186
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->j:Lcom/twitter/android/av/watchmode/e;

    invoke-virtual {v1, v2}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 187
    invoke-virtual {v1, p8}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 188
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->f:Lcom/twitter/android/av/watchmode/view/t;

    invoke-virtual {v1, v2}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 189
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 190
    invoke-virtual {v1, p11}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 191
    iget-object v2, p0, Lcom/twitter/android/av/watchmode/view/r;->m:Lcom/twitter/android/av/watchmode/view/a;

    invoke-virtual {v1, v2}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 192
    return-void

    .line 162
    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/twitter/android/av/watchmode/view/i;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    invoke-virtual {p6, v1}, Lcom/twitter/android/av/watchmode/view/q;->a([Lcom/twitter/android/av/watchmode/view/i;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/library/av/b;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->l:Lcom/twitter/library/av/b;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/av/watchmode/view/r;)Lqp;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->h:Lqp;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/j;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/view/t;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->f:Lcom/twitter/android/av/watchmode/view/t;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/e;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->j:Lcom/twitter/android/av/watchmode/e;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/view/a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->m:Lcom/twitter/android/av/watchmode/view/a;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 258
    invoke-super {p0, p1}, Lcom/twitter/android/client/o;->a(Landroid/content/res/Configuration;)V

    .line 264
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 265
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->i:Lcom/twitter/android/av/aj;

    invoke-virtual {v0}, Lcom/twitter/android/av/aj;->a()V

    .line 270
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/av/watchmode/view/r$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/av/watchmode/view/r$5;-><init>(Lcom/twitter/android/av/watchmode/view/r;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 277
    return-void

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->i:Lcom/twitter/android/av/aj;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/av/aj;->a(J)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 281
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->e:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->m:Lcom/twitter/android/av/watchmode/view/a;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->m:Lcom/twitter/android/av/watchmode/view/a;

    invoke-virtual {v0}, Lcom/twitter/android/av/watchmode/view/a;->h()V

    .line 284
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 196
    invoke-super {p0}, Lcom/twitter/android/client/o;->aO_()V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->g:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/av/watchmode/view/r$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/av/watchmode/view/r$3;-><init>(Lcom/twitter/android/av/watchmode/view/r;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 208
    return-void
.end method

.method public ak_()V
    .locals 1

    .prologue
    .line 212
    invoke-super {p0}, Lcom/twitter/android/client/o;->ak_()V

    .line 213
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->c()V

    .line 214
    return-void
.end method

.method public am_()V
    .locals 2

    .prologue
    .line 224
    invoke-super {p0}, Lcom/twitter/android/client/o;->am_()V

    .line 225
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->b:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 226
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->l:Lcom/twitter/library/av/b;

    invoke-virtual {v0}, Lcom/twitter/library/av/b;->a()V

    .line 227
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->f()V

    .line 229
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/av/watchmode/WatchModeLayoutManager;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/watchmode/WatchModeLayoutManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/WatchModeLayoutManager;->a(Lcom/twitter/android/av/watchmode/WatchModeLayoutManager$a;)V

    .line 232
    :cond_0
    return-void
.end method

.method public an_()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lcom/twitter/android/av/watchmode/g;->a(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/r;->a:Z

    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->g:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/av/watchmode/view/r$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/av/watchmode/view/r$4;-><init>(Lcom/twitter/android/av/watchmode/view/r;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/view/r;->a:Z

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->b()V

    .line 248
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0}, Lcom/twitter/android/client/o;->d()V

    .line 219
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r;->e:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->d()V

    .line 220
    return-void
.end method
