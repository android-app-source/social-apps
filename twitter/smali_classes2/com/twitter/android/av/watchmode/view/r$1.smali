.class Lcom/twitter/android/av/watchmode/view/r$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/av/watchmode/view/r;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;Lcom/twitter/android/av/watchmode/b;Lcom/twitter/android/av/watchmode/e;Lcom/twitter/android/av/j;Lcom/twitter/android/av/watchmode/view/q;Lcom/twitter/android/av/watchmode/view/t;Lcom/twitter/android/av/watchmode/view/o;Landroid/os/Handler;Lcom/twitter/android/av/aj;Lcom/twitter/android/av/watchmode/view/m;Lcom/twitter/android/av/watchmode/view/g$a;Laon;Lcom/twitter/library/av/b;Lcom/twitter/android/av/watchmode/view/a$a;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lqm;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/twitter/android/av/watchmode/view/r;


# direct methods
.method constructor <init>(Lcom/twitter/android/av/watchmode/view/r;Z)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    iput-boolean p2, p0, Lcom/twitter/android/av/watchmode/view/r$1;->a:Z

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 128
    check-cast p1, Lqm;

    invoke-virtual {p0, p1}, Lcom/twitter/android/av/watchmode/view/r$1;->a(Lqm;)V

    return-void
.end method

.method public a(Lqm;)V
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-virtual {p1}, Lqm;->a()Lcbi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/r;->a(Lcbi;)V

    .line 133
    invoke-static {}, Lcom/twitter/library/av/v;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v0}, Lcom/twitter/android/av/watchmode/view/r;->a(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/library/av/b;

    move-result-object v0

    invoke-virtual {p1}, Lqm;->c()Lcom/twitter/library/av/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/av/b;->a(Lcom/twitter/library/av/p;I)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v0}, Lcom/twitter/android/av/watchmode/view/r;->b(Lcom/twitter/android/av/watchmode/view/r;)Lqp;

    move-result-object v0

    invoke-virtual {p1}, Lqm;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqp;->a(Ljava/util/Map;)V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v0}, Lcom/twitter/android/av/watchmode/view/r;->c(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->b()V

    .line 146
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v0}, Lcom/twitter/android/av/watchmode/view/r;->d(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/view/t;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    iget-object v1, v1, Lcom/twitter/android/av/watchmode/view/r;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/t;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 147
    invoke-virtual {p1}, Lqm;->a()Lcbi;

    move-result-object v0

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    .line 149
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v1}, Lcom/twitter/android/av/watchmode/view/r;->e(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/watchmode/e;->a(I)V

    .line 150
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v1}, Lcom/twitter/android/av/watchmode/view/r;->e(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/av/watchmode/e;->a()V

    .line 152
    iget-boolean v1, p0, Lcom/twitter/android/av/watchmode/view/r$1;->a:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 153
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v0}, Lcom/twitter/android/av/watchmode/view/r;->f(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/view/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/av/watchmode/view/a;->c()V

    .line 154
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/view/r$1;->b:Lcom/twitter/android/av/watchmode/view/r;

    invoke-static {v1}, Lcom/twitter/android/av/watchmode/view/r;->f(Lcom/twitter/android/av/watchmode/view/r;)Lcom/twitter/android/av/watchmode/view/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/watchmode/view/r;->a(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 156
    :cond_1
    return-void
.end method
