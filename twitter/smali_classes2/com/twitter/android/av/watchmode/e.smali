.class public Lcom/twitter/android/av/watchmode/e;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "Twttr"

# interfaces
.implements Lala;
.implements Lane;
.implements Lanf;
.implements Lcom/twitter/android/av;
.implements Lcom/twitter/android/av/watchmode/view/i;
.implements Lcom/twitter/ui/anim/c$a;


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final b:Lcom/twitter/library/av/playback/AVDataSource;

.field private final c:Landroid/content/Context;

.field private d:I

.field private final e:Lcom/twitter/android/cp;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 3

    .prologue
    .line 79
    new-instance v0, Lcom/twitter/android/cq;

    invoke-direct {v0}, Lcom/twitter/android/cq;-><init>()V

    const/4 v1, -0x1

    .line 81
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    .line 80
    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/twitter/android/cq;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/client/v;)Lcom/twitter/android/cp;

    move-result-object v0

    .line 79
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/av/watchmode/e;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/android/cp;)V

    .line 82
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/android/cp;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 65
    iput v0, p0, Lcom/twitter/android/av/watchmode/e;->d:I

    .line 68
    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/e;->f:Z

    .line 69
    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/e;->g:Z

    .line 87
    iput-object p2, p0, Lcom/twitter/android/av/watchmode/e;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 88
    iput-object p3, p0, Lcom/twitter/android/av/watchmode/e;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/watchmode/e;->c:Landroid/content/Context;

    .line 90
    iput-object p4, p0, Lcom/twitter/android/av/watchmode/e;->e:Lcom/twitter/android/cp;

    .line 93
    const-string/jumbo v0, "impression"

    invoke-direct {p0, v0}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 94
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 98
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/e;->b:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v1}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v6

    .line 100
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(J)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/av/watchmode/e;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 101
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 104
    if-eqz v6, :cond_0

    .line 105
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/e;->c:Landroid/content/Context;

    invoke-static {v0, v1, v6, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 108
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 139
    iget v0, p0, Lcom/twitter/android/av/watchmode/e;->d:I

    if-le v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/e;->f:Z

    if-nez v0, :cond_0

    .line 140
    iput-boolean v1, p0, Lcom/twitter/android/av/watchmode/e;->f:Z

    .line 141
    const-string/jumbo v0, "suggestions"

    const-string/jumbo v1, "show"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 143
    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 150
    iput p1, p0, Lcom/twitter/android/av/watchmode/e;->d:I

    .line 151
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 118
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "orientation_change_portrait"

    .line 120
    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 121
    return-void

    .line 118
    :cond_0
    const-string/jumbo v0, "orientation_change_landscape"

    goto :goto_0
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 166
    if-eqz p2, :cond_0

    .line 167
    check-cast p2, Lcom/twitter/library/av/playback/AVDataSource;

    .line 168
    invoke-interface {p2}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    .line 170
    iget-object v1, p0, Lcom/twitter/android/av/watchmode/e;->e:Lcom/twitter/android/cp;

    invoke-virtual {v1, v0, p3}, Lcom/twitter/android/cp;->b(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    .line 173
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 2

    .prologue
    .line 155
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 157
    iget v0, p0, Lcom/twitter/android/av/watchmode/e;->d:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/av/watchmode/e;->g:Z

    if-nez v0, :cond_0

    .line 158
    const-string/jumbo v0, "abandoned"

    invoke-direct {p0, v0}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 161
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public ak_()V
    .locals 4

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/android/av/watchmode/e;->e:Lcom/twitter/android/cp;

    .line 196
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v1, "watch_mode::stream::results"

    .line 195
    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/android/cp;->a(JLjava/lang/String;)V

    .line 197
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 201
    const-string/jumbo v0, "expand_button"

    const-string/jumbo v1, "click"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 202
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 177
    const-string/jumbo v0, "swipe_to_dismiss"

    invoke-direct {p0, v0}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 178
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 206
    const-string/jumbo v0, "collapse_button"

    const-string/jumbo v1, "click"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/av/watchmode/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 207
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 126
    instance-of v1, v0, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v1, :cond_0

    .line 127
    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .line 128
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/watchmode/e;->g:Z

    .line 132
    :cond_0
    return-void
.end method
