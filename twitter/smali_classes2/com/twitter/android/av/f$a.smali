.class public Lcom/twitter/android/av/f$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation


# instance fields
.field private final a:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/av/f$a;-><init>(Z)V

    .line 190
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-boolean p1, p0, Lcom/twitter/android/av/f$a;->a:Z

    .line 194
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/PromotedEvent;
    .locals 1

    .prologue
    .line 268
    const-string/jumbo v0, "video"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    sget-object v0, Lcom/twitter/android/av/f;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedEvent;

    .line 274
    :goto_0
    return-object v0

    .line 270
    :cond_0
    const-string/jumbo v0, "ad"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    sget-object v0, Lcom/twitter/android/av/f;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedEvent;

    goto :goto_0

    .line 274
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/av/playback/u;Lcgi;Lcom/twitter/library/av/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 205
    new-instance v1, Lcom/twitter/android/av/ae;

    invoke-virtual {p1}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/android/av/ae;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 206
    invoke-virtual {v1, p3}, Lcom/twitter/android/av/ae;->a(Lcom/twitter/library/av/m;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 207
    new-instance v2, Lcom/twitter/library/av/e;

    invoke-direct {v2}, Lcom/twitter/library/av/e;-><init>()V

    .line 208
    iget-object v3, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->F:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/av/e;->a:Ljava/lang/String;

    .line 209
    iget-object v3, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->H:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/av/e;->c:Ljava/lang/String;

    .line 210
    iget v3, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    iput v3, v2, Lcom/twitter/library/av/e;->l:I

    .line 211
    iget-object v3, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aj:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/av/e;->g:Ljava/lang/String;

    .line 212
    iget-wide v4, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    iput-wide v4, v2, Lcom/twitter/library/av/e;->k:J

    .line 213
    iget-object v3, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->L:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/av/e;->h:Ljava/lang/String;

    .line 214
    iget-boolean v3, p3, Lcom/twitter/library/av/m;->i:Z

    iput-boolean v3, v2, Lcom/twitter/library/av/e;->b:Z

    .line 215
    iget-object v3, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/av/e;->i:Ljava/lang/String;

    .line 216
    iget v3, p3, Lcom/twitter/library/av/m;->j:I

    iput v3, v2, Lcom/twitter/library/av/e;->j:I

    .line 218
    iget-object v3, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->H:Ljava/lang/String;

    invoke-virtual {p0, p4, v3}, Lcom/twitter/android/av/f$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/PromotedEvent;

    move-result-object v3

    .line 219
    if-eqz v3, :cond_2

    .line 221
    sget-object v4, Lcom/twitter/library/api/PromotedEvent;->P:Lcom/twitter/library/api/PromotedEvent;

    if-eq v4, v3, :cond_0

    sget-object v4, Lcom/twitter/library/api/PromotedEvent;->Q:Lcom/twitter/library/api/PromotedEvent;

    if-ne v4, v3, :cond_3

    :cond_0
    move-object p7, v0

    .line 234
    :goto_0
    iput-object v0, v2, Lcom/twitter/library/av/e;->e:Ljava/lang/String;

    .line 235
    iput-object p7, v2, Lcom/twitter/library/av/e;->f:Ljava/lang/String;

    .line 236
    iput-object p5, v2, Lcom/twitter/library/av/e;->d:Ljava/lang/String;

    .line 239
    invoke-static {v3, p2}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    iget-object v1, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    .line 240
    invoke-virtual {v0, v1}, Lbsq$a;->b(Ljava/lang/String;)Lbsq$a;

    move-result-object v0

    .line 241
    invoke-virtual {v2}, Lcom/twitter/library/av/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsq$a;->c(Ljava/lang/String;)Lbsq$a;

    move-result-object v0

    .line 243
    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->aB:Lcom/twitter/library/api/PromotedEvent;

    if-ne v1, v3, :cond_6

    .line 244
    iget-object v1, p3, Lcom/twitter/library/av/m;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbsq$a;->d(Ljava/lang/String;)Lbsq$a;

    .line 255
    :cond_1
    :goto_1
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 257
    :cond_2
    return-void

    .line 225
    :cond_3
    sget-object v4, Lcom/twitter/library/api/PromotedEvent;->O:Lcom/twitter/library/api/PromotedEvent;

    if-eq v4, v3, :cond_4

    sget-object v4, Lcom/twitter/library/api/PromotedEvent;->N:Lcom/twitter/library/api/PromotedEvent;

    if-ne v4, v3, :cond_5

    :cond_4
    move-object p5, v0

    move-object v0, p6

    .line 227
    goto :goto_0

    :cond_5
    move-object p7, v0

    move-object p5, v0

    .line 231
    goto :goto_0

    .line 245
    :cond_6
    iget-boolean v1, p0, Lcom/twitter/android/av/f$a;->a:Z

    if-eqz v1, :cond_1

    .line 246
    invoke-static {v3}, Lbsu;->a(Lcom/twitter/library/api/PromotedEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    new-instance v1, Lbsu$a;

    invoke-direct {v1}, Lbsu$a;-><init>()V

    iget-object v2, p3, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    .line 249
    invoke-static {v2}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lbsu$a;->a(Z)Lbsu$a;

    move-result-object v1

    .line 250
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v2

    invoke-static {v2}, Lbsu;->a(Lcom/twitter/library/av/playback/AVDataSource;)I

    move-result v2

    invoke-virtual {v1, v2}, Lbsu$a;->a(I)Lbsu$a;

    move-result-object v1

    .line 251
    invoke-virtual {v1}, Lbsu$a;->a()Lbsu;

    move-result-object v1

    .line 252
    invoke-static {v1}, Lbsr;->a(Lbsr$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsq$a;->d(Ljava/lang/String;)Lbsq$a;

    goto :goto_1
.end method
