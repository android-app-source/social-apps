.class public Lcom/twitter/android/av/j;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/t;


# instance fields
.field private final a:Z

.field private final b:Lcom/twitter/app/common/util/m;

.field private final c:Lcom/twitter/android/widget/v;

.field private d:Lcom/twitter/android/widget/u;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/widget/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/widget/b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/widget/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/widget/a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/widget/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:I

.field private k:Z

.field private l:Lcom/twitter/android/av/r;

.field private final m:Lcom/twitter/android/av/k;

.field private n:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v1

    invoke-virtual {v1}, Lalc;->n()Lcom/twitter/app/common/util/m;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/av/j;-><init>(Landroid/content/Context;ZLcom/twitter/app/common/util/m;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/twitter/app/common/util/m;)V
    .locals 6

    .prologue
    .line 94
    new-instance v1, Lcom/twitter/android/av/k;

    invoke-direct {v1, p1}, Lcom/twitter/android/av/k;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/twitter/android/widget/v;

    invoke-direct {v2}, Lcom/twitter/android/widget/v;-><init>()V

    const/4 v0, 0x1

    .line 95
    invoke-static {v0}, Lcom/twitter/android/av/r;->a(I)Lcom/twitter/android/av/r;

    move-result-object v3

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    .line 94
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/av/j;-><init>(Lcom/twitter/android/av/k;Lcom/twitter/android/widget/v;Lcom/twitter/android/av/r;ZLcom/twitter/app/common/util/m;)V

    .line 97
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/av/k;Lcom/twitter/android/widget/v;Lcom/twitter/android/av/r;ZLcom/twitter/app/common/util/m;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/16 v1, 0xc

    .line 116
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 59
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    .line 64
    invoke-static {v1}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/j;->f:Ljava/util/List;

    .line 66
    invoke-static {v1}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/j;->g:Ljava/util/List;

    .line 68
    invoke-static {v1}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/j;->h:Ljava/util/List;

    .line 70
    invoke-static {v1}, Lcom/twitter/util/collection/MutableSet;->a(I)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/j;->i:Ljava/util/Set;

    .line 75
    const/4 v0, 0x5

    iput v0, p0, Lcom/twitter/android/av/j;->j:I

    .line 117
    iput-object p1, p0, Lcom/twitter/android/av/j;->m:Lcom/twitter/android/av/k;

    .line 118
    iput-object p2, p0, Lcom/twitter/android/av/j;->c:Lcom/twitter/android/widget/v;

    .line 119
    iput-object p3, p0, Lcom/twitter/android/av/j;->l:Lcom/twitter/android/av/r;

    .line 120
    iput-boolean p4, p0, Lcom/twitter/android/av/j;->a:Z

    .line 121
    iput-object p5, p0, Lcom/twitter/android/av/j;->b:Lcom/twitter/app/common/util/m;

    .line 122
    return-void
.end method

.method private a(Landroid/view/ViewGroup;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/widget/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    if-lez p2, :cond_3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 322
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 323
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 324
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 326
    instance-of v0, v1, Lcom/twitter/android/cu;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/cu;

    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/twitter/android/av/j;->f:Ljava/util/List;

    check-cast v1, Lcom/twitter/android/cu;

    iget-object v1, v1, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 328
    :cond_1
    instance-of v0, v2, Lcom/twitter/library/widget/b;

    if-eqz v0, :cond_2

    .line 329
    iget-object v1, p0, Lcom/twitter/android/av/j;->f:Ljava/util/List;

    move-object v0, v2

    check-cast v0, Lcom/twitter/library/widget/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 330
    :cond_2
    instance-of v0, v2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 331
    check-cast v2, Landroid/view/ViewGroup;

    add-int/lit8 v0, p2, -0x1

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/av/j;->a(Landroid/view/ViewGroup;I)Ljava/util/List;

    goto :goto_1

    .line 335
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/av/j;->f:Ljava/util/List;

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;Z)V
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/twitter/android/av/j;->k:Z

    if-nez v0, :cond_1

    .line 221
    invoke-direct {p0, p1}, Lcom/twitter/android/av/j;->d(Landroid/view/ViewGroup;)V

    .line 222
    if-eqz p2, :cond_2

    .line 223
    iget-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    invoke-virtual {v0}, Lcom/twitter/android/widget/u;->b()V

    .line 227
    :goto_0
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    invoke-virtual {v0}, Lcom/twitter/android/widget/u;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/j;->b()V

    .line 231
    :cond_1
    return-void

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    invoke-virtual {v0}, Lcom/twitter/android/widget/u;->a()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/av/j;Landroid/view/ViewGroup;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/av/j;->a(Landroid/view/ViewGroup;Z)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/j;Landroid/view/ViewGroup;)Z
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/twitter/android/av/j;->c(Landroid/view/ViewGroup;)Z

    move-result v0

    return v0
.end method

.method private c(Landroid/view/ViewGroup;)Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/twitter/android/av/j;->k:Z

    if-nez v0, :cond_0

    .line 196
    invoke-direct {p0, p1}, Lcom/twitter/android/av/j;->d(Landroid/view/ViewGroup;)V

    .line 197
    iget-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    invoke-virtual {v0}, Lcom/twitter/android/widget/u;->a()V

    .line 199
    iget-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    invoke-virtual {v0}, Lcom/twitter/android/widget/u;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lcom/twitter/android/av/j;->b()V

    .line 203
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private d(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    .line 235
    invoke-virtual {v0}, Lcom/twitter/android/widget/u;->d()Landroid/view/ViewGroup;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/j;->c:Lcom/twitter/android/widget/v;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/v;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/widget/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/j;->d:Lcom/twitter/android/widget/u;

    .line 238
    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 241
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/a;

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 244
    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/j;->k:Z

    .line 358
    invoke-direct {p0}, Lcom/twitter/android/av/j;->g()V

    .line 359
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 360
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/twitter/android/av/j;->b()V

    .line 249
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 130
    invoke-static {p1}, Lcom/twitter/android/av/r;->a(I)Lcom/twitter/android/av/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/j;->l:Lcom/twitter/android/av/r;

    .line 131
    return-void
.end method

.method a(Landroid/view/ViewGroup;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 177
    iput-object p1, p0, Lcom/twitter/android/av/j;->n:Landroid/view/ViewGroup;

    .line 178
    invoke-virtual {p0}, Lcom/twitter/android/av/j;->b()V

    .line 179
    return-void
.end method

.method public a(Lcno;)V
    .locals 1

    .prologue
    .line 184
    invoke-interface {p1}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/av/j;->c(Landroid/view/ViewGroup;)Z

    .line 185
    return-void
.end method

.method public a(Lcno;I)V
    .locals 2

    .prologue
    .line 209
    invoke-interface {p1}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v1

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/av/j;->a(Landroid/view/ViewGroup;Z)V

    .line 210
    return-void

    .line 209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/av/r;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/twitter/android/av/j;->l:Lcom/twitter/android/av/r;

    .line 135
    return-void
.end method

.method public a(Lcom/twitter/android/client/o;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p1, Lcom/twitter/android/client/o;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/j;->a(Landroid/view/ViewGroup;)V

    .line 167
    invoke-virtual {p1, p0}, Lcom/twitter/android/client/o;->a(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 168
    return-void
.end method

.method public a(Lcom/twitter/app/common/list/l;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/j;->a(Landroid/view/ViewGroup;)V

    .line 145
    new-instance v0, Lcom/twitter/android/av/j$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/j$1;-><init>(Lcom/twitter/android/av/j;)V

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 157
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 257
    iget-object v0, p0, Lcom/twitter/android/av/j;->n:Landroid/view/ViewGroup;

    .line 258
    if-eqz v0, :cond_0

    .line 259
    iget-object v1, p0, Lcom/twitter/android/av/j;->m:Lcom/twitter/android/av/k;

    invoke-virtual {v1}, Lcom/twitter/android/av/k;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/av/j;->a:Z

    if-nez v1, :cond_1

    .line 260
    invoke-direct {p0}, Lcom/twitter/android/av/j;->g()V

    .line 261
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/j;->b(Landroid/view/ViewGroup;)V

    .line 266
    iget-object v1, p0, Lcom/twitter/android/av/j;->h:Ljava/util/List;

    iget-object v3, p0, Lcom/twitter/android/av/j;->l:Lcom/twitter/android/av/r;

    iget-object v4, p0, Lcom/twitter/android/av/j;->g:Ljava/util/List;

    invoke-virtual {v3, v0, v4}, Lcom/twitter/android/av/r;->a(Landroid/view/ViewGroup;Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move v1, v2

    .line 269
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 270
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/a;

    .line 271
    iget-object v3, p0, Lcom/twitter/android/av/j;->h:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 272
    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 273
    iget-object v3, p0, Lcom/twitter/android/av/j;->i:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 269
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 276
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/android/av/j;->i:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 279
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/av/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 280
    iget-object v0, p0, Lcom/twitter/android/av/j;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/a;

    .line 281
    iget-object v1, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 282
    iget-object v1, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    invoke-interface {v0}, Lcom/twitter/library/widget/a;->at_()V

    .line 279
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 287
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/av/j;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 288
    iget-object v0, p0, Lcom/twitter/android/av/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 289
    iget-object v0, p0, Lcom/twitter/android/av/j;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method b(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 300
    iget-object v0, p0, Lcom/twitter/android/av/j;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 301
    iget v0, p0, Lcom/twitter/android/av/j;->j:I

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/j;->a(Landroid/view/ViewGroup;I)Ljava/util/List;

    .line 302
    iget-object v0, p0, Lcom/twitter/android/av/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 303
    iget-object v0, p0, Lcom/twitter/android/av/j;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/b;

    invoke-interface {v0}, Lcom/twitter/library/widget/b;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    .line 304
    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 305
    iget-object v2, p0, Lcom/twitter/android/av/j;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 309
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/twitter/android/av/j;->b:Lcom/twitter/app/common/util/m;

    invoke-virtual {v0}, Lcom/twitter/app/common/util/m;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    invoke-direct {p0}, Lcom/twitter/android/av/j;->h()V

    .line 345
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/twitter/android/av/j;->b:Lcom/twitter/app/common/util/m;

    invoke-virtual {v0}, Lcom/twitter/app/common/util/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    invoke-direct {p0}, Lcom/twitter/android/av/j;->h()V

    .line 354
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/twitter/android/av/j;->m:Lcom/twitter/android/av/k;

    invoke-virtual {v0}, Lcom/twitter/android/av/k;->c()V

    .line 367
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/j;->k:Z

    .line 368
    iget-object v0, p0, Lcom/twitter/android/av/j;->n:Landroid/view/ViewGroup;

    .line 369
    if-eqz v0, :cond_1

    .line 372
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/j;->b(Landroid/view/ViewGroup;)V

    .line 373
    iget-object v0, p0, Lcom/twitter/android/av/j;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/a;

    .line 374
    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    goto :goto_0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/j;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 377
    invoke-virtual {p0}, Lcom/twitter/android/av/j;->b()V

    .line 379
    :cond_1
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 385
    invoke-direct {p0}, Lcom/twitter/android/av/j;->g()V

    .line 386
    iget-object v0, p0, Lcom/twitter/android/av/j;->m:Lcom/twitter/android/av/k;

    invoke-virtual {v0}, Lcom/twitter/android/av/k;->b()V

    .line 387
    iget-object v0, p0, Lcom/twitter/android/av/j;->c:Lcom/twitter/android/widget/v;

    invoke-virtual {v0}, Lcom/twitter/android/widget/v;->a()V

    .line 388
    return-void
.end method

.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1

    .prologue
    .line 216
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/j;->a(Landroid/view/ViewGroup;Z)V

    .line 217
    return-void

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/twitter/android/av/j;->c(Landroid/view/ViewGroup;)Z

    .line 192
    return-void
.end method
