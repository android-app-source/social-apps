.class public Lcom/twitter/android/av/ae;
.super Lcom/twitter/android/av/b$a;
.source "Twttr"


# static fields
.field static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 44
    const-string/jumbo v1, "show"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    const-string/jumbo v1, "open"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    const-string/jumbo v1, "video_ad_skip"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    const-string/jumbo v1, "view_threshold"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    const-string/jumbo v1, "play_from_tap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    const-string/jumbo v1, "video_view"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    const-string/jumbo v1, "cta_app_open_impression"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    const-string/jumbo v1, "cta_app_install_impression"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    const-string/jumbo v1, "cta_url_impression"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    const-string/jumbo v1, "cta_watch_impression"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 55
    const-string/jumbo v1, "cta_app_open_click"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    const-string/jumbo v1, "cta_app_install_click"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    const-string/jumbo v1, "cta_url_click"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    const-string/jumbo v1, "cta_watch_click"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    const-string/jumbo v1, "video_mrc_view"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    const-string/jumbo v1, "heartbeat"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    const-string/jumbo v1, "playback_startup_error"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    const-string/jumbo v1, "playback_abandoned"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    const-string/jumbo v1, "marketplace_ad_impression"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/av/ae;->c:Ljava/util/Set;

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/twitter/android/av/b$a;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 69
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/av/AVMediaPlaylist;)V
    .locals 2

    .prologue
    .line 119
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 123
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/model/av/AVMediaPlaylist;->k()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 124
    :goto_1
    if-eqz v0, :cond_1

    .line 125
    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aj:Ljava/lang/String;

    .line 126
    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->i()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    .line 129
    :cond_1
    invoke-static {p1}, Lcom/twitter/model/av/f;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/model/av/DynamicAd;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_3

    iget v0, v0, Lcom/twitter/model/av/DynamicAd;->b:I

    :goto_2
    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    .line 132
    invoke-static {p1}, Lcom/twitter/model/av/f;->b(Lcom/twitter/model/av/AVMediaPlaylist;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->al:Ljava/lang/String;

    goto :goto_0

    .line 123
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 130
    :cond_3
    const/4 v0, -0x1

    goto :goto_2
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/m;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 9

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/av/ae;->a:Lcom/twitter/library/av/playback/ax;

    iget-object v1, p1, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/av/playback/ax;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 84
    iget-object v0, p0, Lcom/twitter/android/av/ae;->a:Lcom/twitter/library/av/playback/ax;

    .line 85
    invoke-interface {v0}, Lcom/twitter/library/av/playback/ax;->a()Lbis;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/c;->a(Lbis;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/av/AVMedia;)V

    .line 87
    iget-object v0, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    if-eqz v0, :cond_0

    .line 88
    iget-boolean v0, p0, Lcom/twitter/android/av/ae;->b:Z

    if-eqz v0, :cond_2

    const-string/jumbo v2, ""

    .line 89
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/av/ae;->b:Z

    if-eqz v0, :cond_3

    const-wide/16 v4, -0x1

    .line 90
    :goto_1
    iget-object v0, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->c()Ljava/lang/String;

    move-result-object v3

    iget-boolean v6, p1, Lcom/twitter/library/av/m;->i:Z

    iget-object v7, p1, Lcom/twitter/library/av/m;->l:Ljava/lang/Boolean;

    iget-object v8, p1, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/Boolean;Ljava/lang/Long;)V

    .line 92
    iget-object v0, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->g()Lcom/twitter/model/av/a;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    invoke-interface {v0}, Lcom/twitter/model/av/a;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Y:Ljava/lang/String;

    .line 95
    invoke-interface {v0}, Lcom/twitter/model/av/a;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Z:Ljava/lang/String;

    .line 96
    invoke-interface {v0}, Lcom/twitter/model/av/a;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aa:Ljava/lang/String;

    .line 100
    :cond_0
    iget-object v0, p1, Lcom/twitter/library/av/m;->r:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p1, Lcom/twitter/library/av/m;->r:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ad:J

    .line 104
    :cond_1
    iget-object v0, p1, Lcom/twitter/library/av/m;->o:Lbyf;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/twitter/library/av/m;->o:Lbyf;

    invoke-interface {v0}, Lbyf;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    iput v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ac:I

    .line 107
    iget-object v0, p1, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    invoke-static {v1, v0}, Lcom/twitter/android/av/ae;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/av/AVMediaPlaylist;)V

    .line 108
    return-object v1

    .line 88
    :cond_2
    iget-object v0, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->d()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 89
    :cond_3
    iget-object v0, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->i()J

    move-result-wide v4

    goto :goto_1

    .line 104
    :cond_4
    const/4 v0, 0x2

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/twitter/android/av/ae;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/ae;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
