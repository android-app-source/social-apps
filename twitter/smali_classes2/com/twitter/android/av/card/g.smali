.class public Lcom/twitter/android/av/card/g;
.super Lcom/twitter/android/card/n;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/widget/a;


# instance fields
.field a:Z

.field final b:Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

.field private c:Z

.field private final d:Lcom/twitter/android/av/ad;

.field private final e:Lcom/twitter/android/av/ah;

.field private final f:Lcom/twitter/library/av/playback/q;

.field private final g:Lcom/twitter/android/av/u;

.field private final h:Lcom/twitter/android/av/video/e$b;

.field private i:Lcom/twitter/android/av/video/e;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 10

    .prologue
    .line 46
    new-instance v3, Lcom/twitter/android/card/f;

    invoke-direct {v3, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/twitter/android/card/c;

    invoke-direct {v4, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    new-instance v5, Lcom/twitter/android/av/ad;

    invoke-direct {v5}, Lcom/twitter/android/av/ad;-><init>()V

    .line 50
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v6

    new-instance v7, Lcom/twitter/android/av/ah;

    invoke-direct {v7}, Lcom/twitter/android/av/ah;-><init>()V

    new-instance v8, Lcom/twitter/android/av/u;

    invoke-direct {v8}, Lcom/twitter/android/av/u;-><init>()V

    new-instance v9, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v9}, Lcom/twitter/android/av/video/e$b;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 46
    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/av/card/g;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcom/twitter/android/av/ad;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/ah;Lcom/twitter/android/av/u;Lcom/twitter/android/av/video/e$b;)V

    .line 54
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcom/twitter/android/av/ad;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/ah;Lcom/twitter/android/av/u;Lcom/twitter/android/av/video/e$b;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 66
    iput-object p5, p0, Lcom/twitter/android/av/card/g;->d:Lcom/twitter/android/av/ad;

    .line 67
    iput-object p6, p0, Lcom/twitter/android/av/card/g;->f:Lcom/twitter/library/av/playback/q;

    .line 68
    iput-object p7, p0, Lcom/twitter/android/av/card/g;->e:Lcom/twitter/android/av/ah;

    .line 69
    iput-object p8, p0, Lcom/twitter/android/av/card/g;->g:Lcom/twitter/android/av/u;

    .line 70
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/card/g;->a(Landroid/content/Context;)Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/g;->b:Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->b:Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iput-object p9, p0, Lcom/twitter/android/av/card/g;->h:Lcom/twitter/android/av/video/e$b;

    .line 74
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;
    .locals 1

    .prologue
    .line 198
    new-instance v0, Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

    invoke-direct {v0, p1}, Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/twitter/android/card/n;->a()V

    .line 109
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    .line 113
    :cond_0
    return-void
.end method

.method public a(JLcom/twitter/library/card/CardContext;)V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/card/n;->a(JLcom/twitter/library/card/CardContext;)V

    .line 79
    invoke-static {p3}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 80
    iget-object v1, p0, Lcom/twitter/android/av/card/g;->g:Lcom/twitter/android/av/u;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/u;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/av/card/g;->c:Z

    .line 81
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 10

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/av/card/g;->l()Landroid/app/Activity;

    move-result-object v1

    .line 88
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/card/g;->w:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->w:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 90
    new-instance v8, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v8, v0}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    .line 91
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->h:Lcom/twitter/android/av/video/e$b;

    iget-object v2, p0, Lcom/twitter/android/av/card/g;->b:Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

    iget-object v3, p0, Lcom/twitter/android/av/card/g;->d:Lcom/twitter/android/av/ad;

    iget-object v4, p0, Lcom/twitter/android/av/card/g;->e:Lcom/twitter/android/av/ah;

    iget-object v5, p0, Lcom/twitter/android/av/card/g;->f:Lcom/twitter/library/av/playback/q;

    iget-object v6, p0, Lcom/twitter/android/av/card/g;->g:Lcom/twitter/android/av/u;

    iget-object v7, p0, Lcom/twitter/android/av/card/g;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/av/video/e$b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/av/ad;Lcom/twitter/android/av/ah;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/u;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    .line 94
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    sget-object v1, Lbyo;->c:Lbyf;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/video/e;->a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 95
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v1, p0, Lcom/twitter/android/av/card/g;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_1

    .line 96
    invoke-static {}, Lcom/twitter/android/revenue/k;->h()Lcom/twitter/library/av/model/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/model/b;->a()F

    move-result v0

    .line 98
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/av/card/g;->b:Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;->setAspectRatio(F)V

    .line 99
    iget-boolean v0, p0, Lcom/twitter/android/av/card/g;->a:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/av/card/g;->a:Z

    .line 104
    :cond_0
    return-void

    .line 97
    :cond_1
    invoke-virtual {v8}, Lcom/twitter/library/av/playback/TweetAVDataSource;->o()F

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/av/card/g;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Z)V

    .line 126
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->d()V

    .line 129
    :cond_0
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->at_()V

    .line 174
    :cond_0
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->au_()V

    .line 181
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/card/g;->a:Z

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->b(Z)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->g:Lcom/twitter/android/av/u;

    invoke-virtual {v0}, Lcom/twitter/android/av/u;->a()V

    .line 135
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/twitter/android/av/card/g;->c:Z

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lcom/twitter/android/card/n;->d()V

    .line 118
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->b()V

    .line 121
    :cond_0
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->b:Lcom/twitter/library/widget/LandscapeAwareAspectRatioFrameLayout;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->h()V

    .line 188
    :cond_0
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/twitter/android/av/card/g;->e()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/android/av/card/g;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/video/e;->onClick(Landroid/view/View;)V

    .line 156
    :cond_0
    return-void
.end method
