.class public Lcom/twitter/android/av/card/d;
.super Lcom/twitter/android/card/n;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/card/ak$a;
.implements Lcom/twitter/library/card/q$a;
.implements Lcom/twitter/library/widget/a;


# instance fields
.field final a:Landroid/view/View;

.field final b:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

.field final c:Lcom/twitter/android/card/CallToAction;

.field d:Ljava/lang/Long;

.field e:J

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field final h:Lcom/twitter/ui/widget/i;

.field i:Lcom/twitter/android/av/video/e;

.field private final k:Lcom/twitter/android/av/video/e$b;

.field private final l:Lcom/twitter/android/av/ah;

.field private final m:Lcom/twitter/library/av/playback/q;

.field private final n:Lcom/twitter/android/av/ad;

.field private final o:Lcom/twitter/android/av/video/d;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 10

    .prologue
    .line 72
    new-instance v3, Lcom/twitter/android/card/f;

    invoke-direct {v3, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/twitter/android/card/c;

    invoke-direct {v4, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    new-instance v5, Lcom/twitter/android/av/video/d$a;

    invoke-direct {v5, p1}, Lcom/twitter/android/av/video/d$a;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v6

    new-instance v7, Lcom/twitter/android/av/ah;

    invoke-direct {v7}, Lcom/twitter/android/av/ah;-><init>()V

    new-instance v8, Lcom/twitter/android/av/ad;

    invoke-direct {v8}, Lcom/twitter/android/av/ad;-><init>()V

    new-instance v9, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v9}, Lcom/twitter/android/av/video/e$b;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 72
    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/av/card/d;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcom/twitter/android/av/video/d$a;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/ah;Lcom/twitter/android/av/ad;Lcom/twitter/android/av/video/e$b;)V

    .line 76
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcom/twitter/android/av/video/d$a;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/ah;Lcom/twitter/android/av/ad;Lcom/twitter/android/av/video/e$b;)V
    .locals 4

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 88
    invoke-virtual {p5, p1}, Lcom/twitter/android/av/video/d$a;->a(Landroid/app/Activity;)Lcom/twitter/android/av/video/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    .line 89
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/video/d;->a(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/d;->c()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->a:Landroid/view/View;

    .line 91
    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->h:Lcom/twitter/ui/widget/i;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->a:Landroid/view/View;

    const v1, 0x7f130396

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->b:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 93
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->b:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    const v1, 0x3fe38e39

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 94
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04025a

    iget-object v0, p0, Lcom/twitter/android/av/card/d;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/CallToAction;

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    .line 96
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 97
    iput-object p9, p0, Lcom/twitter/android/av/card/d;->k:Lcom/twitter/android/av/video/e$b;

    .line 98
    iput-object p6, p0, Lcom/twitter/android/av/card/d;->m:Lcom/twitter/library/av/playback/q;

    .line 99
    iput-object p7, p0, Lcom/twitter/android/av/card/d;->l:Lcom/twitter/android/av/ah;

    .line 100
    iput-object p8, p0, Lcom/twitter/android/av/card/d;->n:Lcom/twitter/android/av/ad;

    .line 101
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 127
    invoke-super {p0}, Lcom/twitter/android/card/n;->a()V

    .line 129
    invoke-virtual {p0}, Lcom/twitter/android/av/card/d;->j()Lcom/twitter/library/card/q;

    move-result-object v0

    .line 130
    iget-wide v2, p0, Lcom/twitter/android/av/card/d;->e:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->d:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/av/card/d;->k()Lcom/twitter/library/card/ak;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/twitter/android/av/card/d;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->b(JLjava/lang/Object;)V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/d;->b()V

    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    .line 143
    :cond_1
    return-void
.end method

.method public a(JLcar;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 223
    const-string/jumbo v0, "app_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->f:Ljava/lang/String;

    .line 224
    const-string/jumbo v0, "app_name"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->g:Ljava/lang/String;

    .line 225
    const-string/jumbo v0, "title"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 226
    const-string/jumbo v2, "description"

    invoke-static {v2, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v2

    .line 227
    iget-object v3, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    invoke-virtual {v3, v0}, Lcom/twitter/android/av/video/d;->b(Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    invoke-virtual {v0, v2}, Lcom/twitter/android/av/video/d;->a(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    if-eqz v0, :cond_1

    .line 231
    const/16 v0, 0x8

    .line 232
    iget-object v2, p0, Lcom/twitter/android/av/card/d;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/av/card/d;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    invoke-virtual {p0}, Lcom/twitter/android/av/card/d;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/card/CallToAction;->setScribeElement(Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    iget-object v2, p0, Lcom/twitter/android/av/card/d;->s:Lcom/twitter/android/card/b;

    invoke-virtual {v0, v2}, Lcom/twitter/android/card/CallToAction;->setCardActionHandler(Lcom/twitter/android/card/b;)V

    .line 235
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    iget-object v2, p0, Lcom/twitter/android/av/card/d;->r:Lcom/twitter/android/card/d;

    invoke-virtual {v0, v2}, Lcom/twitter/android/card/CallToAction;->setCardLogger(Lcom/twitter/android/card/d;)V

    .line 236
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    iget-object v2, p0, Lcom/twitter/android/av/card/d;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/av/card/d;->g:Ljava/lang/String;

    move-object v4, v1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/card/CallToAction;->a(Lcom/twitter/library/card/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v0, 0x0

    .line 240
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    invoke-virtual {v1, v0}, Lcom/twitter/android/card/CallToAction;->setVisibility(I)V

    .line 242
    :cond_1
    return-void
.end method

.method public a(JLcom/twitter/library/card/CardContext;)V
    .locals 1

    .prologue
    .line 214
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/card/n;->a(JLcom/twitter/library/card/CardContext;)V

    .line 216
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->c:Lcom/twitter/android/card/CallToAction;

    invoke-virtual {v0, p3}, Lcom/twitter/android/card/CallToAction;->setCardContext(Lcom/twitter/library/card/CardContext;)V

    .line 219
    :cond_0
    return-void
.end method

.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    invoke-virtual {v0, p3}, Lcom/twitter/android/av/video/d;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 210
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 10

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 106
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/av/card/d;->e:J

    .line 107
    invoke-virtual {p0}, Lcom/twitter/android/av/card/d;->j()Lcom/twitter/library/card/q;

    move-result-object v0

    .line 108
    iget-wide v2, p0, Lcom/twitter/android/av/card/d;->e:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 109
    const-string/jumbo v0, "site"

    iget-object v1, p1, Lcom/twitter/library/card/z$a;->c:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->d:Ljava/lang/Long;

    .line 110
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->d:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/twitter/android/av/card/d;->k()Lcom/twitter/library/card/ak;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/twitter/android/av/card/d;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->a(JLjava/lang/Object;)V

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/card/d;->l()Landroid/app/Activity;

    move-result-object v1

    .line 116
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/av/card/d;->w:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->w:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/twitter/model/core/Tweet;

    .line 118
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->k:Lcom/twitter/android/av/video/e$b;

    iget-object v2, p0, Lcom/twitter/android/av/card/d;->b:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    iget-object v3, p0, Lcom/twitter/android/av/card/d;->n:Lcom/twitter/android/av/ad;

    iget-object v4, p0, Lcom/twitter/android/av/card/d;->l:Lcom/twitter/android/av/ah;

    iget-object v5, p0, Lcom/twitter/android/av/card/d;->m:Lcom/twitter/library/av/playback/q;

    new-instance v6, Lcom/twitter/android/av/u;

    invoke-direct {v6}, Lcom/twitter/android/av/u;-><init>()V

    iget-object v7, p0, Lcom/twitter/android/av/card/d;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    new-instance v8, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v8, v9}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/av/video/e$b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/av/ad;Lcom/twitter/android/av/ah;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/u;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    .line 121
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    sget-object v1, Lbyo;->c:Lbyf;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/video/e;->a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 123
    :cond_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/av/card/d;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Z)V

    .line 191
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->d()V

    .line 194
    :cond_0
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->at_()V

    .line 172
    :cond_0
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->au_()V

    .line 179
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->o:Lcom/twitter/android/av/video/d;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/d;->a()V

    .line 149
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 152
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/twitter/android/card/n;->d()V

    .line 162
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->b()V

    .line 165
    :cond_0
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->a:Landroid/view/View;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->h()V

    .line 186
    :cond_0
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->i()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected j()Lcom/twitter/library/card/q;
    .locals 1

    .prologue
    .line 266
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    return-object v0
.end method

.method protected k()Lcom/twitter/library/card/ak;
    .locals 1

    .prologue
    .line 270
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 263
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 248
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/video/e;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 254
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->d:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/twitter/android/av/card/d;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/av/card/d;->a(J)V

    goto :goto_0

    .line 246
    :pswitch_data_0
    .packed-switch 0x7f130393
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
