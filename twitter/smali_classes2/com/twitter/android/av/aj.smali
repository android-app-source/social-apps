.class public Lcom/twitter/android/av/aj;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/aj$a;,
        Lcom/twitter/android/av/aj$b;
    }
.end annotation


# instance fields
.field protected final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/av/aj$b;

.field private c:Lcom/twitter/android/av/aj$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/av/aj;->a:Landroid/content/Context;

    .line 26
    new-instance v0, Lcom/twitter/android/av/aj$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/av/aj$b;-><init>(Lcom/twitter/android/av/aj;Lcom/twitter/android/av/aj$1;)V

    iput-object v0, p0, Lcom/twitter/android/av/aj;->b:Lcom/twitter/android/av/aj$b;

    .line 27
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/av/aj;->b:Lcom/twitter/android/av/aj$b;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/aj$b;->removeMessages(I)V

    .line 43
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 34
    iget-object v0, p0, Lcom/twitter/android/av/aj;->b:Lcom/twitter/android/av/aj$b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/aj$b;->removeMessages(I)V

    .line 35
    iget-object v0, p0, Lcom/twitter/android/av/aj;->b:Lcom/twitter/android/av/aj$b;

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/android/av/aj$b;->sendEmptyMessageDelayed(IJ)Z

    .line 36
    return-void
.end method

.method public a(Lcom/twitter/android/av/aj$a;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/av/aj;->c:Lcom/twitter/android/av/aj$a;

    .line 59
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/av/aj;->b:Lcom/twitter/android/av/aj$b;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/aj$b;->hasMessages(I)Z

    move-result v0

    return v0
.end method

.method public c()Lcom/twitter/android/av/aj$a;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/av/aj;->c:Lcom/twitter/android/av/aj$a;

    return-object v0
.end method
