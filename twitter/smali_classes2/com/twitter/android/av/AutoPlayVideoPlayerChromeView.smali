.class public Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/av/control/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/android/av/AutoPlayBadgeView;

.field protected b:Landroid/view/View;

.field c:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field d:Landroid/view/View;

.field e:Z

.field f:Z

.field private final g:Lcom/twitter/library/av/control/b;

.field private h:Z

.field private i:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-static {}, Lcom/twitter/library/av/control/c;->b()Lcom/twitter/library/av/control/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->g:Lcom/twitter/library/av/control/b;

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    invoke-static {}, Lcom/twitter/library/av/control/c;->b()Lcom/twitter/library/av/control/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->g:Lcom/twitter/library/av/control/b;

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    invoke-static {}, Lcom/twitter/library/av/control/c;->b()Lcom/twitter/library/av/control/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->g:Lcom/twitter/library/av/control/b;

    .line 63
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 339
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 345
    :cond_0
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 352
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lcom/twitter/util/e;->b(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    .line 354
    :cond_0
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    .line 360
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    const/16 v1, 0x12c

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/util/e;->a(Landroid/view/View;ILandroid/view/animation/Interpolator;I)Landroid/view/ViewPropertyAnimator;

    .line 363
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->o()V

    .line 171
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/av/ad;)V
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->o()V

    .line 249
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->i:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->i:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

    invoke-interface {v0, p2}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;->a(Lcom/twitter/library/av/ad;)V

    .line 252
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 2

    .prologue
    .line 159
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->d:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    if-ne p1, v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_1

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->n()V

    .line 166
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->removeAllViews()V

    .line 84
    iput-object p1, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->c:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 88
    const v0, 0x7f04003d

    invoke-virtual {v2, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 89
    const v0, 0x7f13019c

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/AutoPlayBadgeView;

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    .line 91
    const v0, 0x7f04003e

    invoke-virtual {v2, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 92
    const v0, 0x7f13019a

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->b:Landroid/view/View;

    .line 94
    const v0, 0x7f04003f

    invoke-virtual {v2, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 95
    const v0, 0x7f13019b

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    if-eqz p1, :cond_0

    .line 99
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    .line 100
    iget-object v2, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v2, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAVDataSource(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 101
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->e:Z

    .line 103
    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->e:Z

    if-eqz v0, :cond_2

    .line 104
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->p()V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeView;->setVisibility(I)V

    .line 111
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 101
    goto :goto_0

    .line 107
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->q()V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->h:Z

    if-nez v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    if-eqz v0, :cond_2

    .line 263
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/AutoPlayBadgeView;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 266
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->g:Lcom/twitter/library/av/control/b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/b;->a(Lcom/twitter/library/av/playback/aa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->r()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)V
    .locals 1

    .prologue
    .line 150
    invoke-static {p1}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->f:Z

    .line 152
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAvMedia(Lcom/twitter/model/av/AVMedia;)V

    .line 155
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 272
    return-void
.end method

.method public a_(Z)V
    .locals 0

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->o()V

    .line 196
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->n()V

    .line 176
    return-void
.end method

.method public b_(Z)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->c:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->m()V

    .line 183
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->k()V

    .line 188
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->o()V

    .line 206
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 78
    return-object p0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 212
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->k()V

    .line 217
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 220
    :cond_0
    return-void
.end method

.method k()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->c:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 224
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 225
    :goto_0
    if-nez v0, :cond_1

    .line 234
    :goto_1
    return-void

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 227
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 228
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->n()V

    goto :goto_1

    .line 229
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->u()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->m()V

    goto :goto_1

    .line 232
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->o()V

    goto :goto_1
.end method

.method public l()V
    .locals 0

    .prologue
    .line 237
    return-void
.end method

.method protected m()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->a()V

    .line 292
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->s()V

    .line 293
    return-void
.end method

.method protected n()V
    .locals 2

    .prologue
    .line 299
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->h:Z

    .line 301
    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->e:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->f:Z

    if-nez v0, :cond_1

    .line 302
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->p()V

    .line 303
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeView;->setVisibility(I)V

    .line 309
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->b()V

    .line 312
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->s()V

    .line 313
    return-void

    .line 305
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->q()V

    .line 306
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 319
    iput-boolean v1, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->h:Z

    .line 321
    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->e:Z

    if-eqz v0, :cond_1

    .line 322
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->p()V

    .line 323
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeView;->setVisibility(I)V

    .line 329
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->c()V

    .line 332
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->s()V

    .line 333
    return-void

    .line 325
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->q()V

    .line 326
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 140
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->k()V

    .line 141
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->d:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->c:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->c:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->p()V

    .line 279
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 116
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    if-eqz v0, :cond_2

    .line 117
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->getRight()I

    move-result v0

    .line 118
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 119
    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    invoke-virtual {v1}, Lcom/twitter/android/av/AutoPlayBadgeView;->getRight()I

    move-result v1

    .line 120
    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_1

    .line 121
    iget-object v2, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->a:Lcom/twitter/android/av/AutoPlayBadgeView;

    const-string/jumbo v3, "right"

    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v0, v4, v5

    aput v1, v4, v6

    .line 122
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 123
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 124
    invoke-virtual {v0, v6}, Landroid/animation/ObjectAnimator;->setAutoCancel(Z)V

    .line 126
    :cond_0
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 131
    :cond_1
    :goto_0
    return-void

    .line 129
    :cond_2
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    goto :goto_0
.end method

.method public setOnErrorListener(Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->i:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

    .line 283
    return-void
.end method
