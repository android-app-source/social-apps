.class public abstract Lcom/twitter/android/av/RevenueCardCanvasActivity;
.super Lcom/twitter/android/av/AVCardCanvasActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/n$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/av/AVCardCanvasActivity",
        "<",
        "Lcom/twitter/android/av/RevenueCardCanvasView;",
        ">;",
        "Lcom/twitter/util/n$a;"
    }
.end annotation


# instance fields
.field private q:Lcom/twitter/util/n;

.field private r:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->r:I

    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/RevenueCardCanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/av/RevenueCardCanvasView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 174
    const/4 v1, 0x2

    if-ne v1, p1, :cond_0

    .line 175
    const/4 v1, 0x0

    .line 176
    iget-object v2, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-static {v2}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    .line 181
    :goto_0
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 182
    return-void

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e04f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 179
    iget-object v2, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-static {v2}, Lcom/twitter/util/d;->b(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 40
    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->s()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 41
    return-object p2
.end method

.method protected a(Landroid/os/Bundle;)Lcom/twitter/library/av/playback/u;
    .locals 2

    .prologue
    .line 195
    new-instance v0, Lcom/twitter/library/av/playback/v;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/v;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->e:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1, p2}, Lcom/twitter/android/av/AVCardCanvasActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->finish()V

    .line 80
    :goto_0
    return-void

    .line 59
    :cond_0
    const v0, 0x7f13034e

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 61
    const v1, 0x3fe38e39

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 63
    const v0, 0x7f1301a6

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_1

    .line 65
    new-instance v1, Lcom/twitter/android/av/RevenueCardCanvasActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity$1;-><init>(Lcom/twitter/android/av/RevenueCardCanvasActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    :cond_1
    new-instance v0, Lcom/twitter/util/n;

    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/util/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->q:Lcom/twitter/util/n;

    .line 75
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ExpandableViewHost;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->b(I)V

    .line 79
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/RevenueCardCanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/av/RevenueCardCanvasView;->p()V

    .line 191
    return-void
.end method

.method protected abstract b(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
.end method

.method public e_(I)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 103
    const/4 v0, 0x1

    invoke-static {p0}, Lcom/twitter/util/n;->b(Landroid/content/Context;)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 104
    const/16 v1, 0x5a

    .line 105
    const/16 v0, 0x10e

    .line 111
    :goto_0
    invoke-static {p1, v1, v2}, Lcom/twitter/util/math/a;->b(III)Z

    move-result v1

    if-nez v1, :cond_0

    .line 112
    invoke-static {p1, v0, v2}, Lcom/twitter/util/math/a;->b(III)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->setRequestedOrientation(I)V

    .line 115
    :cond_1
    return-void

    .line 107
    :cond_2
    const/4 v1, 0x0

    .line 108
    const/16 v0, 0xb4

    goto :goto_0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    const-string/jumbo v0, "video_app_card_canvas"

    return-object v0
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->j()V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->o:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/RevenueCardCanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/av/RevenueCardCanvasView;->m()V

    .line 96
    :cond_0
    return-void
.end method

.method protected l()Lbyf;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lbyo;->b:Lbyf;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 152
    iget v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->r:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 153
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->r:I

    .line 154
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->b(I)V

    .line 156
    iget-object v1, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 159
    new-instance v0, Lcom/twitter/library/av/playback/r;

    iget-object v2, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->p:Lcom/twitter/library/av/playback/q;

    invoke-direct {v0, v2}, Lcom/twitter/library/av/playback/r;-><init>(Lcom/twitter/library/av/playback/q;)V

    iget-object v2, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->l:Lcom/twitter/library/av/playback/u;

    .line 160
    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 161
    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 162
    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->l()Lbyf;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Lbyf;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 163
    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/r;->a(Landroid/content/Context;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    const/4 v2, 0x1

    .line 164
    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/r;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 166
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->a:Lcom/twitter/android/av/c;

    check-cast v0, Lcom/twitter/android/av/RevenueCardCanvasView;

    iget-object v2, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, v2, p1}, Lcom/twitter/android/av/RevenueCardCanvasView;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;Landroid/content/res/Configuration;)V

    .line 167
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->p:Lcom/twitter/library/av/playback/q;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 169
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->setIntent(Landroid/content/Intent;)V

    .line 48
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->onResume()V

    .line 120
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/av/RevenueCardCanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-static {v0}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->c:Lcom/twitter/android/widget/ExpandableViewHost;

    invoke-static {v0}, Lcom/twitter/util/d;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->onStart()V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->q:Lcom/twitter/util/n;

    invoke-virtual {v0, p0}, Lcom/twitter/util/n;->a(Lcom/twitter/util/n$a;)V

    .line 131
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 135
    invoke-super {p0}, Lcom/twitter/android/av/AVCardCanvasActivity;->onStop()V

    .line 136
    iget-object v0, p0, Lcom/twitter/android/av/RevenueCardCanvasActivity;->q:Lcom/twitter/util/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/util/n;->a(Lcom/twitter/util/n$a;)V

    .line 137
    return-void
.end method

.method protected abstract q()Z
.end method

.method protected abstract r()Ljava/lang/String;
.end method

.method protected abstract s()I
.end method
