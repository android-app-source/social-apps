.class public Lcom/twitter/android/av/v;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final c:Lcom/twitter/library/client/Session;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/v;->c:Lcom/twitter/library/client/Session;

    .line 28
    iput-object p2, p0, Lcom/twitter/android/av/v;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 29
    iput-object p1, p0, Lcom/twitter/android/av/v;->d:Landroid/content/Context;

    .line 30
    iput-object p3, p0, Lcom/twitter/android/av/v;->a:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/av/v;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/av/v;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 41
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/av/v;->a:Ljava/lang/String;

    .line 40
    invoke-static {v3, v4, v5, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 39
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 42
    iget-object v1, p0, Lcom/twitter/android/av/v;->d:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 43
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 44
    return-void
.end method
