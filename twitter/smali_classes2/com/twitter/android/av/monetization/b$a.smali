.class public final Lcom/twitter/android/av/monetization/b$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/monetization/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lqb;

.field private b:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/av/monetization/b$1;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/twitter/android/av/monetization/b$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/av/monetization/b$a;)Lamu;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b$a;->b:Lamu;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/av/monetization/b$a;)Lqb;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b$a;->a:Lqb;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lcom/twitter/android/av/monetization/b$a;
    .locals 1

    .prologue
    .line 154
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lcom/twitter/android/av/monetization/b$a;->b:Lamu;

    .line 155
    return-object p0
.end method

.method public a(Lqb;)Lcom/twitter/android/av/monetization/b$a;
    .locals 1

    .prologue
    .line 138
    .line 139
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqb;

    iput-object v0, p0, Lcom/twitter/android/av/monetization/b$a;->a:Lqb;

    .line 140
    return-object p0
.end method

.method public a()Lcom/twitter/android/av/monetization/e;
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b$a;->a:Lqb;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lqb;

    .line 125
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b$a;->b:Lamu;

    if-nez v0, :cond_1

    .line 129
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 130
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :cond_1
    new-instance v0, Lcom/twitter/android/av/monetization/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/av/monetization/b;-><init>(Lcom/twitter/android/av/monetization/b$a;Lcom/twitter/android/av/monetization/b$1;)V

    return-object v0
.end method
