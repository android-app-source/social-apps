.class public Lcom/twitter/android/av/monetization/g;
.super Lcom/twitter/android/client/o;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/client/o",
        "<",
        "Lcom/twitter/model/av/MonetizationCategory;",
        "Lcom/twitter/android/av/monetization/MonetizationCategorySelectorListItemView;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lrx/j;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;Lrx/c;Lcom/twitter/android/av/monetization/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lcom/twitter/android/client/o$a;",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/MonetizationCategory;",
            ">;>;",
            "Lcom/twitter/android/av/monetization/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/o;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;)V

    .line 52
    invoke-virtual {p0, p4}, Lcom/twitter/android/av/monetization/g;->a(Lcom/twitter/android/client/p;)V

    .line 54
    new-instance v0, Lcom/twitter/android/av/monetization/g$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/monetization/g$1;-><init>(Lcom/twitter/android/av/monetization/g;)V

    invoke-virtual {p3, v0}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/monetization/g;->a:Lrx/j;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;Lrx/c;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lcom/twitter/android/client/o$a;",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/MonetizationCategory;",
            ">;>;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/android/av/monetization/f;

    invoke-direct {v0, p1, p4}, Lcom/twitter/android/av/monetization/f;-><init>(Landroid/view/LayoutInflater;Ljava/util/Set;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/av/monetization/g;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/client/o$a;Lrx/c;Lcom/twitter/android/av/monetization/f;)V

    .line 40
    return-void
.end method


# virtual methods
.method public am_()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/twitter/android/client/o;->am_()V

    .line 70
    iget-object v0, p0, Lcom/twitter/android/av/monetization/g;->a:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 71
    return-void
.end method
