.class public Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/monetization/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$b;,
        Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$a;,
        Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private d(Lcmr;)V
    .locals 2

    .prologue
    .line 106
    if-nez p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    const v0, 0x7f1308ae

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    .line 111
    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->i()Z

    move-result v1

    invoke-interface {v0, v1}, Lcmm;->e(Z)Lcmm;

    goto :goto_0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/monetization/c;

    .line 120
    invoke-interface {v0}, Lcom/twitter/android/av/monetization/c;->d()Lcom/twitter/model/av/h$a;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/twitter/model/av/h$a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/av/h$a;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
    .locals 6

    .prologue
    .line 65
    new-instance v3, Lcom/twitter/android/av/monetization/MonetizationCategorySelectorActivity$c;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/av/monetization/MonetizationCategorySelectorActivity$c;-><init>(Landroid/app/Activity;I)V

    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->X()Lann;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/twitter/android/av/monetization/c;

    .line 69
    new-instance v0, Lcom/twitter/android/av/monetization/d;

    invoke-virtual {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 70
    invoke-interface {v2}, Lcom/twitter/android/av/monetization/c;->d()Lcom/twitter/model/av/h$a;

    move-result-object v4

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/av/monetization/d;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/twitter/android/av/monetization/MonetizationCategorySelectorActivity$c;Lcom/twitter/model/av/h$a;Lcom/twitter/android/av/monetization/d$a;)V

    .line 69
    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 94
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 95
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 96
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 97
    return-object p2
.end method

.method public a(Lcmm;)Z
    .locals 5

    .prologue
    .line 46
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 48
    const v1, 0x7f1308ae

    if-ne v0, v1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/monetization/c;

    .line 51
    invoke-interface {v0}, Lcom/twitter/android/av/monetization/c;->d()Lcom/twitter/model/av/h$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/av/h$a;->c()Lcom/twitter/model/av/h;

    move-result-object v0

    .line 53
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "media_monetization_metadata"

    sget-object v4, Lcom/twitter/model/av/h;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v3, v0, v4}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 55
    invoke-virtual {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->finish()V

    .line 56
    const/4 v0, 0x1

    .line 58
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f140019

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 39
    invoke-direct {p0, p1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->d(Lcmr;)V

    .line 41
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->c()Lcmr;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->d(Lcmr;)V

    .line 103
    return-void
.end method

.method protected d(Lank;)Lcom/twitter/android/av/monetization/c;
    .locals 3

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "media_monetization_metadata"

    sget-object v2, Lcom/twitter/model/av/h;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/h;

    .line 80
    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/model/av/h$a;

    invoke-direct {v0}, Lcom/twitter/model/av/h$a;-><init>()V

    .line 84
    :goto_0
    invoke-static {}, Lcom/twitter/android/av/monetization/a;->a()Lcom/twitter/android/av/monetization/a$a;

    move-result-object v1

    .line 85
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/av/monetization/a$a;->a(Lamu;)Lcom/twitter/android/av/monetization/a$a;

    move-result-object v1

    new-instance v2, Lpz;

    invoke-direct {v2, v0}, Lpz;-><init>(Lcom/twitter/model/av/h$a;)V

    .line 86
    invoke-virtual {v1, v2}, Lcom/twitter/android/av/monetization/a$a;->a(Lpz;)Lcom/twitter/android/av/monetization/a$a;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/twitter/android/av/monetization/a$a;->a()Lcom/twitter/android/av/monetization/c;

    move-result-object v0

    .line 84
    return-object v0

    .line 80
    :cond_0
    new-instance v1, Lcom/twitter/model/av/h$a;

    invoke-direct {v1, v0}, Lcom/twitter/model/av/h$a;-><init>(Lcom/twitter/model/av/h;)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->d(Lank;)Lcom/twitter/android/av/monetization/c;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->d(Lank;)Lcom/twitter/android/av/monetization/c;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity;->d(Lank;)Lcom/twitter/android/av/monetization/c;

    move-result-object v0

    return-object v0
.end method
