.class public final Lcom/twitter/android/av/monetization/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/monetization/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/monetization/a$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/av/h$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/twitter/android/av/monetization/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/av/monetization/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/av/monetization/a$a;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-boolean v0, Lcom/twitter/android/av/monetization/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/av/monetization/a;->a(Lcom/twitter/android/av/monetization/a$a;)V

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/av/monetization/a$a;Lcom/twitter/android/av/monetization/a$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/android/av/monetization/a;-><init>(Lcom/twitter/android/av/monetization/a$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/android/av/monetization/a$a;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/android/av/monetization/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/av/monetization/a$a;-><init>(Lcom/twitter/android/av/monetization/a$1;)V

    return-object v0
.end method

.method private a(Lcom/twitter/android/av/monetization/a$a;)V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/android/av/monetization/a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/av/monetization/a$1;-><init>(Lcom/twitter/android/av/monetization/a;Lcom/twitter/android/av/monetization/a$a;)V

    iput-object v0, p0, Lcom/twitter/android/av/monetization/a;->b:Lcta;

    .line 58
    iget-object v0, p0, Lcom/twitter/android/av/monetization/a;->b:Lcta;

    .line 60
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 59
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/monetization/a;->c:Lcta;

    .line 65
    invoke-static {p1}, Lcom/twitter/android/av/monetization/a$a;->b(Lcom/twitter/android/av/monetization/a$a;)Lpz;

    move-result-object v0

    invoke-static {v0}, Lqa;->a(Lpz;)Ldagger/internal/c;

    move-result-object v0

    .line 63
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/monetization/a;->d:Lcta;

    .line 66
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/av/monetization/a;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/model/av/h$a;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/av/monetization/a;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/h$a;

    return-object v0
.end method
