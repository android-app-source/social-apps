.class public final Lcom/twitter/android/av/monetization/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/monetization/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/monetization/b$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lpy;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/MonetizationCategory;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/twitter/android/av/monetization/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/av/monetization/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/av/monetization/b$a;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    sget-boolean v0, Lcom/twitter/android/av/monetization/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/av/monetization/b;->a(Lcom/twitter/android/av/monetization/b$a;)V

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/av/monetization/b$a;Lcom/twitter/android/av/monetization/b$1;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/twitter/android/av/monetization/b;-><init>(Lcom/twitter/android/av/monetization/b$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/android/av/monetization/b$a;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/android/av/monetization/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/av/monetization/b$a;-><init>(Lcom/twitter/android/av/monetization/b$1;)V

    return-object v0
.end method

.method private a(Lcom/twitter/android/av/monetization/b$a;)V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/twitter/android/av/monetization/b$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/av/monetization/b$1;-><init>(Lcom/twitter/android/av/monetization/b;Lcom/twitter/android/av/monetization/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/av/monetization/b;->b:Lcta;

    .line 66
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b;->b:Lcta;

    .line 68
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 67
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/monetization/b;->c:Lcta;

    .line 73
    invoke-static {p1}, Lcom/twitter/android/av/monetization/b$a;->b(Lcom/twitter/android/av/monetization/b$a;)Lqb;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lqd;->a(Lqb;)Ldagger/internal/c;

    move-result-object v0

    .line 71
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/monetization/b;->d:Lcta;

    .line 75
    new-instance v0, Lcom/twitter/android/av/monetization/b$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/av/monetization/b$2;-><init>(Lcom/twitter/android/av/monetization/b;Lcom/twitter/android/av/monetization/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/av/monetization/b;->e:Lcta;

    .line 88
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b;->e:Lcta;

    .line 90
    invoke-static {v0}, Lqc;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 89
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/monetization/b;->f:Lcta;

    .line 92
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public e()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/MonetizationCategory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/av/monetization/b;->f:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    return-object v0
.end method
