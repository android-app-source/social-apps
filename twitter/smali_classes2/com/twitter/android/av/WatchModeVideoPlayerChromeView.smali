.class public Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;
.super Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;
    }
.end annotation


# instance fields
.field private s:Z

.field private t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

.field private u:Z

.field private v:Lcom/twitter/android/av/watchmode/view/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/library/av/control/f;

    invoke-direct {v0}, Lcom/twitter/library/av/control/f;-><init>()V

    new-instance v1, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;

    invoke-direct {v1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->u:Z

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/control/f;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/twitter/library/av/control/d;Lcom/twitter/android/av/FullscreenVideoPlayerChromeView$a;)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->u:Z

    .line 40
    return-void
.end method


# virtual methods
.method protected B()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method protected C()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->u:Z

    return v0
.end method

.method F()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method protected G()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;
    .locals 4

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->a(Landroid/content/Context;)Lcom/twitter/library/av/control/VideoControlView;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Lcom/twitter/library/av/control/VideoControlView;->getControlBarView()Landroid/view/View;

    move-result-object v1

    .line 48
    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 53
    :cond_0
    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->s:Z

    if-nez v0, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t()V

    .line 158
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 3

    .prologue
    .line 109
    const/4 v0, 0x0

    .line 110
    iget-boolean v1, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->s:Z

    if-eqz v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->v:Lcom/twitter/android/av/watchmode/view/k;

    if-eqz v1, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->getAVPlayer()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v1

    .line 114
    if-eqz v1, :cond_0

    .line 115
    iget-object v2, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->v:Lcom/twitter/android/av/watchmode/view/k;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/av/watchmode/view/k;->a(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v1, :cond_0

    .line 119
    iget-boolean v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->q:Z

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->o()V

    .line 121
    const/4 v0, 0x1

    goto :goto_0

    .line 123
    :cond_2
    invoke-super {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->f()Z

    move-result v0

    goto :goto_0
.end method

.method public o()V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    invoke-virtual {p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;->a(IZ)V

    .line 96
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->o()V

    .line 97
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 139
    invoke-super {p0, p1}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 140
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v0, v1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;->a(I)V

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;->a(IZ)V

    goto :goto_0
.end method

.method protected p()V
    .locals 2

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->s:Z

    if-nez v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    invoke-virtual {p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v0, v1}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;->a(I)V

    .line 87
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/av/FullscreenVideoPlayerChromeView;->p()V

    .line 89
    :cond_1
    return-void
.end method

.method protected r()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public setCollapsed(Z)V
    .locals 0

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->s:Z

    .line 135
    return-void
.end method

.method public setOnChromeVisibilityChangeListener(Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->t:Lcom/twitter/android/av/WatchModeVideoPlayerChromeView$a;

    .line 173
    return-void
.end method

.method public setScrollClickListener(Lcom/twitter/android/av/watchmode/view/k;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->v:Lcom/twitter/android/av/watchmode/view/k;

    .line 181
    return-void
.end method

.method public setShouldShowFullscreenButton(Z)V
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->u:Z

    .line 78
    invoke-virtual {p0}, Lcom/twitter/android/av/WatchModeVideoPlayerChromeView;->m()V

    .line 79
    return-void
.end method
