.class public final Lcom/twitter/android/av/video/f$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/av/video/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/android/av/video/f;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/library/av/playback/AVDataSource;

.field public b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field public c:Lbyf;

.field public d:Lcom/twitter/library/av/VideoPlayerView$Mode;

.field public e:Landroid/view/View$OnClickListener;

.field public f:Lcom/twitter/library/av/model/b;

.field public g:Lcom/twitter/library/av/control/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/android/av/video/f$a;->a:Lcom/twitter/library/av/playback/AVDataSource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/f$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/f$a;->c:Lbyf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/f$a;->d:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/f$a;->f:Lcom/twitter/library/av/model/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/f$a;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/twitter/android/av/video/f$a;->e:Landroid/view/View$OnClickListener;

    .line 110
    return-object p0
.end method

.method public a(Lbyf;)Lcom/twitter/android/av/video/f$a;
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/android/av/video/f$a;->c:Lbyf;

    .line 98
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/av/video/f$a;
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/twitter/android/av/video/f$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 92
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/VideoPlayerView$Mode;)Lcom/twitter/android/av/video/f$a;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/android/av/video/f$a;->d:Lcom/twitter/library/av/VideoPlayerView$Mode;

    .line 104
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/control/e;)Lcom/twitter/android/av/video/f$a;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/twitter/android/av/video/f$a;->g:Lcom/twitter/library/av/control/e;

    .line 122
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/model/b;)Lcom/twitter/android/av/video/f$a;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/twitter/android/av/video/f$a;->f:Lcom/twitter/library/av/model/b;

    .line 116
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/android/av/video/f$a;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/android/av/video/f$a;->a:Lcom/twitter/library/av/playback/AVDataSource;

    .line 86
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/av/video/f$a;->d()Lcom/twitter/android/av/video/f;

    move-result-object v0

    return-object v0
.end method

.method protected d()Lcom/twitter/android/av/video/f;
    .locals 8

    .prologue
    .line 134
    new-instance v0, Lcom/twitter/android/av/video/f;

    iget-object v1, p0, Lcom/twitter/android/av/video/f$a;->a:Lcom/twitter/library/av/playback/AVDataSource;

    iget-object v2, p0, Lcom/twitter/android/av/video/f$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p0, Lcom/twitter/android/av/video/f$a;->c:Lbyf;

    iget-object v4, p0, Lcom/twitter/android/av/video/f$a;->d:Lcom/twitter/library/av/VideoPlayerView$Mode;

    iget-object v5, p0, Lcom/twitter/android/av/video/f$a;->e:Landroid/view/View$OnClickListener;

    iget-object v6, p0, Lcom/twitter/android/av/video/f$a;->f:Lcom/twitter/library/av/model/b;

    iget-object v7, p0, Lcom/twitter/android/av/video/f$a;->g:Lcom/twitter/library/av/control/e;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/av/video/f;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;Landroid/view/View$OnClickListener;Lcom/twitter/library/av/model/b;Lcom/twitter/library/av/control/e;)V

    return-object v0
.end method
