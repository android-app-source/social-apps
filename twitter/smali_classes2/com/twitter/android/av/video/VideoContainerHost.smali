.class public Lcom/twitter/android/av/video/VideoContainerHost;
.super Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/b;


# instance fields
.field a:Lcom/twitter/android/av/video/e;

.field b:Lcom/twitter/android/av/video/f;

.field private d:Lcom/twitter/android/av/video/e$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v0}, Lcom/twitter/android/av/video/e$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->d:Lcom/twitter/android/av/video/e$b;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    new-instance v0, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v0}, Lcom/twitter/android/av/video/e$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->d:Lcom/twitter/android/av/video/e$b;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v0}, Lcom/twitter/android/av/video/e$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->d:Lcom/twitter/android/av/video/e$b;

    .line 53
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->c()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    .line 106
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->removeAllViews()V

    .line 107
    return-void
.end method

.method public a(Lcom/twitter/android/av/video/f;)Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->removeAllViews()V

    .line 149
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->getActivityContext()Landroid/app/Activity;

    move-result-object v1

    .line 150
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->d:Lcom/twitter/android/av/video/e$b;

    iget-object v2, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    iget-object v3, v2, Lcom/twitter/android/av/video/f;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v2, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    iget-object v4, v2, Lcom/twitter/android/av/video/f;->a:Lcom/twitter/library/av/playback/AVDataSource;

    iget-object v2, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    iget-object v5, v2, Lcom/twitter/android/av/video/f;->e:Landroid/view/View$OnClickListener;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/av/video/e$b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    .line 153
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    iget-object v0, v0, Lcom/twitter/android/av/video/f;->f:Lcom/twitter/library/av/model/b;

    invoke-virtual {v0}, Lcom/twitter/library/av/model/b;->a()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/video/VideoContainerHost;->setAspectRatio(F)V

    .line 154
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    iget-object v1, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    iget-object v1, v1, Lcom/twitter/android/av/video/f;->g:Lcom/twitter/library/av/control/e;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/e;->a(Lcom/twitter/library/av/control/e;)V

    .line 155
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    iget-object v1, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    iget-object v1, v1, Lcom/twitter/android/av/video/f;->c:Lbyf;

    iget-object v2, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    iget-object v2, v2, Lcom/twitter/android/av/video/f;->d:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/video/e;->a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 156
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 158
    :cond_0
    return-void
.end method

.method c()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/e;->a(Lcom/twitter/library/av/control/e;)V

    .line 167
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 168
    iput-object v1, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    .line 170
    :cond_0
    return-void
.end method

.method public getAVPlayerAttachment()Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 74
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    goto :goto_0
.end method

.method getActivityContext()Landroid/app/Activity;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 176
    :goto_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_1

    .line 177
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 178
    check-cast v0, Landroid/app/Activity;

    .line 182
    :goto_1
    return-object v0

    .line 180
    :cond_0
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 182
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getAutoPlayableItem()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    goto :goto_0
.end method

.method public getCurrentAVMedia()Lcom/twitter/model/av/AVMedia;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    if-nez v0, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x0

    .line 82
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    goto :goto_0
.end method

.method public getEventDispatcher()Lbix;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    if-nez v0, :cond_1

    .line 64
    :cond_0
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->onAttachedToWindow()V

    .line 131
    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->a:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->b()V

    .line 134
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 138
    invoke-super {p0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->onDetachedFromWindow()V

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->c()V

    .line 140
    return-void
.end method

.method public setVideoContainerConfig(Lcom/twitter/android/av/video/f;)V
    .locals 0

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->a()V

    .line 116
    iput-object p1, p0, Lcom/twitter/android/av/video/VideoContainerHost;->b:Lcom/twitter/android/av/video/f;

    .line 117
    invoke-virtual {p0}, Lcom/twitter/android/av/video/VideoContainerHost;->b()V

    .line 118
    return-void
.end method

.method public setVideoContainerFactory(Lcom/twitter/android/av/video/e$b;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 96
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 97
    iput-object p1, p0, Lcom/twitter/android/av/video/VideoContainerHost;->d:Lcom/twitter/android/av/video/e$b;

    .line 98
    return-void
.end method
