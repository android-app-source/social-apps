.class public abstract Lcom/twitter/android/av/video/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/widget/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/av/video/e$a;,
        Lcom/twitter/android/av/video/e$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ActivityWith",
        "LifecycleCallbacks:Landroid/app/Activity;",
        ":",
        "Lcom/twitter/app/common/util/j;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/twitter/library/widget/a;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TActivityWith",
            "LifecycleCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/View$OnClickListener;

.field protected e:Lcom/twitter/library/av/control/e;

.field protected final f:Landroid/view/ViewGroup;

.field protected final g:Lcom/twitter/library/av/playback/AVDataSource;

.field protected final h:Lcom/twitter/android/av/ad;

.field protected final i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/android/av/ad;Landroid/view/View$OnClickListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TActivityWith",
            "LifecycleCallbacks;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lcom/twitter/android/av/ad;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/av/video/e;->a:Ljava/lang/ref/WeakReference;

    .line 81
    iput-object p2, p0, Lcom/twitter/android/av/video/e;->f:Landroid/view/ViewGroup;

    .line 82
    iput-object p5, p0, Lcom/twitter/android/av/video/e;->h:Lcom/twitter/android/av/ad;

    .line 83
    iput-object p3, p0, Lcom/twitter/android/av/video/e;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 84
    iput-object p4, p0, Lcom/twitter/android/av/video/e;->g:Lcom/twitter/library/av/playback/AVDataSource;

    .line 85
    iput-object p6, p0, Lcom/twitter/android/av/video/e;->b:Landroid/view/View$OnClickListener;

    .line 86
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V
.end method

.method public a(Lcom/twitter/library/av/control/e;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 119
    iput-object p1, p0, Lcom/twitter/android/av/video/e;->e:Lcom/twitter/library/av/control/e;

    .line 120
    return-void
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public au_()V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public abstract e()V
.end method

.method public h()V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/android/av/video/e;->f:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected k()V
    .locals 3

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/twitter/android/av/video/e;->n()Landroid/app/Activity;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 173
    iget-object v1, p0, Lcom/twitter/android/av/video/e;->h:Lcom/twitter/android/av/ad;

    iget-object v2, p0, Lcom/twitter/android/av/video/e;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/android/av/ad;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/ab;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/av/video/e;->g:Lcom/twitter/library/av/playback/AVDataSource;

    .line 174
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/ab;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/ab;

    move-result-object v1

    const/4 v2, 0x1

    .line 175
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/ab;->b(Z)Lcom/twitter/library/av/ab;

    move-result-object v1

    .line 177
    invoke-static {v0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v2

    invoke-virtual {v2}, Lbaa;->k()Z

    move-result v2

    .line 176
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/ab;->e(Z)Lcom/twitter/library/av/ab;

    move-result-object v1

    .line 178
    invoke-virtual {v1, v0}, Lcom/twitter/library/av/ab;->a(Ljava/lang/Object;)V

    .line 180
    :cond_0
    return-void
.end method

.method public l()Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public m()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/av/video/e;->b:Landroid/view/View$OnClickListener;

    invoke-static {v0, p0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method protected n()Landroid/app/Activity;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TActivityWith",
            "LifecycleCallbacks;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/android/av/video/e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/twitter/android/av/video/e;->k()V

    .line 165
    return-void
.end method
