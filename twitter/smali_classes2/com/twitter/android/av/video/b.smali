.class public Lcom/twitter/android/av/video/b;
.super Lcom/twitter/android/av/video/e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ActivityWith",
        "LifecycleCallbacks:Landroid/app/Activity;",
        ":",
        "Lcom/twitter/app/common/util/j;",
        ">",
        "Lcom/twitter/android/av/video/e",
        "<TActivityWith",
        "LifecycleCallbacks;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/library/av/VideoPlayerView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field b:Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field c:Lcom/twitter/library/av/playback/u;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field d:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final k:Z

.field private final l:Lcom/twitter/android/av/ah;

.field private final m:Lcom/twitter/library/av/playback/q;

.field private final n:Lcom/twitter/app/common/util/b$a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TActivityWith",
            "LifecycleCallbacks;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    new-instance v3, Lcom/twitter/android/av/ad;

    invoke-direct {v3}, Lcom/twitter/android/av/ad;-><init>()V

    new-instance v4, Lcom/twitter/android/av/ah;

    invoke-direct {v4}, Lcom/twitter/android/av/ah;-><init>()V

    .line 62
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/av/u;

    invoke-direct {v6}, Lcom/twitter/android/av/u;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    .line 61
    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/av/video/b;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/av/ad;Lcom/twitter/android/av/ah;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/u;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/av/ad;Lcom/twitter/android/av/ah;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/av/u;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TActivityWith",
            "LifecycleCallbacks;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/av/ad;",
            "Lcom/twitter/android/av/ah;",
            "Lcom/twitter/library/av/playback/q;",
            "Lcom/twitter/android/av/u;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move-object v4, p8

    move-object v5, p3

    move-object/from16 v6, p9

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/av/video/e;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/android/av/ad;Landroid/view/View$OnClickListener;)V

    .line 45
    new-instance v0, Lcom/twitter/android/av/video/b$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/video/b$1;-><init>(Lcom/twitter/android/av/video/b;)V

    iput-object v0, p0, Lcom/twitter/android/av/video/b;->n:Lcom/twitter/app/common/util/b$a;

    .line 88
    iput-object p4, p0, Lcom/twitter/android/av/video/b;->l:Lcom/twitter/android/av/ah;

    .line 89
    iput-object p5, p0, Lcom/twitter/android/av/video/b;->m:Lcom/twitter/library/av/playback/q;

    .line 90
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->g:Lcom/twitter/library/av/playback/AVDataSource;

    .line 91
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->k()Lcom/twitter/library/av/playback/ax;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/ax;->a()Lbis;

    move-result-object v0

    invoke-interface {v0}, Lbis;->b()J

    move-result-wide v0

    .line 90
    invoke-virtual {p6, v0, v1}, Lcom/twitter/android/av/u;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/av/video/b;->k:Z

    .line 92
    invoke-virtual {p6}, Lcom/twitter/android/av/u;->a()V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->f:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/twitter/android/av/video/b;->m()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->e:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->e:Lcom/twitter/library/av/control/e;

    iget-object v1, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-interface {v0, v1}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    iget-object v1, p0, Lcom/twitter/android/av/video/b;->e:Lcom/twitter/library/av/control/e;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoPlayerView;->setExternalChromeView(Lcom/twitter/library/av/control/e;)V

    .line 144
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/twitter/android/av/video/b;->j()V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->m:Lcom/twitter/library/av/playback/q;

    iget-object v1, p0, Lcom/twitter/android/av/video/b;->c:Lcom/twitter/library/av/playback/u;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->b(Lcom/twitter/library/av/playback/u;)V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/av/video/b;->n()Landroid/app/Activity;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_1

    .line 156
    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/android/av/video/b;->n:Lcom/twitter/app/common/util/b$a;

    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 158
    :cond_1
    return-void
.end method

.method public a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/av/video/b;->n()Landroid/app/Activity;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_1

    move-object v0, v1

    .line 100
    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v2, p0, Lcom/twitter/android/av/video/b;->n:Lcom/twitter/app/common/util/b$a;

    invoke-interface {v0, v2}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 102
    new-instance v0, Lcom/twitter/library/av/playback/v;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/v;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/av/video/b;->g:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/video/b;->c:Lcom/twitter/library/av/playback/u;

    .line 103
    new-instance v0, Lcom/twitter/library/av/playback/r;

    iget-object v2, p0, Lcom/twitter/android/av/video/b;->m:Lcom/twitter/library/av/playback/q;

    invoke-direct {v0, v2}, Lcom/twitter/library/av/playback/r;-><init>(Lcom/twitter/library/av/playback/q;)V

    iget-object v2, p0, Lcom/twitter/android/av/video/b;->c:Lcom/twitter/library/av/playback/u;

    .line 104
    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 105
    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/r;->a(Lbyf;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/av/video/b;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 106
    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 107
    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Landroid/content/Context;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v3}, Lcom/twitter/library/av/playback/r;->b(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 109
    invoke-static {v1}, Lcom/twitter/app/common/util/l;->a(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/r;->a(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/r;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 111
    iput-boolean v3, p0, Lcom/twitter/android/av/video/b;->d:Z

    .line 113
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->l:Lcom/twitter/android/av/ah;

    iget-object v2, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, v1, v2, p2}, Lcom/twitter/android/av/ah;->a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)Lcom/twitter/library/av/VideoPlayerView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    .line 115
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->g:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/util/q;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$Mode;->g:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, p2, :cond_0

    .line 117
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    new-instance v1, Lcom/twitter/library/av/y;

    iget-object v2, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 118
    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/av/y;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Landroid/view/View;)V

    .line 117
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoPlayerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 119
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {p0}, Lcom/twitter/android/av/video/b;->m()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoPlayerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    :cond_0
    new-instance v0, Lcom/twitter/android/av/video/b$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/video/b$2;-><init>(Lcom/twitter/android/av/video/b;)V

    .line 128
    iget-object v1, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/VideoPlayerView;->setAVPlayerListener(Lcom/twitter/library/av/k;)V

    .line 129
    invoke-direct {p0}, Lcom/twitter/android/av/video/b;->o()V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->f:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/av/control/e;)V
    .locals 0

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/twitter/android/av/video/e;->a(Lcom/twitter/library/av/control/e;)V

    .line 186
    invoke-direct {p0}, Lcom/twitter/android/av/video/b;->o()V

    .line 187
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoPlayerView;->n()Lcom/twitter/library/av/playback/u;

    .line 205
    :cond_0
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoPlayerView;->o()Lcom/twitter/library/av/playback/u;

    .line 212
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Lcom/twitter/android/av/video/e;->b()V

    .line 163
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 166
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/twitter/android/av/video/b;->k:Z

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->j()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 173
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/av/video/e;->d()V

    .line 174
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoPlayerView;->m()V

    .line 181
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoPlayerView;->q()Lcom/twitter/library/av/playback/u;

    .line 219
    :cond_0
    return-void
.end method

.method j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 225
    iget-boolean v0, p0, Lcom/twitter/android/av/video/b;->d:Z

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->m:Lcom/twitter/library/av/playback/q;

    iget-object v1, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 227
    iput-object v2, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 229
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/video/b;->d:Z

    .line 230
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->f:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 231
    iput-object v2, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    .line 232
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->a:Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoPlayerView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/av/video/e;->k()V

    goto :goto_0
.end method

.method public l()Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/av/video/b;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    return-object v0
.end method
