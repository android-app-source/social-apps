.class public Lcom/twitter/android/av/ViewCountBadgeView;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# instance fields
.field a:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 24
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/av/ViewCountBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/av/ViewCountBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 38
    const v0, 0x7f1301a3

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/ViewCountBadgeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/av/ViewCountBadgeView;->a:Landroid/widget/TextView;

    .line 39
    return-void
.end method

.method public setAVDataSource(Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    .line 49
    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 72
    :goto_0
    :pswitch_0
    return-void

    .line 54
    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/twitter/android/av/ViewCountBadgeView;->setVisibility(I)V

    goto :goto_0

    .line 59
    :pswitch_2
    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->p()J

    move-result-wide v0

    .line 60
    invoke-static {v0, v1}, Lcom/twitter/android/av/ai;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/twitter/android/av/ViewCountBadgeView;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/av/ViewCountBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 62
    invoke-virtual {p0}, Lcom/twitter/android/av/ViewCountBadgeView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 61
    invoke-static {v3, v4, v0, v1}, Lcom/twitter/android/av/ai;->a(Landroid/content/res/Resources;Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {p0, v3}, Lcom/twitter/android/av/ViewCountBadgeView;->setVisibility(I)V

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
