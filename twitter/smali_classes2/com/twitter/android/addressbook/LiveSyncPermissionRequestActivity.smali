.class public Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;
.super Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;
.source "Twttr"


# instance fields
.field private f:Lcom/twitter/android/people/ai;

.field private g:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 154
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_prelim_title"

    const v2, 0x7f0a004d

    .line 155
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_prelim_msg"

    const v2, 0x7f0a004b

    .line 156
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_prelim_pos_text"

    const v2, 0x7f0a094a

    .line 157
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_prelim_neg_text"

    const v2, 0x7f0a05eb

    .line 158
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_permissions"

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "android.permission.READ_CONTACTS"

    aput-object v4, v2, v3

    .line 159
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_event_prefix"

    .line 160
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_retarget_msg_fmt"

    const v2, 0x7f0a064c

    .line 162
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 161
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_retarget_title"

    const v2, 0x7f0a064d

    .line 163
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_always_prelim"

    .line 164
    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_retarget_dialog_theme"

    const v2, 0x7f0d0196

    .line 165
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 154
    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->f:Lcom/twitter/android/people/ai;

    const-string/jumbo v1, "contacts_sync_prompt"

    const-string/jumbo v2, ""

    const-string/jumbo v3, "impression"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 49
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->b:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    .line 50
    invoke-virtual {p0}, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 51
    new-instance v2, Lcom/twitter/android/dialog/a$a;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Lcom/twitter/android/dialog/a$a;-><init>(I)V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->b:Ljava/lang/String;

    .line 54
    invoke-virtual {v2, v0}, Lcom/twitter/android/dialog/a$a;->a(Ljava/lang/String;)Lcom/twitter/android/dialog/a$a;

    move-result-object v0

    const-string/jumbo v3, "extra_prelim_title"

    .line 55
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/dialog/a$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v3, "extra_prelim_pos_text"

    .line 56
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/dialog/g$b;->c(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v3, "extra_prelim_neg_text"

    .line 57
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/dialog/g$b;->d(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    .line 59
    const-string/jumbo v0, "extra_prelim_msg"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v2, v0}, Lcom/twitter/android/dialog/a$a;->b(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    .line 63
    :cond_0
    const v0, 0x7f0d0192

    .line 64
    invoke-virtual {v2, v0}, Lcom/twitter/android/dialog/a$a;->i(I)Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 65
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 66
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$c;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 67
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 69
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 4

    .prologue
    .line 79
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 81
    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    .line 82
    iget-wide v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->g:J

    const/4 v2, 0x2

    invoke-static {p0, v0, v1, v2}, Lbmp;->a(Landroid/content/Context;JI)V

    .line 83
    const-string/jumbo v0, "accept"

    .line 90
    :goto_0
    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->f:Lcom/twitter/android/people/ai;

    const-string/jumbo v2, "contacts_sync_prompt"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 96
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/DialogInterface;II)V

    .line 97
    return-void

    .line 84
    :cond_1
    const/4 v0, -0x2

    if-ne p3, v0, :cond_2

    .line 85
    const-string/jumbo v0, "deny"

    goto :goto_0

    .line 87
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()Z
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->g:J

    invoke-static {p0, v0, v1}, Lbmp;->a(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method protected c()V
    .locals 4

    .prologue
    .line 102
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/android/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->f:Lcom/twitter/android/people/ai;

    const-string/jumbo v1, "contacts_prompt"

    const-string/jumbo v2, ""

    const-string/jumbo v3, "impression"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 106
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c()V

    .line 107
    return-void
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 112
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/android/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->f:Lcom/twitter/android/people/ai;

    const-string/jumbo v1, "contacts_denied_prompt"

    const-string/jumbo v2, ""

    const-string/jumbo v3, "impression"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 117
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->d()V

    .line 118
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 123
    invoke-super {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->e()V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->f:Lcom/twitter/android/people/ai;

    const-string/jumbo v1, "contacts_prompt"

    const-string/jumbo v2, ""

    const-string/jumbo v3, "accept"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 126
    return-void
.end method

.method protected f()V
    .locals 4

    .prologue
    .line 131
    invoke-super {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f()V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->f:Lcom/twitter/android/people/ai;

    const-string/jumbo v1, "contacts_prompt"

    const-string/jumbo v2, ""

    const-string/jumbo v3, "deny"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 134
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 148
    invoke-super {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->finish()V

    .line 149
    invoke-virtual {p0, v0, v0}, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->overridePendingTransition(II)V

    .line 150
    return-void
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->setResult(I)V

    .line 143
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->finish()V

    .line 144
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->setResult(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->g:J

    .line 40
    iget-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->b:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->g:J

    invoke-static {v0, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;J)Lcom/twitter/android/people/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/addressbook/LiveSyncPermissionRequestActivity;->f:Lcom/twitter/android/people/ai;

    .line 41
    return-void
.end method
