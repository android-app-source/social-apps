.class public Lcom/twitter/android/bs$a;
.super Lcom/twitter/app/common/list/i$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/bs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/i$a",
        "<",
        "Lcom/twitter/android/bs$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/app/common/list/i$a;-><init>()V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Landroid/os/Bundle;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/bs;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Lcom/twitter/app/common/list/i;)V

    .line 57
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/android/bs$a;
    .locals 2

    .prologue
    .line 61
    new-instance v1, Lcom/twitter/android/bs$a;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Lcom/twitter/android/bs$a;-><init>(Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lcom/twitter/android/bs$a;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/bs$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "search_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    return-object p0
.end method

.method public a(J)Lcom/twitter/android/bs$a;
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/bs$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "search_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 79
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/bs$a;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/bs$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "recent"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 73
    return-object p0
.end method

.method public a()Lcom/twitter/android/bs;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lcom/twitter/android/bs;

    iget-object v1, p0, Lcom/twitter/android/bs$a;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/android/bs;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public b(Z)Lcom/twitter/android/bs$a;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/bs$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "follows"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 85
    return-object p0
.end method

.method public synthetic b()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/bs$a;->a()Lcom/twitter/android/bs;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)Lcom/twitter/android/bs$a;
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/bs$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "near"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 91
    return-object p0
.end method

.method public synthetic c()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/bs$a;->a()Lcom/twitter/android/bs;

    move-result-object v0

    return-object v0
.end method
