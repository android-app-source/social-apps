.class Lcom/twitter/android/ac$2;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ac;->c(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/InlineDismissView;

.field final synthetic b:Lcom/twitter/android/timeline/bk;

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/android/ac;


# direct methods
.method constructor <init>(Lcom/twitter/android/ac;Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;J)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    iput-object p2, p0, Lcom/twitter/android/ac$2;->a:Lcom/twitter/library/widget/InlineDismissView;

    iput-object p3, p0, Lcom/twitter/android/ac$2;->b:Lcom/twitter/android/timeline/bk;

    iput-wide p4, p0, Lcom/twitter/android/ac$2;->c:J

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 275
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 277
    new-instance v0, Lcom/twitter/android/g;

    iget-object v1, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    iget-object v2, p0, Lcom/twitter/android/ac$2;->a:Lcom/twitter/library/widget/InlineDismissView;

    iget-object v3, p0, Lcom/twitter/android/ac$2;->b:Lcom/twitter/android/timeline/bk;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/g;-><init>(Lcom/twitter/android/ab;Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V

    .line 278
    invoke-virtual {v0, p1}, Lcom/twitter/android/g;->b(Landroid/database/Cursor;)V

    .line 280
    iget-object v0, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    iget-wide v2, p0, Lcom/twitter/android/ac$2;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/ac;->c(J)Lcom/twitter/model/timeline/k;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_0

    .line 282
    iget-object v1, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    iget-object v2, p0, Lcom/twitter/android/ac$2;->b:Lcom/twitter/android/timeline/bk;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/k;)V

    .line 283
    iget-object v1, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    iget-object v2, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    invoke-static {v2}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/ac;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/ac$2;->b:Lcom/twitter/android/timeline/bk;

    iget-object v0, v0, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    const-string/jumbo v4, "click"

    invoke-static {v1, v2, v3, v0, v4}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/ac;Landroid/content/Context;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    .line 291
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    iget-wide v2, p0, Lcom/twitter/android/ac$2;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/ac;->h(J)V

    .line 292
    return-void

    .line 286
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "ANDROID-23487 expected InlineDismissInfo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 288
    iget-object v0, p0, Lcom/twitter/android/ac$2;->d:Lcom/twitter/android/ac;

    iget-object v1, p0, Lcom/twitter/android/ac$2;->a:Lcom/twitter/library/widget/InlineDismissView;

    iget-object v2, p0, Lcom/twitter/android/ac$2;->b:Lcom/twitter/android/timeline/bk;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ac;->d(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 272
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ac$2;->a(Landroid/database/Cursor;)V

    return-void
.end method
