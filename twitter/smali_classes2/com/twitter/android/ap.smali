.class public Lcom/twitter/android/ap;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/support/v4/app/Fragment;

.field private final b:Lcom/twitter/android/timeline/be;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/android/timeline/be;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/android/ap;->a:Landroid/support/v4/app/Fragment;

    .line 32
    iput-object p2, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/timeline/be;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ap;)Lcom/twitter/android/timeline/be;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/timeline/be;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 37
    iget-object v0, p0, Lcom/twitter/android/ap;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 38
    if-eqz v1, :cond_0

    .line 39
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v4}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x7f0a0550

    aput v3, v2, v4

    .line 40
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->a([I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 41
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/ap$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/ap$1;-><init>(Lcom/twitter/android/ap;Landroid/view/View;)V

    .line 42
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 65
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 67
    :cond_0
    return-void
.end method
