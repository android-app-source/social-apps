.class public Lcom/twitter/android/ReportFlowWebViewActivity$a;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ReportFlowWebViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/android/ReportFlowWebViewActivity$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 284
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lcom/twitter/app/common/base/h;-><init>(Landroid/content/Intent;)V

    .line 288
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 1

    .prologue
    .line 298
    new-instance v0, Lcom/twitter/android/ReportFlowWebViewActivity$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 328
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "spammer_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 293
    const-class v0, Lcom/twitter/android/ReportFlowWebViewActivity;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "friendship"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 318
    return-object p0
.end method

.method public a(J)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "spammer_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 324
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 7

    .prologue
    .line 365
    if-eqz p1, :cond_0

    .line 366
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 367
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    .line 368
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c()Ljava/lang/String;

    move-result-object v2

    .line 369
    iget-object v3, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v4, "client_location"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v1, v5, v0

    const/4 v0, 0x2

    aput-object v2, v5, v0

    invoke-static {v5}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 4

    .prologue
    .line 302
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->s:J

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(J)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    .line 303
    iget v0, p1, Lcom/twitter/model/core/Tweet;->l:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(I)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    .line 304
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "status_id"

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "is_media"

    .line 305
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->m()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "promoted_content"

    .line 306
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v2

    invoke-static {v2}, Lcgi;->a(Lcgi;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "is_promoted"

    .line 307
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "handle_api_requests"

    const/4 v2, 0x1

    .line 308
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 309
    return-object p0

    .line 302
    :cond_0
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->b:J

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0, p1}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "is_media"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 344
    return-object p0
.end method

.method public b()J
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "moment_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(J)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 3

    .prologue
    .line 333
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "moment_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 334
    return-object p0
.end method

.method public b(Z)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "is_promoted"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 356
    return-object p0
.end method

.method public c()J
    .locals 4

    .prologue
    .line 360
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "status_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public c(J)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 3

    .prologue
    .line 349
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 350
    return-object p0
.end method

.method public c(Z)Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "handle_api_requests"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 410
    return-object p0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "client_location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const-string/jumbo v0, "reporttweet"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "reportprofile"

    goto :goto_0
.end method

.method public g()Z
    .locals 3

    .prologue
    .line 401
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "is_media"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 3

    .prologue
    .line 405
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "is_promoted"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 3

    .prologue
    .line 414
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "handle_api_requests"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public j()Lcgi;
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "promoted_content"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcgi;->a([B)Lcgi;

    move-result-object v0

    return-object v0
.end method
