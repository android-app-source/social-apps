.class public Lcom/twitter/android/timeline/bz;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/ca;",
        "Lcom/twitter/android/timeline/ca$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/ca$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bz;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ca$a;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/ca$a;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/timeline/ca$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/ca$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ca$a;)Lcom/twitter/android/timeline/ca$a;
    .locals 3

    .prologue
    .line 31
    sget v0, Lbue;->r:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/ca$a;->a(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    sget v1, Lbue;->u:I

    .line 32
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/ca$a;->c(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    sget v1, Lbue;->v:I

    .line 33
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/ca$a;->d(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v1

    sget v0, Lbue;->B:I

    .line 35
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 34
    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/ca$a;->a(Lcgi;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v1

    sget v0, Lbue;->C:I

    .line 38
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lcom/twitter/model/topic/b;->a:Lcom/twitter/util/serialization/l;

    .line 37
    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/ca$a;->a(Lcom/twitter/model/topic/e;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v1

    sget v0, Lbue;->j:I

    .line 40
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/ca$a;->a(Z)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    sget v1, Lbue;->t:I

    .line 41
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/ca$a;->e(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    sget v1, Lbue;->s:I

    .line 42
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/ca$a;->a(I)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    sget v1, Lbue;->D:I

    .line 43
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/ca$a;->f(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    sget v1, Lbue;->A:I

    .line 44
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/ca$a;->b(I)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    .line 31
    return-object v0

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 18
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 19
    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bz;->a(J)Lcom/twitter/android/timeline/ca$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bz;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
