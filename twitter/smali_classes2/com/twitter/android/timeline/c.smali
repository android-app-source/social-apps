.class public Lcom/twitter/android/timeline/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lang;
.implements Lcom/twitter/android/timeline/br;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lang",
        "<",
        "Lcom/twitter/android/timeline/NewTweetsBannerState;",
        ">;",
        "Lcom/twitter/android/timeline/br;"
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/twitter/android/widget/NewItemBannerView;

.field private final c:Lcom/twitter/android/timeline/br$a;

.field private final d:Lcom/twitter/android/widget/NewItemBannerView$b;

.field private final e:Landroid/view/View$OnClickListener;

.field private final f:Ljava/lang/Runnable;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/twitter/model/timeline/u;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/twitter/android/timeline/c;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_saved_state_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/timeline/c;->a:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/twitter/android/widget/NewItemBannerView;Lcom/twitter/android/timeline/br$a;Lcom/twitter/android/timeline/NewTweetsBannerState;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcom/twitter/android/timeline/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/timeline/c$a;-><init>(Lcom/twitter/android/timeline/c;Lcom/twitter/android/timeline/c$1;)V

    iput-object v0, p0, Lcom/twitter/android/timeline/c;->d:Lcom/twitter/android/widget/NewItemBannerView$b;

    .line 54
    new-instance v0, Lcom/twitter/android/timeline/c$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/timeline/c$1;-><init>(Lcom/twitter/android/timeline/c;)V

    iput-object v0, p0, Lcom/twitter/android/timeline/c;->e:Landroid/view/View$OnClickListener;

    .line 61
    new-instance v0, Lcom/twitter/android/timeline/c$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/timeline/c$2;-><init>(Lcom/twitter/android/timeline/c;)V

    iput-object v0, p0, Lcom/twitter/android/timeline/c;->f:Ljava/lang/Runnable;

    .line 78
    iput-object p1, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    .line 79
    iput-object p2, p0, Lcom/twitter/android/timeline/c;->c:Lcom/twitter/android/timeline/br$a;

    .line 81
    if-eqz p3, :cond_0

    .line 82
    iget-boolean v0, p3, Lcom/twitter/android/timeline/NewTweetsBannerState;->a:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->g:Z

    .line 83
    iget-object v0, p3, Lcom/twitter/android/timeline/NewTweetsBannerState;->b:Lcom/twitter/model/timeline/u;

    iput-object v0, p0, Lcom/twitter/android/timeline/c;->k:Lcom/twitter/model/timeline/u;

    .line 84
    iget-boolean v0, p3, Lcom/twitter/android/timeline/NewTweetsBannerState;->c:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->i:Z

    .line 85
    iget-boolean v0, p3, Lcom/twitter/android/timeline/NewTweetsBannerState;->d:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->j:Z

    .line 92
    :goto_0
    return-void

    .line 87
    :cond_0
    iput-boolean v2, p0, Lcom/twitter/android/timeline/c;->g:Z

    .line 88
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/timeline/c;->a(Landroid/content/res/Resources;)Lcom/twitter/model/timeline/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/c;->k:Lcom/twitter/model/timeline/u;

    .line 89
    iput-boolean v2, p0, Lcom/twitter/android/timeline/c;->i:Z

    .line 90
    iput-boolean v2, p0, Lcom/twitter/android/timeline/c;->j:Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/timeline/c;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/twitter/android/timeline/c;->v()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/c;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/twitter/android/timeline/c;->i:Z

    return p1
.end method

.method private a(Lcom/twitter/model/timeline/u;Z)Z
    .locals 4

    .prologue
    .line 224
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/model/timeline/u;->b:Lcom/twitter/model/timeline/AlertType;

    sget-object v1, Lcom/twitter/model/timeline/AlertType;->b:Lcom/twitter/model/timeline/AlertType;

    if-ne v0, v1, :cond_0

    .line 225
    iput-boolean p2, p0, Lcom/twitter/android/timeline/c;->g:Z

    .line 226
    iput-object p1, p0, Lcom/twitter/android/timeline/c;->k:Lcom/twitter/model/timeline/u;

    .line 227
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    iget-wide v2, p1, Lcom/twitter/model/timeline/u;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/widget/NewItemBannerView;->setMinDelaySinceLastDisplayed(J)V

    .line 228
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    iget-object v1, p1, Lcom/twitter/model/timeline/u;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setText(Ljava/lang/String;)V

    .line 229
    const/4 v0, 0x1

    .line 231
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/twitter/android/timeline/c;->i:Z

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setVisibility(I)V

    .line 120
    invoke-direct {p0}, Lcom/twitter/android/timeline/c;->v()V

    .line 122
    :cond_0
    return-void
.end method

.method private t()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->c:Lcom/twitter/android/timeline/br$a;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br$a;->aD_()V

    .line 236
    return-void
.end method

.method private u()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->c:Lcom/twitter/android/timeline/br$a;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br$a;->s()V

    .line 240
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 243
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    iget-object v1, p0, Lcom/twitter/android/timeline/c;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 244
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    iget-object v1, p0, Lcom/twitter/android/timeline/c;->f:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/twitter/android/timeline/c;->k:Lcom/twitter/model/timeline/u;

    iget-wide v2, v2, Lcom/twitter/model/timeline/u;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/widget/NewItemBannerView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 245
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/res/Resources;)Lcom/twitter/model/timeline/u;
    .locals 8

    .prologue
    .line 250
    new-instance v1, Lcom/twitter/model/timeline/u;

    sget-object v2, Lcom/twitter/model/timeline/AlertType;->b:Lcom/twitter/model/timeline/AlertType;

    const v0, 0x7f0a05c9

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x3a980

    const-wide/16 v6, 0xbb8

    invoke-direct/range {v1 .. v7}, Lcom/twitter/model/timeline/u;-><init>(Lcom/twitter/model/timeline/AlertType;Ljava/lang/String;JJ)V

    return-object v1
.end method

.method public a(IZLcom/twitter/model/timeline/u;)V
    .locals 2

    .prologue
    .line 159
    const/4 v0, 0x0

    .line 160
    if-lez p1, :cond_2

    .line 161
    invoke-virtual {p0}, Lcom/twitter/android/timeline/c;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    :goto_0
    const/4 v0, 0x1

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/timeline/c;->a(Lcom/twitter/model/timeline/u;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/twitter/android/timeline/c;->q()Z

    .line 171
    :cond_0
    return-void

    .line 163
    :cond_1
    if-eqz p2, :cond_2

    .line 164
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/timeline/c;->a(Landroid/content/res/Resources;)Lcom/twitter/model/timeline/u;

    move-result-object p3

    goto :goto_0

    :cond_2
    move-object p3, v0

    goto :goto_0
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Lcom/twitter/android/timeline/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 137
    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->g:Z

    .line 138
    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->j:Z

    .line 139
    return-void
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/twitter/android/timeline/c;->p()Lcom/twitter/android/timeline/NewTweetsBannerState;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    iget-object v1, p0, Lcom/twitter/android/timeline/c;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    iget-object v1, p0, Lcom/twitter/android/timeline/c;->d:Lcom/twitter/android/widget/NewItemBannerView$b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setDisplayListener(Lcom/twitter/android/widget/NewItemBannerView$b;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->k:Lcom/twitter/model/timeline/u;

    iget-boolean v1, p0, Lcom/twitter/android/timeline/c;->g:Z

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/timeline/c;->a(Lcom/twitter/model/timeline/u;Z)Z

    .line 99
    invoke-direct {p0}, Lcom/twitter/android/timeline/c;->d()V

    .line 100
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    iget-object v1, p0, Lcom/twitter/android/timeline/c;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 105
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/twitter/android/timeline/c;->d()V

    .line 110
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setVisibility(I)V

    .line 115
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/twitter/android/timeline/c;->n()Z

    .line 127
    invoke-direct {p0}, Lcom/twitter/android/timeline/c;->u()V

    .line 128
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->j:Z

    .line 133
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->j:Z

    .line 144
    return-void
.end method

.method public l()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public m()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method protected n()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->b()Z

    move-result v0

    return v0
.end method

.method o()Z
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public p()Lcom/twitter/android/timeline/NewTweetsBannerState;
    .locals 5

    .prologue
    .line 193
    new-instance v0, Lcom/twitter/android/timeline/NewTweetsBannerState;

    iget-boolean v1, p0, Lcom/twitter/android/timeline/c;->g:Z

    iget-object v2, p0, Lcom/twitter/android/timeline/c;->k:Lcom/twitter/model/timeline/u;

    iget-boolean v3, p0, Lcom/twitter/android/timeline/c;->i:Z

    iget-boolean v4, p0, Lcom/twitter/android/timeline/c;->j:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/timeline/NewTweetsBannerState;-><init>(ZLcom/twitter/model/timeline/u;ZZ)V

    return-object v0
.end method

.method q()Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 199
    iget-boolean v1, p0, Lcom/twitter/android/timeline/c;->h:Z

    if-eqz v1, :cond_1

    .line 200
    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->h:Z

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 205
    :cond_1
    iget-boolean v1, p0, Lcom/twitter/android/timeline/c;->j:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/android/timeline/c;->g:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/NewItemBannerView;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 206
    :cond_2
    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/twitter/android/timeline/c;->t()V

    goto :goto_0
.end method

.method protected r()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->c:Lcom/twitter/android/timeline/br$a;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br$a;->aE_()V

    .line 214
    return-void
.end method

.method s()V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/android/timeline/c;->b:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/timeline/c;->h:Z

    .line 221
    return-void

    .line 220
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
