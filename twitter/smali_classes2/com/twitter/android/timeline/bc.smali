.class public abstract Lcom/twitter/android/timeline/bc;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CAROUSE",
        "L_ITEM:Lcom/twitter/android/timeline/bk;",
        "CAROUSE",
        "L_ITEM_BUILDER:Lcom/twitter/android/timeline/bk$a",
        "<TCAROUSE",
        "L_ITEM;",
        "TCAROUSE",
        "L_ITEM_BUILDER;",
        ">;>",
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/bb;",
        "Lcom/twitter/android/timeline/bb$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/bo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/timeline/bo",
            "<TCAROUSE",
            "L_ITEM;",
            "TCAROUSE",
            "L_ITEM_BUILDER;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/twitter/android/timeline/bo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/timeline/bo",
            "<TCAROUSE",
            "L_ITEM;",
            "TCAROUSE",
            "L_ITEM_BUILDER;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/twitter/android/timeline/bc;->a:Lcom/twitter/android/timeline/bo;

    .line 22
    return-void
.end method


# virtual methods
.method protected final a(J)Lcom/twitter/android/timeline/bb$a;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/android/timeline/bb$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/bb$a;-><init>(J)V

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bb$a;)Lcom/twitter/android/timeline/bb$a;
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bc;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/bb$a;->a(Ljava/lang/String;)Lcom/twitter/android/timeline/bb$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/timeline/bc;->a:Lcom/twitter/android/timeline/bo;

    .line 47
    invoke-virtual {p0, p1, v1}, Lcom/twitter/android/timeline/bc;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bo;)Lcbi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/bb$a;->a(Lcbi;)Lcom/twitter/android/timeline/bb$a;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method public bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 12
    check-cast p2, Lcom/twitter/android/timeline/bb$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bc;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bb$a;)Lcom/twitter/android/timeline/bb$a;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Landroid/database/Cursor;)Z
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bc;->a(J)Lcom/twitter/android/timeline/bb$a;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 12
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bc;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
