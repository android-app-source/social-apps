.class public abstract Lcom/twitter/android/timeline/d;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/android/timeline/cd;",
        "B:",
        "Lcom/twitter/android/timeline/bk$a",
        "<TT;TB;>;>",
        "Lcom/twitter/android/timeline/bo",
        "<TT;TB;>;"
    }
.end annotation


# instance fields
.field private final a:Z


# direct methods
.method protected constructor <init>(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    .line 26
    iput-boolean p1, p0, Lcom/twitter/android/timeline/d;->a:Z

    .line 27
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/model/core/Tweet$a;Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/twitter/android/timeline/d;->a:Z

    if-eqz v0, :cond_2

    .line 39
    invoke-static {}, Lcom/twitter/android/util/af;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    sget v0, Lbue;->ax:I

    .line 42
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/TwitterSocialProof;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    .line 41
    invoke-virtual {p1, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/core/Tweet$a;

    .line 46
    :cond_0
    invoke-static {}, Lcom/twitter/android/util/af;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    sget v0, Lbue;->as:I

    .line 49
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 48
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    invoke-virtual {p1, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcgi;)Lcom/twitter/model/core/Tweet$a;

    .line 52
    :cond_1
    invoke-static {}, Lcom/twitter/android/util/af;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    sget v0, Lbue;->g:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 56
    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->f(I)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/model/core/Tweet$a;->c(Z)Lcom/twitter/model/core/Tweet$a;

    .line 59
    :cond_2
    return-object p1
.end method

.method protected a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lbtc;->a:Lbtc;

    invoke-virtual {v0, p1}, Lbtc;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    .line 32
    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/timeline/d;->a(Lcom/twitter/model/core/Tweet$a;Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet$a;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    return-object v0
.end method
