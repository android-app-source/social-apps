.class public Lcom/twitter/android/timeline/aj;
.super Lcom/twitter/android/timeline/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/d",
        "<",
        "Lcom/twitter/android/timeline/ak;",
        "Lcom/twitter/android/timeline/ak$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/timeline/d;-><init>(Z)V

    .line 19
    return-void
.end method

.method private static f(Landroid/database/Cursor;)Z
    .locals 4

    .prologue
    .line 43
    const/16 v0, 0x2a

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/ak$a;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/timeline/ak$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/ak$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ak$a;)Lcom/twitter/android/timeline/ak$a;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/aj;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 39
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/ak$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/ak$a;

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 15
    check-cast p2, Lcom/twitter/android/timeline/ak$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aj;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ak$a;)Lcom/twitter/android/timeline/ak$a;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aj;->a(J)Lcom/twitter/android/timeline/ak$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 24
    sget v1, Lbue;->e:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 25
    if-ne v1, v0, :cond_0

    invoke-static {p1}, Lcom/twitter/android/timeline/aj;->f(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 15
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/aj;->b(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
