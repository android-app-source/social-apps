.class public Lcom/twitter/android/timeline/co;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/co$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/co$a;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 19
    invoke-static {p1}, Lcom/twitter/android/timeline/co$a;->a(Lcom/twitter/android/timeline/co$a;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/co$a;Lcom/twitter/android/timeline/co$1;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/co;-><init>(Lcom/twitter/android/timeline/co$a;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method
