.class public Lcom/twitter/android/timeline/ao;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/ap;",
        "Lcom/twitter/android/timeline/ap$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/bw;


# direct methods
.method constructor <init>(Lcom/twitter/android/timeline/bw;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/twitter/android/timeline/ao;->a:Lcom/twitter/android/timeline/bw;

    .line 19
    return-void
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/ap$a;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/timeline/ap$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/ap$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ap$a;)Lcom/twitter/android/timeline/ap$a;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/timeline/ao;->a:Lcom/twitter/android/timeline/bw;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bw;->c(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 39
    sget v1, Lbue;->f:I

    .line 40
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    sget-object v2, Lcom/twitter/model/timeline/o;->a:Lcom/twitter/util/serialization/b;

    .line 39
    invoke-static {v1, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/o;

    .line 41
    sget-object v2, Lcom/twitter/model/timeline/o;->b:Lcom/twitter/model/timeline/o;

    invoke-static {v1, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/o;

    invoke-virtual {p2, v1}, Lcom/twitter/android/timeline/ap$a;->a(Lcom/twitter/model/timeline/o;)Lcom/twitter/android/timeline/ap$a;

    move-result-object v1

    .line 42
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/ap$a;->a(Lcom/twitter/android/timeline/cd;)Lcom/twitter/android/timeline/ap$a;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/ap$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ao;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ap$a;)Lcom/twitter/android/timeline/ap$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 24
    sget v0, Lbue;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 25
    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->k(I)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ao;->a(J)Lcom/twitter/android/timeline/ap$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ao;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
