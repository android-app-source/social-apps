.class public Lcom/twitter/android/timeline/bl;
.super Lcbh;
.source "Twttr"

# interfaces
.implements Lcbg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbh",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        ">;",
        "Lcbg;"
    }
.end annotation


# static fields
.field private static final a:Lcom/twitter/android/timeline/bl;


# instance fields
.field private final b:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    new-instance v0, Lcom/twitter/android/timeline/bl;

    invoke-direct {v0, v1, v1}, Lcom/twitter/android/timeline/bl;-><init>(Landroid/database/Cursor;Lcbp;)V

    sput-object v0, Lcom/twitter/android/timeline/bl;->a:Lcom/twitter/android/timeline/bl;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Lcbp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcbp",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcbh;-><init>(Landroid/database/Cursor;Lcbp;)V

    .line 47
    iput-object p1, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    .line 48
    return-void
.end method

.method private a(III)I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result p3

    .line 192
    :cond_0
    return p3
.end method

.method private a(IIJ)J
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p3

    .line 176
    :cond_0
    return-wide p3
.end method

.method private a(IILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 185
    :cond_0
    return-object p3
.end method

.method public static bd_()Lcom/twitter/android/timeline/bl;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/twitter/android/timeline/bl;->a:Lcom/twitter/android/timeline/bl;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    return-object v0
.end method

.method public b(I)J
    .locals 4

    .prologue
    .line 123
    const/16 v0, 0x17

    const-wide/16 v2, 0x0

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/twitter/android/timeline/bl;->a(IIJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)J
    .locals 4

    .prologue
    .line 127
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/twitter/android/timeline/bl;->a(IIJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public d(I)J
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    sget v0, Lbue;->b:I

    const-wide/16 v2, -0x1

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/twitter/android/timeline/bl;->a(IIJ)J

    move-result-wide v0

    .line 139
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bl;->b(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public d()Lcom/twitter/android/timeline/cf;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v3

    .line 79
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move v1, v2

    .line 67
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bl;->be_()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 68
    sget v0, Lbue;->e:I

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/android/timeline/bl;->a(III)I

    move-result v0

    .line 69
    const/16 v4, 0xe

    if-ne v0, v4, :cond_2

    .line 70
    invoke-virtual {p0, v1}, Lcom/twitter/android/timeline/bl;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 71
    instance-of v4, v0, Lcom/twitter/android/timeline/cf;

    if-eqz v4, :cond_2

    .line 72
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 73
    iget-object v4, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v4, v4, Lcom/twitter/model/timeline/ar;->c:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 67
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v3

    .line 79
    goto :goto_0
.end method

.method public e()Lcom/twitter/android/timeline/cf;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 103
    :cond_0
    :goto_0
    return-object v0

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bl;->be_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_3

    .line 92
    sget v0, Lbue;->e:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, v0, v3}, Lcom/twitter/android/timeline/bl;->a(III)I

    move-result v0

    .line 93
    const/16 v3, 0xe

    if-ne v0, v3, :cond_2

    .line 94
    invoke-virtual {p0, v2}, Lcom/twitter/android/timeline/bl;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 95
    instance-of v3, v0, Lcom/twitter/android/timeline/cf;

    if-eqz v3, :cond_2

    .line 96
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 97
    iget-object v3, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v3, v3, Lcom/twitter/model/timeline/ar;->c:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 91
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 103
    goto :goto_0
.end method

.method public e(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 144
    const/16 v2, 0x11

    invoke-direct {p0, p1, v2, v1}, Lcom/twitter/android/timeline/bl;->a(III)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public f(I)Z
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bl;->f_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    const/16 v1, 0x1c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f_(I)Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bl;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)I
    .locals 2

    .prologue
    .line 156
    const/16 v0, 0x13

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/timeline/bl;->a(III)I

    move-result v0

    return v0
.end method

.method public h(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 160
    const/16 v1, 0x10

    invoke-direct {p0, p1, v1, v0}, Lcom/twitter/android/timeline/bl;->a(III)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public i(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    sget v0, Lbue;->c:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/timeline/bl;->a(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public j(I)J
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bl;->be_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 198
    invoke-virtual {p0, v0}, Lcom/twitter/android/timeline/bl;->g(I)I

    move-result v1

    if-ne p1, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/timeline/bl;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    invoke-virtual {p0, v0}, Lcom/twitter/android/timeline/bl;->b(I)J

    move-result-wide v0

    .line 202
    :goto_1
    return-wide v0

    .line 197
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 202
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1
.end method
