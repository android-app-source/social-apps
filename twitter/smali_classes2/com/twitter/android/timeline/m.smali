.class public Lcom/twitter/android/timeline/m;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/n;",
        "Lcom/twitter/android/timeline/n$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/n$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/m;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/n$a;)Lcom/twitter/android/timeline/n$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/n$a;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/timeline/n$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/n$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/n$a;)Lcom/twitter/android/timeline/n$a;
    .locals 4

    .prologue
    .line 32
    new-instance v0, Lcom/twitter/model/timeline/n$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/n$a;-><init>()V

    sget v1, Lbue;->V:I

    .line 33
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->U:I

    .line 34
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->W:I

    .line 35
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->X:I

    .line 36
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->aj:I

    .line 37
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->ac:I

    .line 38
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->f(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->S:I

    .line 39
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->o(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->ad:I

    .line 40
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->g(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->ae:I

    .line 41
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->b(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->R:I

    .line 42
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->a(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->af:I

    .line 43
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/model/core/v;->a([B)Lcom/twitter/model/core/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->ag:I

    .line 44
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/model/core/v;->a([B)Lcom/twitter/model/core/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->T:I

    .line 45
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->h(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->ah:I

    .line 46
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/n$a;->a(J)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    sget v1, Lbue;->ai:I

    .line 47
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->c(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v1

    const-string/jumbo v0, "timeline_flags"

    .line 48
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    sget v0, Lbue;->g:I

    .line 50
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 49
    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->t(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 48
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/n$a;->b(Z)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/twitter/model/timeline/n$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/n;

    .line 52
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/n$a;->a(Lcom/twitter/model/timeline/n;)Lcom/twitter/android/timeline/n$a;

    move-result-object v0

    return-object v0

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 18
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/m;->a(J)Lcom/twitter/android/timeline/n$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/m;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
