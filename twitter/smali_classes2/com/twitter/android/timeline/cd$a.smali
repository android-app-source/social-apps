.class public abstract Lcom/twitter/android/timeline/cd$a;
.super Lcom/twitter/android/timeline/bk$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/cd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/android/timeline/cd;",
        "B:",
        "Lcom/twitter/android/timeline/cd$a",
        "<TT;TB;>;>",
        "Lcom/twitter/android/timeline/bk$a",
        "<TT;TB;>;"
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/model/core/Tweet;

.field h:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/bk$a;-><init>(J)V

    .line 86
    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/twitter/android/timeline/bk$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/cd$a;->a:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 90
    iput-object p1, p0, Lcom/twitter/android/timeline/cd$a;->a:Lcom/twitter/model/core/Tweet;

    .line 91
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$a;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/android/timeline/cd$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 96
    iput-object p1, p0, Lcom/twitter/android/timeline/cd$a;->h:Ljava/lang/String;

    .line 97
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$a;

    return-object v0
.end method
