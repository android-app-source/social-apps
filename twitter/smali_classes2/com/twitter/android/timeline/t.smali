.class public Lcom/twitter/android/timeline/t;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lckb",
        "<",
        "Lcom/twitter/android/timeline/az;",
        "Lcom/twitter/android/timeline/u;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private b:Lcom/twitter/android/timeline/s;

.field private c:Lcom/twitter/android/timeline/r;

.field private d:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/timeline/s;Lcom/twitter/android/timeline/r;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lckb;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/twitter/android/timeline/t;->a:Landroid/view/LayoutInflater;

    .line 35
    iput-object p2, p0, Lcom/twitter/android/timeline/t;->b:Lcom/twitter/android/timeline/s;

    .line 36
    iput-object p3, p0, Lcom/twitter/android/timeline/t;->c:Lcom/twitter/android/timeline/r;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/android/timeline/u;
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/timeline/t;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f040132

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 43
    const v0, 0x7f1303ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/timeline/t;->d:Landroid/widget/ImageView;

    .line 44
    new-instance v0, Lcom/twitter/android/timeline/u;

    invoke-direct {v0, v1}, Lcom/twitter/android/timeline/u;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/twitter/android/timeline/u;

    check-cast p2, Lcom/twitter/android/timeline/az;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/t;->a(Lcom/twitter/android/timeline/u;Lcom/twitter/android/timeline/az;)V

    return-void
.end method

.method public a(Lcom/twitter/android/timeline/u;Lcom/twitter/android/timeline/az;)V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p1, Lcom/twitter/android/timeline/u;->a:Lcom/twitter/library/widget/LiveContentView;

    .line 51
    iget-object v1, p2, Lcom/twitter/android/timeline/az;->a:Lcom/twitter/model/timeline/b;

    .line 52
    iget-object v2, p0, Lcom/twitter/android/timeline/t;->b:Lcom/twitter/android/timeline/s;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/LiveContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v2, p0, Lcom/twitter/android/timeline/t;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/twitter/android/timeline/t;->c:Lcom/twitter/android/timeline/r;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v2, p0, Lcom/twitter/android/timeline/t;->d:Landroid/widget/ImageView;

    const v3, 0x7f13007e

    invoke-virtual {v2, v3, p2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/LiveContentView;->setBannerPrompt(Lcom/twitter/model/timeline/b;)V

    .line 56
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/az;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lcom/twitter/android/timeline/az;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/t;->a(Lcom/twitter/android/timeline/az;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/t;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/timeline/u;

    move-result-object v0

    return-object v0
.end method
