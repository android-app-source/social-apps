.class public Lcom/twitter/android/timeline/ad;
.super Lcom/twitter/android/timeline/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/d",
        "<",
        "Lcom/twitter/android/timeline/ae;",
        "Lcom/twitter/android/timeline/ae$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/timeline/d;-><init>(Z)V

    .line 19
    return-void
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/ae$a;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/android/timeline/ae$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/ae$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ae$a;)Lcom/twitter/android/timeline/ae$a;
    .locals 2

    .prologue
    .line 40
    sget v0, Lbue;->Q:I

    .line 41
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/x;->a:Lcom/twitter/util/serialization/l;

    .line 40
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/x;

    .line 42
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ad;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    .line 43
    invoke-virtual {p2, v1}, Lcom/twitter/android/timeline/ae$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/ae$a;

    .line 44
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/ae$a;->a(Lcom/twitter/model/moments/x;)Lcom/twitter/android/timeline/ae$a;

    move-result-object v0

    .line 43
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/ae$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ad;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ae$a;)Lcom/twitter/android/timeline/ae$a;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ad;->a(J)Lcom/twitter/android/timeline/ae$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 24
    sget v1, Lbue;->e:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 26
    sget v2, Lbue;->g:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 27
    if-ne v1, v0, :cond_0

    invoke-static {v2}, Lcom/twitter/model/timeline/z$a;->o(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ad;->b(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
