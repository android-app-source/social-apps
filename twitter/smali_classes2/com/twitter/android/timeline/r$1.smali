.class Lcom/twitter/android/timeline/r$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/timeline/r;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/timeline/az;

.field final synthetic b:Lcom/twitter/android/timeline/r;


# direct methods
.method constructor <init>(Lcom/twitter/android/timeline/r;Lcom/twitter/android/timeline/az;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    iput-object p2, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 78
    iget-object v0, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    iget-object v0, v0, Lcom/twitter/android/timeline/az;->a:Lcom/twitter/model/timeline/b;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    iget-object v0, v0, Lcom/twitter/android/timeline/az;->a:Lcom/twitter/model/timeline/b;

    iget-object v0, v0, Lcom/twitter/model/timeline/b;->d:Ljava/lang/String;

    .line 80
    packed-switch p3, :pswitch_data_0

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 82
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    invoke-static {v1, v0}, Lcom/twitter/android/timeline/r;->a(Lcom/twitter/android/timeline/r;Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :pswitch_1
    iget-object v1, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    invoke-static {v1, v0}, Lcom/twitter/android/timeline/r;->b(Lcom/twitter/android/timeline/r;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :pswitch_2
    iget-object v1, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    invoke-static {v1, v0}, Lcom/twitter/android/timeline/r;->c(Lcom/twitter/android/timeline/r;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    invoke-virtual {v0}, Lcom/twitter/android/timeline/az;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    .line 95
    iget v0, v0, Lcom/twitter/android/timeline/bg;->g:I

    .line 97
    iget-object v1, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    invoke-static {v1}, Lcom/twitter/android/timeline/r;->a(Lcom/twitter/android/timeline/r;)Lcom/twitter/android/timeline/be;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/twitter/android/timeline/be;->a(Lcom/twitter/android/timeline/bk;III)V

    .line 99
    iget-object v0, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    invoke-static {v0}, Lcom/twitter/android/timeline/r;->b(Lcom/twitter/android/timeline/r;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 100
    const-string/jumbo v0, "SeeFewer"

    .line 101
    invoke-static {v0, v4, v4}, Lcom/twitter/model/timeline/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/g;

    move-result-object v0

    .line 103
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 100
    invoke-static {v0, v1}, Lcom/twitter/model/timeline/k;->a(Lcom/twitter/model/timeline/g;Ljava/util/List;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    iget-object v2, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/timeline/r;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/k;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/timeline/r$1;->b:Lcom/twitter/android/timeline/r;

    const-string/jumbo v1, "click"

    iget-object v2, p0, Lcom/twitter/android/timeline/r$1;->a:Lcom/twitter/android/timeline/az;

    const-string/jumbo v3, "feedback_seefewer"

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/timeline/r;->a(Lcom/twitter/android/timeline/r;Ljava/lang/String;Lcom/twitter/android/timeline/az;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
