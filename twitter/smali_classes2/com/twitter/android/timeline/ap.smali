.class public Lcom/twitter/android/timeline/ap;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/timeline/j;
.implements Lcom/twitter/android/timeline/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/ap$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/android/timeline/cd;

.field public final b:Lcom/twitter/model/timeline/o;


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/ap$a;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 26
    invoke-static {p1}, Lcom/twitter/android/timeline/ap$a;->a(Lcom/twitter/android/timeline/ap$a;)Lcom/twitter/android/timeline/cd;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    iput-object v0, p0, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    .line 27
    invoke-static {p1}, Lcom/twitter/android/timeline/ap$a;->b(Lcom/twitter/android/timeline/ap$a;)Lcom/twitter/model/timeline/o;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/o;

    iput-object v0, p0, Lcom/twitter/android/timeline/ap;->b:Lcom/twitter/model/timeline/o;

    .line 28
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/ap$a;Lcom/twitter/android/timeline/ap$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/ap;-><init>(Lcom/twitter/android/timeline/ap$a;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/timeline/o;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/timeline/ap;->b:Lcom/twitter/model/timeline/o;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/cd;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 65
    const-string/jumbo v0, "<div><b>Metadata:</b></div>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "<div><b>Hash Code:</b> "

    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/timeline/ap;->b:Lcom/twitter/model/timeline/o;

    invoke-virtual {v1}, Lcom/twitter/model/timeline/o;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "<div><b>Metadata Class:</b> "

    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/timeline/ap;->b:Lcom/twitter/model/timeline/o;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/timeline/ap;->b:Lcom/twitter/model/timeline/o;

    iget-object v0, v0, Lcom/twitter/model/timeline/o;->d:Lcom/twitter/model/timeline/r;

    .line 70
    if-eqz v0, :cond_0

    .line 71
    const-string/jumbo v1, "<div><b>Scribe Info Hash Code:</b> "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 72
    invoke-virtual {v0}, Lcom/twitter/model/timeline/r;->hashCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {p1, v0}, Lcom/twitter/android/timeline/bm;->a(Ljava/lang/StringBuilder;Lcom/twitter/model/core/Tweet;)V

    .line 75
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/timeline/ap;->a:Lcom/twitter/android/timeline/cd;

    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method
