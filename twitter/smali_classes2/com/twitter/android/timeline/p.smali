.class public Lcom/twitter/android/timeline/p;
.super Lcom/twitter/android/timeline/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/d",
        "<",
        "Lcom/twitter/android/timeline/cd;",
        "Lcom/twitter/android/timeline/cd$b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/timeline/d;-><init>(Z)V

    .line 18
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/cd$b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/p;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cd$b;)Lcom/twitter/android/timeline/cd$b;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/cd$b;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/twitter/android/timeline/cd$b;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/cd$b;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cd$b;)Lcom/twitter/android/timeline/cd$b;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/p;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 37
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/cd$b;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$b;

    return-object v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/p;->a(J)Lcom/twitter/android/timeline/cd$b;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/p;->b(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
