.class public abstract Lcom/twitter/android/timeline/bo;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/android/timeline/bk;",
        "B:",
        "Lcom/twitter/android/timeline/bk$a",
        "<TT;TB;>;>",
        "Lcbp",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/bh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 21
    new-instance v0, Lcom/twitter/android/timeline/bh;

    invoke-direct {v0}, Lcom/twitter/android/timeline/bh;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/timeline/bo;->a:Lcom/twitter/android/timeline/bh;

    return-void
.end method

.method private a(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bg;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/timeline/bo;->a:Lcom/twitter/android/timeline/bh;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bh;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/twitter/android/timeline/bo;->a:Lcom/twitter/android/timeline/bh;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bh;->b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bg;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)Lcom/twitter/model/timeline/r;
    .locals 2

    .prologue
    .line 109
    invoke-static {p0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    sget v0, Lbue;->k:I

    .line 111
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 112
    sget-object v1, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    .line 114
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 91
    move-object v0, p0

    .line 92
    :goto_0
    instance-of v1, v0, Landroid/database/CursorWrapper;

    if-eqz v1, :cond_0

    .line 93
    check-cast v0, Landroid/database/CursorWrapper;

    invoke-virtual {v0}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_0
    return-object v0
.end method

.method private static f(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 118
    invoke-static {p0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    sget v0, Lbue;->b:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 121
    :goto_0
    return-wide v0

    :cond_0
    const/16 v0, 0x17

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method private static g(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    invoke-static {p0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    sget v0, Lbue;->aw:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h(Landroid/database/Cursor;)Lcom/twitter/model/core/TwitterSocialProof;
    .locals 2

    .prologue
    .line 136
    invoke-static {p0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    sget v0, Lbue;->ax:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/TwitterSocialProof;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    .line 140
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bo;)Lcbi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITEM:",
            "Lcom/twitter/android/timeline/bk;",
            "BUI",
            "LDER:Lcom/twitter/android/timeline/bk$a",
            "<TITEM;TBUI",
            "LDER;",
            ">;>(",
            "Landroid/database/Cursor;",
            "Lcom/twitter/android/timeline/bo",
            "<TITEM;TBUI",
            "LDER;",
            ">;)",
            "Lcbi",
            "<TITEM;>;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bo;->a(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bg;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    .line 76
    invoke-static {p1}, Lcom/twitter/android/timeline/bo;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 77
    iget v2, v0, Lcom/twitter/android/timeline/bg;->o:I

    iget v0, v0, Lcom/twitter/android/timeline/bg;->p:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v1, p2, v2, v0}, Lcbj;->a(Landroid/database/Cursor;Lcbp;II)Lcbi;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "TB;)TB;"
        }
    .end annotation
.end method

.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bo;->c(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(J)Lcom/twitter/android/timeline/bk$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation
.end method

.method public final c(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-static {p1}, Lcom/twitter/android/timeline/bo;->b(Landroid/database/Cursor;)Lcom/twitter/model/timeline/r;

    move-result-object v0

    .line 46
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bo;->a(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bg;

    move-result-object v1

    .line 47
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bo;->e(Landroid/database/Cursor;)Lcha;

    move-result-object v2

    .line 48
    invoke-static {p1}, Lcom/twitter/android/timeline/bo;->g(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 49
    invoke-static {p1}, Lcom/twitter/android/timeline/bo;->h(Landroid/database/Cursor;)Lcom/twitter/model/core/TwitterSocialProof;

    move-result-object v4

    .line 50
    invoke-static {p1}, Lcom/twitter/android/timeline/bo;->f(Landroid/database/Cursor;)J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/twitter/android/timeline/bo;->b(J)Lcom/twitter/android/timeline/bk$a;

    move-result-object v5

    invoke-virtual {p0, p1, v5}, Lcom/twitter/android/timeline/bo;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v5

    .line 51
    invoke-virtual {v5, v0}, Lcom/twitter/android/timeline/bk$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/bk$a;->a(Lcom/twitter/android/timeline/bg;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v2}, Lcom/twitter/android/timeline/bk$a;->a(Lcha;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0, v3}, Lcom/twitter/android/timeline/bk$a;->b(Ljava/lang/String;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/bk$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bk$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 50
    return-object v0
.end method

.method protected e(Landroid/database/Cursor;)Lcha;
    .locals 2

    .prologue
    .line 146
    invoke-static {p1}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    sget v0, Lbue;->av:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 148
    sget-object v1, Lcha;->a:Lcha$b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    .line 150
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
