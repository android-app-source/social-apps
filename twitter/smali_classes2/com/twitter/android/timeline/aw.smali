.class public Lcom/twitter/android/timeline/aw;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/aw$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/ax;",
        "Lcom/twitter/android/timeline/ax$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/aw$a;


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/aw$a;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/android/timeline/aw;->a:Lcom/twitter/android/timeline/aw$a;

    .line 21
    return-void
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/ax$a;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/timeline/ax$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/ax$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ax$a;)Lcom/twitter/android/timeline/ax$a;
    .locals 4

    .prologue
    .line 40
    sget v0, Lbue;->f:I

    .line 41
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/v$d;->a:Lcom/twitter/util/serialization/l;

    .line 40
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/v$d;

    .line 44
    iget-object v1, v0, Lcom/twitter/model/moments/v$d;->c:Lcom/twitter/model/moments/v$b;

    .line 45
    iget-wide v2, v0, Lcom/twitter/model/moments/v$d;->d:J

    .line 47
    iget-object v0, p0, Lcom/twitter/android/timeline/aw;->a:Lcom/twitter/android/timeline/aw$a;

    .line 48
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/timeline/aw;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bo;)Lcbi;

    move-result-object v0

    .line 49
    invoke-virtual {p2, v1}, Lcom/twitter/android/timeline/ax$a;->a(Lcom/twitter/model/moments/v$b;)Lcom/twitter/android/timeline/ax$a;

    move-result-object v1

    .line 50
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/ax$a;->a(Lcbi;)Lcom/twitter/android/timeline/ax$a;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/timeline/ax$a;->a(J)Lcom/twitter/android/timeline/ax$a;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 15
    check-cast p2, Lcom/twitter/android/timeline/ax$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aw;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ax$a;)Lcom/twitter/android/timeline/ax$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 25
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aw;->a(J)Lcom/twitter/android/timeline/ax$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 15
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/aw;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
