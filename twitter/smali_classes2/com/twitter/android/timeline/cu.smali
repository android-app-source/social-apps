.class public Lcom/twitter/android/timeline/cu;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/twitter/android/timeline/cu;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 25
    iput-object p2, p0, Lcom/twitter/android/timeline/cu;->b:Ljava/util/HashSet;

    .line 26
    return-void

    .line 24
    :cond_0
    new-instance p1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/timeline/cu;->b:Ljava/util/HashSet;

    return-object v0
.end method

.method public a(Lcom/twitter/android/timeline/co;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 30
    iget-object v0, p1, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    .line 31
    iget-object v2, p0, Lcom/twitter/android/timeline/cu;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v2

    .line 33
    iput p2, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 35
    iget-object v3, v0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 36
    :goto_0
    iget-object v3, p0, Lcom/twitter/android/timeline/cu;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    .line 37
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v1, v5, v3

    const/4 v1, 0x2

    aput-object v0, v5, v1

    const/4 v0, 0x3

    const-string/jumbo v1, "user"

    aput-object v1, v5, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "results"

    aput-object v1, v5, v0

    .line 38
    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/timeline/cu;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 39
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 40
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 37
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 42
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    .line 35
    goto :goto_0
.end method
