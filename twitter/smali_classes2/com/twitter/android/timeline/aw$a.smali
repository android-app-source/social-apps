.class public Lcom/twitter/android/timeline/aw$a;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/aw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/at;",
        "Lcom/twitter/android/timeline/at$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/at$a;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/twitter/android/timeline/at$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/at$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/at$a;)Lcom/twitter/android/timeline/at$a;
    .locals 4

    .prologue
    .line 66
    sget v0, Lbue;->f:I

    .line 67
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/v$d;->a:Lcom/twitter/util/serialization/l;

    .line 66
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/v$d;

    .line 68
    iget-object v1, v0, Lcom/twitter/model/moments/v$d;->b:Lcom/twitter/model/moments/v$c;

    .line 69
    iget-wide v2, v0, Lcom/twitter/model/moments/v$d;->d:J

    .line 70
    invoke-virtual {p2, v1}, Lcom/twitter/android/timeline/at$a;->a(Lcom/twitter/model/moments/v$c;)Lcom/twitter/android/timeline/at$a;

    move-result-object v0

    .line 71
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/timeline/at$a;->a(J)Lcom/twitter/android/timeline/at$a;

    move-result-object v0

    .line 70
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 54
    check-cast p2, Lcom/twitter/android/timeline/at$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aw$a;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/at$a;)Lcom/twitter/android/timeline/at$a;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aw$a;->a(J)Lcom/twitter/android/timeline/at$a;

    move-result-object v0

    return-object v0
.end method
