.class public Lcom/twitter/android/timeline/bt;
.super Lcom/twitter/android/timeline/cd;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/timeline/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/bt$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final i:J


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/bt$a;)V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/cd;-><init>(Lcom/twitter/android/timeline/cd$a;)V

    .line 15
    invoke-static {p1}, Lcom/twitter/android/timeline/bt$a;->a(Lcom/twitter/android/timeline/bt$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/bt;->a:Ljava/lang/String;

    .line 16
    invoke-static {p1}, Lcom/twitter/android/timeline/bt$a;->b(Lcom/twitter/android/timeline/bt$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/timeline/bt;->i:J

    .line 17
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/bt$a;Lcom/twitter/android/timeline/bt$1;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bt;-><init>(Lcom/twitter/android/timeline/bt$a;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/timeline/bt;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/timeline/bt;->a:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/timeline/bt;->i:J

    invoke-static {p1, v0, v2, v3}, Lcom/twitter/android/timeline/bm;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 33
    invoke-super {p0, p1}, Lcom/twitter/android/timeline/cd;->a(Ljava/lang/StringBuilder;)V

    .line 34
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/twitter/android/timeline/bt;->i:J

    return-wide v0
.end method
