.class public Lcom/twitter/android/timeline/bp;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/bq;",
        "Lcom/twitter/android/timeline/bq$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/twitter/model/timeline/l;
    .locals 1

    .prologue
    .line 66
    invoke-static {p0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 69
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/model/timeline/l;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/model/timeline/l;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/bq$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bp;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bq$a;)Lcom/twitter/android/timeline/bq$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/bq$a;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/android/timeline/bq$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/bq$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bq$a;)Lcom/twitter/android/timeline/bq$a;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 32
    sget v0, Lbue;->U:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 33
    sget v0, Lbue;->V:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 34
    sget v0, Lbue;->W:I

    .line 35
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 36
    sget v0, Lbue;->X:I

    .line 37
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 38
    sget v0, Lbue;->Y:I

    .line 39
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 40
    :goto_0
    sget v5, Lbue;->ak:I

    .line 41
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v5, v3, :cond_1

    move v5, v3

    .line 42
    :goto_1
    sget v6, Lbue;->Z:I

    .line 43
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 44
    sget v6, Lbue;->aa:I

    .line 45
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 46
    sget v9, Lbue;->ab:I

    .line 47
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-ne v9, v3, :cond_2

    move v9, v3

    .line 48
    :goto_2
    if-eqz v8, :cond_3

    move-object v3, v8

    .line 49
    :goto_3
    if-eqz v6, :cond_4

    move-object v4, v6

    .line 52
    :goto_4
    invoke-static {v10, v3, v0}, Lcom/twitter/android/timeline/bp;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/twitter/model/timeline/l;

    move-result-object v3

    .line 54
    invoke-static {v11, v4, v9}, Lcom/twitter/android/timeline/bp;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/twitter/model/timeline/l;

    move-result-object v4

    .line 56
    new-instance v0, Lcom/twitter/model/timeline/ab;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/timeline/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/timeline/l;Lcom/twitter/model/timeline/l;Z)V

    .line 60
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/bq$a;->a(Lcom/twitter/model/timeline/ab;)Lcom/twitter/android/timeline/bq$a;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v4

    .line 39
    goto :goto_0

    :cond_1
    move v5, v4

    .line 41
    goto :goto_1

    :cond_2
    move v9, v4

    .line 47
    goto :goto_2

    :cond_3
    move-object v3, v7

    .line 48
    goto :goto_3

    :cond_4
    move-object v4, v7

    .line 49
    goto :goto_4
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 17
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bp;->a(J)Lcom/twitter/android/timeline/bq$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bp;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
