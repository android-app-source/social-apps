.class public Lcom/twitter/android/timeline/ax;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/ax$a;
    }
.end annotation


# instance fields
.field public final a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/at;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/twitter/model/moments/v$b;

.field public final c:J


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/ax$a;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 23
    invoke-static {p1}, Lcom/twitter/android/timeline/ax$a;->a(Lcom/twitter/android/timeline/ax$a;)Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbi;

    iput-object v0, p0, Lcom/twitter/android/timeline/ax;->a:Lcbi;

    .line 24
    invoke-static {p1}, Lcom/twitter/android/timeline/ax$a;->b(Lcom/twitter/android/timeline/ax$a;)Lcom/twitter/model/moments/v$b;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/v$b;

    iput-object v0, p0, Lcom/twitter/android/timeline/ax;->b:Lcom/twitter/model/moments/v$b;

    .line 25
    invoke-static {p1}, Lcom/twitter/android/timeline/ax$a;->c(Lcom/twitter/android/timeline/ax$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/timeline/ax;->c:J

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/ax$a;Lcom/twitter/android/timeline/ax$1;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/ax;-><init>(Lcom/twitter/android/timeline/ax$a;)V

    return-void
.end method


# virtual methods
.method public bc_()Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcbl$a;

    invoke-direct {v0}, Lcbl$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/timeline/ax;->a:Lcbi;

    .line 32
    invoke-virtual {v0, v1}, Lcbl$a;->a(Ljava/lang/Iterable;)Lcbl$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcbl$a;->a()Lcbl;

    move-result-object v0

    .line 31
    return-object v0
.end method
