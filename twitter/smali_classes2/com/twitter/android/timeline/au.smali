.class public Lcom/twitter/android/timeline/au;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/au$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/widget/e",
        "<",
        "Lcom/twitter/android/timeline/at;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final c:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/app/Activity;",
            "Lcom/twitter/android/moments/ui/card/d;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/app/Activity;",
            "Lcom/twitter/android/moments/ui/card/j;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/v$c;",
            "Lcom/twitter/android/moments/viewmodels/m;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/android/timeline/av;

.field private final g:Lcom/twitter/android/timeline/au$a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/viewmodels/ae;Lcom/twitter/android/timeline/av;Lcom/twitter/android/timeline/au$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/app/Activity;",
            "Lcom/twitter/android/moments/ui/card/d;",
            ">;",
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/app/Activity;",
            "Lcom/twitter/android/moments/ui/card/j;",
            ">;",
            "Lcom/twitter/android/moments/viewmodels/ae;",
            "Lcom/twitter/android/timeline/av;",
            "Lcom/twitter/android/timeline/au$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/twitter/android/timeline/au;->a:Landroid/app/Activity;

    .line 63
    iput-object p2, p0, Lcom/twitter/android/timeline/au;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 64
    iput-object p3, p0, Lcom/twitter/android/timeline/au;->c:Lcom/twitter/util/object/d;

    .line 65
    iput-object p4, p0, Lcom/twitter/android/timeline/au;->d:Lcom/twitter/util/object/d;

    .line 66
    iput-object p5, p0, Lcom/twitter/android/timeline/au;->e:Lcom/twitter/util/object/d;

    .line 67
    iput-object p6, p0, Lcom/twitter/android/timeline/au;->f:Lcom/twitter/android/timeline/av;

    .line 68
    iput-object p7, p0, Lcom/twitter/android/timeline/au;->g:Lcom/twitter/android/timeline/au$a;

    .line 69
    return-void
.end method

.method private a(Landroid/app/Activity;Lcom/twitter/android/widget/as$a;I)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/twitter/android/timeline/au$2;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/twitter/android/timeline/au$2;-><init>(Lcom/twitter/android/timeline/au;Lcom/twitter/android/widget/as$a;ILandroid/app/Activity;)V

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/client/v;Lcom/twitter/android/timeline/av;)Lcom/twitter/android/timeline/au;
    .locals 8

    .prologue
    .line 46
    new-instance v0, Lcom/twitter/android/timeline/au;

    .line 48
    invoke-static {p2}, Lcom/twitter/android/moments/ui/card/e;->a(Lcom/twitter/library/client/v;)Lcom/twitter/android/moments/ui/card/e;

    move-result-object v3

    sget-object v4, Lcom/twitter/android/moments/ui/card/j;->a:Lcom/twitter/util/object/d;

    new-instance v5, Lcom/twitter/android/moments/viewmodels/ae;

    invoke-direct {v5}, Lcom/twitter/android/moments/viewmodels/ae;-><init>()V

    new-instance v7, Lcom/twitter/android/timeline/au$a;

    invoke-direct {v7}, Lcom/twitter/android/timeline/au$a;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/timeline/au;-><init>(Landroid/app/Activity;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/viewmodels/ae;Lcom/twitter/android/timeline/av;Lcom/twitter/android/timeline/au$a;)V

    .line 46
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/timeline/au;)Lcom/twitter/android/timeline/av;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/timeline/au;->f:Lcom/twitter/android/timeline/av;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/timeline/au;)Lcom/twitter/android/timeline/au$a;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/timeline/au;->g:Lcom/twitter/android/timeline/au$a;

    return-object v0
.end method


# virtual methods
.method a(Landroid/app/Activity;Lcom/twitter/android/timeline/at;I)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 120
    new-instance v0, Lcom/twitter/android/timeline/au$1;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/twitter/android/timeline/au$1;-><init>(Lcom/twitter/android/timeline/au;Lcom/twitter/android/timeline/at;ILandroid/app/Activity;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/android/timeline/at;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;

    invoke-direct {v0, p1}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;-><init>(Landroid/content/Context;)V

    .line 75
    invoke-virtual {p0, v0, p2, p3}, Lcom/twitter/android/timeline/au;->a(Landroid/view/View;Lcom/twitter/android/timeline/at;I)V

    .line 76
    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    check-cast p2, Lcom/twitter/android/timeline/at;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/timeline/au;->a(Landroid/content/Context;Lcom/twitter/android/timeline/at;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Lcom/twitter/android/timeline/at;I)V
    .locals 6

    .prologue
    .line 81
    instance-of v0, p1, Lcom/twitter/android/widget/MomentsCardCarouselItemView;

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 82
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MomentsCardCarouselItemView;

    .line 83
    iget-object v1, p0, Lcom/twitter/android/timeline/au;->e:Lcom/twitter/util/object/d;

    iget-object v2, p2, Lcom/twitter/android/timeline/at;->a:Lcom/twitter/model/moments/v$c;

    invoke-interface {v1, v2}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/moments/viewmodels/m;

    .line 84
    invoke-virtual {v0}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->getBoundMomentId()J

    move-result-wide v2

    .line 85
    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/m;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 110
    :goto_0
    return-void

    .line 91
    :cond_0
    instance-of v2, p2, Lcom/twitter/android/widget/as$a;

    if-eqz v2, :cond_1

    .line 92
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/as$a;

    .line 93
    iget-object v3, p0, Lcom/twitter/android/timeline/au;->d:Lcom/twitter/util/object/d;

    iget-object v4, p0, Lcom/twitter/android/timeline/au;->a:Landroid/app/Activity;

    .line 94
    invoke-interface {v3, v4}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/moments/ui/card/j;

    .line 95
    iget-object v4, p0, Lcom/twitter/android/timeline/au;->a:Landroid/app/Activity;

    .line 96
    invoke-direct {p0, v4, v2, p3}, Lcom/twitter/android/timeline/au;->a(Landroid/app/Activity;Lcom/twitter/android/widget/as$a;I)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 95
    invoke-virtual {v3, v2}, Lcom/twitter/android/moments/ui/card/j;->a(Landroid/view/View$OnClickListener;)V

    .line 106
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/timeline/au;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v3, v1, v2}, Lcom/twitter/android/moments/ui/card/h;->a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 108
    invoke-interface {v3}, Lcom/twitter/android/moments/ui/card/h;->b()V

    .line 109
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/MomentsCardCarouselItemView;->a(Lcom/twitter/android/moments/ui/card/h;)V

    goto :goto_0

    .line 99
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/timeline/au;->c:Lcom/twitter/util/object/d;

    iget-object v3, p0, Lcom/twitter/android/timeline/au;->a:Landroid/app/Activity;

    invoke-interface {v2, v3}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/moments/ui/card/d;

    .line 100
    iget-object v3, p2, Lcom/twitter/android/timeline/at;->a:Lcom/twitter/model/moments/v$c;

    iget-object v3, v3, Lcom/twitter/model/moments/v$c;->b:Lcom/twitter/model/moments/Moment;

    iget-object v3, v3, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    .line 101
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/twitter/android/moments/ui/card/d;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 102
    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/card/d;->a(Lcom/twitter/model/moments/a;)V

    .line 103
    iget-object v3, p0, Lcom/twitter/android/timeline/au;->a:Landroid/app/Activity;

    invoke-virtual {p0, v3, p2, p3}, Lcom/twitter/android/timeline/au;->a(Landroid/app/Activity;Lcom/twitter/android/timeline/at;I)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/card/d;->a(Landroid/view/View$OnClickListener;)V

    move-object v3, v2

    .line 104
    goto :goto_1
.end method

.method public synthetic a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Lcom/twitter/android/timeline/at;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/timeline/au;->b(Landroid/view/View;Lcom/twitter/android/timeline/at;I)V

    return-void
.end method

.method public b(Landroid/view/View;Lcom/twitter/android/timeline/at;I)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public synthetic b(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Lcom/twitter/android/timeline/at;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/timeline/au;->a(Landroid/view/View;Lcom/twitter/android/timeline/at;I)V

    return-void
.end method
