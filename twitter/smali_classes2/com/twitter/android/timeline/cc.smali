.class public Lcom/twitter/android/timeline/cc;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/timeline/bo",
            "<+",
            "Lcom/twitter/android/timeline/bk;",
            "+",
            "Lcom/twitter/android/timeline/bk$a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 22
    new-instance v0, Lcom/twitter/android/timeline/p;

    invoke-direct {v0}, Lcom/twitter/android/timeline/p;-><init>()V

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/cc;->a:Ljava/util/List;

    .line 24
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/timeline/cc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bo;

    .line 31
    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bo;->b(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bo;->c(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    return-object v0

    .line 35
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "no hydrator can hydrate this timeline item!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/cc;->a(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    return-object v0
.end method
