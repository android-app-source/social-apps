.class public final Lcom/twitter/android/timeline/bg$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/bg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/android/timeline/bg;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:J

.field c:Ljava/lang/String;

.field d:I

.field e:I

.field f:J

.field g:I

.field h:I

.field i:Z

.field j:Z

.field k:Z

.field l:Z

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:Z

.field s:I

.field t:Ljava/lang/String;

.field u:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 130
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/timeline/bg$a;->p:I

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    .line 251
    iget v0, p0, Lcom/twitter/android/timeline/bg$a;->m:I

    iget v1, p0, Lcom/twitter/android/timeline/bg$a;->n:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/timeline/bg$a;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bg$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bg$a;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 157
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->d:I

    .line 158
    return-object p0
.end method

.method public a(J)Lcom/twitter/android/timeline/bg$a;
    .locals 1

    .prologue
    .line 145
    iput-wide p1, p0, Lcom/twitter/android/timeline/bg$a;->b:J

    .line 146
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/twitter/android/timeline/bg$a;->a:Ljava/lang/String;

    .line 140
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/twitter/android/timeline/bg$a;->r:Z

    .line 240
    return-object p0
.end method

.method public a(ZZZZ)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/twitter/android/timeline/bg$a;->i:Z

    .line 201
    iput-boolean p2, p0, Lcom/twitter/android/timeline/bg$a;->j:Z

    .line 202
    iput-boolean p3, p0, Lcom/twitter/android/timeline/bg$a;->k:Z

    .line 203
    iput-boolean p4, p0, Lcom/twitter/android/timeline/bg$a;->l:Z

    .line 204
    return-object p0
.end method

.method public b(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 163
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->e:I

    .line 164
    return-object p0
.end method

.method public b(J)Lcom/twitter/android/timeline/bg$a;
    .locals 1

    .prologue
    .line 169
    iput-wide p1, p0, Lcom/twitter/android/timeline/bg$a;->f:J

    .line 170
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/twitter/android/timeline/bg$a;->c:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bg$a;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 175
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->h:I

    .line 176
    return-object p0
.end method

.method public c(J)Lcom/twitter/android/timeline/bg$a;
    .locals 1

    .prologue
    .line 193
    iput-wide p1, p0, Lcom/twitter/android/timeline/bg$a;->u:J

    .line 194
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/twitter/android/timeline/bg$a;->t:Ljava/lang/String;

    .line 188
    return-object p0
.end method

.method public d(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 181
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->g:I

    .line 182
    return-object p0
.end method

.method public e(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 209
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->m:I

    .line 210
    return-object p0
.end method

.method protected e()Lcom/twitter/android/timeline/bg;
    .locals 1

    .prologue
    .line 260
    new-instance v0, Lcom/twitter/android/timeline/bg;

    invoke-direct {v0, p0}, Lcom/twitter/android/timeline/bg;-><init>(Lcom/twitter/android/timeline/bg$a;)V

    return-object v0
.end method

.method public f(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 215
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->n:I

    .line 216
    return-object p0
.end method

.method public g(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 221
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->o:I

    .line 222
    return-object p0
.end method

.method public h(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 227
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->p:I

    .line 228
    return-object p0
.end method

.method public i(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 233
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->q:I

    .line 234
    return-object p0
.end method

.method public j(I)Lcom/twitter/android/timeline/bg$a;
    .locals 0

    .prologue
    .line 245
    iput p1, p0, Lcom/twitter/android/timeline/bg$a;->s:I

    .line 246
    return-object p0
.end method
