.class public Lcom/twitter/android/timeline/ag;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lckb",
        "<",
        "Lcom/twitter/android/timeline/af;",
        "Lcom/twitter/android/timeline/ah;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lckb;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/twitter/android/timeline/ag;->a:Landroid/view/LayoutInflater;

    .line 15
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/android/timeline/ah;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/timeline/ag;->a:Landroid/view/LayoutInflater;

    invoke-static {v0, p1}, Lcom/twitter/android/timeline/ah;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/timeline/ah;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/twitter/android/timeline/ah;

    check-cast p2, Lcom/twitter/android/timeline/af;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ag;->a(Lcom/twitter/android/timeline/ah;Lcom/twitter/android/timeline/af;)V

    return-void
.end method

.method public a(Lcom/twitter/android/timeline/ah;Lcom/twitter/android/timeline/af;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p2, Lcom/twitter/android/timeline/af;->a:Lcom/twitter/model/moments/Moment;

    invoke-virtual {p1, v0}, Lcom/twitter/android/timeline/ah;->a(Lcom/twitter/model/moments/Moment;)V

    .line 31
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/af;)Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lcom/twitter/android/timeline/af;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ag;->a(Lcom/twitter/android/timeline/af;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ag;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/timeline/ah;

    move-result-object v0

    return-object v0
.end method
