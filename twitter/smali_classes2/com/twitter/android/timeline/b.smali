.class public Lcom/twitter/android/timeline/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/a;Lcom/twitter/android/revenue/a;)Lcom/twitter/android/timeline/bk;
    .locals 4

    .prologue
    .line 15
    instance-of v0, p2, Lcom/twitter/android/revenue/l;

    if-eqz v0, :cond_0

    .line 16
    new-instance v0, Lcom/twitter/android/timeline/cd$b;

    iget-wide v2, p1, Lcom/twitter/android/timeline/a;->d:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/timeline/cd$b;-><init>(J)V

    .line 17
    invoke-virtual {p1}, Lcom/twitter/android/timeline/a;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cd$b;->a(Lcom/twitter/android/timeline/bg;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$b;

    iget-object v1, p1, Lcom/twitter/android/timeline/a;->e:Lcom/twitter/model/timeline/r;

    .line 18
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cd$b;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$b;

    move-object v1, p2

    check-cast v1, Lcom/twitter/android/revenue/l;

    iget-object v1, v1, Lcom/twitter/android/revenue/l;->f:Lcom/twitter/model/core/Tweet;

    .line 19
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cd$b;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$b;

    .line 20
    invoke-virtual {v0}, Lcom/twitter/android/timeline/cd$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 21
    new-instance v1, Lcom/twitter/android/timeline/bt$a;

    invoke-direct {v1, v0}, Lcom/twitter/android/timeline/bt$a;-><init>(Lcom/twitter/android/timeline/cd;)V

    iget-object v0, p1, Lcom/twitter/android/timeline/a;->a:Ljava/lang/String;

    .line 22
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/bt$a;->a(Ljava/lang/String;)Lcom/twitter/android/timeline/bt$a;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/android/revenue/a;->e:J

    .line 23
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/timeline/bt$a;->a(J)Lcom/twitter/android/timeline/bt$a;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bt$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 27
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
