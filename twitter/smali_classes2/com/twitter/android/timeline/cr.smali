.class public Lcom/twitter/android/timeline/cr;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/cq;",
        "Lcom/twitter/android/timeline/cq$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 13
    check-cast p2, Lcom/twitter/android/timeline/cq$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/cr;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cq$a;)Lcom/twitter/android/timeline/cq$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/cq$a;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/android/timeline/cq$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/cq$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cq$a;)Lcom/twitter/android/timeline/cq$a;
    .locals 2

    .prologue
    .line 30
    sget v0, Lbue;->f:I

    .line 31
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/timeline/at$a;->a:Lcom/twitter/util/serialization/b;

    .line 30
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/at$a;

    .line 33
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/twitter/android/timeline/cq$a;->a(Lcbi;)Lcom/twitter/android/timeline/cq$a;

    move-result-object v1

    .line 34
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/cq$a;->a(Lcom/twitter/model/timeline/at$a;)Lcom/twitter/android/timeline/cq$a;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 17
    sget v0, Lbue;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 18
    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->l(I)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/cr;->a(J)Lcom/twitter/android/timeline/cq$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 13
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/cr;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
