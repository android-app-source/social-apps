.class public abstract Lcom/twitter/android/timeline/bk$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/bk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ITEM:",
        "Lcom/twitter/android/timeline/bk;",
        "B:",
        "Lcom/twitter/android/timeline/bk$a",
        "<TITEM;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TITEM;>;"
    }
.end annotation


# instance fields
.field final b:J

.field c:Lcom/twitter/model/timeline/r;

.field d:Lcom/twitter/android/timeline/bg;

.field e:Lcha;

.field f:Ljava/lang/String;

.field g:Lcom/twitter/model/core/TwitterSocialProof;


# direct methods
.method protected constructor <init>(J)V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 232
    iput-wide p1, p0, Lcom/twitter/android/timeline/bk$a;->b:J

    .line 233
    return-void
.end method


# virtual methods
.method public a(Lcha;)Lcom/twitter/android/timeline/bk$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcha;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 255
    iput-object p1, p0, Lcom/twitter/android/timeline/bk$a;->e:Lcha;

    .line 256
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk$a;

    return-object v0
.end method

.method public a(Lcom/twitter/android/timeline/bg;)Lcom/twitter/android/timeline/bk$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/timeline/bg;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 249
    iput-object p1, p0, Lcom/twitter/android/timeline/bk$a;->d:Lcom/twitter/android/timeline/bg;

    .line 250
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/android/timeline/bk$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/TwitterSocialProof;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 243
    iput-object p1, p0, Lcom/twitter/android/timeline/bk$a;->g:Lcom/twitter/model/core/TwitterSocialProof;

    .line 244
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/timeline/r;)Lcom/twitter/android/timeline/bk$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/r;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 237
    iput-object p1, p0, Lcom/twitter/android/timeline/bk$a;->c:Lcom/twitter/model/timeline/r;

    .line 238
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk$a;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/timeline/bk$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 261
    iput-object p1, p0, Lcom/twitter/android/timeline/bk$a;->f:Ljava/lang/String;

    .line 262
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk$a;

    return-object v0
.end method
