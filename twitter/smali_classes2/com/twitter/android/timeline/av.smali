.class public Lcom/twitter/android/timeline/av;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 58
    if-eqz p2, :cond_1

    .line 59
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 60
    invoke-static {v0}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 61
    const-string/jumbo v0, "state_impressed_suggested_moments_modules"

    invoke-static {p2, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 63
    const-string/jumbo v2, "state_impressed_suggested_moments"

    invoke-static {p2, v2, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 66
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/twitter/android/timeline/av;->b:Ljava/util/Set;

    .line 68
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/twitter/android/timeline/av;->c:Ljava/util/Set;

    .line 73
    :goto_1
    return-void

    .line 56
    :cond_0
    new-instance p1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    goto :goto_0

    .line 70
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/av;->b:Ljava/util/Set;

    .line 71
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/av;->c:Ljava/util/Set;

    goto :goto_1
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/timeline/at;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 167
    iget-object v0, p1, Lcom/twitter/android/timeline/at;->a:Lcom/twitter/model/moments/v$c;

    .line 168
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p1, Lcom/twitter/android/timeline/at;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/model/moments/v$c;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/android/widget/as$a;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p1, Lcom/twitter/android/widget/as$a;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/android/widget/as$a;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/v$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p1, Lcom/twitter/model/moments/v$b;->c:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/moments/v$b;->c:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/twitter/android/timeline/at;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p1, Lcom/twitter/android/timeline/at;->e:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/timeline/at;->e:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/twitter/android/timeline/at;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 216
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-object v1, p1, Lcom/twitter/android/timeline/at;->a:Lcom/twitter/model/moments/v$c;

    iget-object v1, v1, Lcom/twitter/model/moments/v$c;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    .line 217
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/android/timeline/at;->b:J

    .line 218
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 220
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 221
    iput p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 222
    iget-object v1, p1, Lcom/twitter/android/timeline/at;->e:Lcom/twitter/model/timeline/r;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 223
    return-object v0
.end method

.method private c(Lcom/twitter/android/widget/as$a;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 229
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-wide v2, p1, Lcom/twitter/android/widget/as$a;->b:J

    .line 231
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 233
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 234
    iput p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 236
    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 83
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 84
    invoke-static {v0}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 85
    const-string/jumbo v1, "state_impressed_suggested_moments_modules"

    iget-object v2, p0, Lcom/twitter/android/timeline/av;->b:Ljava/util/Set;

    invoke-static {p1, v1, v2, v0}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 87
    const-string/jumbo v1, "state_impressed_suggested_moments"

    iget-object v2, p0, Lcom/twitter/android/timeline/av;->c:Ljava/util/Set;

    invoke-static {p1, v1, v2, v0}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 89
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/at;I)V
    .locals 6

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/av;->a(Lcom/twitter/android/timeline/at;)Ljava/lang/String;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/twitter/android/timeline/av;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/av;->c(Lcom/twitter/android/timeline/at;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 133
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/av;->b(Lcom/twitter/android/timeline/at;)Ljava/lang/String;

    move-result-object v0

    .line 134
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 135
    invoke-direct {p0}, Lcom/twitter/android/timeline/av;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    const/4 v0, 0x3

    const-string/jumbo v4, "moment"

    aput-object v4, v3, v0

    const/4 v0, 0x4

    const-string/jumbo v4, "results"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 136
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 137
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 134
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 139
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/ax;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 99
    iget-object v0, p0, Lcom/twitter/android/timeline/av;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/twitter/android/timeline/ax;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 101
    iget-object v4, p1, Lcom/twitter/android/timeline/ax;->a:Lcbi;

    .line 102
    invoke-virtual {v4}, Lcbi;->be_()I

    move-result v5

    move v1, v2

    .line 103
    :goto_0
    if-ge v1, v5, :cond_0

    .line 104
    invoke-virtual {v4, v1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/at;

    .line 105
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/timeline/av;->c(Lcom/twitter/android/timeline/at;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 109
    iput p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 110
    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p1, Lcom/twitter/android/timeline/ax;->b:Lcom/twitter/model/moments/v$b;

    invoke-direct {p0, v0}, Lcom/twitter/android/timeline/av;->a(Lcom/twitter/model/moments/v$b;)Ljava/lang/String;

    move-result-object v0

    .line 113
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    .line 114
    invoke-direct {p0}, Lcom/twitter/android/timeline/av;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const/4 v5, 0x0

    aput-object v5, v4, v2

    const/4 v2, 0x2

    aput-object v0, v4, v2

    const/4 v0, 0x3

    const-string/jumbo v2, "carousel"

    aput-object v2, v4, v0

    const/4 v0, 0x4

    const-string/jumbo v2, "impression"

    aput-object v2, v4, v0

    invoke-virtual {v1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 115
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 116
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 113
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 118
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/android/widget/as$a;I)V
    .locals 6

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/av;->a(Lcom/twitter/android/widget/as$a;)Ljava/lang/String;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/twitter/android/timeline/av;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/av;->c(Lcom/twitter/android/widget/as$a;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 153
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/av;->b(Lcom/twitter/android/timeline/at;)Ljava/lang/String;

    move-result-object v0

    .line 154
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 155
    invoke-direct {p0}, Lcom/twitter/android/timeline/av;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    const/4 v0, 0x3

    const-string/jumbo v4, "pivot"

    aput-object v4, v3, v0

    const/4 v0, 0x4

    const-string/jumbo v4, "results"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 156
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 157
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 154
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 159
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/android/timeline/at;I)V
    .locals 6

    .prologue
    .line 187
    .line 188
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/av;->c(Lcom/twitter/android/timeline/at;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 189
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/av;->b(Lcom/twitter/android/timeline/at;)Ljava/lang/String;

    move-result-object v0

    .line 190
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 191
    invoke-direct {p0}, Lcom/twitter/android/timeline/av;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    const/4 v0, 0x3

    const-string/jumbo v4, "moment"

    aput-object v4, v3, v0

    const/4 v0, 0x4

    const-string/jumbo v4, "click"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 192
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 193
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 190
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 194
    return-void
.end method

.method public b(Lcom/twitter/android/widget/as$a;I)V
    .locals 6

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/av;->c(Lcom/twitter/android/widget/as$a;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 206
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/av;->b(Lcom/twitter/android/timeline/at;)Ljava/lang/String;

    move-result-object v0

    .line 207
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 208
    invoke-direct {p0}, Lcom/twitter/android/timeline/av;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    const/4 v0, 0x3

    const-string/jumbo v4, "pivot"

    aput-object v4, v3, v0

    const/4 v0, 0x4

    const-string/jumbo v4, "click"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/timeline/av;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 209
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 210
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 207
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 211
    return-void
.end method
