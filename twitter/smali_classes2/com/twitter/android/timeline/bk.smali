.class public abstract Lcom/twitter/android/timeline/bk;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/bk$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/bg;

.field public final d:J

.field public final e:Lcom/twitter/model/timeline/r;

.field public final f:Lcha;

.field public final g:Ljava/lang/String;

.field public final h:Lcom/twitter/model/core/TwitterSocialProof;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/timeline/bk$a;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iget-object v0, p1, Lcom/twitter/android/timeline/bk$a;->d:Lcom/twitter/android/timeline/bg;

    iput-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    .line 76
    iget-wide v0, p1, Lcom/twitter/android/timeline/bk$a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/timeline/bk;->d:J

    .line 77
    iget-object v0, p1, Lcom/twitter/android/timeline/bk$a;->c:Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    .line 78
    iget-object v0, p1, Lcom/twitter/android/timeline/bk$a;->e:Lcha;

    iput-object v0, p0, Lcom/twitter/android/timeline/bk;->f:Lcha;

    .line 79
    iget-object v0, p1, Lcom/twitter/android/timeline/bk$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/bk;->g:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lcom/twitter/android/timeline/bk$a;->g:Lcom/twitter/model/core/TwitterSocialProof;

    iput-object v0, p0, Lcom/twitter/android/timeline/bk;->h:Lcom/twitter/model/core/TwitterSocialProof;

    .line 81
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 6

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v1

    .line 172
    const-string/jumbo v2, "<div><b>Entity ID:</b> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/twitter/android/timeline/bg;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Entity Group ID:</b> "

    .line 173
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/twitter/android/timeline/bg;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Sort Index:</b> "

    .line 174
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v1, Lcom/twitter/android/timeline/bg;->b:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Timeline Owner ID:</b> "

    .line 175
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v1, Lcom/twitter/android/timeline/bg;->i:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Timeline Type:</b> "

    .line 176
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/twitter/android/timeline/bg;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Timeline Tag:</b> "

    .line 177
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Timeline Chunk ID:</b> "

    .line 178
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v1, Lcom/twitter/android/timeline/bg;->f:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Entity Type:</b> "

    .line 179
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/twitter/android/timeline/bg;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</div>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div><b>Data Type:</b> "

    .line 180
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lcom/twitter/android/timeline/bg;->e:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</div>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<div><b>Position:</b> "

    .line 181
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</div>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    iget-object v1, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    if-eqz v1, :cond_0

    .line 183
    const-string/jumbo v1, "<div><b>Scribe Info:</b></div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<div><b>Suggestion Type:</b> "

    .line 184
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</div>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<div><b>Type ID:</b> "

    .line 185
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</div>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<div><b>Controller Data:</b> "

    .line 186
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</div>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<div><b>Source Data:</b> "

    .line 187
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</div>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<div><b>Scribe Component:</b> "

    .line 188
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</div>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/timeline/bk;->a(Ljava/lang/StringBuilder;)V

    .line 191
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    if-nez v0, :cond_0

    .line 144
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 148
    :goto_0
    return-object v0

    .line 146
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 147
    iget-object v1, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 148
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->d([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bk;->bc_()Lcbi;

    move-result-object v0

    invoke-virtual {v0}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 105
    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/timeline/bk;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method

.method protected bc_()Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<+",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {}, Lcom/twitter/android/timeline/bl;->bd_()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/twitter/android/timeline/bg;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "getEntityInfo should only be called for rich or URT timelines"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    iget-object v0, v0, Lcom/twitter/android/timeline/bg;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "unspecified"

    goto :goto_0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    iget-wide v0, v0, Lcom/twitter/android/timeline/bg;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->a:Lcom/twitter/android/timeline/bg;

    iget-object v0, v0, Lcom/twitter/android/timeline/bg;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "unspecified"

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bk;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/timeline/bk;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    const/4 v0, 0x2

    .line 210
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
