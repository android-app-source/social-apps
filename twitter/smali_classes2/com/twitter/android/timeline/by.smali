.class public Lcom/twitter/android/timeline/by;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/co;",
        "Lcom/twitter/android/timeline/co$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 16
    check-cast p2, Lcom/twitter/android/timeline/co$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/by;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/co$a;)Lcom/twitter/android/timeline/co$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/co$a;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/timeline/co$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/co$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/co$a;)Lcom/twitter/android/timeline/co$a;
    .locals 6

    .prologue
    .line 34
    sget v0, Lbue;->O:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 35
    sget v0, Lbue;->k:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    .line 37
    new-instance v1, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v1}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    sget v3, Lbue;->G:I

    .line 38
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v3, Lbue;->I:I

    .line 39
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v3, Lbue;->H:I

    .line 40
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v3, Lbue;->J:I

    .line 41
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v3, Lbue;->K:I

    .line 42
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v3, Lbue;->L:I

    .line 43
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v3, Lbue;->M:I

    .line 44
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v3

    sget v1, Lbue;->P:I

    .line 45
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    sget-object v4, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v1, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/v;

    invoke-virtual {v3, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v3, Lbue;->N:I

    .line 47
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->b(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    .line 48
    invoke-static {v2}, Lcom/twitter/model/core/ae$a;->e(I)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/core/TwitterUser$a;->c(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    .line 49
    invoke-static {v2}, Lcom/twitter/model/core/ae$a;->d(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/core/TwitterUser$a;->b(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    .line 50
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget v0, Lbue;->as:I

    .line 52
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 51
    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcgi;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 54
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/co$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/android/timeline/co$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 19
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 20
    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/by;->a(J)Lcom/twitter/android/timeline/co$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 16
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/by;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
