.class public Lcom/twitter/android/timeline/cs;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/twitter/android/timeline/cs;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 30
    iput-object p2, p0, Lcom/twitter/android/timeline/cs;->b:Ljava/util/HashSet;

    .line 31
    return-void

    .line 29
    :cond_0
    new-instance p1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/timeline/cs;->b:Ljava/util/HashSet;

    return-object v0
.end method

.method public a(Lcom/twitter/android/timeline/cq;I)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/twitter/android/timeline/cs;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/twitter/android/timeline/cq;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 37
    iget-object v5, p1, Lcom/twitter/android/timeline/cq;->a:Lcbi;

    .line 38
    iget-object v0, p1, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v6, v0, Lcom/twitter/model/timeline/at$a;->g:Ljava/util/Map;

    .line 39
    invoke-virtual {v5}, Lcbi;->be_()I

    move-result v7

    move v2, v3

    .line 40
    :goto_0
    if-ge v2, v7, :cond_0

    .line 41
    invoke-virtual {v5, v2}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/co;

    iget-object v0, v0, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    .line 42
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v8

    .line 43
    iput v2, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 44
    iput p2, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 45
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    iput-object v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 46
    invoke-virtual {v4, v8}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 40
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/timeline/cs;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/timeline/cs;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 50
    :goto_1
    iget-object v2, p1, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v2, v2, Lcom/twitter/model/timeline/at$a;->e:Lcom/twitter/model/timeline/r;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v2, v2, Lcom/twitter/model/timeline/at$a;->e:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 52
    :goto_2
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    aput-object v0, v6, v3

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v2, v6, v0

    const/4 v0, 0x3

    aput-object v1, v6, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "impression"

    aput-object v1, v6, v0

    .line 53
    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/timeline/cs;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 55
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 52
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 57
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 49
    goto :goto_1

    :cond_3
    move-object v2, v1

    .line 50
    goto :goto_2
.end method
