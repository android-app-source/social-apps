.class public Lcom/twitter/android/timeline/bg;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/bg$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:J

.field public final g:I

.field public final h:I

.field public final i:J

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:Z

.field public final u:I


# direct methods
.method constructor <init>(Lcom/twitter/android/timeline/bg$a;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iget-object v0, p1, Lcom/twitter/android/timeline/bg$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/bg;->a:Ljava/lang/String;

    .line 83
    iget-wide v0, p1, Lcom/twitter/android/timeline/bg$a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/timeline/bg;->b:J

    .line 84
    iget-object v0, p1, Lcom/twitter/android/timeline/bg$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/bg;->c:Ljava/lang/String;

    .line 85
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->d:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->d:I

    .line 86
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->e:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->e:I

    .line 87
    iget-wide v0, p1, Lcom/twitter/android/timeline/bg$a;->f:J

    iput-wide v0, p0, Lcom/twitter/android/timeline/bg;->f:J

    .line 88
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->h:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->h:I

    .line 89
    iget-boolean v0, p1, Lcom/twitter/android/timeline/bg$a;->i:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/bg;->k:Z

    .line 90
    iget-boolean v0, p1, Lcom/twitter/android/timeline/bg$a;->j:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/bg;->l:Z

    .line 91
    iget-boolean v0, p1, Lcom/twitter/android/timeline/bg$a;->k:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/bg;->m:Z

    .line 92
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->m:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->o:I

    .line 93
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->n:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->p:I

    .line 94
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->o:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->q:I

    .line 95
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->q:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->s:I

    .line 96
    iget-boolean v0, p1, Lcom/twitter/android/timeline/bg$a;->r:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/bg;->t:Z

    .line 97
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->s:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->u:I

    .line 98
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->p:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->r:I

    .line 99
    iget v0, p1, Lcom/twitter/android/timeline/bg$a;->g:I

    iput v0, p0, Lcom/twitter/android/timeline/bg;->g:I

    .line 100
    iget-object v0, p1, Lcom/twitter/android/timeline/bg$a;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    .line 101
    iget-wide v0, p1, Lcom/twitter/android/timeline/bg$a;->u:J

    iput-wide v0, p0, Lcom/twitter/android/timeline/bg;->i:J

    .line 102
    iget-boolean v0, p1, Lcom/twitter/android/timeline/bg$a;->l:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/bg;->n:Z

    .line 103
    return-void
.end method
