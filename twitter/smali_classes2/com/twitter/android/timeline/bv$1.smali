.class Lcom/twitter/android/timeline/bv$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/timeline/bv;->a(Landroid/content/Context;Lcom/twitter/android/timeline/cd;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/TweetCarouselView;

.field final synthetic b:Lcom/twitter/android/timeline/bv;


# direct methods
.method constructor <init>(Lcom/twitter/android/timeline/bv;Lcom/twitter/android/widget/TweetCarouselView;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/android/timeline/bv$1;->b:Lcom/twitter/android/timeline/bv;

    iput-object p2, p0, Lcom/twitter/android/timeline/bv$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/timeline/bv$1;->b:Lcom/twitter/android/timeline/bv;

    invoke-static {v0}, Lcom/twitter/android/timeline/bv;->a(Lcom/twitter/android/timeline/bv;)Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/timeline/bv$1;->b:Lcom/twitter/android/timeline/bv;

    iget-object v2, p0, Lcom/twitter/android/timeline/bv$1;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-virtual {v2}, Lcom/twitter/android/widget/TweetCarouselView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/android/timeline/bv;->a(Lcom/twitter/android/timeline/bv;Lcom/twitter/model/core/Tweet;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 80
    return-void
.end method
