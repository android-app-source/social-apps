.class Lcom/twitter/android/timeline/bv$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/timeline/bv;->a(Landroid/content/Context;Lcom/twitter/android/timeline/cd;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/TweetCarouselView;

.field final synthetic b:Lcom/twitter/android/timeline/cd;

.field final synthetic c:Lcom/twitter/android/timeline/bv;


# direct methods
.method constructor <init>(Lcom/twitter/android/timeline/bv;Lcom/twitter/android/widget/TweetCarouselView;Lcom/twitter/android/timeline/cd;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/twitter/android/timeline/bv$2;->c:Lcom/twitter/android/timeline/bv;

    iput-object p2, p0, Lcom/twitter/android/timeline/bv$2;->a:Lcom/twitter/android/widget/TweetCarouselView;

    iput-object p3, p0, Lcom/twitter/android/timeline/bv$2;->b:Lcom/twitter/android/timeline/cd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/timeline/bv$2;->a:Lcom/twitter/android/widget/TweetCarouselView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetCarouselView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 89
    iget-object v1, p0, Lcom/twitter/android/timeline/bv$2;->c:Lcom/twitter/android/timeline/bv;

    invoke-static {v1}, Lcom/twitter/android/timeline/bv;->b(Lcom/twitter/android/timeline/bv;)Lcom/twitter/android/ck;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/ck;->b(Lcom/twitter/model/core/Tweet;)V

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/timeline/bv$2;->c:Lcom/twitter/android/timeline/bv;

    invoke-static {v1}, Lcom/twitter/android/timeline/bv;->c(Lcom/twitter/android/timeline/bv;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/bv$2;->c:Lcom/twitter/android/timeline/bv;

    invoke-static {v2}, Lcom/twitter/android/timeline/bv;->a(Lcom/twitter/android/timeline/bv;)Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/timeline/bv$2;->c:Lcom/twitter/android/timeline/bv;

    .line 92
    invoke-static {v3}, Lcom/twitter/android/timeline/bv;->b(Lcom/twitter/android/timeline/bv;)Lcom/twitter/android/ck;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/timeline/bv$2;->b:Lcom/twitter/android/timeline/cd;

    iget-object v5, p0, Lcom/twitter/android/timeline/bv$2;->c:Lcom/twitter/android/timeline/bv;

    invoke-static {v5}, Lcom/twitter/android/timeline/bv;->d(Lcom/twitter/android/timeline/bv;)Lcom/twitter/ui/view/h;

    move-result-object v5

    iget-boolean v5, v5, Lcom/twitter/ui/view/h;->f:Z

    iget-object v6, p0, Lcom/twitter/android/timeline/bv$2;->c:Lcom/twitter/android/timeline/bv;

    .line 93
    invoke-static {v6}, Lcom/twitter/android/timeline/bv;->d(Lcom/twitter/android/timeline/bv;)Lcom/twitter/ui/view/h;

    move-result-object v6

    iget-boolean v6, v6, Lcom/twitter/ui/view/h;->n:Z

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 91
    invoke-static/range {v0 .. v8}, Lcom/twitter/android/cl;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lbxb;Lcom/twitter/android/timeline/bk;ZZLjava/lang/String;Z)V

    .line 94
    return-void
.end method
