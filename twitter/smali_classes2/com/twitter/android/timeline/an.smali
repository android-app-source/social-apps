.class public Lcom/twitter/android/timeline/an;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/timeline/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/an$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/timeline/o;


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/an$a;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 20
    invoke-static {p1}, Lcom/twitter/android/timeline/an$a;->a(Lcom/twitter/android/timeline/an$a;)Lcom/twitter/model/timeline/o;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/o;

    iput-object v0, p0, Lcom/twitter/android/timeline/an;->a:Lcom/twitter/model/timeline/o;

    .line 21
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/an$a;Lcom/twitter/android/timeline/an$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/an;-><init>(Lcom/twitter/android/timeline/an$a;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/timeline/o;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/timeline/an;->a:Lcom/twitter/model/timeline/o;

    return-object v0
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 39
    const-string/jumbo v0, "<div><b>Metadata:</b></div>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "<div><b>Hash Code:</b> "

    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/timeline/an;->a:Lcom/twitter/model/timeline/o;

    invoke-virtual {v1}, Lcom/twitter/model/timeline/o;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "<div><b>Metadata Class:</b> "

    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/timeline/an;->a:Lcom/twitter/model/timeline/o;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    iget-object v0, p0, Lcom/twitter/android/timeline/an;->a:Lcom/twitter/model/timeline/o;

    iget-object v0, v0, Lcom/twitter/model/timeline/o;->d:Lcom/twitter/model/timeline/r;

    .line 44
    if-eqz v0, :cond_0

    .line 45
    const-string/jumbo v1, "<div><b>Scribe Info Hash Code:</b> "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 46
    invoke-virtual {v0}, Lcom/twitter/model/timeline/r;->hashCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/timeline/an;->a:Lcom/twitter/model/timeline/o;

    iget-object v0, v0, Lcom/twitter/model/timeline/o;->e:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 34
    return-void
.end method
