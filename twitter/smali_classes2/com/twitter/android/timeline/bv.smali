.class public Lcom/twitter/android/timeline/bv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/widget/e",
        "<",
        "Lcom/twitter/android/timeline/cd;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

.field private final c:Lcom/twitter/library/view/d;

.field private final d:Lcom/twitter/android/ck;

.field private final e:Lcom/twitter/model/util/FriendshipCache;

.field private final f:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private final g:Lcom/twitter/ui/view/h;


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/library/view/d;Lcom/twitter/android/ck;Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/ui/view/h;)V
    .locals 1
    .param p5    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/bv;->a:Landroid/content/Context;

    .line 44
    iput-object p1, p0, Lcom/twitter/android/timeline/bv;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 45
    iput-object p2, p0, Lcom/twitter/android/timeline/bv;->c:Lcom/twitter/library/view/d;

    .line 46
    iput-object p3, p0, Lcom/twitter/android/timeline/bv;->d:Lcom/twitter/android/ck;

    .line 47
    iput-object p4, p0, Lcom/twitter/android/timeline/bv;->e:Lcom/twitter/model/util/FriendshipCache;

    .line 48
    iput p5, p0, Lcom/twitter/android/timeline/bv;->f:I

    .line 49
    iput-object p6, p0, Lcom/twitter/android/timeline/bv;->g:Lcom/twitter/ui/view/h;

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/bv;Lcom/twitter/model/core/Tweet;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bv;->a(Lcom/twitter/model/core/Tweet;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/core/Tweet;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 123
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/timeline/bv;->a:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/RootTweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "tw"

    .line 124
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 123
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/timeline/bv;)Lcom/twitter/app/common/base/TwitterFragmentActivity;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/timeline/bv;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    return-object v0
.end method

.method private a(Lcom/twitter/android/widget/TweetCarouselView;Lcom/twitter/android/timeline/cd;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 117
    new-instance v0, Lbxy;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/android/timeline/bv;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    iget-object v3, p2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lbxy;-><init>(ZLandroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 119
    iget-object v1, p2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v1, v0}, Lcom/twitter/android/widget/TweetCarouselView;->a(Lcom/twitter/model/core/Tweet;Lbxy;)V

    .line 120
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/timeline/bv;)Lcom/twitter/android/ck;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/timeline/bv;->d:Lcom/twitter/android/ck;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/timeline/bv;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/timeline/bv;->e:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/timeline/bv;)Lcom/twitter/ui/view/h;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/timeline/bv;->g:Lcom/twitter/ui/view/h;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 54
    iget v0, p0, Lcom/twitter/android/timeline/bv;->f:I

    return v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/android/timeline/cd;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/timeline/bv;->e:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/Tweet;)V

    .line 63
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bv;->a()I

    move-result v0

    .line 64
    iget-object v1, p0, Lcom/twitter/android/timeline/bv;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 65
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetCarouselView;

    .line 67
    iget-object v1, p0, Lcom/twitter/android/timeline/bv;->c:Lcom/twitter/library/view/d;

    instance-of v1, v1, Lcom/twitter/android/ct;

    if-eqz v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/twitter/android/timeline/bv;->c:Lcom/twitter/library/view/d;

    check-cast v1, Lcom/twitter/android/ct;

    .line 69
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetCarouselView;->setTweetViewClickHandler(Lcom/twitter/android/ct;)V

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/timeline/bv;->d:Lcom/twitter/android/ck;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetCarouselView;->setTweetActionsHandler(Lcom/twitter/android/ck;)V

    .line 72
    iget-object v1, p0, Lcom/twitter/android/timeline/bv;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetCarouselView;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 74
    invoke-direct {p0, v0, p2}, Lcom/twitter/android/timeline/bv;->a(Lcom/twitter/android/widget/TweetCarouselView;Lcom/twitter/android/timeline/cd;)V

    .line 76
    new-instance v1, Lcom/twitter/android/timeline/bv$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/timeline/bv$1;-><init>(Lcom/twitter/android/timeline/bv;Lcom/twitter/android/widget/TweetCarouselView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetCarouselView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v1, 0x7f130085

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetCarouselView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 84
    new-instance v2, Lcom/twitter/android/timeline/bv$2;

    invoke-direct {v2, p0, v0, p2}, Lcom/twitter/android/timeline/bv$2;-><init>(Lcom/twitter/android/timeline/bv;Lcom/twitter/android/widget/TweetCarouselView;Lcom/twitter/android/timeline/cd;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 99
    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lcom/twitter/android/timeline/cd;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/timeline/bv;->a(Landroid/content/Context;Lcom/twitter/android/timeline/cd;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)V
    .locals 2

    .prologue
    .line 104
    check-cast p1, Lcom/twitter/android/widget/TweetCarouselView;

    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_2

    const/4 v0, 0x1

    .line 106
    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1}, Lcom/twitter/android/widget/TweetCarouselView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/bv;->a(Lcom/twitter/android/widget/TweetCarouselView;Lcom/twitter/android/timeline/cd;)V

    .line 109
    :cond_1
    return-void

    .line 105
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 28
    check-cast p2, Lcom/twitter/android/timeline/cd;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/timeline/bv;->b(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)V

    return-void
.end method

.method public b(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public synthetic b(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 28
    check-cast p2, Lcom/twitter/android/timeline/cd;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/timeline/bv;->a(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)V

    return-void
.end method
