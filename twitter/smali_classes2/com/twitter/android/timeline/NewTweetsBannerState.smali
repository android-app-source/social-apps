.class public Lcom/twitter/android/timeline/NewTweetsBannerState;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:Z

.field final b:Lcom/twitter/model/timeline/u;

.field final c:Z

.field final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/twitter/android/timeline/NewTweetsBannerState$1;

    invoke-direct {v0}, Lcom/twitter/android/timeline/NewTweetsBannerState$1;-><init>()V

    sput-object v0, Lcom/twitter/android/timeline/NewTweetsBannerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->a:Z

    .line 50
    sget-object v0, Lcom/twitter/model/timeline/u;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/u;

    iput-object v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->b:Lcom/twitter/model/timeline/u;

    .line 51
    invoke-static {p1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->c:Z

    .line 52
    invoke-static {p1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->d:Z

    .line 53
    return-void
.end method

.method public constructor <init>(ZLcom/twitter/model/timeline/u;ZZ)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean p1, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->a:Z

    .line 43
    iput-object p2, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->b:Lcom/twitter/model/timeline/u;

    .line 44
    iput-boolean p3, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->c:Z

    .line 45
    iput-boolean p4, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->d:Z

    .line 46
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->a:Z

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Z)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->b:Lcom/twitter/model/timeline/u;

    sget-object v1, Lcom/twitter/model/timeline/u;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 64
    iget-boolean v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->c:Z

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Z)V

    .line 65
    iget-boolean v0, p0, Lcom/twitter/android/timeline/NewTweetsBannerState;->d:Z

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Z)V

    .line 66
    return-void
.end method
