.class public Lcom/twitter/android/timeline/as;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/f$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/widget/f$a",
        "<",
        "Lcom/twitter/android/timeline/at;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/av;


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/av;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/twitter/android/timeline/as;->a:Lcom/twitter/android/timeline/av;

    .line 15
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/at;I)V
    .locals 1

    .prologue
    .line 19
    instance-of v0, p1, Lcom/twitter/android/widget/as$a;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/twitter/android/timeline/as;->a:Lcom/twitter/android/timeline/av;

    check-cast p1, Lcom/twitter/android/widget/as$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/timeline/av;->a(Lcom/twitter/android/widget/as$a;I)V

    .line 24
    :goto_0
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/timeline/as;->a:Lcom/twitter/android/timeline/av;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/timeline/av;->a(Lcom/twitter/android/timeline/at;I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/timeline/at;Z)V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/twitter/android/timeline/at;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/as;->a(Lcom/twitter/android/timeline/at;I)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/twitter/android/timeline/at;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/as;->a(Lcom/twitter/android/timeline/at;Z)V

    return-void
.end method

.method public a(Lcom/twitter/android/timeline/at;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lcom/twitter/android/timeline/at;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/as;->a(Lcom/twitter/android/timeline/at;)Z

    move-result v0

    return v0
.end method
