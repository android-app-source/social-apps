.class public final Lcom/twitter/android/timeline/ax$a;
.super Lcom/twitter/android/timeline/bk$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/ax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bk$a",
        "<",
        "Lcom/twitter/android/timeline/ax;",
        "Lcom/twitter/android/timeline/ax$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/at;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/twitter/model/moments/v$b;

.field private i:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/bk$a;-><init>(J)V

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/ax$a;)Lcbi;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/timeline/ax$a;->a:Lcbi;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/timeline/ax$a;)Lcom/twitter/model/moments/v$b;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/timeline/ax$a;->h:Lcom/twitter/model/moments/v$b;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/timeline/ax$a;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/twitter/android/timeline/ax$a;->i:J

    return-wide v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/twitter/android/timeline/bk$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/ax$a;->a:Lcbi;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/ax$a;->h:Lcom/twitter/model/moments/v$b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/android/timeline/ax$a;
    .locals 1

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/twitter/android/timeline/ax$a;->i:J

    .line 62
    return-object p0
.end method

.method public a(Lcbi;)Lcom/twitter/android/timeline/ax$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/at;",
            ">;)",
            "Lcom/twitter/android/timeline/ax$a;"
        }
    .end annotation

    .prologue
    .line 49
    iput-object p1, p0, Lcom/twitter/android/timeline/ax$a;->a:Lcbi;

    .line 50
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/v$b;)Lcom/twitter/android/timeline/ax$a;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/android/timeline/ax$a;->h:Lcom/twitter/model/moments/v$b;

    .line 56
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/timeline/ax$a;->e()Lcom/twitter/android/timeline/ax;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/android/timeline/ax;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/twitter/android/timeline/ax;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/timeline/ax;-><init>(Lcom/twitter/android/timeline/ax$a;Lcom/twitter/android/timeline/ax$1;)V

    return-object v0
.end method
