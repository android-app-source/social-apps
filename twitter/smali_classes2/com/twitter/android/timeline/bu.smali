.class public Lcom/twitter/android/timeline/bu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/f$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/widget/f$a",
        "<",
        "Lcom/twitter/android/timeline/cd;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/cd;I)V
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p1, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 20
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 21
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/cd;Z)V
    .locals 2

    .prologue
    .line 25
    if-eqz p2, :cond_0

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->ar:Lcom/twitter/library/api/PromotedEvent;

    .line 28
    :goto_0
    iget-object v1, p1, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 29
    return-void

    .line 25
    :cond_0
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->as:Lcom/twitter/library/api/PromotedEvent;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/twitter/android/timeline/cd;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bu;->a(Lcom/twitter/android/timeline/cd;I)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/twitter/android/timeline/cd;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bu;->a(Lcom/twitter/android/timeline/cd;Z)V

    return-void
.end method

.method public a(Lcom/twitter/android/timeline/cd;)Z
    .locals 1

    .prologue
    .line 14
    iget-object v0, p1, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lcom/twitter/android/timeline/cd;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bu;->a(Lcom/twitter/android/timeline/cd;)Z

    move-result v0

    return v0
.end method
