.class public Lcom/twitter/android/timeline/ab;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/ac;",
        "Lcom/twitter/android/timeline/ac$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/ac$a;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/android/timeline/ac$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/ac$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ac$a;)Lcom/twitter/android/timeline/ac$a;
    .locals 2

    .prologue
    .line 34
    sget v0, Lbue;->Q:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/x;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/x;

    .line 36
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/ac$a;->a(Lcom/twitter/model/moments/x;)Lcom/twitter/android/timeline/ac$a;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 13
    check-cast p2, Lcom/twitter/android/timeline/ac$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ab;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/ac$a;)Lcom/twitter/android/timeline/ac$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    .line 19
    sget v0, Lbue;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 20
    sget v1, Lbue;->e:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    .line 21
    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->p(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 20
    :goto_0
    return v0

    .line 21
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ab;->a(J)Lcom/twitter/android/timeline/ac$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 13
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ab;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
