.class public Lcom/twitter/android/timeline/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/twitter/android/timeline/be;

.field private b:Landroid/support/v4/app/FragmentManager;

.field private c:Landroid/content/Context;

.field private d:Lcom/twitter/library/client/Session;

.field private e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private f:Lcom/twitter/library/client/p;


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/be;Landroid/support/v4/app/FragmentManager;Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/client/p;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p2, p0, Lcom/twitter/android/timeline/r;->b:Landroid/support/v4/app/FragmentManager;

    .line 58
    iput-object p3, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    .line 59
    iput-object p1, p0, Lcom/twitter/android/timeline/r;->a:Lcom/twitter/android/timeline/be;

    .line 60
    iput-object p4, p0, Lcom/twitter/android/timeline/r;->d:Lcom/twitter/library/client/Session;

    .line 61
    iput-object p5, p0, Lcom/twitter/android/timeline/r;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 62
    iput-object p6, p0, Lcom/twitter/android/timeline/r;->f:Lcom/twitter/library/client/p;

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/r;)Lcom/twitter/android/timeline/be;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/timeline/r;->a:Lcom/twitter/android/timeline/be;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/timeline/r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/r;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/r;Ljava/lang/String;Lcom/twitter/android/timeline/az;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/timeline/r;->a(Ljava/lang/String;Lcom/twitter/android/timeline/az;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v1

    const/4 v2, 0x0

    .line 135
    invoke-virtual {v1, p1, v2}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    .line 136
    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 134
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 137
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/android/timeline/az;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 128
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/timeline/r;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/timeline/r;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 129
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    iget-object v5, p2, Lcom/twitter/android/timeline/az;->e:Lcom/twitter/model/timeline/r;

    iget-object v5, v5, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    invoke-static {v3, v4, v5, p3, p1}, Lcom/twitter/analytics/model/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v3

    .line 130
    invoke-virtual {v3}, Lcom/twitter/analytics/model/a;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 129
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 128
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 131
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/timeline/r;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/timeline/r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/r;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 140
    iget-object v1, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    new-instance v0, Lcom/twitter/app/dm/h$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/h$a;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/h$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 142
    invoke-virtual {v0, v4}, Lcom/twitter/app/dm/h$a;->g(Z)Lcom/twitter/app/dm/h$a;

    move-result-object v0

    .line 143
    invoke-virtual {v0, v4}, Lcom/twitter/app/dm/h$a;->a(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 144
    invoke-virtual {v0}, Lcom/twitter/app/dm/h$a;->e()Lcom/twitter/app/dm/h;

    move-result-object v0

    .line 140
    invoke-static {v1, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/h;)Landroid/content/Intent;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 146
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/timeline/r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/r;->c(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 150
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/k;)V
    .locals 4

    .prologue
    .line 121
    new-instance v0, Lajb;

    iget-object v1, p0, Lcom/twitter/android/timeline/r;->c:Landroid/content/Context;

    new-instance v2, Lcom/twitter/library/service/v;

    iget-object v3, p0, Lcom/twitter/android/timeline/r;->d:Lcom/twitter/library/client/Session;

    invoke-direct {v2, v3}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {v0, v1, v2}, Lajb;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 123
    iget-object v1, p0, Lcom/twitter/android/timeline/r;->f:Lcom/twitter/library/client/p;

    iget-object v2, p2, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    const/4 v3, 0x0

    .line 124
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 123
    invoke-virtual {v0, p1, v2, v3}, Lajb;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/Boolean;)Lcom/twitter/library/service/s;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 125
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/timeline/r;->b:Landroid/support/v4/app/FragmentManager;

    if-eqz v0, :cond_0

    .line 68
    const v0, 0x7f13007e

    .line 69
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/az;

    .line 70
    const-string/jumbo v1, "click"

    const-string/jumbo v2, "caret"

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/android/timeline/r;->a(Ljava/lang/String;Lcom/twitter/android/timeline/az;Ljava/lang/String;)V

    .line 71
    new-instance v1, Lcom/twitter/android/widget/aj$b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const/4 v2, 0x4

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 72
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->a([I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    .line 74
    invoke-virtual {v1}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/timeline/r$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/timeline/r$1;-><init>(Lcom/twitter/android/timeline/r;Lcom/twitter/android/timeline/az;)V

    .line 75
    invoke-virtual {v1, v2}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/timeline/r;->b:Landroid/support/v4/app/FragmentManager;

    .line 114
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 116
    :cond_0
    return-void

    .line 71
    nop

    :array_0
    .array-data 4
        0x7f0a0551
        0x7f0a07fd
        0x7f0a08a6
        0x7f0a0550
    .end array-data
.end method
