.class public Lcom/twitter/android/timeline/bh;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/android/timeline/bg;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bh;->b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bg;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 21
    invoke-static {p1}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bg;
    .locals 29

    .prologue
    .line 27
    sget v4, Lbue;->c:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 28
    sget v4, Lbue;->h:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 29
    sget v4, Lbue;->ar:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 30
    sget v4, Lbue;->d:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 31
    sget v4, Lbue;->e:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 32
    sget v4, Lbue;->F:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 33
    sget v4, Lbue;->au:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 35
    sget v4, Lbue;->ap:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 36
    sget v4, Lbue;->at:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 38
    sget v4, Lbue;->g:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 40
    sget v4, Lbue;->al:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 41
    sget v4, Lbue;->am:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 42
    sget v4, Lbue;->ao:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 43
    sget v4, Lbue;->j:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v4, v0, :cond_0

    const/4 v4, 0x1

    .line 45
    :goto_0
    sget v21, Lbue;->an:I

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 47
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v22

    .line 48
    const-string/jumbo v23, "requires_top_divider"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v23

    .line 49
    const-string/jumbo v24, "should_hide_borders"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v24

    .line 50
    const-string/jumbo v25, "entity_start"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v25

    .line 51
    const-string/jumbo v26, "entity_end"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v26

    .line 53
    const-string/jumbo v27, "data_type_source_start"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v27

    .line 54
    const-string/jumbo v28, "data_type_source_end"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 56
    new-instance v28, Lcom/twitter/android/timeline/bg$a;

    invoke-direct/range {v28 .. v28}, Lcom/twitter/android/timeline/bg$a;-><init>()V

    .line 57
    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Lcom/twitter/android/timeline/bg$a;->a(Ljava/lang/String;)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 58
    invoke-virtual {v5, v6, v7}, Lcom/twitter/android/timeline/bg$a;->a(J)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 59
    invoke-virtual {v5, v8}, Lcom/twitter/android/timeline/bg$a;->b(Ljava/lang/String;)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 60
    invoke-virtual {v5, v9}, Lcom/twitter/android/timeline/bg$a;->a(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 61
    invoke-virtual {v5, v10}, Lcom/twitter/android/timeline/bg$a;->b(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 62
    invoke-virtual {v5, v11}, Lcom/twitter/android/timeline/bg$a;->d(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 63
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/twitter/android/timeline/bg$a;->c(Ljava/lang/String;)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 64
    invoke-virtual {v5, v14, v15}, Lcom/twitter/android/timeline/bg$a;->c(J)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 65
    invoke-virtual {v5, v12, v13}, Lcom/twitter/android/timeline/bg$a;->b(J)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 66
    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/twitter/android/timeline/bg$a;->c(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 67
    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/twitter/android/timeline/bg$a;->g(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 68
    move/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/twitter/android/timeline/bg$a;->i(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 69
    move/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/twitter/android/timeline/bg$a;->h(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v5

    .line 70
    invoke-virtual {v5, v4}, Lcom/twitter/android/timeline/bg$a;->a(Z)Lcom/twitter/android/timeline/bg$a;

    move-result-object v4

    .line 71
    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/twitter/android/timeline/bg$a;->j(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v4

    .line 72
    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/twitter/android/timeline/bg$a;->a(ZZZZ)Lcom/twitter/android/timeline/bg$a;

    move-result-object v4

    .line 73
    move/from16 v0, v27

    invoke-virtual {v4, v0}, Lcom/twitter/android/timeline/bg$a;->e(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v4

    .line 74
    move/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/twitter/android/timeline/bg$a;->f(I)Lcom/twitter/android/timeline/bg$a;

    move-result-object v4

    .line 75
    invoke-virtual {v4}, Lcom/twitter/android/timeline/bg$a;->q()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/timeline/bg;

    .line 56
    return-object v4

    .line 43
    :cond_0
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 17
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bh;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
