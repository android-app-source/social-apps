.class public Lcom/twitter/android/timeline/ch;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lckb",
        "<",
        "Lcom/twitter/android/timeline/cg;",
        "Lcom/twitter/android/trends/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/twitter/android/client/n;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Lcom/twitter/android/client/n;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lckb;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/timeline/ch;->a:Landroid/view/LayoutInflater;

    .line 23
    iput-object p2, p0, Lcom/twitter/android/timeline/ch;->b:Landroid/content/res/Resources;

    .line 24
    iput-object p3, p0, Lcom/twitter/android/timeline/ch;->c:Lcom/twitter/android/client/n;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/ch;)Lcom/twitter/android/client/n;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/twitter/android/timeline/ch;->c:Lcom/twitter/android/client/n;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/android/trends/h;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/timeline/ch;->a:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/twitter/android/timeline/ch;->b:Landroid/content/res/Resources;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/trends/h;->a(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/view/ViewGroup;)Lcom/twitter/android/trends/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/twitter/android/trends/h;

    check-cast p2, Lcom/twitter/android/timeline/cg;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ch;->a(Lcom/twitter/android/trends/h;Lcom/twitter/android/timeline/cg;)V

    return-void
.end method

.method public a(Lcom/twitter/android/trends/h;Lcom/twitter/android/timeline/cg;)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/android/timeline/ch$1;

    invoke-direct {v0, p0, p2}, Lcom/twitter/android/timeline/ch$1;-><init>(Lcom/twitter/android/timeline/ch;Lcom/twitter/android/timeline/cg;)V

    .line 42
    iget-object v1, p2, Lcom/twitter/android/timeline/cg;->a:Lchy;

    invoke-virtual {p1, v1, v0}, Lcom/twitter/android/trends/h;->a(Lchy;Landroid/view/View$OnClickListener;)V

    .line 43
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/cg;)Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lcom/twitter/android/timeline/cg;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ch;->a(Lcom/twitter/android/timeline/cg;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ch;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/trends/h;

    move-result-object v0

    return-object v0
.end method
