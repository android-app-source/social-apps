.class public Lcom/twitter/android/timeline/bj;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()Lcbp;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbp",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/android/timeline/bw;

    invoke-direct {v0}, Lcom/twitter/android/timeline/bw;-><init>()V

    .line 18
    new-instance v1, Lcom/twitter/android/timeline/by;

    invoke-direct {v1}, Lcom/twitter/android/timeline/by;-><init>()V

    .line 19
    new-instance v2, Lcbo;

    new-instance v3, Lcom/twitter/android/timeline/bz;

    invoke-direct {v3}, Lcom/twitter/android/timeline/bz;-><init>()V

    const/16 v4, 0x19

    new-array v4, v4, [Lcbp;

    const/4 v5, 0x0

    new-instance v6, Lcom/twitter/android/timeline/h;

    invoke-direct {v6}, Lcom/twitter/android/timeline/h;-><init>()V

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Lcom/twitter/android/timeline/ar;

    invoke-direct {v6, v0}, Lcom/twitter/android/timeline/ar;-><init>(Lcom/twitter/android/timeline/bw;)V

    aput-object v6, v4, v5

    const/4 v5, 0x2

    new-instance v6, Lcom/twitter/android/timeline/cl;

    invoke-direct {v6, v0}, Lcom/twitter/android/timeline/cl;-><init>(Lcom/twitter/android/timeline/bw;)V

    aput-object v6, v4, v5

    const/4 v5, 0x3

    new-instance v6, Lcom/twitter/android/timeline/bx;

    invoke-direct {v6, v1}, Lcom/twitter/android/timeline/bx;-><init>(Lcom/twitter/android/timeline/by;)V

    aput-object v6, v4, v5

    const/4 v5, 0x4

    new-instance v6, Lcom/twitter/android/timeline/ao;

    invoke-direct {v6, v0}, Lcom/twitter/android/timeline/ao;-><init>(Lcom/twitter/android/timeline/bw;)V

    aput-object v6, v4, v5

    const/4 v5, 0x5

    new-instance v6, Lcom/twitter/android/timeline/am;

    invoke-direct {v6}, Lcom/twitter/android/timeline/am;-><init>()V

    aput-object v6, v4, v5

    const/4 v5, 0x6

    new-instance v6, Lcom/twitter/android/timeline/cp;

    new-instance v7, Lcom/twitter/android/timeline/ct;

    invoke-direct {v7, v1}, Lcom/twitter/android/timeline/ct;-><init>(Lcom/twitter/android/timeline/by;)V

    new-instance v8, Lcom/twitter/android/timeline/cr;

    invoke-direct {v8}, Lcom/twitter/android/timeline/cr;-><init>()V

    invoke-direct {v6, v7, v8}, Lcom/twitter/android/timeline/cp;-><init>(Lcom/twitter/android/timeline/ct;Lcom/twitter/android/timeline/cr;)V

    aput-object v6, v4, v5

    const/4 v5, 0x7

    new-instance v6, Lcom/twitter/android/timeline/m;

    invoke-direct {v6}, Lcom/twitter/android/timeline/m;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0x8

    new-instance v6, Lcom/twitter/android/timeline/ba;

    invoke-direct {v6}, Lcom/twitter/android/timeline/ba;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0x9

    new-instance v6, Lcom/twitter/android/timeline/ab;

    invoke-direct {v6}, Lcom/twitter/android/timeline/ab;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0xa

    new-instance v6, Lcom/twitter/android/timeline/aw;

    new-instance v7, Lcom/twitter/android/timeline/aw$a;

    invoke-direct {v7}, Lcom/twitter/android/timeline/aw$a;-><init>()V

    invoke-direct {v6, v7}, Lcom/twitter/android/timeline/aw;-><init>(Lcom/twitter/android/timeline/aw$a;)V

    aput-object v6, v4, v5

    const/16 v5, 0xb

    new-instance v6, Lcom/twitter/android/timeline/bi;

    invoke-direct {v6}, Lcom/twitter/android/timeline/bi;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0xc

    new-instance v6, Lcom/twitter/android/timeline/bp;

    invoke-direct {v6}, Lcom/twitter/android/timeline/bp;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0xd

    new-instance v6, Lcom/twitter/android/timeline/ay;

    invoke-direct {v6}, Lcom/twitter/android/timeline/ay;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0xe

    new-instance v6, Lcom/twitter/android/timeline/v;

    invoke-direct {v6}, Lcom/twitter/android/timeline/v;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0xf

    new-instance v6, Lcom/twitter/android/timeline/ce;

    invoke-direct {v6}, Lcom/twitter/android/timeline/ce;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0x10

    new-instance v6, Lcom/twitter/android/timeline/ci;

    invoke-direct {v6}, Lcom/twitter/android/timeline/ci;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0x11

    new-instance v6, Lcom/twitter/android/timeline/y;

    invoke-direct {v6}, Lcom/twitter/android/timeline/y;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0x12

    new-instance v6, Lcom/twitter/android/timeline/ck;

    invoke-direct {v6}, Lcom/twitter/android/timeline/ck;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0x13

    new-instance v6, Lcom/twitter/android/timeline/aa;

    invoke-direct {v6}, Lcom/twitter/android/timeline/aa;-><init>()V

    aput-object v6, v4, v5

    const/16 v5, 0x14

    aput-object v1, v4, v5

    const/16 v1, 0x15

    new-instance v5, Lcom/twitter/android/timeline/aj;

    invoke-direct {v5}, Lcom/twitter/android/timeline/aj;-><init>()V

    aput-object v5, v4, v1

    const/16 v1, 0x16

    new-instance v5, Lcom/twitter/android/timeline/ad;

    invoke-direct {v5}, Lcom/twitter/android/timeline/ad;-><init>()V

    aput-object v5, v4, v1

    const/16 v1, 0x17

    new-instance v5, Lcom/twitter/android/timeline/al;

    .line 50
    invoke-static {}, Lcom/twitter/library/revenue/b;->c()Z

    move-result v6

    invoke-direct {v5, v6}, Lcom/twitter/android/timeline/al;-><init>(Z)V

    aput-object v5, v4, v1

    const/16 v1, 0x18

    aput-object v0, v4, v1

    .line 19
    invoke-static {v3, v4}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Lcbo;-><init>(Ljava/util/Collection;)V

    return-object v2
.end method
