.class Lcom/twitter/android/timeline/l;
.super Lcom/twitter/android/timeline/c;
.source "Twttr"


# static fields
.field static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/twitter/android/timeline/l;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_saved_state_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/timeline/l;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/twitter/android/widget/NewItemBannerView;Lcom/twitter/android/timeline/br$a;Lcom/twitter/android/timeline/NewTweetsBannerState;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/timeline/c;-><init>(Lcom/twitter/android/widget/NewItemBannerView;Lcom/twitter/android/timeline/br$a;Lcom/twitter/android/timeline/NewTweetsBannerState;)V

    .line 28
    return-void
.end method


# virtual methods
.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/twitter/android/timeline/l;->b:Ljava/lang/String;

    return-object v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/twitter/android/timeline/c;->l()V

    .line 38
    invoke-virtual {p0}, Lcom/twitter/android/timeline/l;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/twitter/android/timeline/l;->r()V

    .line 41
    :cond_0
    return-void
.end method

.method public m()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lcom/twitter/android/timeline/c;->m()V

    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/timeline/l;->s()V

    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/timeline/l;->n()Z

    .line 48
    return-void
.end method

.method o()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    return v0
.end method
