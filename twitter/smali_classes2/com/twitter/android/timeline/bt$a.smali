.class public final Lcom/twitter/android/timeline/bt$a;
.super Lcom/twitter/android/timeline/cd$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/bt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/cd$a",
        "<",
        "Lcom/twitter/android/timeline/bt;",
        "Lcom/twitter/android/timeline/bt$a;",
        ">;"
    }
.end annotation


# instance fields
.field private i:Ljava/lang/String;

.field private j:J


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/cd;)V
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p1, Lcom/twitter/android/timeline/cd;->d:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/timeline/cd$a;-><init>(J)V

    .line 43
    invoke-virtual {p1}, Lcom/twitter/android/timeline/cd;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/timeline/bt$a;->a(Lcom/twitter/android/timeline/bg;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bt$a;

    iget-object v1, p1, Lcom/twitter/android/timeline/cd;->e:Lcom/twitter/model/timeline/r;

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/bt$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bt$a;

    iget-object v1, p1, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/bt$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/bt$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/timeline/bt$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/timeline/bt$a;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/twitter/android/timeline/bt$a;->j:J

    return-wide v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lcom/twitter/android/timeline/cd$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bt$a;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/android/timeline/bt$a;
    .locals 1

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/twitter/android/timeline/bt$a;->j:J

    .line 57
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/timeline/bt$a;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/twitter/android/timeline/bt$a;->i:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bt$a;->e()Lcom/twitter/android/timeline/bt;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/android/timeline/bt;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/twitter/android/timeline/bt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/timeline/bt;-><init>(Lcom/twitter/android/timeline/bt$a;Lcom/twitter/android/timeline/bt$1;)V

    return-object v0
.end method
