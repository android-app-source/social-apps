.class public Lcom/twitter/android/timeline/be;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/be$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Lcom/twitter/library/client/v;

.field private final d:Lcom/twitter/android/timeline/be$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/p;Lcom/twitter/library/client/v;Lcom/twitter/android/timeline/be$a;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/timeline/be;->a:Ljava/lang/ref/WeakReference;

    .line 38
    iput-object p2, p0, Lcom/twitter/android/timeline/be;->b:Lcom/twitter/library/client/p;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/timeline/be;->c:Lcom/twitter/library/client/v;

    .line 40
    iput-object p4, p0, Lcom/twitter/android/timeline/be;->d:Lcom/twitter/android/timeline/be$a;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/be;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/be;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/timeline/be;->d:Lcom/twitter/android/timeline/be$a;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/twitter/android/timeline/be;->d:Lcom/twitter/android/timeline/be$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/timeline/be$a;->a(Ljava/lang/String;)V

    .line 94
    :cond_0
    return-void
.end method

.method private b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/timeline/be;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 99
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 102
    :cond_0
    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;IIIZ)Lbwp;
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 68
    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p7

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Lbwp;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;IZII)Lbwp;

    move-result-object v0

    .line 73
    invoke-virtual {p0}, Lcom/twitter/android/timeline/be;->a()Lcom/twitter/library/service/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbwp;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    check-cast v0, Lbwp;

    .line 68
    return-object v0
.end method

.method a()Lcom/twitter/library/service/w;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/service/w",
            "<",
            "Ljava/lang/Void;",
            "Lbwp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lcom/twitter/android/timeline/be$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/timeline/be$1;-><init>(Lcom/twitter/android/timeline/be;)V

    return-object v0
.end method

.method public a(Lcom/twitter/android/timeline/bk;III)V
    .locals 9

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/twitter/android/timeline/be;->b()Landroid/content/Context;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_0

    const-string/jumbo v0, "unspecified"

    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v8, p0, Lcom/twitter/android/timeline/be;->b:Lcom/twitter/library/client/p;

    iget-object v0, p0, Lcom/twitter/android/timeline/be;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/4 v7, 0x1

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/timeline/be;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;IIIZ)Lbwp;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 50
    :cond_0
    return-void
.end method
