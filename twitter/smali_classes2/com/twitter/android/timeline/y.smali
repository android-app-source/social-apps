.class public Lcom/twitter/android/timeline/y;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/z;",
        "Lcom/twitter/android/timeline/z$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 11
    check-cast p2, Lcom/twitter/android/timeline/z$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/y;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/z$a;)Lcom/twitter/android/timeline/z$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/z$a;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/timeline/z$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/z$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/z$a;)Lcom/twitter/android/timeline/z$a;
    .locals 0

    .prologue
    .line 32
    return-object p2
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    .line 15
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 16
    sget v1, Lbue;->g:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 17
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 18
    invoke-static {v1}, Lcom/twitter/model/timeline/z$a;->q(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19
    invoke-static {v1}, Lcom/twitter/model/timeline/z$a;->r(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 17
    :goto_0
    return v0

    .line 19
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/y;->a(J)Lcom/twitter/android/timeline/z$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 11
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/y;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
