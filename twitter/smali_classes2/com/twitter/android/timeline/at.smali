.class public Lcom/twitter/android/timeline/at;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/at$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/moments/v$c;

.field public final b:J


# direct methods
.method protected constructor <init>(Lcom/twitter/android/timeline/at$a;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 18
    invoke-static {p1}, Lcom/twitter/android/timeline/at$a;->a(Lcom/twitter/android/timeline/at$a;)Lcom/twitter/model/moments/v$c;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/v$c;

    iput-object v0, p0, Lcom/twitter/android/timeline/at;->a:Lcom/twitter/model/moments/v$c;

    .line 19
    invoke-static {p1}, Lcom/twitter/android/timeline/at$a;->b(Lcom/twitter/android/timeline/at$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/timeline/at;->b:J

    .line 20
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/timeline/at;->a:Lcom/twitter/model/moments/v$c;

    iget-object v0, v0, Lcom/twitter/model/moments/v$c;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method
