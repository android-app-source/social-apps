.class public Lcom/twitter/android/timeline/ah;
.super Lckb$a;
.source "Twttr"


# instance fields
.field private final a:Lacq;


# direct methods
.method public constructor <init>(Lacq;)V
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p1}, Lacq;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lckb$a;-><init>(Landroid/view/View;)V

    .line 23
    iput-object p1, p0, Lcom/twitter/android/timeline/ah;->a:Lacq;

    .line 24
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/timeline/ah;
    .locals 2

    .prologue
    .line 17
    invoke-static {p0, p1}, Lacq;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lacq;

    move-result-object v0

    .line 18
    new-instance v1, Lcom/twitter/android/timeline/ah;

    invoke-direct {v1, v0}, Lcom/twitter/android/timeline/ah;-><init>(Lacq;)V

    return-object v1
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/timeline/ah;->a:Lacq;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lacq;->a(Ljava/lang/CharSequence;)V

    .line 28
    iget-object v0, p0, Lcom/twitter/android/timeline/ah;->a:Lacq;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lacq;->b(Ljava/lang/CharSequence;)V

    .line 29
    return-void
.end method
