.class public Lcom/twitter/android/timeline/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/android/timeline/s;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 32
    iput-object p2, p0, Lcom/twitter/android/timeline/s;->b:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/model/timeline/r;)V
    .locals 7

    .prologue
    .line 55
    if-eqz p2, :cond_0

    .line 56
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/timeline/s;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 57
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    iget-object v5, p2, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    const-string/jumbo v6, ""

    invoke-static {v3, v4, v5, v6, p1}, Lcom/twitter/analytics/model/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v3

    .line 58
    invoke-virtual {v3}, Lcom/twitter/analytics/model/a;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 57
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 40
    move-object v0, p1

    check-cast v0, Lcom/twitter/library/widget/LiveContentView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/LiveContentView;->getBannerPrompt()Lcom/twitter/model/timeline/b;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/timeline/b;->d:Ljava/lang/String;

    .line 41
    invoke-static {v0}, Lcom/twitter/android/livevideo/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 42
    invoke-static {v0}, Lcom/twitter/android/livevideo/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 43
    check-cast v0, Lcom/twitter/library/widget/LiveContentView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/LiveContentView;->getBannerPrompt()Lcom/twitter/model/timeline/b;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/timeline/b;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 44
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/timeline/s;->b:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/UrlInterpreterActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 46
    check-cast p1, Lcom/twitter/library/widget/LiveContentView;

    .line 47
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/LiveContentView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/LiveContentView;->getCaretView()Landroid/widget/ImageView;

    move-result-object v0

    const v2, 0x7f13007e

    .line 48
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/az;

    .line 49
    const-string/jumbo v2, "click"

    iget-object v0, v0, Lcom/twitter/android/timeline/az;->e:Lcom/twitter/model/timeline/r;

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/timeline/s;->a(Ljava/lang/String;Lcom/twitter/model/timeline/r;)V

    .line 50
    iget-object v0, p0, Lcom/twitter/android/timeline/s;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 52
    :cond_1
    return-void
.end method
