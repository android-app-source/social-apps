.class public Lcom/twitter/android/timeline/al;
.super Lcom/twitter/android/timeline/bw;
.source "Twttr"


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/android/timeline/bw;-><init>()V

    .line 25
    iput-boolean p1, p0, Lcom/twitter/android/timeline/al;->a:Z

    .line 26
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/model/core/Tweet$a;Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/twitter/android/timeline/bw;->a(Lcom/twitter/model/core/Tweet$a;Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    .line 48
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->F:Lcgi;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->F:Lcgi;

    iget-object v0, v0, Lcgi;->c:Ljava/lang/String;

    move-object v1, v0

    .line 51
    :goto_0
    if-eqz v1, :cond_0

    .line 52
    invoke-static {}, Lbwd;->c()Lbwd;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbwd;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mopub/nativeads/NativeAd;

    .line 54
    if-eqz v0, :cond_0

    .line 55
    invoke-static {v0}, Lbwh;->a(Lcom/mopub/nativeads/NativeAd;)Lbwf;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    invoke-static {v0, v1}, Lbwg;->a(Lbwf;Ljava/lang/String;)Lcax;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/model/core/Tweet$a;->a(Lcax;)Lcom/twitter/model/core/Tweet$a;

    .line 59
    invoke-interface {v0}, Lbwf;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/model/core/Tweet$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 60
    invoke-interface {v0}, Lbwf;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/model/core/Tweet$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 61
    invoke-interface {v0}, Lbwf;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/model/core/Tweet$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 62
    invoke-interface {v0}, Lbwf;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/model/core/Tweet$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 63
    invoke-interface {v0}, Lbwf;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/twitter/model/core/Tweet$a;->k:Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-virtual {p1, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/Tweet$a;

    .line 69
    :cond_0
    return-object p1

    .line 48
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method public b(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 30
    iget-boolean v1, p0, Lcom/twitter/android/timeline/al;->a:Z

    if-nez v1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 35
    :cond_1
    const/16 v1, 0x1d

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 36
    invoke-static {v1}, Lbwi;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/android/timeline/bw;->b(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 20
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/al;->b(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
