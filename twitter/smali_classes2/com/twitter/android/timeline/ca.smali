.class public Lcom/twitter/android/timeline/ca;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/ca$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcgi;

.field public i:Ljava/lang/String;

.field public j:Lcom/twitter/model/topic/e;

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:I

.field public o:I


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/ca$a;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 26
    iget-object v0, p1, Lcom/twitter/android/timeline/ca$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/ca;->a:Ljava/lang/String;

    .line 27
    iget-object v0, p1, Lcom/twitter/android/timeline/ca$a;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/ca;->b:Ljava/lang/String;

    .line 28
    iget-object v0, p1, Lcom/twitter/android/timeline/ca$a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/ca;->i:Ljava/lang/String;

    .line 29
    iget-object v0, p1, Lcom/twitter/android/timeline/ca$a;->j:Lcgi;

    iput-object v0, p0, Lcom/twitter/android/timeline/ca;->c:Lcgi;

    .line 30
    iget-object v0, p1, Lcom/twitter/android/timeline/ca$a;->k:Lcom/twitter/model/topic/e;

    iput-object v0, p0, Lcom/twitter/android/timeline/ca;->j:Lcom/twitter/model/topic/e;

    .line 31
    iget-boolean v0, p1, Lcom/twitter/android/timeline/ca$a;->l:Z

    iput-boolean v0, p0, Lcom/twitter/android/timeline/ca;->k:Z

    .line 32
    iget-object v0, p1, Lcom/twitter/android/timeline/ca$a;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/ca;->l:Ljava/lang/String;

    .line 33
    iget-object v0, p1, Lcom/twitter/android/timeline/ca$a;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/ca;->m:Ljava/lang/String;

    .line 34
    iget v0, p1, Lcom/twitter/android/timeline/ca$a;->o:I

    iput v0, p0, Lcom/twitter/android/timeline/ca;->n:I

    .line 35
    iget v0, p1, Lcom/twitter/android/timeline/ca$a;->p:I

    iput v0, p0, Lcom/twitter/android/timeline/ca;->o:I

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/ca$a;Lcom/twitter/android/timeline/ca$1;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/ca;-><init>(Lcom/twitter/android/timeline/ca$a;)V

    return-void
.end method
