.class public Lcom/twitter/android/timeline/cd;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/timeline/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/cd$b;,
        Lcom/twitter/android/timeline/cd$a;
    }
.end annotation


# instance fields
.field public final b:Lcom/twitter/model/core/Tweet;

.field public final c:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/timeline/cd$a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 27
    iget-object v0, p1, Lcom/twitter/android/timeline/cd$a;->a:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 28
    iget-object v0, p1, Lcom/twitter/android/timeline/cd$a;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/timeline/cd;->c:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string/jumbo v0, "focal"

    .line 71
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {p1, v1, v0}, Lcom/twitter/library/scribe/b;->b(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    const-string/jumbo v0, "ancestor"

    goto :goto_0

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {p1, v0}, Lcom/twitter/android/timeline/bm;->a(Ljava/lang/StringBuilder;Lcom/twitter/model/core/Tweet;)V

    .line 54
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/android/timeline/bm;->a(Ljava/lang/StringBuilder;Lcgi;)V

    .line 57
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method
