.class public Lcom/twitter/android/timeline/cq;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/cq$a;
    }
.end annotation


# instance fields
.field public final a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/co;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/twitter/model/timeline/at$a;


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/cq$a;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 21
    invoke-static {p1}, Lcom/twitter/android/timeline/cq$a;->a(Lcom/twitter/android/timeline/cq$a;)Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbi;

    iput-object v0, p0, Lcom/twitter/android/timeline/cq;->a:Lcbi;

    .line 22
    invoke-static {p1}, Lcom/twitter/android/timeline/cq$a;->b(Lcom/twitter/android/timeline/cq$a;)Lcom/twitter/model/timeline/at$a;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/at$a;

    iput-object v0, p0, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget v0, v0, Lcom/twitter/model/timeline/at$a;->b:I

    packed-switch v0, :pswitch_data_0

    .line 44
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Incapable of collecting content ids for Who To Follow type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget v2, v2, Lcom/twitter/model/timeline/at$a;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/timeline/cq;->b:Lcom/twitter/model/timeline/at$a;

    iget-object v0, v0, Lcom/twitter/model/timeline/at$a;->i:Ljava/util/List;

    invoke-interface {p2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 48
    return-void

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bc_()Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcbl$a;

    invoke-direct {v0}, Lcbl$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/timeline/cq;->a:Lcbi;

    .line 29
    invoke-virtual {v0, v1}, Lcbl$a;->a(Ljava/lang/Iterable;)Lcbl$a;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcbl$a;->a()Lcbl;

    move-result-object v0

    .line 28
    return-object v0
.end method
