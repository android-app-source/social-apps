.class public Lcom/twitter/android/timeline/bx;
.super Lcom/twitter/android/timeline/bc;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bc",
        "<",
        "Lcom/twitter/android/timeline/co;",
        "Lcom/twitter/android/timeline/co$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/by;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bc;-><init>(Lcom/twitter/android/timeline/bo;)V

    .line 19
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    .line 32
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 34
    sget v1, Lbue;->g:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 35
    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    invoke-static {v1}, Lcom/twitter/model/timeline/z$a;->r(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    sget v0, Lbue;->av:I

    .line 25
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcha;->a:Lcha$b;

    .line 24
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    .line 26
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcha;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bx;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
