.class public final Lcom/twitter/android/timeline/ca$a;
.super Lcom/twitter/android/timeline/bk$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/ca;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bk$a",
        "<",
        "Lcom/twitter/android/timeline/ca;",
        "Lcom/twitter/android/timeline/ca$a;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Lcgi;

.field k:Lcom/twitter/model/topic/e;

.field l:Z

.field m:Ljava/lang/String;

.field n:Ljava/lang/String;

.field o:I

.field p:I


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/bk$a;-><init>(J)V

    .line 59
    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/twitter/android/timeline/bk$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/ca$a;->a:Ljava/lang/String;

    .line 124
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/ca$a;->h:Ljava/lang/String;

    .line 125
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/ca$a;->m:Ljava/lang/String;

    .line 126
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 123
    :goto_0
    return v0

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 111
    iput p1, p0, Lcom/twitter/android/timeline/ca$a;->o:I

    .line 112
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/android/timeline/ca$a;->j:Lcgi;

    .line 82
    return-object p0
.end method

.method public a(Lcom/twitter/model/topic/e;)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/android/timeline/ca$a;->k:Lcom/twitter/model/topic/e;

    .line 88
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/android/timeline/ca$a;->a:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/twitter/android/timeline/ca$a;->l:Z

    .line 94
    return-object p0
.end method

.method public b(I)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 117
    iput p1, p0, Lcom/twitter/android/timeline/ca$a;->p:I

    .line 118
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/twitter/android/timeline/ca$a;->e()Lcom/twitter/android/timeline/ca;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/android/timeline/ca$a;->h:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/android/timeline/ca$a;->i:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/android/timeline/ca$a;->m:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method protected e()Lcom/twitter/android/timeline/ca;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lcom/twitter/android/timeline/ca;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/timeline/ca;-><init>(Lcom/twitter/android/timeline/ca$a;Lcom/twitter/android/timeline/ca$1;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/android/timeline/ca$a;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/twitter/android/timeline/ca$a;->n:Ljava/lang/String;

    .line 106
    return-object p0
.end method
