.class public Lcom/twitter/android/timeline/aa;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/af;",
        "Lcom/twitter/android/timeline/af$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/af$a;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/android/timeline/af$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/af$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/af$a;)Lcom/twitter/android/timeline/af$a;
    .locals 2

    .prologue
    .line 29
    sget v0, Lbue;->f:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 30
    sget-object v1, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    .line 31
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/af$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/timeline/af$a;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 13
    check-cast p2, Lcom/twitter/android/timeline/af$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aa;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/af$a;)Lcom/twitter/android/timeline/af$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 16
    sget v0, Lbue;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 17
    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/aa;->a(J)Lcom/twitter/android/timeline/af$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 13
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/aa;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
