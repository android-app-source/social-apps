.class public Lcom/twitter/android/timeline/g;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/g$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/timeline/f$a;


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/g$a;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 15
    invoke-static {p1}, Lcom/twitter/android/timeline/g$a;->a(Lcom/twitter/android/timeline/g$a;)Lcom/twitter/model/timeline/f$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/g;->a:Lcom/twitter/model/timeline/f$a;

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/g$a;Lcom/twitter/android/timeline/g$1;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/g;-><init>(Lcom/twitter/android/timeline/g$a;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/android/timeline/g;->a:Lcom/twitter/model/timeline/f$a;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/twitter/android/timeline/g;->a:Lcom/twitter/model/timeline/f$a;

    iget-wide v0, v0, Lcom/twitter/model/timeline/f$a;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    :cond_0
    return-void
.end method
