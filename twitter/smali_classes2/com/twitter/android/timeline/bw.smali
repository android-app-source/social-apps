.class public Lcom/twitter/android/timeline/bw;
.super Lcom/twitter/android/timeline/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/d",
        "<",
        "Lcom/twitter/android/timeline/cd;",
        "Lcom/twitter/android/timeline/cd$b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/timeline/d;-><init>(Z)V

    .line 20
    return-void
.end method

.method private static f(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-static {p0}, Lcom/twitter/android/timeline/bd;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lbue;->aq:I

    .line 50
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 16
    check-cast p2, Lcom/twitter/android/timeline/cd$b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bw;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cd$b;)Lcom/twitter/android/timeline/cd$b;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/cd$b;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/twitter/android/timeline/cd$b;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/cd$b;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cd$b;)Lcom/twitter/android/timeline/cd$b;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bw;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 41
    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/cd$b;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/timeline/cd$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$b;

    .line 42
    invoke-static {p1}, Lcom/twitter/android/timeline/bw;->f(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cd$b;->c(Ljava/lang/String;)Lcom/twitter/android/timeline/cd$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd$b;

    .line 40
    return-object v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/bw;->a(J)Lcom/twitter/android/timeline/cd$b;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 25
    sget v1, Lbue;->e:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 26
    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 16
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/bw;->b(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
