.class public Lcom/twitter/android/timeline/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lang;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lang",
        "<",
        "Lcom/twitter/android/timeline/FooterImpressionState;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/timeline/FooterImpressionState;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/w;->b:Ljava/util/Set;

    .line 28
    iput-object p1, p0, Lcom/twitter/android/timeline/w;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 29
    iget-object v1, p0, Lcom/twitter/android/timeline/w;->b:Ljava/util/Set;

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/twitter/android/timeline/FooterImpressionState;->a:Ljava/util/Set;

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 30
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/x;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lcom/twitter/android/timeline/w;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/twitter/android/timeline/x;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/twitter/android/timeline/w;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/timeline/w;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 35
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/timeline/w;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/timeline/w;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    .line 36
    :goto_1
    iget-object v3, p1, Lcom/twitter/android/timeline/x;->e:Lcom/twitter/model/timeline/r;

    if-eqz v3, :cond_0

    iget-object v1, p1, Lcom/twitter/android/timeline/x;->e:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 38
    :cond_0
    new-instance v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 39
    iget-object v4, p1, Lcom/twitter/android/timeline/x;->e:Lcom/twitter/model/timeline/r;

    iput-object v4, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 40
    iput p2, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 41
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    const/4 v0, 0x2

    aput-object v1, v5, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "footer"

    aput-object v1, v5, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "impression"

    aput-object v1, v5, v0

    .line 42
    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 43
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 41
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 45
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 34
    goto :goto_0

    :cond_3
    move-object v2, v1

    .line 35
    goto :goto_1
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string/jumbo v0, "footer_impression_helper_id"

    return-object v0
.end method

.method public b()Lcom/twitter/android/timeline/FooterImpressionState;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/twitter/android/timeline/FooterImpressionState;

    iget-object v1, p0, Lcom/twitter/android/timeline/w;->b:Ljava/util/Set;

    invoke-direct {v0, v1}, Lcom/twitter/android/timeline/FooterImpressionState;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/android/timeline/w;->b()Lcom/twitter/android/timeline/FooterImpressionState;

    move-result-object v0

    return-object v0
.end method
