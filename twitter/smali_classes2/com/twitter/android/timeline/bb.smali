.class public Lcom/twitter/android/timeline/bb;
.super Lcom/twitter/android/timeline/bk;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/timeline/bb$a;
    }
.end annotation


# instance fields
.field public final a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<+",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/twitter/android/timeline/bb$a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bk;-><init>(Lcom/twitter/android/timeline/bk$a;)V

    .line 27
    invoke-static {p1}, Lcom/twitter/android/timeline/bb$a;->a(Lcom/twitter/android/timeline/bb$a;)Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbi;

    iput-object v0, p0, Lcom/twitter/android/timeline/bb;->a:Lcbi;

    .line 28
    invoke-static {p1}, Lcom/twitter/android/timeline/bb$a;->b(Lcom/twitter/android/timeline/bb$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/timeline/bb;->b:Ljava/lang/String;

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/timeline/bb$a;Lcom/twitter/android/timeline/bb$1;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bb;-><init>(Lcom/twitter/android/timeline/bb$a;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/timeline/bb;->a:Lcbi;

    new-instance v1, Lcom/twitter/android/timeline/bb$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/timeline/bb$1;-><init>(Lcom/twitter/android/timeline/bb;Landroid/content/Context;)V

    .line 35
    invoke-static {v0, v1}, Lcpt;->c(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    .line 34
    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bc_()Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<+",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/timeline/bb;->a:Lcbi;

    return-object v0
.end method
