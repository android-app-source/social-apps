.class public Lcom/twitter/android/timeline/cp;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/android/timeline/cq;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/ct;

.field private final b:Lcom/twitter/android/timeline/cr;


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/ct;Lcom/twitter/android/timeline/cr;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/twitter/android/timeline/cp;->a:Lcom/twitter/android/timeline/ct;

    .line 16
    iput-object p2, p0, Lcom/twitter/android/timeline/cp;->b:Lcom/twitter/android/timeline/cr;

    .line 17
    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/cp;->b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/cq;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/android/timeline/cp;->a:Lcom/twitter/android/timeline/ct;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/ct;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/cp;->b:Lcom/twitter/android/timeline/cr;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/cr;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/cq;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/timeline/cp;->a:Lcom/twitter/android/timeline/ct;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/ct;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/twitter/android/timeline/cp;->a:Lcom/twitter/android/timeline/ct;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/ct;->c(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cq;

    .line 30
    :goto_0
    return-object v0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/timeline/cp;->b:Lcom/twitter/android/timeline/cr;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/cr;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    iget-object v0, p0, Lcom/twitter/android/timeline/cp;->b:Lcom/twitter/android/timeline/cr;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/cr;->c(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bk;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cq;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot be hydrated to a WhoToFollowItem"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/cp;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
