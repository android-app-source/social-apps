.class public final Lcom/twitter/android/timeline/co$a;
.super Lcom/twitter/android/timeline/bk$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/co;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bk$a",
        "<",
        "Lcom/twitter/android/timeline/co;",
        "Lcom/twitter/android/timeline/co$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 33
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/timeline/bk$a;-><init>(J)V

    .line 34
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/bk$a;-><init>(J)V

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/co$a;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/timeline/co$a;->a:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Lcom/twitter/android/timeline/bk$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/co$a;->a:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/android/timeline/co$a;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/twitter/android/timeline/co$a;->a:Lcom/twitter/model/core/TwitterUser;

    .line 43
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/android/timeline/co$a;->e()Lcom/twitter/android/timeline/co;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/android/timeline/co;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lcom/twitter/android/timeline/co;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/timeline/co;-><init>(Lcom/twitter/android/timeline/co$a;Lcom/twitter/android/timeline/co$1;)V

    return-object v0
.end method
