.class public Lcom/twitter/android/timeline/am;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/an;",
        "Lcom/twitter/android/timeline/an$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(J)Lcom/twitter/android/timeline/an$a;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/twitter/android/timeline/an$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/an$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/an$a;)Lcom/twitter/android/timeline/an$a;
    .locals 2

    .prologue
    .line 36
    sget v0, Lbue;->f:I

    .line 37
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/timeline/o;->a:Lcom/twitter/util/serialization/b;

    .line 36
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/o;

    .line 38
    sget-object v1, Lcom/twitter/model/timeline/o;->b:Lcom/twitter/model/timeline/o;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/o;

    invoke-virtual {p2, v0}, Lcom/twitter/android/timeline/an$a;->a(Lcom/twitter/model/timeline/o;)Lcom/twitter/android/timeline/an$a;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/an$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/am;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/an$a;)Lcom/twitter/android/timeline/an$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 20
    sget v0, Lbue;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 22
    sget v1, Lbue;->e:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 23
    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->j(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/am;->a(J)Lcom/twitter/android/timeline/an$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/am;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
