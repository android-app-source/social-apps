.class public final Lcom/twitter/android/timeline/bb$a;
.super Lcom/twitter/android/timeline/bk$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/timeline/bb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bk$a",
        "<",
        "Lcom/twitter/android/timeline/bb;",
        "Lcom/twitter/android/timeline/bb$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<+",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/timeline/bk$a;-><init>(J)V

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/timeline/bb$a;)Lcbi;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/timeline/bb$a;->a:Lcbi;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/timeline/bb$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/timeline/bb$a;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/twitter/android/timeline/bk$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/bb$a;->a:Lcbi;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcbi;)Lcom/twitter/android/timeline/bb$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<+",
            "Lcom/twitter/android/timeline/bk;",
            ">;)",
            "Lcom/twitter/android/timeline/bb$a;"
        }
    .end annotation

    .prologue
    .line 62
    iput-object p1, p0, Lcom/twitter/android/timeline/bb$a;->a:Lcbi;

    .line 63
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/timeline/bb$a;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/twitter/android/timeline/bb$a;->h:Ljava/lang/String;

    .line 69
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/twitter/android/timeline/bb$a;->e()Lcom/twitter/android/timeline/bb;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/android/timeline/bb;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/android/timeline/bb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/timeline/bb;-><init>(Lcom/twitter/android/timeline/bb$a;Lcom/twitter/android/timeline/bb$1;)V

    return-object v0
.end method
