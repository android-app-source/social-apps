.class public Lcom/twitter/android/timeline/cl;
.super Lcom/twitter/android/timeline/bc;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bc",
        "<",
        "Lcom/twitter/android/timeline/cd;",
        "Lcom/twitter/android/timeline/cd$b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/bw;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/twitter/android/timeline/bc;-><init>(Lcom/twitter/android/timeline/bo;)V

    .line 23
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 27
    sget v1, Lbue;->e:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 29
    sget v2, Lbue;->g:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 30
    if-ne v1, v0, :cond_0

    invoke-static {v2}, Lcom/twitter/model/timeline/z$a;->r(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    sget v0, Lbue;->av:I

    .line 37
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcha;->a:Lcha$b;

    .line 36
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    .line 38
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcha;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/cl;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
