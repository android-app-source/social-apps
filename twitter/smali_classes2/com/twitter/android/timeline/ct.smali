.class public Lcom/twitter/android/timeline/ct;
.super Lcom/twitter/android/timeline/bo;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/bo",
        "<",
        "Lcom/twitter/android/timeline/cq;",
        "Lcom/twitter/android/timeline/cq$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/timeline/by;


# direct methods
.method public constructor <init>(Lcom/twitter/android/timeline/by;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/android/timeline/bo;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/twitter/android/timeline/ct;->a:Lcom/twitter/android/timeline/by;

    .line 19
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bk$a;)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    check-cast p2, Lcom/twitter/android/timeline/cq$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ct;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cq$a;)Lcom/twitter/android/timeline/cq$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)Lcom/twitter/android/timeline/cq$a;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/timeline/cq$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/timeline/cq$a;-><init>(J)V

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/timeline/cq$a;)Lcom/twitter/android/timeline/cq$a;
    .locals 3

    .prologue
    .line 37
    sget v0, Lbue;->f:I

    .line 38
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/timeline/at$a;->a:Lcom/twitter/util/serialization/b;

    .line 37
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/at$a;

    .line 41
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/model/timeline/at$a;->e:Lcom/twitter/model/timeline/r;

    .line 43
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/timeline/ct;->a:Lcom/twitter/android/timeline/by;

    invoke-virtual {p0, p1, v2}, Lcom/twitter/android/timeline/ct;->a(Landroid/database/Cursor;Lcom/twitter/android/timeline/bo;)Lcbi;

    move-result-object v2

    .line 46
    invoke-virtual {p2, v1}, Lcom/twitter/android/timeline/cq$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/android/timeline/bk$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/cq$a;

    .line 47
    invoke-virtual {v1, v2}, Lcom/twitter/android/timeline/cq$a;->a(Lcbi;)Lcom/twitter/android/timeline/cq$a;

    move-result-object v1

    .line 48
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/cq$a;->a(Lcom/twitter/model/timeline/at$a;)Lcom/twitter/android/timeline/cq$a;

    move-result-object v0

    .line 45
    return-object v0

    .line 41
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 24
    sget v0, Lbue;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 25
    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->m(I)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(J)Lcom/twitter/android/timeline/bk$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/timeline/ct;->a(J)Lcom/twitter/android/timeline/cq$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/timeline/ct;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
