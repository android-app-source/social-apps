.class public Lcom/twitter/android/DispatchActivity;
.super Lcom/twitter/app/common/base/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/DispatchActivity$a;,
        Lcom/twitter/android/DispatchActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/library/client/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 274
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/DispatchActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "front:welcome:::impression"

    aput-object v3, v1, v2

    .line 275
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 274
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 276
    invoke-virtual {p0, p1}, Lcom/twitter/android/DispatchActivity;->setContentView(I)V

    .line 278
    const v0, 0x7f13087d

    invoke-virtual {p0, v0}, Lcom/twitter/android/DispatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    const v0, 0x7f13087e

    invoke-virtual {p0, v0}, Lcom/twitter/android/DispatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DispatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    .line 71
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 70
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 72
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 73
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DispatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    .line 83
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    .line 84
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 82
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 85
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 86
    return-void
.end method

.method protected static a(Lcom/twitter/android/DispatchActivity$a;)V
    .locals 1

    .prologue
    .line 234
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity$a;->b()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity$a;->b()V

    goto :goto_0
.end method

.method public static b(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DispatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x10008000

    .line 96
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 97
    if-eqz p1, :cond_0

    .line 98
    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 100
    :cond_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 101
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 102
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/app/main/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 223
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 199
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onAttachedToWindow()V

    .line 200
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    .line 201
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 251
    const v1, 0x7f13087e

    if-ne v0, v1, :cond_1

    .line 252
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/DispatchActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "signup:form:sign_in:button:click"

    aput-object v2, v1, v4

    .line 253
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 252
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 254
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "android.intent.extra.INTENT"

    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "android.intent.extra.INTENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 255
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 254
    invoke-virtual {p0, v0}, Lcom/twitter/android/DispatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    const v1, 0x7f13087d

    if-ne v0, v1, :cond_0

    .line 258
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/DispatchActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "signup:form:sign_up:button:click"

    aput-object v2, v1, v4

    .line 259
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 258
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 260
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 261
    new-instance v1, Lcom/twitter/android/v$a;

    invoke-direct {v1}, Lcom/twitter/android/v$a;-><init>()V

    .line 262
    invoke-virtual {v1, v4}, Lcom/twitter/android/v$a;->a(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    .line 263
    invoke-virtual {v1, v4}, Lcom/twitter/android/v$a;->b(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    .line 264
    invoke-virtual {v1, v5}, Lcom/twitter/android/v$a;->c(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    .line 265
    const-string/jumbo v2, "android.intent.extra.INTENT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    const-string/jumbo v2, "android.intent.extra.INTENT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lcom/twitter/android/v$a;->a(Landroid/content/Intent;)Lcom/twitter/android/v$a;

    .line 268
    :cond_2
    new-instance v2, Lcom/twitter/android/FlowActivity$a;

    invoke-direct {v2, p0}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lako;

    invoke-virtual {v2, v0}, Lcom/twitter/android/FlowActivity$a;->a(Lako;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 114
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v6, v0

    mul-long/2addr v4, v6

    .line 116
    const-wide/32 v6, 0x19000

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v0, v1

    .line 117
    :goto_0
    if-nez v0, :cond_2

    .line 118
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DiskCleanupPromptActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/DispatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 119
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->finish()V

    .line 195
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 116
    goto :goto_0

    .line 123
    :cond_2
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DispatchActivity;->a:Lcom/twitter/library/client/v;

    .line 125
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 126
    const-string/jumbo v3, "scribe_event"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string/jumbo v3, "scribe_context"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 127
    const-string/jumbo v3, "scribe_event"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 128
    const-string/jumbo v4, "scribe_context"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 129
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v6, p0, Lcom/twitter/android/DispatchActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v6}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    aput-object v3, v6, v2

    aput-object v4, v6, v1

    .line 130
    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 129
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 131
    const-string/jumbo v2, "scribe_event"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 132
    const-string/jumbo v2, "scribe_context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 135
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 137
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 138
    const-string/jumbo v3, "DispatchActivity"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/DispatchActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 139
    const-string/jumbo v4, "version_code_for_app_update"

    const/4 v5, -0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 140
    if-ne v4, v8, :cond_4

    .line 142
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v5

    .line 143
    new-instance v6, Lbcg;

    .line 144
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/twitter/android/DispatchActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v8}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "android:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lbcg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 146
    new-instance v7, Lcom/twitter/android/DispatchActivity$b;

    .line 147
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/twitter/android/DispatchActivity$b;-><init>(Landroid/content/Context;)V

    .line 146
    invoke-virtual {v5, v6, v7}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 149
    :cond_4
    if-eq v2, v4, :cond_5

    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/twitter/android/util/AppEventTrack$EventType;->e:Lcom/twitter/android/util/AppEventTrack$EventType;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-static {v4, v5, v6}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/android/util/AppEventTrack$EventType;[Ljava/lang/String;)V

    .line 151
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 152
    const-string/jumbo v4, "version_code_for_app_update"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 153
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_5
    :goto_2
    invoke-static {p0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/android/util/s;->i()V

    .line 160
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 161
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 162
    invoke-static {}, Lcom/twitter/android/metrics/LaunchTracker;->a()Lcom/twitter/android/metrics/LaunchTracker;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/android/metrics/LaunchTracker;->a(Landroid/content/Intent;)V

    .line 163
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    .line 164
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 165
    invoke-static {v0, v2, v3}, Lcom/twitter/android/metrics/a;->a(Lcom/twitter/metrics/j;J)Lcom/twitter/android/metrics/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/android/metrics/a;->i()V

    .line 166
    invoke-static {v0, v2, v3}, Lcom/twitter/android/metrics/e;->a(Lcom/twitter/metrics/j;J)Lcom/twitter/android/metrics/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->i()V

    .line 167
    invoke-static {p0}, Lcom/twitter/android/bw;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 168
    new-instance v0, Lcom/twitter/android/v$a;

    invoke-static {p0}, Lcom/twitter/android/FlowData;->d(Landroid/content/Context;)Lcom/twitter/android/FlowData;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/v$a;-><init>(Lcom/twitter/android/FlowData;)V

    .line 169
    invoke-virtual {v0, v1}, Lcom/twitter/android/v$a;->e(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/v;

    .line 171
    new-instance v1, Lcom/twitter/android/FlowActivity$a;

    invoke-direct {v1, p0}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    .line 172
    invoke-virtual {v1, v0}, Lcom/twitter/android/FlowActivity$a;->a(Lako;)V

    .line 173
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->finish()V

    goto/16 :goto_1

    .line 182
    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 183
    if-nez v0, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/twitter/android/DispatchActivity;->c()V

    goto/16 :goto_1

    .line 189
    :cond_7
    if-nez p1, :cond_8

    .line 190
    const-string/jumbo v0, "welcome"

    invoke-static {p0, v0}, Lcom/twitter/android/al;->b(Landroid/app/Activity;Ljava/lang/String;)V

    .line 193
    :cond_8
    const v0, 0x7f04044e

    invoke-direct {p0, v0}, Lcom/twitter/android/DispatchActivity;->a(I)V

    .line 194
    invoke-static {p0}, Lcom/twitter/android/bw;->a(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 155
    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 107
    invoke-virtual {p0, p1}, Lcom/twitter/android/DispatchActivity;->setIntent(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 206
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onStart()V

    .line 208
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/twitter/android/DispatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 210
    if-eqz v0, :cond_0

    .line 211
    new-instance v0, Lcom/twitter/android/DispatchActivity$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/DispatchActivity$a;-><init>(Lcom/twitter/android/DispatchActivity;)V

    invoke-static {v0}, Lcom/twitter/android/DispatchActivity;->a(Lcom/twitter/android/DispatchActivity$a;)V

    .line 219
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/DispatchActivity;->c()V

    goto :goto_0

    .line 217
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/DispatchActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "front::::impression"

    aput-object v3, v1, v2

    .line 218
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 217
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method
