.class public Lcom/twitter/android/aq;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/aq$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/library/client/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/twitter/android/aq;->a:Landroid/content/Context;

    .line 35
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/aq;->b:Lcom/twitter/library/client/v;

    .line 36
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/aq;->c:Lcom/twitter/library/client/p;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/aq;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/aq;->b:Lcom/twitter/library/client/v;

    return-object v0
.end method

.method private a(Lcom/twitter/library/service/s;Lcom/twitter/android/aq$a;)V
    .locals 2

    .prologue
    .line 51
    if-eqz p2, :cond_0

    .line 52
    new-instance v0, Lcom/twitter/android/aq$1;

    invoke-direct {v0, p0, p2}, Lcom/twitter/android/aq$1;-><init>(Lcom/twitter/android/aq;Lcom/twitter/android/aq$a;)V

    .line 65
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/aq;->c:Lcom/twitter/library/client/p;

    invoke-virtual {v1, p1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 66
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(JJLcom/twitter/android/aq$a;)V
    .locals 9

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/aq;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 41
    new-instance v1, Lbez;

    iget-object v2, p0, Lcom/twitter/android/aq;->a:Landroid/content/Context;

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lbez;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    invoke-direct {p0, v1, p5}, Lcom/twitter/android/aq;->a(Lcom/twitter/library/service/s;Lcom/twitter/android/aq$a;)V

    .line 42
    return-void
.end method

.method public b(JJLcom/twitter/android/aq$a;)V
    .locals 9

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/aq;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 46
    new-instance v1, Lbfe;

    iget-object v2, p0, Lcom/twitter/android/aq;->a:Landroid/content/Context;

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lbfe;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    invoke-direct {p0, v1, p5}, Lcom/twitter/android/aq;->a(Lcom/twitter/library/service/s;Lcom/twitter/android/aq$a;)V

    .line 47
    return-void
.end method
