.class public Lcom/twitter/android/ManualEntryPinFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/client/u$a;
.implements Lcom/twitter/android/widget/PinEntryEditText$a;
.implements Lcom/twitter/app/common/dialog/b$d;


# instance fields
.field protected a:Lcom/twitter/android/bd;

.field protected b:Lcom/twitter/android/by;

.field protected c:Lcom/twitter/android/widget/PinEntryEditText;

.field protected d:Lcom/twitter/ui/widget/TwitterButton;

.field protected e:Landroid/view/View;

.field protected f:Lcom/twitter/ui/widget/TypefacesTextView;

.field protected g:Landroid/view/View;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/twitter/android/ValidationState$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method

.method private static a(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 324
    if-eqz p0, :cond_0

    .line 325
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 326
    invoke-virtual {p0}, Landroid/widget/EditText;->requestFocus()Z

    .line 328
    :cond_0
    return-void
.end method

.method private static a(Lcom/twitter/app/common/base/b;Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 115
    const-string/jumbo v0, "extra_flow_data"

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string/jumbo v0, "extra_flow_data"

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FlowData;

    .line 117
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 118
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    return-void

    .line 117
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 125
    if-nez p1, :cond_0

    if-eqz p2, :cond_5

    .line 128
    :cond_0
    const v0, 0x7f0a067e

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/android/ManualEntryPinFragment;->a:Lcom/twitter/android/bd;

    invoke-interface {v2}, Lcom/twitter/android/bd;->r()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ManualEntryPinFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 130
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, v5}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->g:Landroid/view/View;

    const v1, 0x7f1303a0

    .line 137
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 138
    iget-object v1, p0, Lcom/twitter/android/ManualEntryPinFragment;->g:Landroid/view/View;

    const v3, 0x7f1307f4

    .line 139
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 140
    const v3, 0x7f0a067f

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    .line 141
    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v2, p0, Lcom/twitter/android/ManualEntryPinFragment;->g:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 143
    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 144
    invoke-virtual {v1, v4}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 146
    if-eqz p2, :cond_4

    .line 147
    invoke-direct {p0, v4}, Lcom/twitter/android/ManualEntryPinFragment;->b(I)V

    .line 163
    :cond_3
    :goto_0
    return-void

    .line 149
    :cond_4
    invoke-direct {p0, v5}, Lcom/twitter/android/ManualEntryPinFragment;->b(I)V

    goto :goto_0

    .line 152
    :cond_5
    invoke-direct {p0, v4}, Lcom/twitter/android/ManualEntryPinFragment;->b(I)V

    .line 153
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->g:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 154
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 156
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 157
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 159
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    if-eqz v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->d:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 168
    if-nez p1, :cond_0

    .line 169
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    :cond_0
    return-void
.end method

.method private h()Lcom/twitter/android/ValidationState$State;
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PinEntryEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v1}, Lcom/twitter/android/widget/PinEntryEditText;->getFullLength()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 317
    sget-object v0, Lcom/twitter/android/ValidationState$State;->c:Lcom/twitter/android/ValidationState$State;

    .line 319
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v3

    .line 62
    const-string/jumbo v0, "nux_flow"

    invoke-virtual {v3, v0, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v4

    .line 63
    const-string/jumbo v0, "phone100_flow"

    invoke-virtual {v3, v0, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v5

    .line 64
    const-string/jumbo v0, "is_seamful_design_enabled"

    invoke-virtual {v3, v0, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v6

    .line 65
    const-string/jumbo v0, "settings_add_phone"

    invoke-virtual {v3, v0, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v7

    .line 66
    const-string/jumbo v0, "settings_update_phone"

    invoke-virtual {v3, v0, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v8

    .line 67
    const-string/jumbo v0, "phone100_add_phone_flow"

    invoke-virtual {v3, v0, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->i:Z

    .line 69
    if-eqz v4, :cond_4

    if-nez v6, :cond_4

    .line 70
    const v0, 0x7f0402a6

    .line 74
    :goto_0
    const/4 v9, 0x0

    invoke-virtual {p1, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 75
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v10, p0, Lcom/twitter/android/ManualEntryPinFragment;->T:Landroid/content/Context;

    new-array v11, v1, [Ljava/lang/String;

    const-string/jumbo v12, "android.permission.RECEIVE_SMS"

    aput-object v12, v11, v2

    invoke-virtual {v0, v10, v11}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    .line 76
    const-string/jumbo v10, "should_intercept_pin"

    invoke-virtual {v3, v10, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_5

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->h:Z

    .line 77
    const v0, 0x7f130648

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->d:Lcom/twitter/ui/widget/TwitterButton;

    .line 78
    const v0, 0x7f13024a

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    .line 79
    const v0, 0x7f130487

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 80
    const v0, 0x7f1307f3

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->g:Landroid/view/View;

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a067f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 83
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 84
    if-eqz v4, :cond_6

    if-nez v6, :cond_6

    .line 85
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    check-cast v0, Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/PromptView;->setTitle(Ljava/lang/CharSequence;)V

    .line 91
    :cond_0
    :goto_2
    const v0, 0x7f130647

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PinEntryEditText;

    iput-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PinEntryEditText;->setOnFilledInputListener(Lcom/twitter/android/widget/PinEntryEditText$a;)V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PinEntryEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    if-nez p2, :cond_1

    .line 95
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-static {v3, v0}, Lcom/twitter/android/ManualEntryPinFragment;->a(Lcom/twitter/app/common/base/b;Landroid/widget/EditText;)V

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-static {v0}, Lcom/twitter/android/ManualEntryPinFragment;->a(Landroid/widget/EditText;)V

    .line 99
    invoke-direct {p0, v5, v6}, Lcom/twitter/android/ManualEntryPinFragment;->a(ZZ)V

    .line 101
    if-nez v7, :cond_2

    if-eqz v8, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    if-eqz v0, :cond_3

    .line 102
    if-eqz v7, :cond_7

    .line 103
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    const v1, 0x7f0a067c

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    .line 109
    :cond_3
    :goto_3
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Lcom/twitter/app/common/base/b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->j:Z

    .line 111
    return-object v9

    .line 72
    :cond_4
    const v0, 0x7f04019e

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 76
    goto/16 :goto_1

    .line 87
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 104
    :cond_7
    if-eqz v8, :cond_3

    .line 105
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    const v1, 0x7f0a068b

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    goto :goto_3
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 227
    if-nez p2, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->b:Lcom/twitter/android/by;

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "phone100_phone_optional_verify"

    aput-object v3, v1, v2

    const-string/jumbo v2, "phone_verification"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/twitter/android/ManualEntryPinFragment;->j:Z

    .line 231
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "resend"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 230
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 229
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 232
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->b:Lcom/twitter/android/by;

    invoke-interface {v0, v4}, Lcom/twitter/android/by;->d(Z)V

    .line 235
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PinEntryEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PinEntryEditText;->getFullLength()I

    move-result v0

    .line 288
    iget-boolean v1, p0, Lcom/twitter/android/ManualEntryPinFragment;->i:Z

    if-eqz v1, :cond_1

    .line 289
    invoke-virtual {p0, p1}, Lcom/twitter/android/ManualEntryPinFragment;->d(Ljava/lang/String;)V

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PinEntryEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 2

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-static {v0, v1, p1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 266
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 306
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PinEntryEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->k:Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->k:Lcom/twitter/android/ValidationState$a;

    new-instance v1, Lcom/twitter/android/ValidationState;

    .line 309
    invoke-direct {p0}, Lcom/twitter/android/ManualEntryPinFragment;->h()Lcom/twitter/android/ValidationState$State;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    .line 308
    invoke-interface {v0, v1}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    .line 312
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 190
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 191
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PinEntryEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 192
    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 194
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->k:Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->k:Lcom/twitter/android/ValidationState$a;

    new-instance v1, Lcom/twitter/android/ValidationState;

    .line 196
    invoke-direct {p0}, Lcom/twitter/android/ManualEntryPinFragment;->h()Lcom/twitter/android/ValidationState$State;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    .line 195
    invoke-interface {v0, v1}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    .line 199
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->h:Z

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/u;->a(Landroid/content/Context;)Lcom/twitter/android/client/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/u;->a(Lcom/twitter/android/client/u$a;)V

    .line 202
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->f()V

    .line 243
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 298
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->i:Z

    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {p0, p1}, Lcom/twitter/android/ManualEntryPinFragment;->d(Ljava/lang/String;)V

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->a:Lcom/twitter/android/bd;

    invoke-interface {v0, p1}, Lcom/twitter/android/bd;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->a:Lcom/twitter/android/bd;

    invoke-interface {v0}, Lcom/twitter/android/bd;->q()V

    .line 239
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->b:Lcom/twitter/android/by;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->b:Lcom/twitter/android/by;

    invoke-interface {v0, p1}, Lcom/twitter/android/by;->f(Ljava/lang/String;)V

    .line 276
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 246
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0681

    .line 247
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0682

    .line 248
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 249
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 250
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 251
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 252
    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 253
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PinEntryEditText;->setText(Ljava/lang/CharSequence;)V

    .line 270
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->b:Lcom/twitter/android/by;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->b:Lcom/twitter/android/by;

    iget-object v1, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v1}, Lcom/twitter/android/widget/PinEntryEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/by;->b(Ljava/lang/String;)V

    .line 282
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 176
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    .line 177
    check-cast v0, Lcom/twitter/android/bd;

    iput-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->a:Lcom/twitter/android/bd;

    .line 179
    instance-of v0, p1, Lcom/twitter/android/by;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 180
    check-cast v0, Lcom/twitter/android/by;

    iput-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->b:Lcom/twitter/android/by;

    .line 183
    :cond_0
    instance-of v0, p1, Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_1

    .line 184
    check-cast p1, Lcom/twitter/android/ValidationState$a;

    iput-object p1, p0, Lcom/twitter/android/ManualEntryPinFragment;->k:Lcom/twitter/android/ValidationState$a;

    .line 186
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f130648

    if-ne v0, v1, :cond_1

    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/ManualEntryPinFragment;->d()V

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f130647

    if-ne v0, v1, :cond_0

    .line 220
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ManualEntryPinFragment;->a(Z)V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ManualEntryPinFragment;->a(Z)V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->c:Lcom/twitter/android/widget/PinEntryEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PinEntryEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 208
    iget-boolean v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->h:Z

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/twitter/android/ManualEntryPinFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/u;->a(Landroid/content/Context;)Lcom/twitter/android/client/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/u;->b()V

    .line 211
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 212
    return-void
.end method
