.class public Lcom/twitter/android/GalleryActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/twitter/android/cy$c;
.implements Lcom/twitter/android/m;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/ui/anim/c$a;
.implements Lsj$a;
.implements Lsp$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/GalleryActivity$b;,
        Lcom/twitter/android/GalleryActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/support/v4/view/ViewPager$OnPageChangeListener;",
        "Lcom/twitter/android/cy$c;",
        "Lcom/twitter/android/m;",
        "Lcom/twitter/app/common/dialog/b$d;",
        "Lcom/twitter/ui/anim/c$a;",
        "Lsj$a;",
        "Lsp$a;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final l:Lcom/twitter/ui/view/h;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:Lcom/twitter/android/cy;

.field private E:Lcom/twitter/library/widget/SlidingPanel;

.field private K:J

.field private final L:Lsp;

.field private M:Ljava/lang/String;

.field private N:Landroid/view/View;

.field private O:I

.field private P:Ljava/lang/String;

.field private Q:Z

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;"
        }
    .end annotation
.end field

.field private S:F

.field private T:Z

.field private U:Lcom/twitter/model/core/n;

.field private V:Landroid/view/ViewGroup;

.field private W:Lsi;

.field private X:Lcom/twitter/library/av/b;

.field private Y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lbrm;",
            ">;>;"
        }
    .end annotation
.end field

.field private Z:Z

.field protected b:Z

.field protected c:Lcom/twitter/android/av/GalleryVideoChromeView;

.field protected d:Landroid/widget/LinearLayout;

.field protected e:Landroid/widget/FrameLayout;

.field protected f:Lsj;

.field g:Landroid/view/animation/Animation;

.field h:Landroid/view/animation/Animation;

.field i:Lcom/twitter/model/core/Tweet;

.field j:Lcom/twitter/model/core/MediaEntity;

.field k:I

.field private m:Lcgi;

.field private n:Lcom/twitter/library/widget/TweetView;

.field private o:Landroid/support/v4/view/ViewPager;

.field private p:J

.field private q:Z

.field private r:Landroid/view/ViewGroup;

.field private s:Lcom/twitter/android/MediaActionBarFragment;

.field private t:Landroid/widget/TextView;

.field private u:Lcom/twitter/library/client/Session;

.field private v:[Ljava/lang/String;

.field private w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private x:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private y:Lcom/twitter/android/GalleryActivity$a;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 280
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/GalleryActivity;->a:[Ljava/lang/String;

    .line 302
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    .line 303
    invoke-virtual {v0, v3}, Lcom/twitter/ui/view/h$a;->b(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/GalleryActivity;->l:Lcom/twitter/ui/view/h;

    .line 302
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 369
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 315
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/GalleryActivity;->k:I

    .line 320
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/twitter/android/GalleryActivity;->p:J

    .line 321
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->q:Z

    .line 370
    new-instance v0, Lsp;

    invoke-direct {v0, p0}, Lsp;-><init>(Lsp$a;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->L:Lsp;

    .line 371
    return-void
.end method

.method private a(IILcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 1519
    add-int/lit8 v0, p1, 0x1

    if-ne p2, v0, :cond_1

    .line 1520
    const/4 v0, 0x0

    invoke-direct {p0, v0, p3}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/model/core/Tweet;)V

    .line 1524
    :cond_0
    :goto_0
    return-void

    .line 1521
    :cond_1
    add-int/lit8 v0, p1, -0x1

    if-ne p2, v0, :cond_0

    .line 1522
    const/4 v0, 0x1

    invoke-direct {p0, v0, p3}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method private a(IJLcgi;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1711
    invoke-direct {p0, p1}, Lcom/twitter/android/GalleryActivity;->d(I)Ljava/lang/String;

    move-result-object v0

    .line 1712
    if-eqz v0, :cond_0

    .line 1713
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 1714
    invoke-static {v1, p2, p3, p4, p5}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 1715
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1716
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->x:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 1717
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1715
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1719
    :cond_0
    return-void
.end method

.method private a(ILcom/twitter/model/core/Tweet;)V
    .locals 6

    .prologue
    .line 1684
    invoke-direct {p0, p1}, Lcom/twitter/android/GalleryActivity;->d(I)Ljava/lang/String;

    move-result-object v2

    .line 1685
    if-eqz v2, :cond_0

    .line 1688
    const/16 v0, 0x9

    if-ne p1, v0, :cond_1

    .line 1689
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 1690
    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->G:J

    iput-wide v4, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 1691
    new-instance v1, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;-><init>()V

    .line 1692
    iget-object v3, p2, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    iget-object v3, v3, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    iput-object v3, v1, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->a:Ljava/lang/String;

    .line 1693
    iget-object v3, p2, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    iget-object v3, v3, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    invoke-virtual {v3}, Lcom/twitter/model/geo/TwitterPlace$PlaceType;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->b:Ljava/lang/String;

    .line 1694
    iget-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iget-object v3, v3, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->c:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 1699
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 1700
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v3, p2, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 1701
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1702
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1703
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1701
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1705
    :cond_0
    return-void

    .line 1696
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->x:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-object v1, v0

    goto :goto_0
.end method

.method private a(JLjava/util/List;Ljava/util/Set;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 890
    new-instance v1, Lbfn;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    .line 891
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v2, p0

    move-wide v4, p1

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v1 .. v9}, Lbfn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLjava/util/List;Ljava/util/Set;)V

    const/4 v0, 0x0

    .line 890
    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/GalleryActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 892
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;Lcom/twitter/media/ui/image/BaseMediaImageView;)V
    .locals 2

    .prologue
    const/16 v1, 0x23bf

    .line 359
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-static {p1, p2}, Lsi;->a(Landroid/content/Intent;Lcom/twitter/media/ui/image/BaseMediaImageView;)V

    .line 362
    invoke-static {p0, p1, p2, v1}, Lcom/twitter/ui/anim/b;->a(Landroid/app/Activity;Landroid/content/Intent;Landroid/view/View;I)V

    .line 367
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-virtual {p0, p1, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->t()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;ILcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/model/core/Tweet;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/twitter/android/GalleryActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 2

    .prologue
    .line 1493
    .line 1494
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    .line 1495
    :goto_0
    invoke-static {p1}, Lcom/twitter/android/av/f;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1496
    invoke-static {p2, v0}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1498
    :cond_0
    return-void

    .line 1494
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->m:Lcgi;

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/high16 v10, -0x8000000000000000L

    const/4 v3, 0x0

    .line 1160
    iget v0, p0, Lcom/twitter/android/GalleryActivity;->k:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1161
    iget-wide v0, p0, Lcom/twitter/android/GalleryActivity;->p:J

    cmp-long v0, v0, v10

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    if-nez v0, :cond_3

    .line 1162
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v1, v3

    .line 1163
    :goto_0
    if-ge v1, v4, :cond_5

    .line 1164
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsn;

    .line 1165
    iget-object v0, v0, Lsn;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v6, v0, Lcom/twitter/model/core/Tweet;->u:J

    iget-wide v8, p0, Lcom/twitter/android/GalleryActivity;->p:J

    cmp-long v0, v6, v8

    if-nez v0, :cond_2

    .line 1167
    iput-wide v10, p0, Lcom/twitter/android/GalleryActivity;->p:J

    move v0, v1

    :goto_1
    move v2, v0

    .line 1184
    :cond_0
    :goto_2
    iput v2, p0, Lcom/twitter/android/GalleryActivity;->O:I

    .line 1185
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-virtual {v0, p1}, Lsj;->a(Ljava/util/List;)V

    .line 1186
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 1187
    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->onPageSelected(I)V

    .line 1188
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->o()V

    .line 1190
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->T:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1193
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    new-instance v1, Lcom/twitter/android/GalleryActivity$7;

    invoke-direct {v1, p0}, Lcom/twitter/android/GalleryActivity$7;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1203
    :cond_1
    return-void

    .line 1163
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1171
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/GalleryActivity;->k:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1172
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1173
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v1, v3

    .line 1174
    :goto_3
    if-ge v1, v4, :cond_0

    .line 1175
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsn;

    .line 1176
    iget-object v5, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    iget-object v5, v5, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    iget-object v0, v0, Lsn;->c:Lcom/twitter/media/request/a$a;

    iget-object v0, v0, Lcom/twitter/media/request/a$a;->p:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v2, v1

    .line 1178
    goto :goto_2

    .line 1174
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;Z)Z
    .locals 0

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/twitter/android/GalleryActivity;->Q:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/GalleryActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->R:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/GalleryActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/twitter/android/GalleryActivity;->R:Ljava/util/List;

    return-object p1
.end method

.method private b(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->L:Lsp;

    invoke-virtual {v0, p1}, Lsp;->a(Lcom/twitter/model/core/Tweet;)V

    .line 1303
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Lcom/twitter/library/widget/TweetView;

    sget-object v1, Lcom/twitter/android/GalleryActivity;->l:Lcom/twitter/ui/view/h;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)V

    .line 1304
    iput-object p1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    .line 1305
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/android/MediaActionBarFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MediaActionBarFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 1306
    invoke-static {p1}, Lbxd;->j(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1307
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/android/MediaActionBarFragment;

    invoke-virtual {v1, p1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;)V

    .line 1308
    if-eqz v0, :cond_0

    .line 1309
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1316
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->l()V

    .line 1317
    invoke-static {}, Lbpt;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1318
    invoke-virtual {p0, p1}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/model/core/Tweet;)Ljava/util/Map;

    .line 1320
    :cond_1
    return-void

    .line 1312
    :cond_2
    if-eqz v0, :cond_0

    .line 1313
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/widget/SlidingPanel;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    return-object v0
.end method

.method private d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1671
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->v:[Ljava/lang/String;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->v:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1673
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->v:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 1675
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/GalleryActivity;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->q()V

    return-void
.end method

.method private e(I)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1728
    const-string/jumbo v1, ""

    .line 1729
    packed-switch p1, :pswitch_data_0

    .line 1782
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->v:[Ljava/lang/String;

    .line 1783
    const/4 v0, 0x0

    .line 1800
    :goto_0
    return-object v0

    .line 1731
    :pswitch_0
    const-string/jumbo v0, "profile"

    .line 1786
    :cond_0
    :goto_1
    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/String;

    new-array v3, v10, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "next:click"

    aput-object v4, v3, v9

    .line 1787
    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    new-array v3, v10, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "prev:click"

    aput-object v4, v3, v9

    .line 1788
    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    new-array v3, v10, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->M:Ljava/lang/String;

    aput-object v4, v3, v9

    .line 1789
    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    new-array v3, v10, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "tweet:click"

    aput-object v4, v3, v9

    .line 1790
    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    new-array v3, v10, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "media_tag_summary:click"

    aput-object v4, v3, v9

    .line 1791
    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    const/4 v3, 0x5

    new-array v4, v10, [Ljava/lang/String;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "remove_my_media_tag:click"

    aput-object v5, v4, v9

    .line 1792
    invoke-static {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-array v4, v10, [Ljava/lang/String;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "media_tagged_user:follow"

    aput-object v5, v4, v9

    .line 1793
    invoke-static {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-array v4, v10, [Ljava/lang/String;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "media_tagged_user:unfollow"

    aput-object v5, v4, v9

    .line 1794
    invoke-static {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-array v4, v10, [Ljava/lang/String;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "tweet:open_link"

    aput-object v5, v4, v9

    .line 1795
    invoke-static {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    new-array v4, v10, [Ljava/lang/String;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "button:cta_click_open"

    aput-object v5, v4, v9

    .line 1796
    invoke-static {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    new-array v4, v10, [Ljava/lang/String;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    const-string/jumbo v1, "gallery"

    aput-object v1, v4, v8

    const-string/jumbo v1, "place_tag:click"

    aput-object v1, v4, v9

    .line 1797
    invoke-static {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    iput-object v2, p0, Lcom/twitter/android/GalleryActivity;->v:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1735
    :pswitch_1
    const-string/jumbo v0, "events"

    goto/16 :goto_1

    .line 1739
    :pswitch_2
    const-string/jumbo v0, "search"

    goto/16 :goto_1

    .line 1743
    :pswitch_3
    const-string/jumbo v0, "search"

    .line 1744
    const-string/jumbo v1, "cluster"

    goto/16 :goto_1

    .line 1748
    :pswitch_4
    const-string/jumbo v0, "home"

    goto/16 :goto_1

    .line 1752
    :pswitch_5
    const-string/jumbo v0, "tweet"

    goto/16 :goto_1

    .line 1756
    :pswitch_6
    const-string/jumbo v0, "profile_tweets"

    goto/16 :goto_1

    .line 1760
    :pswitch_7
    const-string/jumbo v0, "list"

    goto/16 :goto_1

    .line 1764
    :pswitch_8
    const-string/jumbo v0, "favorites"

    goto/16 :goto_1

    .line 1768
    :pswitch_9
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1769
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 1770
    :goto_2
    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1771
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 1769
    :cond_1
    const-string/jumbo v0, "photo_grid"

    goto :goto_2

    .line 1775
    :pswitch_a
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1776
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 1777
    :goto_3
    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1778
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 1776
    :cond_2
    const-string/jumbo v0, "profile"

    goto :goto_3

    .line 1729
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method static synthetic e(Lcom/twitter/android/GalleryActivity;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->r()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/GalleryActivity;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 643
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/SlidingPanel;->setLayerType(ILandroid/graphics/Paint;)V

    .line 644
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setAlpha(F)V

    .line 645
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->V:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 646
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 648
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->V:Landroid/view/ViewGroup;

    invoke-static {p0, v1, v2, v0}, Lsi;->a(Landroid/support/v4/app/FragmentActivity;Landroid/content/Intent;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)Lsi;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->W:Lsi;

    .line 649
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->W:Lsi;

    invoke-virtual {v0}, Lsi;->b()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    .line 650
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->W:Lsi;

    new-instance v1, Lcom/twitter/android/GalleryActivity$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/GalleryActivity$4;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v1}, Lsi;->a(Lcom/twitter/ui/anim/a$a;)V

    .line 664
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 733
    iget v0, p0, Lcom/twitter/android/GalleryActivity;->O:I

    iget v1, p0, Lcom/twitter/android/GalleryActivity;->k:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->W:Lsi;

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 736
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/support/v4/view/ViewPager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 737
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/SlidingPanel;->setLayerType(ILandroid/graphics/Paint;)V

    .line 738
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setAlpha(F)V

    .line 739
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->W:Lsi;

    invoke-virtual {v0}, Lsi;->az_()V

    .line 743
    :goto_0
    return-void

    .line 741
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, -0x2

    const/4 v2, 0x0

    .line 1364
    .line 1365
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    .line 1366
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/twitter/android/GalleryActivity;->U:Lcom/twitter/model/core/n;

    .line 1367
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->M()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1369
    sget-object v3, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 1370
    invoke-static {v0, v3}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    .line 1369
    invoke-static {v0}, Lcom/twitter/model/util/d;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v3

    .line 1371
    invoke-static {v3}, Lcom/twitter/model/util/d;->b(Ljava/util/List;)[J

    move-result-object v0

    .line 1372
    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lcom/twitter/android/cy;->a(Landroid/support/v4/app/LoaderManager;[J)V

    .line 1373
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1374
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/n;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->U:Lcom/twitter/model/core/n;

    .line 1378
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->t:Landroid/widget/TextView;

    new-instance v4, Lcom/twitter/android/GalleryActivity$8;

    invoke-direct {v4, p0}, Lcom/twitter/android/GalleryActivity$8;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1384
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->t:Landroid/widget/TextView;

    const v4, 0x7f020838

    invoke-static {p0, v3, v4}, Lbrw;->b(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v3

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1387
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1390
    iget v3, p0, Lcom/twitter/android/GalleryActivity;->A:I

    iget v4, p0, Lcom/twitter/android/GalleryActivity;->B:I

    iget v5, p0, Lcom/twitter/android/GalleryActivity;->C:I

    invoke-virtual {v0, v3, v4, v2, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1392
    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v1

    .line 1395
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->t:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1396
    return-void

    .line 1395
    :cond_1
    const/16 v2, 0x8

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private n()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1399
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1400
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->n:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1404
    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/android/bi$a;->TweetView:[I

    const v3, 0x7f0100d4

    .line 1405
    invoke-virtual {p0, v1, v2, v3, v5}, Lcom/twitter/android/GalleryActivity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1407
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    .line 1409
    const/16 v3, 0xb

    const/4 v4, -0x3

    .line 1411
    invoke-static {v4}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v4

    .line 1409
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    .line 1412
    const/16 v4, 0xa

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    .line 1415
    iget-object v5, p0, Lcom/twitter/android/GalleryActivity;->n:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v5}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v5

    add-int/2addr v2, v5

    add-int/2addr v2, v3

    add-int/2addr v2, v4

    iput v2, p0, Lcom/twitter/android/GalleryActivity;->A:I

    .line 1417
    const v2, 0x7f0e022e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/GalleryActivity;->B:I

    .line 1419
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1426
    :goto_0
    const v1, 0x7f0e022b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/GalleryActivity;->C:I

    .line 1428
    return-void

    .line 1421
    :cond_0
    const v1, 0x7f0e022c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/GalleryActivity;->A:I

    .line 1423
    const v1, 0x7f0e022d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/GalleryActivity;->B:I

    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 1504
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->q:Z

    if-nez v0, :cond_0

    .line 1505
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/model/core/Tweet;)V

    .line 1506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->q:Z

    .line 1508
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1621
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    if-nez v0, :cond_1

    .line 1626
    :cond_0
    :goto_0
    return-void

    .line 1624
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    invoke-static {p0, v0}, Lcom/twitter/android/geo/places/a;->a(Landroid/content/Context;Lcom/twitter/model/geo/TwitterPlace;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    .line 1625
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method private r()V
    .locals 6

    .prologue
    .line 1629
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    .line 1652
    :goto_0
    return-void

    .line 1633
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->U:Lcom/twitter/model/core/n;

    if-eqz v0, :cond_2

    .line 1634
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V

    .line 1635
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->U:Lcom/twitter/model/core/n;

    iget-object v2, v2, Lcom/twitter/model/core/n;->d:Ljava/lang/String;

    .line 1636
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v3, 0x5

    .line 1639
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1640
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, "gallery"

    .line 1641
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, "media_tag_summary"

    .line 1642
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 1637
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 1635
    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    .line 1651
    :cond_1
    :goto_1
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 1645
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/cy;->a(Z)V

    .line 1646
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1647
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->a()Z

    goto :goto_1
.end method

.method private s()V
    .locals 2

    .prologue
    .line 1655
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 1656
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    new-instance v1, Lcom/twitter/android/GalleryActivity$9;

    invoke-direct {v1, p0}, Lcom/twitter/android/GalleryActivity$9;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 1667
    :cond_0
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 1830
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "tw"

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    .line 1831
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1832
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x23c1

    .line 1830
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1834
    return-void
.end method


# virtual methods
.method public S_()V
    .locals 2

    .prologue
    .line 1805
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1806
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    .line 1807
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    .line 1806
    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    .line 1807
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 1806
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1809
    :cond_0
    return-void
.end method

.method public a()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/MediaEntity;)Lbrm;
    .locals 6

    .prologue
    .line 1347
    new-instance v0, Lbrm;

    new-instance v1, Lbrn;

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    .line 1348
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lbrn;-><init>(ILjava/util/List;J)V

    invoke-direct {v0, p0, v1}, Lbrm;-><init>(Landroid/content/Context;Lbrn;)V

    .line 1347
    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 376
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 377
    const v1, 0x7f0402a2

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 378
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 379
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 380
    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lbrm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->Y:Ljava/util/Map;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1325
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->Y:Ljava/util/Map;

    .line 1327
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->Y:Ljava/util/Map;

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1328
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->j(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 1329
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1330
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 1331
    iget-object v3, v0, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    invoke-static {v3}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0}, Lcom/twitter/model/util/c;->f(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1332
    new-instance v3, Lbrm;

    new-instance v4, Lbrn;

    const/4 v5, 0x1

    iget-object v6, v0, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    .line 1334
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v4, v5, v6, v8, v9}, Lbrn;-><init>(ILjava/util/List;J)V

    invoke-direct {v3, p0, v4}, Lbrm;-><init>(Landroid/content/Context;Lbrn;)V

    .line 1335
    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1338
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->Y:Ljava/util/Map;

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 1341
    :goto_1
    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->Y:Ljava/util/Map;

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_1
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 968
    iput p1, p0, Lcom/twitter/android/GalleryActivity;->S:F

    .line 971
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    .line 972
    if-eqz v0, :cond_0

    .line 973
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_2

    .line 974
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 975
    neg-float v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTranslationY(F)V

    .line 982
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 983
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 985
    :cond_1
    return-void

    .line 977
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    neg-float v2, p1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 978
    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->setTranslationY(F)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 1457
    if-nez p1, :cond_2

    .line 1458
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->h()V

    .line 1463
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    .line 1471
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->requestFocus()Z

    .line 1473
    :cond_1
    return-void

    .line 1459
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0}, Lcom/twitter/android/cy;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1460
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->i()V

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 4

    .prologue
    .line 869
    packed-switch p2, :pswitch_data_0

    .line 887
    :cond_0
    :goto_0
    return-void

    .line 871
    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 872
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    const v1, 0x7f1308c8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 874
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 875
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 876
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->G:J

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    .line 877
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v1}, Lcom/twitter/model/util/c;->k(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 876
    invoke-direct {p0, v2, v3, v1, v0}, Lcom/twitter/android/GalleryActivity;->a(JLjava/util/List;Ljava/util/Set;)V

    .line 878
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/model/core/Tweet;)V

    .line 879
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0}, Lcom/twitter/android/cy;->c()V

    goto :goto_0

    .line 869
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1146
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1148
    new-instance v0, Lbga;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    iget-wide v2, p0, Lcom/twitter/android/GalleryActivity;->K:J

    invoke-direct {v0, p0, v1, v2, v3}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1157
    :goto_0
    return-void

    .line 1150
    :cond_0
    check-cast p1, Lcom/twitter/android/GalleryActivity$b;

    invoke-virtual {p1}, Lcom/twitter/android/GalleryActivity$b;->a()Ljava/util/List;

    move-result-object v0

    .line 1151
    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->T:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->Q:Z

    if-nez v1, :cond_1

    .line 1152
    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->R:Ljava/util/List;

    goto :goto_0

    .line 1154
    :cond_1
    invoke-direct {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 4

    .prologue
    .line 1553
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 1554
    packed-switch p2, :pswitch_data_0

    .line 1573
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    .line 1556
    check-cast v0, Lbfn;

    .line 1557
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-virtual {v0}, Lbfn;->b()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 1558
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1560
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1561
    const v0, 0x7f0a052a

    .line 1566
    :goto_1
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1563
    :cond_1
    const v0, 0x7f0a0529

    .line 1564
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    const v2, 0x7f1308c8

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lazv;->b(Z)Lazv;

    goto :goto_1

    .line 1554
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Lsl;Z)V
    .locals 0

    .prologue
    .line 1298
    invoke-virtual {p1, p2}, Lsl;->a(Z)V

    .line 1299
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 903
    if-eqz p1, :cond_0

    .line 904
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Lcom/twitter/library/widget/TweetView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    .line 908
    :goto_0
    return-void

    .line 906
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Lcom/twitter/library/widget/TweetView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(ZJLjava/lang/String;Lcgi;)V
    .locals 6

    .prologue
    .line 1825
    if-eqz p1, :cond_0

    const/4 v1, 0x6

    :goto_0
    move-object v0, p0

    move-wide v2, p2

    move-object v4, p5

    move-object v5, p4

    .line 1826
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/GalleryActivity;->a(IJLcgi;Ljava/lang/String;)V

    .line 1827
    return-void

    .line 1825
    :cond_0
    const/4 v1, 0x7

    goto :goto_0
.end method

.method public a(Lcmm;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 802
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 803
    const v1, 0x7f13089c

    if-ne v0, v1, :cond_2

    .line 804
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    invoke-virtual {v0}, Lcom/twitter/android/GalleryActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 805
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/GalleryActivity$a;->cancel(Z)Z

    .line 807
    :cond_0
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/GalleryActivity;->a:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 808
    new-instance v0, Lcom/twitter/android/GalleryActivity$a;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/GalleryActivity$a;-><init>(Landroid/content/Context;Lsj;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    .line 809
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    new-array v1, v2, [Lsl;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    iget v4, p0, Lcom/twitter/android/GalleryActivity;->k:I

    invoke-virtual {v3, v4}, Lsj;->b(I)Lsl;

    move-result-object v3

    aput-object v3, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    move v0, v2

    .line 864
    :goto_1
    return v0

    .line 811
    :cond_1
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    const v1, 0x7f0a07b7

    .line 812
    invoke-virtual {p0, v1}, Lcom/twitter/android/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/GalleryActivity;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, p0, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v1, "%s:%s:%s:"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->P:Ljava/lang/String;

    aput-object v4, v3, v5

    const-string/jumbo v4, ""

    aput-object v4, v3, v2

    const/4 v4, 0x2

    const-string/jumbo v5, "gallery"

    aput-object v5, v3, v4

    .line 814
    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 813
    invoke-virtual {v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 815
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 816
    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/GalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 819
    :cond_2
    const v1, 0x7f13089b

    if-ne v0, v1, :cond_7

    .line 820
    const/4 v0, 0x0

    .line 824
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    if-nez v1, :cond_4

    .line 825
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    if-eqz v1, :cond_3

    .line 826
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->F:Ljava/lang/String;

    .line 845
    :cond_3
    :goto_2
    if-eqz v0, :cond_8

    .line 846
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 847
    invoke-virtual {v1, v0, v5}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    .line 848
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 849
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 846
    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v2

    .line 850
    goto :goto_1

    .line 829
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    .line 830
    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v3}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v3

    .line 831
    if-eqz v1, :cond_6

    .line 832
    invoke-virtual {v1}, Lcax;->c()Ljava/lang/String;

    move-result-object v1

    .line 835
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/v;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 836
    iget-object v4, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 837
    iget-object v0, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    goto :goto_2

    .line 841
    :cond_6
    if-eqz v3, :cond_3

    .line 842
    iget-object v0, v3, Lcom/twitter/model/core/MediaEntity;->F:Ljava/lang/String;

    goto :goto_2

    .line 852
    :cond_7
    const v1, 0x7f1308c8

    if-ne v0, v1, :cond_9

    .line 853
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a075b

    .line 854
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0616

    .line 855
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 856
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 857
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 858
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 859
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 864
    :cond_8
    :goto_3
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto/16 :goto_1

    .line 860
    :cond_9
    const v1, 0x7f13088e

    if-ne v0, v1, :cond_8

    .line 861
    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->showDialog(I)V

    goto :goto_3

    :cond_a
    move-object v0, v1

    goto/16 :goto_2
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 747
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    .line 748
    const v0, 0x7f140011

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 749
    const v0, 0x7f140025

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 750
    const v0, 0x7f14000b

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 751
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 12

    .prologue
    const v11, 0x7f13089b

    const v10, 0x7f13088e

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 757
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Lcmr;)I

    move-result v1

    .line 758
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 759
    instance-of v4, p1, Lcom/twitter/library/widget/StatusToolBar;

    if-eqz v4, :cond_0

    .line 760
    check-cast p1, Lcom/twitter/library/widget/StatusToolBar;

    invoke-virtual {p1, v3}, Lcom/twitter/library/widget/StatusToolBar;->setDisplayShowStatusBarEnabled(Z)V

    .line 762
    :cond_0
    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    iget v5, p0, Lcom/twitter/android/GalleryActivity;->k:I

    .line 763
    invoke-virtual {v4, v5}, Lsj;->a(I)Lsn;

    move-result-object v4

    move-object v5, v4

    .line 764
    :goto_0
    if-eqz v5, :cond_b

    .line 765
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->Q()Lcom/twitter/library/media/manager/g;

    move-result-object v1

    iget-object v4, v5, Lsn;->c:Lcom/twitter/media/request/a$a;

    invoke-virtual {v1, v4}, Lcom/twitter/library/media/manager/g;->b(Lcom/twitter/media/request/a$a;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 766
    :goto_1
    const v4, 0x7f13089c

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v6

    if-eqz v1, :cond_5

    .line 767
    invoke-virtual {v5}, Lsn;->c()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    .line 768
    invoke-virtual {v4}, Lcom/twitter/android/GalleryActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v7, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v4, v7, :cond_5

    :cond_1
    move v4, v2

    .line 767
    :goto_2
    invoke-virtual {v6, v4}, Lazv;->c(Z)Lazv;

    move-result-object v4

    .line 769
    invoke-virtual {v5}, Lsn;->c()Z

    move-result v6

    invoke-virtual {v4, v6}, Lazv;->b(Z)Lazv;

    .line 771
    iget-boolean v4, p0, Lcom/twitter/android/GalleryActivity;->z:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    if-nez v4, :cond_6

    .line 772
    :cond_2
    invoke-virtual {v0, v11}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    invoke-virtual {v1, v3}, Lazv;->b(Z)Lazv;

    .line 773
    invoke-virtual {v0, v10}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-virtual {v0, v3}, Lazv;->b(Z)Lazv;

    .line 795
    :goto_3
    const/4 v0, 0x2

    .line 797
    :goto_4
    return v0

    .line 763
    :cond_3
    const/4 v4, 0x0

    move-object v5, v4

    goto :goto_0

    :cond_4
    move v1, v3

    .line 765
    goto :goto_1

    :cond_5
    move v4, v3

    .line 768
    goto :goto_2

    .line 775
    :cond_6
    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    sget-object v6, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 776
    invoke-static {v4, v6}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v4

    .line 777
    iget-object v6, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    .line 778
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/twitter/model/util/d;->a(Ljava/util/List;J)Z

    move-result v4

    .line 779
    const v6, 0x7f1308c8

    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v6

    invoke-virtual {v6, v4}, Lazv;->b(Z)Lazv;

    .line 780
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v4

    if-nez v4, :cond_a

    .line 783
    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v6, v4, Lcom/twitter/model/core/Tweet;->b:J

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    cmp-long v4, v6, v8

    if-nez v4, :cond_7

    move v4, v2

    .line 784
    :goto_5
    invoke-virtual {v0, v10}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v6

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v4

    if-nez v4, :cond_8

    move v4, v2

    :goto_6
    invoke-virtual {v6, v4}, Lazv;->b(Z)Lazv;

    .line 786
    invoke-virtual {v0, v11}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 787
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->d()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 788
    invoke-virtual {v5}, Lsn;->b()I

    move-result v4

    if-ne v4, v2, :cond_9

    .line 787
    :goto_7
    invoke-virtual {v0, v2}, Lazv;->b(Z)Lazv;

    move-result-object v0

    .line 789
    invoke-virtual {v0, v1}, Lazv;->c(Z)Lazv;

    goto :goto_3

    :cond_7
    move v4, v3

    .line 783
    goto :goto_5

    :cond_8
    move v4, v3

    .line 784
    goto :goto_6

    :cond_9
    move v2, v3

    .line 788
    goto :goto_7

    .line 791
    :cond_a
    invoke-virtual {v0, v10}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    invoke-virtual {v1, v3}, Lazv;->b(Z)Lazv;

    .line 792
    invoke-virtual {v0, v11}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-virtual {v0, v3}, Lazv;->b(Z)Lazv;

    goto :goto_3

    :cond_b
    move v0, v1

    .line 797
    goto :goto_4
.end method

.method public b()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 1482
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->h:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 912
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Lcom/twitter/library/widget/TweetView;

    .line 913
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    .line 915
    new-instance v2, Lcom/twitter/android/GalleryActivity$6;

    invoke-direct {v2, p0}, Lcom/twitter/android/GalleryActivity$6;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    .line 927
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setAutoLink(Z)V

    .line 928
    if-eqz v1, :cond_0

    .line 929
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 931
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 14

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    .line 387
    invoke-static {v12}, Lcom/twitter/ui/anim/b;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_b

    if-nez p1, :cond_b

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->T:Z

    .line 388
    const-string/jumbo v0, "photo_impression"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->M:Ljava/lang/String;

    .line 389
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->M:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    const-string/jumbo v0, "photo:impression"

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->M:Ljava/lang/String;

    .line 393
    :cond_0
    const-string/jumbo v0, "association"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 394
    const-string/jumbo v0, "dm"

    const/4 v1, 0x0

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->z:Z

    .line 395
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    .line 398
    const v0, 0x7f13063f

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->V:Landroid/view/ViewGroup;

    .line 399
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->V:Landroid/view/ViewGroup;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v2, -0x1000000

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 401
    const v0, 0x7f130640

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/widget/SlidingPanel;

    .line 402
    new-instance v0, Lcom/twitter/android/cy;

    .line 403
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const v5, 0x7f0200b5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cy;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/widget/SlidingPanel;I)V

    .line 405
    invoke-virtual {v0, p0}, Lcom/twitter/android/cy;->a(Lcom/twitter/android/cy$c;)V

    .line 406
    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    .line 408
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Lcom/twitter/library/widget/SlidingPanel;->a(I)V

    .line 409
    invoke-virtual {v4, v0}, Lcom/twitter/library/widget/SlidingPanel;->setPanelSlideListener(Lcom/twitter/library/widget/SlidingUpPanelLayout$c;)V

    .line 410
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Lcom/twitter/library/widget/SlidingPanel;->setClipChildren(Z)V

    .line 411
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Lcom/twitter/library/widget/SlidingPanel;->setFadeMode(I)V

    .line 412
    const/high16 v1, -0x1000000

    invoke-virtual {v4, v1}, Lcom/twitter/library/widget/SlidingPanel;->setCoveredFadeColor(I)V

    .line 413
    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    .line 415
    const/4 v3, 0x0

    .line 416
    const/4 v2, 0x0

    .line 417
    const/4 v1, 0x0

    .line 418
    const-string/jumbo v4, "li"

    invoke-virtual {v12, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 419
    const-string/jumbo v3, "li"

    invoke-virtual {v12, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 420
    const-string/jumbo v4, "list_starting_index"

    invoke-virtual {v12, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 421
    const-string/jumbo v2, "list_starting_index"

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object v9, v1

    move v10, v2

    move-object v11, v3

    .line 451
    :goto_1
    const-string/jumbo v1, "media"

    invoke-virtual {v12, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 452
    const-string/jumbo v1, "media"

    sget-object v2, Lcom/twitter/model/core/MediaEntity;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v12, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/MediaEntity;

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    .line 455
    :cond_1
    const-string/jumbo v1, "source_tweet_id"

    invoke-virtual {v12, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 456
    if-eqz v9, :cond_2

    .line 457
    const-string/jumbo v1, "sourceStatusId"

    const-string/jumbo v2, "source_tweet_id"

    const-wide/16 v4, -0x1

    .line 458
    invoke-virtual {v12, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 457
    invoke-virtual {v9, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 462
    :cond_2
    const-string/jumbo v1, "promoted_content"

    invoke-virtual {v12, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 463
    const-string/jumbo v1, "promoted_content"

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 464
    invoke-static {v12, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcgi;

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->m:Lcgi;

    .line 467
    :cond_3
    const-string/jumbo v1, "etc"

    const/4 v2, 0x1

    .line 468
    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    .line 470
    const-string/jumbo v1, "context"

    const/4 v2, -0x1

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/android/GalleryActivity;->e(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->P:Ljava/lang/String;

    .line 471
    const-string/jumbo v1, "item"

    invoke-virtual {v12, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->x:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 473
    new-instance v1, Lsj;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->L:Lsp;

    iget-object v5, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    new-instance v6, Lsm;

    invoke-direct {v6}, Lsm;-><init>()V

    const-string/jumbo v2, "should_show_sticker_visual_hashtags"

    const/4 v7, 0x1

    .line 476
    invoke-virtual {v12, v2, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    move-object v2, p0

    move-object v7, p0

    invoke-direct/range {v1 .. v8}, Lsj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lsm;Lsj$a;Z)V

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    .line 477
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-virtual {v1, p0}, Lsj;->a(Lcom/twitter/ui/anim/c$a;)V

    .line 478
    const v1, 0x7f130378

    invoke-virtual {p0, v1}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    .line 480
    const-string/jumbo v2, "page_cache_size"

    invoke-virtual {v12, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 481
    const-string/jumbo v2, "page_cache_size"

    const/4 v3, 0x3

    invoke-virtual {v12, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 485
    :goto_2
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 486
    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 487
    invoke-virtual {v1, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 488
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 489
    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/support/v4/view/ViewPager;

    .line 491
    new-instance v2, Lcom/twitter/android/GalleryActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/GalleryActivity$1;-><init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/android/cy;)V

    .line 505
    new-instance v3, Landroid/support/v4/view/GestureDetectorCompat;

    .line 506
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 507
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/support/v4/view/GestureDetectorCompat;->setIsLongpressEnabled(Z)V

    .line 509
    const v2, 0x7f130641

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/widget/FrameLayout;

    .line 510
    const v2, 0x7f130643

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/twitter/android/GalleryActivity;->d:Landroid/widget/LinearLayout;

    .line 511
    const v2, 0x7f130642

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    .line 512
    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    new-instance v4, Lcom/twitter/android/GalleryActivity$2;

    invoke-direct {v4, p0, v3, v1}, Lcom/twitter/android/GalleryActivity$2;-><init>(Lcom/twitter/android/GalleryActivity;Landroid/support/v4/view/GestureDetectorCompat;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 520
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->s()V

    .line 522
    const v1, 0x7f130644

    invoke-virtual {p0, v1}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->t:Landroid/widget/TextView;

    .line 524
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    const v2, 0x7f130645

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/TweetView;

    .line 525
    const/4 v2, 0x1

    .line 526
    iget-boolean v3, p0, Lcom/twitter/android/GalleryActivity;->z:Z

    if-eqz v3, :cond_17

    .line 527
    const/4 v2, 0x0

    move v3, v2

    .line 529
    :goto_3
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    .line 530
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setHideMediaTagSummary(Z)V

    .line 531
    if-eqz v13, :cond_4

    .line 532
    new-instance v2, Lcom/twitter/android/GalleryActivity$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/GalleryActivity$3;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 540
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setAlwaysStripMediaUrls(Z)V

    .line 541
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setPromotedBadgeEnabled(Z)V

    .line 542
    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->n:Lcom/twitter/library/widget/TweetView;

    .line 544
    const v2, 0x7f13051f

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/av/GalleryVideoChromeView;

    iput-object v2, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/av/GalleryVideoChromeView;

    .line 546
    new-instance v2, Lcom/twitter/android/l;

    invoke-direct {v2, p0}, Lcom/twitter/android/l;-><init>(Lcom/twitter/android/m;)V

    .line 549
    const v4, 0x7f05002f

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 550
    invoke-virtual {v4, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 551
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 552
    const-wide/16 v6, 0x96

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 553
    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/view/animation/Animation;

    .line 555
    const v4, 0x7f050031

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 556
    invoke-virtual {v4, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 557
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 558
    const-wide/16 v6, 0x96

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 559
    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->h:Landroid/view/animation/Animation;

    .line 561
    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    const/16 v4, 0x600

    invoke-static {v2, v4}, Lcom/twitter/util/d;->a(Landroid/view/View;I)V

    .line 563
    if-nez p1, :cond_11

    .line 565
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    .line 567
    const-string/jumbo v2, "tagged_user_list"

    const/4 v4, 0x0

    .line 568
    invoke-virtual {v12, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/twitter/android/cy;->a:Z

    .line 578
    :goto_4
    if-eqz v9, :cond_12

    .line 579
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v9, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 580
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 603
    :cond_5
    :goto_5
    if-eqz v3, :cond_6

    .line 604
    const v1, 0x7f1302e4

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->P:Ljava/lang/String;

    const-string/jumbo v4, ""

    const-string/jumbo v5, "gallery"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/android/MediaActionBarFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/android/MediaActionBarFragment;

    .line 607
    invoke-static {}, Lbpm;->a()Lbpm;

    move-result-object v0

    invoke-virtual {v0}, Lbpm;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 608
    new-instance v0, Lbpl;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/widget/FrameLayout;

    invoke-direct {v0, v1}, Lbpl;-><init>(Landroid/widget/FrameLayout;)V

    .line 611
    invoke-static {}, Lbpm;->a()Lbpm;

    move-result-object v1

    invoke-virtual {v1}, Lbpm;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbpl;->a(Ljava/util/List;)V

    .line 612
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/android/MediaActionBarFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/MediaActionBarFragment;->a(Lbpl;)V

    .line 616
    :cond_6
    const-string/jumbo v0, "saved_task"

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/GalleryActivity$a;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    .line 618
    const-string/jumbo v0, "sticker_repos"

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 619
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Map;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 620
    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->Y:Ljava/util/Map;

    .line 623
    :cond_7
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 624
    new-instance v0, Lcom/twitter/library/av/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->X:Lcom/twitter/library/av/b;

    .line 627
    :cond_8
    const-string/jumbo v0, "show_tw"

    const/4 v1, 0x1

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string/jumbo v0, "statusId"

    .line 628
    invoke-virtual {v12, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string/jumbo v0, "prj"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_9
    const/4 v0, 0x1

    .line 629
    :goto_6
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->L:Lsp;

    invoke-static {}, Lsk;->a()Lsk$a;

    move-result-object v2

    .line 630
    invoke-virtual {v2, v0}, Lsk$a;->a(Z)Lsk$a;

    move-result-object v0

    const-string/jumbo v2, "tw_link"

    const/4 v3, 0x0

    .line 631
    invoke-virtual {v12, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lsk$a;->a(I)Lsk$a;

    move-result-object v0

    .line 632
    invoke-virtual {v0}, Lsk$a;->a()Lsk;

    move-result-object v0

    .line 629
    invoke-virtual {v1, v0}, Lsp;->a(Lsk;)V

    .line 634
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->n()V

    .line 636
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->T:Z

    if-eqz v0, :cond_a

    .line 638
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->i()V

    .line 640
    :cond_a
    :goto_7
    return-void

    .line 387
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 423
    :cond_c
    iget-boolean v4, p0, Lcom/twitter/android/GalleryActivity;->z:Z

    if-eqz v4, :cond_d

    move-object v9, v1

    move v10, v2

    move-object v11, v3

    goto/16 :goto_1

    .line 425
    :cond_d
    const-string/jumbo v1, "statusId"

    invoke-virtual {v12, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 426
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 427
    const-string/jumbo v4, "statusId"

    const-wide/16 v6, 0x0

    invoke-virtual {v12, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/GalleryActivity;->K:J

    .line 428
    const-string/jumbo v4, "uri"

    iget-wide v6, p0, Lcom/twitter/android/GalleryActivity;->K:J

    .line 429
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/twitter/database/schema/a;->b(JJ)Landroid/net/Uri;

    move-result-object v5

    .line 428
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 431
    const-string/jumbo v4, "projection"

    sget-object v5, Lbuj;->b:[Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    move-object v9, v1

    move v10, v2

    move-object v11, v3

    goto/16 :goto_1

    .line 432
    :cond_e
    invoke-virtual {v12}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 433
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 434
    const-string/jumbo v4, "uri"

    invoke-virtual {v12}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 435
    const-string/jumbo v4, "projection"

    const-string/jumbo v5, "prj"

    .line 436
    invoke-virtual {v12, v5}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 435
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 437
    const-string/jumbo v4, "selection"

    const-string/jumbo v5, "sel"

    .line 438
    invoke-virtual {v12, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 437
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string/jumbo v4, "selectionArgs"

    const-string/jumbo v5, "selArgs"

    .line 441
    invoke-virtual {v12, v5}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 439
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 442
    const-string/jumbo v4, "orderBy"

    const-string/jumbo v5, "orderBy"

    .line 443
    invoke-virtual {v12, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 442
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string/jumbo v4, "id"

    const-wide/high16 v6, -0x8000000000000000L

    invoke-virtual {v12, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/GalleryActivity;->p:J

    move-object v9, v1

    move v10, v2

    move-object v11, v3

    goto/16 :goto_1

    .line 447
    :cond_f
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->finish()V

    goto/16 :goto_7

    .line 483
    :cond_10
    const/4 v2, 0x3

    goto/16 :goto_2

    .line 570
    :cond_11
    const-string/jumbo v2, "cv"

    iget-boolean v4, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    .line 572
    const-string/jumbo v2, "current_position"

    iget v4, p0, Lcom/twitter/android/GalleryActivity;->k:I

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/GalleryActivity;->k:I

    .line 574
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->Z:Z

    .line 575
    invoke-virtual {v0, p1}, Lcom/twitter/android/cy;->b(Landroid/os/Bundle;)V

    goto/16 :goto_4

    .line 581
    :cond_12
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->z:Z

    if-eqz v0, :cond_13

    .line 582
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/GalleryActivity;->k:I

    .line 583
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lsj;->a(Lcom/twitter/model/core/MediaEntity;Z)V

    .line 584
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->o()V

    .line 585
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->b(Z)V

    .line 586
    const v0, 0x7f0a0406

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(I)V

    goto/16 :goto_5

    .line 587
    :cond_13
    if-eqz v11, :cond_5

    .line 588
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-virtual {v0, v11}, Lsj;->b(Ljava/util/List;)V

    .line 589
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    .line 590
    const/4 v3, 0x0

    .line 591
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_14

    .line 592
    const v0, 0x7f0a098b

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(I)V

    goto/16 :goto_5

    .line 596
    :cond_14
    if-nez v10, :cond_15

    .line 597
    invoke-virtual {p0, v10}, Lcom/twitter/android/GalleryActivity;->onPageSelected(I)V

    .line 599
    :cond_15
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v10}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_5

    .line 628
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_6

    :cond_17
    move v3, v2

    goto/16 :goto_3

    :cond_18
    move-object v9, v1

    move v10, v2

    move-object v11, v3

    goto/16 :goto_1
.end method

.method public b(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x600

    .line 1436
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    if-ne v0, p1, :cond_0

    .line 1453
    :goto_0
    return-void

    .line 1439
    :cond_0
    if-eqz p1, :cond_3

    .line 1440
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/widget/FrameLayout;

    invoke-static {v0, v1}, Lcom/twitter/util/d;->a(Landroid/view/View;I)V

    .line 1442
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    if-nez v0, :cond_1

    .line 1443
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/twitter/util/d;->a(Landroid/view/View;I)V

    .line 1445
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1452
    :cond_2
    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    goto :goto_0

    .line 1447
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1448
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0}, Lcom/twitter/android/cy;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1449
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/widget/FrameLayout;

    invoke-static {v0}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method public c()Lcom/twitter/android/av/GalleryVideoChromeView;
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/av/GalleryVideoChromeView;

    return-object v0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 941
    iget v0, p0, Lcom/twitter/android/GalleryActivity;->O:I

    if-ne p1, v0, :cond_0

    .line 942
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 943
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 944
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 947
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1813
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->b(Z)V

    .line 1814
    if-nez p1, :cond_0

    .line 1815
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    iget v2, p0, Lcom/twitter/android/GalleryActivity;->k:I

    invoke-virtual {v0, v2}, Lsj;->b(I)Lsl;

    move-result-object v0

    .line 1816
    if-eqz v0, :cond_0

    .line 1817
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(Lsl;Z)V

    .line 1820
    :cond_0
    return-void

    .line 1813
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1100
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1101
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 1108
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    if-eqz v0, :cond_1

    .line 1109
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/GalleryActivity;->k:I

    .line 1110
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-virtual {v0}, Lsj;->d()V

    .line 1111
    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    .line 1113
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 1114
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1115
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1116
    invoke-virtual {v0, v2, v2}, Landroid/view/ViewGroup;->measure(II)V

    .line 1117
    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/support/v4/view/ViewPager;

    .line 1120
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->X:Lcom/twitter/library/av/b;

    if-eqz v0, :cond_3

    .line 1121
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->X:Lcom/twitter/library/av/b;

    invoke-virtual {v0}, Lcom/twitter/library/av/b;->a()V

    .line 1122
    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->X:Lcom/twitter/library/av/b;

    .line 1125
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->W:Lsi;

    if-eqz v0, :cond_4

    .line 1126
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->W:Lsi;

    invoke-virtual {v0}, Lsi;->c()V

    .line 1129
    :cond_4
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 1130
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    const v2, 0x7f05003c

    .line 951
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->T:Z

    if-eqz v0, :cond_0

    .line 952
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->p()V

    .line 964
    :goto_0
    return-void

    .line 954
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->finish()V

    .line 956
    iget v0, p0, Lcom/twitter/android/GalleryActivity;->S:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 957
    const v0, 0x7f05003e

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/GalleryActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 960
    :cond_1
    const v0, 0x7f05003d

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/GalleryActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method protected l_()V
    .locals 2

    .prologue
    .line 896
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->l_()V

    .line 897
    const-string/jumbo v0, "saved_task"

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 898
    const-string/jumbo v0, "sticker_repos"

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->Y:Ljava/util/Map;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1528
    sparse-switch p1, :sswitch_data_0

    .line 1543
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    if-eqz v0, :cond_0

    .line 1544
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/cy;->a(IILandroid/content/Intent;)V

    .line 1549
    :cond_0
    :goto_0
    return-void

    .line 1530
    :sswitch_0
    const/16 v0, 0xa

    if-ne v0, p2, :cond_0

    .line 1531
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->finish()V

    goto :goto_0

    .line 1536
    :sswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1537
    new-instance v0, Lcom/twitter/android/GalleryActivity$a;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/GalleryActivity$a;-><init>(Landroid/content/Context;Lsj;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    .line 1538
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Lcom/twitter/android/GalleryActivity$a;

    const/4 v1, 0x1

    new-array v1, v1, [Lsl;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    iget v4, p0, Lcom/twitter/android/GalleryActivity;->k:I

    invoke-virtual {v3, v4}, Lsj;->b(I)Lsl;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1528
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x23c1 -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 725
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0}, Lcom/twitter/android/cy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/cy;->a(Z)V

    .line 730
    :goto_0
    return-void

    .line 728
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->j()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 689
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 692
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 694
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-eqz v0, :cond_1

    .line 699
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->E:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/GalleryActivity$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/GalleryActivity$5;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 710
    :cond_1
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1590
    .line 1591
    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    .line 1615
    :goto_0
    return-object v0

    .line 1593
    :pswitch_0
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->z:Z

    if-eqz v0, :cond_1

    .line 1597
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    if-nez v0, :cond_0

    .line 1598
    const-string/jumbo v0, "ANATOMY-371: attempting to delete a null DM photo"

    .line 1602
    :goto_1
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 1603
    goto :goto_0

    .line 1600
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ANATOMY-371: attempting to delete DM photo #"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/model/core/MediaEntity;

    iget-wide v2, v2, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1604
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_2

    .line 1605
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "ANATOMY-371: attempting to delete a photo but mTweet is null"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 1608
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/model/core/Tweet;

    invoke-static {p0, v0, p1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/core/Tweet;I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 1591
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1134
    const-string/jumbo v0, "uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 1135
    const-string/jumbo v0, "projection"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1136
    const-string/jumbo v0, "selection"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1137
    const-string/jumbo v0, "selectionArgs"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1138
    const-string/jumbo v0, "orderBy"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1139
    const-string/jumbo v0, "sourceStatusId"

    const-wide/16 v8, -0x1

    invoke-virtual {p2, v0, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 1140
    new-instance v1, Lcom/twitter/android/GalleryActivity$b;

    move-object v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/GalleryActivity$b;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)V

    return-object v1
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 144
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/GalleryActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    if-eqz v0, :cond_0

    .line 1208
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsj;->a(Ljava/util/List;)V

    .line 1210
    :cond_0
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 1432
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 1214
    return-void
.end method

.method public onPageSelected(I)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1218
    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    .line 1219
    invoke-virtual {v4}, Lsj;->getCount()I

    move-result v0

    .line 1220
    if-lez v0, :cond_7

    .line 1222
    if-le v0, v3, :cond_8

    .line 1223
    const v1, 0x7f0a0697

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, p1, 0x1

    .line 1224
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 1225
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v3

    .line 1223
    invoke-virtual {p0, v1, v5}, Lcom/twitter/android/GalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1229
    :goto_0
    invoke-virtual {v4, p1}, Lsj;->b(I)Lsl;

    move-result-object v0

    .line 1230
    if-eqz v0, :cond_7

    .line 1231
    iget v5, p0, Lcom/twitter/android/GalleryActivity;->k:I

    .line 1237
    iget v1, p0, Lcom/twitter/android/GalleryActivity;->k:I

    if-ne p1, v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->Z:Z

    if-eqz v1, :cond_9

    :cond_0
    move v1, v3

    .line 1238
    :goto_1
    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->Z:Z

    .line 1239
    invoke-virtual {v0}, Lsl;->b()Lcom/twitter/model/core/Tweet;

    move-result-object v6

    .line 1240
    if-eqz v6, :cond_2

    .line 1243
    iget-object v7, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    iget-boolean v8, v7, Lcom/twitter/android/cy;->b:Z

    or-int/2addr v8, v1

    iput-boolean v8, v7, Lcom/twitter/android/cy;->b:Z

    if-eqz v8, :cond_1

    .line 1244
    iget-object v7, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v7}, Lcom/twitter/android/cy;->c()V

    .line 1246
    :cond_1
    invoke-direct {p0, v6}, Lcom/twitter/android/GalleryActivity;->b(Lcom/twitter/model/core/Tweet;)V

    .line 1248
    if-eq v5, v9, :cond_2

    .line 1249
    invoke-direct {p0, v5, p1, v6}, Lcom/twitter/android/GalleryActivity;->a(IILcom/twitter/model/core/Tweet;)V

    .line 1252
    sget-object v7, Lcom/twitter/library/api/PromotedEvent;->p:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {p0, v6, v7}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;)V

    .line 1256
    :cond_2
    if-eqz v1, :cond_6

    .line 1257
    invoke-virtual {v4, v5}, Lsj;->b(I)Lsl;

    move-result-object v1

    .line 1258
    if-eqz v1, :cond_3

    .line 1259
    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/GalleryActivity;->a(Lsl;Z)V

    .line 1262
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v1}, Lcom/twitter/android/cy;->b()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1263
    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/GalleryActivity;->a(Lsl;Z)V

    .line 1267
    :cond_4
    if-eq v5, v9, :cond_5

    instance-of v1, v0, Lso;

    if-eqz v1, :cond_5

    .line 1269
    check-cast v0, Lso;

    invoke-virtual {v0}, Lso;->e()V

    .line 1272
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->X:Lcom/twitter/library/av/b;

    if-eqz v0, :cond_6

    .line 1274
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->X:Lcom/twitter/library/av/b;

    new-instance v1, Lcom/twitter/library/av/s;

    .line 1275
    invoke-virtual {v4}, Lsj;->a()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/library/av/s;-><init>(Ljava/util/List;)V

    .line 1274
    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/av/b;->a(Lcom/twitter/library/av/p;I)V

    .line 1281
    :cond_6
    iget v0, p0, Lcom/twitter/android/GalleryActivity;->O:I

    if-eq p1, v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    .line 1282
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7

    .line 1283
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->N:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1287
    :cond_7
    iput p1, p0, Lcom/twitter/android/GalleryActivity;->k:I

    .line 1288
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 1289
    return-void

    .line 1227
    :cond_8
    const v0, 0x7f0a098b

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_9
    move v1, v2

    .line 1237
    goto/16 :goto_1
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 668
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 670
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0}, Lcom/twitter/android/cy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/cy;->a(Z)V

    .line 674
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    if-eqz v0, :cond_1

    .line 675
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(I)V

    .line 679
    :goto_0
    return-void

    .line 677
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(I)V

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 1081
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onRestart()V

    .line 1082
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->b(Z)V

    .line 1083
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-virtual {v0}, Lsj;->c()V

    .line 1085
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    iget v1, p0, Lcom/twitter/android/GalleryActivity;->k:I

    invoke-virtual {v0, v1}, Lsj;->b(I)Lsl;

    move-result-object v0

    .line 1087
    if-eqz v0, :cond_0

    .line 1088
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(Lsl;Z)V

    .line 1090
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1065
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1066
    const-string/jumbo v0, "cv"

    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1067
    const-string/jumbo v0, "current_position"

    iget v1, p0, Lcom/twitter/android/GalleryActivity;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1068
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0, p1}, Lcom/twitter/android/cy;->a(Landroid/os/Bundle;)V

    .line 1069
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->f:Lsj;

    invoke-virtual {v0}, Lsj;->b()V

    .line 1095
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 1096
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 1073
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onWindowFocusChanged(Z)V

    .line 1074
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->D:Lcom/twitter/android/cy;

    invoke-virtual {v0}, Lcom/twitter/android/cy;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1075
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    .line 1077
    :cond_0
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 714
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->T:Z

    if-eqz v0, :cond_0

    .line 715
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->j()V

    .line 721
    :goto_0
    return-void

    .line 716
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->z:Z

    if-eqz v0, :cond_1

    .line 717
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 719
    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->p()V

    goto :goto_0
.end method
