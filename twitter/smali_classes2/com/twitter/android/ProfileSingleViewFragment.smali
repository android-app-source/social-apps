.class public abstract Lcom/twitter/android/ProfileSingleViewFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewStub$OnInflateListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/widget/ScrollingHeaderListFragment",
        "<",
        "Landroid/database/Cursor;",
        "Lcjr",
        "<",
        "Landroid/database/Cursor;",
        ">;>;",
        "Landroid/view/ViewStub$OnInflateListener;"
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 37
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 38
    invoke-virtual {p0}, Lcom/twitter/android/ProfileSingleViewFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    .line 39
    iget-object v0, v2, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 43
    iget-object v0, v2, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, v2, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ScrollView;

    .line 45
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setFocusableInTouchMode(Z)V

    .line 46
    const/high16 v3, 0x20000

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setDescendantFocusability(I)V

    .line 49
    :cond_0
    iget-object v0, v2, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    const v2, 0x7f1306a0

    .line 50
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 51
    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/twitter/android/ProfileSingleViewFragment;->f()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 53
    invoke-virtual {v0, p0}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    .line 54
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 57
    :cond_1
    return-object v1
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 70
    const v0, 0x7f040396

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0402ca

    .line 71
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 72
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const v0, 0x7f0403e0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 74
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 76
    :cond_0
    return-void
.end method

.method protected ap_()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b()V

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/ProfileSingleViewFragment;->T()V

    .line 82
    return-void
.end method

.method protected abstract f()I
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/twitter/android/ProfileSingleViewFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/ProfileSingleViewFragment;->a:Lcom/twitter/model/core/TwitterUser;

    .line 31
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/twitter/android/ProfileSingleViewFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileSingleViewFragment;->T:Landroid/content/Context;

    invoke-static {v1}, Lcjr;->b(Landroid/content/Context;)Lcjr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 65
    return-void
.end method
