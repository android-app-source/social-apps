.class public Lcom/twitter/android/ao$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/MediaListFragment$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:J

.field public final b:Lcom/twitter/model/core/Tweet;

.field public final c:Lcom/twitter/model/core/MediaEntity;

.field public d:Lcax;


# direct methods
.method constructor <init>(JLcom/twitter/model/core/Tweet;Lcax;)V
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/ao$a;-><init>(JLcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;)V

    .line 255
    iput-object p4, p0, Lcom/twitter/android/ao$a;->d:Lcax;

    .line 256
    return-void
.end method

.method constructor <init>(JLcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;)V
    .locals 1

    .prologue
    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    iput-wide p1, p0, Lcom/twitter/android/ao$a;->a:J

    .line 249
    iput-object p3, p0, Lcom/twitter/android/ao$a;->b:Lcom/twitter/model/core/Tweet;

    .line 250
    iput-object p4, p0, Lcom/twitter/android/ao$a;->c:Lcom/twitter/model/core/MediaEntity;

    .line 251
    return-void
.end method


# virtual methods
.method public a()F
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/twitter/android/ao$a;->d:Lcax;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/twitter/android/ao$a;->d:Lcax;

    invoke-virtual {v0}, Lcax;->q()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_1

    .line 262
    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/model/card/property/Vector2F;->x:F

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/model/card/property/Vector2F;->y:F

    invoke-static {v1, v0}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    .line 268
    :goto_0
    return v0

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ao$a;->c:Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/twitter/android/ao$a;->c:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    goto :goto_0

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/android/ao$a;->d:Lcax;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/twitter/android/ao$a;->d:Lcax;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcax;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 278
    :goto_0
    return-object v0

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ao$a;->c:Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/twitter/android/ao$a;->c:Lcom/twitter/model/core/MediaEntity;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    goto :goto_0

    .line 278
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 284
    iget-wide v0, p0, Lcom/twitter/android/ao$a;->a:J

    return-wide v0
.end method

.method public d()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/twitter/android/ao$a;->b:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public e()Lcom/twitter/model/core/MediaEntity;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/android/ao$a;->c:Lcom/twitter/model/core/MediaEntity;

    return-object v0
.end method
