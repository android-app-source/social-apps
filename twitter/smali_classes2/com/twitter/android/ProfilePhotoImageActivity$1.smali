.class Lcom/twitter/android/ProfilePhotoImageActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ProfilePhotoImageActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfilePhotoImageActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfilePhotoImageActivity;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/twitter/android/ProfilePhotoImageActivity$1;->a:Lcom/twitter/android/ProfilePhotoImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 26
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ProfilePhotoImageActivity$1;->a:Lcom/twitter/android/ProfilePhotoImageActivity;

    invoke-static {v1}, Lcom/twitter/android/ProfilePhotoImageActivity;->a(Lcom/twitter/android/ProfilePhotoImageActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "profile"

    aput-object v3, v1, v2

    const-string/jumbo v2, "avatar_detail"

    aput-object v2, v1, v4

    const/4 v2, 0x0

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const-string/jumbo v3, "edit_button"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 26
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 28
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoImageActivity$1;->a:Lcom/twitter/android/ProfilePhotoImageActivity;

    invoke-static {v0, v4}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/twitter/android/ProfilePhotoImageActivity$1;->a:Lcom/twitter/android/ProfilePhotoImageActivity;

    invoke-virtual {v1, v0, v5}, Lcom/twitter/android/ProfilePhotoImageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 31
    return-void
.end method
