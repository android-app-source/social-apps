.class public Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/TweetListFragment",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        "Lcom/twitter/android/cv;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:Z

.field private d:Z

.field private e:Lcom/twitter/model/util/FriendshipCache;

.field private f:Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment$a;

.field private g:Lcom/twitter/android/UsersAdapter;

.field private h:Z

.field private i:Lcom/twitter/android/widget/ap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 316
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 317
    return-void
.end method


# virtual methods
.method protected H_()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->f:Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment$a;->j()V

    .line 152
    return-void
.end method

.method protected L_()I
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->g:Lcom/twitter/android/UsersAdapter;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->g:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    new-instance v1, Lcbe;

    invoke-direct {v1, p2}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 143
    :cond_0
    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 247
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->i:Lcom/twitter/android/widget/ap;

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ap;->b(I)I

    move-result v0

    .line 248
    iget-boolean v1, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->h:Z

    if-eqz v1, :cond_1

    if-ne v0, v2, :cond_1

    .line 249
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 250
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    const/4 v3, 0x2

    .line 251
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 250
    invoke-virtual {p0, v0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->startActivity(Landroid/content/Intent;)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    iget-boolean v1, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->h:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    :cond_2
    if-ne v0, v2, :cond_0

    .line 253
    :cond_3
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 254
    new-instance v1, Lcom/twitter/app/common/base/h;

    invoke-direct {v1}, Lcom/twitter/app/common/base/h;-><init>()V

    const/4 v2, 0x0

    .line 255
    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "tw"

    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 257
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    .line 258
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 254
    invoke-virtual {p0, v0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected a(Lcbi;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->c:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcbi;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/news/NewsDetailActivity;

    .line 127
    invoke-virtual {v0}, Lcom/twitter/android/news/NewsDetailActivity;->i()V

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->c:Z

    .line 132
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->d:Z

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->g()V

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->d:Z

    .line 136
    :cond_0
    return-void

    .line 130
    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->b(Lcbi;)V

    goto :goto_0
.end method

.method public a(Lcno;IIIZ)V
    .locals 3

    .prologue
    .line 322
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/TweetListFragment;->a(Lcno;IIIZ)V

    .line 325
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ScrollingHeaderActivity;

    .line 327
    iget-boolean v1, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->Q:Z

    if-eqz v1, :cond_0

    .line 328
    iget v1, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->M:I

    iget v2, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->L:I

    sub-int/2addr v1, v2

    .line 329
    neg-int v1, v1

    iget v2, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->N:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ScrollingHeaderActivity;->b(II)V

    .line 332
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 62
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 8

    .prologue
    .line 289
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 291
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->S:Lcom/twitter/library/client/p;

    new-instance v1, Lbhs;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 292
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v6

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 291
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 293
    const-string/jumbo v0, "unfollow"

    .line 300
    :goto_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 301
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ":user_module:user"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-direct {p0, v0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->b(Ljava/lang/String;)V

    .line 303
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 296
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->S:Lcom/twitter/library/client/p;

    new-instance v1, Lbhq;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 297
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v6

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 296
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 298
    const-string/jumbo v0, "follow"

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 3

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Z)V

    .line 109
    iget-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->h:Z

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 112
    :cond_0
    return-void
.end method

.method protected ap_()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method protected ar_()V
    .locals 3

    .prologue
    .line 116
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->ar_()V

    .line 117
    iget-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->h:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 120
    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->b()V

    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->ar_()V

    .line 104
    return-void
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    const-string/jumbo v0, "news_details"

    return-object v0
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 164
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 170
    :goto_0
    return-object v4

    .line 167
    :cond_0
    sget-object v0, Lcom/twitter/database/schema/a$o;->b:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->b:J

    .line 168
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 169
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 167
    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 170
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lbuj;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0
.end method

.method protected l()V
    .locals 3

    .prologue
    .line 337
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "tweet::last:impression"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->b(Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 14
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .prologue
    const v13, 0x7f1303a0

    const/4 v3, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v6, 0x1

    .line 187
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 189
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->i()Ljava/lang/String;

    move-result-object v1

    .line 190
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    .line 191
    invoke-virtual {v0, v6}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 192
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, "tweet"

    .line 193
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 194
    new-instance v0, Lbxc$a;

    invoke-direct {v0}, Lbxc$a;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":tweet:avatar:profile_click"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 195
    invoke-virtual {v0, v4}, Lbxc$a;->a(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":tweet::open_link"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 196
    invoke-virtual {v0, v4}, Lbxc$a;->b(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ":tweet:photo:click"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 197
    invoke-virtual {v0, v1}, Lbxc$a;->c(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lbxc$a;->a()Lbxc;

    move-result-object v4

    .line 200
    new-instance v0, Lcom/twitter/android/ct;

    new-instance v5, Lcom/twitter/android/ck;

    invoke-direct {v5, p0, v2}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lcom/twitter/android/ck;)V

    .line 203
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v1

    const-string/jumbo v2, "tweet_count"

    invoke-virtual {v1, v2}, Lcom/twitter/app/common/list/i;->b(Ljava/lang/String;)I

    move-result v2

    .line 205
    new-instance v4, Lcom/twitter/android/y;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v5

    iget-object v8, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    const/4 v9, -0x1

    .line 206
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v10

    move-object v7, v0

    invoke-direct/range {v4 .. v10}, Lcom/twitter/android/y;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 209
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 209
    invoke-static {v0, v1}, Lcom/twitter/android/news/c;->f(J)I

    move-result v0

    .line 211
    invoke-static {}, Lcom/twitter/android/news/c;->b()I

    move-result v5

    .line 212
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 213
    invoke-virtual {v1, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-array v8, v6, [Ljava/lang/Object;

    .line 214
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v7, v5, v2, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 213
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 215
    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    .line 217
    iget-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->h:Z

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040269

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 221
    invoke-virtual {v2, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0a05cb

    .line 222
    invoke-virtual {p0, v3}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 221
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    .line 223
    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    .line 225
    new-instance v0, Lcom/twitter/android/news/b;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v5, 0x7f0200b0

    iget-object v7, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0, v3, v5, p0, v7}, Lcom/twitter/android/news/b;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;)V

    iput-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->g:Lcom/twitter/android/UsersAdapter;

    .line 227
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->g:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0, v11}, Lcom/twitter/android/UsersAdapter;->e(Z)V

    .line 229
    new-instance v0, Lcom/twitter/android/widget/ap;

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/widget/BaseAdapter;

    new-instance v5, Lcom/twitter/android/widget/aq;

    invoke-direct {v5, v2}, Lcom/twitter/android/widget/aq;-><init>(Landroid/view/View;)V

    aput-object v5, v3, v11

    iget-object v2, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->g:Lcom/twitter/android/UsersAdapter;

    aput-object v2, v3, v6

    new-instance v2, Lcom/twitter/android/widget/aq;

    invoke-direct {v2, v1}, Lcom/twitter/android/widget/aq;-><init>(Landroid/view/View;)V

    aput-object v2, v3, v12

    const/4 v1, 0x3

    aput-object v4, v3, v1

    invoke-direct {v0, v3, v12}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->i:Lcom/twitter/android/widget/ap;

    .line 236
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->i:Lcom/twitter/android/widget/ap;

    invoke-virtual {v0, v4, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    .line 237
    return-void

    .line 232
    :cond_0
    new-instance v0, Lcom/twitter/android/widget/ap;

    new-array v2, v12, [Landroid/widget/BaseAdapter;

    new-instance v3, Lcom/twitter/android/widget/aq;

    invoke-direct {v3, v1}, Lcom/twitter/android/widget/aq;-><init>(Landroid/view/View;)V

    aput-object v3, v2, v11

    aput-object v4, v2, v6

    invoke-direct {v0, v2, v12}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->i:Lcom/twitter/android/widget/ap;

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onAttach(Landroid/app/Activity;)V

    .line 157
    instance-of v0, p1, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment$a;

    if-eqz v0, :cond_0

    .line 158
    check-cast p1, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment$a;

    iput-object p1, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->f:Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment$a;

    .line 160
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 83
    const-string/jumbo v3, "news_id"

    invoke-virtual {v0, v3}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->a:Ljava/lang/String;

    .line 84
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->a:Ljava/lang/String;

    invoke-static {v0}, Lbsj;->a(Ljava/lang/String;)I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->b:J

    .line 86
    if-eqz p1, :cond_0

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    .line 92
    :goto_0
    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->d:Z

    .line 94
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-string/jumbo v0, "japan_news_android_author_account_section_enabled"

    invoke-static {v4, v5, v0, v1}, Lcoj;->a(JLjava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->h:Z

    .line 97
    iput-boolean v2, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->c:Z

    .line 98
    return-void

    .line 90
    :cond_0
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 92
    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 175
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 181
    :goto_0
    return-object v4

    .line 178
    :cond_0
    sget-object v0, Lcom/twitter/database/schema/a$o;->c:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->b:J

    .line 179
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 180
    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 178
    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 181
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lbun;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 264
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 265
    iget-object v0, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/news/NewsDetailRelatedTweetsFragment;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 268
    :cond_0
    return-void
.end method
