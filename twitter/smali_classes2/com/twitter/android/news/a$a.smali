.class public Lcom/twitter/android/news/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/news/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/twitter/ui/widget/TypefacesTextView;

.field public final b:Lcom/twitter/ui/widget/TypefacesTextView;

.field public final c:Lcom/twitter/media/ui/image/MediaImageView;

.field public final d:Lcom/twitter/ui/widget/TypefacesTextView;

.field public final e:Lcom/twitter/media/ui/image/MediaImageView;

.field public final f:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

.field public final g:Lcom/twitter/ui/widget/TypefacesTextView;

.field final synthetic h:Lcom/twitter/android/news/a;


# direct methods
.method public constructor <init>(Lcom/twitter/android/news/a;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 151
    iput-object p1, p0, Lcom/twitter/android/news/a$a;->h:Lcom/twitter/android/news/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    const v0, 0x7f1305d5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/news/a$a;->a:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 153
    const v0, 0x7f1305d9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/news/a$a;->b:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 154
    const v0, 0x7f1305d4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/news/a$a;->c:Lcom/twitter/media/ui/image/MediaImageView;

    .line 155
    const v0, 0x7f1305dc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/news/a$a;->d:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 156
    const v0, 0x7f1305dd

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/news/a$a;->e:Lcom/twitter/media/ui/image/MediaImageView;

    .line 157
    const v0, 0x7f1305db

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    iput-object v0, p0, Lcom/twitter/android/news/a$a;->f:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 158
    const v0, 0x7f1305de

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/news/a$a;->g:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 159
    return-void
.end method
