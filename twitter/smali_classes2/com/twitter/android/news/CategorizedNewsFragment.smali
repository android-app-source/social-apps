.class public Lcom/twitter/android/news/CategorizedNewsFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Landroid/database/Cursor;",
        "Lcom/twitter/android/news/a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/twitter/android/metrics/b;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 201
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 203
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 207
    new-instance v2, Lcom/twitter/util/a;

    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 208
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-string/jumbo v1, "news"

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 209
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v1

    const-string/jumbo v2, "latestTopNewsIds"

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    .line 210
    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 209
    invoke-virtual {v1, v2, v0, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 212
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 166
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 167
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 168
    invoke-virtual {v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 169
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 166
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 170
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 162
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 163
    return-void
.end method


# virtual methods
.method protected H_()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/news/CategorizedNewsFragment;->b(I)Z

    .line 130
    return-void
.end method

.method public V_()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->V_()V

    .line 115
    const-string/jumbo v0, "news:headline:::pull_to_refresh"

    invoke-direct {p0, v0}, Lcom/twitter/android/news/CategorizedNewsFragment;->b(Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    .line 174
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 175
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 176
    new-instance v2, Lcom/twitter/app/common/base/h;

    invoke-direct {v2}, Lcom/twitter/app/common/base/h;-><init>()V

    const/4 v3, 0x1

    .line 177
    invoke-virtual {v2, v3}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v2

    .line 178
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/news/NewsDetailActivity;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "news_id"

    .line 179
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "tweet_count"

    const/16 v4, 0x9

    .line 180
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 181
    invoke-virtual {p0, v0}, Lcom/twitter/android/news/CategorizedNewsFragment;->startActivity(Landroid/content/Intent;)V

    .line 182
    const-string/jumbo v0, "news:headline::details:click"

    add-int/lit8 v2, p3, -0x1

    int-to-long v2, v2

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/twitter/android/news/CategorizedNewsFragment;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 183
    return-void
.end method

.method protected a(Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcbi;)V

    .line 135
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/news/a;

    invoke-virtual {v0}, Lcom/twitter/android/news/a;->i()Landroid/database/Cursor;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    invoke-virtual {v1}, Lcom/twitter/android/metrics/b;->aR_()V

    .line 138
    iget-boolean v1, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->e:Z

    if-nez v1, :cond_1

    .line 139
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/news/CategorizedNewsFragment;->b(I)Z

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->e:Z

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget v1, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->d:I

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 142
    invoke-direct {p0, v0}, Lcom/twitter/android/news/CategorizedNewsFragment;->a(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 62
    const v0, 0x7f04005e

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 63
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const v0, 0x7f0403e1

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 65
    const v0, 0x7f0400e1

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 67
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 187
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 188
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->aT_()V

    .line 190
    iget-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->j()V

    .line 195
    :goto_0
    if-ne p2, v2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a05d4

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 198
    :cond_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->k()V

    goto :goto_0
.end method

.method protected aP_()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aP_()V

    .line 109
    const-string/jumbo v0, "news:headline:::impression"

    invoke-direct {p0, v0}, Lcom/twitter/android/news/CategorizedNewsFragment;->b(Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method protected b(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 147
    invoke-virtual {p0, p1}, Lcom/twitter/android/news/CategorizedNewsFragment;->c_(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 158
    :goto_0
    return v0

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    invoke-virtual {v1}, Lcom/twitter/android/metrics/b;->aS_()V

    .line 151
    new-instance v1, Lbdz;

    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lbdz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v2, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->a:Ljava/lang/String;

    .line 152
    invoke-virtual {v1, v2}, Lbdz;->a(Ljava/lang/String;)Lbdz;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->b:Ljava/lang/String;

    .line 153
    invoke-virtual {v1, v2}, Lbdz;->b(Ljava/lang/String;)Lbdz;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->d:I

    int-to-long v2, v2

    .line 154
    invoke-virtual {v1, v2, v3}, Lbdz;->a(J)Lbdz;

    move-result-object v1

    const/16 v2, 0x1e

    .line 155
    invoke-virtual {v1, v2}, Lbdz;->a(I)Lbdz;

    move-result-object v1

    .line 151
    invoke-virtual {p0, v1, v0, p1}, Lcom/twitter/android/news/CategorizedNewsFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0
.end method

.method protected f()V
    .locals 5

    .prologue
    .line 76
    new-instance v0, Lcom/twitter/android/metrics/b;

    const-string/jumbo v1, "news:timeline:load"

    const-string/jumbo v2, "news:timeline:load"

    sget-object v3, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/metrics/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/h;)V

    iput-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    .line 79
    iget-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->i()V

    .line 80
    return-void
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->c:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->aQ_()V

    .line 121
    sget-object v0, Lcom/twitter/database/schema/a$o;->a:Landroid/net/Uri;

    iget v1, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->d:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 122
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 121
    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 123
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lbtp;->a:[Ljava/lang/String;

    const-string/jumbo v4, "country=? AND language=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->d:I

    return v0
.end method

.method protected l()V
    .locals 1

    .prologue
    .line 217
    const-string/jumbo v0, "news:headline::last:impression"

    invoke-direct {p0, v0}, Lcom/twitter/android/news/CategorizedNewsFragment;->b(Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "topicId"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->d:I

    .line 89
    if-eqz p1, :cond_0

    .line 90
    const-string/jumbo v0, "forceReload"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->e:Z

    .line 93
    :cond_0
    invoke-static {}, Lbsi;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->a:Ljava/lang/String;

    .line 94
    invoke-static {}, Lbsi;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->b:Ljava/lang/String;

    .line 96
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/news/a;

    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/android/news/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 97
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/twitter/android/news/CategorizedNewsFragment;->f()V

    .line 73
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 102
    const-string/jumbo v0, "forceReload"

    iget-boolean v1, p0, Lcom/twitter/android/news/CategorizedNewsFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 103
    return-void
.end method
