.class public Lcom/twitter/android/events/ScrollableEventGridFragment;
.super Lcom/twitter/android/EventGridFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/events/TwitterEventActivity$a;


# instance fields
.field private final aa:Lcom/twitter/android/events/b;

.field private ab:Z

.field private ac:I

.field private ad:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13
    invoke-direct {p0}, Lcom/twitter/android/EventGridFragment;-><init>()V

    .line 16
    new-instance v0, Lcom/twitter/android/events/b;

    invoke-direct {v0}, Lcom/twitter/android/events/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    .line 17
    iput-boolean v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ab:Z

    .line 18
    iput v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ac:I

    .line 19
    iput v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ad:I

    return-void
.end method


# virtual methods
.method public a(Lcno;I)V
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/twitter/android/events/ScrollableEventGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/events/TwitterEventActivity;

    .line 40
    invoke-virtual {v0}, Lcom/twitter/android/events/TwitterEventActivity;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 41
    iget-object v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    invoke-virtual {v1, p2}, Lcom/twitter/android/events/b;->a(I)V

    .line 42
    iget-object v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/android/events/b;)V

    .line 43
    iget-object v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    invoke-virtual {v0}, Lcom/twitter/android/events/b;->d()V

    .line 44
    if-nez p2, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/events/ScrollableEventGridFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 46
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ac:I

    .line 50
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ad:I

    .line 53
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/EventGridFragment;->a(Lcno;I)V

    .line 55
    :cond_1
    return-void
.end method

.method public a(Lcno;IIIZ)V
    .locals 3

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/android/events/ScrollableEventGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/events/TwitterEventActivity;

    .line 29
    invoke-virtual {v0}, Lcom/twitter/android/events/TwitterEventActivity;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    iget-object v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    iget v2, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->N:I

    invoke-virtual {v1, p1, p2, v2}, Lcom/twitter/android/events/b;->a(Lcno;II)V

    .line 31
    iget-object v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/android/events/b;)V

    .line 32
    iget-object v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    invoke-virtual {v0}, Lcom/twitter/android/events/b;->d()V

    .line 33
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/EventGridFragment;->a(Lcno;IIIZ)V

    .line 35
    :cond_0
    return-void
.end method

.method protected aP_()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    invoke-virtual {v0}, Lcom/twitter/android/events/b;->d()V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->aa:Lcom/twitter/android/events/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/b;->b(I)V

    .line 83
    invoke-super {p0}, Lcom/twitter/android/EventGridFragment;->aP_()V

    .line 84
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/twitter/android/EventGridFragment;->b()V

    .line 67
    iget-boolean v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ab:Z

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/twitter/android/events/ScrollableEventGridFragment;->H_()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ab:Z

    .line 71
    :cond_0
    return-void
.end method

.method public d_(I)V
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ac:I

    if-nez v0, :cond_0

    .line 89
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/events/ScrollableEventGridFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ac:I

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 90
    return-void

    .line 88
    :cond_0
    iget p1, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ad:I

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, -0x1

    return v0
.end method

.method protected g()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/events/ScrollableEventGridFragment;->ab:Z

    .line 76
    invoke-super {p0}, Lcom/twitter/android/EventGridFragment;->q_()V

    .line 77
    return-void
.end method
