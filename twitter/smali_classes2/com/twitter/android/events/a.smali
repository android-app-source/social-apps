.class public Lcom/twitter/android/events/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "SPORTSCRICKETWC"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "SPORTSSOCCER"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "SPORTSNBA"

    aput-object v3, v1, v2

    .line 38
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 37
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/events/a;->a:Ljava/util/Set;

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/android/widget/TopicView$TopicData;)Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 167
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 168
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 181
    :goto_1
    return-object v2

    .line 168
    :sswitch_0
    const-string/jumbo v3, "SOCCER"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v3, "BASKETBALL"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v3, "CRICKET"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 172
    :pswitch_0
    const-string/jumbo v0, "search_takeover"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 173
    const-string/jumbo v0, "topic_data"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 174
    const-string/jumbo v0, "event_subtype"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 168
    :sswitch_data_0
    .sparse-switch
        -0x6de50a97 -> :sswitch_0
        0x484158c5 -> :sswitch_1
        0x68532fd1 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    .line 219
    invoke-static {p0}, Lcom/twitter/android/events/a;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "cricket_experience_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const-string/jumbo v0, "CRICKET"

    .line 230
    :goto_0
    return-object v0

    .line 222
    :cond_0
    invoke-static {p0}, Lcom/twitter/android/events/a;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "soccer_experience_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    const-string/jumbo v0, "SOCCER"

    goto :goto_0

    .line 225
    :cond_1
    invoke-static {p0}, Lcom/twitter/android/events/a;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "nba_finals_timeline_android_game_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    const-string/jumbo v0, "BASKETBALL"

    goto :goto_0

    .line 230
    :cond_2
    const-string/jumbo v0, "NONE"

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/api/search/d;)V
    .locals 2

    .prologue
    .line 59
    if-nez p0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    const-string/jumbo v0, "ip_android_cricket_v2_2755"

    const-string/jumbo v1, "cricket_experience"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    const-string/jumbo v0, "cricket"

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 66
    :cond_2
    const-string/jumbo v0, "ip_android_soccer_v1_3046"

    const-string/jumbo v1, "soccer_bucket"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    const-string/jumbo v0, "soccer"

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 70
    :cond_3
    const-string/jumbo v0, "nba_finals_timeline_android_game_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const-string/jumbo v0, "basketball"

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/twitter/android/events/a;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 98
    if-nez p0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 101
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 103
    :pswitch_0
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    const-string/jumbo v1, "LEAGUE"

    .line 107
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 108
    const-string/jumbo v0, "cricket_experience_league_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 101
    :sswitch_0
    const-string/jumbo v2, "CRICKET"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v2, "SOCCER"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "BASKETBALL"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x2

    goto :goto_1

    .line 111
    :cond_3
    const-string/jumbo v1, "GAME"

    .line 112
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    const-string/jumbo v0, "cricket_experience_game_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 119
    :pswitch_1
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    const-string/jumbo v1, "LEAGUE"

    .line 123
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 124
    const-string/jumbo v0, "soccer_experience_league_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 127
    :cond_4
    const-string/jumbo v1, "GAME"

    .line 128
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    const-string/jumbo v0, "soccer_experience_game_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0

    .line 135
    :pswitch_2
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    const-string/jumbo v1, "LEAGUE"

    .line 139
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 140
    const-string/jumbo v0, "nba_finals_timeline_android_league_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0

    .line 143
    :cond_5
    const-string/jumbo v1, "GAME"

    .line 144
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    const-string/jumbo v0, "nba_finals_timeline_android_game_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0

    .line 101
    :sswitch_data_0
    .sparse-switch
        -0x6de50a97 -> :sswitch_1
        0x484158c5 -> :sswitch_2
        0x68532fd1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    .prologue
    .line 194
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 208
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 194
    :sswitch_0
    const-string/jumbo v1, "CRICKET"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "SOCCER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v1, "BASKETBALL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 196
    :pswitch_0
    const-class v0, Lcom/twitter/android/events/sports/cricket/CricketLandingActivity;

    goto :goto_1

    .line 199
    :pswitch_1
    const-class v0, Lcom/twitter/android/events/sports/soccer/SoccerLandingActivity;

    goto :goto_1

    .line 202
    :pswitch_2
    const-class v0, Lcom/twitter/android/events/sports/nba/NBALandingActivity;

    goto :goto_1

    .line 194
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6de50a97 -> :sswitch_1
        0x484158c5 -> :sswitch_2
        0x68532fd1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 235
    if-eqz p0, :cond_0

    .line 236
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "CRICKET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 235
    :goto_0
    return v0

    .line 236
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 241
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "SOCCER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 246
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "BASKETBALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 250
    invoke-static {p0}, Lcom/twitter/android/events/a;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    invoke-static {p0}, Lcom/twitter/android/events/a;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    invoke-static {p0}, Lcom/twitter/android/events/a;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 250
    :goto_0
    return v0

    .line 252
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
