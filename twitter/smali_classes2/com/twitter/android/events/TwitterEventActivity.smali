.class public abstract Lcom/twitter/android/events/TwitterEventActivity;
.super Lcom/twitter/android/ScrollingHeaderActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/bv;
.implements Lcom/twitter/android/u;
.implements Lcom/twitter/media/request/a$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/events/TwitterEventActivity$a;,
        Lcom/twitter/android/events/TwitterEventActivity$c;,
        Lcom/twitter/android/events/TwitterEventActivity$b;
    }
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;


# instance fields
.field private A:Landroid/view/ViewGroup;

.field private B:Lcom/twitter/media/ui/image/BackgroundImageView;

.field private C:Z

.field private D:Lcom/twitter/android/at;

.field private E:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcom/twitter/android/ScrollingHeaderActivity$a;

.field private L:Ljava/lang/String;

.field private M:I

.field private N:Ljava/lang/String;

.field private O:Lcom/twitter/internal/android/widget/ToolBar;

.field private P:Landroid/widget/TextView;

.field private Q:Ljava/lang/String;

.field private R:Z

.field private S:Ljava/io/File;

.field private T:Ljava/lang/String;

.field private U:J

.field private V:J

.field private W:Landroid/view/ViewGroup;

.field private X:Landroid/view/ViewGroup;

.field private Y:Z

.field private Z:I

.field protected g:Z

.field protected h:Landroid/graphics/drawable/Drawable;

.field protected i:Ljava/lang/String;

.field protected j:Ljava/lang/String;

.field protected k:Lcom/twitter/android/widget/TopicView$TopicData;

.field private l:Lcom/twitter/library/provider/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const-string/jumbo v0, "twitter://events/default"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/events/TwitterEventActivity;->a:Landroid/net/Uri;

    .line 100
    const-string/jumbo v0, "twitter://events/commentary"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/events/TwitterEventActivity;->b:Landroid/net/Uri;

    .line 101
    const-string/jumbo v0, "twitter://events/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/events/TwitterEventActivity;->c:Landroid/net/Uri;

    .line 102
    const-string/jumbo v0, "twitter://events/photos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/events/TwitterEventActivity;->d:Landroid/net/Uri;

    .line 103
    const-string/jumbo v0, "twitter://events/videos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/events/TwitterEventActivity;->e:Landroid/net/Uri;

    .line 104
    const-string/jumbo v0, "twitter://events/matches"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/events/TwitterEventActivity;->f:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Lcom/twitter/android/ScrollingHeaderActivity;-><init>()V

    .line 164
    iput-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Y:Z

    .line 165
    iput v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/events/TwitterEventActivity;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    return v0
.end method

.method public static a(Landroid/content/Context;J)Ljava/io/File;
    .locals 5

    .prologue
    .line 579
    invoke-static {p0}, Lcqc;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 580
    if-nez v1, :cond_0

    .line 581
    const/4 v0, 0x0

    .line 583
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_event_header.jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 588
    const/4 v2, 0x0

    .line 590
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 591
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 595
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 597
    :goto_0
    return-void

    .line 592
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 593
    :goto_1
    :try_start_2
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 595
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 592
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/events/TwitterEventActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private a(Lcom/twitter/util/collection/h;ILjava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/h",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;I",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const v6, 0x7f0a07d0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 886
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 887
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 888
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const/4 v2, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    move v1, v2

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 902
    sget-object v1, Lcom/twitter/android/events/TwitterEventActivity;->d:Landroid/net/Uri;

    .line 903
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "highlights"

    .line 902
    invoke-virtual {p0, v1, p2, v2, v3}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 909
    :goto_1
    return v0

    .line 888
    :sswitch_0
    const-string/jumbo v5, "videos"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "media"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_2
    const-string/jumbo v1, "photos"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 890
    :pswitch_0
    sget-object v1, Lcom/twitter/android/events/TwitterEventActivity;->e:Landroid/net/Uri;

    const v2, 0x7f0a093d

    .line 891
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "highlights"

    .line 890
    invoke-virtual {p0, v1, p2, v2, v3}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 895
    :pswitch_1
    sget-object v1, Lcom/twitter/android/events/TwitterEventActivity;->c:Landroid/net/Uri;

    .line 896
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "highlights"

    .line 895
    invoke-virtual {p0, v1, p2, v2, v3}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    :cond_1
    move v0, v1

    .line 909
    goto :goto_1

    .line 888
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3af3777f -> :sswitch_2
        -0x30ad84a8 -> :sswitch_0
        0x62f6fe4 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private ab()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 257
    return-void
.end method

.method private ac()I
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getMeasuredHeight()I

    move-result v0

    .line 636
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ad()I
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->getMeasuredHeight()I

    move-result v0

    .line 644
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/events/TwitterEventActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/events/TwitterEventActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 557
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/BackgroundImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 561
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 562
    iget-wide v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->V:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    .line 565
    iput-wide v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->V:J

    .line 566
    iput-wide v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    .line 567
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    invoke-static {v0, v2, v3}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    .line 568
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->R:Z

    .line 569
    iget-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->R:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    .line 570
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-static {p1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->b(Lcom/twitter/media/request/a$a;)Z

    goto :goto_0

    .line 568
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/android/events/TwitterEventActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/events/TwitterEventActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/events/TwitterEventActivity;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->M:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/events/TwitterEventActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/events/TwitterEventActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected C()Z
    .locals 1

    .prologue
    .line 615
    const/4 v0, 0x0

    return v0
.end method

.method protected D()I
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/UnboundedFrameLayout;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    invoke-virtual {v1}, Lcom/twitter/android/widget/UnboundedFrameLayout;->getTranslationY()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->r:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected X_()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 350
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->k()Ljava/lang/String;

    move-result-object v0

    .line 351
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 352
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x24

    if-ne v1, v2, :cond_1

    .line 353
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a098b

    .line 354
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 356
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->X_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 575
    const v0, 0x7f0e0206

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected a(Ljava/util/List;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/PagerAdapter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            ")",
            "Landroid/support/v4/view/PagerAdapter;"
        }
    .end annotation

    .prologue
    .line 815
    new-instance v0, Lcom/twitter/android/events/TwitterEventActivity$b;

    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->p:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->D:Lcom/twitter/android/at;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/events/TwitterEventActivity$b;-><init>(Lcom/twitter/android/events/TwitterEventActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    return-object v0
.end method

.method protected a(Ljava/util/List;)Landroid/widget/BaseAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)",
            "Landroid/widget/BaseAdapter;"
        }
    .end annotation

    .prologue
    .line 808
    new-instance v0, Lcom/twitter/android/at;

    invoke-direct {v0, p1}, Lcom/twitter/android/at;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->D:Lcom/twitter/android/at;

    .line 809
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->D:Lcom/twitter/android/at;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 262
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 263
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 264
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 265
    return-object p2

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;
    .locals 10

    .prologue
    .line 914
    const/4 v2, 0x0

    .line 917
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 918
    const/4 v1, 0x0

    .line 919
    const-class v0, Lcom/twitter/android/events/EventLandingFragment;

    .line 920
    const/4 v3, 0x0

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move-object v1, v0

    .line 947
    :goto_0
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->M:I

    packed-switch v0, :pswitch_data_0

    .line 953
    const-string/jumbo v0, "unknown"

    move-object v5, v0

    .line 958
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 959
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 963
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->l:Lcom/twitter/library/provider/t;

    invoke-virtual {v0, v6, v7}, Lcom/twitter/library/provider/t;->m(J)V

    .line 964
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/common/list/i$b;->a(Landroid/content/Intent;)Lcom/twitter/app/common/list/i$b;

    move-result-object v0

    const v8, 0x7f0a0385

    .line 967
    invoke-virtual {v0, v8}, Lcom/twitter/app/common/list/i$b;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const/4 v8, 0x1

    .line 968
    invoke-virtual {v0, v8}, Lcom/twitter/app/common/list/i$b;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v8, "search_type"

    .line 969
    invoke-virtual {v0, v8, v2}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "fetch_type"

    const/4 v8, 0x5

    .line 970
    invoke-virtual {v0, v2, v8}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "scribe_page"

    iget-object v8, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    .line 971
    invoke-virtual {v0, v2, v8}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "scribe_section"

    .line 972
    invoke-virtual {v0, v2, p4}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "scribe_component"

    iget-object v8, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    .line 973
    invoke-virtual {v0, v2, v8}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v8, "event_header_available"

    iget-boolean v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->g:Z

    if-nez v2, :cond_9

    const/4 v2, 0x1

    .line 974
    :goto_3
    invoke-virtual {v0, v8, v2}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "q_source"

    .line 975
    invoke-virtual {v0, v2, v5}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "event_id"

    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->L:Ljava/lang/String;

    .line 976
    invoke-virtual {v0, v2, v5}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "event_type"

    iget v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->M:I

    .line 977
    invoke-virtual {v0, v2, v5}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "fragment_page_number"

    .line 978
    invoke-virtual {v0, v2, p2}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "should_shim"

    const/4 v5, 0x0

    .line 979
    invoke-virtual {v0, v2, v5}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v2, "search_id"

    .line 980
    invoke-virtual {v0, v2, v6, v7}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;J)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    .line 981
    invoke-static {v4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 982
    const-string/jumbo v2, "query_rewrite_id"

    invoke-virtual {v0, v2, v4}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    .line 985
    :cond_0
    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 986
    const-string/jumbo v2, "data_lookup_id"

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    .line 989
    :cond_1
    new-instance v2, Lcom/twitter/library/client/m$a;

    invoke-direct {v2, p1, v1}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 990
    invoke-virtual {v2, p3}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v1

    .line 991
    invoke-virtual {v1, p4}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/client/m$a;

    move-result-object v1

    .line 992
    invoke-virtual {v0}, Lcom/twitter/app/common/list/i$b;->b()Lcom/twitter/app/common/list/i;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 993
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 989
    return-object v0

    .line 921
    :cond_2
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 922
    const/4 v1, 0x1

    .line 923
    const-string/jumbo v3, "official"

    .line 924
    const-class v0, Lcom/twitter/android/events/EventLandingFragment;

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move-object v1, v0

    goto/16 :goto_0

    .line 925
    :cond_3
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 926
    const/4 v1, 0x3

    .line 927
    const-string/jumbo v3, "photo"

    .line 928
    const-class v0, Lcom/twitter/android/events/ScrollableEventGridFragment;

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move-object v1, v0

    goto/16 :goto_0

    .line 929
    :cond_4
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 930
    const/4 v1, 0x7

    .line 931
    const-string/jumbo v3, "photo"

    .line 932
    const-class v0, Lcom/twitter/android/events/ScrollableEventGridFragment;

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move-object v1, v0

    goto/16 :goto_0

    .line 933
    :cond_5
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 934
    const/4 v1, -0x1

    .line 935
    const-string/jumbo v3, "video"

    .line 936
    const-class v0, Lcom/twitter/android/events/EventLandingFragment;

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move-object v1, v0

    goto/16 :goto_0

    .line 937
    :cond_6
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 938
    const/16 v1, 0x8

    .line 939
    const-class v0, Lcom/twitter/android/events/EventLandingFragment;

    .line 940
    const-string/jumbo v3, "schedule"

    .line 941
    const-string/jumbo v2, "schedule"

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move-object v1, v0

    goto/16 :goto_0

    .line 943
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown Uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 949
    :pswitch_0
    const-string/jumbo v0, "spev"

    move-object v5, v0

    .line 950
    goto/16 :goto_1

    .line 961
    :cond_8
    sget-object v0, Lcom/twitter/util/y;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v6

    goto/16 :goto_2

    .line 973
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 947
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    const v8, 0x7f0a040e

    const/4 v2, 0x0

    .line 840
    invoke-static {p1}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 841
    invoke-static {p2}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 842
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 844
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 845
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->a:Landroid/net/Uri;

    invoke-virtual {p0, v8}, Lcom/twitter/android/events/TwitterEventActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "tweets"

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 882
    :cond_0
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    .line 849
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 850
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_2
    move v1, v0

    .line 880
    goto :goto_0

    .line 850
    :sswitch_0
    const-string/jumbo v7, "matches"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v0, v2

    goto :goto_1

    :sswitch_1
    const-string/jumbo v7, "photos"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v7, "official"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v7, "tweets"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    .line 852
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->L:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/events/a;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0a093c

    .line 853
    invoke-virtual {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 854
    :goto_3
    sget-object v6, Lcom/twitter/android/events/TwitterEventActivity;->f:Landroid/net/Uri;

    const-string/jumbo v7, "games"

    invoke-virtual {p0, v6, v1, v0, v7}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 855
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    .line 856
    goto :goto_2

    .line 853
    :cond_3
    const v0, 0x7f0a0936

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 859
    :pswitch_1
    invoke-direct {p0, v4, v1, v3}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/util/collection/h;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit8 v1, v1, 0x1

    :cond_4
    move v0, v1

    .line 860
    goto :goto_2

    .line 863
    :pswitch_2
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->b:Landroid/net/Uri;

    const v6, 0x7f0a07ca

    .line 864
    invoke-virtual {p0, v6}, Lcom/twitter/android/events/TwitterEventActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "commentary"

    .line 863
    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 866
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    .line 867
    goto :goto_2

    .line 870
    :pswitch_3
    sget-object v0, Lcom/twitter/android/events/TwitterEventActivity;->a:Landroid/net/Uri;

    .line 871
    invoke-virtual {p0, v8}, Lcom/twitter/android/events/TwitterEventActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "tweets"

    .line 870
    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 873
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    .line 874
    goto/16 :goto_2

    .line 850
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3af3777f -> :sswitch_1
        -0x335177de -> :sswitch_3
        -0x2d9d6515 -> :sswitch_2
        0x321e8933 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 321
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->a(I)V

    .line 322
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->D:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->a()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 324
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/m;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SearchFragment;

    .line 325
    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->G_()V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 330
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->D:Lcom/twitter/android/at;

    invoke-virtual {v0, p1}, Lcom/twitter/android/at;->a(I)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/BackgroundImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 836
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/android/events/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 649
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->f()I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 650
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->b()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    if-eqz v0, :cond_0

    .line 655
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 661
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/events/TwitterEventActivity;->ad()I

    move-result v5

    .line 662
    invoke-direct {p0}, Lcom/twitter/android/events/TwitterEventActivity;->ac()I

    move-result v6

    .line 663
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/UnboundedFrameLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    .line 666
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_9

    :cond_2
    move v2, v1

    .line 672
    :goto_2
    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_a

    :cond_3
    move v3, v1

    .line 678
    :goto_3
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->b()I

    move-result v7

    if-nez v7, :cond_c

    .line 680
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->a()I

    move-result v0

    if-lez v0, :cond_b

    .line 681
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    invoke-virtual {p1}, Lcom/twitter/android/events/b;->c()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    .line 696
    :cond_4
    :goto_4
    sub-int v0, v6, v3

    sub-int v2, v0, v5

    .line 697
    sub-int v3, v6, v5

    .line 698
    sub-int v0, v5, v6

    iget v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    add-int/2addr v5, v0

    .line 699
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    if-le v0, v2, :cond_10

    invoke-virtual {p1}, Lcom/twitter/android/events/b;->b()I

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Y:Z

    if-nez v0, :cond_10

    .line 701
    :cond_5
    iget-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    if-ge v0, v3, :cond_e

    int-to-float v0, v3

    :goto_5
    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/UnboundedFrameLayout;->setTranslationY(F)V

    .line 702
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setTranslationY(F)V

    .line 703
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0, v4}, Lcom/twitter/media/ui/image/BackgroundImageView;->setTranslationY(F)V

    .line 704
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    .line 705
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 707
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-eqz v0, :cond_7

    .line 708
    iget-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-lez v5, :cond_f

    move v0, v4

    :goto_6
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 710
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    iget v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/UnboundedFrameLayout;->setTranslationY(F)V

    .line 711
    iput-boolean v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->Y:Z

    goto/16 :goto_0

    .line 663
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    .line 664
    invoke-virtual {v0}, Lcom/twitter/android/widget/UnboundedFrameLayout;->getMeasuredHeight()I

    move-result v0

    goto/16 :goto_1

    .line 669
    :cond_9
    iget-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    goto/16 :goto_2

    .line 675
    :cond_a
    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    goto :goto_3

    .line 682
    :cond_b
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->a()I

    move-result v0

    if-gez v0, :cond_4

    .line 683
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    invoke-virtual {p1}, Lcom/twitter/android/events/b;->c()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    goto :goto_4

    .line 687
    :cond_c
    neg-int v7, v5

    sub-int v2, v7, v2

    sub-int/2addr v2, v3

    sub-int v0, v2, v0

    .line 688
    iget v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    invoke-virtual {p1}, Lcom/twitter/android/events/b;->a()I

    move-result v7

    add-int/2addr v2, v7

    iput v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    .line 689
    iget v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    if-lez v2, :cond_d

    .line 690
    iput v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    goto/16 :goto_4

    .line 691
    :cond_d
    iget v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    if-ge v2, v0, :cond_4

    .line 692
    iput v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    goto/16 :goto_4

    .line 701
    :cond_e
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    int-to-float v0, v0

    goto :goto_5

    .line 708
    :cond_f
    int-to-float v0, v5

    goto :goto_6

    .line 712
    :cond_10
    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    add-int/lit16 v0, v0, 0x96

    if-le v0, v2, :cond_14

    invoke-virtual {p1}, Lcom/twitter/android/events/b;->b()I

    move-result v0

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Y:Z

    if-nez v0, :cond_14

    .line 715
    :cond_11
    iput-boolean v8, p0, Lcom/twitter/android/events/TwitterEventActivity;->Y:Z

    .line 716
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    int-to-float v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UnboundedFrameLayout;->setTranslationY(F)V

    .line 717
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setTranslationY(F)V

    .line 718
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0, v4}, Lcom/twitter/media/ui/image/BackgroundImageView;->setTranslationY(F)V

    .line 719
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    if-eqz v0, :cond_12

    .line 720
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 722
    :cond_12
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-eqz v0, :cond_13

    .line 723
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    int-to-float v1, v5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 725
    :cond_13
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    int-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UnboundedFrameLayout;->setTranslationY(F)V

    goto/16 :goto_0

    .line 727
    :cond_14
    iput-boolean v8, p0, Lcom/twitter/android/events/TwitterEventActivity;->Y:Z

    .line 728
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->a()I

    move-result v0

    if-lez v0, :cond_15

    iget v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    if-le v0, v2, :cond_15

    .line 730
    iput v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    .line 732
    :cond_15
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 737
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->b()I

    move-result v0

    if-lez v0, :cond_17

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    .line 738
    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getTranslationY()F

    move-result v0

    neg-int v7, v6

    int-to-float v7, v7

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    .line 739
    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getTranslationY()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_17

    .line 740
    :cond_16
    invoke-virtual {p1}, Lcom/twitter/android/events/b;->e()I

    move-result v0

    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    .line 750
    :cond_17
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getTranslationY()F

    move-result v0

    invoke-virtual {p1}, Lcom/twitter/android/events/b;->a()I

    move-result v4

    int-to-float v4, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v4, v7

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 752
    neg-int v4, v6

    if-ge v0, v4, :cond_18

    .line 753
    neg-int v0, v6

    .line 755
    :cond_18
    if-lez v0, :cond_1b

    .line 759
    :goto_7
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/UnboundedFrameLayout;->setTranslationY(F)V

    .line 760
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    int-to-float v3, v1

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/ToolBar;->setTranslationY(F)V

    .line 761
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    int-to-float v3, v1

    invoke-virtual {v0, v3}, Lcom/twitter/media/ui/image/BackgroundImageView;->setTranslationY(F)V

    .line 762
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    if-eqz v0, :cond_19

    .line 763
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    int-to-float v3, v1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 765
    :cond_19
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1a

    .line 766
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    int-to-float v3, v5

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 768
    :cond_1a
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->q:Lcom/twitter/android/widget/UnboundedFrameLayout;

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UnboundedFrameLayout;->setTranslationY(F)V

    goto/16 :goto_0

    :cond_1b
    move v1, v0

    goto :goto_7
.end method

.method public a(Lcom/twitter/android/widget/TopicView$TopicData;)V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 479
    iget-object v0, p1, Lcom/twitter/android/widget/TopicView$TopicData;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 482
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 483
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->L:Ljava/lang/String;

    iget v4, p0, Lcom/twitter/android/events/TwitterEventActivity;->M:I

    invoke-static {v0, v4}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v4

    .line 484
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->A:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 486
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    aput-object v6, v5, v10

    const-string/jumbo v6, "event_card"

    aput-object v6, v5, v11

    iget-object v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    aput-object v6, v5, v12

    aput-object v1, v5, v13

    const/4 v6, 0x4

    const-string/jumbo v7, "pull_to_refresh"

    aput-object v7, v5, v6

    .line 487
    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v5, "query"

    .line 490
    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 491
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 486
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 532
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 533
    iget-object v0, p1, Lcom/twitter/android/widget/TopicView$TopicData;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->b(Ljava/lang/String;)V

    .line 541
    :goto_2
    invoke-virtual {p0, p1}, Lcom/twitter/android/events/TwitterEventActivity;->b(Lcom/twitter/android/widget/TopicView$TopicData;)V

    .line 543
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    aput-object v3, v2, v10

    const-string/jumbo v3, "event_card"

    aput-object v3, v2, v11

    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    aput-object v3, v2, v12

    aput-object v1, v2, v13

    const/4 v1, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v2, v1

    .line 548
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v1, "query"

    .line 551
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 552
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 547
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 553
    iput-object p1, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    goto/16 :goto_0

    .line 493
    :cond_3
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f0400e8

    iget-object v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    .line 494
    invoke-virtual {v0, v5, v6, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->A:Landroid/view/ViewGroup;

    .line 495
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->A:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->setHeaderView(Landroid/view/View;)V

    .line 496
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->A:Landroid/view/ViewGroup;

    const v5, 0x7f130373

    .line 497
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/BackgroundImageView;

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    .line 498
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 499
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/BackgroundImageView;->setAspectRatio(F)V

    .line 500
    iget-wide v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_6

    iget-wide v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    .line 501
    invoke-static {p0, v6, v7}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v0

    .line 503
    :goto_3
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 505
    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    .line 506
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 505
    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 517
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->A:Landroid/view/ViewGroup;

    const v5, 0x7f130372

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    .line 518
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->A:Landroid/view/ViewGroup;

    const v5, 0x7f130371

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    .line 520
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->i()Landroid/view/View;

    move-result-object v0

    .line 521
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->j()Landroid/view/View;

    move-result-object v5

    .line 523
    iget-object v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    if-eqz v6, :cond_5

    if-eqz v0, :cond_5

    .line 524
    iget-object v6, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 527
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    if-eqz v5, :cond_2

    .line 528
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_6
    move-object v0, v1

    .line 501
    goto :goto_3

    .line 507
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->T:Ljava/lang/String;

    if-eqz v0, :cond_8

    new-instance v0, Ljava/io/File;

    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->T:Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 509
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->T:Ljava/lang/String;

    invoke-static {v5}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/BackgroundImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 512
    :cond_8
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->o()V

    .line 513
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/BackgroundImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 535
    :cond_9
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->o()V

    .line 536
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/BackgroundImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 537
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/BackgroundImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 538
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->r:I

    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 539
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->requestLayout()V

    goto/16 :goto_2
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 2

    .prologue
    .line 1004
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1005
    if-eqz v0, :cond_1

    .line 1006
    invoke-virtual {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->b(Landroid/graphics/Bitmap;)V

    .line 1007
    iget-boolean v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->R:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 1008
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 1018
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->R:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 1012
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1013
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    invoke-virtual {v0}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1015
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->z:Lcom/twitter/android/bq;

    invoke-interface {v0}, Lcom/twitter/android/bq;->b()V

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 82
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/model/topic/d;)V
    .locals 2

    .prologue
    .line 788
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    if-eqz v0, :cond_0

    .line 789
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    sget-object v1, Lcom/twitter/model/topic/b;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v1}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/widget/TopicView$TopicData;->m:[B

    .line 790
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/android/widget/TopicView$TopicData;)V

    .line 792
    :cond_0
    return-void
.end method

.method public a(ZLandroid/widget/ListView;Lcom/twitter/android/SearchFragment;)V
    .locals 0

    .prologue
    .line 419
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 290
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 291
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v4, "query"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-interface {p1}, Lcmm;->a()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 301
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->a(Lcmm;)Z

    move-result v0

    :goto_0
    return v0

    .line 295
    :pswitch_0
    invoke-static {p0, v1, v1}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x0

    aput-object v3, v2, v0

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "query"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "share"

    aput-object v4, v2, v3

    .line 297
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 296
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 293
    :pswitch_data_0
    .packed-switch 0x7f1308bc
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 270
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->a(Lcmr;)Z

    .line 271
    const v0, 0x7f14002f

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;JLcom/twitter/model/topic/TwitterTopic;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 424
    iput-object p1, p0, Lcom/twitter/android/events/TwitterEventActivity;->j:Ljava/lang/String;

    .line 425
    const/4 v0, 0x0

    return v0
.end method

.method protected b(Landroid/content/res/Resources;)I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 999
    const/4 v0, 0x0

    return v0
.end method

.method public b(Lcmr;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 278
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Lcmr;)I

    .line 279
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 280
    const v1, 0x7f13088d

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    invoke-virtual {v1, v3}, Lazv;->b(Z)Lazv;

    .line 281
    const v1, 0x7f1308bc

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lazv;->b(Z)Lazv;

    .line 282
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->P:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 283
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->P:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 284
    const/4 v0, 0x2

    return v0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected b(I)V
    .locals 1

    .prologue
    .line 796
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->b(I)V

    .line 797
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->C:Z

    .line 798
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    .line 799
    return-void
.end method

.method protected b(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 774
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Landroid/graphics/Bitmap;)V

    .line 775
    iget-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->C:Z

    if-nez v0, :cond_1

    .line 776
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;->cancel(Z)Z

    .line 779
    :cond_0
    new-instance v0, Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;-><init>(Lcom/twitter/android/ScrollingHeaderActivity;Z)V

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    .line 780
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 785
    :cond_1
    :goto_0
    return-void

    .line 782
    :catch_0
    move-exception v0

    .line 783
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->z:Lcom/twitter/android/bq;

    invoke-interface {v0}, Lcom/twitter/android/bq;->b()V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 172
    invoke-static {}, Lcom/twitter/library/provider/t;->c()Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->l:Lcom/twitter/library/provider/t;

    .line 173
    if-nez p1, :cond_4

    .line 174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    .line 175
    iput-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->T:Ljava/lang/String;

    .line 176
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    .line 177
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    .line 185
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 186
    const-string/jumbo v0, "search_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 187
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    :cond_0
    const-string/jumbo v0, "EXTRA_PARENT_EVENT"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 191
    if-eqz v0, :cond_1

    .line 192
    invoke-virtual {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->d(Landroid/content/Intent;)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->l:Lcom/twitter/library/provider/t;

    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;)V

    .line 195
    const-string/jumbo v0, "topic_data"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TopicView$TopicData;

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    .line 196
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    .line 198
    const-string/jumbo v3, "event_page_type"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->i:Ljava/lang/String;

    .line 199
    const-string/jumbo v3, "LEAGUE"

    iget-object v4, p0, Lcom/twitter/android/events/TwitterEventActivity;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->g:Z

    .line 200
    iget-boolean v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->g:Z

    if-nez v3, :cond_5

    if-eqz v0, :cond_5

    .line 201
    iget-object v3, v0, Lcom/twitter/android/widget/TopicView$TopicData;->a:Ljava/lang/String;

    iput-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->L:Ljava/lang/String;

    .line 202
    iget v3, v0, Lcom/twitter/android/widget/TopicView$TopicData;->b:I

    iput v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->M:I

    .line 208
    :goto_1
    const-string/jumbo v3, "search"

    iput-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    .line 210
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 213
    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    invoke-virtual {v3, p0}, Lcom/twitter/android/widget/UnboundedFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    if-eqz v0, :cond_2

    .line 215
    invoke-virtual {p0, v0}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/android/widget/TopicView$TopicData;)V

    .line 218
    :cond_2
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0403a7

    .line 219
    invoke-virtual {v0, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->P:Landroid/widget/TextView;

    .line 220
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->P:Landroid/widget/TextView;

    const-string/jumbo v3, "query"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->P:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 224
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->L:Ljava/lang/String;

    iget v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->M:I

    invoke-static {v0, v3}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v3

    .line 226
    new-instance v6, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v6, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v7, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    aput-object v0, v7, v9

    iget-boolean v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->g:Z

    if-eqz v0, :cond_6

    move-object v0, v1

    :goto_2
    aput-object v0, v7, v10

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    aput-object v0, v7, v11

    aput-object v1, v7, v12

    const/4 v0, 0x4

    const-string/jumbo v8, "takeover"

    aput-object v8, v7, v0

    .line 227
    invoke-virtual {v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v6, "query"

    .line 230
    invoke-virtual {v0, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v6, "search_src_ref"

    .line 231
    invoke-virtual {v2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 232
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 226
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 234
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->m:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 235
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_3

    .line 236
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->m:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 237
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/events/TwitterEventActivity;->N:Ljava/lang/String;

    aput-object v5, v4, v9

    iget-object v0, v0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    aput-object v0, v4, v10

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    aput-object v0, v4, v11

    aput-object v1, v4, v12

    const/4 v0, 0x4

    const-string/jumbo v1, "impression"

    aput-object v1, v4, v0

    .line 238
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 240
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 237
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 243
    :cond_3
    return-void

    .line 179
    :cond_4
    const-string/jumbo v0, "search_ids"

    .line 180
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    .line 181
    const-string/jumbo v0, "scribe_component"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    .line 182
    const-string/jumbo v0, "prev_header_image"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->T:Ljava/lang/String;

    .line 183
    const-string/jumbo v0, "header_image_id"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    goto/16 :goto_0

    .line 204
    :cond_5
    const-string/jumbo v3, "event_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->L:Ljava/lang/String;

    .line 205
    const-string/jumbo v3, "event_type"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/events/TwitterEventActivity;->M:I

    goto/16 :goto_1

    .line 226
    :cond_6
    const-string/jumbo v0, "games"

    goto/16 :goto_2
.end method

.method protected abstract b(Lcom/twitter/android/widget/TopicView$TopicData;)V
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 388
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;->cancel(Z)Z

    .line 390
    iput-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->K:Lcom/twitter/android/ScrollingHeaderActivity$a;

    .line 392
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v1, :cond_1

    .line 393
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->l:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/t;->b(Ljava/util/Collection;)V

    .line 395
    :cond_1
    iput-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    .line 396
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->d()V

    .line 397
    return-void
.end method

.method protected f()[I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 361
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->k()Ljava/lang/String;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 363
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_1

    .line 364
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 366
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->f()[I

    move-result-object v0

    goto :goto_0

    .line 364
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method protected g(I)V
    .locals 1

    .prologue
    .line 605
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->w:Lcom/twitter/android/widget/ar;

    if-eqz v0, :cond_0

    .line 607
    invoke-virtual {p0, p1}, Lcom/twitter/android/events/TwitterEventActivity;->h(I)V

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 610
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->g(I)V

    goto :goto_0
.end method

.method protected i()Landroid/view/View;
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x0

    return-object v0
.end method

.method protected j()Landroid/view/View;
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x0

    return-object v0
.end method

.method protected k()Ljava/lang/String;
    .locals 3

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 337
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_0

    .line 338
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 343
    :goto_0
    return-object v0

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    iget-object v0, v0, Lcom/twitter/android/widget/TopicView$TopicData;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->k:Lcom/twitter/android/widget/TopicView$TopicData;

    iget-object v1, v1, Lcom/twitter/android/widget/TopicView$TopicData;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 343
    :cond_1
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->X_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected l()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/16 v3, 0x8

    .line 447
    iget-boolean v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->g:Z

    if-eqz v1, :cond_2

    .line 448
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 452
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 453
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 464
    :cond_1
    :goto_0
    return v0

    .line 457
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->W:Landroid/view/ViewGroup;

    .line 458
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-ne v1, v3, :cond_4

    .line 459
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 460
    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->X:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 464
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected abstract n()Z
.end method

.method protected o()V
    .locals 2

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020802

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->h:Landroid/graphics/drawable/Drawable;

    .line 473
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 308
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f13065d

    if-ne v0, v1, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->N()Z

    .line 310
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->P:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->b(Ljava/lang/CharSequence;)V

    .line 312
    :cond_0
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 372
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onRestart()V

    .line 373
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->e()V

    .line 376
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 247
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onResume()V

    .line 248
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 249
    invoke-direct {p0}, Lcom/twitter/android/events/TwitterEventActivity;->ab()V

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/events/TwitterEventActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->O:Lcom/twitter/internal/android/widget/ToolBar;

    .line 252
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 401
    const-string/jumbo v0, "search_ids"

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->E:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 402
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 403
    const-string/jumbo v0, "prev_header_image"

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->S:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_0
    iget-wide v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 406
    const-string/jumbo v0, "header_image_id"

    iget-wide v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->U:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 408
    :cond_1
    const-string/jumbo v0, "scribe_component"

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 410
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->A:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity;->B:Lcom/twitter/media/ui/image/BackgroundImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/BackgroundImageView;->f()V

    .line 383
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onStop()V

    .line 384
    return-void
.end method

.method protected q()Z
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x1

    return v0
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 821
    const/4 v0, 0x0

    return-object v0
.end method

.method protected s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 827
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract t()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation
.end method

.method public u()F
    .locals 3

    .prologue
    .line 624
    invoke-direct {p0}, Lcom/twitter/android/events/TwitterEventActivity;->ad()I

    move-result v0

    invoke-direct {p0}, Lcom/twitter/android/events/TwitterEventActivity;->ac()I

    move-result v1

    sub-int/2addr v0, v1

    .line 625
    if-lez v0, :cond_0

    .line 626
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/twitter/android/events/TwitterEventActivity;->Z:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 628
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected z()Z
    .locals 1

    .prologue
    .line 316
    const-string/jumbo v0, "event_timelines_blur_header_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
