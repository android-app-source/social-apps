.class public Lcom/twitter/android/events/EventLandingFragment;
.super Lcom/twitter/android/SearchResultsFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/events/TwitterEventActivity$a;


# instance fields
.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:I

.field private final ad:Lcom/twitter/android/events/b;

.field private ae:Ljava/lang/String;

.field private af:I

.field private ag:Z

.field private ah:Z

.field private ai:I

.field private aj:I

.field private ak:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;-><init>()V

    .line 58
    new-instance v0, Lcom/twitter/android/events/b;

    invoke-direct {v0}, Lcom/twitter/android/events/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/events/EventLandingFragment;->af:I

    .line 64
    iput v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ai:I

    .line 65
    iput v1, p0, Lcom/twitter/android/events/EventLandingFragment;->aj:I

    .line 66
    iput v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ak:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/events/EventLandingFragment;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ac:I

    return v0
.end method

.method private aO()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/events/TwitterEventActivity;

    .line 166
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->aj()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    move-object v4, v1

    .line 167
    :goto_0
    if-eqz v4, :cond_0

    if-nez v0, :cond_2

    .line 190
    :cond_0
    :goto_1
    return-void

    .line 166
    :cond_1
    const/4 v1, 0x0

    move-object v4, v1

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {v4}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    .line 171
    iget-boolean v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ah:Z

    if-eqz v1, :cond_0

    move v2, v3

    .line 172
    :goto_2
    invoke-interface {v5}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 173
    invoke-interface {v5, v2}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/bu;

    .line 174
    if-eqz v1, :cond_4

    iget-boolean v1, v1, Lcom/twitter/android/bu;->k:Z

    if-eqz v1, :cond_4

    .line 175
    iput v2, p0, Lcom/twitter/android/events/EventLandingFragment;->ac:I

    .line 179
    :cond_3
    new-instance v1, Lcom/twitter/android/events/EventLandingFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/events/EventLandingFragment$1;-><init>(Lcom/twitter/android/events/EventLandingFragment;Lcom/twitter/android/events/TwitterEventActivity;)V

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v1, v6, v7}, Landroid/widget/ListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 187
    iput-boolean v3, p0, Lcom/twitter/android/events/EventLandingFragment;->ah:Z

    .line 188
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ai:I

    goto :goto_1

    .line 172
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2
.end method


# virtual methods
.method protected E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->aa:Ljava/lang/String;

    return-object v0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->a(Landroid/content/Context;)V

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->I_()V

    goto :goto_0
.end method

.method protected a(Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/bu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    const/4 v0, 0x3

    iget v1, p0, Lcom/twitter/android/events/EventLandingFragment;->k:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ai:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/events/EventLandingFragment;->l:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 127
    invoke-virtual {p1}, Lcbi;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ah:Z

    .line 128
    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->a(Lcbi;)V

    .line 129
    invoke-direct {p0}, Lcom/twitter/android/events/EventLandingFragment;->aO()V

    .line 130
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcno;I)V
    .locals 2

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/events/TwitterEventActivity;

    .line 250
    invoke-virtual {v0}, Lcom/twitter/android/events/TwitterEventActivity;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 251
    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    invoke-virtual {v1, p2}, Lcom/twitter/android/events/b;->a(I)V

    .line 252
    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/android/events/b;)V

    .line 253
    iget-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    invoke-virtual {v0}, Lcom/twitter/android/events/b;->d()V

    .line 254
    if-nez p2, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 256
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 257
    if-eqz v1, :cond_0

    .line 259
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/events/EventLandingFragment;->aj:I

    .line 260
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ak:I

    .line 263
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/SearchResultsFragment;->a(Lcno;I)V

    .line 265
    :cond_1
    return-void
.end method

.method public a(Lcno;IIIZ)V
    .locals 3

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/events/TwitterEventActivity;

    .line 239
    invoke-virtual {v0}, Lcom/twitter/android/events/TwitterEventActivity;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    iget v2, p0, Lcom/twitter/android/events/EventLandingFragment;->N:I

    invoke-virtual {v1, p1, p2, v2}, Lcom/twitter/android/events/b;->a(Lcno;II)V

    .line 241
    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/android/events/b;)V

    .line 242
    iget-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    invoke-virtual {v0}, Lcom/twitter/android/events/b;->d()V

    .line 243
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/SearchResultsFragment;->a(Lcno;IIIZ)V

    .line 245
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 203
    const v0, 0x7f040396

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v1

    .line 204
    invoke-virtual {p1}, Lcom/twitter/app/common/list/l$d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f040221

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 206
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    const v0, 0x7f0403e0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 208
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 210
    :cond_0
    return-void

    .line 204
    :cond_1
    const v0, 0x7f040397

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 2

    .prologue
    .line 228
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 229
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 230
    instance-of v1, v0, Lcom/twitter/android/ScrollingHeaderActivity;

    if-eqz v1, :cond_0

    .line 231
    check-cast v0, Lcom/twitter/android/ScrollingHeaderActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Z)V

    .line 233
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/model/topic/TwitterTopic;)V
    .locals 2

    .prologue
    .line 143
    if-eqz p1, :cond_0

    .line 144
    const-class v0, Lcom/twitter/model/topic/d;

    .line 145
    invoke-virtual {p1, v0}, Lcom/twitter/model/topic/TwitterTopic;->a(Ljava/lang/Class;)Lcom/twitter/model/topic/b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/d;

    .line 146
    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/events/TwitterEventActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/events/TwitterEventActivity;->a(Lcom/twitter/model/topic/d;)V

    .line 150
    :cond_0
    return-void
.end method

.method protected aP_()V
    .locals 2

    .prologue
    .line 278
    invoke-super {p0}, Lcom/twitter/android/SearchResultsFragment;->aP_()V

    .line 279
    iget-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    invoke-virtual {v0}, Lcom/twitter/android/events/b;->d()V

    .line 280
    iget-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ad:Lcom/twitter/android/events/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/b;->b(I)V

    .line 281
    return-void
.end method

.method protected ap_()Z
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 269
    invoke-super {p0}, Lcom/twitter/android/SearchResultsFragment;->b()V

    .line 270
    iget-boolean v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ag:Z

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->H_()V

    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ag:Z

    .line 274
    :cond_0
    return-void
.end method

.method public d_(I)V
    .locals 2

    .prologue
    .line 291
    iget v0, p0, Lcom/twitter/android/events/EventLandingFragment;->aj:I

    if-nez v0, :cond_0

    .line 292
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/events/EventLandingFragment;->aj:I

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 293
    return-void

    .line 291
    :cond_0
    iget p1, p0, Lcom/twitter/android/events/EventLandingFragment;->ak:I

    goto :goto_0
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget v0, p0, Lcom/twitter/android/events/EventLandingFragment;->l:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const-string/jumbo v6, "ev_start_time ASC "

    .line 157
    :goto_0
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$t;->a:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/twitter/android/events/EventLandingFragment;->a_:J

    .line 158
    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lbtv;->a:[Ljava/lang/String;

    const-string/jumbo v4, "search_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/twitter/android/events/EventLandingFragment;->r:J

    .line 161
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return-object v0

    .line 154
    :cond_0
    const-string/jumbo v6, "type_id ASC, _id ASC"

    goto :goto_0
.end method

.method protected l()V
    .locals 2

    .prologue
    .line 216
    const/16 v0, 0x8

    iget v1, p0, Lcom/twitter/android/events/EventLandingFragment;->l:I

    if-eq v0, v1, :cond_0

    .line 217
    invoke-super {p0}, Lcom/twitter/android/SearchResultsFragment;->l()V

    .line 219
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->k()Lcom/twitter/android/bs;

    move-result-object v0

    .line 72
    if-eqz p1, :cond_0

    .line 73
    const-string/jumbo v1, "scribe_section"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->aa:Ljava/lang/String;

    .line 74
    const-string/jumbo v1, "scribe_component"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ab:Ljava/lang/String;

    .line 75
    const-string/jumbo v1, "first_time"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ah:Z

    .line 76
    const-string/jumbo v1, "count"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ai:I

    .line 82
    :goto_0
    const-string/jumbo v1, "search_type"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->b(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/events/EventLandingFragment;->af:I

    .line 83
    const-string/jumbo v1, "event_id"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ae:Ljava/lang/String;

    .line 84
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->A:Ljava/lang/String;

    .line 85
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->aa:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ab:Ljava/lang/String;

    .line 87
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v1, 0x6

    .line 88
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->C:Ljava/lang/String;

    .line 89
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 84
    invoke-virtual {p0, v0}, Lcom/twitter/android/events/EventLandingFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 90
    new-instance v0, Lcom/twitter/android/ck;

    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/events/EventLandingFragment;->H:Lcom/twitter/android/ck;

    .line 91
    return-void

    .line 78
    :cond_0
    const-string/jumbo v1, "scribe_section"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->aa:Ljava/lang/String;

    .line 79
    const-string/jumbo v1, "scribe_component"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ab:Ljava/lang/String;

    .line 80
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ah:Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    const-string/jumbo v0, "scribe_section"

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string/jumbo v0, "scribe_component"

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget v0, p0, Lcom/twitter/android/events/EventLandingFragment;->af:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 102
    const-string/jumbo v0, "event_id"

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ae:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_0
    const-string/jumbo v0, "first_time"

    iget-boolean v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ah:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    const-string/jumbo v0, "count"

    iget v1, p0, Lcom/twitter/android/events/EventLandingFragment;->ai:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    return-void
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/events/EventLandingFragment;->ag:Z

    .line 286
    invoke-super {p0}, Lcom/twitter/android/SearchResultsFragment;->q_()V

    .line 287
    return-void
.end method

.method protected s()V
    .locals 4

    .prologue
    .line 111
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/events/EventLandingFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/events/EventLandingFragment;->A:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/events/EventLandingFragment;->aa:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/android/events/EventLandingFragment;->ab:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "show_polled_content"

    aput-object v3, v1, v2

    .line 112
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/events/EventLandingFragment;->C:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/events/EventLandingFragment;->p:I

    .line 114
    invoke-static {v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 111
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 115
    invoke-super {p0}, Lcom/twitter/android/SearchResultsFragment;->s()V

    .line 116
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->setUserVisibleHint(Z)V

    .line 195
    if-eqz p1, :cond_0

    .line 196
    invoke-direct {p0}, Lcom/twitter/android/events/EventLandingFragment;->aO()V

    .line 198
    :cond_0
    return-void
.end method
