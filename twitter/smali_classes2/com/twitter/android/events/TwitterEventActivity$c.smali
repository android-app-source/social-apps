.class Lcom/twitter/android/events/TwitterEventActivity$c;
.super Lcom/twitter/android/aw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/events/TwitterEventActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/events/TwitterEventActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/events/TwitterEventActivity;Landroid/net/Uri;Lcom/twitter/android/at;)V
    .locals 0

    .prologue
    .line 1097
    iput-object p1, p0, Lcom/twitter/android/events/TwitterEventActivity$c;->a:Lcom/twitter/android/events/TwitterEventActivity;

    .line 1098
    invoke-direct {p0, p2, p3}, Lcom/twitter/android/aw;-><init>(Landroid/net/Uri;Lcom/twitter/android/at;)V

    .line 1099
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity$c;->a:Lcom/twitter/android/events/TwitterEventActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/TwitterEventActivity;->b(Z)V

    .line 1110
    invoke-super {p0}, Lcom/twitter/android/aw;->a()V

    .line 1111
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/twitter/android/events/TwitterEventActivity$c;->a:Lcom/twitter/android/events/TwitterEventActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/events/TwitterEventActivity;->b(Z)V

    .line 1104
    invoke-super {p0, p1}, Lcom/twitter/android/aw;->a(I)V

    .line 1105
    return-void
.end method

.method protected a(Lcom/twitter/library/client/m;)V
    .locals 4

    .prologue
    .line 1116
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity$c;->a:Lcom/twitter/android/events/TwitterEventActivity;

    .line 1117
    invoke-static {v3}, Lcom/twitter/android/events/TwitterEventActivity;->e(Lcom/twitter/android/events/TwitterEventActivity;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/android/events/TwitterEventActivity$c;->a:Lcom/twitter/android/events/TwitterEventActivity;

    .line 1118
    invoke-static {v3}, Lcom/twitter/android/events/TwitterEventActivity;->f(Lcom/twitter/android/events/TwitterEventActivity;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "polled_content_available"

    aput-object v3, v1, v2

    .line 1117
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/events/TwitterEventActivity$c;->a:Lcom/twitter/android/events/TwitterEventActivity;

    .line 1119
    invoke-static {v1}, Lcom/twitter/android/events/TwitterEventActivity;->c(Lcom/twitter/android/events/TwitterEventActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/events/TwitterEventActivity$c;->a:Lcom/twitter/android/events/TwitterEventActivity;

    invoke-static {v2}, Lcom/twitter/android/events/TwitterEventActivity;->d(Lcom/twitter/android/events/TwitterEventActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1116
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1120
    return-void
.end method
