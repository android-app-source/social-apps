.class public Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/events/sports/nba/a;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lcom/twitter/media/ui/image/UserImageView;

.field private final c:Lcom/twitter/media/ui/image/UserImageView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Lcom/twitter/android/util/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 38
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Lcom/twitter/android/util/e;

    invoke-direct {v0}, Lcom/twitter/android/util/e;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->h:Lcom/twitter/android/util/e;

    .line 47
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040260

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 48
    const v0, 0x7f1302ae

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->a:Landroid/widget/TextView;

    .line 50
    invoke-virtual {p0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e04cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 51
    const v0, 0x7f1302b2

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->b:Lcom/twitter/media/ui/image/UserImageView;

    .line 52
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->b:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 53
    const v0, 0x7f1302b7

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->c:Lcom/twitter/media/ui/image/UserImageView;

    .line 54
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->c:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 55
    const v0, 0x7f1302b4

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->d:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f1302b9

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->e:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f1302b5

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->f:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f1302ba

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->g:Landroid/widget/TextView;

    .line 59
    return-void
.end method

.method private a(Landroid/view/View;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    invoke-static {p4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 108
    new-instance v0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl$1;-><init>(Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;J)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 63
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->setVisibility(I)V

    .line 64
    return-void
.end method

.method public a(Lcom/twitter/model/topic/a;Lcom/twitter/model/topic/a;)V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->b:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p1, Lcom/twitter/model/topic/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 81
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->c:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p2, Lcom/twitter/model/topic/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 82
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->b:Lcom/twitter/media/ui/image/UserImageView;

    iget-wide v2, p1, Lcom/twitter/model/topic/a;->h:J

    iget-object v1, p1, Lcom/twitter/model/topic/a;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->a(Landroid/view/View;JLjava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->c:Lcom/twitter/media/ui/image/UserImageView;

    iget-wide v2, p2, Lcom/twitter/model/topic/a;->h:J

    iget-object v1, p2, Lcom/twitter/model/topic/a;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->a(Landroid/view/View;JLjava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/topic/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/twitter/model/topic/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->setVisibility(I)V

    .line 69
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 74
    return-object p0
.end method

.method public setGameStatusDate(J)V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->h:Lcom/twitter/android/util/e;

    iget-object v1, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->a:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/twitter/android/util/e;->a(Landroid/widget/TextView;JZ)V

    .line 97
    return-void
.end method

.method public setGameStatusText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/events/sports/nba/NBAScoreCardImpl;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method
