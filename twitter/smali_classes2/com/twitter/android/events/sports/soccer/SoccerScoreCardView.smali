.class public Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lcom/twitter/media/ui/image/UserImageView;

.field private final c:Lcom/twitter/media/ui/image/UserImageView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/content/Context;

.field private i:J

.field private j:J

.field private final k:Landroid/widget/LinearLayout;

.field private final l:Landroid/graphics/Rect;

.field private final m:Lcom/twitter/android/util/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    new-instance v0, Lcom/twitter/android/util/e;

    invoke-direct {v0}, Lcom/twitter/android/util/e;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->m:Lcom/twitter/android/util/e;

    .line 66
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403ca

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 67
    const v0, 0x7f1302ae

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f1302b2

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->b:Lcom/twitter/media/ui/image/UserImageView;

    .line 69
    const v0, 0x7f1302b7

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->c:Lcom/twitter/media/ui/image/UserImageView;

    .line 70
    const v0, 0x7f1302b5

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->d:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f1302ba

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->e:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f1302b3

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->f:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f1302b8

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->g:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f13016c

    invoke-virtual {p0, v0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->k:Landroid/widget/LinearLayout;

    .line 75
    iput-object p1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->h:Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView$a;-><init>(Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView$1;)V

    .line 77
    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->b:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->c:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->l:Landroid/graphics/Rect;

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;)J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->i:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;)J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->j:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->h:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/widget/TopicView$TopicData;)V
    .locals 12

    .prologue
    const/16 v5, 0x8

    const/4 v11, 0x0

    const v10, 0x7f1100d9

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 85
    iget-object v0, p1, Lcom/twitter/android/widget/TopicView$TopicData;->m:[B

    sget-object v1, Lcom/twitter/model/topic/b;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/b;

    .line 88
    instance-of v1, v0, Lcom/twitter/model/topic/d;

    if-nez v1, :cond_1

    .line 89
    new-instance v1, Ljava/io/InvalidClassException;

    if-nez v0, :cond_0

    const-string/jumbo v0, "null"

    :goto_0
    const-string/jumbo v2, "Invalid TwitterTopic provided for ScoreCard"

    invoke-direct {v1, v0, v2}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 176
    :goto_1
    return-void

    .line 89
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_1
    check-cast v0, Lcom/twitter/model/topic/d;

    .line 95
    iget-object v1, v0, Lcom/twitter/model/topic/d;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/model/topic/d;->h:Ljava/util/List;

    if-nez v1, :cond_3

    .line 96
    :cond_2
    invoke-virtual {p0, v5}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->setVisibility(I)V

    goto :goto_1

    .line 99
    :cond_3
    invoke-virtual {p0, v3}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->setVisibility(I)V

    .line 102
    iget-object v2, v0, Lcom/twitter/model/topic/d;->h:Ljava/util/List;

    .line 103
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/topic/a;

    .line 104
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/topic/a;

    .line 105
    if-eqz v1, :cond_4

    if-nez v2, :cond_5

    .line 106
    :cond_4
    invoke-virtual {p0, v5}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->setVisibility(I)V

    goto :goto_1

    .line 109
    :cond_5
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->b:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v6, v1, Lcom/twitter/model/topic/a;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 110
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->c:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v6, v2, Lcom/twitter/model/topic/a;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 111
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->f:Landroid/widget/TextView;

    iget-object v6, v1, Lcom/twitter/model/topic/a;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->g:Landroid/widget/TextView;

    iget-object v6, v2, Lcom/twitter/model/topic/a;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-wide v6, v1, Lcom/twitter/model/topic/a;->h:J

    iput-wide v6, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->i:J

    .line 114
    iget-wide v6, v2, Lcom/twitter/model/topic/a;->h:J

    iput-wide v6, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->j:J

    .line 115
    iget-wide v6, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->i:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_6

    iget-object v5, v1, Lcom/twitter/model/topic/a;->c:Ljava/lang/String;

    invoke-static {v5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 116
    :cond_6
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->b:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v5, v3}, Lcom/twitter/media/ui/image/UserImageView;->setClickable(Z)V

    .line 117
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->f:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 123
    :goto_2
    iget-wide v6, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->j:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_7

    iget-object v5, v2, Lcom/twitter/model/topic/a;->c:Ljava/lang/String;

    invoke-static {v5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 124
    :cond_7
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->c:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v5, v3}, Lcom/twitter/media/ui/image/UserImageView;->setClickable(Z)V

    .line 125
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->g:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 130
    :goto_3
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->d:Landroid/widget/TextView;

    iget-object v1, v1, Lcom/twitter/model/topic/a;->d:Ljava/lang/String;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->e:Landroid/widget/TextView;

    iget-object v2, v2, Lcom/twitter/model/topic/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 133
    iget-object v5, v0, Lcom/twitter/model/topic/d;->f:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_8
    :goto_4
    packed-switch v1, :pswitch_data_0

    goto/16 :goto_1

    .line 135
    :pswitch_0
    iget-object v0, v0, Lcom/twitter/model/topic/d;->e:Ljava/lang/String;

    .line 136
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 137
    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 139
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 119
    :cond_9
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->b:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v5, v4}, Lcom/twitter/media/ui/image/UserImageView;->setClickable(Z)V

    .line 120
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->f:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setClickable(Z)V

    goto :goto_2

    .line 127
    :cond_a
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->c:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v5, v4}, Lcom/twitter/media/ui/image/UserImageView;->setClickable(Z)V

    .line 128
    iget-object v5, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->g:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setClickable(Z)V

    goto :goto_3

    .line 133
    :sswitch_0
    const-string/jumbo v4, "IN_PROGRESS"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v1, v3

    goto :goto_4

    :sswitch_1
    const-string/jumbo v3, "SCHEDULED"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v1, v4

    goto :goto_4

    :sswitch_2
    const-string/jumbo v3, "POSTPONED"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v1, 0x2

    goto :goto_4

    :sswitch_3
    const-string/jumbo v3, "COMPLETED"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v1, 0x3

    goto :goto_4

    :sswitch_4
    const-string/jumbo v3, "CANCELLED"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v1, 0x4

    goto :goto_4

    .line 141
    :cond_b
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    const v1, 0x7f0a04a2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    const v1, 0x7f0207e6

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 144
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    const v1, 0x7f110195

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 149
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->m:Lcom/twitter/android/util/e;

    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    iget-wide v4, p1, Lcom/twitter/android/widget/TopicView$TopicData;->l:J

    invoke-virtual {v0, v1, v4, v5}, Lcom/twitter/android/util/e;->a(Landroid/widget/TextView;J)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 151
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 155
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    const v1, 0x7f0a06c2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 161
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    const v1, 0x7f0a01ea

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 163
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 167
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    const v1, 0x7f0a00f9

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 169
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 133
    nop

    :sswitch_data_0
    .sparse-switch
        -0x637c2f73 -> :sswitch_1
        -0x3d7fc6cf -> :sswitch_4
        -0x2408abf9 -> :sswitch_0
        0x1e68426e -> :sswitch_2
        0x5279062b -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x0

    .line 180
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 181
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->l:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 183
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    if-ge v0, v5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->l:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-le v0, v5, :cond_0

    .line 184
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->k:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/events/sports/soccer/SoccerScoreCardView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
