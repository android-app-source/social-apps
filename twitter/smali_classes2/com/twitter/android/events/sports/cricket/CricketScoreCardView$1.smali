.class Lcom/twitter/android/events/sports/cricket/CricketScoreCardView$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;->a(Landroid/view/View;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;


# direct methods
.method constructor <init>(Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;J)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView$1;->b:Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;

    iput-wide p2, p0, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 295
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView$1;->b:Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;

    invoke-static {v1}, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;->a(Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    iget-wide v2, p0, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView$1;->a:J

    .line 296
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    .line 297
    const-string/jumbo v2, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v3, 0x5

    .line 299
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v4, p0, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView$1;->a:J

    .line 300
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 297
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 301
    iget-object v0, p0, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView$1;->b:Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;

    invoke-static {v0}, Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;->a(Lcom/twitter/android/events/sports/cricket/CricketScoreCardView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 302
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:event_card:cricket::profile_click"

    aput-object v3, v1, v2

    .line 303
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 302
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 304
    return-void
.end method
