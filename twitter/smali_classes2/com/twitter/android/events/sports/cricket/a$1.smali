.class final Lcom/twitter/android/events/sports/cricket/a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/events/sports/cricket/a;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/topic/a;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/twitter/media/ui/image/MediaImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/model/topic/a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/model/topic/a;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/twitter/android/events/sports/cricket/a$1;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/events/sports/cricket/a$1;->b:Lcom/twitter/model/topic/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/events/sports/cricket/a$1;->a:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lcom/twitter/android/events/sports/cricket/a$1;->b:Lcom/twitter/model/topic/a;

    iget-wide v2, v2, Lcom/twitter/model/topic/a;->h:J

    .line 65
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    iget-object v2, p0, Lcom/twitter/android/events/sports/cricket/a$1;->b:Lcom/twitter/model/topic/a;

    iget-object v2, v2, Lcom/twitter/model/topic/a;->c:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 67
    const-string/jumbo v2, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v3, 0x5

    .line 69
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p0, Lcom/twitter/android/events/sports/cricket/a$1;->b:Lcom/twitter/model/topic/a;

    iget-wide v4, v3, Lcom/twitter/model/topic/a;->h:J

    .line 70
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 67
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/events/sports/cricket/a$1;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 72
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 73
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:event_card:cricket::profile_click"

    aput-object v3, v1, v2

    .line 74
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 73
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 75
    return-void
.end method
