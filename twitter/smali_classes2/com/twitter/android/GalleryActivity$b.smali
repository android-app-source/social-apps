.class Lcom/twitter/android/GalleryActivity$b;
.super Lcom/twitter/util/android/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/GalleryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:J

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 1843
    .line 1844
    invoke-static {p6}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v6, "status_groups_type DESC"

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1843
    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1845
    iput-wide p7, p0, Lcom/twitter/android/GalleryActivity$b;->a:J

    .line 1846
    return-void

    :cond_0
    move-object v6, p6

    .line 1844
    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/twitter/model/core/Tweet;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;",
            "Lcom/twitter/model/core/Tweet;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1869
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1870
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 1871
    invoke-static {p1, v0}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    .line 1873
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 1874
    new-instance v2, Lss;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/request/a$a;

    move-result-object v3

    iget-object v4, v0, Lcom/twitter/model/core/MediaEntity;->y:Ljava/lang/String;

    invoke-direct {v2, p1, v0, v3, v4}, Lss;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/media/request/a$a;Ljava/lang/String;)V

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1871
    :cond_0
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 1872
    invoke-static {p1, p2, p3, v0}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;JLcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1877
    :cond_1
    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1878
    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->u(Lcom/twitter/model/core/Tweet;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 1879
    if-eqz v0, :cond_2

    .line 1880
    new-instance v1, Lst;

    invoke-direct {v1, p1, v0}, Lst;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/media/request/a$a;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1883
    :cond_2
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1865
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity$b;->b:Ljava/util/List;

    return-object v0
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 1850
    invoke-super {p0}, Lcom/twitter/util/android/d;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    .line 1851
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1852
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity$b;->b:Ljava/util/List;

    .line 1861
    :goto_0
    return-object v0

    .line 1854
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1856
    :cond_2
    sget-object v2, Lbtd;->a:Lbtd;

    invoke-virtual {v2, v0}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v2

    .line 1857
    iget-wide v4, p0, Lcom/twitter/android/GalleryActivity$b;->a:J

    invoke-static {v1, v2, v4, v5}, Lcom/twitter/android/GalleryActivity$b;->a(Ljava/util/List;Lcom/twitter/model/core/Tweet;J)V

    .line 1858
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1859
    iput-object v1, p0, Lcom/twitter/android/GalleryActivity$b;->b:Ljava/util/List;

    goto :goto_0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1836
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity$b;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
