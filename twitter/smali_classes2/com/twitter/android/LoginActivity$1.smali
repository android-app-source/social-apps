.class Lcom/twitter/android/LoginActivity$1;
.super Lcom/twitter/util/ui/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/LoginActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginActivity;

.field private b:Z

.field private final c:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/LoginActivity;)V
    .locals 1

    .prologue
    .line 208
    iput-object p1, p0, Lcom/twitter/android/LoginActivity$1;->a:Lcom/twitter/android/LoginActivity;

    invoke-direct {p0}, Lcom/twitter/util/ui/d;-><init>()V

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity$1;->b:Z

    .line 210
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$1;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;)Lcom/twitter/ui/widget/TwitterEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity$1;->c:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 218
    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity$1;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity$1;->b:Z

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$1;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->b(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "login:::username:edit"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/twitter/android/am;->a(J[Ljava/lang/String;)V

    .line 220
    iput-boolean v5, p0, Lcom/twitter/android/LoginActivity$1;->b:Z

    .line 222
    :cond_0
    return-void
.end method
