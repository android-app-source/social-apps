.class Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/cz$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->C()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ScrollingHeaderUserQueryActivity;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 136
    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-static {v0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/android/ScrollingHeaderUserQueryActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    if-eqz p1, :cond_3

    .line 142
    invoke-static {}, Lcom/twitter/media/util/s;->a()Lcom/twitter/media/util/s;

    move-result-object v0

    .line 143
    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/media/util/s;->a(J)Lcom/twitter/media/model/MediaFile;

    move-result-object v2

    .line 144
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/twitter/media/model/MediaFile;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 145
    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/media/util/s;->b(J)V

    .line 148
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 153
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    iget-wide v2, v0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->A:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    iget-object v0, v0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->B:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v1

    .line 154
    :goto_1
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/android/ScrollingHeaderUserQueryActivity;Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 155
    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->D()V

    goto :goto_0

    .line 153
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 156
    :cond_6
    if-eqz p1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->b(Lcom/twitter/android/ScrollingHeaderUserQueryActivity;Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 160
    new-instance v0, Lbhx;

    iget-object v2, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    iget-object v3, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    .line 161
    invoke-static {v3}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->b(Lcom/twitter/android/ScrollingHeaderUserQueryActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lbhx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 162
    iput-object p1, v0, Lbhx;->a:Lcom/twitter/model/core/TwitterUser;

    .line 163
    iget-object v2, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    const/16 v3, 0xc8

    invoke-static {v2, v0, v3}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/android/ScrollingHeaderUserQueryActivity;Lcom/twitter/library/service/s;I)Z

    .line 164
    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    invoke-static {v0, v1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/android/ScrollingHeaderUserQueryActivity;Z)Z

    .line 167
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity$1;->a:Lcom/twitter/android/ScrollingHeaderUserQueryActivity;

    iget-object v0, v0, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->E:Lcom/twitter/android/profiles/x;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/x;->a()V

    goto/16 :goto_0
.end method
