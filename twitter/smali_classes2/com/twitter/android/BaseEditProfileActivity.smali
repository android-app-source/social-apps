.class public abstract Lcom/twitter/android/BaseEditProfileActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/profiles/HeaderImageView$a;
.implements Lcom/twitter/android/profiles/g$c;
.implements Lcom/twitter/app/common/dialog/b$a;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/BaseEditProfileActivity$a;
    }
.end annotation


# static fields
.field private static final m:[Ljava/lang/String;


# instance fields
.field protected a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected b:Lcom/twitter/android/profiles/t;

.field c:Lcom/twitter/model/media/EditableImage;

.field d:Lcom/twitter/model/media/EditableImage;

.field e:Z

.field f:Z

.field g:Z

.field h:Lcom/twitter/model/core/TwitterUser;

.field i:Lcom/twitter/android/profiles/HeaderImageView;

.field j:Lcom/twitter/media/ui/image/UserImageView;

.field k:Landroid/widget/EditText;

.field l:Ljava/lang/String;

.field private final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:Z

.field private q:Lcom/twitter/android/profiles/g;

.field private r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

.field private s:Lcom/twitter/android/BaseEditProfileActivity$a;

.field private t:Lcom/twitter/media/model/MediaFile;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 96
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/BaseEditProfileActivity;->m:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/android/BaseEditProfileActivity$a;)Lcom/twitter/android/BaseEditProfileActivity$a;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/android/BaseEditProfileActivity;->s:Lcom/twitter/android/BaseEditProfileActivity$a;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/model/MediaFile;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/android/BaseEditProfileActivity;->t:Lcom/twitter/media/model/MediaFile;

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 471
    const-string/jumbo v0, "%s:%s:%s:%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 472
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 473
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    .line 471
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e(Lcom/twitter/media/model/MediaFile;)V
    .locals 13

    .prologue
    .line 516
    if-eqz p1, :cond_6

    sget-object v0, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    .line 517
    invoke-static {p1, v0}, Lcom/twitter/model/media/EditableImage;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 518
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v12

    .line 519
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->n()Ljava/lang/String;

    move-result-object v5

    .line 521
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/profiles/g;->a(J)V

    .line 523
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "update_header"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 526
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->t()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 527
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->j()Ljava/lang/String;

    move-result-object v4

    .line 528
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->z_()Ljava/lang/String;

    move-result-object v6

    .line 529
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->l()Ljava/lang/String;

    move-result-object v7

    .line 531
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->m()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v8

    .line 533
    invoke-static {}, Lbhu;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 534
    invoke-static {}, Lbhu;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Z)Lcom/twitter/model/profile/ExtendedProfile;

    move-result-object v11

    .line 535
    :goto_1
    new-instance v0, Lcom/twitter/library/client/n;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    iget-object v1, v1, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v1, Lcom/twitter/media/model/ImageFile;

    :goto_2
    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    iget-object v2, v2, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v2, Lcom/twitter/media/model/ImageFile;

    :goto_3
    iget-boolean v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    .line 539
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->r()Z

    move-result v9

    .line 540
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->s()Z

    move-result v10

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/client/n;-><init>(Lcom/twitter/media/model/MediaFile;Lcom/twitter/media/model/MediaFile;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/util/collection/k;ZZLcom/twitter/model/profile/ExtendedProfile;)V

    .line 541
    invoke-static {p0, v12, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;)Ljava/lang/String;

    .line 551
    :cond_2
    :goto_4
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(J)V

    .line 553
    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    if-eqz v0, :cond_3

    .line 554
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/profiles/g;->b(J)V

    .line 555
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "remove_header"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 556
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    .line 557
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    .line 560
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_4

    .line 561
    invoke-static {}, Lcom/twitter/media/util/s;->a()Lcom/twitter/media/util/s;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    iget-object v1, v1, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    .line 562
    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/media/util/s;->a(JLcom/twitter/media/model/MediaFile;)V

    .line 564
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_5

    .line 565
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-static {v1}, Lcom/twitter/android/profiles/f;->a(Lcom/twitter/android/profiles/t;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/g;->c(Lcom/twitter/media/request/a$a;)V

    .line 568
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 569
    const-string/jumbo v1, "updated_profile_picture"

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->t:Lcom/twitter/media/model/MediaFile;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 571
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    .line 572
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 574
    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/content/Intent;)V

    .line 575
    return-void

    .line 517
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 534
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 535
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 542
    :cond_a
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 543
    new-instance v2, Lcom/twitter/library/client/n;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    move-object v1, v0

    :goto_5
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    :goto_6
    iget-boolean v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    invoke-direct {v2, v1, v0, v3}, Lcom/twitter/library/client/n;-><init>(Lcom/twitter/media/model/MediaFile;Lcom/twitter/media/model/MediaFile;Z)V

    .line 548
    invoke-static {p0, v12, v2}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;)Ljava/lang/String;

    goto/16 :goto_4

    .line 543
    :cond_b
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_5

    :cond_c
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private x()V
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->b()V

    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Z

    .line 338
    return-void
.end method

.method private y()V
    .locals 2

    .prologue
    .line 700
    .line 701
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0357

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 702
    iget-boolean v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Z

    if-eqz v1, :cond_1

    .line 703
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 704
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
.end method

.method protected a(Z)Lcom/twitter/model/profile/ExtendedProfile;
    .locals 1

    .prologue
    .line 579
    const/4 v0, 0x0

    return-object v0
.end method

.method a(I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 756
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-nez v0, :cond_0

    .line 757
    invoke-static {p1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 758
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 759
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 761
    :cond_0
    return-void
.end method

.method protected a(J)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 679
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 680
    new-array v0, v6, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "header_image"

    const-string/jumbo v4, "change"

    .line 681
    invoke-static {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 680
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 684
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 685
    new-array v0, v6, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "bio"

    const-string/jumbo v4, "change"

    .line 686
    invoke-static {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 685
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 688
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_2

    .line 689
    new-array v0, v6, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "avatar"

    const-string/jumbo v4, "change"

    .line 690
    invoke-static {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 689
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 693
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    if-eqz v0, :cond_3

    .line 694
    new-array v0, v6, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "header_image"

    const-string/jumbo v4, "remove"

    .line 695
    invoke-static {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 694
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 697
    :cond_3
    return-void
.end method

.method protected varargs a(J[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-static {p1, p2, v0, p3}, Lcom/twitter/android/profiles/v;->a(JLcom/twitter/android/profiles/t;[Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method public a(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 415
    packed-switch p2, :pswitch_data_0

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 417
    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableImage;->i()Lrx/g;

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_2

    .line 422
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableImage;->i()Lrx/g;

    .line 424
    :cond_2
    invoke-virtual {p0, v7}, Lcom/twitter/android/BaseEditProfileActivity;->setResult(I)V

    .line 425
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, ""

    const-string/jumbo v5, ""

    const-string/jumbo v6, "cancel"

    .line 426
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 425
    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 427
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->finish()V

    goto :goto_0

    .line 433
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 434
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 435
    const v2, 0x7f0a0358

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 436
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "change_header_dialog"

    const-string/jumbo v5, "take_photo"

    const-string/jumbo v6, "click"

    .line 437
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 436
    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 438
    iput-boolean v7, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    .line 439
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    const v1, 0x7f0a03e0

    .line 440
    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/BaseEditProfileActivity;->m:[Ljava/lang/String;

    invoke-direct {v0, v1, p0, v2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v1, "change_header_dialog"

    const-string/jumbo v2, "take_photo"

    .line 441
    invoke-direct {p0, v1, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 442
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 443
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 444
    :cond_3
    const v2, 0x7f0a0353

    .line 445
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 444
    invoke-static {v0, v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 446
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "change_header_dialog"

    const-string/jumbo v5, "choose_photo"

    const-string/jumbo v6, "click"

    .line 447
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 446
    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 449
    iput-boolean v7, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    .line 450
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lbrv;->a(Landroid/app/Activity;I)Z

    goto/16 :goto_0

    .line 452
    :cond_4
    const v2, 0x7f0a0357

    .line 453
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 452
    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    iput-object v9, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 455
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "change_header_dialog"

    const-string/jumbo v5, "remove"

    const-string/jumbo v6, "click"

    .line 456
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 455
    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 457
    iput-boolean v8, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    .line 458
    iput-boolean v7, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Z

    .line 459
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0, v9}, Lcom/twitter/android/profiles/HeaderImageView;->b(Lcom/twitter/media/request/a$a;)Z

    goto/16 :goto_0

    .line 415
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected abstract a(Landroid/content/Intent;)V
.end method

.method public a(Lcom/twitter/media/model/MediaFile;)V
    .locals 1

    .prologue
    .line 196
    if-eqz p1, :cond_0

    sget-object v0, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    .line 197
    invoke-static {p1, v0}, Lcom/twitter/model/media/EditableImage;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 198
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->u()V

    .line 199
    return-void

    .line 197
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 330
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2, p0}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/core/TwitterUser;Landroid/content/Context;)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 333
    :cond_0
    return-void
.end method

.method b()V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Z

    .line 213
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 214
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 215
    invoke-static {p0, v0}, Lbru;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 214
    :goto_1
    invoke-virtual {v1, v0}, Lcom/twitter/android/profiles/HeaderImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 217
    :cond_0
    return-void

    .line 212
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 215
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->a()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 134
    const v0, 0x7f130165

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:Lcom/twitter/media/ui/image/UserImageView;

    .line 135
    const v0, 0x7f13035f

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->k:Landroid/widget/EditText;

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    .line 137
    const v0, 0x7f13035d

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profiles/HeaderImageView;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    .line 138
    const-string/jumbo v0, "location_header_repository"

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profiles/g;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    .line 139
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    if-nez v0, :cond_5

    .line 140
    new-instance v0, Lcom/twitter/android/profiles/g;

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/twitter/android/profiles/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    .line 146
    :cond_0
    :goto_0
    new-instance v0, Lcom/twitter/android/profiles/t;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v0, p0, v3, v1}, Lcom/twitter/android/profiles/t;-><init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Z)V

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    .line 147
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_1

    .line 148
    const-string/jumbo v0, "bitmaps"

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 149
    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    iget-object v4, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v4, p0}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/core/TwitterUser;Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v3, p0, v0, v4}, Lcom/twitter/android/profiles/HeaderImageView;->a(Lcom/twitter/android/profiles/HeaderImageView$a;Ljava/util/Set;I)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, v3}, Lcom/twitter/android/profiles/HeaderImageView;->setProfileUser(Lcom/twitter/android/profiles/t;)V

    .line 153
    :cond_1
    invoke-static {p0}, Lcom/twitter/android/util/b;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    .line 155
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 156
    const v3, 0x7f0a0358

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 157
    iget-boolean v4, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    if-eqz v4, :cond_2

    .line 158
    iget-object v4, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_2
    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    const v4, 0x7f0a0353

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    if-eqz p1, :cond_6

    .line 163
    const-string/jumbo v0, "pending_avatar_media"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    .line 164
    const-string/jumbo v0, "initial_header"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    .line 165
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_3

    .line 166
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    iget-object v1, v1, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setCropRectangle(Lcom/twitter/util/math/c;)V

    .line 167
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {v1}, Lcom/twitter/model/media/EditableImage;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 169
    :cond_3
    const-string/jumbo v0, "pending_header_media"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 170
    const-string/jumbo v0, "has_updated_header"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Z

    .line 171
    const-string/jumbo v0, "remove_header"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    .line 172
    const-string/jumbo v0, "remove_header_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Z

    .line 174
    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_4

    .line 175
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 178
    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/BaseEditProfileActivity;->y()V

    .line 179
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->u()V

    .line 188
    :goto_1
    return-void

    .line 142
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    invoke-virtual {v0, p0}, Lcom/twitter/android/profiles/g;->a(Lcom/twitter/android/profiles/g$c;)V

    goto/16 :goto_0

    .line 181
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    new-array v0, v1, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const-string/jumbo v8, "impression"

    .line 182
    invoke-static {v3, v6, v7, v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 181
    invoke-virtual {p0, v4, v5, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    new-array v3, v1, [Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v7, ""

    const-string/jumbo v8, "camera"

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    if-eqz v0, :cond_7

    const-string/jumbo v0, "available"

    .line 184
    :goto_2
    invoke-static {v6, v7, v8, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    .line 183
    invoke-virtual {p0, v4, v5, v3}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    .line 186
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, p0, v1, p0}, Lcom/twitter/android/profiles/g;->a(Landroid/app/Activity;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/android/profiles/g$c;)V

    goto :goto_1

    .line 183
    :cond_7
    const-string/jumbo v0, "unavailable"

    goto :goto_2

    :cond_8
    move v0, v2

    .line 185
    goto :goto_3
.end method

.method b(Lcom/twitter/media/model/MediaFile;)V
    .locals 4

    .prologue
    .line 504
    invoke-static {}, Lcom/twitter/media/util/s;->a()Lcom/twitter/media/util/s;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3, p1}, Lcom/twitter/media/util/s;->a(JLcom/twitter/media/model/MediaFile;)V

    .line 505
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/n;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lcom/twitter/library/client/n;-><init>(Lcom/twitter/media/model/MediaFile;Lcom/twitter/media/model/MediaFile;Z)V

    invoke-static {p0, v0, v1}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;)Ljava/lang/String;

    .line 507
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_2

    .line 222
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->b()V

    .line 227
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Z

    .line 228
    return-void

    .line 224
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-static {v1}, Lcom/twitter/android/profiles/f;->a(Lcom/twitter/android/profiles/t;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->b(Lcom/twitter/media/request/a$a;)Z

    goto :goto_0

    .line 227
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c(Lcom/twitter/media/model/MediaFile;)V
    .locals 0

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->w()V

    .line 512
    invoke-direct {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->e(Lcom/twitter/media/model/MediaFile;)V

    .line 513
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Z

    if-nez v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableImage;->i()Lrx/g;

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableImage;->i()Lrx/g;

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    if-eqz v0, :cond_2

    .line 307
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/g;->a(Lcom/twitter/android/profiles/g$c;)V

    .line 309
    :cond_2
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 310
    return-void
.end method

.method d(Lcom/twitter/media/model/MediaFile;)V
    .locals 6

    .prologue
    .line 723
    if-eqz p1, :cond_0

    sget-object v0, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    .line 724
    invoke-static {p1, v0}, Lcom/twitter/model/media/EditableImage;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 726
    if-eqz p1, :cond_1

    .line 727
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    const-string/jumbo v2, "profile"

    const/high16 v3, 0x40400000    # 3.0f

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;FIZ)Landroid/content/Intent;

    move-result-object v0

    .line 729
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 733
    :goto_1
    return-void

    .line 724
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 731
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->v()V

    goto :goto_1
.end method

.method protected e()V
    .locals 3

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    if-nez v0, :cond_0

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    .line 262
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lbrv;->a(Landroid/app/Activity;I)Z

    .line 273
    :goto_0
    return-void

    .line 267
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/BaseEditProfileActivity;->y()V

    .line 268
    new-instance v1, Lcom/twitter/android/widget/aj$b;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Ljava/util/ArrayList;

    .line 269
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/aj$b;->a([Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 270
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 271
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 272
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method

.method i()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 489
    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-eqz v2, :cond_1

    .line 490
    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    iget-object v2, v2, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 491
    if-eqz v2, :cond_1

    const v3, 0x3a83126f    # 0.001f

    invoke-virtual {v2, v3}, Lcom/twitter/util/math/c;->a(F)Z

    move-result v2

    if-nez v2, :cond_1

    .line 492
    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->s:Lcom/twitter/android/BaseEditProfileActivity$a;

    if-eqz v2, :cond_0

    .line 493
    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->s:Lcom/twitter/android/BaseEditProfileActivity$a;

    invoke-virtual {v2, v0}, Lcom/twitter/android/BaseEditProfileActivity$a;->cancel(Z)Z

    .line 495
    :cond_0
    new-instance v2, Lcom/twitter/android/BaseEditProfileActivity$a;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/BaseEditProfileActivity$a;-><init>(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/model/media/EditableImage;)V

    iput-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->s:Lcom/twitter/android/BaseEditProfileActivity$a;

    .line 496
    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->s:Lcom/twitter/android/BaseEditProfileActivity$a;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v1}, Lcom/twitter/android/BaseEditProfileActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 500
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected abstract j()Ljava/lang/String;
.end method

.method protected abstract l()Ljava/lang/String;
.end method

.method protected l_()V
    .locals 2

    .prologue
    .line 287
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->l_()V

    .line 288
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 289
    const-string/jumbo v0, "bitmaps"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/HeaderImageView;->getSavedBitmaps()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    if-eqz v0, :cond_1

    .line 292
    const-string/jumbo v0, "location_header_repository"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    :cond_1
    return-void
.end method

.method protected m()Lcom/twitter/model/geo/TwitterPlace;
    .locals 1

    .prologue
    .line 604
    const/4 v0, 0x0

    return-object v0
.end method

.method protected n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->k:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()Z
    .locals 1

    .prologue
    .line 618
    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Z

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 342
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 343
    packed-switch p1, :pswitch_data_0

    .line 407
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 345
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 346
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {p0, v0, v1}, Lcom/twitter/media/model/MediaFile;->b(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/BaseEditProfileActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/BaseEditProfileActivity$1;-><init>(Lcom/twitter/android/BaseEditProfileActivity;)V

    .line 347
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 357
    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lrx/j;)V

    goto :goto_0

    .line 362
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 363
    const-string/jumbo v0, "media_file"

    .line 364
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 365
    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->d(Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0

    .line 370
    :pswitch_3
    if-ne p2, v0, :cond_1

    if-eqz p3, :cond_1

    .line 371
    invoke-static {p3}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Intent;)Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    .line 372
    if-eqz v0, :cond_0

    .line 373
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 374
    invoke-direct {p0}, Lcom/twitter/android/BaseEditProfileActivity;->x()V

    goto :goto_0

    .line 377
    :cond_1
    iput-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    .line 378
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->c()V

    goto :goto_0

    .line 383
    :pswitch_4
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/BaseEditProfileActivity;->m:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    invoke-static {p0, v3, v2, v2}, Lcom/twitter/android/media/camera/CameraActivity;->a(Landroid/content/Context;IZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 390
    :pswitch_5
    if-ne p2, v0, :cond_2

    if-eqz p3, :cond_2

    .line 391
    const-string/jumbo v0, "extra_editable_image"

    .line 392
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 393
    if-eqz v0, :cond_0

    .line 394
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    .line 395
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v2, v0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/UserImageView;->setCropRectangle(Lcom/twitter/util/math/c;)V

    .line 396
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableImage;->d()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    goto :goto_0

    .line 399
    :cond_2
    iput-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    goto :goto_0

    .line 343
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 624
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 625
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->x_()V

    .line 629
    :goto_0
    return-void

    .line 627
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClickHandler(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 243
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->f_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 245
    const v1, 0x7f13035d

    if-eq v0, v1, :cond_0

    const v1, 0x7f130167

    if-ne v0, v1, :cond_2

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, ""

    const-string/jumbo v5, "header_image"

    const-string/jumbo v6, "click"

    .line 247
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 246
    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->e()V

    .line 257
    :cond_1
    :goto_0
    return-void

    .line 249
    :cond_2
    const v1, 0x7f130165

    if-eq v0, v1, :cond_3

    const v1, 0x7f1306a4

    if-ne v0, v1, :cond_1

    .line 250
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, ""

    const-string/jumbo v5, "avatar"

    const-string/jumbo v6, "click"

    .line 251
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 250
    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(J[Ljava/lang/String;)V

    .line 253
    invoke-static {p0, v7}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    .line 252
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 314
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onRestart()V

    .line 315
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/HeaderImageView;->e()V

    .line 318
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 232
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 233
    const-string/jumbo v0, "pending_avatar_media"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 234
    const-string/jumbo v0, "pending_header_media"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 235
    const-string/jumbo v0, "initial_header"

    iget-boolean v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 236
    const-string/jumbo v0, "remove_header"

    iget-boolean v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 237
    const-string/jumbo v0, "remove_header_enabled"

    iget-boolean v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 238
    const-string/jumbo v0, "has_updated_header"

    iget-boolean v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 239
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/HeaderImageView;->f()V

    .line 325
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 326
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 638
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->x_()V

    .line 644
    :goto_0
    return-void

    .line 641
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->setResult(I)V

    .line 642
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->p()V

    goto :goto_0
.end method

.method protected q()Z
    .locals 1

    .prologue
    .line 650
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected r()Z
    .locals 2

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->n()Ljava/lang/String;

    move-result-object v0

    .line 655
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->l:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->l:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->l:Ljava/lang/String;

    .line 656
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 655
    :goto_0
    return v0

    .line 656
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract s()Z
.end method

.method protected abstract t()Z
.end method

.method u()V
    .locals 2

    .prologue
    .line 712
    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    .line 714
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->c()V

    .line 717
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-nez v0, :cond_2

    .line 718
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 720
    :cond_2
    return-void
.end method

.method v()V
    .locals 2

    .prologue
    .line 751
    const v0, 0x7f0a06cb

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 752
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 753
    return-void
.end method

.method w()V
    .locals 1

    .prologue
    .line 764
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->e()V

    .line 766
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 768
    :cond_0
    return-void
.end method

.method protected x_()V
    .locals 2

    .prologue
    .line 276
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a034a

    .line 277
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a002b

    .line 278
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0278

    .line 279
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 280
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 281
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 282
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 283
    return-void
.end method

.method y_()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 479
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 480
    const v1, 0x7f0a06f1

    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(I)V

    .line 481
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    .line 484
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    if-nez v1, :cond_1

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->t:Lcom/twitter/media/model/MediaFile;

    .line 485
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Lcom/twitter/android/profiles/g;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {v0, p0, v1, v2, p0}, Lcom/twitter/android/profiles/g;->a(Landroid/app/Activity;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/profiles/g$c;)V

    .line 486
    return-void

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    goto :goto_0
.end method

.method protected abstract z_()Ljava/lang/String;
.end method
