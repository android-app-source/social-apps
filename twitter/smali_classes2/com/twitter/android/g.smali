.class Lcom/twitter/android/g;
.super Lcom/twitter/util/concurrent/b;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/ab;

.field private final b:Lcom/twitter/library/widget/InlineDismissView;

.field private final c:Lcom/twitter/android/timeline/bk;

.field private final d:Lbwo;


# direct methods
.method constructor <init>(Lcom/twitter/android/ab;Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/util/concurrent/b;-><init>()V

    .line 23
    new-instance v0, Lbwo;

    new-instance v1, Lbwn;

    invoke-direct {v1}, Lbwn;-><init>()V

    invoke-direct {v0, v1}, Lbwo;-><init>(Lbwn;)V

    iput-object v0, p0, Lcom/twitter/android/g;->d:Lbwo;

    .line 28
    iput-object p1, p0, Lcom/twitter/android/g;->a:Lcom/twitter/android/ab;

    .line 29
    iput-object p2, p0, Lcom/twitter/android/g;->b:Lcom/twitter/library/widget/InlineDismissView;

    .line 30
    iput-object p3, p0, Lcom/twitter/android/g;->c:Lcom/twitter/android/timeline/bk;

    .line 31
    return-void
.end method


# virtual methods
.method protected a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 43
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 44
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/g;->d:Lbwo;

    invoke-virtual {v0, p1}, Lbwo;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/android/g;->d:Lbwo;

    invoke-virtual {v0, p1}, Lbwo;->b(Landroid/database/Cursor;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/twitter/android/g;->c:Lcom/twitter/android/timeline/bk;

    iget-wide v2, v1, Lcom/twitter/android/timeline/bk;->d:J

    .line 47
    iget-object v1, p0, Lcom/twitter/android/g;->a:Lcom/twitter/android/ab;

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/ab;->a(JLcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/k;

    .line 48
    iget-object v1, p0, Lcom/twitter/android/g;->a:Lcom/twitter/android/ab;

    invoke-interface {v1, v2, v3}, Lcom/twitter/android/ab;->a(J)Z

    .line 49
    iget-object v1, p0, Lcom/twitter/android/g;->b:Lcom/twitter/library/widget/InlineDismissView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/InlineDismissView;->setDismissInfo(Lcom/twitter/model/timeline/k;)V

    .line 51
    :cond_0
    return-void
.end method
