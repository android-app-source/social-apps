.class public abstract Lcom/twitter/android/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/v$h;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    .line 23
    iput-boolean p2, p0, Lcom/twitter/android/f;->b:Z

    .line 24
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/client/Session;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 49
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 50
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v4, "signup::::success"

    aput-object v4, v1, v6

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 52
    iget-object v1, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/az;->a(Landroid/content/Context;)Lcom/twitter/android/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/az;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 53
    iget-object v1, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 54
    iget-boolean v1, p0, Lcom/twitter/android/f;->b:Z

    if-eqz v1, :cond_0

    .line 55
    const-string/jumbo v1, "sso_sdk"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 57
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 59
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const-string/jumbo v0, "signup"

    aput-object v0, v4, v6

    const-string/jumbo v0, ""

    aput-object v0, v4, v5

    const/4 v5, 0x2

    iget-boolean v0, p0, Lcom/twitter/android/f;->b:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "switch_account"

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x3

    const-string/jumbo v5, ""

    aput-object v5, v4, v0

    const/4 v0, 0x4

    const-string/jumbo v5, "success"

    aput-object v5, v4, v0

    .line 60
    invoke-virtual {v1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 62
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    const-string/jumbo v1, "signup:form:::success"

    invoke-static {v0, v2, v3, v1, v6}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;JLjava/lang/String;Z)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    const-string/jumbo v1, "signup::::success"

    invoke-static {v0, v2, v3, v1, v6}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;JLjava/lang/String;Z)V

    .line 66
    return-void

    .line 59
    :cond_1
    const-string/jumbo v0, "logged_out"

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    sget-object v1, Lcom/twitter/android/util/AppEventTrack$EventType;->d:Lcom/twitter/android/util/AppEventTrack$EventType;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Lcom/twitter/android/util/AppEventTrack$EventType;[Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/al;->a(Landroid/content/Context;)V

    .line 33
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/f;->a:Landroid/content/Context;

    .line 34
    invoke-static {v1, p1}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbbg;

    move-result-object v1

    const/4 v2, 0x0

    .line 33
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 37
    iget-boolean v0, p0, Lcom/twitter/android/f;->b:Z

    if-eqz v0, :cond_0

    .line 40
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 43
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/f;->a(Lcom/twitter/library/client/Session;)V

    .line 44
    return-void
.end method
