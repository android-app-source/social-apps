.class Lcom/twitter/android/composer/ComposerActivity$34;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ab;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/library/client/Session;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 681
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 684
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v2}, Lcom/twitter/android/composer/ComposerActivity;->j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 685
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v1}, Lcom/twitter/android/composer/ComposerActivity;->j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 700
    :goto_0
    return-void

    .line 688
    :cond_0
    if-nez p1, :cond_1

    .line 689
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    goto :goto_0

    .line 692
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v2}, Lcom/twitter/android/composer/ComposerActivity;->e(Lcom/twitter/android/composer/ComposerActivity;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v1

    .line 693
    :goto_1
    if-nez v2, :cond_3

    .line 694
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v1}, Lcom/twitter/android/composer/ComposerActivity;->j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 692
    goto :goto_1

    .line 697
    :cond_3
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v2}, Lcom/twitter/android/composer/ComposerActivity;->k(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/composer/g;->c()Z

    move-result v2

    .line 698
    invoke-static {}, Lbpo;->a()Z

    move-result v3

    .line 699
    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity$34;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v4}, Lcom/twitter/android/composer/ComposerActivity;->j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v4

    if-nez v2, :cond_4

    if-eqz v3, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    invoke-virtual {v4, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    goto :goto_0
.end method
