.class public Lcom/twitter/android/composer/v;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:J

.field private final c:J

.field private final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V
    .locals 3

    .prologue
    .line 22
    const-class v0, Lcom/twitter/android/composer/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/twitter/android/composer/v;->a:Landroid/content/Context;

    .line 24
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/composer/v;->b:J

    .line 25
    iput-wide p3, p0, Lcom/twitter/android/composer/v;->c:J

    .line 26
    iput-boolean p5, p0, Lcom/twitter/android/composer/v;->g:Z

    .line 27
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Boolean;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/twitter/android/composer/v;->b:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v0

    .line 32
    new-instance v1, Laut;

    iget-object v2, p0, Lcom/twitter/android/composer/v;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 33
    iget-wide v2, p0, Lcom/twitter/android/composer/v;->c:J

    iget-boolean v4, p0, Lcom/twitter/android/composer/v;->g:Z

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/twitter/library/provider/h;->a(JLaut;Z)Z

    move-result v0

    .line 34
    invoke-virtual {v1}, Laut;->a()V

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/android/composer/v;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/android/composer/v;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
