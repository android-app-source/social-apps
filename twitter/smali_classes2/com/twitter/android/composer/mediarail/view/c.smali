.class public Lcom/twitter/android/composer/mediarail/view/c;
.super Lcom/twitter/android/composer/mediarail/view/d;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/MediaImageView$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/composer/mediarail/view/d",
        "<",
        "Lsh;",
        ">;",
        "Lcom/twitter/media/ui/image/MediaImageView$b;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/media/ui/image/MediaImageView;

.field private final b:Lcom/twitter/android/widget/MediaBadgeOverlayView;

.field private c:Lcom/twitter/model/media/EditableMedia;


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/mediarail/view/d;-><init>(Landroid/view/View;)V

    .line 30
    const v0, 0x7f130499

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->a:Lcom/twitter/media/ui/image/MediaImageView;

    .line 31
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFadeIn(Z)V

    .line 32
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, p0}, Lcom/twitter/media/ui/image/MediaImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 33
    const v0, 0x7f13049a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaBadgeOverlayView;

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->b:Lcom/twitter/android/widget/MediaBadgeOverlayView;

    .line 34
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/twitter/android/composer/mediarail/view/c;
    .locals 3

    .prologue
    .line 43
    .line 44
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401ab

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 45
    new-instance v1, Lcom/twitter/android/composer/mediarail/view/c;

    invoke-direct {v1, v0}, Lcom/twitter/android/composer/mediarail/view/c;-><init>(Landroid/view/View;)V

    return-object v1
.end method


# virtual methods
.method public a()Lcom/twitter/model/media/EditableMedia;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->c:Lcom/twitter/model/media/EditableMedia;

    return-object v0
.end method

.method public bridge synthetic a(ILsg;)V
    .locals 0

    .prologue
    .line 19
    check-cast p2, Lsh;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/mediarail/view/c;->a(ILsh;)V

    return-void
.end method

.method public a(ILsh;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    iput-object v1, p0, Lcom/twitter/android/composer/mediarail/view/c;->c:Lcom/twitter/model/media/EditableMedia;

    .line 51
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->b:Lcom/twitter/android/widget/MediaBadgeOverlayView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a()V

    .line 52
    if-eqz p2, :cond_0

    .line 53
    invoke-virtual {p2}, Lsh;->a()Lcom/twitter/media/model/b;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/media/model/b;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/c;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/mediarail/view/c;->a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/media/MediaSource;->c:Lcom/twitter/model/media/MediaSource;

    invoke-static {v0, v1}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->c:Lcom/twitter/model/media/EditableMedia;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/c;->b:Lcom/twitter/android/widget/MediaBadgeOverlayView;

    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/c;->c:Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaBadgeOverlayView;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 71
    :cond_0
    return-void
.end method
