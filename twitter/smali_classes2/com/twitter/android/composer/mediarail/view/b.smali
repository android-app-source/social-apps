.class public Lcom/twitter/android/composer/mediarail/view/b;
.super Lcom/twitter/android/composer/mediarail/view/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/composer/mediarail/view/d",
        "<",
        "Lsf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/mediarail/view/d;-><init>(Landroid/view/View;)V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/composer/mediarail/view/b;->c:Landroid/view/View;

    .line 26
    const v0, 0x7f130497

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->a:Landroid/widget/ImageView;

    .line 27
    const v0, 0x7f130498

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->b:Landroid/widget/TextView;

    .line 28
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/twitter/android/composer/mediarail/view/b;
    .locals 3

    .prologue
    .line 32
    .line 33
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/twitter/android/composer/mediarail/view/b;

    invoke-direct {v1, v0}, Lcom/twitter/android/composer/mediarail/view/b;-><init>(Landroid/view/View;)V

    return-object v1
.end method


# virtual methods
.method public a(ILsf;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    if-eqz p2, :cond_0

    .line 40
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lsf;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->a:Landroid/widget/ImageView;

    invoke-virtual {p2}, Lsf;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 42
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->a:Landroid/widget/ImageView;

    invoke-virtual {p2}, Lsf;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->c:Landroid/view/View;

    invoke-virtual {p2}, Lsf;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 50
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 47
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    goto :goto_0
.end method

.method public bridge synthetic a(ILsg;)V
    .locals 0

    .prologue
    .line 15
    check-cast p2, Lsf;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/mediarail/view/b;->a(ILsf;)V

    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/b;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-void
.end method
