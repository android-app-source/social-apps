.class public Lcom/twitter/android/composer/mediarail/view/MediaRailView;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/mediarail/view/a$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v7/widget/RecyclerView;

.field private final b:Lcom/twitter/android/composer/mediarail/view/a;

.field private c:Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Lcom/twitter/android/composer/mediarail/view/a;

    invoke-direct {v0}, Lcom/twitter/android/composer/mediarail/view/a;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->b:Lcom/twitter/android/composer/mediarail/view/a;

    .line 36
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->b:Lcom/twitter/android/composer/mediarail/view/a;

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/mediarail/view/a;->a(Lcom/twitter/android/composer/mediarail/view/a$c;)V

    .line 37
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401ac

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 38
    const v0, 0x7f13049b

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->a:Landroid/support/v7/widget/RecyclerView;

    .line 39
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p1, v3, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 41
    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 42
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->b:Lcom/twitter/android/composer/mediarail/view/a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 43
    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/android/composer/mediarail/view/d;Lsg;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->c:Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->c:Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;

    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;->a(ILcom/twitter/android/composer/mediarail/view/d;Lsg;)V

    .line 94
    :cond_0
    return-void
.end method

.method public a(Lsg;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->b:Lcom/twitter/android/composer/mediarail/view/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/mediarail/view/a;->a(Lsg;)V

    .line 61
    return-void
.end method

.method public b(Lsg;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->b:Lcom/twitter/android/composer/mediarail/view/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/mediarail/view/a;->b(Lsg;)V

    .line 69
    return-void
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->a:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public setItems(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->b:Lcom/twitter/android/composer/mediarail/view/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/mediarail/view/a;->a(Landroid/database/Cursor;)V

    .line 57
    return-void
.end method

.method public setOnMediaRailItemClickedListener(Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->c:Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;

    .line 53
    return-void
.end method
