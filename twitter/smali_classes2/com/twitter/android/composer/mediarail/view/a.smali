.class public Lcom/twitter/android/composer/mediarail/view/a;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/mediarail/view/a$a;,
        Lcom/twitter/android/composer/mediarail/view/a$b;,
        Lcom/twitter/android/composer/mediarail/view/a$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/twitter/android/composer/mediarail/view/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsg;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsg;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcbn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbn",
            "<",
            "Lsg;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcbe;

.field private e:Lcom/twitter/android/composer/mediarail/view/a$c;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->b:Ljava/util/List;

    .line 32
    new-instance v0, Lcbn;

    new-instance v1, Lcom/twitter/android/composer/mediarail/view/a$b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/android/composer/mediarail/view/a$b;-><init>(Lcom/twitter/android/composer/mediarail/view/a$1;)V

    invoke-direct {v0, v1}, Lcbn;-><init>(Lcbp;)V

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->c:Lcbn;

    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->d:Lcbe;

    if-nez v0, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->d:Lcbe;

    invoke-virtual {v0}, Lcbe;->be_()I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/composer/mediarail/view/a;Lcom/twitter/android/composer/mediarail/view/d;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/mediarail/view/a;->a(Lcom/twitter/android/composer/mediarail/view/d;)V

    return-void
.end method

.method private a(Lcom/twitter/android/composer/mediarail/view/d;)V
    .locals 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->e:Lcom/twitter/android/composer/mediarail/view/a$c;

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {p1}, Lcom/twitter/android/composer/mediarail/view/d;->getAdapterPosition()I

    move-result v0

    .line 165
    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/composer/mediarail/view/a;->getItemCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/a;->e:Lcom/twitter/android/composer/mediarail/view/a$c;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/mediarail/view/a;->a(I)Lsg;

    move-result-object v2

    invoke-interface {v1, v0, p1, v2}, Lcom/twitter/android/composer/mediarail/view/a$c;->a(ILcom/twitter/android/composer/mediarail/view/d;Lsg;)V

    .line 169
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/twitter/android/composer/mediarail/view/d;
    .locals 2

    .prologue
    .line 61
    packed-switch p2, :pswitch_data_0

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :pswitch_0
    invoke-static {p1}, Lcom/twitter/android/composer/mediarail/view/b;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/composer/mediarail/view/b;

    move-result-object v0

    .line 75
    :goto_0
    new-instance v1, Lcom/twitter/android/composer/mediarail/view/a$a;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/composer/mediarail/view/a$a;-><init>(Lcom/twitter/android/composer/mediarail/view/a;Lcom/twitter/android/composer/mediarail/view/d;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/d;->a(Landroid/view/View$OnClickListener;)V

    .line 76
    return-object v0

    .line 67
    :pswitch_1
    invoke-static {p1}, Lcom/twitter/android/composer/mediarail/view/c;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/composer/mediarail/view/c;

    move-result-object v0

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(I)Lsg;
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsg;

    .line 159
    :goto_0
    return-object v0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/twitter/android/composer/mediarail/view/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->d:Lcbe;

    .line 152
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbe;

    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcbe;->b(I)Landroid/database/Cursor;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_2

    .line 154
    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/a;->c:Lcbn;

    invoke-virtual {v1, v0}, Lcbn;->a(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsg;

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/twitter/android/composer/mediarail/view/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-direct {p0}, Lcom/twitter/android/composer/mediarail/view/a;->a()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsg;

    goto :goto_0

    .line 159
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcbe;

    invoke-direct {v0, p1}, Lcbe;-><init>(Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->d:Lcbe;

    .line 51
    invoke-virtual {p0}, Lcom/twitter/android/composer/mediarail/view/a;->notifyDataSetChanged()V

    .line 52
    return-void
.end method

.method public a(Lcom/twitter/android/composer/mediarail/view/a$c;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/android/composer/mediarail/view/a;->e:Lcom/twitter/android/composer/mediarail/view/a$c;

    .line 56
    return-void
.end method

.method public a(Lcom/twitter/android/composer/mediarail/view/d;I)V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p2}, Lcom/twitter/android/composer/mediarail/view/a;->a(I)Lsg;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p1, p2, v0}, Lcom/twitter/android/composer/mediarail/view/d;->a(ILsg;)V

    .line 85
    :cond_0
    return-void
.end method

.method public a(Lsg;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/mediarail/view/a;->notifyItemInserted(I)V

    .line 111
    return-void
.end method

.method public b(Lsg;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/mediarail/view/a;->notifyItemInserted(I)V

    .line 130
    return-void
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/composer/mediarail/view/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/twitter/android/composer/mediarail/view/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/composer/mediarail/view/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 4

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/mediarail/view/a;->a(I)Lsg;

    move-result-object v0

    .line 91
    instance-of v1, v0, Lsf;

    if-eqz v1, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    .line 93
    :cond_0
    instance-of v1, v0, Lsh;

    if-eqz v1, :cond_1

    .line 94
    const/4 v0, 0x1

    goto :goto_0

    .line 95
    :cond_1
    if-nez v0, :cond_2

    .line 96
    const/4 v0, -0x1

    goto :goto_0

    .line 98
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unsupported item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 99
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " added to MediaRailAdapter"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/twitter/android/composer/mediarail/view/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/mediarail/view/a;->a(Lcom/twitter/android/composer/mediarail/view/d;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/mediarail/view/a;->a(Landroid/view/ViewGroup;I)Lcom/twitter/android/composer/mediarail/view/d;

    move-result-object v0

    return-object v0
.end method
