.class public Lcom/twitter/android/composer/ComposerActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lbrp;
.implements Lcom/twitter/android/composer/e;
.implements Lcom/twitter/android/composer/h;
.implements Lcom/twitter/android/composer/k;
.implements Lcom/twitter/android/composer/m;
.implements Lcom/twitter/android/dogfood/a$b;
.implements Lcom/twitter/android/widget/DraggableDrawerLayout$b;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/ComposerActivity$c;,
        Lcom/twitter/android/composer/ComposerActivity$d;,
        Lcom/twitter/android/composer/ComposerActivity$a;,
        Lcom/twitter/android/composer/ComposerActivity$b;,
        Lcom/twitter/android/composer/ComposerActivity$e;
    }
.end annotation


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/view/View;

.field private C:Landroid/widget/ImageView;

.field private D:Lcom/twitter/ui/widget/TwitterButton;

.field private E:Lcom/twitter/ui/widget/TwitterButton;

.field private K:Lcom/twitter/ui/widget/TwitterButton;

.field private L:Lcom/twitter/library/view/QuoteView;

.field private M:Lsc;

.field private final N:Lse$a;

.field private O:Lcom/twitter/android/media/selection/c;

.field private P:Lcom/twitter/library/client/Session;

.field private Q:I

.field private R:I

.field private S:I

.field private T:J

.field private U:Lcom/twitter/library/client/p;

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field final a:Lcom/twitter/android/composer/f;

.field private aA:Z

.field private aB:Lsa;

.field private aC:Lrx/j;

.field private aD:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

.field private aE:Lakv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakv",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Z

.field private ab:Z

.field private ac:I

.field private ad:Z

.field private ae:Lcom/twitter/app/common/util/b$a;

.field private af:Z

.field private ag:Lcom/twitter/android/b;

.field private ah:Z

.field private ai:I

.field private aj:Lcom/twitter/android/composer/r;

.field private ak:I

.field private al:I

.field private am:Landroid/view/animation/Animation;

.field private an:Landroid/view/animation/Animation;

.field private ao:Landroid/animation/Animator;

.field private ap:Z

.field private aq:Z

.field private ar:Landroid/view/View;

.field private as:Lcom/twitter/android/composer/x;

.field private at:Z

.field private au:Z

.field private av:J

.field private aw:Lcom/twitter/android/card/j;

.field private ax:Lcax;

.field private ay:Z

.field private az:Lcom/twitter/android/card/pollcompose/d;

.field b:Lcom/twitter/android/composer/s;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field c:Landroid/widget/ImageButton;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final d:Lcom/twitter/android/composer/ComposerActivity$e;

.field private final e:Landroid/os/Handler;

.field private final f:Lcom/twitter/android/composer/o;

.field private final g:Lcom/twitter/android/composer/g;

.field private h:Lcom/twitter/android/composer/l;

.field private i:Lcom/twitter/android/composer/t;

.field private j:Lcom/twitter/android/composer/j;

.field private k:Lcom/twitter/android/composer/TweetBox;

.field private l:Lcom/twitter/android/composer/ComposerScrollView;

.field private m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

.field private n:Lcom/twitter/android/widget/FoundMediaAttributionView;

.field private o:Lcom/twitter/media/ui/image/UserImageView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/ImageButton;

.field private s:Lcom/twitter/android/widget/ToggleImageButton;

.field private t:Landroid/widget/ImageButton;

.field private u:Landroid/widget/ImageButton;

.field private v:Lcom/twitter/android/composer/ComposerCountView;

.field private w:Lcom/twitter/android/widget/DraggableDrawerLayout;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 217
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 294
    new-instance v0, Lcom/twitter/android/composer/f;

    invoke-direct {v0}, Lcom/twitter/android/composer/f;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 299
    new-instance v0, Lcom/twitter/android/composer/ComposerActivity$e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/composer/ComposerActivity$e;-><init>(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/android/composer/ComposerActivity$1;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->d:Lcom/twitter/android/composer/ComposerActivity$e;

    .line 301
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->e:Landroid/os/Handler;

    .line 302
    new-instance v0, Lcom/twitter/android/composer/o;

    invoke-direct {v0}, Lcom/twitter/android/composer/o;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    .line 303
    new-instance v0, Lcom/twitter/android/composer/g;

    invoke-direct {v0}, Lcom/twitter/android/composer/g;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    .line 342
    new-instance v0, Lcom/twitter/android/composer/ComposerActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/composer/ComposerActivity$1;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->N:Lse$a;

    .line 385
    iput v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    .line 387
    iput v2, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    .line 391
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/composer/ComposerActivity;->T:J

    .line 409
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ai:I

    return-void
.end method

.method static synthetic A(Lcom/twitter/android/composer/ComposerActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->B:Landroid/view/View;

    return-object v0
.end method

.method private B()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 925
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 926
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    invoke-virtual {v0}, Lcom/twitter/android/card/pollcompose/d;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 927
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 928
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->z:Landroid/widget/Button;

    const v4, 0x7f0a06b9

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    .line 934
    :goto_1
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->z:Landroid/widget/Button;

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v4}, Lcom/twitter/android/composer/TweetBox;->o()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    .line 935
    invoke-virtual {v4}, Lcom/twitter/android/media/selection/c;->c()Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    .line 934
    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 937
    return-void

    :cond_1
    move v0, v1

    .line 926
    goto :goto_0

    .line 929
    :cond_2
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v3}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v3}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 930
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->z:Landroid/widget/Button;

    const v4, 0x7f0a099c

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    .line 932
    :cond_3
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->z:Landroid/widget/Button;

    const v4, 0x7f0a06c0

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    :cond_4
    move v2, v1

    .line 935
    goto :goto_2
.end method

.method private static B(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 4

    .prologue
    .line 2546
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->e:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$30;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$30;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2571
    return-void
.end method

.method private C()V
    .locals 5

    .prologue
    .line 940
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 941
    new-instance v2, Lcom/twitter/util/concurrent/c;

    invoke-direct {v2}, Lcom/twitter/util/concurrent/c;-><init>()V

    .line 942
    invoke-static {}, Lcom/twitter/async/service/c;->a()Lcom/twitter/async/service/c;

    move-result-object v3

    sget-object v4, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->g:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v3, v4}, Lcom/twitter/async/service/c;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/util/concurrent/c;->a(Ljava/util/concurrent/Executor;)Lcom/twitter/util/concurrent/c;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/composer/ComposerActivity$7;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity$7;-><init>(Lcom/twitter/android/composer/ComposerActivity;J)V

    .line 943
    invoke-virtual {v2, v3}, Lcom/twitter/util/concurrent/c;->a(Ljava/util/concurrent/Callable;)Lcom/twitter/util/concurrent/c;

    move-result-object v2

    .line 951
    invoke-virtual {v2}, Lcom/twitter/util/concurrent/c;->a()Lcom/twitter/util/concurrent/g;

    move-result-object v2

    .line 952
    new-instance v3, Lcom/twitter/util/concurrent/e;

    invoke-direct {v3}, Lcom/twitter/util/concurrent/e;-><init>()V

    sget-object v4, Lcom/twitter/util/concurrent/f;->b:Ljava/util/concurrent/Executor;

    .line 953
    invoke-virtual {v3, v4}, Lcom/twitter/util/concurrent/e;->a(Ljava/util/concurrent/Executor;)Lcom/twitter/util/concurrent/e;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/composer/ComposerActivity$8;

    invoke-direct {v4, p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity$8;-><init>(Lcom/twitter/android/composer/ComposerActivity;J)V

    .line 954
    invoke-virtual {v3, v4}, Lcom/twitter/util/concurrent/e;->a(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/e;

    move-result-object v0

    .line 952
    invoke-interface {v2, v0}, Lcom/twitter/util/concurrent/g;->b(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 963
    invoke-virtual {p0, v2}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/util/concurrent/g;)V

    .line 964
    return-void
.end method

.method private D()V
    .locals 6

    .prologue
    .line 1328
    const v0, 0x7f1301d4

    .line 1329
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/CardPreviewView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/card/CardPreviewView;

    .line 1330
    invoke-static {}, Lcom/twitter/android/card/k;->a()Lcom/twitter/android/card/k;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->ax:Lcax;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const/4 v5, 0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/card/k;->a(Landroid/app/Activity;Lcom/twitter/android/card/i;Lcax;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Z)Lcom/twitter/android/card/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    .line 1332
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$21;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$21;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/card/j;->a(Lcom/twitter/android/card/j$a;)V

    .line 1353
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    invoke-virtual {v2, v0}, Lcom/twitter/android/card/CardPreviewView;->setController(Lcom/twitter/android/card/j;)V

    .line 1354
    const v0, 0x7f130294

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const v1, 0x7f130293

    .line 1355
    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1354
    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/card/CardPreviewView;->a(Landroid/widget/ProgressBar;Ljava/util/List;)V

    .line 1356
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;Ljava/lang/Runnable;)Lcom/twitter/android/composer/ComposerActivity$a;
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->a(Ljava/lang/Runnable;)Lcom/twitter/android/composer/ComposerActivity$a;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Runnable;)Lcom/twitter/android/composer/ComposerActivity$a;
    .locals 1

    .prologue
    .line 3179
    new-instance v0, Lcom/twitter/android/composer/ComposerActivity$33;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/composer/ComposerActivity$33;-><init>(Lcom/twitter/android/composer/ComposerActivity;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/app/common/util/b$a;)Lcom/twitter/app/common/util/b$a;
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity;->ae:Lcom/twitter/app/common/util/b$a;

    return-object p1
.end method

.method private a(Landroid/net/Uri;ZLcom/twitter/android/media/selection/a;)V
    .locals 2

    .prologue
    .line 2989
    const/16 v0, 0x207

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2990
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->setVisibility(I)V

    .line 2991
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;ZLcom/twitter/android/media/selection/a;)V

    .line 2992
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->an()V

    .line 2994
    :cond_0
    return-void
.end method

.method private a(Landroid/net/Uri;[IZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1240
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/String;[I)V

    .line 1242
    new-instance v0, Lcom/twitter/android/composer/ComposerActivity$b;

    invoke-direct {v0, p0, p3}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    .line 1243
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1244
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v1, v3}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->setVisibility(I)V

    .line 1245
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/media/selection/c;->a(Ljava/util/List;Lcom/twitter/android/media/selection/a;)V

    .line 1247
    :cond_0
    if-eqz p1, :cond_1

    .line 1248
    invoke-static {}, Lcom/twitter/media/util/t;->a()Z

    move-result v1

    invoke-direct {p0, p1, v1, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/net/Uri;ZLcom/twitter/android/media/selection/a;)V

    .line 1251
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v0

    .line 1252
    if-eqz v0, :cond_2

    .line 1253
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->L:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/view/QuoteView;->setQuoteData(Lcom/twitter/model/core/r;)V

    .line 1254
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->L:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v1, v3}, Lcom/twitter/library/view/QuoteView;->setVisibility(I)V

    .line 1255
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/TweetBox;->setQuote(Lcom/twitter/model/core/r;)V

    .line 1256
    invoke-direct {p0, v3}, Lcom/twitter/android/composer/ComposerActivity;->f(Z)V

    .line 1258
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020113

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1259
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->as()V

    .line 1260
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->at()V

    .line 1263
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1264
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/composer/f;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$19;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$19;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1265
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 1277
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lrx/j;)V

    .line 1280
    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->av()V

    .line 1281
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ad()V

    .line 1282
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->o()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aA:Z

    if-eqz v0, :cond_8

    .line 1283
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    if-eqz v0, :cond_7

    .line 1284
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->n()Lcau;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/pollcompose/d;->a(Lcau;)V

    .line 1290
    :goto_0
    iput-boolean v3, p0, Lcom/twitter/android/composer/ComposerActivity;->aA:Z

    .line 1296
    :cond_5
    :goto_1
    invoke-static {}, Lcom/twitter/android/util/j;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    if-ne p4, v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v1, "found_media_umf_tooltip"

    .line 1298
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1299
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v1, "found_media_umf_tooltip"

    .line 1300
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 1299
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;Landroid/support/v4/app/FragmentManager;)V

    .line 1302
    :cond_6
    return-void

    .line 1287
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcau;)V

    goto :goto_0

    .line 1292
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    if-eqz v0, :cond_5

    .line 1293
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    invoke-virtual {v0}, Lcom/twitter/android/card/pollcompose/d;->a()V

    goto :goto_1
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 785
    const v0, 0x7f130296

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 786
    const v1, 0x7f13029b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->A:Landroid/view/View;

    .line 787
    const v0, 0x7f13028c

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->B:Landroid/view/View;

    .line 788
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->B:Landroid/view/View;

    const v1, 0x7f13028f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 789
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 790
    const v2, 0x7f020113

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 792
    const v2, 0x7f050020

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->am:Landroid/view/animation/Animation;

    .line 794
    const v2, 0x7f050022

    .line 795
    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 796
    const v4, 0x7f050021

    .line 797
    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 798
    new-instance v5, Lcom/twitter/android/composer/ComposerActivity$39;

    invoke-direct {v5, p0, v0, v1, v4}, Lcom/twitter/android/composer/ComposerActivity$39;-><init>(Lcom/twitter/android/composer/ComposerActivity;Landroid/widget/ImageButton;Landroid/content/res/Resources;Landroid/view/animation/Animation;)V

    .line 799
    invoke-virtual {v2, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 806
    iput-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->an:Landroid/view/animation/Animation;

    .line 808
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$40;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$40;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 815
    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    .line 817
    const v0, 0x7f13029a

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->o:Lcom/twitter/media/ui/image/UserImageView;

    .line 818
    const v0, 0x7f130693

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->p:Landroid/widget/TextView;

    .line 821
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->o:Lcom/twitter/media/ui/image/UserImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/twitter/media/ui/image/UserImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 823
    const v0, 0x7f130194

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 824
    if-eqz v0, :cond_4

    .line 825
    :goto_0
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$2;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 832
    const v0, 0x7f130297

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 833
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$3;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 840
    const v0, 0x7f13028e

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ToggleImageButton;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->s:Lcom/twitter/android/widget/ToggleImageButton;

    .line 842
    const v0, 0x7f130293

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerCountView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->v:Lcom/twitter/android/composer/ComposerCountView;

    .line 843
    const v0, 0x7f130295

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->z:Landroid/widget/Button;

    .line 844
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->z:Landroid/widget/Button;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$4;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 851
    const v0, 0x7f13028d

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    .line 852
    new-instance v0, Lsc;

    .line 853
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    iget-object v5, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-direct/range {v0 .. v5}, Lsc;-><init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Landroid/support/v4/app/LoaderManager;ILcom/twitter/android/composer/o;Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    .line 855
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->N:Lse$a;

    invoke-virtual {v0, v1}, Lsc;->a(Lse$a;)V

    .line 856
    if-eqz p1, :cond_0

    .line 857
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    const-string/jumbo v1, "media_rail"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsc;->a(Landroid/os/Bundle;)V

    .line 860
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->af()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 861
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v0}, Lsc;->i()V

    .line 866
    :goto_1
    const v0, 0x7f130298

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    .line 867
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 868
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$5;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 880
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->C()V

    .line 883
    :cond_1
    const v0, 0x7f130299

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->r:Landroid/widget/ImageButton;

    .line 885
    invoke-static {}, Lcom/twitter/android/util/j;->a()Z

    move-result v1

    .line 886
    if-eqz v1, :cond_2

    .line 887
    const v0, 0x7f130291

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 888
    const v0, 0x7f130290

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->t:Landroid/widget/ImageButton;

    .line 889
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->t:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 890
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->t:Landroid/widget/ImageButton;

    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$6;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/ComposerActivity$6;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 900
    :cond_2
    invoke-static {p0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 901
    invoke-virtual {v0}, Lcom/twitter/android/composer/a;->c()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 903
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ag()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 904
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ah()V

    .line 911
    :cond_3
    :goto_2
    return-void

    .line 824
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->o:Lcom/twitter/media/ui/image/UserImageView;

    goto/16 :goto_0

    .line 863
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v0}, Lsc;->k()V

    goto :goto_1

    .line 905
    :cond_6
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v1, "found_media_tooltip"

    .line 906
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 907
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v1, "found_media_tooltip"

    .line 908
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;Landroid/support/v4/app/FragmentManager;)V

    goto :goto_2
.end method

.method private a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 1305
    .line 1307
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 1305
    invoke-static {p0, v0, p2, p1}, Lcom/twitter/android/composer/r;->a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/library/client/Session;Landroid/os/Bundle;)Lcom/twitter/android/composer/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    .line 1310
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$20;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$20;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/r;->a(Lcom/twitter/ui/widget/Tooltip$c;)V

    .line 1325
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1554
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1555
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0514

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 1556
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ae()V

    .line 1560
    :cond_0
    :goto_0
    return-void

    .line 1557
    :cond_1
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1558
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 3041
    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 3042
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 3043
    return-void

    .line 3042
    :cond_0
    const v0, 0x3e99999a    # 0.3f

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/model/av/h;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/model/av/h;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 657
    new-instance v0, Lcom/twitter/android/media/selection/c;

    const-string/jumbo v3, "composition"

    sget-object v4, Lcom/twitter/media/model/MediaType;->h:Ljava/util/EnumSet;

    const/4 v5, 0x4

    sget-object v6, Lcom/twitter/android/composer/ComposerType;->a:Lcom/twitter/android/composer/ComposerType;

    move-object v1, p0

    move-object v2, p0

    move-object v7, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/j;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    .line 667
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 669
    new-instance v0, Lcom/twitter/android/composer/l;

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v9, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    iget-object v10, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    move-object v8, p0

    move-object v11, p2

    invoke-direct/range {v0 .. v11}, Lcom/twitter/android/composer/l;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/composer/f;Lcom/twitter/android/composer/e;Lcom/twitter/android/composer/m;Lcom/twitter/android/composer/h;Lcom/twitter/android/composer/k;Lcom/twitter/android/composer/g;Lcom/twitter/android/composer/o;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    .line 681
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$34;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$34;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/l;->a(Lcom/twitter/android/widget/ab;)V

    .line 702
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$36;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$36;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/l;->a(Lcom/twitter/android/composer/l$a;)V

    .line 735
    const v0, 0x7f130278

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/TweetBox;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/TweetBox;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    .line 736
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    const/16 v1, 0x8c

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setMaxChars(I)V

    .line 737
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$37;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$37;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setTextWatcher(Landroid/text/TextWatcher;)V

    .line 745
    new-instance v1, Lcom/twitter/android/composer/t;

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v5, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    iget-object v6, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/composer/t;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/composer/f;Lcom/twitter/android/composer/TweetBox;Lcom/twitter/android/composer/o;)V

    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    .line 751
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$38;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$38;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/t;->a(Lcom/twitter/android/composer/t$a;)V

    .line 782
    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Landroid/view/View;)V
    .locals 6

    .prologue
    const v5, 0x7f130282

    const/4 v4, 0x0

    .line 967
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 968
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$9;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/composer/ComposerActivity$9;-><init>(Lcom/twitter/android/composer/ComposerActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 977
    const v0, 0x7f130271

    .line 978
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 979
    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 980
    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Z)V

    .line 981
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDrawerLayoutListener(Lcom/twitter/android/widget/DraggableDrawerLayout$b;)V

    .line 983
    new-instance v1, Lcom/twitter/android/b;

    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$10;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/ComposerActivity$10;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-direct {v1, p0, v0, v2}, Lcom/twitter/android/b;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/twitter/android/b$a;)V

    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ag:Lcom/twitter/android/b;

    .line 992
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/TweetBox;->setSession(Lcom/twitter/library/client/Session;)V

    .line 993
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$11;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$11;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setTweetBoxListener(Lcom/twitter/android/composer/TweetBox$b;)V

    .line 1033
    const v0, 0x7f1300b9

    .line 1034
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerScrollView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerScrollView;

    .line 1035
    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->l:Lcom/twitter/android/composer/ComposerScrollView;

    .line 1036
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$13;

    invoke-direct {v1, p0, p2, v0}, Lcom/twitter/android/composer/ComposerActivity$13;-><init>(Lcom/twitter/android/composer/ComposerActivity;Landroid/view/View;Lcom/twitter/android/composer/ComposerScrollView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerScrollView;->setObservableScrollViewListener(Lcom/twitter/library/widget/ObservableScrollView$a;)V

    .line 1069
    invoke-virtual {v0, v4}, Lcom/twitter/android/composer/ComposerScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1070
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$14;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$14;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1078
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$15;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$15;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1085
    const v0, 0x7f13027c

    .line 1086
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/MediaAttachmentsView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    .line 1087
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->l:Lcom/twitter/android/composer/ComposerScrollView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->setVisibleAreaContainer(Landroid/view/ViewGroup;)V

    .line 1088
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$16;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$16;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->setActionListener(Lcom/twitter/android/media/widget/MediaAttachmentsView$b;)V

    .line 1156
    const v0, 0x7f13027d

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/FoundMediaAttributionView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->n:Lcom/twitter/android/widget/FoundMediaAttributionView;

    .line 1158
    const v0, 0x7f13027e

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->D:Lcom/twitter/ui/widget/TwitterButton;

    .line 1159
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->D:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$17;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$17;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1166
    const v0, 0x7f13027f

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    .line 1167
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ak:I

    .line 1168
    const v0, 0x7f130280

    .line 1169
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Lcom/twitter/ui/widget/TwitterButton;

    .line 1170
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->al:I

    .line 1172
    invoke-virtual {p0, v5}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->x:Landroid/view/View;

    .line 1173
    const v0, 0x7f130283

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->C:Landroid/widget/ImageView;

    .line 1174
    const v0, 0x7f130284

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->y:Landroid/widget/TextView;

    .line 1176
    const v0, 0x7f13027b

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/QuoteView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/QuoteView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:Lcom/twitter/library/view/QuoteView;

    .line 1177
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:Lcom/twitter/library/view/QuoteView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setDisplaySensitiveMedia(Z)V

    .line 1178
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:Lcom/twitter/library/view/QuoteView;

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setRenderRtl(Z)V

    .line 1180
    const v0, 0x7f130276

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ar:Landroid/view/View;

    .line 1181
    new-instance v2, Lcom/twitter/android/composer/x;

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ar:Landroid/view/View;

    const v1, 0x7f130277

    .line 1182
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f130275

    .line 1183
    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/twitter/android/composer/x;-><init>(Landroid/content/res/Resources;ZLandroid/widget/TextView;Landroid/widget/TextView;)V

    iput-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->as:Lcom/twitter/android/composer/x;

    .line 1186
    invoke-virtual {p0, v5}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    .line 1185
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1c2

    .line 1187
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1188
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1189
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$18;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$18;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1196
    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ao:Landroid/animation/Animator;

    .line 1197
    return-void

    .line 1186
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(Lcom/twitter/model/av/h;)V
    .locals 3

    .prologue
    .line 2609
    if-eqz p1, :cond_1

    .line 2610
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 2611
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->D:Lcom/twitter/ui/widget/TwitterButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 2612
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->D:Lcom/twitter/ui/widget/TwitterButton;

    .line 2613
    invoke-virtual {p1}, Lcom/twitter/model/av/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0515

    .line 2612
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2619
    :goto_1
    return-void

    .line 2613
    :cond_0
    const v0, 0x7f0a0516

    goto :goto_0

    .line 2617
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->D:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_1
.end method

.method private a(Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V
    .locals 2

    .prologue
    .line 2977
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->setVisibility(I)V

    .line 2978
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 2979
    if-nez v0, :cond_0

    .line 2980
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    new-instance v1, Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-direct {v1, p1}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 2985
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->an()V

    .line 2986
    return-void

    .line 2982
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v1, p0, p1}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 2983
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v1, v0, p2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;Z)Z
    .locals 0

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    return p1
.end method

.method private static a(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2223
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2224
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v2

    .line 2225
    iget v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->d:I

    if-nez v0, :cond_0

    instance-of v0, v2, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 2227
    const/4 v0, 0x1

    .line 2230
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ab()V
    .locals 3

    .prologue
    .line 1359
    const-string/jumbo v0, "cards_polling_card_poll2choice_text_only_compose"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1360
    const v0, 0x7f130281

    .line 1361
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/pollcompose/PollComposeView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/pollcompose/d$b;

    .line 1362
    const v1, 0x7f130292

    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->u:Landroid/widget/ImageButton;

    .line 1363
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->u:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1364
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->u:Landroid/widget/ImageButton;

    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$22;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/ComposerActivity$22;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1373
    new-instance v1, Lcom/twitter/android/card/pollcompose/d;

    new-instance v2, Lcom/twitter/android/card/pollcompose/a$a;

    invoke-direct {v2}, Lcom/twitter/android/card/pollcompose/a$a;-><init>()V

    .line 1374
    invoke-virtual {v2, p0}, Lcom/twitter/android/card/pollcompose/a$a;->a(Landroid/app/Activity;)Lcom/twitter/android/card/pollcompose/a;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/card/pollcompose/d;-><init>(Lcom/twitter/android/card/pollcompose/d$b;Lcom/twitter/android/card/pollcompose/a;)V

    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    .line 1375
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$24;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$24;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/pollcompose/d;->a(Lcom/twitter/android/card/pollcompose/d$a;)V

    .line 1418
    :cond_0
    return-void
.end method

.method private ac()V
    .locals 0

    .prologue
    .line 1515
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->as()V

    .line 1516
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->at()V

    .line 1517
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->z()V

    .line 1518
    return-void
.end method

.method private ad()V
    .locals 3

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 1522
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 1523
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 1524
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1525
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/card/j;->a(Ljava/lang/String;Z)V

    .line 1526
    return-void

    .line 1524
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ae()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1563
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Y:Z

    if-eqz v0, :cond_0

    .line 1564
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Z)V

    .line 1565
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    .line 1566
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Y:Z

    .line 1570
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    const v1, 0x7f130279

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1571
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1573
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ao()V

    .line 1574
    return-void
.end method

.method private af()Z
    .locals 1

    .prologue
    .line 1577
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->n()Lcau;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 1578
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1577
    :goto_0
    return v0

    .line 1578
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ag()Z
    .locals 2

    .prologue
    .line 1582
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v1, "go_live_rail_tooltip"

    .line 1583
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v1, "go_live_gallery_tooltip"

    .line 1584
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1582
    :goto_0
    return v0

    .line 1584
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ah()V
    .locals 3

    .prologue
    .line 1588
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 1589
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v1}, Lsc;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v2, "go_live_rail_tooltip"

    .line 1590
    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1591
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v2, "go_live_rail_tooltip"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;Landroid/support/v4/app/FragmentManager;)V

    .line 1595
    :cond_0
    :goto_0
    return-void

    .line 1592
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v2, "go_live_gallery_tooltip"

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1593
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v2, "go_live_gallery_tooltip"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method

.method private ai()Z
    .locals 4

    .prologue
    .line 1684
    iget-wide v0, p0, Lcom/twitter/android/composer/ComposerActivity;->av:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1685
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/composer/ComposerActivity;->av:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 1684
    :goto_0
    return v0

    .line 1685
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aj()Z
    .locals 1

    .prologue
    .line 2366
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->n()Z

    move-result v0

    return v0
.end method

.method private ak()V
    .locals 3

    .prologue
    .line 2579
    .line 2580
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->am()Lcom/twitter/android/composer/b;

    move-result-object v0

    .line 2581
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2582
    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/b;->a(Lcom/twitter/library/client/Session;Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    .line 2584
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->aC:Lrx/j;

    if-eqz v2, :cond_0

    .line 2585
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->aC:Lrx/j;

    invoke-interface {v2}, Lrx/j;->B_()V

    .line 2588
    :cond_0
    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$31;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/composer/ComposerActivity$31;-><init>(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/android/composer/b;)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aC:Lrx/j;

    .line 2605
    return-void
.end method

.method private al()V
    .locals 3

    .prologue
    .line 2622
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aD:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    if-nez v0, :cond_1

    .line 2636
    :cond_0
    :goto_0
    return-void

    .line 2627
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->am()Lcom/twitter/android/composer/b;

    move-result-object v0

    .line 2629
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2630
    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/b;->a(Ljava/util/List;)Lcom/twitter/model/av/h;

    move-result-object v0

    .line 2632
    if-eqz v0, :cond_0

    .line 2633
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aD:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    new-instance v2, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$a;

    invoke-direct {v2, v0}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$a;-><init>(Lcom/twitter/model/av/h;)V

    .line 2634
    invoke-virtual {v1, v2}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;->c(Lako;)V

    goto :goto_0
.end method

.method private am()Lcom/twitter/android/composer/b;
    .locals 1

    .prologue
    .line 2640
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/d;

    .line 2641
    invoke-interface {v0}, Lcom/twitter/android/composer/d;->a()Lcom/twitter/android/composer/b;

    move-result-object v0

    return-object v0
.end method

.method private an()V
    .locals 1

    .prologue
    .line 2658
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->c()Z

    move-result v0

    .line 2659
    if-eqz v0, :cond_1

    .line 2660
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    .line 2664
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->B()V

    .line 2665
    return-void

    .line 2661
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getMediaCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2662
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c()V

    goto :goto_0
.end method

.method private ao()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2924
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 2925
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->B:Landroid/view/View;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2926
    return-void

    :cond_0
    move v0, v1

    .line 2924
    goto :goto_0

    .line 2925
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private ap()V
    .locals 8

    .prologue
    .line 2947
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->i()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    .line 2948
    if-eqz v1, :cond_0

    .line 2949
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->as:Lcom/twitter/android/composer/x;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    .line 2951
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2952
    invoke-virtual {v4}, Lcom/twitter/android/composer/f;->a()Z

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2953
    invoke-virtual {v5}, Lcom/twitter/android/composer/f;->h()Ljava/util/List;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/composer/ComposerActivity$d;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/twitter/android/composer/ComposerActivity$d;-><init>(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/android/composer/ComposerActivity$1;)V

    .line 2949
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/composer/x;->a(Lcom/twitter/model/core/Tweet;JZLjava/util/List;Lbxk$a;)V

    .line 2956
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aq()V

    .line 2957
    return-void
.end method

.method private aq()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2960
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 2961
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v1

    .line 2962
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ar:Landroid/view/View;

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2963
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->l:Lcom/twitter/android/composer/ComposerScrollView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerScrollView;->setHeaderVisible(Z)V

    .line 2968
    :goto_1
    return-void

    .line 2962
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 2965
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ar:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2966
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->l:Lcom/twitter/android/composer/ComposerScrollView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/ComposerScrollView;->setHeaderVisible(Z)V

    goto :goto_1
.end method

.method private ar()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2997
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ad:Z

    if-nez v0, :cond_1

    .line 2998
    iput-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ad:Z

    .line 2999
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->af:Z

    if-eqz v0, :cond_0

    .line 3000
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->d()Lcom/twitter/android/media/selection/b;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/media/selection/b;Z)V

    .line 3001
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->af:Z

    .line 3003
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ae:Lcom/twitter/app/common/util/b$a;

    if-eqz v0, :cond_1

    .line 3004
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ae:Lcom/twitter/app/common/util/b$a;

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/util/b;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 3005
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ae:Lcom/twitter/app/common/util/b$a;

    .line 3008
    :cond_1
    return-void
.end method

.method private as()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3011
    const/4 v2, 0x1

    .line 3012
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ay:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 3026
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    invoke-static {v1, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/view/View;Z)V

    .line 3027
    return-void

    .line 3014
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0}, Lcom/twitter/library/view/QuoteView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 3015
    goto :goto_0

    .line 3016
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 3017
    goto :goto_0

    .line 3019
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 3020
    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    sget-object v4, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-eq v0, v4, :cond_3

    move v0, v1

    .line 3022
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private at()V
    .locals 2

    .prologue
    .line 3030
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->t:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 3038
    :goto_0
    return-void

    .line 3033
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ay:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:Lcom/twitter/library/view/QuoteView;

    .line 3035
    invoke-virtual {v0}, Lcom/twitter/library/view/QuoteView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 3036
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->o()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 3037
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->t:Landroid/widget/ImageButton;

    invoke-static {v1, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 3036
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private au()V
    .locals 8

    .prologue
    .line 3095
    invoke-static {p0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 3096
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3097
    const-string/jumbo v2, "page"

    invoke-virtual {v0}, Lcom/twitter/android/composer/a;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3098
    const-string/jumbo v0, "title"

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3099
    const-string/jumbo v0, "e_owner_id"

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3100
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v0}, Lcom/twitter/android/composer/j;->d()Lcom/twitter/model/geo/c;

    move-result-object v0

    .line 3101
    if-eqz v0, :cond_1

    .line 3102
    new-instance v2, Landroid/location/Location;

    const-string/jumbo v3, ""

    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 3103
    invoke-virtual {v0}, Lcom/twitter/model/geo/c;->a()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v3

    .line 3104
    invoke-virtual {v0}, Lcom/twitter/model/geo/c;->b()Lcom/twitter/model/geo/b;

    move-result-object v4

    .line 3105
    if-eqz v4, :cond_2

    .line 3106
    invoke-virtual {v4}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    .line 3107
    invoke-virtual {v4}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 3112
    :cond_0
    :goto_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 3113
    const-string/jumbo v5, "e_broadcast_location_geotag"

    sget-object v6, Lcom/twitter/model/geo/c;->a:Lcom/twitter/util/serialization/l;

    .line 3114
    invoke-static {v0, v6}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    .line 3113
    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 3115
    const-string/jumbo v0, "e_broadcast_location"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3116
    const-string/jumbo v0, "e_broadcast_location_name"

    iget-object v2, v3, Lcom/twitter/model/geo/TwitterPlace;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3117
    const-string/jumbo v0, "e_broadcast_location_extras"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 3119
    :cond_1
    const/16 v0, 0x20e

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/composer/ComposerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3120
    return-void

    .line 3108
    :cond_2
    iget-object v4, v3, Lcom/twitter/model/geo/TwitterPlace;->h:Lcom/twitter/model/geo/b;

    if-eqz v4, :cond_0

    .line 3109
    iget-object v4, v3, Lcom/twitter/model/geo/TwitterPlace;->h:Lcom/twitter/model/geo/b;

    invoke-virtual {v4}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 3110
    iget-object v4, v3, Lcom/twitter/model/geo/TwitterPlace;->h:Lcom/twitter/model/geo/b;

    invoke-virtual {v4}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    goto :goto_0
.end method

.method private av()V
    .locals 2

    .prologue
    .line 3123
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a06b9

    .line 3126
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/TweetBox;->setImeActionLabel(Ljava/lang/CharSequence;)V

    .line 3127
    return-void

    .line 3123
    :cond_0
    const v0, 0x7f0a06c0

    goto :goto_0
.end method

.method private aw()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3209
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->setResult(I)V

    .line 3210
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Z)V

    .line 3212
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 3213
    const-string/jumbo v1, "save_drafts_dialog"

    .line 3214
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 3215
    if-eqz v0, :cond_0

    .line 3216
    invoke-virtual {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 3217
    if-eqz v0, :cond_0

    .line 3218
    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 3221
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->finish()V

    .line 3222
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/composer/ComposerActivity;)Lsc;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 1421
    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$25;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/ComposerActivity$25;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1439
    new-instance v0, Lsa;

    .line 1442
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->r:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/twitter/android/composer/ComposerActivity;->x:Landroid/view/View;

    iget-object v6, p0, Lcom/twitter/android/composer/ComposerActivity;->y:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/twitter/android/composer/ComposerActivity;->C:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object v1, p0

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lsa;-><init>(Landroid/content/Context;Lsb;Landroid/support/v4/app/FragmentManager;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/twitter/android/composer/o;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    .line 1449
    return-void
.end method

.method private b(Lcom/twitter/library/client/Session;Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 1452
    new-instance v6, Lcom/twitter/android/geo/a;

    const-string/jumbo v0, "composer_activity_location_dialog"

    .line 1455
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v1

    const/16 v2, 0x204

    invoke-direct {v6, p0, v0, v1, v2}, Lcom/twitter/android/geo/a;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Lcom/twitter/util/android/f;I)V

    .line 1458
    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$26;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/ComposerActivity$26;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1499
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->U:Lcom/twitter/library/client/p;

    .line 1503
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->R()Lbqn;

    move-result-object v5

    const v0, 0x7f130431

    .line 1505
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/geotag/InlinePlacePickerView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/android/composer/geotag/InlinePlacePickerView;

    const v0, 0x7f130433

    .line 1506
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iget-object v9, p0, Lcom/twitter/android/composer/ComposerActivity;->s:Lcom/twitter/android/widget/ToggleImageButton;

    iget-object v10, p0, Lcom/twitter/android/composer/ComposerActivity;->x:Landroid/view/View;

    iget-object v11, p0, Lcom/twitter/android/composer/ComposerActivity;->y:Landroid/widget/TextView;

    iget-object v12, p0, Lcom/twitter/android/composer/ComposerActivity;->C:Landroid/widget/ImageView;

    move-object v0, p0

    move-object v3, p1

    move-object/from16 v13, p2

    .line 1497
    invoke-static/range {v0 .. v13}, Lcom/twitter/android/composer/j;->a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/composer/geotag/b;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbqn;Lcom/twitter/android/geo/a;Lcom/twitter/android/composer/geotag/InlinePlacePickerView;Landroid/widget/TextView;Lcom/twitter/android/widget/ToggleImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/os/Bundle;)Lcom/twitter/android/composer/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    .line 1512
    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 2234
    invoke-static {p0, p1, v4}, Lbrw;->b(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2235
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2236
    invoke-direct {p0, v2}, Lcom/twitter/android/composer/ComposerActivity;->f(I)V

    .line 2237
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2238
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c000e

    invoke-virtual {v2, v3, v0}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2239
    invoke-direct {p0, v4}, Lcom/twitter/android/composer/ComposerActivity;->e(I)V

    .line 2245
    :goto_0
    return-void

    .line 2241
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2242
    invoke-direct {p0, v4}, Lcom/twitter/android/composer/ComposerActivity;->f(I)V

    .line 2243
    invoke-direct {p0, v2}, Lcom/twitter/android/composer/ComposerActivity;->e(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/composer/ComposerActivity;Z)Z
    .locals 0

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/twitter/android/composer/ComposerActivity;->X:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/composer/ComposerActivity;Z)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->f(Z)V

    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2842
    new-instance v4, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 2843
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2844
    iget-object v6, v0, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    .line 2845
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1, v6}, Lcom/twitter/android/composer/f;->b(Landroid/net/Uri;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v1

    .line 2846
    if-eqz v1, :cond_0

    .line 2847
    invoke-virtual {v1, v8}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    .line 2848
    invoke-virtual {v0, v8}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 2849
    instance-of v7, v1, Lcom/twitter/model/media/EditableImage;

    if-eqz v7, :cond_1

    instance-of v7, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v7, :cond_1

    .line 2850
    check-cast v0, Lcom/twitter/model/media/EditableImage;

    check-cast v1, Lcom/twitter/model/media/EditableImage;

    iget-object v1, v1, Lcom/twitter/model/media/EditableImage;->g:Ljava/util/List;

    iput-object v1, v0, Lcom/twitter/model/media/EditableImage;->g:Ljava/util/List;

    .line 2852
    :cond_1
    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2856
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    .line 2857
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2858
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2859
    iget-object v6, v0, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    .line 2860
    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 2861
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 2862
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2866
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2867
    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/net/Uri;Z)V

    goto :goto_2

    .line 2870
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2871
    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v4, p0, v0}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Lcom/twitter/model/drafts/DraftAttachment;)V

    goto :goto_3

    .line 2874
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;)V

    .line 2875
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2876
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    .line 2877
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_a

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v0}, Lsc;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v2

    .line 2876
    :goto_4
    invoke-virtual {v1, v0}, Lsc;->a(Z)V

    .line 2878
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v0}, Lsc;->j()V

    .line 2886
    :cond_7
    :goto_5
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    if-eqz v0, :cond_9

    .line 2887
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    .line 2888
    if-nez v0, :cond_8

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 2889
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {v0, v8}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 2891
    :cond_8
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/j;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 2893
    :cond_9
    return-void

    :cond_a
    move v0, v3

    .line 2877
    goto :goto_4

    .line 2879
    :cond_b
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v0}, Lsc;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2880
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-ne v0, v2, :cond_c

    move v0, v2

    .line 2881
    :goto_6
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->o()V

    .line 2882
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v1, v0}, Lsc;->b(Z)V

    .line 2883
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    goto :goto_5

    :cond_c
    move v0, v3

    .line 2880
    goto :goto_6
.end method

.method static synthetic d(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/selection/c;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    return-object v0
.end method

.method private d(Z)V
    .locals 4

    .prologue
    .line 1697
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/composer/ComposerActivity;->T:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1698
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/composer/ComposerActivity;->T:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 1700
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 1701
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    if-eqz v0, :cond_1

    .line 1702
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    iget v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ai:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/j;->a(ZI)V

    .line 1704
    :cond_1
    const v0, 0x7f05003c

    const v1, 0x7f05003d

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->overridePendingTransition(II)V

    .line 1707
    invoke-static {p0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 1708
    invoke-virtual {v0}, Lcom/twitter/android/composer/a;->v()J

    move-result-wide v2

    .line 1709
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_2

    .line 1710
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->V:Z

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    .line 1711
    :goto_0
    invoke-static {}, Lcom/twitter/android/card/h;->b()Lcom/twitter/android/card/h;

    move-result-object v1

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/card/h;->a(JI)V

    .line 1713
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->a()V

    .line 1714
    return-void

    .line 1710
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/composer/ComposerActivity;Z)Z
    .locals 0

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/twitter/android/composer/ComposerActivity;->ay:Z

    return p1
.end method

.method static synthetic e(Lcom/twitter/android/composer/ComposerActivity;)I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    return v0
.end method

.method private e(I)V
    .locals 0

    .prologue
    .line 2822
    iput p1, p0, Lcom/twitter/android/composer/ComposerActivity;->ak:I

    .line 2823
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->u()V

    .line 2824
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1996
    if-eqz p1, :cond_0

    .line 1997
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x201

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a06bd

    .line 1999
    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0262

    .line 2000
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a07b3

    .line 2001
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2002
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 2003
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 2004
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "save_drafts_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 2011
    :goto_0
    return-void

    .line 2006
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->b(Lcom/twitter/library/client/Session;)V

    .line 2007
    invoke-virtual {p0, v2}, Lcom/twitter/android/composer/ComposerActivity;->setResult(I)V

    .line 2008
    invoke-virtual {p0, v2}, Lcom/twitter/android/composer/ComposerActivity;->a(Z)V

    .line 2009
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/l;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    return-object v0
.end method

.method private f(I)V
    .locals 0

    .prologue
    .line 2827
    iput p1, p0, Lcom/twitter/android/composer/ComposerActivity;->al:I

    .line 2828
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->u()V

    .line 2829
    return-void
.end method

.method private f(Z)V
    .locals 5

    .prologue
    .line 2401
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    .line 2422
    :goto_0
    return-void

    .line 2406
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2407
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2408
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    const v2, 0x7f0a01eb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    goto :goto_0

    .line 2412
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->i()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    .line 2413
    if-eqz v1, :cond_5

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2414
    :cond_3
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const v1, 0x7f0a0215

    .line 2415
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2417
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    goto :goto_0

    .line 2415
    :cond_4
    const v2, 0x7f0a01f7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2416
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2421
    :cond_5
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    const v2, 0x7f0a01f1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    return-object v0
.end method

.method private g(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2911
    invoke-static {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2920
    :goto_0
    return v0

    .line 2914
    :cond_0
    new-instance v2, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    const v3, 0x7f0a03c9

    .line 2915
    invoke-virtual {p0, v3}, Lcom/twitter/android/composer/ComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/String;

    const-string/jumbo v5, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v5, v4, v1

    invoke-direct {v2, v3, p0, v4}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v3, ":composition:gallery:"

    .line 2916
    invoke-virtual {v2, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v2

    .line 2917
    invoke-virtual {v2, v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->b(Z)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 2918
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2919
    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/composer/ComposerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    .line 2920
    goto :goto_0
.end method

.method static synthetic h(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/widget/MediaAttachmentsView;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/b;
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->am()Lcom/twitter/android/composer/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/g;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/t;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ad()V

    return-void
.end method

.method static synthetic n(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/j;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/composer/ComposerActivity;)Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    return v0
.end method

.method static synthetic p(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->al()V

    return-void
.end method

.method static synthetic q(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ap()V

    return-void
.end method

.method static synthetic r(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->B()V

    return-void
.end method

.method static synthetic s(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ac()V

    return-void
.end method

.method static synthetic t(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/card/pollcompose/d;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/android/composer/ComposerActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->u:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->av()V

    return-void
.end method

.method static synthetic w(Lcom/twitter/android/composer/ComposerActivity;)Lsa;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/r;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ar()V

    return-void
.end method

.method static synthetic z(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/ui/widget/TwitterButton;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    return-object v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 914
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->u:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 922
    :goto_0
    return-void

    .line 917
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ay:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 919
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 920
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->u:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 921
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->u:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto :goto_0

    .line 919
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 921
    :cond_2
    const v0, 0x3e99999a    # 0.3f

    goto :goto_2
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 1530
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 454
    const-string/jumbo v0, "android_tweet_composer_location_picker_end"

    .line 455
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d015d

    .line 457
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 458
    const v0, 0x7f04008a

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 459
    invoke-virtual {p2, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 460
    invoke-virtual {p2, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 461
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 462
    return-object p2

    .line 455
    :cond_0
    const v0, 0x7f0d015c

    goto :goto_0
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 2674
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2675
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/l;->a(F)V

    .line 2677
    :cond_0
    return-void
.end method

.method a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2371
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->at:Z

    if-nez v0, :cond_0

    .line 2372
    iput-boolean v3, p0, Lcom/twitter/android/composer/ComposerActivity;->at:Z

    .line 2373
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->n()V

    .line 2375
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->v:Lcom/twitter/android/composer/ComposerCountView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/ComposerCountView;->a(I)I

    move-result v0

    .line 2376
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ao()V

    .line 2377
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->B()V

    .line 2378
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aq()V

    .line 2379
    if-eqz p1, :cond_3

    .line 2380
    iput-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    .line 2381
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 2382
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2389
    :cond_1
    :goto_0
    if-gez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->au:Z

    if-nez v0, :cond_2

    .line 2390
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->m()V

    .line 2391
    iput-boolean v3, p0, Lcom/twitter/android/composer/ComposerActivity;->au:Z

    .line 2393
    :cond_2
    return-void

    .line 2385
    :cond_3
    invoke-direct {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->f(Z)V

    .line 2386
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/f;->b(Lcom/twitter/library/client/Session;)V

    .line 2387
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->C()V

    goto :goto_0
.end method

.method a(IZ)V
    .locals 1

    .prologue
    .line 1842
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(IZZ)V

    .line 1843
    return-void
.end method

.method a(IZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1846
    iget v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-eq v2, p1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1965
    :cond_0
    :goto_0
    return-void

    .line 1850
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->clearAnimation()V

    .line 1852
    iget-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->aa:Z

    if-nez v2, :cond_2

    .line 1855
    iput p1, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    goto :goto_0

    .line 1859
    :cond_2
    if-ne p1, v0, :cond_3

    invoke-static {p0}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    if-eq p1, v3, :cond_5

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    .line 1860
    invoke-virtual {v2}, Lcom/twitter/android/composer/g;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move p1, v1

    .line 1864
    :cond_5
    if-ne p1, v0, :cond_6

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v2

    if-ne v2, v3, :cond_6

    .line 1866
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1867
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, p2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Z)V

    .line 1877
    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 1878
    packed-switch p1, :pswitch_data_0

    .line 1909
    :goto_1
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1911
    iput p1, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    .line 1913
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setAllowDrawerUpPositionIfKeyboard(Z)V

    .line 1914
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDrawerDraggable(Z)V

    .line 1915
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDraggableBelowUpPosition(Z)V

    .line 1916
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDispatchDragToChildren(Z)V

    .line 1917
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 1918
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setFullScreenHeaderView(Landroid/view/View;)V

    .line 1920
    packed-switch p1, :pswitch_data_1

    .line 1964
    :goto_2
    iput-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    goto :goto_0

    .line 1870
    :cond_7
    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ah:Z

    .line 1871
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    goto :goto_0

    .line 1880
    :pswitch_0
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/j;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1881
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/t;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1882
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/l;->a(Landroid/support/v4/app/FragmentTransaction;)V

    goto :goto_1

    .line 1886
    :pswitch_1
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/l;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1887
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/t;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1888
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/j;->a(Landroid/support/v4/app/FragmentTransaction;)V

    goto :goto_1

    .line 1892
    :pswitch_2
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/l;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1893
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/j;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1894
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/t;->a(Landroid/support/v4/app/FragmentTransaction;)V

    goto :goto_1

    .line 1899
    :pswitch_3
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/l;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1900
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/j;->b(Landroid/support/v4/app/FragmentTransaction;)V

    .line 1901
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/t;->b(Landroid/support/v4/app/FragmentTransaction;)V

    goto :goto_1

    .line 1922
    :pswitch_4
    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Y:Z

    .line 1923
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    goto :goto_2

    .line 1927
    :pswitch_5
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 1928
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/l;->a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V

    .line 1929
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, p2, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    .line 1930
    invoke-static {}, Lbpo;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1931
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 1933
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->e()V

    goto :goto_2

    .line 1937
    :pswitch_6
    iput-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->Y:Z

    .line 1938
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 1939
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 1940
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    goto/16 :goto_2

    .line 1944
    :pswitch_7
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    if-nez p3, :cond_9

    :goto_3
    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 1945
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/t;->a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V

    .line 1946
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    .line 1947
    if-nez p3, :cond_a

    .line 1948
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->j()Z

    goto/16 :goto_2

    :cond_9
    move v0, v1

    .line 1944
    goto :goto_3

    .line 1950
    :cond_a
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2}, Lcom/twitter/android/composer/TweetBox;->getTokenAtCursor()Lnk;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/t;->a(Lnk;)V

    goto/16 :goto_2

    .line 1955
    :pswitch_8
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Z)V

    .line 1956
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    goto/16 :goto_2

    .line 1878
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1920
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3189
    const/16 v0, 0x201

    if-ne p2, v0, :cond_0

    .line 3190
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 3191
    :goto_0
    const/4 v2, -0x2

    if-ne p3, v2, :cond_2

    .line 3192
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v2}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b()V

    .line 3193
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->k()V

    .line 3194
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/f;->b(Lcom/twitter/library/client/Session;)V

    .line 3195
    iput v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ai:I

    .line 3196
    invoke-static {}, Lcom/twitter/android/composer/q;->a()Lcom/twitter/android/composer/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/composer/q;->c()J

    move-result-wide v2

    .line 3197
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v4

    invoke-virtual {v1, v4, v0, v2, v3}, Lcom/twitter/android/composer/o;->a(ZZJ)V

    .line 3198
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aw()V

    .line 3206
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 3190
    goto :goto_0

    .line 3199
    :cond_2
    const/4 v1, -0x1

    if-ne p3, v1, :cond_0

    .line 3200
    const/4 v1, 0x2

    iput v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ai:I

    .line 3201
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/composer/o;->a(ZZ)V

    .line 3202
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/library/client/Session;)V

    .line 3203
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aw()V

    goto :goto_1
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2669
    invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 2670
    return-void
.end method

.method a(Landroid/net/Uri;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 2761
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/f;->a(Landroid/net/Uri;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 2762
    if-nez v0, :cond_0

    .line 2816
    :goto_0
    return-void

    .line 2767
    :cond_0
    sget-object v1, Lcom/twitter/android/composer/ComposerActivity$35;->a:[I

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2783
    const-string/jumbo v0, ""

    .line 2787
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2788
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, ":composition::"

    aput-object v3, v2, v5

    aput-object v0, v2, v6

    const-string/jumbo v0, ":dismiss"

    aput-object v0, v2, v7

    .line 2789
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2788
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2792
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;)V

    .line 2794
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2795
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getMediaCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 2796
    invoke-direct {p0, v4}, Lcom/twitter/android/composer/ComposerActivity;->e(I)V

    .line 2797
    invoke-direct {p0, v4}, Lcom/twitter/android/composer/ComposerActivity;->f(I)V

    .line 2798
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->g()V

    .line 2799
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ao:Landroid/animation/Animator;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-eq v0, v7, :cond_3

    .line 2800
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getMeasuredHeight()I

    move-result v1

    .line 2801
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ao:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    .line 2802
    new-array v2, v6, [F

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v2, v5

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 2803
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2808
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2809
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->as()V

    .line 2814
    :cond_2
    :goto_3
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->at()V

    .line 2815
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->z()V

    goto/16 :goto_0

    .line 2769
    :pswitch_0
    const-string/jumbo v0, "photo"

    goto :goto_1

    .line 2774
    :pswitch_1
    const-string/jumbo v0, "video"

    goto/16 :goto_1

    .line 2778
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->n:Lcom/twitter/android/widget/FoundMediaAttributionView;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/FoundMediaAttributionView;->setVisibility(I)V

    .line 2779
    const-string/jumbo v0, "gif"

    goto/16 :goto_1

    .line 2805
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c()V

    goto :goto_2

    .line 2811
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->an:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_3

    .line 2767
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Lcom/twitter/android/media/selection/b;Z)V
    .locals 10

    .prologue
    const/16 v2, 0x8

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 2425
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ad:Z

    if-nez v0, :cond_1

    .line 2432
    iput-boolean v9, p0, Lcom/twitter/android/composer/ComposerActivity;->af:Z

    .line 2543
    :cond_0
    :goto_0
    return-void

    .line 2436
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->e()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v3

    .line 2437
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 2438
    iget v5, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    packed-switch v5, :pswitch_data_0

    .line 2445
    iget-object v5, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, p0, v6}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2446
    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p0, v5, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/net/Uri;Z)V

    .line 2447
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->an()V

    .line 2448
    iget v0, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    const/4 v5, 0x4

    if-ne v0, v5, :cond_2

    .line 2449
    invoke-static {p0}, Lwe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2441
    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->d()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2451
    :cond_2
    const v0, 0x7f0a04b2

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 2458
    :cond_3
    invoke-direct {p0, v3}, Lcom/twitter/android/composer/ComposerActivity;->c(Ljava/util/List;)V

    .line 2460
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getCount()I

    move-result v4

    .line 2462
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->setVisibility(I)V

    .line 2463
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/selection/b;Z)Z

    .line 2465
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    .line 2466
    if-ne v5, v9, :cond_4

    sget-object v0, Lcom/twitter/android/composer/ComposerType;->a:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0}, Lcom/twitter/android/util/j;->a(Lcom/twitter/android/composer/ComposerType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2467
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->n:Lcom/twitter/android/widget/FoundMediaAttributionView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/FoundMediaAttributionView;->setVisibility(I)V

    .line 2475
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2476
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->g()V

    .line 2480
    :goto_3
    invoke-direct {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->f(Z)V

    .line 2482
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/model/av/h;)V

    .line 2483
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ak()V

    .line 2485
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 2486
    invoke-static {v3}, Lcom/twitter/android/composer/ComposerActivity;->a(Ljava/util/List;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 2487
    invoke-direct {p0, v2}, Lcom/twitter/android/composer/ComposerActivity;->e(I)V

    .line 2488
    invoke-direct {p0, v2}, Lcom/twitter/android/composer/ComposerActivity;->f(I)V

    .line 2524
    :cond_5
    :goto_4
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->an()V

    .line 2525
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ao()V

    .line 2527
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2528
    if-lez v5, :cond_c

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne v0, v1, :cond_c

    .line 2529
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    const v1, 0x7f020114

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2530
    if-ne v5, v9, :cond_6

    if-nez v4, :cond_6

    .line 2531
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->am:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2537
    :cond_6
    :goto_5
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ac()V

    .line 2539
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    const-string/jumbo v1, "stickers_composer_tooltip"

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/r;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2541
    invoke-static {p0}, Lcom/twitter/android/composer/ComposerActivity;->B(Lcom/twitter/android/composer/ComposerActivity;)V

    goto/16 :goto_0

    .line 2469
    :cond_7
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2470
    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    invoke-virtual {v0}, Lcom/twitter/model/media/MediaSource;->c()Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    move-result-object v0

    .line 2471
    iget-object v6, p0, Lcom/twitter/android/composer/ComposerActivity;->n:Lcom/twitter/android/widget/FoundMediaAttributionView;

    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/FoundMediaAttributionView;->setProvider(Lcom/twitter/model/media/foundmedia/FoundMediaProvider;)V

    .line 2472
    iget-object v6, p0, Lcom/twitter/android/composer/ComposerActivity;->n:Lcom/twitter/android/widget/FoundMediaAttributionView;

    if-eqz v0, :cond_8

    move v0, v1

    :goto_6
    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/FoundMediaAttributionView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_8
    move v0, v2

    goto :goto_6

    .line 2478
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->f()V

    goto/16 :goto_3

    .line 2489
    :cond_a
    iget-boolean v2, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v2, :cond_b

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->R:I

    if-lez v0, :cond_5

    .line 2490
    :cond_b
    invoke-static {v3}, Lcom/twitter/model/util/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->b(Ljava/util/List;)V

    .line 2491
    new-instance v0, Lcom/twitter/android/composer/ComposerActivity$28;

    invoke-direct {v0, p0}, Lcom/twitter/android/composer/ComposerActivity$28;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 2498
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v2, v0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2499
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v2, v0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2501
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0e02f2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2503
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2504
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 2506
    iget-object v7, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v7}, Lcom/twitter/ui/widget/TwitterButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v7

    new-instance v8, Lcom/twitter/android/composer/ComposerActivity$29;

    invoke-direct {v8, p0, v0, v6, v2}, Lcom/twitter/android/composer/ComposerActivity$29;-><init>(Lcom/twitter/android/composer/ComposerActivity;Landroid/view/View;Landroid/graphics/Rect;I)V

    invoke-virtual {v7, v8}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_4

    .line 2534
    :cond_c
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    const v1, 0x7f020113

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 2438
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 3089
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$b;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;)V

    .line 3091
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->an()V

    .line 3092
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1200
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 1201
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 1203
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1204
    invoke-virtual {v0, p1}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1205
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/twitter/android/composer/o;->a(J)V

    .line 1206
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/TweetBox;->setSession(Lcom/twitter/library/client/Session;)V

    .line 1207
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/r;->a(Lcom/twitter/library/client/Session;)V

    .line 1208
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/j;->a(Lcom/twitter/library/client/Session;)V

    move-object v1, v0

    .line 1211
    :goto_0
    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    .line 1213
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 1214
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->o:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v3, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 1215
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->p:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 1216
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->p:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1219
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v0}, Lcom/twitter/android/composer/j;->c()V

    .line 1220
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->C()V

    .line 1222
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    .line 1223
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V

    .line 1226
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ap()V

    .line 1227
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/composer/t;->a(J)V

    .line 1229
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v0, v1}, Lsc;->a(Lcom/twitter/library/client/Session;)V

    .line 1230
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    invoke-virtual {v0, v1}, Lsa;->a(Lcom/twitter/library/client/Session;)V

    .line 1231
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/l;->a(Lcom/twitter/library/client/Session;)V

    .line 1232
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;)V

    .line 1233
    return-void

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 2351
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 2352
    if-eqz p1, :cond_0

    .line 2353
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    .line 2355
    :cond_0
    return-void
.end method

.method public a(Lcmr;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1541
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 1542
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowHomeAsUpEnabled(Z)V

    .line 1543
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 1828
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v0}, Lcom/twitter/android/composer/j;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    .line 1829
    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    .line 1830
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 1831
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    invoke-virtual {v2}, Lsa;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 1832
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    invoke-virtual {v2, v0, v1}, Lsa;->a(Ljava/lang/String;Lcom/twitter/model/core/TwitterUser;)V

    .line 1836
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    if-eqz v0, :cond_0

    .line 1837
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->l()V

    .line 1839
    :cond_0
    return-void

    .line 1829
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1834
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/j;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2681
    if-nez p1, :cond_0

    .line 2682
    iput v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    .line 2685
    :cond_0
    if-ne p1, v5, :cond_4

    move v0, v1

    .line 2686
    :goto_0
    iget-object v4, p0, Lcom/twitter/android/composer/ComposerActivity;->z:Landroid/widget/Button;

    if-nez v0, :cond_5

    move v3, v1

    :goto_1
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 2687
    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-eq v3, v1, :cond_1

    iget-boolean v3, p0, Lcom/twitter/android/composer/ComposerActivity;->ah:Z

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    .line 2688
    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 2690
    :cond_1
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ah:Z

    .line 2692
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ao()V

    .line 2694
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v2, p1}, Lcom/twitter/android/composer/j;->a(I)V

    .line 2695
    iget v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-ne v2, v5, :cond_2

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    invoke-virtual {v2}, Lcom/twitter/android/composer/g;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2696
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 2698
    :cond_2
    iget v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    if-eqz v0, :cond_3

    .line 2699
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getTokenAtCursor()Lnk;

    move-result-object v0

    .line 2701
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 2702
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/t;->a(Lnk;)V

    .line 2703
    if-eqz v0, :cond_3

    .line 2704
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    const-string/jumbo v2, "full_screen"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/composer/o;->a(Lnk;Ljava/lang/String;)V

    .line 2707
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 2685
    goto :goto_0

    :cond_5
    move v3, v2

    .line 2686
    goto :goto_1
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 12

    .prologue
    const/4 v10, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 467
    const-string/jumbo v0, "composer:complete"

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->O()Lcom/twitter/metrics/j;

    move-result-object v1

    sget-object v5, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-static {v0, v1, v5}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 468
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->i()V

    .line 472
    iput-boolean v3, p0, Lcom/twitter/android/composer/ComposerActivity;->aa:Z

    .line 474
    invoke-static {p0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/a;

    move-result-object v6

    .line 476
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/o;->a(Ljava/lang/String;)V

    .line 477
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->t()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/o;->a(Lcom/twitter/analytics/model/ScribeItem;)V

    .line 479
    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->d()Lcom/twitter/android/composer/s;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->b:Lcom/twitter/android/composer/s;

    .line 480
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->U:Lcom/twitter/library/client/p;

    .line 481
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->U:Lcom/twitter/library/client/p;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/composer/f;->a(Landroid/content/Context;Lcom/twitter/library/client/p;)V

    .line 483
    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->i()Ljava/lang/String;

    move-result-object v0

    .line 484
    if-nez p1, :cond_0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    move-object v1, v0

    .line 486
    :goto_0
    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    .line 487
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/android/composer/o;->a(J)V

    .line 492
    if-eqz p1, :cond_3

    .line 493
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/f;->a(Landroid/os/Bundle;)V

    .line 494
    const/4 v5, 0x0

    .line 495
    const-string/jumbo v0, "original_owner_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/android/composer/ComposerActivity;->T:J

    .line 496
    const-string/jumbo v0, "loaded_from_draft"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    .line 497
    const-string/jumbo v0, "tweet_posted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->V:Z

    .line 498
    const-string/jumbo v0, "conversation_card_data"

    sget-object v6, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v6}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ax:Lcax;

    .line 500
    const-string/jumbo v0, "current_drawer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 501
    const-string/jumbo v6, "current_drawer_position"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 502
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    if-ne v7, v4, :cond_1

    if-ne v0, v10, :cond_1

    if-ne v6, v4, :cond_1

    .line 504
    iput v2, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    .line 505
    iput v2, p0, Lcom/twitter/android/composer/ComposerActivity;->S:I

    .line 506
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->aq:Z

    .line 514
    :goto_1
    const-string/jumbo v0, "show_link_hint"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Z:Z

    .line 516
    const-string/jumbo v0, "launch_camera_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    .line 517
    const-string/jumbo v0, "selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 518
    const-string/jumbo v4, "edited_text"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/twitter/android/composer/ComposerActivity;->at:Z

    .line 519
    const-string/jumbo v4, "reached_limit"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/twitter/android/composer/ComposerActivity;->au:Z

    .line 520
    const-string/jumbo v4, "requesting_camera_permissions"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/twitter/android/composer/ComposerActivity;->ap:Z

    move-object v4, v0

    move v0, v3

    .line 567
    :goto_2
    invoke-direct {p0, v1, p1}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/library/client/Session;Landroid/os/Bundle;)V

    .line 568
    invoke-direct {p0, p1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V

    .line 569
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/os/Bundle;)V

    .line 570
    iget-object v6, p0, Lcom/twitter/android/composer/ComposerActivity;->A:Landroid/view/View;

    invoke-direct {p0, v1, v6}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/library/client/Session;Landroid/view/View;)V

    .line 571
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->D()V

    .line 572
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ab()V

    .line 573
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->b(Landroid/os/Bundle;)V

    .line 574
    invoke-direct {p0, v1, p1}, Lcom/twitter/android/composer/ComposerActivity;->b(Lcom/twitter/library/client/Session;Landroid/os/Bundle;)V

    .line 575
    if-nez p1, :cond_7

    :goto_3
    invoke-direct {p0, v5, v4, v2, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/net/Uri;[IZI)V

    .line 576
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ao()V

    .line 577
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    .line 579
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const v0, 0x7f13014c

    .line 580
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 578
    invoke-virtual {v1, v2, v0, p1}, Lcom/twitter/android/composer/g;->a(Landroid/support/v4/app/FragmentManager;Landroid/view/View;Landroid/os/Bundle;)V

    .line 582
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$12;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$12;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/g;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment$b;)V

    .line 617
    invoke-static {}, Lcom/twitter/android/composer/q;->a()Lcom/twitter/android/composer/q;

    move-result-object v0

    .line 619
    invoke-virtual {v0}, Lcom/twitter/android/composer/q;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    if-eqz p1, :cond_8

    .line 628
    :goto_4
    new-instance v0, Lcom/twitter/android/composer/ComposerActivity$23;

    invoke-direct {v0, p0}, Lcom/twitter/android/composer/ComposerActivity$23;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aE:Lakv;

    .line 643
    new-instance v0, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    const/16 v1, 0x20c

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aD:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    .line 645
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aD:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aE:Lakv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;->a(Lakv;)V

    .line 646
    return-void

    .line 485
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 507
    :cond_1
    const-string/jumbo v7, "show_full_screen_suggestions"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 508
    iput v10, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    .line 509
    iput v4, p0, Lcom/twitter/android/composer/ComposerActivity;->S:I

    goto/16 :goto_1

    .line 511
    :cond_2
    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    .line 512
    iput v6, p0, Lcom/twitter/android/composer/ComposerActivity;->S:I

    goto/16 :goto_1

    .line 523
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, v6}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/android/composer/a;)V

    .line 524
    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->q()Landroid/net/Uri;

    move-result-object v5

    .line 525
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/twitter/android/composer/ComposerActivity;->T:J

    .line 526
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->c()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_4

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    .line 527
    iput-boolean v3, p0, Lcom/twitter/android/composer/ComposerActivity;->V:Z

    .line 528
    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->x()Lcax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ax:Lcax;

    .line 529
    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->b()I

    move-result v0

    .line 531
    iput v3, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    .line 532
    iput-boolean v3, p0, Lcom/twitter/android/composer/ComposerActivity;->aA:Z

    .line 533
    packed-switch v0, :pswitch_data_0

    .line 551
    :goto_6
    :pswitch_0
    iget v7, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    if-eqz v7, :cond_5

    .line 552
    iput v3, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    .line 558
    :goto_7
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Z:Z

    .line 559
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    .line 560
    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->f()[I

    move-result-object v4

    .line 562
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    iget-wide v8, p0, Lcom/twitter/android/composer/ComposerActivity;->T:J

    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v8, v9, v7}, Lcom/twitter/android/composer/o;->a(JLjava/lang/String;)V

    .line 564
    invoke-virtual {v6}, Lcom/twitter/android/composer/a;->c()I

    move-result v0

    goto/16 :goto_2

    :cond_4
    move v0, v3

    .line 526
    goto :goto_5

    .line 535
    :pswitch_1
    invoke-virtual {v6, v2}, Lcom/twitter/android/composer/a;->c(I)I

    move-result v7

    iput v7, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    goto :goto_6

    .line 539
    :pswitch_2
    invoke-virtual {v6, v4}, Lcom/twitter/android/composer/a;->c(I)I

    move-result v7

    iput v7, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    goto :goto_6

    .line 543
    :pswitch_3
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->aA:Z

    goto :goto_6

    .line 554
    :cond_5
    if-ne v0, v4, :cond_6

    move v0, v4

    :goto_8
    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    goto :goto_7

    :cond_6
    move v0, v2

    goto :goto_8

    :cond_7
    move v2, v3

    .line 575
    goto/16 :goto_3

    .line 621
    :cond_8
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->o()Z

    move-result v1

    if-nez v1, :cond_9

    .line 623
    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/q;->a(Z)V

    goto/16 :goto_4

    .line 625
    :cond_9
    invoke-virtual {v0}, Lcom/twitter/android/composer/q;->c()J

    goto/16 :goto_4

    .line 533
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2900
    const/16 v0, 0x206

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2901
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-eq v0, v2, :cond_1

    .line 2902
    invoke-virtual {p0, v2, p1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZZ)V

    .line 2908
    :cond_0
    :goto_0
    return-void

    .line 2903
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 2905
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v1, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    goto :goto_0
.end method

.method public c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3047
    invoke-static {p0, p1}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3048
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 3049
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->au()V

    .line 3060
    :goto_0
    return-void

    .line 3051
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, v2, p1}, Lcom/twitter/android/media/selection/c;->a(ZI)V

    goto :goto_0

    .line 3054
    :cond_1
    const-string/jumbo v0, ":composition::twitter_camera"

    .line 3055
    invoke-static {p0, p1, v0}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x205

    .line 3054
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3057
    iput p1, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    .line 3058
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ap:Z

    goto :goto_0
.end method

.method c(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2929
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getCount()I

    move-result v0

    const/16 v2, 0x8c

    if-le v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2930
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->v:Lcom/twitter/android/composer/ComposerCountView;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/ComposerCountView;->setVisibility(I)V

    .line 2931
    return-void

    :cond_1
    move v0, v1

    .line 2929
    goto :goto_0

    .line 2930
    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method protected d(Lank;)Lcom/twitter/android/composer/d;
    .locals 2

    .prologue
    .line 651
    invoke-static {}, Lcom/twitter/android/composer/u;->d()Lcom/twitter/android/composer/u$a;

    move-result-object v0

    .line 652
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/u$a;->a(Lamu;)Lcom/twitter/android/composer/u$a;

    move-result-object v0

    .line 653
    invoke-virtual {v0}, Lcom/twitter/android/composer/u$a;->a()Lcom/twitter/android/composer/d;

    move-result-object v0

    .line 651
    return-object v0
.end method

.method protected d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1765
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 1768
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->e()V

    .line 1769
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setTweetBoxListener(Lcom/twitter/android/composer/TweetBox$b;)V

    .line 1770
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/t;->a(Lcom/twitter/android/composer/t$a;)V

    .line 1772
    invoke-static {}, Lcom/twitter/android/provider/SuggestionsProvider;->b()V

    .line 1773
    invoke-static {}, Lcom/twitter/android/provider/SuggestionsProvider;->c()V

    .line 1775
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    invoke-interface {v0}, Lcom/twitter/android/card/j;->e()V

    .line 1776
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    invoke-interface {v0, v1}, Lcom/twitter/android/card/j;->a(Lcom/twitter/android/card/j$a;)V

    .line 1778
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v0}, Lcom/twitter/android/composer/j;->f()V

    .line 1780
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aC:Lrx/j;

    if-eqz v0, :cond_0

    .line 1781
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aC:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 1784
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aD:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aE:Lakv;

    if-eqz v0, :cond_1

    .line 1785
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aD:Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aE:Lakv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/monetization/MediaMonetizationSettingsActivity$c;->b(Lakv;)V

    .line 1787
    :cond_1
    return-void
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 3064
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/composer/ComposerActivity;->av:J

    .line 3065
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3066
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->c(I)V

    .line 3080
    :goto_0
    return-void

    .line 3068
    :cond_0
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/composer/ComposerActivity;->av:J

    .line 3069
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    .line 3071
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    .line 3072
    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$32;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/composer/ComposerActivity$32;-><init>(Lcom/twitter/android/composer/ComposerActivity;I)V

    .line 3069
    invoke-static {p0, v0, v1, v2}, Lcya;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcya$a;)V

    goto :goto_0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->d(Lank;)Lcom/twitter/android/composer/d;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->d(Lank;)Lcom/twitter/android/composer/d;

    move-result-object v0

    return-object v0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 1718
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->d(Z)V

    .line 1719
    return-void
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->d(Lank;)Lcom/twitter/android/composer/d;

    move-result-object v0

    return-object v0
.end method

.method h()V
    .locals 21

    .prologue
    .line 2251
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/composer/ComposerActivity;->V:Z

    .line 2253
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/composer/ComposerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 2254
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    .line 2255
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/composer/ComposerActivity;->i()Lcom/twitter/model/drafts/a;

    move-result-object v19

    .line 2256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->m()Lcom/twitter/model/core/r;

    move-result-object v20

    .line 2257
    if-eqz v20, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v11, 0x1

    .line 2258
    :goto_0
    if-eqz v20, :cond_3

    move-object/from16 v0, v20

    iget-wide v2, v0, Lcom/twitter/model/core/r;->b:J

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    const/4 v2, 0x1

    .line 2260
    :goto_1
    if-eqz v11, :cond_4

    .line 2262
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v3, v6}, Lcom/twitter/android/composer/f;->b(Lcom/twitter/library/client/Session;)V

    .line 2263
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/composer/ComposerActivity;->b:Lcom/twitter/android/composer/s;

    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/twitter/model/core/r;->e:J

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/model/core/r;->e:J

    move-object/from16 v0, v20

    iget-object v10, v0, Lcom/twitter/model/core/r;->m:Lcgi;

    invoke-interface/range {v3 .. v10}, Lcom/twitter/android/composer/s;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcgi;)Ljava/lang/String;

    .line 2276
    :goto_2
    invoke-static {}, Lcom/twitter/android/composer/q;->a()Lcom/twitter/android/composer/q;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/composer/q;->c()J

    move-result-wide v14

    .line 2277
    if-eqz v2, :cond_0

    .line 2279
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->i()Lcom/twitter/model/core/Tweet;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v12, 0x1

    move-object/from16 v0, v19

    iget-object v13, v0, Lcom/twitter/model/drafts/a;->g:Lcom/twitter/model/geo/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    .line 2281
    invoke-interface {v2}, Lcom/twitter/android/card/j;->d()Lcax;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    .line 2282
    invoke-interface {v2}, Lcom/twitter/android/card/j;->c()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v16, p0

    .line 2279
    invoke-virtual/range {v7 .. v18}, Lcom/twitter/android/composer/o;->a(ZLcom/twitter/model/core/Tweet;ZZZLcom/twitter/model/geo/c;JLandroid/content/Context;Lcax;Ljava/lang/String;)V

    .line 2284
    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->i()Lcom/twitter/model/core/Tweet;

    move-result-object v9

    const/4 v10, 0x0

    if-eqz v20, :cond_5

    const/4 v12, 0x1

    :goto_3
    move-object/from16 v0, v19

    iget-object v13, v0, Lcom/twitter/model/drafts/a;->g:Lcom/twitter/model/geo/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    .line 2286
    invoke-interface {v2}, Lcom/twitter/android/card/j;->d()Lcax;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    .line 2287
    invoke-interface {v2}, Lcom/twitter/android/card/j;->c()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v16, p0

    .line 2284
    invoke-virtual/range {v7 .. v18}, Lcom/twitter/android/composer/o;->a(ZLcom/twitter/model/core/Tweet;ZZZLcom/twitter/model/geo/c;JLandroid/content/Context;Lcax;Ljava/lang/String;)V

    .line 2288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v7

    invoke-virtual {v2, v4, v3, v6, v7}, Lcom/twitter/android/composer/o;->a(Landroid/content/Context;Ljava/util/List;ZZ)V

    .line 2289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/o;->a(Ljava/util/List;)V

    .line 2290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/twitter/model/drafts/a;->k:Lcau;

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/android/composer/o;->a(Lcau;Z)V

    .line 2291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v3}, Lcom/twitter/android/composer/f;->i()Lcom/twitter/model/core/Tweet;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v11}, Lcom/twitter/android/composer/o;->a(Lcom/twitter/model/drafts/a;Lcom/twitter/model/core/Tweet;Z)V

    .line 2293
    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    move-object/from16 v0, v19

    invoke-static {v2, v3, v0}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(JLcom/twitter/model/drafts/a;)V

    .line 2295
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/composer/ComposerActivity;->ai:I

    .line 2297
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/composer/ComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2298
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v4, "android.intent.extra.RETURN_RESULT"

    const-string/jumbo v5, "android.intent.extra.RETURN_RESULT"

    .line 2300
    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2299
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "extra_is_retweet"

    .line 2301
    invoke-virtual {v3, v4, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    .line 2302
    const/4 v4, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3}, Lcom/twitter/android/composer/ComposerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2304
    const-string/jumbo v3, "android.intent.extra.INTENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 2305
    if-eqz v2, :cond_1

    .line 2306
    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2307
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/ComposerActivity;->e(Landroid/content/Intent;)V

    .line 2310
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/twitter/android/composer/ComposerActivity;->d(Z)V

    .line 2311
    return-void

    .line 2257
    :cond_2
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 2258
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 2272
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v3}, Lcom/twitter/android/composer/f;->p()V

    .line 2273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/composer/ComposerActivity;->b:Lcom/twitter/android/composer/s;

    move-object/from16 v0, v19

    invoke-interface {v3, v4, v5, v0}, Lcom/twitter/android/composer/s;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)Ljava/lang/String;

    goto/16 :goto_2

    .line 2284
    :cond_5
    const/4 v12, 0x0

    goto/16 :goto_3
.end method

.method public i()Lcom/twitter/model/drafts/a;
    .locals 2

    .prologue
    .line 2315
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aw:Lcom/twitter/android/card/j;

    invoke-interface {v1}, Lcom/twitter/android/card/j;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->b(Ljava/lang/String;)V

    .line 2316
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v1}, Lcom/twitter/android/composer/j;->d()Lcom/twitter/model/geo/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/model/geo/c;)V

    .line 2317
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->b()Lcom/twitter/model/drafts/a;

    move-result-object v0

    return-object v0
.end method

.method j()V
    .locals 2

    .prologue
    .line 2321
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    invoke-virtual {v0}, Lsa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2322
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    const/16 v1, 0x202

    invoke-virtual {v0, v1}, Lsa;->a(I)V

    .line 2326
    :goto_0
    return-void

    .line 2324
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->h()V

    goto :goto_0
.end method

.method protected l()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2329
    iget-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->V:Z

    if-eqz v2, :cond_1

    .line 2338
    :cond_0
    :goto_0
    return v0

    .line 2332
    :cond_1
    iget-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    if-eqz v2, :cond_2

    move v0, v1

    .line 2333
    goto :goto_0

    .line 2335
    :cond_2
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2}, Lcom/twitter/android/composer/TweetBox;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 2336
    goto :goto_0

    .line 2338
    :cond_3
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->e()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2339
    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2340
    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    invoke-virtual {v2}, Lcom/twitter/android/card/pollcompose/d;->d()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method protected n()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2344
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->m()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2347
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->o()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2358
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-eq v0, v1, :cond_0

    .line 2360
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->i()V

    .line 2361
    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 2363
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 2015
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2016
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    new-instance v3, Lcom/twitter/android/composer/ComposerActivity$b;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-virtual {v2, p1, p2, p3, v3}, Lcom/twitter/android/media/selection/c;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 2018
    sparse-switch p1, :sswitch_data_0

    .line 2205
    :cond_0
    :goto_0
    return-void

    .line 2020
    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l;->a()V

    goto :goto_0

    .line 2024
    :sswitch_1
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2025
    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    goto :goto_0

    .line 2031
    :sswitch_2
    if-eq p2, v4, :cond_1

    .line 2032
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    if-eqz v0, :cond_0

    .line 2033
    iput v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    .line 2034
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->finish()V

    goto :goto_0

    .line 2037
    :cond_1
    iput v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    .line 2038
    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 2039
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->e:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$c;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/ComposerActivity$c;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2045
    :sswitch_3
    if-ne p2, v4, :cond_0

    .line 2046
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    if-eqz v0, :cond_0

    .line 2047
    if-eqz p3, :cond_2

    .line 2048
    const-string/jumbo v0, "extra_editable_image"

    .line 2049
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 2050
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    invoke-virtual {v1, v0}, Lcom/twitter/android/card/pollcompose/d;->a(Lcom/twitter/model/media/EditableImage;)V

    goto :goto_0

    .line 2052
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->az:Lcom/twitter/android/card/pollcompose/d;

    invoke-virtual {v0}, Lcom/twitter/android/card/pollcompose/d;->b()V

    goto :goto_0

    .line 2060
    :sswitch_4
    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    .line 2061
    const-string/jumbo v0, "photo_tags"

    sget-object v1, Lcom/twitter/model/core/n;->a:Lcom/twitter/util/serialization/l;

    .line 2064
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 2061
    invoke-static {p3, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2065
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2066
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    .line 2067
    instance-of v3, v1, Lcom/twitter/model/media/EditableImage;

    if-eqz v3, :cond_3

    .line 2068
    check-cast v1, Lcom/twitter/model/media/EditableImage;

    iput-object v0, v1, Lcom/twitter/model/media/EditableImage;->g:Ljava/util/List;

    goto :goto_1

    .line 2071
    :cond_4
    if-eqz v0, :cond_0

    .line 2072
    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->b(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2079
    :sswitch_5
    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    .line 2080
    const-string/jumbo v0, "alt_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2081
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    .line 2082
    iput-object v0, v1, Lcom/twitter/model/media/EditableImage;->i:Ljava/lang/String;

    .line 2083
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->m:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f()V

    goto/16 :goto_0

    .line 2089
    :sswitch_6
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 2090
    const-string/jumbo v0, "account"

    .line 2091
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 2090
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 2092
    iget-object v0, v0, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    .line 2093
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v0, v1}, Lcom/twitter/library/util/b;->a(J)Lakm;

    move-result-object v0

    .line 2092
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 2094
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2095
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ag:Lcom/twitter/android/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/b;->a(Lakm;)V

    goto/16 :goto_0

    .line 2102
    :sswitch_7
    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    .line 2103
    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    goto/16 :goto_0

    .line 2109
    :sswitch_8
    if-ne p2, v4, :cond_5

    if-eqz p3, :cond_5

    .line 2110
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->p()V

    .line 2111
    invoke-static {p3}, Lcom/twitter/app/drafts/b;->a(Landroid/content/Intent;)Lcom/twitter/app/drafts/b;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/app/drafts/b;->a:Lcom/twitter/model/drafts/a;

    .line 2112
    if-eqz v2, :cond_5

    .line 2113
    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    .line 2114
    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v3, v2}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/model/drafts/a;)V

    .line 2115
    invoke-direct {p0, v5, v5, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/net/Uri;[IZI)V

    .line 2118
    :cond_5
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->C()V

    .line 2119
    invoke-virtual {p0, v0, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    goto/16 :goto_0

    .line 2124
    :sswitch_9
    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    .line 2125
    const-string/jumbo v0, "extra_perm_result"

    .line 2126
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/android/PermissionResult;

    .line 2127
    invoke-virtual {v0}, Lcom/twitter/util/android/PermissionResult;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2128
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l;->b()V

    .line 2129
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->v()V

    goto/16 :goto_0

    .line 2131
    :cond_6
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->c:Landroid/widget/ImageButton;

    .line 2132
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/util/android/PermissionResult;->b:[Ljava/lang/String;

    invoke-virtual {v2, p0, v0}, Lcom/twitter/util/android/f;->b(Landroid/content/Context;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 2131
    invoke-static {p0, v1, v0}, Lcom/twitter/android/runtimepermissions/a;->a(Landroid/content/Context;Landroid/view/View;Ljava/util/Set;)V

    goto/16 :goto_0

    .line 2139
    :sswitch_a
    if-ne p2, v4, :cond_0

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2140
    invoke-static {p0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 2141
    invoke-virtual {v1}, Lcom/twitter/android/composer/a;->q()Landroid/net/Uri;

    move-result-object v1

    .line 2142
    if-eqz v1, :cond_0

    .line 2143
    invoke-static {}, Lcom/twitter/media/util/t;->a()Z

    move-result v2

    new-instance v3, Lcom/twitter/android/composer/ComposerActivity$b;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/net/Uri;ZLcom/twitter/android/media/selection/a;)V

    goto/16 :goto_0

    .line 2151
    :sswitch_b
    if-eqz p3, :cond_7

    .line 2152
    invoke-static {p3}, Lcom/twitter/android/util/j;->a(Landroid/content/Intent;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 2153
    if-eqz v0, :cond_7

    .line 2154
    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$b;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 2157
    :cond_7
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->a:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0, p3}, Lcom/twitter/android/util/j;->a(Lcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2162
    :sswitch_c
    if-ne p2, v4, :cond_8

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2163
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->c(I)V

    .line 2165
    :cond_8
    iput v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    .line 2166
    iput-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ap:Z

    goto/16 :goto_0

    .line 2171
    :sswitch_d
    const/16 v2, 0x20b

    if-ne p1, v2, :cond_9

    .line 2172
    :goto_2
    const/16 v1, 0xced

    if-ne p2, v1, :cond_a

    .line 2173
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/o;->a(Z)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 2171
    goto :goto_2

    .line 2174
    :cond_a
    const/16 v1, 0xcee

    if-ne p2, v1, :cond_0

    .line 2175
    invoke-virtual {p0, p3}, Lcom/twitter/android/composer/ComposerActivity;->startActivity(Landroid/content/Intent;)V

    .line 2176
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/o;->b(Z)V

    goto/16 :goto_0

    .line 2182
    :sswitch_e
    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    .line 2184
    invoke-static {p3}, Lcom/twitter/app/users/f;->a(Landroid/content/Intent;)Lcom/twitter/app/users/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/f;->e()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v0

    .line 2185
    if-eqz v0, :cond_b

    .line 2186
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v0, v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->b:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/f;->a(Ljava/util/List;)V

    .line 2188
    :cond_b
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ap()V

    .line 2189
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/composer/t;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/composer/t;->a(J)V

    goto/16 :goto_0

    .line 2195
    :sswitch_f
    if-ne p2, v4, :cond_0

    .line 2196
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->finish()V

    goto/16 :goto_0

    .line 2018
    nop

    :sswitch_data_0
    .sparse-switch
        0x101 -> :sswitch_1
        0x102 -> :sswitch_2
        0x103 -> :sswitch_0
        0x104 -> :sswitch_7
        0x201 -> :sswitch_4
        0x202 -> :sswitch_6
        0x203 -> :sswitch_8
        0x205 -> :sswitch_c
        0x206 -> :sswitch_9
        0x207 -> :sswitch_a
        0x208 -> :sswitch_b
        0x209 -> :sswitch_5
        0x20a -> :sswitch_d
        0x20b -> :sswitch_d
        0x20d -> :sswitch_e
        0x20e -> :sswitch_f
        0x301 -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1969
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    invoke-virtual {v2}, Lcom/twitter/android/composer/g;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1993
    :goto_0
    return-void

    .line 1972
    :cond_0
    iget-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    :cond_1
    move v2, v1

    .line 1974
    :goto_1
    if-eqz v2, :cond_4

    .line 1975
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1976
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->n()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->e(Z)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 1972
    goto :goto_1

    .line 1978
    :cond_3
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 1981
    :cond_4
    iget v2, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    if-ne v2, v4, :cond_6

    .line 1982
    invoke-static {}, Lbpo;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1983
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l;->c()V

    goto :goto_0

    .line 1985
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l;->d()V

    goto :goto_0

    .line 1988
    :cond_6
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v2

    if-ne v2, v4, :cond_7

    move v0, v1

    .line 1990
    :cond_7
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2717
    sparse-switch p1, :sswitch_data_0

    .line 2727
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 2719
    :sswitch_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->c(I)V

    goto :goto_0

    .line 2723
    :sswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->c(I)V

    goto :goto_0

    .line 2717
    :sswitch_data_0
    .sparse-switch
        0x1b -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1723
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->U:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->d:Lcom/twitter/android/composer/ComposerActivity$e;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 1724
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aa:Z

    .line 1725
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1726
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    const v1, 0x7f130279

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1727
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1728
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1730
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPause()V

    .line 1731
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1535
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 1536
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->A:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1537
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 2210
    packed-switch p1, :pswitch_data_0

    .line 2219
    :goto_0
    return-void

    .line 2212
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/composer/j;->a([Ljava/lang/String;[I)V

    goto :goto_0

    .line 2210
    :pswitch_data_0
    .packed-switch 0x204
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1641
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 1642
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->U:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->d:Lcom/twitter/android/composer/ComposerActivity$e;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 1646
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ap:Z

    if-nez v0, :cond_1

    .line 1647
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ae:Lcom/twitter/app/common/util/b$a;

    if-nez v0, :cond_0

    .line 1648
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    .line 1649
    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$27;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/composer/ComposerActivity$27;-><init>(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/app/common/util/b;)V

    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ae:Lcom/twitter/app/common/util/b$a;

    .line 1666
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ae:Lcom/twitter/app/common/util/b$a;

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/util/b;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 1668
    :cond_0
    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->c(I)V

    .line 1671
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ai()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1672
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->finish()V

    .line 1674
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;)V

    .line 1675
    return-void
.end method

.method protected onResumeFragments()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v5, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1605
    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aa:Z

    .line 1606
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResumeFragments()V

    .line 1607
    iget-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    .line 1608
    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    if-eq v3, v5, :cond_1

    .line 1609
    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    if-ne v3, v6, :cond_0

    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->S:I

    if-ne v3, v0, :cond_0

    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 1610
    invoke-virtual {v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1611
    :cond_0
    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    if-ne v3, v4, :cond_3

    .line 1612
    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->b(Z)V

    .line 1623
    :goto_0
    iput v5, p0, Lcom/twitter/android/composer/ComposerActivity;->S:I

    .line 1624
    iput v5, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    .line 1627
    :cond_1
    iput-boolean v2, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    .line 1628
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->hasFocus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->c(Z)V

    .line 1630
    const-string/jumbo v0, "composer:complete"

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->O()Lcom/twitter/metrics/j;

    move-result-object v1

    sget-object v2, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-static {v0, v1, v2}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 1631
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->j()V

    .line 1633
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->g:Lcom/twitter/android/composer/g;

    invoke-virtual {v0}, Lcom/twitter/android/composer/g;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1635
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, v6}, Lcom/twitter/android/composer/TweetBox;->setVisibility(I)V

    .line 1637
    :cond_2
    return-void

    .line 1615
    :cond_3
    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->S:I

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    .line 1618
    :cond_4
    :goto_1
    iget v3, p0, Lcom/twitter/android/composer/ComposerActivity;->R:I

    invoke-virtual {p0, v3, v1, v0}, Lcom/twitter/android/composer/ComposerActivity;->a(IZZ)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1615
    goto :goto_1

    .line 1621
    :cond_6
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1791
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1792
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ar()V

    .line 1793
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/f;->b(Landroid/os/Bundle;)V

    .line 1794
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Lcom/twitter/android/composer/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/l;->a(Landroid/os/Bundle;)V

    .line 1795
    const-string/jumbo v0, "original_owner_id"

    iget-wide v2, p0, Lcom/twitter/android/composer/ComposerActivity;->T:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1796
    const-string/jumbo v0, "loaded_from_draft"

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1797
    const-string/jumbo v0, "tweet_posted"

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->V:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1798
    const-string/jumbo v0, "conversation_card_data"

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ax:Lcax;

    sget-object v2, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 1800
    const-string/jumbo v0, "current_drawer"

    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->Q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1801
    const-string/jumbo v0, "current_drawer_position"

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1802
    const-string/jumbo v0, "show_link_hint"

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->Z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1803
    const-string/jumbo v0, "launch_camera_mode"

    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ac:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1804
    const-string/jumbo v0, "selection"

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->getSelection()[I

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1805
    const-string/jumbo v0, "edited_text"

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->at:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1806
    const-string/jumbo v0, "reached_limit"

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->au:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1807
    const-string/jumbo v0, "show_full_screen_suggestions"

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->aq:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1808
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/r;->a(Landroid/os/Bundle;)V

    .line 1809
    const-string/jumbo v0, "requesting_camera_permissions"

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ap:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1810
    const-string/jumbo v0, "media_rail"

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Lsc;

    invoke-virtual {v1}, Lsc;->c()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1812
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aB:Lsa;

    invoke-virtual {v0, p1}, Lsa;->a(Landroid/os/Bundle;)V

    .line 1813
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/j;->a(Landroid/os/Bundle;)V

    .line 1814
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 1735
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 1736
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->V:Z

    if-eqz v0, :cond_1

    .line 1761
    :cond_0
    :goto_0
    return-void

    .line 1740
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1745
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1750
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->p()V

    goto :goto_0

    .line 1754
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1756
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 1759
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->b(Lcom/twitter/library/client/Session;)V

    goto :goto_0
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 1818
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1819
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->n()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->e(Z)V

    .line 1825
    :goto_0
    return-void

    .line 1821
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->setResult(I)V

    .line 1822
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ab:Z

    .line 1823
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->p()V

    goto :goto_0
.end method

.method q()V
    .locals 3

    .prologue
    .line 2645
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2646
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2647
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 2648
    instance-of v1, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v1, :cond_0

    .line 2649
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/MediaTagActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "editable_image"

    .line 2651
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x201

    .line 2649
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2653
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->j()V

    .line 2655
    :cond_0
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 2711
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->ah:Z

    .line 2712
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ao()V

    .line 2713
    return-void
.end method

.method s()V
    .locals 3

    .prologue
    .line 2733
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "AccountsDialogActivity_account_name"

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    .line 2735
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "AccountsDialogActivity_switch_only"

    const/4 v2, 0x1

    .line 2736
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x202

    .line 2733
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2738
    return-void
.end method

.method t()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2742
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    .line 2743
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 2744
    if-ne v1, v5, :cond_1

    .line 2745
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 2746
    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne v0, v1, :cond_0

    .line 2747
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, ":composition::photo:send_photo_tweet"

    aput-object v2, v1, v4

    .line 2748
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2747
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2755
    :cond_0
    :goto_0
    invoke-virtual {p0, v4}, Lcom/twitter/android/composer/ComposerActivity;->a(Z)V

    .line 2756
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->j()V

    .line 2757
    return-void

    .line 2750
    :cond_1
    if-le v1, v5, :cond_0

    .line 2751
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->P:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, ":composition::multiphoto:send_photo_tweet"

    aput-object v3, v2, v4

    .line 2752
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    int-to-long v2, v1

    .line 2753
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2751
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method u()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2832
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->aj:Lcom/twitter/android/composer/r;

    invoke-virtual {v0}, Lcom/twitter/android/composer/r;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2833
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 2834
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 2839
    :goto_0
    return-void

    .line 2836
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->E:Lcom/twitter/ui/widget/TwitterButton;

    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->ak:I

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 2837
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Lcom/twitter/ui/widget/TwitterButton;

    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->al:I

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0
.end method

.method v()V
    .locals 1

    .prologue
    .line 2896
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->b(Z)V

    .line 2897
    return-void
.end method

.method w()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2934
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2944
    :goto_0
    return-void

    .line 2937
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->k:Lcom/twitter/android/composer/TweetBox;

    .line 2938
    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->W:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->X:Z

    if-eqz v0, :cond_1

    .line 2941
    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    .line 2942
    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 2943
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->q:Landroid/view/View;

    if-eqz v0, :cond_2

    const/4 v1, 0x4

    :cond_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2942
    goto :goto_1
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2973
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->j:Lcom/twitter/android/composer/j;

    invoke-virtual {v0}, Lcom/twitter/android/composer/j;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public y()V
    .locals 1

    .prologue
    .line 3084
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->O:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->b()V

    .line 3085
    return-void
.end method
