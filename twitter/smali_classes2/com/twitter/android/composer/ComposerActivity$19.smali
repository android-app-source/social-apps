.class Lcom/twitter/android/composer/ComposerActivity$19;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->a(Landroid/net/Uri;[IZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 1265
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Lcqy;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v1, v1, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->a()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/composer/TweetBox;->a(Lcom/twitter/model/core/Tweet;Z)V

    .line 1269
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v1, v1, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1}, Lcom/twitter/android/composer/f;->h()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setExcludedRecipientIds(Ljava/util/Collection;)V

    .line 1270
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->c(Lcom/twitter/android/composer/ComposerActivity;Z)V

    .line 1271
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->q(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1272
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->l(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/t;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v1}, Lcom/twitter/android/composer/ComposerActivity;->c(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/composer/t;->a(J)V

    .line 1273
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->f(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;)V

    .line 1274
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$19;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->r(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1275
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1265
    check-cast p1, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/ComposerActivity$19;->a(Lcom/twitter/model/core/Tweet;)V

    return-void
.end method
