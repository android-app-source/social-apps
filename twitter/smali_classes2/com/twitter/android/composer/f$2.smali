.class Lcom/twitter/android/composer/f$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/f;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Landroid/database/Cursor;",
        "Lcom/twitter/model/core/Tweet;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/f;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/f;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/twitter/android/composer/f$2;->a:Lcom/twitter/android/composer/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 184
    if-eqz p1, :cond_1

    .line 186
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    sget-object v0, Lbtd;->a:Lbtd;

    invoke-virtual {v0, p1}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 190
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 193
    :goto_0
    return-object v0

    .line 190
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 193
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/f$2;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    return-object v0
.end method
