.class public Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;
.super Lcom/twitter/android/widget/GalleryGridHeaderIconView;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f04010d

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/widget/GalleryGridHeaderIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 31
    const v0, 0x7f1303a5

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a:Landroid/widget/ImageView;

    .line 32
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 35
    invoke-static {}, Lbpo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/d;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a:Landroid/widget/ImageView;

    const v1, 0x7f020375

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 38
    iget-object v0, p0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 43
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a:Landroid/widget/ImageView;

    const v1, 0x7f020376

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 41
    iget-object v0, p0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a:Landroid/widget/ImageView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 48
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 53
    invoke-static {p1}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbpq;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a()V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->b()V

    goto :goto_0
.end method
