.class Lcom/twitter/android/composer/f$a;
.super Lcom/twitter/library/service/w;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/composer/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/w",
        "<",
        "Ljava/lang/Void;",
        "Lcom/twitter/android/composer/y;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/f;

.field private final b:Lcom/twitter/library/client/Session;

.field private c:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/f;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/twitter/android/composer/f$a;->a:Lcom/twitter/android/composer/f;

    invoke-direct {p0}, Lcom/twitter/library/service/w;-><init>()V

    .line 408
    iput-object p2, p0, Lcom/twitter/android/composer/f$a;->b:Lcom/twitter/library/client/Session;

    .line 409
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/f$a;)Z
    .locals 1

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/twitter/android/composer/f$a;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/composer/f$a;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/twitter/android/composer/f$a;->b:Lcom/twitter/library/client/Session;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/f$a;->c:Z

    .line 413
    return-void
.end method

.method public a(Lcom/twitter/android/composer/y;)V
    .locals 3

    .prologue
    .line 417
    invoke-virtual {p1}, Lcom/twitter/android/composer/y;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 424
    if-nez v0, :cond_0

    .line 441
    :goto_0
    return-void

    .line 427
    :cond_0
    sget-object v1, Lcom/twitter/util/concurrent/f;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/twitter/android/composer/f$a$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/composer/f$a$1;-><init>(Lcom/twitter/android/composer/f$a;Ljava/lang/Long;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 403
    check-cast p1, Lcom/twitter/android/composer/y;

    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/f$a;->a(Lcom/twitter/android/composer/y;)V

    return-void
.end method
