.class Lcom/twitter/android/composer/ComposerActivity$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbxk$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/composer/ComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 3236
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$d;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/android/composer/ComposerActivity$1;)V
    .locals 0

    .prologue
    .line 3236
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity$d;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    return-void
.end method


# virtual methods
.method public a([JLjava/util/List;JJJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;JJJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 3240
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$d;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->p()V

    .line 3241
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$d;->a:Lcom/twitter/android/composer/ComposerActivity;

    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    .line 3243
    invoke-virtual {v0, p7, p8}, Lcom/twitter/app/users/f;->c(J)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 3244
    invoke-virtual {v0, p1}, Lcom/twitter/app/users/f;->a([J)Lcom/twitter/app/users/f;

    move-result-object v0

    const/16 v2, 0x2a

    .line 3245
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 3246
    invoke-virtual {v0, p3, p4}, Lcom/twitter/app/users/f;->b(J)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 3247
    invoke-virtual {v0, v6}, Lcom/twitter/app/users/f;->b(Z)Lcom/twitter/app/users/f;

    move-result-object v2

    .line 3248
    invoke-static {}, Lbpj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    new-array v3, v6, [Ljava/lang/Long;

    const/4 v4, 0x0

    .line 3251
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/twitter/util/collection/ImmutableList;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v6, p2, v3}, Lcom/twitter/android/UsersAdapter$CheckboxConfig;-><init>(ZLjava/util/List;Ljava/util/List;)V

    .line 3248
    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/app/users/f;->a(Lcom/twitter/android/UsersAdapter$CheckboxConfig;)Lcom/twitter/app/users/f;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$d;->a:Lcom/twitter/android/composer/ComposerActivity;

    .line 3252
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0x20d

    .line 3241
    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/composer/ComposerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3254
    return-void

    .line 3251
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
