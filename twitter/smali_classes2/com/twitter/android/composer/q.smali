.class public Lcom/twitter/android/composer/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/android/b$a;


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile b:Lcom/twitter/android/composer/q;


# instance fields
.field private c:J

.field private d:J

.field private e:Z

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/twitter/android/composer/ComposerActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/twitter/android/MediaTagActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/twitter/android/media/camera/CameraActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/twitter/android/media/imageeditor/EditImageActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/twitter/android/VideoEditorActivity;

    aput-object v2, v0, v1

    .line 23
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/composer/q;->a:Ljava/util/List;

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/twitter/android/composer/q;
    .locals 3

    .prologue
    .line 40
    sget-object v0, Lcom/twitter/android/composer/q;->b:Lcom/twitter/android/composer/q;

    if-nez v0, :cond_1

    .line 41
    const-class v1, Lcom/twitter/android/composer/q;

    monitor-enter v1

    .line 42
    :try_start_0
    sget-object v0, Lcom/twitter/android/composer/q;->b:Lcom/twitter/android/composer/q;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/twitter/android/composer/q;

    invoke-direct {v0}, Lcom/twitter/android/composer/q;-><init>()V

    sput-object v0, Lcom/twitter/android/composer/q;->b:Lcom/twitter/android/composer/q;

    .line 44
    const-class v0, Lcom/twitter/android/composer/q;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 45
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/composer/q$1;

    invoke-direct {v2}, Lcom/twitter/android/composer/q$1;-><init>()V

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/util/b;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 52
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    sget-object v2, Lcom/twitter/android/composer/q;->b:Lcom/twitter/android/composer/q;

    invoke-virtual {v0, v2}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 54
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_1
    sget-object v0, Lcom/twitter/android/composer/q;->b:Lcom/twitter/android/composer/q;

    return-object v0

    .line 54
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic d()Lcom/twitter/android/composer/q;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/twitter/android/composer/q;->b:Lcom/twitter/android/composer/q;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/twitter/android/composer/q;->f:Z

    if-eqz v0, :cond_0

    .line 101
    iget-boolean v0, p0, Lcom/twitter/android/composer/q;->e:Z

    if-eqz v0, :cond_1

    .line 102
    iget-wide v0, p0, Lcom/twitter/android/composer/q;->c:J

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/android/composer/q;->d:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/android/composer/q;->c:J

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/q;->c()J

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/composer/q;->c:J

    .line 75
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/composer/q;->d:J

    .line 76
    iput-boolean p1, p0, Lcom/twitter/android/composer/q;->e:Z

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/q;->f:Z

    .line 78
    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/twitter/android/composer/q;->f:Z

    if-eqz v0, :cond_0

    .line 112
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/composer/q;->d:J

    .line 114
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/twitter/android/composer/q;->f:Z

    return v0
.end method

.method public c()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 86
    iget-boolean v0, p0, Lcom/twitter/android/composer/q;->f:Z

    if-eqz v0, :cond_0

    .line 87
    iget-wide v0, p0, Lcom/twitter/android/composer/q;->c:J

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/twitter/android/composer/q;->d:J

    sub-long/2addr v0, v2

    .line 88
    iput-wide v6, p0, Lcom/twitter/android/composer/q;->c:J

    .line 89
    iput-wide v6, p0, Lcom/twitter/android/composer/q;->d:J

    .line 90
    iput-boolean v4, p0, Lcom/twitter/android/composer/q;->e:Z

    .line 91
    iput-boolean v4, p0, Lcom/twitter/android/composer/q;->f:Z

    .line 95
    :goto_0
    return-wide v0

    .line 93
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method protected c(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/twitter/android/composer/q;->f:Z

    if-eqz v0, :cond_1

    .line 118
    const/4 v1, 0x0

    .line 119
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 120
    sget-object v0, Lcom/twitter/android/composer/q;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 121
    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    const/4 v0, 0x1

    .line 126
    :goto_0
    if-nez v0, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/composer/q;->c()J

    .line 130
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method
