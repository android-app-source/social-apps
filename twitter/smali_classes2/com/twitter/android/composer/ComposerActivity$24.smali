.class Lcom/twitter/android/composer/ComposerActivity$24;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/card/pollcompose/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->ab()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 1375
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1378
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v1}, Lcom/twitter/android/composer/ComposerActivity;->t(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/card/pollcompose/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/card/pollcompose/d;->e()Lcau;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcau;)V

    .line 1379
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->h()V

    .line 1380
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->s(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1381
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->r(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1382
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0, v2}, Lcom/twitter/android/composer/ComposerActivity;->c(Lcom/twitter/android/composer/ComposerActivity;Z)V

    .line 1383
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->b(Lcom/twitter/android/composer/ComposerActivity;)Lsc;

    move-result-object v0

    invoke-virtual {v0, v2}, Lsc;->a(Z)V

    .line 1384
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->b(Lcom/twitter/android/composer/ComposerActivity;)Lsc;

    move-result-object v0

    invoke-virtual {v0}, Lsc;->j()V

    .line 1385
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/CharSequence;I)V

    .line 1386
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->u(Lcom/twitter/android/composer/ComposerActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1387
    return-void
.end method

.method public a(Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/model/drafts/DraftAttachment;

    .line 1410
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1391
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v1}, Lcom/twitter/android/composer/ComposerActivity;->t(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/card/pollcompose/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/card/pollcompose/d;->e()Lcau;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcau;)V

    .line 1392
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->r(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1393
    return-void
.end method

.method public b(Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 2

    .prologue
    .line 1414
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    iget-object v1, p1, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Landroid/net/Uri;)Lcom/twitter/model/drafts/DraftAttachment;

    .line 1415
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1397
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcau;)V

    .line 1398
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(I)V

    .line 1399
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->i()V

    .line 1400
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->v(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1401
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->s(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1402
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->r(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1403
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->u(Lcom/twitter/android/composer/ComposerActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1404
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$24;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->requestFocus()Z

    .line 1405
    return-void
.end method
