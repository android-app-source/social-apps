.class Lcom/twitter/android/composer/ComposerActivity$36;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/l$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/library/client/Session;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 702
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 719
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->d(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/selection/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$b;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V

    .line 722
    return-void
.end method

.method public a(Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 711
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$b;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-static {v0, p1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 712
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 713
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 715
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 726
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->d(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/selection/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->e()V

    .line 727
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 728
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->d(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/selection/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$b;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/selection/c;->a(Ljava/util/List;Lcom/twitter/android/media/selection/a;)V

    .line 732
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 705
    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 706
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$36;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->h(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/widget/MediaAttachmentsView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->d()V

    .line 707
    return-void

    :cond_0
    move v0, v1

    .line 705
    goto :goto_0
.end method
