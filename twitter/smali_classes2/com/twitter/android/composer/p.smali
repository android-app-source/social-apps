.class public Lcom/twitter/android/composer/p;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(JLcom/twitter/android/composer/ComposerType;Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 2

    .prologue
    .line 150
    if-nez p3, :cond_0

    .line 152
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 150
    :goto_0
    invoke-static {p0, p1, p2, v0}, Lcom/twitter/android/composer/p;->a(JLcom/twitter/android/composer/ComposerType;Ljava/util/List;)V

    .line 154
    return-void

    .line 153
    :cond_0
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(JLcom/twitter/android/composer/ComposerType;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/twitter/android/composer/ComposerType;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 86
    iget-object v2, p2, Lcom/twitter/android/composer/ComposerType;->scribeName:Ljava/lang/String;

    .line 89
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 131
    const-string/jumbo v1, "local"

    .line 132
    const-string/jumbo v0, "send_4_photo_tweet"

    .line 136
    :goto_0
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, ""

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v2, v4, v5

    const-string/jumbo v2, "tweet"

    aput-object v2, v4, v7

    const/4 v2, 0x3

    aput-object v1, v4, v2

    const/4 v1, 0x4

    aput-object v0, v4, v1

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 137
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/drafts/DraftAttachment;

    .line 138
    invoke-virtual {v1, v7}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_0

    .line 140
    new-instance v3, Lcom/twitter/library/scribe/ScribeItemSendMedia;

    invoke-direct {v3, v1}, Lcom/twitter/library/scribe/ScribeItemSendMedia;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 91
    :pswitch_0
    const-string/jumbo v1, ""

    .line 92
    const-string/jumbo v0, "send"

    goto :goto_0

    .line 96
    :pswitch_1
    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 97
    iget-object v1, v0, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    invoke-virtual {v1}, Lcom/twitter/model/media/MediaSource;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "remote"

    .line 99
    :goto_2
    sget-object v3, Lcom/twitter/android/composer/p$1;->a:[I

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_1

    .line 114
    const-string/jumbo v0, "send"

    goto :goto_0

    .line 97
    :cond_1
    const-string/jumbo v1, "local"

    goto :goto_2

    .line 101
    :pswitch_2
    const-string/jumbo v0, "send_1_photo_tweet"

    goto :goto_0

    .line 106
    :pswitch_3
    const-string/jumbo v0, "send_video_tweet"

    goto :goto_0

    .line 110
    :pswitch_4
    const-string/jumbo v0, "send_gif_tweet"

    goto :goto_0

    .line 121
    :pswitch_5
    const-string/jumbo v1, "local"

    .line 122
    const-string/jumbo v0, "send_2_photo_tweet"

    goto :goto_0

    .line 126
    :pswitch_6
    const-string/jumbo v1, "local"

    .line 127
    const-string/jumbo v0, "send_3_photo_tweet"

    goto/16 :goto_0

    .line 143
    :cond_2
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 144
    return-void

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 99
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 186
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "recipient_list"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 187
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 186
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 188
    return-void
.end method

.method public static a(Lcom/twitter/analytics/model/ScribeItem;JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 179
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 180
    invoke-virtual {v0, p0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "mentions"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "edited"

    aput-object v3, v1, v2

    .line 181
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 179
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 182
    return-void
.end method

.method public static a(Lcom/twitter/model/drafts/a;Lcom/twitter/model/core/Tweet;JZ)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 158
    if-eqz p1, :cond_2

    if-nez p4, :cond_2

    .line 159
    iget-boolean v2, p0, Lcom/twitter/model/drafts/a;->f:Z

    if-eqz v2, :cond_1

    .line 160
    iget-object v2, p0, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    iget-object v2, p0, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    .line 165
    if-eqz v2, :cond_2

    .line 167
    invoke-static {p1, p2, p3}, Lbxd;->c(Lcom/twitter/model/core/Tweet;J)Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lbxd;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 166
    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    .line 168
    :goto_1
    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    .line 174
    goto :goto_0

    :cond_3
    move v2, v1

    .line 166
    goto :goto_1
.end method
