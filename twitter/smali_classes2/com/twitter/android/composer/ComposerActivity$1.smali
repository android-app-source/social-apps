.class Lcom/twitter/android/composer/ComposerActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lse$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/composer/ComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 357
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v2}, Lcom/twitter/android/composer/ComposerActivity;->c(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/android/composer/o;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 358
    packed-switch p1, :pswitch_data_0

    .line 378
    :goto_0
    return-void

    .line 360
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/ComposerActivity;->c(I)V

    goto :goto_0

    .line 364
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->c(I)V

    goto :goto_0

    .line 368
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/16 v1, 0x20b

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->d(I)V

    goto :goto_0

    .line 372
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/ComposerActivity;->b(Z)V

    goto :goto_0

    .line 358
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/twitter/media/model/b;Lcom/twitter/model/media/EditableMedia;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 345
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->q()V

    .line 346
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v0, v0, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {p2}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/media/model/MediaType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    new-instance v1, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v1, p2}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    new-instance v2, Lcom/twitter/android/composer/ComposerActivity$b;

    iget-object v3, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {v2, v3, v5}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-static {v0, v1, v2}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 348
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0, v4}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;Z)Z

    .line 349
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->b(Lcom/twitter/android/composer/ComposerActivity;)Lsc;

    move-result-object v0

    invoke-virtual {v0, v5}, Lsc;->a(Z)V

    .line 350
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->b(Lcom/twitter/android/composer/ComposerActivity;)Lsc;

    move-result-object v0

    invoke-virtual {v0}, Lsc;->j()V

    .line 351
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$1;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/composer/ComposerActivity;->a(Z)V

    .line 353
    :cond_0
    return-void
.end method
