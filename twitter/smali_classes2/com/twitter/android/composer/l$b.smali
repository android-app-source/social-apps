.class Lcom/twitter/android/composer/l$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/selection/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/composer/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/l;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method private constructor <init>(Lcom/twitter/android/composer/l;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/composer/l;Lcom/twitter/android/composer/l$1;)V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/l$b;-><init>(Lcom/twitter/android/composer/l;)V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 398
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->g(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v0

    .line 400
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    .line 401
    iget-object v1, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 403
    :cond_0
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    .line 407
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 366
    invoke-direct {p0}, Lcom/twitter/android/composer/l$b;->e()V

    .line 367
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/composer/l$b;->c:Z

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/l$a;->a(Z)V

    .line 370
    :cond_0
    return-void
.end method

.method a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 390
    const-string/jumbo v0, "attachments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    .line 391
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 3

    .prologue
    .line 341
    invoke-direct {p0}, Lcom/twitter/android/composer/l$b;->d()V

    .line 342
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/l$b;->c:Z

    .line 343
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v0

    .line 344
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    sget-object v2, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    if-ne v1, v2, :cond_1

    .line 345
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/twitter/android/composer/l;->a(Lcom/twitter/android/composer/l;Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/l$b;->a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v1, p1}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/l$a;->a(Lcom/twitter/model/drafts/DraftAttachment;)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Z
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->g(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/f;->c(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->g(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/media/model/MediaType;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/l$a;->a(Ljava/util/List;)V

    .line 378
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/composer/l$b;->e()V

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 381
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/composer/l$b;->c:Z

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/l$a;->a(Z)V

    .line 383
    :cond_2
    return-void
.end method

.method b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 394
    const-string/jumbo v0, "attachments"

    iget-object v1, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 395
    return-void
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;)V
    .locals 2

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/twitter/android/composer/l$b;->d()V

    .line 356
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v0

    .line 357
    iget-object v1, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v1}, Lcom/twitter/android/composer/l;->g(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/f;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 358
    iget-object v1, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v1}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/twitter/android/composer/l$b;->a:Lcom/twitter/android/composer/l;

    invoke-static {v1}, Lcom/twitter/android/composer/l;->f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/twitter/android/composer/l$a;->a(Landroid/net/Uri;)V

    .line 362
    :cond_0
    return-void
.end method

.method c()Z
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/twitter/android/composer/l$b;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
