.class public Lcom/twitter/android/composer/x;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;ZLandroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/twitter/android/composer/x;->a:Landroid/content/res/Resources;

    .line 41
    const v0, 0x7f1100c9

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/composer/x;->b:I

    .line 42
    iput-boolean p2, p0, Lcom/twitter/android/composer/x;->e:Z

    .line 43
    iput-object p3, p0, Lcom/twitter/android/composer/x;->c:Landroid/widget/TextView;

    .line 44
    iput-object p4, p0, Lcom/twitter/android/composer/x;->d:Landroid/widget/TextView;

    .line 45
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Z)V
    .locals 6

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/composer/x;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 77
    if-eqz p2, :cond_1

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    move-object v1, v0

    .line 80
    :goto_0
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a01f4

    .line 82
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/composer/x;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/composer/x;->a:Landroid/content/res/Resources;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    .line 83
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->f()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 82
    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_0
    return-void

    .line 77
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 80
    :cond_2
    const v0, 0x7f0a01f5

    goto :goto_1
.end method

.method private b(Lcom/twitter/model/core/Tweet;JZLjava/util/List;Lbxk$a;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "JZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lbxk$a;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 57
    if-eqz p4, :cond_1

    .line 58
    iget-object v0, p0, Lcom/twitter/android/composer/x;->a:Landroid/content/res/Resources;

    iget v6, p0, Lcom/twitter/android/composer/x;->b:I

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p5

    move-object/from16 v5, p6

    invoke-static/range {v0 .. v6}, Lbxl;->a(Landroid/content/res/Resources;Lcom/twitter/model/core/Tweet;JLjava/util/List;Lbxk$a;I)Lbxi;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/twitter/android/composer/x;->c:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 61
    iget-object v1, p0, Lcom/twitter/android/composer/x;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v1, p0, Lcom/twitter/android/composer/x;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move v0, v7

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    :goto_1
    return-void

    :cond_0
    move v0, v8

    .line 62
    goto :goto_0

    .line 64
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/composer/x;->e:Z

    if-nez v0, :cond_3

    .line 65
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    .line 67
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/composer/x;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/composer/x;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a01f6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/composer/x;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 65
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 70
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/composer/x;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;JZLjava/util/List;Lbxk$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "JZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lbxk$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/composer/x;->b(Lcom/twitter/model/core/Tweet;JZLjava/util/List;Lbxk$a;)V

    .line 51
    invoke-direct {p0, p1, p4}, Lcom/twitter/android/composer/x;->a(Lcom/twitter/model/core/Tweet;Z)V

    .line 52
    return-void
.end method
