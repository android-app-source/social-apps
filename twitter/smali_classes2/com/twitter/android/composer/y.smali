.class public Lcom/twitter/android/composer/y;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lcom/twitter/model/drafts/a;

.field private final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;Z)V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/twitter/android/composer/y;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 38
    iput-object p1, p0, Lcom/twitter/android/composer/y;->a:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/twitter/android/composer/y;->b:Lcom/twitter/library/client/Session;

    .line 40
    iput-object p3, p0, Lcom/twitter/android/composer/y;->c:Lcom/twitter/model/drafts/a;

    .line 41
    iput-boolean p4, p0, Lcom/twitter/android/composer/y;->g:Z

    .line 42
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/composer/y;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v1

    .line 47
    new-instance v2, Laut;

    iget-object v0, p0, Lcom/twitter/android/composer/y;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v2, v0}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 48
    iget-object v3, p0, Lcom/twitter/android/composer/y;->c:Lcom/twitter/model/drafts/a;

    iget-boolean v0, p0, Lcom/twitter/android/composer/y;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v3, v0, v2}, Lcom/twitter/library/provider/h;->a(Lcom/twitter/model/drafts/a;ILaut;)J

    move-result-wide v0

    .line 51
    invoke-virtual {v2}, Laut;->a()V

    .line 52
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "SaveDraftOperation.cancel is not supported!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/android/composer/y;->b()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/android/composer/y;->a()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
