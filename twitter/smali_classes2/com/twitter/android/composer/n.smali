.class public Lcom/twitter/android/composer/n;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/n$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/android/composer/n;


# instance fields
.field public final b:Lcom/twitter/analytics/model/ScribeItem;

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/twitter/android/composer/n$a;

    invoke-direct {v0}, Lcom/twitter/android/composer/n$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/android/composer/n$a;->a()Lcom/twitter/android/composer/n;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/composer/n;->a:Lcom/twitter/android/composer/n;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/android/composer/n$a;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/twitter/android/composer/n$a;->a(Lcom/twitter/android/composer/n$a;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/n;->b:Lcom/twitter/analytics/model/ScribeItem;

    .line 29
    invoke-static {p1}, Lcom/twitter/android/composer/n$a;->b(Lcom/twitter/android/composer/n$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/n;->c:Ljava/lang/String;

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/composer/n$a;Lcom/twitter/android/composer/n$1;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/n;-><init>(Lcom/twitter/android/composer/n$a;)V

    return-void
.end method
