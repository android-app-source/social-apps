.class public Lcom/twitter/android/composer/b;
.super Lcom/twitter/android/e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/e",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/drafts/DraftAttachment;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lpv;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/twitter/android/e;-><init>(Lpv;)V

    .line 35
    return-void
.end method

.method private static b(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {}, Lcom/twitter/android/av/y;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-static {p0}, Lcom/twitter/android/composer/b;->c(Ljava/util/List;)Lcom/twitter/model/media/b;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/util/List;)Lcom/twitter/model/media/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)",
            "Lcom/twitter/model/media/b;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 108
    if-nez v0, :cond_0

    .line 113
    :goto_0
    return-object v1

    .line 112
    :cond_0
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 113
    instance-of v2, v0, Lcom/twitter/model/media/b;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/twitter/model/media/b;

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/util/List;)Lcom/twitter/model/av/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)",
            "Lcom/twitter/model/av/h;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/composer/b;->a()Lcom/twitter/model/av/h;

    move-result-object v1

    .line 83
    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-object v0

    .line 87
    :cond_1
    invoke-static {p1}, Lcom/twitter/android/composer/b;->c(Ljava/util/List;)Lcom/twitter/model/media/b;

    move-result-object v2

    .line 88
    if-eqz v2, :cond_0

    .line 93
    invoke-interface {v2}, Lcom/twitter/model/media/b;->k()Lcom/twitter/model/av/h;

    move-result-object v0

    .line 95
    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/client/Session;Ljava/util/List;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p2}, Lcom/twitter/android/composer/b;->b(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/b;->a:Lpv;

    .line 42
    invoke-virtual {v0, p1}, Lpv;->a(Lcom/twitter/library/client/Session;)Lrx/c;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/List;Lcom/twitter/model/av/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;",
            "Lcom/twitter/model/av/h;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/android/composer/b;->a()Lcom/twitter/model/av/h;

    move-result-object v0

    .line 58
    if-nez v0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-static {p1}, Lcom/twitter/android/composer/b;->c(Ljava/util/List;)Lcom/twitter/model/media/b;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 67
    invoke-interface {v0, p2}, Lcom/twitter/model/media/b;->a(Lcom/twitter/model/av/h;)V

    goto :goto_0
.end method

.method protected synthetic b(Lcom/twitter/library/client/Session;Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/b;->a(Lcom/twitter/library/client/Session;Ljava/util/List;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
