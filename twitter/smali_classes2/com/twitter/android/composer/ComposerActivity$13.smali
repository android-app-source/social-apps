.class Lcom/twitter/android/composer/ComposerActivity$13;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/ObservableScrollView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/library/client/Session;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/composer/ComposerScrollView;

.field final synthetic c:Lcom/twitter/android/composer/ComposerActivity;

.field private d:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;Landroid/view/View;Lcom/twitter/android/composer/ComposerScrollView;)V
    .locals 0

    .prologue
    .line 1036
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$13;->c:Lcom/twitter/android/composer/ComposerActivity;

    iput-object p2, p0, Lcom/twitter/android/composer/ComposerActivity$13;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/twitter/android/composer/ComposerActivity$13;->b:Lcom/twitter/android/composer/ComposerScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 1

    .prologue
    .line 1047
    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->b:Lcom/twitter/android/composer/ComposerScrollView;

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerScrollView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->c:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->f()V

    .line 1049
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->d:Z

    .line 1051
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->c:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->h(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/widget/MediaAttachmentsView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a()V

    .line 1052
    return-void
.end method

.method public a(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
    .locals 2

    .prologue
    .line 1041
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$13;->a:Landroid/view/View;

    if-nez p3, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1042
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->c:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->h(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/widget/MediaAttachmentsView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a()V

    .line 1043
    return-void

    .line 1041
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 1

    .prologue
    .line 1056
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->d:Z

    .line 1057
    return-void
.end method

.method public b(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
    .locals 2

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->c:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->e(Lcom/twitter/android/composer/ComposerActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->c:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->g(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->a()V

    .line 1064
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$13;->c:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->h(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/widget/MediaAttachmentsView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a()V

    .line 1065
    return-void
.end method
