.class Lcom/twitter/android/composer/ComposerActivity$27;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/common/util/b;

.field final synthetic b:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/app/common/util/b;)V
    .locals 0

    .prologue
    .line 1650
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$27;->b:Lcom/twitter/android/composer/ComposerActivity;

    iput-object p2, p0, Lcom/twitter/android/composer/ComposerActivity$27;->a:Lcom/twitter/app/common/util/b;

    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$27;->b:Lcom/twitter/android/composer/ComposerActivity;

    if-ne p1, v0, :cond_0

    .line 1661
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$27;->a:Lcom/twitter/app/common/util/b;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/b;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 1662
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$27;->b:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;Lcom/twitter/app/common/util/b$a;)Lcom/twitter/app/common/util/b$a;

    .line 1664
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1653
    instance-of v0, p1, Lcom/twitter/android/media/camera/CameraActivity;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1654
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$27;->b:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->y(Lcom/twitter/android/composer/ComposerActivity;)V

    .line 1656
    :cond_0
    return-void
.end method
