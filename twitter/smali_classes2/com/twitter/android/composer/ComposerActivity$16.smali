.class Lcom/twitter/android/composer/ComposerActivity$16;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/MediaAttachmentsView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/library/client/Session;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/model/media/EditableImage;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1137
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 1138
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Z)V

    .line 1139
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 1140
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Z)V

    .line 1141
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->k(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    .line 1142
    invoke-static {v1}, Lcom/twitter/android/composer/ComposerActivity;->h(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/widget/MediaAttachmentsView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    .line 1141
    invoke-virtual {v0, v1, p2, p1, p3}, Lcom/twitter/android/composer/g;->a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/model/media/EditableImage;Landroid/view/View;I)V

    .line 1146
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1091
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->d(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/media/selection/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/ComposerActivity$b;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/composer/ComposerActivity$b;-><init>(Lcom/twitter/android/composer/ComposerActivity;Z)V

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V

    .line 1092
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 1

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->n(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->n(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/j;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 1153
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1096
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    .line 1097
    sget-object v1, Lcom/twitter/android/composer/ComposerActivity$35;->a:[I

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1111
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/o;->a(Lcom/twitter/media/model/MediaType;)V

    .line 1112
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->c()V

    .line 1113
    return-void

    .line 1099
    :pswitch_0
    check-cast p1, Lcom/twitter/model/media/EditableImage;

    const/4 v1, 0x1

    invoke-direct {p0, p2, p1, v1}, Lcom/twitter/android/composer/ComposerActivity$16;->a(Landroid/view/View;Lcom/twitter/model/media/EditableImage;I)V

    goto :goto_0

    .line 1104
    :pswitch_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    goto :goto_0

    .line 1097
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1117
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1118
    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 1119
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    const-class v3, Lcom/twitter/android/AltTextActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "editable_image"

    .line 1120
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 1121
    iget-object v2, v0, Lcom/twitter/model/media/EditableImage;->i:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1122
    const-string/jumbo v2, "alt_text"

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1124
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$16;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/16 v2, 0x209

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/ComposerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1126
    :cond_1
    return-void
.end method

.method public c(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1130
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 1131
    check-cast p1, Lcom/twitter/model/media/EditableImage;

    const/4 v0, 0x3

    invoke-direct {p0, p2, p1, v0}, Lcom/twitter/android/composer/ComposerActivity$16;->a(Landroid/view/View;Lcom/twitter/model/media/EditableImage;I)V

    .line 1133
    :cond_0
    return-void
.end method
