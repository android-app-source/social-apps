.class public Lcom/twitter/android/composer/j;
.super Lcom/twitter/android/composer/geotag/a;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/widget/ComposerPoiFragment;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/twitter/android/composer/geotag/b;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbqn;Lcom/twitter/android/geo/a;Lcom/twitter/android/widget/ComposerPoiFragment;Lcom/twitter/android/composer/geotag/f;Landroid/widget/TextView;Lcom/twitter/android/widget/ToggleImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct/range {p0 .. p14}, Lcom/twitter/android/composer/geotag/a;-><init>(Landroid/content/Context;Lcom/twitter/android/composer/geotag/b;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbqn;Lcom/twitter/android/geo/a;Lcom/twitter/android/widget/ComposerPoiFragment;Lcom/twitter/android/composer/geotag/f;Landroid/widget/TextView;Lcom/twitter/android/widget/ToggleImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/os/Bundle;)V

    .line 60
    iput-object p7, p0, Lcom/twitter/android/composer/j;->a:Lcom/twitter/android/widget/ComposerPoiFragment;

    .line 61
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/composer/geotag/b;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbqn;Lcom/twitter/android/geo/a;Lcom/twitter/android/composer/geotag/InlinePlacePickerView;Landroid/widget/TextView;Lcom/twitter/android/widget/ToggleImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/os/Bundle;)Lcom/twitter/android/composer/j;
    .locals 17

    .prologue
    .line 77
    const-string/jumbo v2, "location"

    .line 78
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/ComposerPoiFragment;

    .line 79
    if-nez v2, :cond_0

    .line 80
    new-instance v9, Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-direct {v9}, Lcom/twitter/android/widget/ComposerPoiFragment;-><init>()V

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f130286

    const-string/jumbo v4, "location"

    .line 83
    invoke-virtual {v2, v3, v9, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 84
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 87
    :goto_0
    new-instance v2, Lcom/twitter/android/composer/j;

    new-instance v10, Lcom/twitter/android/composer/geotag/f;

    .line 98
    invoke-virtual {v9}, Lcom/twitter/android/widget/ComposerPoiFragment;->j()Lcom/twitter/android/geo/PlacePickerModel;

    move-result-object v3

    move-object/from16 v0, p7

    move-object/from16 v1, p3

    invoke-direct {v10, v0, v1, v3}, Lcom/twitter/android/composer/geotag/f;-><init>(Lcom/twitter/android/composer/geotag/f$b;Lcom/twitter/library/client/Session;Lcom/twitter/android/geo/PlacePickerModel;)V

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    invoke-direct/range {v2 .. v16}, Lcom/twitter/android/composer/j;-><init>(Landroid/content/Context;Lcom/twitter/android/composer/geotag/b;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbqn;Lcom/twitter/android/geo/a;Lcom/twitter/android/widget/ComposerPoiFragment;Lcom/twitter/android/composer/geotag/f;Landroid/widget/TextView;Lcom/twitter/android/widget/ToggleImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/os/Bundle;)V

    .line 87
    return-object v2

    :cond_0
    move-object v9, v2

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/composer/j;->a:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/ComposerPoiFragment;->b(I)V

    .line 118
    return-void
.end method

.method public a(Landroid/support/v4/app/FragmentTransaction;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/composer/j;->a:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 110
    return-void
.end method

.method public b(Landroid/support/v4/app/FragmentTransaction;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/composer/j;->a:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 114
    return-void
.end method
