.class public Lcom/twitter/android/composer/o;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Lcom/twitter/analytics/model/ScribeItem;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 74
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "exit"

    aput-object v3, v1, v2

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 73
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 76
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/twitter/android/composer/o;->a:J

    .line 53
    return-void
.end method

.method a(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 65
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 66
    invoke-virtual {v0, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 67
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 68
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 69
    return-void
.end method

.method a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 5

    .prologue
    .line 519
    invoke-static {p2}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const-string/jumbo v0, "open_native"

    move-object v1, v0

    .line 525
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 526
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "composition"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "gallery"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "periscope_go_live"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v1, v2, v3

    .line 527
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 525
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 528
    return-void

    .line 522
    :cond_0
    invoke-static {p1}, Lbpq;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "open_app"

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "open_interstitial"

    goto :goto_1
.end method

.method a(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V
    .locals 6

    .prologue
    .line 567
    packed-switch p3, :pswitch_data_0

    .line 594
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not a recognized icon type for scribing!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :pswitch_0
    const-string/jumbo v1, "camera"

    .line 570
    const-string/jumbo v0, "click"

    move-object v2, v1

    move-object v1, v0

    .line 597
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v3, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 598
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "composition"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "media_rail"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v2, v3, v4

    const/4 v2, 0x4

    aput-object v1, v3, v2

    .line 599
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 597
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 600
    return-void

    .line 574
    :pswitch_1
    const-string/jumbo v1, "video"

    .line 575
    const-string/jumbo v0, "click"

    move-object v2, v1

    move-object v1, v0

    .line 576
    goto :goto_0

    .line 579
    :pswitch_2
    const-string/jumbo v1, "periscope_go_live"

    .line 580
    invoke-static {p2}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    const-string/jumbo v0, "open_native"

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 583
    :cond_0
    invoke-static {p1}, Lbpq;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "open_app"

    :goto_1
    move-object v2, v1

    move-object v1, v0

    .line 586
    goto :goto_0

    .line 583
    :cond_1
    const-string/jumbo v0, "open_interstitial"

    goto :goto_1

    .line 589
    :pswitch_3
    const-string/jumbo v1, "gallery"

    .line 590
    const-string/jumbo v0, "click"

    move-object v2, v1

    move-object v1, v0

    .line 591
    goto :goto_0

    .line 567
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method a(Landroid/content/Context;Ljava/util/List;ZZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 408
    iget-wide v0, p0, Lcom/twitter/android/composer/o;->a:J

    sget-object v2, Lcom/twitter/android/composer/ComposerType;->a:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0, v1, v2, p2}, Lcom/twitter/android/composer/p;->a(JLcom/twitter/android/composer/ComposerType;Ljava/util/List;)V

    .line 409
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 413
    iget-object v4, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    .line 414
    sget-object v1, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    if-eq v4, v1, :cond_0

    .line 418
    if-eqz p3, :cond_4

    const-string/jumbo v1, ":drafts:composition"

    .line 419
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 421
    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne v4, v1, :cond_5

    .line 422
    const-string/jumbo v1, "send_photo_tweet"

    .line 429
    :goto_2
    const-string/jumbo v2, ""

    .line 430
    sget-object v6, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-eq v4, v6, :cond_7

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_7

    .line 431
    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    invoke-virtual {v0}, Lcom/twitter/model/media/MediaSource;->a()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 440
    :cond_2
    :goto_3
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v6, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v4, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 441
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-instance v4, Lcom/twitter/library/scribe/ScribeAltTextMedia;

    invoke-direct {v4, p1, p2}, Lcom/twitter/library/scribe/ScribeAltTextMedia;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 442
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    aput-object v5, v4, v3

    aput-object v2, v4, v8

    aput-object v1, v4, v9

    .line 443
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 440
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 445
    invoke-static {p2}, Lcom/twitter/model/util/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 446
    if-lez v1, :cond_3

    .line 447
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v6, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v2, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v4, v9, [Ljava/lang/String;

    aput-object v5, v4, v3

    if-eqz p4, :cond_9

    const-string/jumbo v0, ":reply_with_tags"

    :goto_4
    aput-object v0, v4, v8

    .line 448
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 449
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    int-to-long v6, v1

    .line 450
    invoke-virtual {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 451
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 455
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v3

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 456
    invoke-virtual {v0, v9}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 457
    instance-of v4, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v4, :cond_b

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iget v0, v0, Lcom/twitter/model/media/EditableImage;->c:I

    if-lez v0, :cond_b

    .line 458
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_6
    move v1, v0

    .line 460
    goto :goto_5

    .line 418
    :cond_4
    const-string/jumbo v1, ":composition:"

    goto/16 :goto_1

    .line 423
    :cond_5
    sget-object v1, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-ne v4, v1, :cond_6

    .line 424
    const-string/jumbo v1, "send_gif_tweet"

    goto/16 :goto_2

    .line 426
    :cond_6
    const-string/jumbo v1, "send_video_tweet"

    goto/16 :goto_2

    .line 433
    :cond_7
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 434
    iget-object v6, v0, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    sget-object v7, Lcom/twitter/model/media/MediaSource;->d:Lcom/twitter/model/media/MediaSource;

    if-ne v6, v7, :cond_8

    .line 435
    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    invoke-virtual {v0}, Lcom/twitter/model/media/MediaSource;->a()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 436
    goto/16 :goto_3

    .line 447
    :cond_9
    const-string/jumbo v0, ":tweet_with_tags"

    goto :goto_4

    .line 461
    :cond_a
    if-lez v1, :cond_0

    .line 462
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v6, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    aput-object v5, v2, v3

    const-string/jumbo v3, ":send_filtered_photo"

    aput-object v3, v2, v8

    .line 463
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 464
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    int-to-long v2, v1

    .line 465
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 466
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto :goto_6
.end method

.method a(Lcau;Z)V
    .locals 4

    .prologue
    .line 473
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcau;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v3, "poll_composer"

    aput-object v3, v2, v0

    const/4 v3, 0x4

    if-eqz p2, :cond_2

    const-string/jumbo v0, "send_reply"

    :goto_1
    aput-object v0, v2, v3

    .line 477
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 478
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 479
    invoke-virtual {p1}, Lcau;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 476
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "send_tweet"

    goto :goto_1
.end method

.method public a(Lcom/twitter/analytics/model/ScribeItem;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 61
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 502
    iget-boolean v0, p0, Lcom/twitter/android/composer/o;->e:Z

    if-nez v0, :cond_0

    .line 503
    iput-boolean v5, p0, Lcom/twitter/android/composer/o;->e:Z

    .line 504
    const-string/jumbo v0, "impression_enabled"

    .line 505
    invoke-static {p1}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 506
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_native"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 508
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 509
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const-string/jumbo v3, "composition"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "media_rail"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "periscope_go_live"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v1, v2, v3

    .line 510
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 508
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 512
    :cond_0
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method a(Lcom/twitter/library/client/Session;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 487
    iget-boolean v0, p0, Lcom/twitter/android/composer/o;->d:Z

    if-nez v0, :cond_0

    .line 488
    iput-boolean v5, p0, Lcom/twitter/android/composer/o;->d:Z

    .line 489
    if-eqz p2, :cond_1

    const-string/jumbo v0, "impression_enabled"

    .line 490
    :goto_0
    invoke-static {p1}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 491
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_native"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 493
    :goto_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 494
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const-string/jumbo v3, "composition"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "gallery"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "periscope_go_live"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v1, v2, v3

    .line 495
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 493
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 497
    :cond_0
    return-void

    .line 489
    :cond_1
    const-string/jumbo v0, "impression_disabled"

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method a(Lcom/twitter/media/model/MediaType;)V
    .locals 5

    .prologue
    .line 82
    sget-object v0, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/twitter/media/model/MediaType;->f:Lcom/twitter/media/model/MediaType;

    if-ne p1, v0, :cond_2

    .line 83
    :cond_0
    const-string/jumbo v0, "video"

    move-object v1, v0

    .line 89
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 90
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "composition"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "editor"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v1, v2, v3

    const/4 v1, 0x4

    const-string/jumbo v3, "open"

    aput-object v3, v2, v1

    .line 91
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 89
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 92
    :cond_1
    return-void

    .line 84
    :cond_2
    sget-object v0, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne p1, v0, :cond_1

    .line 85
    const-string/jumbo v0, "photo"

    move-object v1, v0

    goto :goto_0
.end method

.method a(Lcom/twitter/model/drafts/a;Lcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 374
    iget-wide v0, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-static {p1, p2, v0, v1, p3}, Lcom/twitter/android/composer/p;->a(Lcom/twitter/model/drafts/a;Lcom/twitter/model/core/Tweet;JZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    iget-object v1, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3, v1}, Lcom/twitter/android/composer/p;->a(Lcom/twitter/analytics/model/ScribeItem;JLjava/lang/String;)V

    .line 377
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 113
    iget-object v1, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    const-string/jumbo v2, "composition"

    iget-wide v4, p0, Lcom/twitter/android/composer/o;->a:J

    move-object v0, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lbrv;->a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 114
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    .line 57
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 340
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 345
    sget-object v1, Lcom/twitter/android/composer/o$1;->a:[I

    iget-object v2, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    invoke-virtual {v2}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 359
    const/4 v0, 0x0

    move-object v1, v0

    .line 363
    :goto_1
    if-eqz v1, :cond_0

    .line 364
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 365
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 366
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeSection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 367
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 347
    :pswitch_0
    new-instance v0, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;-><init>()V

    const-string/jumbo v1, "(multiple):composition:video:trim:send_video_tweet"

    .line 348
    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    move-result-object v0

    move-object v1, v0

    .line 349
    goto :goto_1

    .line 352
    :pswitch_1
    const/4 v1, 0x2

    .line 353
    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableSegmentedVideo;

    .line 354
    new-instance v1, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableSegmentedVideo;

    invoke-direct {v1, v0}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;-><init>(Lcom/twitter/model/media/EditableSegmentedVideo;)V

    const-string/jumbo v0, "(multiple):composition:video:segment:send_video_tweet"

    .line 355
    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    move-object v1, v0

    .line 356
    goto :goto_1

    .line 345
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Lnk;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 174
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/composer/o;->a(Lnk;Ljava/lang/String;I)V

    .line 175
    return-void
.end method

.method a(Lnk;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 184
    iget v0, p1, Lnk;->b:I

    if-eq v0, v6, :cond_0

    iget v0, p1, Lnk;->b:I

    if-ne v0, v5, :cond_2

    .line 185
    :cond_0
    iget v0, p1, Lnk;->b:I

    if-ne v0, v5, :cond_3

    const-string/jumbo v0, "hashtag"

    .line 186
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const-string/jumbo v3, "composition"

    aput-object v3, v2, v6

    const-string/jumbo v3, "autocomplete_dropdown"

    aput-object v3, v2, v5

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    aput-object p2, v2, v0

    .line 187
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 188
    iget-object v1, p1, Lnk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 189
    const/4 v1, -0x1

    if-eq p3, v1, :cond_1

    .line 190
    invoke-virtual {v0, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 193
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 195
    :cond_2
    return-void

    .line 185
    :cond_3
    const-string/jumbo v0, "username"

    goto :goto_0
.end method

.method a(Z)V
    .locals 5

    .prologue
    .line 533
    const-string/jumbo v0, "gallery"

    .line 534
    if-eqz p1, :cond_0

    .line 535
    const-string/jumbo v0, "media_rail"

    move-object v1, v0

    .line 537
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 538
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "composition"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    const/4 v1, 0x3

    const-string/jumbo v3, "periscope_takeover"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string/jumbo v3, "close"

    aput-object v3, v2, v1

    .line 539
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 537
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 540
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method a(ZLcom/twitter/model/core/Tweet;ZZZLcom/twitter/model/geo/c;JLandroid/content/Context;Lcax;Ljava/lang/String;)V
    .locals 23

    .prologue
    .line 290
    if-eqz p1, :cond_3

    const-string/jumbo v4, "drafts:composition"

    move-object/from16 v20, v4

    .line 292
    :goto_0
    if-eqz p2, :cond_4

    .line 293
    const-string/jumbo v4, "send_reply"

    .line 301
    :goto_1
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 302
    invoke-static {v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 303
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    move-object/from16 v2, p11

    invoke-static {v5, v0, v1, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcax;Ljava/lang/String;)V

    .line 304
    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v20, v6, v7

    const/4 v7, 0x2

    const-string/jumbo v8, ""

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object v4, v6, v7

    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 305
    const-wide/16 v6, -0x1

    cmp-long v4, p7, v6

    if-eqz v4, :cond_0

    .line 306
    move-wide/from16 v0, p7

    invoke-virtual {v5, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(J)Lcom/twitter/analytics/model/ScribeLog;

    .line 308
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    invoke-virtual {v5, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 309
    invoke-static {v5}, Lcpm;->a(Lcpk;)V

    .line 310
    if-eqz p6, :cond_1

    .line 311
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v21

    .line 312
    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    .line 313
    invoke-virtual/range {p6 .. p6}, Lcom/twitter/model/geo/c;->a()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v4

    iget-object v6, v4, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    .line 314
    invoke-virtual/range {p6 .. p6}, Lcom/twitter/model/geo/c;->a()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v4

    iget-object v7, v4, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    const-wide/high16 v8, 0x7ff8000000000000L    # NaN

    const-wide/high16 v10, 0x7ff8000000000000L    # NaN

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, -0x1

    const/4 v15, -0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    const-wide/16 v18, -0x1

    .line 312
    invoke-virtual/range {v5 .. v19}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->a(Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    .line 324
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v4, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 325
    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    check-cast v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 326
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    check-cast v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v20, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, ""

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "geotag"

    aput-object v7, v5, v6

    .line 327
    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    .line 324
    invoke-static {v4}, Lcpm;->a(Lcpk;)V

    .line 330
    :cond_1
    if-eqz p2, :cond_2

    .line 331
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v4, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/composer/o;->a:J

    const/4 v5, 0x0

    .line 332
    move-object/from16 v0, p2

    invoke-static {v0, v6, v7, v5}, Lbxd;->a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    check-cast v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v20, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, ""

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "num_recipients"

    aput-object v7, v5, v6

    .line 333
    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    .line 331
    invoke-static {v4}, Lcpm;->a(Lcpk;)V

    .line 335
    :cond_2
    return-void

    .line 290
    :cond_3
    const-string/jumbo v4, "composition:"

    move-object/from16 v20, v4

    goto/16 :goto_0

    .line 294
    :cond_4
    if-eqz p4, :cond_6

    .line 295
    if-eqz p3, :cond_5

    const-string/jumbo v4, "self_retweet"

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v4, "retweet"

    goto/16 :goto_1

    .line 296
    :cond_6
    if-eqz p5, :cond_8

    .line 297
    if-eqz p3, :cond_7

    const-string/jumbo v4, "send_self_quote_tweet"

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v4, "send_quote_tweet"

    goto/16 :goto_1

    .line 299
    :cond_8
    const-string/jumbo v4, "send_tweet"

    goto/16 :goto_1
.end method

.method a(ZZ)V
    .locals 5

    .prologue
    .line 224
    if-eqz p1, :cond_0

    .line 225
    const-string/jumbo v0, "cancel_reply_sheet"

    move-object v1, v0

    .line 231
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 232
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "composition"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    const/4 v1, 0x3

    const-string/jumbo v3, "save_draft"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v2, v1

    .line 233
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 231
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 234
    return-void

    .line 226
    :cond_0
    if-eqz p2, :cond_1

    .line 227
    const-string/jumbo v0, "cancel_quote_sheet"

    move-object v1, v0

    goto :goto_0

    .line 229
    :cond_1
    const-string/jumbo v0, "cancel_sheet"

    move-object v1, v0

    goto :goto_0
.end method

.method a(ZZJ)V
    .locals 5

    .prologue
    .line 202
    if-eqz p1, :cond_1

    .line 203
    const-string/jumbo v0, "cancel_reply_sheet"

    .line 209
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "composition"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v0, v2, v3

    const/4 v0, 0x3

    const-string/jumbo v3, "dont_save"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v2, v0

    .line 210
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 211
    const-wide/16 v2, -0x1

    cmp-long v1, p3, v2

    if-eqz v1, :cond_0

    .line 213
    invoke-virtual {v0, p3, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(J)Lcom/twitter/analytics/model/ScribeLog;

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 216
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 217
    return-void

    .line 204
    :cond_1
    if-eqz p2, :cond_2

    .line 205
    const-string/jumbo v0, "cancel_quote_sheet"

    goto :goto_0

    .line 207
    :cond_2
    const-string/jumbo v0, "cancel_sheet"

    goto :goto_0
.end method

.method b()V
    .locals 4

    .prologue
    .line 96
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 97
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "editor"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "photo"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "dismiss"

    aput-object v3, v1, v2

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 96
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 99
    return-void
.end method

.method b(Z)V
    .locals 5

    .prologue
    .line 545
    const-string/jumbo v0, "gallery"

    .line 546
    if-eqz p1, :cond_0

    .line 547
    const-string/jumbo v0, "media_rail"

    move-object v1, v0

    .line 549
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 550
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "composition"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    const/4 v1, 0x3

    const-string/jumbo v3, "periscope_takeover"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string/jumbo v3, "install_app"

    aput-object v3, v2, v1

    .line 551
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 549
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 552
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method c()V
    .locals 4

    .prologue
    .line 103
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 104
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition:image_attachment::impression"

    aput-object v3, v1, v2

    .line 105
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 103
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 106
    return-void
.end method

.method d()V
    .locals 4

    .prologue
    .line 118
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 119
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "editor"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "photo"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "select"

    aput-object v3, v1, v2

    .line 120
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 118
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 121
    return-void
.end method

.method e()V
    .locals 4

    .prologue
    .line 133
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 134
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "photo_picker"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 135
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 133
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 136
    return-void
.end method

.method f()V
    .locals 4

    .prologue
    .line 140
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 141
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "reply"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "show"

    aput-object v3, v1, v2

    .line 142
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 140
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 143
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 147
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 148
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "lifeline_alerts"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "select"

    aput-object v3, v1, v2

    .line 149
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 147
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 150
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 154
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 155
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "lifeline_alerts"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 156
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 154
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 157
    return-void
.end method

.method i()V
    .locals 4

    .prologue
    .line 161
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 162
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "text_view"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "focus_field"

    aput-object v3, v1, v2

    .line 163
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 161
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 164
    return-void
.end method

.method j()V
    .locals 4

    .prologue
    .line 168
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 169
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "media_tag_prompt"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 170
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 168
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 171
    return-void
.end method

.method public k()V
    .locals 4

    .prologue
    .line 238
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 239
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "lifeline_alerts"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "tweet"

    aput-object v3, v1, v2

    .line 240
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 238
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 241
    return-void
.end method

.method public l()V
    .locals 4

    .prologue
    .line 245
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 246
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "lifeline_alerts"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "cancel"

    aput-object v3, v1, v2

    .line 247
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 245
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 248
    return-void
.end method

.method m()V
    .locals 4

    .prologue
    .line 252
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 253
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "limit_exceeded"

    aput-object v3, v1, v2

    .line 254
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 252
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 255
    return-void
.end method

.method n()V
    .locals 4

    .prologue
    .line 259
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 260
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "edit"

    aput-object v3, v1, v2

    .line 261
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 259
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 262
    return-void
.end method

.method o()V
    .locals 4

    .prologue
    .line 266
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 267
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "photo_gallery"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "open_album"

    aput-object v3, v1, v2

    .line 268
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 266
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 269
    return-void
.end method

.method p()V
    .locals 3

    .prologue
    .line 381
    iget-wide v0, p0, Lcom/twitter/android/composer/o;->a:J

    iget-object v2, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/composer/p;->a(JLjava/lang/String;)V

    .line 382
    return-void
.end method

.method q()V
    .locals 4

    .prologue
    .line 556
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/composer/o;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/composer/o;->c:Lcom/twitter/analytics/model/ScribeItem;

    .line 557
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/composer/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition:media_rail:media:click"

    aput-object v3, v1, v2

    .line 558
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 556
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 559
    return-void
.end method
