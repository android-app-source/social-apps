.class public Lcom/twitter/android/composer/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/Tooltip$c;


# static fields
.field public static final a:[Ljava/lang/String;

.field private static b:Lcom/twitter/android/composer/r;


# instance fields
.field private final c:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/util/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private e:Lcom/twitter/library/client/Session;

.field private f:Lcom/twitter/ui/widget/Tooltip;

.field private g:Lcom/twitter/ui/widget/Tooltip$c;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "record_video_tooltip"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "found_media_tooltip"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "stickers_composer_tooltip"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "go_live_rail_tooltip"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "go_live_gallery_tooltip"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/composer/r;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/r;->c:Landroid/util/LongSparseArray;

    .line 62
    iput-object p1, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/twitter/android/composer/r;->e:Lcom/twitter/library/client/Session;

    .line 64
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/library/client/Session;Landroid/os/Bundle;)Lcom/twitter/android/composer/r;
    .locals 5

    .prologue
    .line 81
    sget-object v0, Lcom/twitter/android/composer/r;->b:Lcom/twitter/android/composer/r;

    if-eqz v0, :cond_1

    .line 82
    sget-object v0, Lcom/twitter/android/composer/r;->b:Lcom/twitter/android/composer/r;

    move-object v2, v0

    .line 87
    :goto_0
    if-eqz p3, :cond_0

    .line 88
    const-string/jumbo v0, "composer_tooltip_suppress_in_session"

    .line 89
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/twitter/android/composer/r;->h:Z

    .line 91
    sget-object v3, Lcom/twitter/android/composer/r;->a:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 92
    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/Tooltip;

    .line 93
    if-eqz v0, :cond_2

    .line 94
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip;->a(Lcom/twitter/ui/widget/Tooltip$c;)V

    .line 95
    iput-object v0, v2, Lcom/twitter/android/composer/r;->f:Lcom/twitter/ui/widget/Tooltip;

    .line 100
    :cond_0
    return-object v2

    .line 84
    :cond_1
    new-instance v0, Lcom/twitter/android/composer/r;

    invoke-direct {v0, p0, p2}, Lcom/twitter/android/composer/r;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    move-object v2, v0

    goto :goto_0

    .line 91
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private c()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/util/h;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 270
    iget-object v0, p0, Lcom/twitter/android/composer/r;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 271
    iget-object v0, p0, Lcom/twitter/android/composer/r;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 272
    if-eqz v0, :cond_0

    .line 296
    :goto_0
    return-object v0

    .line 276
    :cond_0
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 277
    const-string/jumbo v9, "record_video_tooltip"

    new-instance v0, Lcom/twitter/android/util/h;

    iget-object v1, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    const-string/jumbo v2, "record_video_tooltip"

    const/4 v3, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    const-string/jumbo v0, "found_media_tooltip"

    iget-object v1, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    const-string/jumbo v2, "found_media_tooltip"

    .line 282
    invoke-static {v1, v2, v6, v7}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 280
    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    const-string/jumbo v0, "stickers_composer_tooltip"

    iget-object v1, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    const-string/jumbo v2, "stickers_composer_tooltip"

    .line 285
    invoke-static {v1, v2, v6, v7}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 283
    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    const-string/jumbo v9, "found_media_umf_tooltip"

    new-instance v0, Lcom/twitter/android/util/h;

    iget-object v1, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    const-string/jumbo v2, "found_media_umf_tooltip"

    const v3, 0x7fffffff

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    const-string/jumbo v0, "go_live_rail_tooltip"

    iget-object v1, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    const-string/jumbo v2, "go_live_rail_tooltip"

    .line 291
    invoke-static {v1, v2, v6, v7}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 289
    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    const-string/jumbo v0, "go_live_gallery_tooltip"

    iget-object v1, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    const-string/jumbo v2, "go_live_gallery_tooltip"

    .line 294
    invoke-static {v1, v2, v6, v7}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 292
    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    iget-object v0, p0, Lcom/twitter/android/composer/r;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v6, v7, v8}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    move-object v0, v8

    .line 296
    goto :goto_0
.end method


# virtual methods
.method a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 265
    const-string/jumbo v0, "composer_tooltip_suppress_in_session"

    iget-boolean v1, p0, Lcom/twitter/android/composer/r;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 266
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/twitter/android/composer/r;->e:Lcom/twitter/library/client/Session;

    .line 105
    return-void
.end method

.method public a(Lcom/twitter/ui/widget/Tooltip$c;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/twitter/android/composer/r;->g:Lcom/twitter/ui/widget/Tooltip$c;

    .line 113
    return-void
.end method

.method public a(Lcom/twitter/ui/widget/Tooltip;I)V
    .locals 1

    .prologue
    .line 301
    packed-switch p2, :pswitch_data_0

    .line 314
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/composer/r;->g:Lcom/twitter/ui/widget/Tooltip$c;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/twitter/android/composer/r;->g:Lcom/twitter/ui/widget/Tooltip$c;

    invoke-interface {v0, p1, p2}, Lcom/twitter/ui/widget/Tooltip$c;->a(Lcom/twitter/ui/widget/Tooltip;I)V

    .line 317
    :cond_0
    return-void

    .line 303
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/Tooltip;->a(Z)V

    goto :goto_0

    .line 307
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/composer/r;->f:Lcom/twitter/ui/widget/Tooltip;

    goto :goto_0

    .line 301
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Ljava/lang/String;Landroid/support/v4/app/FragmentManager;)V
    .locals 5

    .prologue
    const v1, 0x7f13028f

    const v2, 0x7f0a063b

    const v0, 0x7f0d03d1

    .line 176
    const/4 v3, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 226
    :goto_1
    return-void

    .line 176
    :sswitch_0
    const-string/jumbo v4, "record_video_tooltip"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v4, "found_media_tooltip"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v4, "found_media_umf_tooltip"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v4, "stickers_composer_tooltip"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x3

    goto :goto_0

    :sswitch_4
    const-string/jumbo v4, "go_live_rail_tooltip"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x4

    goto :goto_0

    :sswitch_5
    const-string/jumbo v4, "go_live_gallery_tooltip"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x5

    goto :goto_0

    .line 178
    :pswitch_0
    const v2, 0x7f0a00f2

    .line 218
    :goto_2
    :pswitch_1
    iget-object v3, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 219
    invoke-static {v3, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v1

    .line 220
    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v1

    .line 221
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f130272

    .line 222
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->d(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 223
    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/Tooltip$a;->a(Lcom/twitter/ui/widget/Tooltip$c;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 224
    invoke-virtual {v0, p2, p1}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/r;->f:Lcom/twitter/ui/widget/Tooltip;

    .line 225
    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/r;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 185
    :pswitch_2
    const v2, 0x7f0a03c0

    .line 186
    const v1, 0x7f130290

    .line 188
    goto :goto_2

    .line 191
    :pswitch_3
    const v2, 0x7f0a091f

    .line 192
    const v1, 0x7f13028a

    .line 193
    const v0, 0x7f0d033a

    .line 194
    goto :goto_2

    .line 198
    :pswitch_4
    const v1, 0x7f130053

    .line 199
    const v0, 0x7f0d024a

    .line 200
    goto :goto_2

    .line 176
    nop

    :sswitch_data_0
    .sparse-switch
        -0x70532646 -> :sswitch_5
        -0x38c1cace -> :sswitch_4
        -0x24d3432f -> :sswitch_0
        0x1ecf222d -> :sswitch_3
        0x30ac0a3a -> :sswitch_2
        0x4b55e0ab -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method a()Z
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/android/composer/r;->f:Lcom/twitter/ui/widget/Tooltip;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/twitter/android/composer/r;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 141
    iget-boolean v2, p0, Lcom/twitter/android/composer/r;->h:Z

    if-nez v2, :cond_0

    if-nez v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v1

    .line 144
    :cond_1
    const-string/jumbo v0, "record_video_tooltip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 145
    invoke-static {}, Lcom/twitter/android/util/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/r;->d:Landroid/content/Context;

    .line 146
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :cond_2
    const-string/jumbo v0, "stickers_composer_tooltip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 150
    invoke-static {v1}, Lbpt;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    :cond_3
    const-string/jumbo v0, "go_live_rail_tooltip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/composer/r;->e:Lcom/twitter/library/client/Session;

    .line 154
    invoke-static {v0}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-static {}, Lbpo;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    :cond_4
    const-string/jumbo v0, "go_live_gallery_tooltip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/composer/r;->e:Lcom/twitter/library/client/Session;

    .line 159
    invoke-static {v0}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    :cond_5
    invoke-direct {p0}, Lcom/twitter/android/composer/r;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/h;

    .line 163
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method b()V
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/r;->h:Z

    .line 257
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/twitter/android/composer/r;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/h;

    .line 242
    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 245
    :cond_0
    const-string/jumbo v0, "found_media_umf_tooltip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    const-string/jumbo v0, "found_media_tooltip"

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/r;->b(Ljava/lang/String;)V

    .line 249
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/r;->b()V

    .line 250
    return-void
.end method
