.class Lcom/twitter/android/composer/ComposerActivity$38;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/t$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/library/client/Session;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    .prologue
    .line 751
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 770
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->e(Lcom/twitter/android/composer/ComposerActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 771
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v2, v2}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    .line 773
    :cond_0
    return-void
.end method

.method public a(Lcbi;Lnk;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/provider/e;",
            ">;",
            "Lnk;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 756
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 757
    invoke-virtual {p1}, Lcbi;->be_()I

    move-result v0

    if-lez v0, :cond_2

    .line 758
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->e(Lcom/twitter/android/composer/ComposerActivity;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 759
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/o;

    move-result-object v0

    const-string/jumbo v1, "show"

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/composer/o;->a(Lnk;Ljava/lang/String;)V

    .line 761
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v2, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZZ)V

    .line 766
    :cond_1
    :goto_0
    return-void

    .line 762
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->e(Lcom/twitter/android/composer/ComposerActivity;)I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 763
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {v0, v2, v2}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 777
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$38;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->j(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 778
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity$38;->a()V

    .line 780
    :cond_0
    return-void
.end method
