.class public Lcom/twitter/android/composer/l;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/l$a;,
        Lcom/twitter/android/composer/l$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/composer/f;

.field private final b:Lcom/twitter/android/composer/e;

.field private final c:Lcom/twitter/android/composer/m;

.field private final d:Lcom/twitter/android/composer/h;

.field private final e:Lcom/twitter/android/composer/k;

.field private final f:Lcom/twitter/android/composer/g;

.field private final g:Lcom/twitter/android/composer/o;

.field private final h:Lcom/twitter/android/composer/l$b;

.field private i:Lcom/twitter/android/widget/GalleryGridFragment;

.field private j:Lcom/twitter/android/composer/l$a;

.field private k:[Landroid/view/View;

.field private l:Lcom/twitter/library/client/Session;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/composer/f;Lcom/twitter/android/composer/e;Lcom/twitter/android/composer/m;Lcom/twitter/android/composer/h;Lcom/twitter/android/composer/k;Lcom/twitter/android/composer/g;Lcom/twitter/android/composer/o;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p2, p0, Lcom/twitter/android/composer/l;->l:Lcom/twitter/library/client/Session;

    .line 71
    iput-object p5, p0, Lcom/twitter/android/composer/l;->b:Lcom/twitter/android/composer/e;

    .line 72
    iput-object p6, p0, Lcom/twitter/android/composer/l;->c:Lcom/twitter/android/composer/m;

    .line 73
    iput-object p7, p0, Lcom/twitter/android/composer/l;->d:Lcom/twitter/android/composer/h;

    .line 74
    iput-object p8, p0, Lcom/twitter/android/composer/l;->e:Lcom/twitter/android/composer/k;

    .line 75
    iput-object p4, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    .line 76
    iput-object p9, p0, Lcom/twitter/android/composer/l;->f:Lcom/twitter/android/composer/g;

    .line 77
    iput-object p10, p0, Lcom/twitter/android/composer/l;->g:Lcom/twitter/android/composer/o;

    .line 78
    new-instance v0, Lcom/twitter/android/composer/l$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/composer/l$b;-><init>(Lcom/twitter/android/composer/l;Lcom/twitter/android/composer/l$1;)V

    iput-object v0, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x7f0d01bc

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 82
    const-string/jumbo v0, "gallery"

    invoke-virtual {p3, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GalleryGridFragment;

    iput-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    .line 83
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    invoke-static {p1}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/android/widget/GalleryGridFragment;->a(IZI)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    .line 86
    invoke-virtual {p3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f130286

    iget-object v2, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    const-string/jumbo v3, "gallery"

    .line 87
    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {p4}, Lcom/twitter/android/composer/f;->f()Z

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Z)[Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->a([Landroid/view/View;)V

    .line 93
    if-eqz p11, :cond_2

    const-string/jumbo v0, "media_selection_buffer"

    .line 94
    invoke-virtual {p11, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 96
    :goto_0
    if-eqz v0, :cond_1

    .line 97
    iget-object v1, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/l$b;->a(Landroid/os/Bundle;)V

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    iget-object v1, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/media/selection/d;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    new-instance v1, Lcom/twitter/android/composer/l$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/l$1;-><init>(Lcom/twitter/android/composer/l;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/GalleryGridFragment$a;)V

    .line 111
    return-void

    .line 94
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/h;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/l;->d:Lcom/twitter/android/composer/h;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/composer/l;Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/composer/l;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 314
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v0

    .line 315
    iget-object v1, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/f;->c(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    .line 316
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/media/model/MediaType;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_2

    .line 320
    iget-object v1, p0, Lcom/twitter/android/composer/l;->f:Lcom/twitter/android/composer/g;

    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    .line 321
    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->i()Lcom/twitter/model/media/EditableImage;

    move-result-object v2

    check-cast p1, Lcom/twitter/model/media/EditableImage;

    .line 323
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 320
    invoke-virtual {v1, v2, p1, v0}, Lcom/twitter/android/composer/g;->a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/model/media/EditableImage;Landroid/view/View;)V

    goto :goto_0

    .line 324
    :cond_2
    instance-of v0, p1, Lcom/twitter/model/media/EditableAnimatedGif;

    if-eqz v0, :cond_3

    .line 325
    iget-object v0, p0, Lcom/twitter/android/composer/l;->j:Lcom/twitter/android/composer/l$a;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/twitter/android/composer/l;->j:Lcom/twitter/android/composer/l$a;

    new-instance v1, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v1, p1}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/l$a;->a(Lcom/twitter/model/drafts/DraftAttachment;)V

    goto :goto_0

    .line 329
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/composer/l;->e:Lcom/twitter/android/composer/k;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/composer/k;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Z)[Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 229
    iget-object v0, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    if-eqz v0, :cond_3

    .line 231
    iget-boolean v0, p0, Lcom/twitter/android/composer/l;->m:Z

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 232
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 233
    iget-object v3, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 234
    instance-of v6, v5, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;

    if-nez v6, :cond_0

    .line 235
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    .line 239
    iput-boolean v1, p0, Lcom/twitter/android/composer/l;->m:Z

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    .line 310
    :goto_1
    return-object v0

    .line 244
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 246
    invoke-static {p1}, Lcom/twitter/android/widget/m;->a(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    invoke-static {p1}, Lcom/twitter/android/widget/m;->b(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    invoke-static {}, Lbpo;->a()Z

    move-result v0

    .line 252
    if-nez p2, :cond_4

    invoke-static {}, Lbpq;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 253
    new-instance v2, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;

    invoke-direct {v2, p1}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;-><init>(Landroid/content/Context;)V

    .line 255
    const v3, 0x7f13003d

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setId(I)V

    .line 256
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a093a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 257
    if-eqz v0, :cond_6

    .line 258
    const v0, 0x7f020810

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setIcon(I)V

    .line 259
    const v0, 0x7f0a03c6

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setLabel(I)V

    .line 260
    const/high16 v0, 0x7f110000

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setBackgroundResource(I)V

    .line 266
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/composer/l;->l:Lcom/twitter/library/client/Session;

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a(Lcom/twitter/library/client/Session;)V

    .line 267
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/l;->m:Z

    .line 271
    :cond_4
    invoke-static {}, Lbpo;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 272
    invoke-static {p1}, Lcom/twitter/android/widget/m;->c(Landroid/content/Context;)Lcom/twitter/android/widget/GalleryGridHeaderIconView;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    :cond_5
    new-instance v2, Lcom/twitter/android/composer/l$2;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/composer/l$2;-><init>(Lcom/twitter/android/composer/l;Landroid/content/Context;)V

    .line 305
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 306
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 262
    :cond_6
    const v0, 0x7f02010f

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setIcon(I)V

    .line 263
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setLabelVisibility(I)V

    .line 264
    const v0, 0x7f11018a

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setBackgroundResource(I)V

    goto :goto_2

    .line 309
    :cond_7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    .line 310
    iget-object v0, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    goto/16 :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/o;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/l;->g:Lcom/twitter/android/composer/o;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/e;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/l;->b:Lcom/twitter/android/composer/e;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/m;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/l;->c:Lcom/twitter/android/composer/m;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/composer/l;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/l;->l:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    if-eqz v0, :cond_0

    .line 218
    iget-object v1, p0, Lcom/twitter/android/composer/l;->k:[Landroid/view/View;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 219
    instance-of v4, v3, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;

    if-eqz v4, :cond_1

    .line 220
    iget-object v0, p0, Lcom/twitter/android/composer/l;->g:Lcom/twitter/android/composer/o;

    iget-object v1, p0, Lcom/twitter/android/composer/l;->l:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Landroid/view/View;->isEnabled()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/o;->a(Lcom/twitter/library/client/Session;Z)V

    .line 225
    :cond_0
    return-void

    .line 218
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/l$a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/l;->j:Lcom/twitter/android/composer/l$a;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/f;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->j()V

    .line 131
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(F)V

    .line 153
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    iget-object v0, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->l()Z

    move-result v3

    .line 161
    iget-object v4, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Z)V

    .line 162
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    iget-object v4, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v4}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Z)V

    .line 164
    iget-object v0, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->f()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Z)[Landroid/view/View;

    move-result-object v1

    .line 165
    array-length v4, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v0, v1, v2

    .line 168
    instance-of v5, v0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;

    if-eqz v5, :cond_2

    .line 169
    check-cast v0, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;

    .line 170
    iget-object v5, p0, Lcom/twitter/android/composer/l;->l:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v5}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->a(Lcom/twitter/library/client/Session;)V

    .line 171
    iget-object v5, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v5}, Lcom/twitter/android/composer/f;->j()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/twitter/android/composer/PeriscopeBroadcastGalleryHeaderView;->setEnabled(Z)V

    .line 165
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 161
    goto :goto_0

    :cond_1
    move v1, v2

    .line 162
    goto :goto_1

    .line 173
    :cond_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_3

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->a([Landroid/view/View;)V

    .line 178
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/GalleryGridFragment;->c(Z)V

    .line 179
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, p2}, Lcom/twitter/android/composer/f;->a(Landroid/net/Uri;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_0

    .line 196
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/net/Uri;)V

    .line 199
    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;)V

    .line 200
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/android/composer/l;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0, p2}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0, p2}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 186
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_1

    .line 188
    iget-object v1, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Lcom/twitter/model/media/EditableMedia;)V

    .line 189
    invoke-virtual {p0, p1}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;)V

    .line 191
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 203
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 204
    iget-object v1, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/l$b;->b(Landroid/os/Bundle;)V

    .line 205
    const-string/jumbo v1, "media_selection_buffer"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 206
    return-void
.end method

.method public a(Landroid/support/v4/app/FragmentTransaction;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 135
    iget-boolean v0, p0, Lcom/twitter/android/composer/l;->m:Z

    if-eqz v0, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/twitter/android/composer/l;->e()V

    .line 138
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/composer/l$a;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/twitter/android/composer/l;->j:Lcom/twitter/android/composer/l$a;

    .line 115
    return-void
.end method

.method public a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V

    .line 119
    return-void
.end method

.method public a(Lcom/twitter/android/widget/ab;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/ab;)V

    .line 123
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/twitter/android/composer/l;->l:Lcom/twitter/library/client/Session;

    .line 157
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableImage;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/model/media/EditableImage;)V

    .line 127
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->h()V

    .line 149
    return-void
.end method

.method public b(Landroid/support/v4/app/FragmentTransaction;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l$b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l$b;->a()V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/l;->i:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 145
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l$b;->b()V

    .line 210
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/twitter/android/composer/l;->h:Lcom/twitter/android/composer/l$b;

    invoke-virtual {v0}, Lcom/twitter/android/composer/l$b;->a()V

    .line 214
    return-void
.end method
