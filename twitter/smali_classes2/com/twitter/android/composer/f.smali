.class public Lcom/twitter/android/composer/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/f$a;
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/twitter/library/client/p;

.field c:Lcom/twitter/android/composer/f$a;

.field private final d:Lcom/twitter/model/drafts/a$a;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/twitter/model/core/Tweet;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/twitter/model/drafts/a$a;

    invoke-direct {v0}, Lcom/twitter/model/drafts/a$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    return-void
.end method

.method private static a(Lcom/twitter/android/composer/a;J)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 385
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gtz v1, :cond_1

    .line 392
    :cond_0
    :goto_0
    return v0

    .line 388
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/a;->m()Lcom/twitter/util/Tristate;

    move-result-object v1

    .line 389
    sget-object v2, Lcom/twitter/util/Tristate;->a:Lcom/twitter/util/Tristate;

    if-ne v1, v2, :cond_2

    .line 390
    invoke-static {}, Lbpj;->b()Z

    move-result v0

    goto :goto_0

    .line 392
    :cond_2
    sget-object v2, Lcom/twitter/util/Tristate;->c:Lcom/twitter/util/Tristate;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Landroid/net/Uri;)I
    .locals 3

    .prologue
    .line 375
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 376
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 377
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 381
    :goto_1
    return v0

    .line 376
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 381
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private q()V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->e()V

    .line 397
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 398
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 2

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/f;->d(Landroid/net/Uri;)I

    move-result v0

    .line 220
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 221
    const/4 v0, 0x0

    .line 223
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 2

    .prologue
    .line 209
    iget-object v0, p1, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/f;->d(Landroid/net/Uri;)I

    move-result v0

    .line 210
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 211
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    const/4 v0, 0x0

    .line 214
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/android/composer/f;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/twitter/android/composer/f;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 179
    :goto_0
    return-object v0

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->h()J

    move-result-wide v0

    .line 173
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 174
    const/4 v0, 0x0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0

    .line 177
    :cond_1
    sget-object v2, Lcom/twitter/database/schema/a$v;->c:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 178
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 180
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lbuj;->a:[Ljava/lang/String;

    .line 179
    invoke-static {v1, v0, v2}, Lauk;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/f$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/f$2;-><init>(Lcom/twitter/android/composer/f;)V

    .line 181
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 196
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/f$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/f$1;-><init>(Lcom/twitter/android/composer/f;)V

    .line 197
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 248
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 249
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    iget v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->d:I

    if-ne v0, p1, :cond_0

    .line 250
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 251
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 248
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 254
    :cond_1
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/drafts/a$a;->a(J)Lcom/twitter/model/drafts/a$a;

    .line 126
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/p;)V
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/f;->a:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/twitter/android/composer/f;->b:Lcom/twitter/library/client/p;

    .line 64
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/twitter/android/composer/f;->q()V

    .line 98
    const-string/jumbo v0, "draft_tweet"

    sget-object v1, Lcom/twitter/model/drafts/a;->a:Lcom/twitter/util/serialization/b;

    .line 99
    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    .line 98
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/model/drafts/a;)V

    .line 100
    const-string/jumbo v0, "replied_tweet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/model/core/Tweet;)V

    .line 101
    return-void
.end method

.method public a(Lcau;)V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/a$a;->a(Lcau;)Lcom/twitter/model/drafts/a$a;

    .line 288
    return-void
.end method

.method public a(Lcom/twitter/android/composer/a;)V
    .locals 4

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/twitter/android/composer/f;->q()V

    .line 68
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->k()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 69
    if-nez v0, :cond_1

    .line 70
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->j()J

    move-result-wide v0

    .line 71
    iget-object v2, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    .line 72
    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/drafts/a$a;->b(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v2

    .line 73
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->g()Lcgi;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/model/drafts/a$a;->a(Lcgi;)Lcom/twitter/model/drafts/a$a;

    move-result-object v2

    .line 74
    invoke-static {p1, v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/android/composer/a;J)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/twitter/model/drafts/a$a;->a(Z)Lcom/twitter/model/drafts/a$a;

    .line 81
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    .line 82
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->o()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->a(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/composer/f;->a:Landroid/content/Context;

    .line 83
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/twitter/android/composer/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 84
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->h()Lcom/twitter/model/geo/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/geo/c;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 85
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->l()Lcom/twitter/model/timeline/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 86
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->n()Lcom/twitter/model/core/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 87
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->b(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 88
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->c(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    .line 89
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->p()Ljava/util/List;

    move-result-object v0

    .line 90
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 93
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->e()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/f;->a(Z)V

    .line 94
    return-void

    .line 76
    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/model/core/Tweet;)V

    .line 77
    iget-object v1, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->t:J

    .line 78
    invoke-static {p1, v2, v3}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/android/composer/a;J)Z

    move-result v0

    .line 77
    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/a$a;->a(Z)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 79
    invoke-virtual {p1}, Lcom/twitter/android/composer/a;->w()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->c(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 6

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->p()V

    .line 304
    new-instance v1, Lcom/twitter/android/composer/y;

    iget-object v0, p0, Lcom/twitter/android/composer/f;->a:Landroid/content/Context;

    .line 305
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->b()Lcom/twitter/model/drafts/a;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, p1, v2, v3}, Lcom/twitter/android/composer/y;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;Z)V

    .line 306
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->g:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/y;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 307
    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 310
    new-instance v0, Lcom/twitter/android/composer/f$a;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/composer/f$a;-><init>(Lcom/twitter/android/composer/f;Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/android/composer/f;->c:Lcom/twitter/android/composer/f$a;

    .line 311
    iget-object v0, p0, Lcom/twitter/android/composer/f;->c:Lcom/twitter/android/composer/f$a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/y;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/f;->b:Lcom/twitter/library/client/p;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 314
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;J)V
    .locals 8

    .prologue
    .line 326
    new-instance v1, Lcom/twitter/android/composer/v;

    iget-object v0, p0, Lcom/twitter/android/composer/f;->a:Landroid/content/Context;

    .line 327
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/4 v6, 0x0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/composer/v;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V

    .line 328
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->g:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/v;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 329
    iget-object v0, p0, Lcom/twitter/android/composer/f;->b:Lcom/twitter/library/client/p;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 330
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 343
    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->p()V

    .line 345
    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->c()J

    move-result-wide v0

    .line 346
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 348
    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/library/client/Session;J)V

    .line 352
    invoke-virtual {p0, v4, v5}, Lcom/twitter/android/composer/f;->a(J)V

    .line 353
    invoke-virtual {p0, p2}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/library/client/Session;)V

    .line 355
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 364
    iput-object p1, p0, Lcom/twitter/android/composer/f;->f:Lcom/twitter/model/core/Tweet;

    .line 365
    if-nez p1, :cond_0

    .line 366
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->b(J)Lcom/twitter/model/drafts/a$a;

    .line 367
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcgi;)Lcom/twitter/model/drafts/a$a;

    .line 372
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-static {p1}, Lbpj;->a(Lcom/twitter/model/core/Tweet;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->b(J)Lcom/twitter/model/drafts/a$a;

    .line 370
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcgi;)Lcom/twitter/model/drafts/a$a;

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/drafts/a;)V
    .locals 2

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/twitter/android/composer/f;->q()V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/drafts/a;)Lcom/twitter/model/drafts/a$a;

    .line 106
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    .line 107
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 108
    return-void
.end method

.method public a(Lcom/twitter/model/geo/c;)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/geo/c;)Lcom/twitter/model/drafts/a$a;

    .line 284
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    .line 135
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/a$a;->c(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    .line 155
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/twitter/android/composer/f;->g:Z

    .line 143
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->i()Z

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/media/model/MediaType;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 261
    iget-object v2, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 262
    sget-object v3, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-eq p1, v3, :cond_0

    sget-object v3, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    if-ne p1, v3, :cond_3

    .line 263
    :cond_0
    const/4 v3, 0x4

    if-ge v2, v3, :cond_2

    .line 265
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 263
    goto :goto_0

    .line 265
    :cond_3
    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 2

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/f;->d(Landroid/net/Uri;)I

    move-result v0

    .line 235
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    goto :goto_0
.end method

.method public b()Lcom/twitter/model/drafts/a;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    iget-object v1, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    .line 117
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->p()V

    .line 359
    const-string/jumbo v0, "draft_tweet"

    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->b()Lcom/twitter/model/drafts/a;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/drafts/a;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 360
    const-string/jumbo v0, "replied_tweet"

    iget-object v1, p0, Lcom/twitter/android/composer/f;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 361
    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 317
    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->p()V

    .line 318
    invoke-virtual {p0}, Lcom/twitter/android/composer/f;->c()J

    move-result-wide v0

    .line 319
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 320
    invoke-virtual {p0, v4, v5}, Lcom/twitter/android/composer/f;->a(J)V

    .line 321
    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/library/client/Session;J)V

    .line 323
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/a$a;->b(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    .line 280
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public c(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/f;->d(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/twitter/android/composer/f;->g:Z

    return v0
.end method

.method public f()Z
    .locals 4

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->h()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public i()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/twitter/android/composer/f;->f:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    return-object v0
.end method

.method public k()V
    .locals 4

    .prologue
    .line 239
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 240
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 241
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 244
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 270
    sget-object v0, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/media/model/MediaType;)Z

    move-result v0

    return v0
.end method

.method public m()Lcom/twitter/model/core/r;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->k()Lcom/twitter/model/core/r;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcau;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->j()Lcau;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/android/composer/f;->d:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->j()Lcau;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/twitter/android/composer/f;->c:Lcom/twitter/android/composer/f$a;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/twitter/android/composer/f;->c:Lcom/twitter/android/composer/f$a;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f$a;->a()V

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/composer/f;->c:Lcom/twitter/android/composer/f$a;

    .line 337
    :cond_0
    return-void
.end method
