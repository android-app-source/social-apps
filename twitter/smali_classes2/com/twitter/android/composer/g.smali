.class public Lcom/twitter/android/composer/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/imageeditor/EditImageFragment$b;


# instance fields
.field private a:Landroid/support/v4/app/FragmentManager;

.field private b:Lcom/twitter/android/widget/RevealClipFrameLayout;

.field private c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

.field private d:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/twitter/android/composer/g;->a:Landroid/support/v4/app/FragmentManager;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 77
    iget-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/RevealClipFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/RevealClipFrameLayout;->setVisibility(I)V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/FragmentManager;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const v1, 0x7f13029c

    .line 29
    iput-object p1, p0, Lcom/twitter/android/composer/g;->a:Landroid/support/v4/app/FragmentManager;

    .line 31
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/RevealClipFrameLayout;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/RevealClipFrameLayout;

    iput-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    .line 32
    if-eqz p3, :cond_0

    .line 33
    invoke-virtual {p1, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;

    iput-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 34
    iget-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment$b;)V

    .line 36
    iget-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/RevealClipFrameLayout;->setVisibility(I)V

    .line 39
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/media/imageeditor/EditImageFragment$b;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/twitter/android/composer/g;->d:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    .line 43
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/model/media/EditableImage;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/widget/RevealClipFrameLayout;

    .line 107
    const-string/jumbo v5, "composer"

    move-object v0, p1

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    .line 108
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/media/imageeditor/EditImageFragment$b;Landroid/view/View;Lcom/twitter/android/widget/RevealClipFrameLayout;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 109
    iget-object v0, p0, Lcom/twitter/android/composer/g;->a:Landroid/support/v4/app/FragmentManager;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentManager;

    .line 110
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f13029c

    iget-object v2, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 111
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 113
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/model/media/EditableImage;Landroid/view/View;I)V
    .locals 7

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/widget/RevealClipFrameLayout;

    .line 88
    const-string/jumbo v6, "composer"

    move-object v0, p1

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    move v5, p4

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/media/imageeditor/EditImageFragment$b;Landroid/view/View;Lcom/twitter/android/widget/RevealClipFrameLayout;Lcom/twitter/model/media/EditableImage;ILjava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 96
    iget-object v0, p0, Lcom/twitter/android/composer/g;->a:Landroid/support/v4/app/FragmentManager;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentManager;

    .line 97
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f13029c

    iget-object v2, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 98
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 100
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/composer/g;->d:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/twitter/android/composer/g;->d:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/media/imageeditor/EditImageFragment$b;->a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)V

    .line 50
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/composer/g;->d()V

    .line 51
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    .line 63
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/RevealClipFrameLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/RevealClipFrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/twitter/android/composer/g;->c:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e()V

    .line 65
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/android/composer/g;->d()V

    .line 56
    iget-object v0, p0, Lcom/twitter/android/composer/g;->d:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/android/composer/g;->d:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    invoke-interface {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$b;->b()V

    .line 59
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/g;->b:Lcom/twitter/android/widget/RevealClipFrameLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/RevealClipFrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
