.class public Lcom/twitter/android/composer/TweetBox;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/android/autocomplete/SuggestionEditText$d;
.implements Lcom/twitter/android/autocomplete/SuggestionEditText$e;
.implements Lcom/twitter/android/autocomplete/SuggestionEditText$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/TweetBox$SavedState;,
        Lcom/twitter/android/composer/TweetBox$b;,
        Lcom/twitter/android/composer/TweetBox$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Landroid/text/TextWatcher;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/twitter/android/autocomplete/SuggestionEditText$d;",
        "Lcom/twitter/android/autocomplete/SuggestionEditText$e",
        "<",
        "Lnk;",
        "Lcom/twitter/android/provider/e;",
        ">;",
        "Lcom/twitter/android/autocomplete/SuggestionEditText$f",
        "<",
        "Lnk;",
        "Lcom/twitter/android/provider/e;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field a:Lcom/twitter/android/autocomplete/SuggestionEditText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/autocomplete/SuggestionEditText",
            "<",
            "Lnk;",
            "Lcom/twitter/android/provider/e;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/twitter/android/composer/TweetBox$b;

.field c:Z

.field d:Z

.field private final f:Z

.field private final g:Lnl;

.field private h:Lcom/twitter/library/client/Session;

.field private i:Lcom/twitter/model/core/Tweet;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:Lcom/twitter/model/core/r;

.field private s:Z

.field private t:Landroid/text/TextWatcher;

.field private u:I

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Ljava/util/regex/Pattern;

.field private y:Lml;

.field private z:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "dm "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "d "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "m "

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/composer/TweetBox;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/composer/TweetBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 125
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->d:Z

    .line 92
    new-instance v0, Lnl;

    invoke-direct {v0}, Lnl;-><init>()V

    .line 93
    invoke-virtual {v0, v1}, Lnl;->b(Z)Lnl;

    move-result-object v0

    .line 94
    invoke-virtual {v0, v1}, Lnl;->a(Z)Lnl;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TweetBox;->g:Lnl;

    .line 101
    iput-boolean v1, p0, Lcom/twitter/android/composer/TweetBox;->m:Z

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/composer/TweetBox;->q:I

    .line 109
    const/16 v0, 0x8c

    iput v0, p0, Lcom/twitter/android/composer/TweetBox;->u:I

    .line 118
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TweetBox;->z:Ljava/util/Collection;

    .line 126
    const-string/jumbo v0, "hashflags_in_composer_android_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->f:Z

    .line 127
    return-void
.end method

.method private static a(Landroid/text/Spanned;I)Lcom/twitter/android/composer/TweetBox$a;
    .locals 1

    .prologue
    .line 470
    const-class v0, Lcom/twitter/android/composer/TweetBox$a;

    invoke-interface {p0, p1, p1, v0}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/TweetBox$a;

    return-object v0
.end method

.method private a(Landroid/text/SpannableStringBuilder;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 962
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v2, Lcom/twitter/android/composer/TweetBox$a;

    invoke-virtual {p1, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/composer/TweetBox$a;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 963
    invoke-virtual {p1, v3}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v3}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    const-string/jumbo v5, ""

    invoke-virtual {p1, v4, v3, v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 962
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 965
    :cond_0
    return-void
.end method

.method static a(Landroid/widget/EditText;II)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 442
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 443
    invoke-static {v0, p1}, Lcom/twitter/android/composer/TweetBox;->a(Landroid/text/Spanned;I)Lcom/twitter/android/composer/TweetBox$a;

    move-result-object v1

    .line 444
    if-ne p2, p1, :cond_1

    .line 445
    if-eqz v1, :cond_0

    .line 446
    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    .line 447
    invoke-virtual {p0, v0, v0}, Landroid/widget/EditText;->setSelection(II)V

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    invoke-static {v0, p2}, Lcom/twitter/android/composer/TweetBox;->a(Landroid/text/Spanned;I)Lcom/twitter/android/composer/TweetBox$a;

    move-result-object v2

    .line 454
    if-eqz v1, :cond_2

    .line 455
    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 p1, v1, -0x1

    .line 459
    :cond_2
    if-eqz v2, :cond_3

    .line 460
    invoke-interface {v0, v2}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result p2

    .line 464
    :cond_3
    invoke-virtual {p0, p1, p2}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/composer/TweetBox;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->q()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/TweetBox;Landroid/text/SpannableStringBuilder;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/TweetBox;->a(Landroid/text/SpannableStringBuilder;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/TweetBox;Lcom/twitter/android/composer/w$a;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/TweetBox;->a(Lcom/twitter/android/composer/w$a;)V

    return-void
.end method

.method private a(Lcom/twitter/android/composer/w$a;)V
    .locals 8

    .prologue
    .line 930
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableStringBuilder;

    .line 932
    invoke-virtual {p1}, Lcom/twitter/android/composer/w$a;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/twitter/android/composer/w$a;->b()I

    move-result v2

    const-class v3, Lcom/twitter/android/composer/TweetBox$a;

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/twitter/android/composer/TweetBox$a;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v1, v2

    .line 933
    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    const-string/jumbo v7, ""

    invoke-virtual {v0, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 934
    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 932
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 936
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 698
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 699
    invoke-static {p1, v0, v1}, Lbxd;->c(Lcom/twitter/model/core/Tweet;J)Ljava/util/List;

    move-result-object v4

    .line 700
    invoke-static {v4}, Lbxd;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/String;[I)V

    .line 705
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 706
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->getMaxUserReplyCount()I

    move-result v6

    .line 707
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v7

    move v1, v2

    move v3, v2

    .line 708
    :goto_0
    if-ge v1, v7, :cond_0

    .line 709
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/t;

    iget-object v0, v0, Lcom/twitter/model/core/t;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 710
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    add-int/2addr v3, v0

    .line 708
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 712
    :cond_0
    if-le v5, v6, :cond_1

    .line 714
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v4, ":composition::mentions_highlight:impression"

    aput-object v4, v1, v2

    .line 715
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    sub-int v1, v5, v6

    int-to-long v4, v1

    .line 716
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 717
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 720
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 722
    new-instance v1, Lcom/twitter/android/composer/TweetBox$8;

    invoke-direct {v1, p0, v0, v3}, Lcom/twitter/android/composer/TweetBox$8;-><init>(Lcom/twitter/android/composer/TweetBox;Landroid/widget/EditText;I)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/TweetBox;->post(Ljava/lang/Runnable;)Z

    .line 730
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/composer/TweetBox;Lcom/twitter/android/composer/w$a;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/TweetBox;->b(Lcom/twitter/android/composer/w$a;)V

    return-void
.end method

.method private b(Lcom/twitter/android/composer/w$a;)V
    .locals 5

    .prologue
    .line 939
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableStringBuilder;

    .line 941
    invoke-virtual {p1}, Lcom/twitter/android/composer/w$a;->b()I

    move-result v1

    .line 942
    invoke-virtual {p1}, Lcom/twitter/android/composer/w$a;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 943
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/view/b;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 945
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/view/b;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v1}, Lcom/twitter/library/view/b;-><init>(Ljava/lang/String;I)V

    const/4 v2, 0x1

    .line 944
    invoke-static {v3, v0, v4, p0, v2}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Landroid/view/View;Z)I

    move-result v2

    .line 946
    new-instance v3, Lcom/twitter/android/composer/TweetBox$a;

    invoke-direct {v3}, Lcom/twitter/android/composer/TweetBox$a;-><init>()V

    add-int/2addr v2, v1

    const/16 v4, 0x21

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 948
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 687
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/String;[I)V

    .line 689
    new-instance v0, Lcom/twitter/android/composer/TweetBox$7;

    invoke-direct {v0, p0}, Lcom/twitter/android/composer/TweetBox$7;-><init>(Lcom/twitter/android/composer/TweetBox;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TweetBox;->post(Ljava/lang/Runnable;)Z

    .line 695
    return-void

    .line 687
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private c(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 952
    if-ne p1, p2, :cond_0

    .line 953
    const-string/jumbo v0, ""

    .line 958
    :goto_0
    return-object v0

    .line 956
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    .line 957
    invoke-direct {p0, v0}, Lcom/twitter/android/composer/TweetBox;->a(Landroid/text/SpannableStringBuilder;)V

    .line 958
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getMaxUserReplyCount()I
    .locals 1

    .prologue
    .line 733
    const/4 v0, 0x5

    return v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->d:Z

    .line 258
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    invoke-interface {v0}, Lcom/twitter/android/composer/TweetBox$b;->c()V

    .line 261
    :cond_0
    return-void
.end method

.method private r()V
    .locals 6

    .prologue
    .line 338
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->y:Lml;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    .line 340
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->y:Lml;

    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    .line 343
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->z:Ljava/util/Collection;

    .line 341
    invoke-static {v2, v4, v5, v0}, Lbxd;->a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    .line 340
    invoke-virtual {v1, v0}, Lml;->a(Ljava/util/Collection;)V

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->y:Lml;

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lml;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private s()V
    .locals 4

    .prologue
    .line 372
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/android/composer/TweetBox$6;

    invoke-direct {v3, p0}, Lcom/twitter/android/composer/TweetBox$6;-><init>(Lcom/twitter/android/composer/TweetBox;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 382
    return-void
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->w:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()I
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->e(Ljava/lang/String;)I

    move-result v0

    .line 738
    if-lez v0, :cond_0

    iget v1, p0, Lcom/twitter/android/composer/TweetBox;->l:I

    add-int/2addr v0, v1

    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->v()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 742
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->f:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 750
    :goto_0
    return v0

    .line 746
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 747
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->length()I

    move-result v0

    const-class v2, Lcom/twitter/android/composer/TweetBox$a;

    invoke-interface {v4, v1, v0, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/composer/TweetBox$a;

    array-length v5, v0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v3, v0, v2

    .line 748
    invoke-interface {v4, v3}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v4, v3}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    sub-int v3, v6, v3

    add-int/2addr v3, v1

    .line 747
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_1
    move v0, v1

    .line 750
    goto :goto_0
.end method

.method private w()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 897
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 898
    sget-object v3, Lcom/twitter/android/composer/TweetBox;->e:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 899
    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 900
    const/4 v0, 0x1

    .line 903
    :cond_0
    return v0

    .line 898
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private x()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 915
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->m:Z

    if-eqz v0, :cond_0

    .line 916
    iput-boolean v3, p0, Lcom/twitter/android/composer/TweetBox;->m:Z

    .line 917
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 918
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 919
    const-string/jumbo v2, "url_hints"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 920
    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    .line 921
    const v3, 0x7f0a06bb

    invoke-static {v0, v3}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;I)V

    .line 922
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 923
    const-string/jumbo v1, "url_hints"

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 924
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 927
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    check-cast p1, Lnk;

    check-cast p2, Lcom/twitter/android/provider/e;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/TweetBox;->a(Lnk;Lcom/twitter/android/provider/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lnk;Lcom/twitter/android/provider/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 861
    iget v0, p1, Lnk;->b:I

    invoke-static {v0, p2}, Lml;->a(ILcom/twitter/android/provider/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionStart()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->bringPointIntoView(I)Z

    .line 286
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->s:Z

    if-eqz v0, :cond_2

    .line 425
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->length()I

    move-result v0

    const-string/jumbo v1, " #alert"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    .line 426
    if-ltz v0, :cond_2

    .line 427
    if-le p1, v0, :cond_1

    .line 428
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1, v0, v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(II)V

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    if-le p2, v0, :cond_0

    .line 430
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(II)V

    goto :goto_0

    .line 435
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->f:Z

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/composer/TweetBox;->a(Landroid/widget/EditText;II)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Z)V
    .locals 1

    .prologue
    .line 316
    iput-boolean p2, p0, Lcom/twitter/android/composer/TweetBox;->v:Z

    .line 317
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    invoke-static {p1, v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    iput-object p1, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    .line 319
    if-eqz p1, :cond_1

    .line 320
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->k()V

    .line 321
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TweetBox;->setReplyToUsername(Ljava/lang/String;)V

    .line 325
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->r()V

    .line 327
    :cond_0
    return-void

    .line 323
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 619
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcbi;)V
    .locals 0

    .prologue
    .line 73
    check-cast p1, Lnk;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/TweetBox;->a(Lnk;Lcbi;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 640
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 641
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->g:Lnl;

    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 642
    invoke-virtual {v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionEnd()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lnl;->a_(Ljava/lang/CharSequence;I)Lnj$a;

    move-result-object v1

    .line 643
    if-eqz v1, :cond_0

    .line 644
    iget v2, v1, Lnj$a;->a:I

    iget v1, v1, Lnj$a;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v1, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 651
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 652
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 653
    return-void
.end method

.method public a(Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 355
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->a(Z)Z

    move-result v1

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setText(Ljava/lang/CharSequence;)V

    .line 358
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    aget v0, p2, v0

    const/4 v2, 0x1

    aget v2, p2, v2

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/composer/TweetBox;->b(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 359
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getTextLength()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TweetBox;->setCursorPosition(I)V

    .line 361
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->a(Z)Z

    .line 365
    return-void

    .line 363
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v2, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->a(Z)Z

    throw v0
.end method

.method public a(Lnk;Lcbi;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnk;",
            "Lcbi",
            "<",
            "Lcom/twitter/android/provider/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 849
    iget v0, p1, Lnk;->b:I

    if-ne v0, v6, :cond_0

    const-string/jumbo v0, "user"

    move-object v1, v0

    .line 850
    :goto_0
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v0, v6, [Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:autocomplete_dropdown"

    aput-object v4, v3, v7

    aput-object v1, v3, v6

    const/4 v1, 0x2

    const-string/jumbo v4, "results"

    aput-object v4, v3, v1

    .line 851
    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 850
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 852
    return-void

    .line 849
    :cond_0
    const-string/jumbo v0, "hashtag"

    move-object v1, v0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 589
    if-nez p1, :cond_0

    .line 590
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 591
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->clearFocus()V

    .line 592
    iput-boolean v2, p0, Lcom/twitter/android/composer/TweetBox;->j:Z

    .line 600
    :goto_0
    return-void

    .line 593
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 594
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->requestFocus()Z

    .line 595
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-static {v0, v1, v3}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 596
    iput-boolean v2, p0, Lcom/twitter/android/composer/TweetBox;->j:Z

    goto :goto_0

    .line 598
    :cond_1
    iput-boolean v3, p0, Lcom/twitter/android/composer/TweetBox;->j:Z

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;JLjava/lang/Object;I)Z
    .locals 6

    .prologue
    .line 73
    move-object v1, p1

    check-cast v1, Lnk;

    move-object v4, p4

    check-cast v4, Lcom/twitter/android/provider/e;

    move-object v0, p0

    move-wide v2, p2

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/composer/TweetBox;->a(Lnk;JLcom/twitter/android/provider/e;I)Z

    move-result v0

    return v0
.end method

.method public a(Lnk;JLcom/twitter/android/provider/e;I)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 838
    iget v0, p1, Lnk;->b:I

    if-ne v0, v7, :cond_0

    const-string/jumbo v0, "user"

    move-object v1, v0

    .line 839
    :goto_0
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v0, v7, [Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:autocomplete_dropdown"

    aput-object v4, v3, v6

    aput-object v1, v3, v7

    const/4 v1, 0x2

    const-string/jumbo v4, "select"

    aput-object v4, v3, v1

    .line 840
    invoke-static {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 839
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 841
    return v6

    .line 838
    :cond_0
    const-string/jumbo v0, "hashtag"

    move-object v1, v0

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 8

    .prologue
    const/4 v0, 0x5

    const/4 v1, 0x3

    const/4 v7, 0x0

    .line 795
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/android/composer/TweetBox;->k:Z

    .line 797
    sget-object v2, Lmb;->h:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 798
    iput v7, p0, Lcom/twitter/android/composer/TweetBox;->l:I

    .line 799
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 800
    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 801
    iget v4, p0, Lcom/twitter/android/composer/TweetBox;->l:I

    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/android/client/y;->a(Landroid/content/Context;)Lcom/twitter/android/client/y;

    move-result-object v5

    .line 802
    invoke-static {v3}, Lcom/twitter/util/ac;->c(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/twitter/android/client/y;->a(Z)I

    move-result v5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v3, v5, v3

    add-int/2addr v3, v4

    iput v3, p0, Lcom/twitter/android/composer/TweetBox;->l:I

    goto :goto_0

    .line 804
    :cond_0
    iget v2, p0, Lcom/twitter/android/composer/TweetBox;->l:I

    if-lez v2, :cond_1

    .line 805
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->x()V

    .line 807
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->l()V

    .line 809
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 810
    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-interface {p1, v7}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/twitter/util/b;->a(C)Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_1
    invoke-virtual {v2, v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setGravity(I)V

    .line 816
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->t:Landroid/text/TextWatcher;

    if-eqz v0, :cond_3

    .line 817
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->t:Landroid/text/TextWatcher;

    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 819
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 810
    goto :goto_1

    .line 812
    :cond_5
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 813
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setGravity(I)V

    goto :goto_2
.end method

.method public b()V
    .locals 1

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getTextLength()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TweetBox;->setCursorPosition(I)V

    .line 369
    return-void
.end method

.method public b(II)Z
    .locals 1

    .prologue
    .line 578
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->length()I

    move-result v0

    if-gt p2, v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(II)V

    .line 580
    const/4 v0, 0x1

    .line 582
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->t:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->t:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 784
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 477
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 856
    return-void
.end method

.method public e()Z
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    const/4 v4, 0x0

    .line 487
    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 488
    invoke-virtual {v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getMeasuredWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v6, v2

    move-wide v2, v0

    move v7, v4

    .line 487
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 489
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 490
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 491
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 492
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 493
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->performLongClick()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->n:Z

    if-nez v0, :cond_1

    .line 502
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->c:Z

    if-eqz v0, :cond_0

    .line 503
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->c:Z

    .line 504
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->k()V

    .line 506
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->n:Z

    .line 508
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->l()V

    .line 509
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 516
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->n:Z

    .line 517
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->l()V

    .line 518
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 754
    iget v0, p0, Lcom/twitter/android/composer/TweetBox;->q:I

    return v0
.end method

.method public getInputType()I
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getInputType()I

    move-result v0

    return v0
.end method

.method public getMaxLines()I
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getMaxLines()I

    move-result v0

    return v0
.end method

.method public getSelection()[I
    .locals 3

    .prologue
    .line 557
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionStart()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionEnd()I

    move-result v2

    aput v2, v0, v1

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 547
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->length()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/composer/TweetBox;->c(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextLength()I
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->length()I

    move-result v0

    return v0
.end method

.method public getTokenAtCursor()Lnk;
    .locals 3

    .prologue
    .line 636
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->g:Lnl;

    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionEnd()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lnl;->c(Ljava/lang/CharSequence;I)Lnk;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 521
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->o:Z

    if-nez v0, :cond_1

    .line 522
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->c:Z

    if-eqz v0, :cond_0

    .line 523
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->c:Z

    .line 524
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->k()V

    .line 526
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->o:Z

    .line 528
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->l()V

    .line 529
    return-void
.end method

.method public hasFocus()Z
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->hasFocus()Z

    move-result v0

    return v0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->o:Z

    .line 533
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->l()V

    .line 534
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->requestFocus()Z

    move-result v0

    return v0
.end method

.method k()V
    .locals 1

    .prologue
    .line 669
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getTextLength()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->v:Z

    if-eqz v0, :cond_1

    .line 679
    :cond_0
    :goto_0
    return-void

    .line 674
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_2

    .line 675
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/TweetBox;->a(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 676
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->w:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->w:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/TweetBox;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public l()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 758
    iget v0, p0, Lcom/twitter/android/composer/TweetBox;->u:I

    if-ne v0, v1, :cond_1

    .line 761
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/TweetBox$b;->a(I)V

    .line 773
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->u()I

    move-result v0

    .line 766
    iget v1, p0, Lcom/twitter/android/composer/TweetBox;->q:I

    if-eq v1, v0, :cond_0

    .line 767
    iput v0, p0, Lcom/twitter/android/composer/TweetBox;->q:I

    .line 768
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    iget v1, p0, Lcom/twitter/android/composer/TweetBox;->q:I

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/TweetBox$b;->a(I)V

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 865
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->r:Lcom/twitter/model/core/r;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 875
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_1

    move v1, v2

    .line 888
    :cond_0
    :goto_0
    return v1

    .line 878
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->v:Z

    if-nez v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    .line 888
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->x:Ljava/util/regex/Pattern;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->x:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 893
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    if-eqz v0, :cond_0

    const/16 v0, 0x65

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 824
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    invoke-interface {v0}, Lcom/twitter/android/composer/TweetBox$b;->b()V

    .line 826
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 131
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 134
    const v0, 0x7f13027a

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TweetBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;

    .line 135
    if-nez v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No edit text found in layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    invoke-virtual {v0, p0}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    invoke-virtual {v0, p0}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 140
    invoke-virtual {v0, p0}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setSuggestionListener(Lcom/twitter/android/autocomplete/SuggestionEditText$e;)V

    .line 141
    invoke-virtual {v0, p0}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setSuggestionStringConverter(Lcom/twitter/android/autocomplete/SuggestionEditText$f;)V

    .line 142
    invoke-virtual {v0, p0}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setSelectionChangeListener(Lcom/twitter/android/autocomplete/SuggestionEditText$d;)V

    .line 143
    new-instance v2, Lcom/twitter/android/composer/TweetBox$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/TweetBox$1;-><init>(Lcom/twitter/android/composer/TweetBox;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 154
    new-instance v2, Lcom/twitter/android/composer/TweetBox$2;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/TweetBox$2;-><init>(Lcom/twitter/android/composer/TweetBox;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setKeyPreImeListener(Lcom/twitter/android/autocomplete/SuggestionEditText$a;)V

    .line 161
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 162
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setGravity(I)V

    .line 164
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a06c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x65

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 165
    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 166
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->q()V

    .line 171
    :goto_0
    new-instance v2, Lcom/twitter/android/composer/TweetBox$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/TweetBox$3;-><init>(Lcom/twitter/android/composer/TweetBox;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 190
    invoke-static {v1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 191
    new-instance v2, Lml;

    invoke-direct {v2, v1}, Lml;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/twitter/android/composer/TweetBox;->y:Lml;

    .line 192
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->y:Lml;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setAdapter(Lmq;)V

    .line 194
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->g:Lnl;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setTokenizer(Lnj;)V

    .line 195
    iget-boolean v1, p0, Lcom/twitter/android/composer/TweetBox;->f:Z

    if-eqz v1, :cond_2

    .line 196
    new-instance v1, Lcom/twitter/android/composer/w;

    new-instance v2, Lnl;

    invoke-direct {v2}, Lnl;-><init>()V

    invoke-virtual {v2, v4}, Lnl;->b(Z)Lnl;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/composer/TweetBox$4;

    invoke-direct {v3, p0}, Lcom/twitter/android/composer/TweetBox$4;-><init>(Lcom/twitter/android/composer/TweetBox;)V

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/composer/w;-><init>(Lnj;Lcom/twitter/android/composer/w$b;)V

    .line 206
    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/w;->a(Landroid/widget/EditText;)V

    .line 207
    new-instance v1, Lcom/twitter/android/composer/TweetBox$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/TweetBox$5;-><init>(Lcom/twitter/android/composer/TweetBox;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/PopupSuggestionEditText;->setCopyTransformer(Lcom/twitter/android/autocomplete/SuggestionEditText$b;)V

    .line 214
    :cond_2
    iput-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 216
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->k()V

    .line 217
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->s:Z

    if-eqz v0, :cond_3

    .line 218
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->s()V

    .line 220
    :cond_3
    return-void

    .line 168
    :cond_4
    iput-boolean v4, p0, Lcom/twitter/android/composer/TweetBox;->c:Z

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 270
    move-object v0, p1

    check-cast v0, Lcom/twitter/android/composer/TweetBox$SavedState;

    .line 271
    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    move-object v0, p1

    .line 272
    check-cast v0, Lcom/twitter/android/composer/TweetBox$SavedState;

    iget-boolean v0, v0, Lcom/twitter/android/composer/TweetBox$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->m:Z

    .line 273
    check-cast p1, Lcom/twitter/android/composer/TweetBox$SavedState;

    iget-boolean v0, p1, Lcom/twitter/android/composer/TweetBox$SavedState;->b:Z

    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->v:Z

    .line 274
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 265
    new-instance v0, Lcom/twitter/android/composer/TweetBox$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/composer/TweetBox;->m:Z

    iget-boolean v3, p0, Lcom/twitter/android/composer/TweetBox;->v:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/composer/TweetBox$SavedState;-><init>(Landroid/os/Parcelable;ZZ)V

    return-object v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->t:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 789
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->t:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 791
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 604
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 605
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->j:Z

    if-eqz v0, :cond_0

    .line 606
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 608
    :cond_0
    return-void
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 907
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/composer/TweetBox;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/composer/TweetBox;->q:I

    iget v1, p0, Lcom/twitter/android/composer/TweetBox;->u:I

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlertHashtag(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 388
    iput-boolean p1, p0, Lcom/twitter/android/composer/TweetBox;->s:Z

    .line 389
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 390
    const-string/jumbo v1, " #alert"

    .line 391
    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionStart()I

    move-result v1

    .line 392
    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getSelectionEnd()I

    move-result v2

    .line 393
    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 394
    if-eqz p1, :cond_1

    .line 395
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " #alert"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 396
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 397
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 398
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    .line 399
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f11002a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v3, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 400
    const-string/jumbo v6, " #alert"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int v6, v4, v6

    const/16 v7, 0x21

    invoke-virtual {v5, v3, v6, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 402
    invoke-virtual {v0, v5}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setText(Ljava/lang/CharSequence;)V

    .line 403
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(II)V

    .line 404
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->s()V

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 407
    const-string/jumbo v5, " #alert"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int v5, v4, v5

    .line 408
    new-array v6, v7, [Landroid/text/InputFilter;

    invoke-virtual {v0, v6}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 409
    if-ltz v5, :cond_0

    const-string/jumbo v6, " #alert"

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 410
    invoke-virtual {v3, v7, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setText(Ljava/lang/CharSequence;)V

    .line 411
    if-le v1, v5, :cond_2

    .line 412
    invoke-virtual {v0, v5, v5}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(II)V

    goto :goto_0

    .line 413
    :cond_2
    if-le v2, v5, :cond_3

    .line 414
    invoke-virtual {v0, v1, v5}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(II)V

    goto :goto_0

    .line 416
    :cond_3
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(II)V

    goto :goto_0
.end method

.method public setAttachmentsUseSecureUrls(Z)V
    .locals 0

    .prologue
    .line 776
    iput-boolean p1, p0, Lcom/twitter/android/composer/TweetBox;->p:Z

    .line 777
    return-void
.end method

.method public setCursorPosition(I)V
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSelection(I)V

    .line 572
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setCursorVisible(Z)V

    .line 295
    return-void
.end method

.method public setEditTextOnKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 238
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 254
    return-void
.end method

.method public setExcludedRecipientIds(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->z:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 331
    if-eqz p1, :cond_0

    .line 332
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->z:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 334
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/composer/TweetBox;->r()V

    .line 335
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox;->c:Z

    .line 290
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 291
    return-void

    .line 289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImeActionLabel(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    const/16 v1, 0x65

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 615
    return-void
.end method

.method public setInputType(I)V
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setInputType(I)V

    .line 628
    return-void
.end method

.method public setMaxChars(I)V
    .locals 0

    .prologue
    .line 830
    iput p1, p0, Lcom/twitter/android/composer/TweetBox;->u:I

    .line 831
    return-void
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setMaxLines(I)V

    .line 246
    return-void
.end method

.method public setPrefillText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/twitter/android/composer/TweetBox;->w:Ljava/lang/String;

    .line 305
    return-void
.end method

.method public setQuote(Lcom/twitter/model/core/r;)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lcom/twitter/android/composer/TweetBox;->r:Lcom/twitter/model/core/r;

    .line 542
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->l()V

    .line 543
    return-void
.end method

.method public setReplyToUsername(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 869
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "(?:^|\\s)@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "(?:\\s|$)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TweetBox;->x:Ljava/util/regex/Pattern;

    .line 872
    return-void
.end method

.method public setSession(Lcom/twitter/library/client/Session;)V
    .locals 5

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    :goto_0
    return-void

    .line 226
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/composer/TweetBox;->h:Lcom/twitter/library/client/Session;

    .line 227
    invoke-virtual {p0}, Lcom/twitter/android/composer/TweetBox;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    new-instance v2, Lmv;

    new-instance v3, Lnd;

    const-string/jumbo v4, "compose"

    invoke-direct {v3, v0, p1, v4}, Lnd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    invoke-direct {v2, v0, v3}, Lmv;-><init>(Landroid/content/Context;Lnd;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSuggestionProvider(Lna;)V

    goto :goto_0
.end method

.method public setSingleLine(Z)V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setSingleLine(Z)V

    .line 242
    return-void
.end method

.method public setSuggestionsEnabled(Z)V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/twitter/android/composer/TweetBox;->a:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->a(Z)Z

    .line 278
    return-void
.end method

.method public setTextWatcher(Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/twitter/android/composer/TweetBox;->t:Landroid/text/TextWatcher;

    .line 282
    return-void
.end method

.method public setTweetBoxListener(Lcom/twitter/android/composer/TweetBox$b;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/twitter/android/composer/TweetBox;->b:Lcom/twitter/android/composer/TweetBox$b;

    .line 309
    return-void
.end method
