.class Lcom/twitter/android/composer/ComposerActivity$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/selection/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/composer/ComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/ComposerActivity;

.field private final b:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/ComposerActivity;Z)V
    .locals 0

    .prologue
    .line 3146
    iput-object p1, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3147
    iput-boolean p2, p0, Lcom/twitter/android/composer/ComposerActivity$b;->b:Z

    .line 3148
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 2

    .prologue
    .line 3173
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-boolean v1, p0, Lcom/twitter/android/composer/ComposerActivity$b;->b:Z

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(Lcom/twitter/android/media/selection/b;Z)V

    .line 3174
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3152
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v1, v1, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/f;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3153
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    const v3, 0x7f0a06bc

    .line 3155
    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/ComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3153
    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 3157
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 3168
    :cond_0
    :goto_0
    return v0

    .line 3160
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    iget-object v1, v1, Lcom/twitter/android/composer/ComposerActivity;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->b()Lcom/twitter/media/model/MediaType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/f;->a(Lcom/twitter/media/model/MediaType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3164
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 3165
    if-eqz v0, :cond_2

    .line 3166
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerActivity;->f(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity$b;->a:Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->d()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 3168
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
