.class public Lcom/twitter/android/composer/a;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/android/composer/a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 109
    return-void
.end method

.method protected constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/twitter/app/common/base/h;-><init>(Landroid/content/Intent;)V

    .line 113
    return-void
.end method

.method public static a(Landroid/app/Activity;I)Lakt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Landroid/app/Activity;",
            ":",
            "Lcom/twitter/app/common/util/j;",
            ">(TA;I)",
            "Lakt",
            "<",
            "Lcom/twitter/android/composer/a;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 589
    new-instance v0, Laks;

    const-class v1, Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {v0, p0, v1, p1}, Laks;-><init>(Landroid/app/Activity;Ljava/lang/Class;I)V

    return-object v0
.end method

.method public static a()Lcom/twitter/android/composer/a;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/android/composer/a;

    invoke-direct {v0}, Lcom/twitter/android/composer/a;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/twitter/android/composer/ComposerActivity;)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/android/composer/a;

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/composer/a;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 593
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 594
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 595
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 597
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 583
    const-class v0, Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "composer_mode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 132
    return-object p0
.end method

.method public a(J)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 385
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "replied_tweet_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 386
    return-object p0
.end method

.method public a(Landroid/net/Uri;)Lcom/twitter/android/composer/a;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 155
    return-object p0
.end method

.method public a(Lcax;)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 549
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "conversation_card_data"

    sget-object v2, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 550
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 335
    if-eqz p1, :cond_0

    .line 336
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "pc"

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 340
    :goto_0
    return-object p0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "pc"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_item"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 297
    return-object p0
.end method

.method public a(Lcom/twitter/android/composer/n;)Lcom/twitter/android/composer/a;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p1, Lcom/twitter/android/composer/n;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    .line 309
    iget-object v0, p1, Lcom/twitter/android/composer/n;->b:Lcom/twitter/analytics/model/ScribeItem;

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/android/composer/a;

    .line 310
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "replied_tweet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 397
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/r;)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 438
    if-eqz p1, :cond_0

    .line 439
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "quoted_tweet"

    sget-object v2, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 443
    :goto_0
    return-object p0

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "quoted_tweet"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/drafts/a;)Lcom/twitter/android/composer/a;
    .locals 4

    .prologue
    .line 566
    iget-wide v0, p1, Lcom/twitter/model/drafts/a;->b:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/a;->b(J)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/drafts/a;->e:J

    .line 567
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/composer/a;->a(J)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    const/4 v2, 0x0

    .line 568
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    .line 569
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Ljava/util/List;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->h:Lcgi;

    .line 570
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Lcgi;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->g:Lcom/twitter/model/geo/c;

    .line 571
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/geo/c;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    .line 572
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->j:Ljava/lang/String;

    .line 573
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->c(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/drafts/a;->f:Z

    .line 574
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Z)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->m:Lcom/twitter/model/timeline/ap;

    .line 575
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->n:Ljava/lang/String;

    .line 576
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->d(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    .line 577
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Ljava/util/List;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 566
    return-object v0
.end method

.method public a(Lcom/twitter/model/geo/c;)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "geo_tag"

    sget-object v2, Lcom/twitter/model/geo/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 351
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "tweet_preview_info"

    sget-object v2, Lcom/twitter/model/timeline/ap;->a:Lcom/twitter/util/serialization/i;

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 409
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    return-object p0
.end method

.method public a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 246
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, p2, :cond_1

    .line 247
    :cond_0
    const/4 v0, 0x0

    .line 251
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    move-result-object v0

    return-object v0

    .line 249
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 256
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    invoke-virtual {p0, p2}, Lcom/twitter/android/composer/a;->a([I)Lcom/twitter/android/composer/a;

    .line 263
    :goto_0
    return-object p0

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 261
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/a;->a([I)Lcom/twitter/android/composer/a;

    goto :goto_0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/android/composer/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)",
            "Lcom/twitter/android/composer/a;"
        }
    .end annotation

    .prologue
    .line 464
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "attachments"

    sget-object v2, Lcom/twitter/model/drafts/DraftAttachment;->a:Lcom/twitter/util/serialization/l;

    .line 465
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 464
    invoke-static {v0, v1, p1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 466
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "tweet_text_edited"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 269
    return-object p0
.end method

.method public a([I)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 320
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 321
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "selection"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 325
    :goto_0
    return-object p0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "selection"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()I
    .locals 3

    .prologue
    .line 117
    const-string/jumbo v0, "twitter"

    iget-object v1, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 119
    const-string/jumbo v1, "post"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    const-string/jumbo v1, "poll"

    const-string/jumbo v2, "mode"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const/4 v0, 0x5

    .line 126
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "composer_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public b(I)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "highlight_target"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 144
    return-object p0
.end method

.method public b(J)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 452
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "draft_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 453
    return-object p0
.end method

.method public b(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;
    .locals 1

    .prologue
    .line 432
    new-instance v0, Lcom/twitter/model/core/r;

    invoke-direct {v0, p1}, Lcom/twitter/model/core/r;-><init>(Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/android/composer/a;

    .line 433
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 361
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "AbsFragmentActivity_account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    :goto_0
    return-object p0

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "AbsFragmentActivity_account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/android/composer/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/android/composer/a;"
        }
    .end annotation

    .prologue
    .line 534
    iget-object v1, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v2, "excluded_users"

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 535
    return-object p0

    .line 534
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public b(Z)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "reply_prefill_disabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 403
    return-object p0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 209
    const-string/jumbo v0, "twitter"

    iget-object v1, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 210
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 212
    const-string/jumbo v0, "text"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    if-nez v0, :cond_0

    .line 214
    const-string/jumbo v0, "message"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    :cond_0
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 218
    const-string/jumbo v4, "post"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 219
    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 221
    :goto_0
    const-string/jumbo v1, "url"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/composer/a;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 222
    const-string/jumbo v1, "hashtags"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 223
    if-eqz v1, :cond_2

    .line 224
    const-string/jumbo v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    .line 225
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/twitter/android/composer/a;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 219
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    .line 228
    :cond_2
    const-string/jumbo v1, "via"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 229
    if-eqz v1, :cond_3

    .line 230
    const v3, 0x7f0a098d

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v1, v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/composer/a;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 232
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 240
    :goto_2
    return-object v0

    .line 233
    :cond_4
    const-string/jumbo v4, "quote"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 234
    const v1, 0x7f0a0c1b

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string/jumbo v5, "screen_name"

    .line 235
    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    aput-object v0, v4, v9

    .line 234
    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 237
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 238
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 240
    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public c()I
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "highlight_target"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c(I)I
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "camera_mode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c(J)Lcom/twitter/android/composer/a;
    .locals 3

    .prologue
    .line 528
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "card_host_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 529
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "card_url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 519
    return-object p0
.end method

.method public d(I)Lcom/twitter/android/composer/a;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 172
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "engagement_metadata"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 556
    return-object p0
.end method

.method public d()Lcom/twitter/android/composer/s;
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "tweet_uploader_class"

    .line 184
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 185
    if-eqz v0, :cond_0

    .line 188
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/s;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    return-object v0

    .line 190
    :catch_0
    move-exception v0

    .line 193
    :cond_0
    :goto_1
    new-instance v0, Lcom/twitter/android/composer/z;

    invoke-direct {v0}, Lcom/twitter/android/composer/z;-><init>()V

    goto :goto_0

    .line 189
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "tweet_text_edited"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public f()[I
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "selection"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public g()Lcgi;
    .locals 3

    .prologue
    .line 330
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "pc"

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    return-object v0
.end method

.method public h()Lcom/twitter/model/geo/c;
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "geo_tag"

    sget-object v2, Lcom/twitter/model/geo/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/c;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "AbsFragmentActivity_account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()J
    .locals 4

    .prologue
    .line 370
    const-string/jumbo v0, "twitter"

    iget-object v1, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 372
    const-string/jumbo v1, "post"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    :try_start_0
    const-string/jumbo v1, "in_reply_to_status_id"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 380
    :goto_0
    return-wide v0

    .line 375
    :catch_0
    move-exception v0

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "replied_tweet_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public k()Lcom/twitter/model/core/Tweet;
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "replied_tweet"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public l()Lcom/twitter/model/timeline/ap;
    .locals 3

    .prologue
    .line 414
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "tweet_preview_info"

    sget-object v2, Lcom/twitter/model/timeline/ap;->a:Lcom/twitter/util/serialization/i;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ap;

    return-object v0
.end method

.method public m()Lcom/twitter/util/Tristate;
    .locals 3

    .prologue
    .line 418
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "reply_prefill_disabled"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "reply_prefill_disabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/Tristate;->a(Z)Lcom/twitter/util/Tristate;

    move-result-object v0

    .line 421
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/util/Tristate;->a:Lcom/twitter/util/Tristate;

    goto :goto_0
.end method

.method public n()Lcom/twitter/model/core/r;
    .locals 3

    .prologue
    .line 427
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "quoted_tweet"

    sget-object v2, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/r;

    return-object v0
.end method

.method public o()J
    .locals 4

    .prologue
    .line 447
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "draft_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public p()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "attachments"

    sget-object v2, Lcom/twitter/model/drafts/DraftAttachment;->a:Lcom/twitter/util/serialization/l;

    .line 459
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 458
    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public q()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method public r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public t()Lcom/twitter/analytics/model/ScribeItem;
    .locals 2

    .prologue
    .line 502
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_item"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeItem;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "card_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v()J
    .locals 4

    .prologue
    .line 523
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "card_host_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 540
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "excluded_users"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public x()Lcax;
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "conversation_card_data"

    sget-object v2, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 2

    .prologue
    .line 561
    iget-object v0, p0, Lcom/twitter/android/composer/a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "engagement_metadata"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
