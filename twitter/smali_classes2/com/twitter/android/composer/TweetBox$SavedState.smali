.class Lcom/twitter/android/composer/TweetBox$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/composer/TweetBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/composer/TweetBox$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 987
    new-instance v0, Lcom/twitter/android/composer/TweetBox$SavedState$1;

    invoke-direct {v0}, Lcom/twitter/android/composer/TweetBox$SavedState$1;-><init>()V

    sput-object v0, Lcom/twitter/android/composer/TweetBox$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1004
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1005
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/composer/TweetBox$SavedState;->a:Z

    .line 1006
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/android/composer/TweetBox$SavedState;->b:Z

    .line 1007
    return-void

    :cond_0
    move v0, v2

    .line 1005
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1006
    goto :goto_1
.end method

.method constructor <init>(Landroid/os/Parcelable;ZZ)V
    .locals 0

    .prologue
    .line 1010
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1011
    iput-boolean p2, p0, Lcom/twitter/android/composer/TweetBox$SavedState;->a:Z

    .line 1012
    iput-boolean p3, p0, Lcom/twitter/android/composer/TweetBox$SavedState;->b:Z

    .line 1013
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1017
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1018
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox$SavedState;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1019
    iget-boolean v0, p0, Lcom/twitter/android/composer/TweetBox$SavedState;->b:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1020
    return-void

    :cond_0
    move v0, v2

    .line 1018
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1019
    goto :goto_1
.end method
