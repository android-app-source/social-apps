.class public final Lcom/twitter/android/composer/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/u$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lpv;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/composer/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/twitter/android/composer/u;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/composer/u;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/composer/u$a;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-boolean v0, Lcom/twitter/android/composer/u;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/u;->a(Lcom/twitter/android/composer/u$a;)V

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/composer/u$a;Lcom/twitter/android/composer/u$1;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/u;-><init>(Lcom/twitter/android/composer/u$a;)V

    return-void
.end method

.method private a(Lcom/twitter/android/composer/u$a;)V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/android/composer/u$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/composer/u$1;-><init>(Lcom/twitter/android/composer/u;Lcom/twitter/android/composer/u$a;)V

    iput-object v0, p0, Lcom/twitter/android/composer/u;->b:Lcta;

    .line 60
    iget-object v0, p0, Lcom/twitter/android/composer/u;->b:Lcta;

    .line 62
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 61
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/u;->c:Lcta;

    .line 64
    new-instance v0, Lcom/twitter/android/composer/u$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/composer/u$2;-><init>(Lcom/twitter/android/composer/u;Lcom/twitter/android/composer/u$a;)V

    iput-object v0, p0, Lcom/twitter/android/composer/u;->d:Lcta;

    .line 80
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/u;->d:Lcta;

    .line 79
    invoke-static {v0, v1}, Lcom/twitter/android/composer/c;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 78
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/u;->e:Lcta;

    .line 82
    return-void
.end method

.method public static d()Lcom/twitter/android/composer/u$a;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/android/composer/u$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/composer/u$a;-><init>(Lcom/twitter/android/composer/u$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/composer/b;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/composer/u;->e:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/b;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/composer/u;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method
