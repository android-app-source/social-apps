.class Lcom/twitter/android/composer/l$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/l;->a(Landroid/content/Context;Z)[Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/android/composer/l;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/l;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    iput-object p2, p0, Lcom/twitter/android/composer/l$2;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 278
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 281
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->b(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->o()V

    .line 282
    iget-object v0, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->a(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/composer/h;->y()V

    goto :goto_0

    .line 286
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->c(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/e;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/e;->c(I)V

    goto :goto_0

    .line 290
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->c(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/e;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/e;->c(I)V

    goto :goto_0

    .line 294
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->d(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/m;

    move-result-object v0

    const/16 v1, 0x20a

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/m;->d(I)V

    .line 295
    iget-object v0, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    invoke-static {v0}, Lcom/twitter/android/composer/l;->b(Lcom/twitter/android/composer/l;)Lcom/twitter/android/composer/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/l$2;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/composer/l$2;->b:Lcom/twitter/android/composer/l;

    invoke-static {v2}, Lcom/twitter/android/composer/l;->e(Lcom/twitter/android/composer/l;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/o;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x7f13003b
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
