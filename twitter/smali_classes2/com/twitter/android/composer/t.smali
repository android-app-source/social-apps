.class public Lcom/twitter/android/composer/t;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/t$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/composer/f;

.field private final b:Lcom/twitter/android/composer/o;

.field private final c:Lcom/twitter/android/composer/TweetBox;

.field private final d:Lcom/twitter/android/widget/ComposerSelectionFragment;

.field private e:Lcom/twitter/android/composer/t$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/composer/f;Lcom/twitter/android/composer/TweetBox;Lcom/twitter/android/composer/o;)V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p4, p0, Lcom/twitter/android/composer/t;->c:Lcom/twitter/android/composer/TweetBox;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/composer/t;->a:Lcom/twitter/android/composer/f;

    .line 40
    iput-object p5, p0, Lcom/twitter/android/composer/t;->b:Lcom/twitter/android/composer/o;

    .line 44
    iget-object v0, p0, Lcom/twitter/android/composer/t;->c:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setSuggestionsEnabled(Z)V

    .line 46
    invoke-static {p1}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    .line 98
    :goto_0
    return-void

    .line 52
    :cond_0
    const-string/jumbo v0, "user_select"

    .line 53
    invoke-virtual {p2, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ComposerSelectionFragment;

    .line 54
    if-nez v0, :cond_1

    .line 55
    new-instance v0, Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-direct {v0}, Lcom/twitter/android/widget/ComposerSelectionFragment;-><init>()V

    .line 57
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f130286

    const-string/jumbo v3, "user_select"

    .line 58
    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 61
    :cond_1
    iput-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    .line 62
    new-instance v1, Lcom/twitter/android/composer/t$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/t$1;-><init>(Lcom/twitter/android/composer/t;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Lcom/twitter/android/widget/ComposerSelectionFragment$b;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/composer/t;)Lcom/twitter/android/composer/t$a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/android/composer/t;->e:Lcom/twitter/android/composer/t$a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/composer/t;)Lcom/twitter/android/composer/TweetBox;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/android/composer/t;->c:Lcom/twitter/android/composer/TweetBox;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/composer/t;)Lcom/twitter/android/composer/o;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/android/composer/t;->b:Lcom/twitter/android/composer/o;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    iget-object v1, p0, Lcom/twitter/android/composer/t;->c:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->getTokenAtCursor()Lnk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Lnk;)V

    .line 134
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    if-nez v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/t;->a:Lcom/twitter/android/composer/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/f;->i()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 141
    if-nez v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Ljava/util/Collection;)V

    goto :goto_0

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    iget-object v2, p0, Lcom/twitter/android/composer/t;->a:Lcom/twitter/android/composer/f;

    .line 149
    invoke-virtual {v2}, Lcom/twitter/android/composer/f;->h()Ljava/util/List;

    move-result-object v2

    .line 146
    invoke-static {v0, p1, p2, v2}, Lbxd;->a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    .line 145
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public a(Landroid/support/v4/app/FragmentTransaction;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 107
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->j()V

    .line 109
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/composer/t$a;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/twitter/android/composer/t;->e:Lcom/twitter/android/composer/t$a;

    .line 102
    return-void
.end method

.method public a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Lcom/twitter/android/widget/DraggableDrawerLayout;)V

    .line 128
    :cond_0
    return-void
.end method

.method public a(Lnk;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/ComposerSelectionFragment;->a(Lnk;)V

    .line 120
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerSelectionFragment;->m()V

    .line 122
    :cond_0
    return-void
.end method

.method public b(Landroid/support/v4/app/FragmentTransaction;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/composer/t;->d:Lcom/twitter/android/widget/ComposerSelectionFragment;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 115
    :cond_0
    return-void
.end method
