.class Lcom/twitter/android/composer/geotag/a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ComposerLocationFragment$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/composer/geotag/a;-><init>(Landroid/content/Context;Lcom/twitter/android/composer/geotag/b;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbqn;Lcom/twitter/android/geo/a;Lcom/twitter/android/widget/ComposerPoiFragment;Lcom/twitter/android/composer/geotag/f;Landroid/widget/TextView;Lcom/twitter/android/widget/ToggleImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/composer/geotag/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/composer/geotag/a;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/twitter/android/composer/geotag/a$1;->a:Lcom/twitter/android/composer/geotag/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a$1;->a:Lcom/twitter/android/composer/geotag/a;

    invoke-static {v0}, Lcom/twitter/android/composer/geotag/a;->a(Lcom/twitter/android/composer/geotag/a;)Lcom/twitter/android/composer/geotag/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a$1;->a:Lcom/twitter/android/composer/geotag/a;

    invoke-virtual {v1}, Lcom/twitter/android/composer/geotag/a;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/geotag/b;->a(Lcom/twitter/android/geo/GeoTagState;)V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a$1;->a:Lcom/twitter/android/composer/geotag/a;

    invoke-static {v0}, Lcom/twitter/android/composer/geotag/a;->b(Lcom/twitter/android/composer/geotag/a;)Lcom/twitter/android/widget/ToggleImageButton;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 103
    return-void

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a$1;->a:Lcom/twitter/android/composer/geotag/a;

    invoke-static {v0}, Lcom/twitter/android/composer/geotag/a;->a(Lcom/twitter/android/composer/geotag/a;)Lcom/twitter/android/composer/geotag/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/composer/geotag/b;->a(Z)V

    .line 97
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a$1;->a:Lcom/twitter/android/composer/geotag/a;

    invoke-static {v0}, Lcom/twitter/android/composer/geotag/a;->a(Lcom/twitter/android/composer/geotag/a;)Lcom/twitter/android/composer/geotag/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/composer/geotag/b;->b(Z)V

    .line 108
    return-void
.end method
