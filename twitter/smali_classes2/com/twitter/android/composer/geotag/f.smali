.class public Lcom/twitter/android/composer/geotag/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/geotag/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/composer/geotag/f$a;,
        Lcom/twitter/android/composer/geotag/f$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/composer/geotag/f$b;

.field private final b:Lcom/twitter/android/geo/PlacePickerModel;

.field private c:Lcom/twitter/library/client/Session;

.field private d:Lcom/twitter/android/composer/geotag/f$a;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/composer/geotag/f$b;Lcom/twitter/library/client/Session;Lcom/twitter/android/geo/PlacePickerModel;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/twitter/android/composer/geotag/f;->a:Lcom/twitter/android/composer/geotag/f$b;

    .line 38
    iput-object p2, p0, Lcom/twitter/android/composer/geotag/f;->c:Lcom/twitter/library/client/Session;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/composer/geotag/f;->b:Lcom/twitter/android/geo/PlacePickerModel;

    .line 41
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->b:Lcom/twitter/android/geo/PlacePickerModel;

    new-instance v1, Lcom/twitter/android/composer/geotag/f$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/geotag/f$1;-><init>(Lcom/twitter/android/composer/geotag/f;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/geo/PlacePickerModel;->registerObserver(Ljava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->a:Lcom/twitter/android/composer/geotag/f$b;

    invoke-interface {v0, p0}, Lcom/twitter/android/composer/geotag/f$b;->setViewListener(Lcom/twitter/android/composer/geotag/g;)V

    .line 54
    return-void
.end method

.method private static a(Lcom/twitter/android/geo/b;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/geo/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x5

    .line 94
    if-eqz p0, :cond_1

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/twitter/android/geo/b;->c()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 96
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 97
    const/4 v1, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 101
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/composer/geotag/f;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "compose"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "poi"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 106
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 107
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->b:Lcom/twitter/android/geo/PlacePickerModel;

    invoke-virtual {v0}, Lcom/twitter/android/geo/PlacePickerModel;->c()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v1

    .line 72
    iget-boolean v0, p0, Lcom/twitter/android/composer/geotag/f;->e:Z

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->a:Lcom/twitter/android/composer/geotag/f$b;

    invoke-interface {v0}, Lcom/twitter/android/composer/geotag/f$b;->a()V

    .line 80
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/composer/geotag/f;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->a:Lcom/twitter/android/composer/geotag/f$b;

    invoke-interface {v0}, Lcom/twitter/android/composer/geotag/f$b;->b()V

    .line 86
    :goto_1
    return-void

    .line 76
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/composer/geotag/f;->a:Lcom/twitter/android/composer/geotag/f$b;

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->e()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    :goto_2
    invoke-interface {v2, v0}, Lcom/twitter/android/composer/geotag/f$b;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->b:Lcom/twitter/android/geo/PlacePickerModel;

    sget-object v1, Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;->a:Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    invoke-virtual {v0, v1}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;)Lcom/twitter/android/geo/b;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/f;->a:Lcom/twitter/android/composer/geotag/f$b;

    invoke-static {v0}, Lcom/twitter/android/composer/geotag/f;->a(Lcom/twitter/android/geo/b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/twitter/android/composer/geotag/f$b;->a(Ljava/util/List;)V

    goto :goto_1
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->a:Lcom/twitter/android/composer/geotag/f$b;

    invoke-interface {v0, p1}, Lcom/twitter/android/composer/geotag/f$b;->setVisibility(I)V

    .line 90
    return-void
.end method

.method public a(Lcom/twitter/android/composer/geotag/f$a;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    .line 62
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/android/composer/geotag/f;->c:Lcom/twitter/library/client/Session;

    .line 58
    return-void
.end method

.method public a(Lcom/twitter/model/geo/TwitterPlace;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/composer/geotag/f$a;->a(Lcom/twitter/model/geo/TwitterPlace;)V

    .line 145
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/twitter/android/composer/geotag/f;->e:Z

    .line 66
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 116
    const-string/jumbo v0, "add_location"

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/geotag/f;->a(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    invoke-interface {v0}, Lcom/twitter/android/composer/geotag/f$a;->h()V

    .line 120
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 125
    const-string/jumbo v0, "poi_tag"

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/geotag/f;->a(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    invoke-interface {v0}, Lcom/twitter/android/composer/geotag/f$a;->h()V

    .line 129
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 134
    const-string/jumbo v0, "search_locations"

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/geotag/f;->a(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/f;->d:Lcom/twitter/android/composer/geotag/f$a;

    invoke-interface {v0}, Lcom/twitter/android/composer/geotag/f$a;->h()V

    .line 138
    :cond_0
    return-void
.end method
