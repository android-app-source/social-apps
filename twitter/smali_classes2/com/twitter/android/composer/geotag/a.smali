.class public Lcom/twitter/android/composer/geotag/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/geotag/f$a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/composer/geotag/b;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Lbqn;

.field private final e:Lcom/twitter/android/widget/ComposerPoiFragment;

.field private final f:Lcom/twitter/android/geo/a;

.field private final g:Lcom/twitter/android/composer/geotag/f;

.field private final h:Landroid/widget/TextView;

.field private final i:Lcom/twitter/android/widget/ToggleImageButton;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/ImageView;

.field private m:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/composer/geotag/b;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbqn;Lcom/twitter/android/geo/a;Lcom/twitter/android/widget/ComposerPoiFragment;Lcom/twitter/android/composer/geotag/f;Landroid/widget/TextView;Lcom/twitter/android/widget/ToggleImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/twitter/android/composer/geotag/a;->a:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/twitter/android/composer/geotag/a;->b:Lcom/twitter/android/composer/geotag/b;

    .line 80
    iput-object p3, p0, Lcom/twitter/android/composer/geotag/a;->m:Lcom/twitter/library/client/Session;

    .line 81
    iput-object p4, p0, Lcom/twitter/android/composer/geotag/a;->c:Lcom/twitter/library/client/p;

    .line 82
    iput-object p5, p0, Lcom/twitter/android/composer/geotag/a;->d:Lbqn;

    .line 83
    iput-object p6, p0, Lcom/twitter/android/composer/geotag/a;->f:Lcom/twitter/android/geo/a;

    .line 84
    iput-object p7, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    .line 85
    iput-object p8, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    .line 86
    iput-object p9, p0, Lcom/twitter/android/composer/geotag/a;->h:Landroid/widget/TextView;

    .line 87
    iput-object p10, p0, Lcom/twitter/android/composer/geotag/a;->i:Lcom/twitter/android/widget/ToggleImageButton;

    .line 88
    iput-object p11, p0, Lcom/twitter/android/composer/geotag/a;->j:Landroid/view/View;

    .line 89
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/composer/geotag/a;->k:Landroid/widget/TextView;

    .line 90
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/composer/geotag/a;->l:Landroid/widget/ImageView;

    .line 92
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    new-instance v2, Lcom/twitter/android/composer/geotag/a$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/geotag/a$1;-><init>(Lcom/twitter/android/composer/geotag/a;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/widget/ComposerLocationFragment$a;)V

    .line 111
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v1, p0}, Lcom/twitter/android/composer/geotag/f;->a(Lcom/twitter/android/composer/geotag/f$a;)V

    .line 113
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->i:Lcom/twitter/android/widget/ToggleImageButton;

    iget-object v2, p0, Lcom/twitter/android/composer/geotag/a;->b:Lcom/twitter/android/composer/geotag/b;

    new-instance v3, Lcom/twitter/android/composer/geotag/a$2;

    invoke-direct {v3, p0}, Lcom/twitter/android/composer/geotag/a$2;-><init>(Lcom/twitter/android/composer/geotag/a;)V

    invoke-interface {v2, v3}, Lcom/twitter/android/composer/geotag/b;->a(Ljava/lang/Runnable;)Lcom/twitter/android/composer/ComposerActivity$a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ToggleImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/composer/geotag/a;->b:Lcom/twitter/android/composer/geotag/b;

    new-instance v3, Lcom/twitter/android/composer/geotag/a$3;

    invoke-direct {v3, p0}, Lcom/twitter/android/composer/geotag/a$3;-><init>(Lcom/twitter/android/composer/geotag/a;)V

    invoke-interface {v2, v3}, Lcom/twitter/android/composer/geotag/b;->a(Ljava/lang/Runnable;)Lcom/twitter/android/composer/ComposerActivity$a;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->f:Lcom/twitter/android/geo/a;

    iget-object v2, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v1, v2}, Lcom/twitter/android/geo/a;->a(Lcom/twitter/android/geo/a$a;)V

    .line 130
    invoke-direct {p0}, Lcom/twitter/android/composer/geotag/a;->i()V

    .line 132
    if-eqz p14, :cond_0

    .line 133
    move-object/from16 v0, p14

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/geotag/a;->b(Landroid/os/Bundle;)V

    .line 135
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/geotag/a;)Lcom/twitter/android/composer/geotag/b;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->b:Lcom/twitter/android/composer/geotag/b;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/composer/geotag/a;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/android/composer/geotag/a;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 230
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->m:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    const-string/jumbo v0, "compose:map::map_pin:close"

    :goto_0
    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 232
    return-void

    .line 230
    :cond_0
    const-string/jumbo v0, "compose:map::map_pin:open"

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/composer/geotag/a;)Lcom/twitter/android/widget/ToggleImageButton;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->i:Lcom/twitter/android/widget/ToggleImageButton;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->i:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/composer/geotag/a;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 275
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/geotag/f;->a()V

    .line 277
    const-string/jumbo v0, "bundle_geo_tag_module"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 278
    if-eqz v0, :cond_0

    .line 281
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/composer/geotag/a;)Lcom/twitter/android/widget/ComposerPoiFragment;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 138
    invoke-static {}, Lcom/twitter/android/composer/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/geotag/f;->a(I)V

    .line 145
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/geotag/f;->a(I)V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/android/geo/GeoTagState;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->d()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 284
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 286
    const-string/jumbo v1, "bundle_geo_tag_module"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 287
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 148
    iput-object p1, p0, Lcom/twitter/android/composer/geotag/a;->m:Lcom/twitter/library/client/Session;

    .line 149
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/geotag/f;->a(Lcom/twitter/library/client/Session;)V

    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/composer/geotag/a;->c()V

    .line 151
    return-void
.end method

.method public a(Lcom/twitter/model/geo/TwitterPlace;)V
    .locals 19

    .prologue
    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->j()Lcom/twitter/android/geo/PlacePickerModel;

    move-result-object v2

    .line 319
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v18

    .line 320
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    const-wide/high16 v8, 0x7ff8000000000000L    # NaN

    .line 325
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/android/geo/PlacePickerModel;->b(Lcom/twitter/model/geo/TwitterPlace;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    const/4 v12, 0x0

    .line 328
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/model/geo/TwitterPlace;)I

    move-result v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, -0x1

    .line 320
    invoke-virtual/range {v3 .. v17}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->a(Ljava/lang/String;Lcom/twitter/model/geo/TwitterPlace$PlaceType;DDLjava/lang/String;IIILjava/lang/String;Ljava/lang/String;J)Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    .line 332
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/composer/geotag/a;->m:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "compose"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "poi"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "poi_suggestion"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    .line 333
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 334
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 332
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/composer/geotag/a;->e()Lcom/twitter/android/geo/b;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 336
    new-instance v2, Lcom/twitter/android/geo/GeoTagState;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/model/geo/TwitterPlace;->h:Lcom/twitter/model/geo/b;

    .line 339
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/composer/geotag/a;->e()Lcom/twitter/android/geo/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/geo/b;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/geo/GeoTagState;-><init>(Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/model/geo/b;Ljava/lang/String;ZZZ)V

    .line 343
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v3, v2}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/geo/GeoTagState;)V

    .line 345
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 2

    .prologue
    .line 160
    if-nez p1, :cond_1

    .line 162
    invoke-virtual {p0}, Lcom/twitter/android/composer/geotag/a;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->j()Lcom/twitter/android/geo/PlacePickerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/geo/PlacePickerModel;->a()V

    .line 164
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/geo/GeoTagState;)V

    .line 165
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/geotag/f;->a(Z)V

    .line 166
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/geotag/f;->a()V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/geotag/f;->a(Z)V

    .line 170
    const-string/jumbo v0, "composer_geo_inline_location_picker_android_4125"

    invoke-static {v0}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 171
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    if-ne v0, v1, :cond_0

    .line 172
    :cond_2
    invoke-static {}, Lcom/twitter/android/composer/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->m:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lbqg;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    invoke-static {}, Lcom/twitter/android/composer/i;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    .line 175
    invoke-static {v0}, Lcom/twitter/media/util/e;->a(Ljava/io/File;)Lcom/twitter/model/geo/b;

    move-result-object v0

    .line 178
    :goto_1
    if-eqz v0, :cond_4

    .line 179
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/model/geo/b;)Z

    move-result v0

    .line 183
    :goto_2
    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v0}, Lcom/twitter/android/composer/geotag/f;->a()V

    goto :goto_0

    .line 175
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 181
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->i()Z

    move-result v0

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 215
    if-eqz p1, :cond_0

    .line 216
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->l:Landroid/widget/ImageView;

    const v1, 0x7f020695

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 219
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 225
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/composer/geotag/a;->i()V

    .line 226
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->k:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(ZI)V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(ZI)V

    .line 300
    return-void
.end method

.method public a([Ljava/lang/String;[I)V
    .locals 2

    .prologue
    .line 257
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    const-string/jumbo v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/util/android/f;->a(Ljava/lang/String;[Ljava/lang/String;[I)Z

    move-result v0

    .line 259
    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->b:Lcom/twitter/android/composer/geotag/b;

    invoke-interface {v1}, Lcom/twitter/android/composer/geotag/b;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->d(Z)V

    .line 269
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/geo/a;->b(Landroid/content/Context;)V

    .line 267
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqg;->a(Z)V

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->m:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 200
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->d:Lbqn;

    invoke-virtual {v1}, Lbqn;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 203
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->i:Lcom/twitter/android/widget/ToggleImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ToggleImageButton;->setVisibility(I)V

    .line 204
    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->c:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/composer/geotag/a;->m:Lcom/twitter/library/client/Session;

    .line 207
    invoke-static {v1, v2}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbbg;

    move-result-object v1

    const/4 v2, 0x0

    .line 206
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->i:Lcom/twitter/android/widget/ToggleImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ToggleImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()Lcom/twitter/model/geo/c;
    .locals 9

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/twitter/android/composer/geotag/a;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->f()Lcom/twitter/model/geo/c;

    move-result-object v5

    .line 242
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->a:Landroid/content/Context;

    .line 243
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/composer/geotag/a;->i:Lcom/twitter/android/widget/ToggleImageButton;

    iget-object v3, p0, Lcom/twitter/android/composer/geotag/a;->k:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/twitter/android/composer/geotag/a;->h:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    .line 244
    invoke-virtual {v6}, Lcom/twitter/android/widget/ComposerPoiFragment;->h()Z

    move-result v6

    iget-object v7, p0, Lcom/twitter/android/composer/geotag/a;->b:Lcom/twitter/android/composer/geotag/b;

    invoke-interface {v7}, Lcom/twitter/android/composer/geotag/b;->a()Z

    move-result v7

    .line 245
    invoke-static {}, Lcom/twitter/android/composer/i;->a()Z

    move-result v8

    .line 242
    invoke-static/range {v0 .. v8}, Lcom/twitter/android/composer/geotag/c;->a(Landroid/content/Context;Lbqg;Lcom/twitter/android/widget/ToggleImageButton;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/twitter/model/geo/c;ZZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    const/4 v5, 0x0

    .line 248
    :cond_0
    return-object v5
.end method

.method public e()Lcom/twitter/android/geo/b;
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->j()Lcom/twitter/android/geo/PlacePickerModel;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;->a:Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;

    invoke-virtual {v0, v1}, Lcom/twitter/android/geo/PlacePickerModel;->a(Lcom/twitter/android/geo/PlacePickerModel$PlaceListSource;)Lcom/twitter/android/geo/b;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 290
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/widget/ComposerLocationFragment$a;)V

    .line 291
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->g:Lcom/twitter/android/composer/geotag/f;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/geotag/f;->a(Lcom/twitter/android/composer/geotag/f$a;)V

    .line 292
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->f()V

    .line 296
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/twitter/android/composer/geotag/a;->a()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/twitter/android/composer/geotag/a;->e:Lcom/twitter/android/widget/ComposerPoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/geo/GeoTagState;->b()Lcom/twitter/android/geo/GeoTagState;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ComposerPoiFragment;->a(Lcom/twitter/android/geo/GeoTagState;)V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/geotag/a;->f:Lcom/twitter/android/geo/a;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/twitter/android/geo/a;->a(I)V

    .line 313
    return-void
.end method
