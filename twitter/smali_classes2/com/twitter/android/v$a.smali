.class public final Lcom/twitter/android/v$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/android/v;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/FlowData;

.field private final b:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 26
    new-instance v0, Lcom/twitter/android/FlowData;

    invoke-direct {v0}, Lcom/twitter/android/FlowData;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    .line 27
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/v$a;->b:Landroid/content/Intent;

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/FlowData;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    .line 32
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/v$a;->b:Landroid/content/Intent;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lcom/twitter/android/v$a;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->b(Landroid/content/Context;)V

    .line 68
    return-object p0
.end method

.method public a(Landroid/content/Intent;)Lcom/twitter/android/v$a;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/v$a;->b:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 92
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/v$a;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->d(Ljava/lang/String;)V

    .line 62
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/v$a;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->c(Z)V

    .line 38
    return-object p0
.end method

.method public b(Landroid/content/Context;)Lcom/twitter/android/v$a;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->a(Landroid/content/Context;)V

    .line 74
    return-object p0
.end method

.method public b(Z)Lcom/twitter/android/v$a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->g(Z)V

    .line 44
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/android/v$a;->c()Lcom/twitter/android/v;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)Lcom/twitter/android/v$a;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->a(Z)V

    .line 50
    return-object p0
.end method

.method protected c()Lcom/twitter/android/v;
    .locals 3

    .prologue
    .line 103
    new-instance v0, Lcom/twitter/android/v;

    iget-object v1, p0, Lcom/twitter/android/v$a;->b:Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/v;-><init>(Landroid/content/Intent;Lcom/twitter/android/v$1;)V

    return-object v0
.end method

.method protected c_()V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/v$a;->b:Landroid/content/Intent;

    const-string/jumbo v1, "flow_data"

    iget-object v2, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 98
    return-void
.end method

.method public d(Z)Lcom/twitter/android/v$a;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/v$a;->a:Lcom/twitter/android/FlowData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->b(Z)V

    .line 56
    return-object p0
.end method

.method public e(Z)Lcom/twitter/android/v$a;
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/v$a;->b:Landroid/content/Intent;

    const-string/jumbo v1, "phone100_signup_first_step_password"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 80
    return-object p0
.end method

.method public f(Z)Lcom/twitter/android/v$a;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/v$a;->b:Landroid/content/Intent;

    const-string/jumbo v1, "phone100_signup_first_step_add_phone"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 86
    return-object p0
.end method
