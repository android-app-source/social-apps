.class public Lcom/twitter/android/ct;
.super Lcom/twitter/library/view/a;
.source "Twttr"


# instance fields
.field private a:Z

.field protected final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field protected final c:Landroid/content/Context;

.field protected final d:Lcom/twitter/library/client/v;

.field protected final e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected final f:Lbxc;

.field protected final g:Lapb;

.field protected final h:Lcom/twitter/android/ck;

.field protected i:Lcom/twitter/library/client/Session;

.field protected j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private k:Z

.field private l:Ljava/lang/String;

.field private final m:Lcom/twitter/android/cs;

.field private final n:Landroid/support/v4/app/FragmentActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;Lcom/twitter/library/client/v;Lcom/twitter/android/ck;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;)V
    .locals 6

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/twitter/library/view/a;-><init>()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    .line 152
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ct;->n:Landroid/support/v4/app/FragmentActivity;

    .line 153
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    .line 154
    iput-object p3, p0, Lcom/twitter/android/ct;->d:Lcom/twitter/library/client/v;

    .line 155
    iput-object p5, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ct;->a:Z

    .line 157
    iput-object p6, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    .line 158
    iput-object p7, p0, Lcom/twitter/android/ct;->f:Lbxc;

    .line 159
    iput-object p8, p0, Lcom/twitter/android/ct;->g:Lapb;

    .line 160
    iget-object v0, p0, Lcom/twitter/android/ct;->d:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    .line 161
    iput-object p4, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    .line 163
    new-instance v0, Lcom/twitter/android/cs;

    iget-object v1, p0, Lcom/twitter/android/ct;->n:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/ct;->f:Lbxc;

    .line 164
    invoke-virtual {v4}, Lbxc;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cs;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/ct;->m:Lcom/twitter/android/cs;

    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;)V
    .locals 7

    .prologue
    .line 127
    const/4 v5, 0x0

    new-instance v6, Lcom/twitter/android/ck;

    invoke-direct {v6, p1, p2}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;Lcom/twitter/android/ck;)V

    .line 129
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;)V
    .locals 7

    .prologue
    .line 138
    new-instance v6, Lcom/twitter/android/ck;

    invoke-direct {v6, p1, p2}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;Lcom/twitter/android/ck;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;Lcom/twitter/android/ck;)V
    .locals 9

    .prologue
    .line 145
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    move-object v0, p0

    move-object v2, p1

    move-object v4, p6

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/ct;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;Lcom/twitter/library/client/v;Lcom/twitter/android/ck;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;)V

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lcom/twitter/android/ck;)V
    .locals 7

    .prologue
    .line 133
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;Lcom/twitter/android/ck;)V

    .line 134
    return-void
.end method

.method private a(J)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 234
    iget-object v0, p0, Lcom/twitter/android/ct;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    .line 235
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/ct;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 236
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 237
    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 243
    :goto_0
    return-object v0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_1

    .line 239
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 240
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 241
    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    goto :goto_0

    .line 243
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 192
    invoke-static {p1}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {v1, v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/support/v4/app/Fragment;Lcom/twitter/media/ui/image/BaseMediaImageView;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 340
    instance-of v0, p1, Lcom/twitter/app/common/list/TwitterListFragment;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 343
    check-cast v0, Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v3, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 345
    new-array v0, v9, [I

    .line 346
    new-array v4, v9, [I

    .line 347
    invoke-virtual {p2, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getLocationOnScreen([I)V

    .line 348
    invoke-virtual {v3, v4}, Landroid/widget/ListView;->getLocationOnScreen([I)V

    .line 349
    aget v5, v0, v1

    .line 350
    aget v0, v0, v1

    invoke-virtual {p2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getHeight()I

    move-result v6

    add-int/2addr v6, v0

    .line 351
    aget v7, v4, v1

    .line 352
    aget v0, v4, v1

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 353
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 354
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v9, :cond_1

    move v0, v1

    .line 355
    :goto_0
    const v1, 0x7f0e0051

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 358
    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getHeight()I

    move-result v8

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeight()I

    move-result v9

    if-le v8, v9, :cond_2

    .line 372
    :cond_0
    :goto_1
    if-eqz v2, :cond_4

    .line 376
    neg-int v0, v2

    const/16 v1, 0xc8

    invoke-virtual {v3, v0, v1}, Landroid/widget/ListView;->smoothScrollBy(II)V

    .line 377
    new-instance v0, Lcom/twitter/android/ct$1;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/twitter/android/ct$1;-><init>(Lcom/twitter/android/ct;Landroid/support/v4/app/Fragment;Landroid/content/Intent;Lcom/twitter/media/ui/image/BaseMediaImageView;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v0, v4, v5}, Landroid/widget/ListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 395
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 354
    goto :goto_0

    .line 362
    :cond_2
    if-ge v5, v7, :cond_3

    .line 364
    sub-int v0, v7, v5

    add-int v2, v0, v1

    goto :goto_1

    .line 365
    :cond_3
    if-le v6, v4, :cond_0

    if-nez v0, :cond_0

    .line 367
    sub-int v0, v4, v6

    sub-int v2, v0, v1

    goto :goto_1

    .line 389
    :cond_4
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p3, p2}, Lcom/twitter/android/GalleryActivity;->a(Landroid/app/Activity;Landroid/content/Intent;Lcom/twitter/media/ui/image/BaseMediaImageView;)V

    goto :goto_2

    .line 393
    :cond_5
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p3, p2}, Lcom/twitter/android/GalleryActivity;->a(Landroid/app/Activity;Landroid/content/Intent;Lcom/twitter/media/ui/image/BaseMediaImageView;)V

    goto :goto_2
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 4

    .prologue
    .line 689
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 690
    invoke-static {}, Lbss;->a()Lbsp;

    move-result-object v0

    .line 691
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-static {p2, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    .line 690
    invoke-interface {v0, v1}, Lbsp;->a(Ljava/lang/Object;)V

    .line 694
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 695
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 696
    iget-object v0, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v2, v0, p1, v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 697
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p3, p4}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 698
    invoke-virtual {v0, p5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 699
    invoke-virtual {v0, p6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 700
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 697
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 701
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 185
    if-eqz p1, :cond_0

    .line 186
    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/twitter/android/ct;->f:Lbxc;

    invoke-virtual {v0}, Lbxc;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "platform_photo_card"

    const-string/jumbo v2, "click"

    .line 252
    invoke-direct {p0, p2, v1, v2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 251
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 253
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 254
    iget-object v2, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v1, v2, p2, v3}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 255
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 256
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 257
    invoke-virtual {p0, p4}, Lcom/twitter/android/ct;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    .line 258
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 255
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 261
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->p:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 262
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 265
    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/ct;->b(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    .line 266
    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;ZLcom/twitter/library/widget/TweetView;)V
    .locals 8

    .prologue
    const/16 v7, 0x23bf

    const/4 v6, 0x0

    .line 275
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "etc"

    iget-boolean v2, p0, Lcom/twitter/android/ct;->a:Z

    .line 276
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 277
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 278
    iget-object v0, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 280
    const-string/jumbo v2, "home"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 281
    const-string/jumbo v0, "context"

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 301
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ct;->g:Lapb;

    if-eqz v0, :cond_8

    .line 302
    iget-object v0, p0, Lcom/twitter/android/ct;->g:Lapb;

    iget-object v0, v0, Lapb;->d:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "prj"

    iget-object v3, p0, Lcom/twitter/android/ct;->g:Lapb;

    iget-object v3, v3, Lapb;->e:[Ljava/lang/String;

    .line 303
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "sel"

    iget-object v3, p0, Lcom/twitter/android/ct;->g:Lapb;

    iget-object v3, v3, Lapb;->a:Ljava/lang/String;

    .line 304
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "selArgs"

    iget-object v3, p0, Lcom/twitter/android/ct;->g:Lapb;

    iget-object v3, v3, Lapb;->b:[Ljava/lang/String;

    .line 305
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "orderBy"

    iget-object v3, p0, Lcom/twitter/android/ct;->g:Lapb;

    iget-object v3, v3, Lapb;->c:Ljava/lang/String;

    .line 306
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "id"

    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->G:J

    .line 307
    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "controls"

    .line 308
    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 322
    :cond_1
    :goto_1
    if-eqz p5, :cond_a

    .line 323
    invoke-virtual {p5}, Lcom/twitter/library/widget/TweetView;->getContentContainer()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    .line 325
    :goto_2
    instance-of v2, v0, Lbya;

    if-eqz v2, :cond_c

    if-eqz p3, :cond_c

    .line 326
    check-cast v0, Lbya;

    .line 327
    invoke-virtual {v0, p3}, Lbya;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/ui/image/BaseMediaImageView;

    move-result-object v0

    .line 328
    if-eqz v0, :cond_b

    .line 329
    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/media/ui/image/BaseMediaImageView;Landroid/content/Intent;)V

    .line 336
    :goto_3
    return-void

    .line 283
    :cond_2
    const-string/jumbo v2, "tweet"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 284
    const-string/jumbo v0, "context"

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 286
    :cond_3
    const-string/jumbo v2, "profile_tweets"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 287
    const-string/jumbo v0, "context"

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 289
    :cond_4
    const-string/jumbo v2, "list"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 290
    const-string/jumbo v0, "context"

    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 292
    :cond_5
    const-string/jumbo v2, "favorites"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 293
    const-string/jumbo v0, "context"

    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 295
    :cond_6
    const-string/jumbo v2, "profile"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string/jumbo v2, "me"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    :cond_7
    const-string/jumbo v0, "context"

    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 298
    const-string/jumbo v0, "association"

    iget-object v2, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 310
    :cond_8
    const-string/jumbo v0, "statusId"

    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->G:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 311
    const-string/jumbo v0, "show_tw"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 312
    if-eqz p3, :cond_9

    .line 313
    const-string/jumbo v0, "media"

    sget-object v2, Lcom/twitter/model/core/MediaEntity;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v0, p3, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 314
    const-string/jumbo v0, "source_tweet_id"

    iget-wide v2, p3, Lcom/twitter/model/core/MediaEntity;->j:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 316
    :cond_9
    if-eqz p4, :cond_1

    .line 317
    const-string/jumbo v0, "tagged_user_list"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_1

    .line 323
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 331
    :cond_b
    invoke-virtual {p1, v1, v7}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_3

    .line 334
    :cond_c
    invoke-virtual {p1, v1, v7}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_3
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 13

    .prologue
    .line 527
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 528
    if-eqz p4, :cond_0

    .line 529
    new-instance v1, Lcom/twitter/media/model/MediaDescriptor;

    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-direct {v1, v0, v2}, Lcom/twitter/media/model/MediaDescriptor;-><init>(Ljava/lang/String;Z)V

    .line 530
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 532
    :cond_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v12, p9

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;ZZZIIZ)V

    .line 534
    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;ZZZIIZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Lcom/twitter/model/core/Tweet;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/media/model/MediaDescriptor;",
            ">;",
            "Ljava/lang/String;",
            "ZZZIIZ)V"
        }
    .end annotation

    .prologue
    .line 540
    iget-object v0, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    .line 542
    if-eqz p11, :cond_2

    .line 543
    const-string/jumbo v0, "platform_forward_player_card"

    .line 547
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/ct;->f:Lbxc;

    invoke-virtual {v1}, Lbxc;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "click"

    .line 548
    invoke-direct {p0, p2, v0, v2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 547
    invoke-static {v1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 549
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 550
    iget-object v2, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v1, v2, p2, v3}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 551
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 552
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 551
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 555
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 556
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->p:Lcom/twitter/library/api/PromotedEvent;

    .line 557
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 559
    :cond_1
    if-eqz p4, :cond_4

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 560
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 561
    new-instance v0, Lcom/twitter/android/av/ad;

    invoke-direct {v0}, Lcom/twitter/android/av/ad;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 562
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ad;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/ab;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v1, p2, p3}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 563
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/ab;

    move-result-object v0

    const/4 v1, 0x1

    .line 564
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->b(Z)Lcom/twitter/library/av/ab;

    move-result-object v0

    .line 566
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    .line 567
    invoke-virtual {v1}, Lbaa;->k()Z

    move-result v1

    .line 565
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->e(Z)Lcom/twitter/library/av/ab;

    move-result-object v0

    .line 568
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->a(Ljava/lang/Object;)V

    .line 598
    :goto_1
    return-void

    .line 545
    :cond_2
    const-string/jumbo v0, "platform_player_card"

    goto/16 :goto_0

    .line 570
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/MediaPlayerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 571
    const-string/jumbo v1, "image_url"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "aud"

    .line 572
    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "is_looping"

    .line 573
    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "simple_controls"

    .line 574
    invoke-virtual {v1, v2, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "player_url"

    .line 575
    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "player_stream_urls"

    .line 576
    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "tweet"

    .line 577
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "video_position"

    .line 578
    invoke-virtual {v1, v2, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "video_index"

    .line 579
    invoke-virtual {v1, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 580
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "amplify"

    .line 581
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 582
    const/16 v1, 0x23c3

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 584
    :cond_4
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 586
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 587
    :catch_0
    move-exception v0

    .line 588
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a09c3

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 591
    :cond_5
    new-instance v0, Lcpb;

    new-instance v1, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v2, "No streams or player url supplied"

    invoke-direct {v1, v2}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    .line 592
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    .line 593
    if-eqz v1, :cond_6

    .line 594
    const-string/jumbo v2, "cardType"

    invoke-virtual {v1}, Lcax;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v3, "cardUrl"

    invoke-virtual {v1}, Lcax;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 596
    :cond_6
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto/16 :goto_1
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Z)V
    .locals 3

    .prologue
    .line 604
    iget-object v0, p0, Lcom/twitter/android/ct;->m:Lcom/twitter/android/cs;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, p2, p3, v2}, Lcom/twitter/android/cs;->a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;ZLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 606
    return-void
.end method

.method public a(Lbxg;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 201
    iget-object v0, p1, Lbxg;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 206
    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 208
    if-eqz v1, :cond_0

    .line 209
    iget-object v4, p1, Lbxg;->a:Lcom/twitter/model/core/Tweet;

    .line 210
    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v5

    .line 211
    invoke-virtual {p1}, Lbxg;->b()J

    move-result-wide v2

    .line 212
    invoke-virtual {p1}, Lbxg;->a()J

    move-result-wide v6

    .line 214
    iget-object v0, p0, Lcom/twitter/android/ct;->f:Lbxc;

    invoke-virtual {v0}, Lbxc;->a()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p1, Lbxg;->a:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, p1, Lbxg;->c:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "avatar"

    :goto_1
    const-string/jumbo v10, "profile_click"

    .line 215
    invoke-direct {p0, v9, v0, v10}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-static {v8, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 217
    new-instance v8, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v9, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v9}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 218
    iget-object v9, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    invoke-static {v8, v9, v4, v12}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 219
    invoke-static {v8, v2, v3, v5, v12}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 220
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-virtual {v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v8, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 221
    invoke-virtual {v0, v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v8, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    .line 222
    invoke-virtual {v0, v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 220
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 223
    invoke-direct {p0, v6, v7}, Lcom/twitter/android/ct;->a(J)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v6

    .line 225
    iget-object v7, v4, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    .line 226
    invoke-virtual {p1}, Lbxg;->c()Ljava/lang/String;

    move-result-object v4

    .line 227
    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V

    goto :goto_0

    .line 214
    :cond_2
    const-string/jumbo v0, "screen_name"

    goto :goto_1
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 708
    iput-object p1, p0, Lcom/twitter/android/ct;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 709
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    .line 177
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;JLcom/twitter/library/widget/TweetView;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x1

    .line 449
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 450
    if-nez v1, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 454
    if-eqz v2, :cond_0

    .line 458
    invoke-virtual {p0, p4}, Lcom/twitter/android/ct;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v3

    .line 459
    iget-object v0, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_2

    .line 460
    const-string/jumbo v0, "media_tag_summary"

    const-string/jumbo v5, "click"

    invoke-direct {p0, p1, v0, v5}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 461
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v6, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 462
    iget-object v6, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-static {v5, v6, p1, v7}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 463
    new-array v6, v4, [Ljava/lang/String;

    aput-object v0, v6, v8

    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v5, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 464
    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 465
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    .line 466
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 463
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 469
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/core/k;->a(J)Lcom/twitter/model/core/MediaEntity;

    move-result-object v3

    .line 470
    if-eqz v3, :cond_0

    .line 471
    iget-object v0, v3, Lcom/twitter/model/core/MediaEntity;->q:Ljava/util/List;

    .line 472
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v4, :cond_4

    .line 473
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 474
    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    .line 475
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v4

    invoke-static {v3, v4}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v3

    .line 476
    invoke-virtual {v3}, Lbsq$a;->a()Lbsq;

    move-result-object v3

    .line 475
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    .line 478
    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v3, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    .line 479
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/n;

    iget-object v0, v0, Lcom/twitter/model/core/n;->d:Ljava/lang/String;

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 478
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    move-object v0, p0

    move-object v2, p1

    move-object v5, p4

    .line 481
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;ZLcom/twitter/library/widget/TweetView;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcax;Lcom/twitter/library/widget/TweetView;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 490
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 491
    if-nez v1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 495
    if-eqz v0, :cond_0

    .line 498
    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 499
    invoke-virtual {p0, v1, p1, v9}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Z)V

    goto :goto_0

    .line 501
    :cond_2
    invoke-static {p2}, Lcom/twitter/media/util/k;->a(Lcax;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 502
    invoke-virtual {p2}, Lcax;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcax;->l()Ljava/lang/String;

    move-result-object v4

    .line 503
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v2, p1

    move v7, v6

    move v8, v6

    .line 502
    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 510
    if-eqz v0, :cond_0

    .line 511
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Z)V

    .line 513
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/timeline/bk;Ljava/lang/String;)V
    .locals 9
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 779
    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getFriendshipCache()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    iget-boolean v6, p0, Lcom/twitter/android/ct;->k:Z

    move-object v0, p1

    move-object v2, p3

    move-object v4, p4

    move-object v7, p5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/cl;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lbxb;Lcom/twitter/android/timeline/bk;ZZLjava/lang/String;Z)V

    .line 781
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 414
    if-nez v0, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 418
    if-eqz v1, :cond_0

    .line 421
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V
    .locals 3

    .prologue
    .line 632
    iget-object v0, p0, Lcom/twitter/android/ct;->m:Lcom/twitter/android/cs;

    iget-object v1, p0, Lcom/twitter/android/ct;->f:Lbxc;

    invoke-virtual {v1}, Lbxc;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/twitter/android/cs;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 633
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/twitter/android/ct;->m:Lcom/twitter/android/cs;

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/twitter/android/cs;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 705
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/b;)V
    .locals 7

    .prologue
    .line 651
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 652
    if-eqz v0, :cond_0

    .line 653
    iget-object v1, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/b;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 655
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ct;->a(J)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 656
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->u:Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v3, "cashtag"

    const-string/jumbo v4, "search"

    iget-object v5, p2, Lcom/twitter/model/core/b;->c:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 659
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/h;)V
    .locals 2

    .prologue
    .line 638
    new-instance v0, Lajh$a;

    invoke-direct {v0}, Lajh$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ct;->n:Landroid/support/v4/app/FragmentActivity;

    .line 639
    invoke-virtual {v0, v1}, Lajh$a;->a(Landroid/app/Activity;)Lajh$a;

    move-result-object v0

    .line 640
    invoke-virtual {v0, p1}, Lajh$a;->a(Lcom/twitter/model/core/Tweet;)Lajh$a;

    move-result-object v0

    .line 641
    invoke-virtual {v0, p2}, Lajh$a;->a(Lcom/twitter/model/core/h;)Lajh$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 642
    invoke-virtual {v0, v1}, Lajh$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lajh$a;

    move-result-object v0

    const-string/jumbo v1, "hashtag"

    .line 643
    invoke-virtual {v0, v1}, Lajh$a;->a(Ljava/lang/String;)Lajh$a;

    move-result-object v0

    const-string/jumbo v1, "search"

    .line 644
    invoke-virtual {v0, v1}, Lajh$a;->b(Ljava/lang/String;)Lajh$a;

    move-result-object v0

    .line 645
    invoke-virtual {v0}, Lajh$a;->a()Lajh;

    move-result-object v0

    invoke-virtual {v0}, Lajh;->a()V

    .line 646
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/q;)V
    .locals 8

    .prologue
    .line 664
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 665
    if-eqz v0, :cond_1

    .line 666
    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/ct;->a(J)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    .line 667
    iget-object v5, p2, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    .line 668
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "screen_name"

    .line 669
    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 670
    const-string/jumbo v3, "association"

    new-instance v4, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v4, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const/4 v1, 0x1

    .line 672
    invoke-virtual {v4, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->t:J

    .line 673
    invoke-virtual {v1, v6, v7}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v1

    .line 670
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 675
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 676
    const-string/jumbo v1, "pc"

    .line 677
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v3

    invoke-static {v3}, Lcgi;->a(Lcgi;)[B

    move-result-object v3

    .line 676
    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 679
    :cond_0
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 681
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v3, ""

    const-string/jumbo v4, "mention_click"

    .line 682
    invoke-static {v5}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    .line 681
    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 684
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/library/widget/TweetView;)V
    .locals 6

    .prologue
    .line 427
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 428
    if-nez v0, :cond_0

    .line 443
    :goto_0
    return-void

    .line 432
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/twitter/android/geo/places/a;->a(Landroid/content/Context;Lcom/twitter/model/geo/TwitterPlace;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 434
    invoke-static {}, Lcom/twitter/library/scribe/b;->b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 435
    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 436
    new-instance v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;-><init>()V

    .line 437
    iget-object v2, p2, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->a:Ljava/lang/String;

    .line 438
    iget-object v2, p2, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    invoke-virtual {v2}, Lcom/twitter/model/geo/TwitterPlace$PlaceType;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails$ScribeGeoPlace;->b:Ljava/lang/String;

    .line 439
    iget-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iget-object v2, v2, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "place_tag"

    const-string/jumbo v5, "click"

    .line 441
    invoke-direct {p0, p1, v4, v5}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 442
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 440
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 9

    .prologue
    .line 714
    iget-object v0, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    if-eqz v0, :cond_0

    .line 715
    const v0, 0x7f13007e

    invoke-virtual {p2, v0}, Lcom/twitter/library/widget/TweetView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/android/timeline/bk;

    .line 716
    iget-object v0, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getFriendshipCache()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v3

    .line 717
    invoke-virtual {p0, p2}, Lcom/twitter/android/ct;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v4

    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object v1, p1

    move-object v5, p2

    .line 716
    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;ZLcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 719
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/twitter/android/ct;->a:Z

    .line 173
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/ct;->g:Lapb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ct;->g:Lapb;

    iget-object v0, v0, Lapb;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ck;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    .line 757
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 6

    .prologue
    .line 270
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ct;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;ZLcom/twitter/library/widget/TweetView;)V

    .line 271
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 736
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 737
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    if-eqz v1, :cond_0

    .line 738
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 739
    if-eqz v2, :cond_0

    .line 740
    iget-object v0, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    .line 741
    :goto_0
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 742
    invoke-static {v3, v4, p1, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 743
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "quoted_tweet"

    const-string/jumbo v5, "click"

    invoke-direct {p0, p1, v4, v5}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 744
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 745
    invoke-virtual {p0, p2}, Lcom/twitter/android/ct;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ct;->l:Ljava/lang/String;

    .line 746
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 743
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 747
    iget-object v0, p0, Lcom/twitter/android/ct;->h:Lcom/twitter/android/ck;

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->x:J

    invoke-virtual {v0, v4, v5, v2}, Lcom/twitter/android/ck;->a(JLandroid/support/v4/app/FragmentActivity;)V

    .line 750
    :cond_0
    return-void

    .line 740
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    goto :goto_0
.end method

.method public b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 724
    iget-object v1, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 725
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 726
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    const/4 v2, 0x2

    const-string/jumbo v3, ":attribution:open_link"

    aput-object v3, v0, v2

    .line 725
    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 727
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/ct;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 728
    iget-object v3, p0, Lcom/twitter/android/ct;->c:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v2, v3, p1, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 729
    new-array v3, v7, [Ljava/lang/String;

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 730
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V

    .line 731
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 180
    iput-boolean p1, p0, Lcom/twitter/android/ct;->k:Z

    .line 181
    return-void
.end method

.method public c(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 8

    .prologue
    .line 762
    iget-object v0, p0, Lcom/twitter/android/ct;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 763
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 764
    :goto_0
    if-eqz v3, :cond_0

    .line 765
    const v0, 0x7f13007e

    invoke-virtual {p2, v0}, Lcom/twitter/library/widget/TweetView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/timeline/bk;

    .line 766
    const v0, 0x7f130092

    invoke-virtual {p2, v0}, Lcom/twitter/library/widget/TweetView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 767
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-nez v0, :cond_2

    const-string/jumbo v0, "unknown"

    .line 768
    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 769
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":caret:click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 770
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/ct;->d:Lcom/twitter/library/client/v;

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v2, v6

    .line 771
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 770
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 772
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 774
    :cond_0
    return-void

    .line 763
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 767
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ct;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 768
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
