.class public Lcom/twitter/android/MediaListFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/MediaListFragment$c;,
        Lcom/twitter/android/MediaListFragment$b;,
        Lcom/twitter/android/MediaListFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/android/widget/ScrollingHeaderListFragment",
        "<TT;",
        "Lcom/twitter/android/widget/w",
        "<TT;>;>;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field protected a:Z

.field protected b:Lcom/twitter/android/metrics/b;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private e:J

.field private f:Lcom/twitter/model/core/TwitterUser;

.field private g:Z

.field private h:Z

.field private i:Lcom/twitter/media/ui/image/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;-><init>()V

    .line 78
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaListFragment;->c:Ljava/util/List;

    .line 79
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaListFragment;->d:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MediaListFragment;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->d:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/MediaListFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/MediaListFragment;)Lcom/twitter/media/ui/image/b;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->i:Lcom/twitter/media/ui/image/b;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 279
    if-eq p1, v1, :cond_0

    if-nez p1, :cond_1

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/w;

    if-ne p1, v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/w;->a(Z)V

    .line 282
    :cond_1
    return-void

    .line 280
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private e(I)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 361
    packed-switch p1, :pswitch_data_0

    .line 375
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->aC()Landroid/database/Cursor;

    move-result-object v2

    .line 365
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 366
    const/16 v0, 0x17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 372
    :cond_0
    :pswitch_1
    return-wide v0

    .line 361
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private i(I)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 381
    packed-switch p1, :pswitch_data_0

    .line 395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->aC()Landroid/database/Cursor;

    move-result-object v2

    .line 389
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 390
    const/16 v0, 0x17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 392
    :cond_0
    :pswitch_1
    return-wide v0

    .line 381
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/w;

    invoke-virtual {v0}, Lcom/twitter/android/widget/w;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected H_()V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaListFragment;->a(I)Z

    .line 287
    return-void
.end method

.method protected K_()Z
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->g:Z

    .line 101
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 103
    return-object v0
.end method

.method protected a(Lcbi;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 244
    invoke-virtual {p0, p1}, Lcom/twitter/android/MediaListFragment;->b(Lcbi;)V

    .line 245
    invoke-direct {p0}, Lcom/twitter/android/MediaListFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaListFragment;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->b:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->j()V

    .line 249
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->g:Z

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->b:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->j()V

    goto :goto_0
.end method

.method public a(Lcno;I)V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0, p2}, Lcom/twitter/android/MediaListFragment;->c(I)V

    .line 275
    invoke-virtual {p0, p2}, Lcom/twitter/android/MediaListFragment;->h(I)V

    .line 276
    return-void
.end method

.method public a(Lcno;IIIZ)V
    .locals 1

    .prologue
    .line 258
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Lcno;IIIZ)V

    .line 260
    if-eqz p3, :cond_0

    if-nez p5, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    if-nez p2, :cond_2

    .line 265
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->y()V

    goto :goto_0

    .line 266
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/w;

    invoke-virtual {v0}, Lcom/twitter/android/widget/w;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    if-lez p2, :cond_0

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    .line 268
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->l()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 213
    invoke-virtual {p1}, Lcom/twitter/app/common/list/l$d;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    const v0, 0x7f04029f

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0400e3

    .line 215
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 216
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const v0, 0x7f0403e2

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 218
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 221
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 351
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 352
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->b:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->j()V

    .line 353
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-nez v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0998

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    check-cast p1, Lbgb;

    invoke-virtual {p1}, Lbgb;->G()I

    move-result v0

    if-nez v0, :cond_0

    .line 356
    iput-boolean v2, p0, Lcom/twitter/android/MediaListFragment;->a:Z

    goto :goto_0
.end method

.method protected a(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 335
    invoke-virtual {p0, p1}, Lcom/twitter/android/MediaListFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 338
    :goto_0
    return v0

    :cond_0
    new-instance v0, Lbfv;

    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/MediaListFragment;->e:J

    invoke-direct {v0, v2, v3, v4, v5}, Lbfv;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    .line 339
    invoke-direct {p0, p1}, Lcom/twitter/android/MediaListFragment;->e(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbfv;->c(J)Lbge;

    move-result-object v0

    .line 340
    invoke-direct {p0, p1}, Lcom/twitter/android/MediaListFragment;->i(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbge;->b(J)Lbge;

    move-result-object v2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    :cond_1
    const/16 v0, 0x14

    .line 341
    :goto_1
    invoke-virtual {v2, v0}, Lbge;->c(I)Lbge;

    move-result-object v0

    const-string/jumbo v2, "scribe_event"

    .line 344
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->i()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "photo_grid"

    invoke-static {v3, v4, p1}, Lcom/twitter/android/MediaListFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbge;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 338
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/android/MediaListFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    goto :goto_0

    .line 340
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected aP_()V
    .locals 5

    .prologue
    .line 185
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->aP_()V

    .line 186
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->i()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "photo_grid:::impression"

    invoke-static {v3, v4}, Lcom/twitter/android/profiles/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 188
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/MediaListFragment;->f:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 186
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 189
    return-void
.end method

.method protected ar_()V
    .locals 1

    .prologue
    .line 319
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->ar_()V

    .line 320
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/MediaListFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaListFragment;->a(I)Z

    .line 322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->g:Z

    .line 324
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 161
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b()V

    .line 162
    iget-wide v0, p0, Lcom/twitter/android/MediaListFragment;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->ar_()V

    .line 165
    :cond_0
    return-void
.end method

.method protected f()V
    .locals 5

    .prologue
    .line 112
    new-instance v0, Lcom/twitter/android/metrics/b;

    const-string/jumbo v1, "list:photogrid"

    const-string/jumbo v2, "list:photogrid"

    sget-object v3, Lcom/twitter/metrics/g;->l:Lcom/twitter/metrics/g$b;

    .line 113
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/metrics/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/h;)V

    iput-object v0, p0, Lcom/twitter/android/MediaListFragment;->b:Lcom/twitter/android/metrics/b;

    .line 114
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->b:Lcom/twitter/android/metrics/b;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/metrics/b;->b(J)V

    .line 115
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->b:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->i()V

    .line 116
    return-void
.end method

.method protected h()Lcom/twitter/android/widget/w;
    .locals 6

    .prologue
    .line 142
    new-instance v0, Lcom/twitter/android/ao;

    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v4, Lcom/twitter/android/MediaListFragment$a;

    invoke-direct {v4, p0}, Lcom/twitter/android/MediaListFragment$a;-><init>(Lcom/twitter/android/MediaListFragment;)V

    .line 144
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ao;-><init>(Landroid/content/Context;ILandroid/view/View$OnClickListener;Lcom/twitter/android/av;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 142
    return-object v0
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->h:Z

    invoke-static {v0}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->k()Laiz;

    move-result-object v0

    invoke-static {v0}, Laja;->a(Laiz;)Lapb;

    move-result-object v6

    .line 226
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v6, Lapb;->d:Landroid/net/Uri;

    iget-object v3, v6, Lapb;->e:[Ljava/lang/String;

    iget-object v4, v6, Lapb;->a:Ljava/lang/String;

    iget-object v5, v6, Lapb;->b:[Ljava/lang/String;

    iget-object v6, v6, Lapb;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected k()Laiz;
    .locals 4

    .prologue
    .line 232
    new-instance v0, Laiz$a;

    invoke-direct {v0}, Laiz$a;-><init>()V

    const/4 v1, 0x2

    .line 233
    invoke-virtual {v0, v1}, Laiz$a;->a(I)Laiz$a;

    move-result-object v0

    .line 234
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Laiz$a;->a(J)Laiz$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/MediaListFragment;->a_:J

    .line 235
    invoke-virtual {v0, v2, v3}, Laiz$a;->b(J)Laiz$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/MediaListFragment;->h:Z

    .line 236
    invoke-virtual {v0, v1}, Laiz$a;->a(Z)Laiz$a;

    move-result-object v0

    .line 237
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/settings/c;->a(Lcom/twitter/library/client/Session;)Z

    move-result v1

    invoke-virtual {v0, v1}, Laiz$a;->b(Z)Laiz$a;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, Laiz$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiz;

    .line 232
    return-object v0
.end method

.method protected l()V
    .locals 2

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->v_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->a:Z

    if-nez v0, :cond_0

    .line 292
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/w;

    invoke-virtual {v0}, Lcom/twitter/android/widget/w;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    .line 293
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaListFragment;->a(I)Z

    .line 295
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 120
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 122
    if-eqz p1, :cond_0

    .line 123
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/MediaListFragment;->e:J

    .line 124
    const-string/jumbo v0, "is_last"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->a:Z

    .line 129
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x5

    .line 130
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v2, p0, Lcom/twitter/android/MediaListFragment;->e:J

    .line 131
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "photo_grid"

    .line 133
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 129
    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaListFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 135
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->h()Lcom/twitter/android/widget/w;

    move-result-object v0

    .line 136
    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaListFragment;->a(Lcom/twitter/android/client/j;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 137
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 138
    return-void

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/MediaListFragment;->e:J

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 299
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaListFragment$c;

    .line 300
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->k()Laiz;

    move-result-object v1

    invoke-static {v1}, Laja;->a(Laiz;)Lapb;

    move-result-object v1

    .line 301
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, v1, Lapb;->d:Landroid/net/Uri;

    .line 302
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "prj"

    iget-object v4, v1, Lapb;->e:[Ljava/lang/String;

    .line 303
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "sel"

    iget-object v4, v1, Lapb;->a:Ljava/lang/String;

    .line 304
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "orderBy"

    iget-object v1, v1, Lapb;->c:Ljava/lang/String;

    .line 305
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "id"

    iget-object v3, v0, Lcom/twitter/android/MediaListFragment$c;->a:Lcom/twitter/android/MediaListFragment$b;

    .line 307
    invoke-interface {v3}, Lcom/twitter/android/MediaListFragment$b;->d()Lcom/twitter/model/core/Tweet;

    move-result-object v3

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->G:J

    .line 306
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "media"

    iget-object v3, v0, Lcom/twitter/android/MediaListFragment$c;->a:Lcom/twitter/android/MediaListFragment$b;

    .line 309
    invoke-interface {v3}, Lcom/twitter/android/MediaListFragment$b;->e()Lcom/twitter/model/core/MediaEntity;

    move-result-object v3

    sget-object v4, Lcom/twitter/model/core/MediaEntity;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v3

    .line 308
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "page_cache_size"

    const/4 v3, 0x1

    .line 310
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "context"

    const/16 v3, 0x9

    .line 311
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    .line 313
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 314
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/android/MediaListFragment$c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {v2, v1, v0}, Lcom/twitter/android/GalleryActivity;->a(Landroid/app/Activity;Landroid/content/Intent;Lcom/twitter/media/ui/image/BaseMediaImageView;)V

    .line 315
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "is_me"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaListFragment;->h:Z

    .line 91
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->f()V

    .line 92
    new-instance v0, Lcom/twitter/media/ui/image/b;

    invoke-direct {v0}, Lcom/twitter/media/ui/image/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/MediaListFragment;->i:Lcom/twitter/media/ui/image/b;

    .line 93
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "arg_profile_user"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/MediaListFragment;->f:Lcom/twitter/model/core/TwitterUser;

    .line 94
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 193
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 194
    const-string/jumbo v0, "user_id"

    iget-wide v2, p0, Lcom/twitter/android/MediaListFragment;->e:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 195
    const-string/jumbo v0, "is_last"

    iget-boolean v1, p0, Lcom/twitter/android/MediaListFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 196
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 200
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onStart()V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->i:Lcom/twitter/media/ui/image/b;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/b;->e()V

    .line 202
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->i:Lcom/twitter/media/ui/image/b;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/b;->f()V

    .line 207
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onStop()V

    .line 208
    return-void
.end method

.method public q_()V
    .locals 4

    .prologue
    .line 170
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->q_()V

    .line 171
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "photo_grid:stream::tweets"

    invoke-static {v0, v1}, Lcom/twitter/android/profiles/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 175
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 176
    invoke-virtual {p0}, Lcom/twitter/android/MediaListFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/MediaListFragment;->c:Ljava/util/List;

    .line 177
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 174
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 178
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 180
    :cond_0
    return-void
.end method
