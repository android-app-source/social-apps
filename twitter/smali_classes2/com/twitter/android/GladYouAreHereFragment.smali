.class public Lcom/twitter/android/GladYouAreHereFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/twitter/android/au;

.field private b:Lcom/twitter/android/by;

.field private c:Landroid/widget/CheckBox;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 89
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v2, v0, v1

    .line 90
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "{{}}"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 38
    const v0, 0x7f04011a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 39
    const v0, 0x7f1303bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    const v0, 0x7f1303be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->c:Landroid/widget/CheckBox;

    .line 41
    invoke-virtual {p0}, Lcom/twitter/android/GladYouAreHereFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/bw;->a(Lcom/twitter/app/common/base/b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->d:Z

    .line 43
    const v0, 0x7f1303bf

    .line 44
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/GladYouAreHereFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "people"

    const-string/jumbo v4, "phone100_glad_you_are_here"

    invoke-static {v2, v0, v3, v4}, Lcom/twitter/android/dialog/b;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/GladYouAreHereFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/GladYouAreHereFragment;->a(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 50
    return-object v1
.end method

.method public d()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 70
    iget-object v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 71
    invoke-virtual {p0}, Lcom/twitter/android/GladYouAreHereFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 73
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v6, "phone100_glad_you_are_here"

    aput-object v6, v5, v0

    const-string/jumbo v0, "form"

    aput-object v0, v5, v7

    iget-boolean v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->d:Z

    .line 74
    invoke-static {v0}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    const/4 v0, 0x3

    const/4 v6, 0x0

    aput-object v6, v5, v0

    const/4 v6, 0x4

    if-eqz v1, :cond_1

    const-string/jumbo v0, "opt_in"

    :goto_0
    aput-object v0, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 73
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 78
    iget-object v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->b:Lcom/twitter/android/by;

    invoke-interface {v0, v1}, Lcom/twitter/android/by;->e(Z)V

    .line 79
    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/GladYouAreHereFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v3, v8}, Lbmp;->a(Landroid/content/Context;JI)V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->b:Lcom/twitter/android/by;

    invoke-interface {v0, v7}, Lcom/twitter/android/by;->f(Z)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->a:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->bl_()V

    .line 85
    return-void

    .line 74
    :cond_1
    const-string/jumbo v0, "opt_out"

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/twitter/android/au;

    iput-object v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->a:Lcom/twitter/android/au;

    .line 57
    check-cast p1, Lcom/twitter/android/by;

    iput-object p1, p0, Lcom/twitter/android/GladYouAreHereFragment;->b:Lcom/twitter/android/by;

    .line 58
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1303bd

    if-ne v0, v1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/twitter/android/GladYouAreHereFragment;->a:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->b()V

    .line 65
    :cond_0
    return-void
.end method
