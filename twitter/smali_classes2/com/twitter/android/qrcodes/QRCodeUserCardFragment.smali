.class public Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;
.super Lcom/twitter/android/qrcodes/QRCodeFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/qrcodes/QRCodeUserCardFragment$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/model/util/FriendshipCache;

.field private final c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private d:Lcom/twitter/app/users/c;

.field private e:Lcom/twitter/ui/user/ProfileCardView;

.field private f:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeFragment;-><init>()V

    .line 38
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->b:Lcom/twitter/model/util/FriendshipCache;

    .line 39
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v1, "qr"

    .line 40
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "user_card"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    .line 131
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 73
    packed-switch p1, :pswitch_data_0

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 75
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 76
    const-string/jumbo v0, "user_id"

    const-wide/16 v2, 0x0

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 77
    const-string/jumbo v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 78
    iget-object v3, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/model/util/FriendshipCache;->c(JI)V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 55
    const v0, 0x7f04033c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-super {p0}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onStart()V

    .line 92
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v2, 0x5

    .line 94
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v2, 0x3

    .line 95
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(I)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 96
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/ui/user/ProfileCardView;->setScribeItem(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 97
    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/ui/user/ProfileCardView;->setScribeComponent(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/user/ProfileCardView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->b:Lcom/twitter/model/util/FriendshipCache;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 101
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->b:Lcom/twitter/model/util/FriendshipCache;

    iget-object v3, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v3, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/ui/user/ProfileCardView;->setIsFollowing(Z)V

    .line 114
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/model/core/TwitterUser;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    .line 115
    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v2}, Lcom/twitter/model/core/g;->f(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v2}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/twitter/ui/user/ProfileCardView;->setFollowVisibility(I)V

    .line 118
    :cond_1
    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/ProfileCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "qr:user_card:::impression"

    aput-object v3, v1, v2

    .line 124
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 125
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    .line 126
    invoke-virtual {v1}, Lcom/twitter/ui/user/ProfileCardView;->getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 123
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 127
    return-void

    :cond_3
    move-object v0, v1

    .line 97
    goto/16 :goto_0

    .line 103
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v2}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/ui/user/ProfileCardView;->setIsFollowing(Z)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->b:Lcom/twitter/model/util/FriendshipCache;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->f:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v2}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 61
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment$a;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->S:Lcom/twitter/library/client/p;

    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->b:Lcom/twitter/model/util/FriendshipCache;

    iget-object v5, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment$a;-><init>(Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->d:Lcom/twitter/app/users/c;

    .line 63
    const v0, 0x7f1305d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/ProfileCardView;

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    .line 64
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    invoke-virtual {v0}, Lcom/twitter/ui/user/ProfileCardView;->c()V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/ProfileCardView;->c(Z)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    const v1, 0x7f0a03a5

    invoke-virtual {p0, v1}, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/user/ProfileCardView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->d:Lcom/twitter/app/users/c;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/ProfileCardView;->setActionButtonClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->e:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->d:Lcom/twitter/app/users/c;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/ProfileCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-void
.end method
