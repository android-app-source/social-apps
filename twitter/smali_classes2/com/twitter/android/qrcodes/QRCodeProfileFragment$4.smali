.class Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/dialog/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->d()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;->b:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    iput-object p2, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 102
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;->b:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    .line 103
    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->e(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "qr:qr_profile:::tweet_photo"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 104
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$b;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;->b:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->d(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)Lcom/twitter/android/qrcodes/view/QRCodeView;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 105
    return-void
.end method
