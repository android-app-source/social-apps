.class Lcom/twitter/android/qrcodes/QRCodeProfileFragment$a;
.super Lcom/twitter/android/qrcodes/QRCodeProfileFragment$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/qrcodes/QRCodeProfileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 142
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/io/File;
    .locals 3

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$b;->a([Ljava/lang/Void;)Ljava/io/File;

    move-result-object v1

    .line 148
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 149
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 151
    :try_start_0
    invoke-static {v0}, Lcom/twitter/media/util/o;->a(Landroid/content/Context;)Lcom/twitter/media/util/o;

    move-result-object v0

    new-instance v2, Lcom/twitter/media/util/j;

    invoke-direct {v2, v1}, Lcom/twitter/media/util/j;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v2}, Lcom/twitter/media/util/o;->b(Lcom/twitter/media/util/o$a;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 153
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->c()Lcqr;

    move-result-object v2

    invoke-interface {v2, v1}, Lcqr;->b(Ljava/io/File;)Z

    .line 156
    :goto_0
    return-object v0

    .line 153
    :catchall_0
    move-exception v0

    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->c()Lcqr;

    move-result-object v2

    invoke-interface {v2, v1}, Lcqr;->b(Ljava/io/File;)Z

    throw v0

    .line 156
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 162
    if-eqz v0, :cond_0

    .line 163
    if-eqz p1, :cond_1

    const v1, 0x7f0a0732

    :goto_0
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 169
    :cond_0
    return-void

    .line 163
    :cond_1
    const v1, 0x7f0a0731

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$a;->a([Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 138
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$a;->a(Ljava/io/File;)V

    return-void
.end method
