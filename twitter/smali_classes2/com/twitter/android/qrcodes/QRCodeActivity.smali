.class public Lcom/twitter/android/qrcodes/QRCodeActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/qrcodes/QRCodeFragment$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/qrcodes/QRCodeActivity$b;,
        Lcom/twitter/android/qrcodes/QRCodeActivity$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/media/camera/c$a;

.field private b:Lcom/twitter/android/media/camera/c;

.field private c:Lcom/twitter/android/media/widget/CameraPreviewContainer;

.field private d:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

.field private e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

.field private f:Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 36
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/qrcodes/QRCodeActivity$a;-><init>(Lcom/twitter/android/qrcodes/QRCodeActivity;Lcom/twitter/android/qrcodes/QRCodeActivity$1;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->a:Lcom/twitter/android/media/camera/c$a;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeActivity;)Lcom/twitter/android/media/widget/CameraPreviewContainer;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->c:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/qrcodes/QRCodeActivity;)Lcom/twitter/android/media/camera/c;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/qrcodes/QRCodeActivity;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/qrcodes/QRCodeActivity;)Lcom/twitter/android/qrcodes/QRCodeReaderFragment;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/qrcodes/QRCodeActivity;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->F:J

    return-wide v0
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 89
    invoke-static {p0, v1}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Z)V

    .line 95
    :goto_0
    return-void

    .line 92
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v1, v0}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/qrcodes/QRCodeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 120
    const v0, 0x7f040338

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 121
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 122
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 57
    const v0, 0x7f1301bf

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->c:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 59
    invoke-static {p0}, Lcom/twitter/android/media/camera/c;->a(Landroid/content/Context;)Lcom/twitter/android/media/camera/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    .line 60
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->a:Lcom/twitter/android/media/camera/c$a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c$a;)V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->b(Landroid/os/Bundle;)V

    .line 63
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    invoke-direct {v0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->d:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    .line 65
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-direct {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    .line 66
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Lcom/twitter/android/media/camera/c;)V

    .line 68
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;

    invoke-direct {v0}, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->f:Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;

    .line 70
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 72
    const-string/jumbo v1, "start_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    .line 73
    iget v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    .line 74
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f13014c

    .line 76
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 79
    const v0, 0x7f1307ab

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 80
    new-instance v1, Lcom/twitter/android/qrcodes/QRCodeActivity$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/qrcodes/QRCodeActivity$b;-><init>(Lcom/twitter/android/qrcodes/QRCodeActivity;Lcom/twitter/android/qrcodes/QRCodeActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 83
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 86
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->d:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 3

    .prologue
    .line 187
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    .line 188
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->f:Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 189
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f13014c

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->f:Lcom/twitter/android/qrcodes/QRCodeUserCardFragment;

    .line 190
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 192
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 151
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->F:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 153
    iget v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    packed-switch v0, :pswitch_data_0

    .line 173
    const/4 v0, 0x0

    .line 177
    :goto_0
    if-eqz v0, :cond_0

    .line 178
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 179
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f13014c

    .line 180
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 183
    :cond_0
    return-void

    .line 155
    :pswitch_0
    iput v4, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    .line 156
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    .line 157
    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "qr:qr_profile::qr_scan:click"

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 161
    :pswitch_1
    iput v5, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    .line 162
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->d:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    .line 163
    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "qr:qr_scan::qr_code:click"

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 167
    :pswitch_2
    iput v4, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->g:I

    .line 168
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    .line 169
    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "qr:user_card:::scan_another_qr_code"

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public d()V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 146
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->a:Lcom/twitter/android/media/camera/c$a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->b(Lcom/twitter/android/media/camera/c$a;)V

    .line 147
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 99
    packed-switch p1, :pswitch_data_0

    .line 110
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 114
    :goto_0
    return-void

    .line 101
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->i()V

    goto :goto_0

    .line 104
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->setResult(I)V

    .line 105
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->finish()V

    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPause()V

    .line 140
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->e()I

    .line 141
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 133
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 134
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->i()V

    .line 135
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity;->b:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->a(Landroid/os/Bundle;)V

    .line 129
    return-void
.end method
