.class Lcom/twitter/android/qrcodes/QRCodeReaderFragment$b;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/qrcodes/QRCodeReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<",
        "Lcom/twitter/media/model/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/qrcodes/QRCodeReaderFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)V
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Lcqy;-><init>()V

    .line 405
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$b;->a:Ljava/lang/ref/WeakReference;

    .line 406
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/model/MediaFile;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 410
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    .line 411
    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    .line 412
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->f(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "qr:qr_scan::image_picker:success"

    aput-object v4, v2, v3

    .line 413
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 412
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 414
    invoke-static {v0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Lcom/twitter/media/model/MediaFile;)V

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    if-eqz p1, :cond_2

    .line 417
    invoke-virtual {p1}, Lcom/twitter/media/model/MediaFile;->c()Lrx/g;

    .line 419
    :cond_2
    if-eqz v0, :cond_0

    .line 420
    invoke-virtual {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a04b2

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 401
    check-cast p1, Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$b;->a(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method
