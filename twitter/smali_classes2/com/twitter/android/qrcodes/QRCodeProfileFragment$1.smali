.class Lcom/twitter/android/qrcodes/QRCodeProfileFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$1;->a:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 61
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$1;->a:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->a(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "qr:qr_profile:::share_via"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 62
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$1;->a:Lcom/twitter/android/qrcodes/QRCodeProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->d()V

    .line 63
    return-void
.end method
