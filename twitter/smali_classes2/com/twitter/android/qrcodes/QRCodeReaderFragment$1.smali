.class Lcom/twitter/android/qrcodes/QRCodeReaderFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$1;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 124
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$1;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "qr:qr_scan::image_picker:click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$1;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {}, Lbrv;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 126
    return-void
.end method
