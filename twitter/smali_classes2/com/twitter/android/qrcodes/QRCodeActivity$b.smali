.class Lcom/twitter/android/qrcodes/QRCodeActivity$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/qrcodes/QRCodeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/qrcodes/QRCodeActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/qrcodes/QRCodeActivity;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$b;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/qrcodes/QRCodeActivity;Lcom/twitter/android/qrcodes/QRCodeActivity$1;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeActivity$b;-><init>(Lcom/twitter/android/qrcodes/QRCodeActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 238
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$b;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeActivity;->e(Lcom/twitter/android/qrcodes/QRCodeActivity;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 239
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$b;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeActivity;->c(Lcom/twitter/android/qrcodes/QRCodeActivity;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 256
    :goto_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 257
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$b;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/qrcodes/QRCodeActivity;->setResult(I)V

    .line 258
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$b;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->finish()V

    .line 259
    return-void

    .line 241
    :pswitch_0
    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "qr:qr_profile:::cancel"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 245
    :pswitch_1
    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "qr:qr_scan:::cancel"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 249
    :pswitch_2
    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "qr:user_card:::cancel"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
