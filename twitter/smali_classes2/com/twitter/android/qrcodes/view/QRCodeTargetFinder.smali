.class public Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;
.super Landroid/view/View;
.source "Twttr"


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Path;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    .line 31
    sget-object v0, Lcom/twitter/android/bi$a;->QRCodeTargetFinder:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 34
    const/4 v0, 0x2

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 35
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 36
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    .line 37
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    .line 38
    iget-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 39
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    return-void

    .line 42
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 48
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->getHeight()I

    move-result v0

    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->getWidth()I

    move-result v1

    .line 51
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    .line 52
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 53
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    invoke-virtual {v2, v5, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 54
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    int-to-float v3, v3

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 55
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 57
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    .line 58
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    sub-int v3, v1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 59
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    int-to-float v3, v1

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 60
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    int-to-float v3, v1

    iget v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 61
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 63
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    .line 64
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    int-to-float v3, v1

    iget v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    sub-int v4, v0, v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 65
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    int-to-float v3, v1

    int-to-float v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 66
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    int-to-float v3, v0

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 67
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 69
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->rewind()V

    .line 70
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    int-to-float v2, v2

    int-to-float v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 71
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    int-to-float v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 72
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->c:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {v1, v5, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->b:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeTargetFinder;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 74
    return-void
.end method
