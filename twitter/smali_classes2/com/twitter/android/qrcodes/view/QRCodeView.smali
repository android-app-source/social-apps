.class public Lcom/twitter/android/qrcodes/view/QRCodeView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private final a:Landroid/graphics/Bitmap;

.field private final b:Landroid/graphics/RectF;

.field private final c:Landroid/graphics/Paint;

.field private final d:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final e:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private j:Lkv;

.field private k:Landroid/widget/ImageView;

.field private l:Lcom/twitter/media/ui/image/UserImageView;

.field private m:F

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 70
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/qrcodes/view/QRCodeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->b:Landroid/graphics/RectF;

    .line 79
    invoke-virtual {p0, v3}, Lcom/twitter/android/qrcodes/view/QRCodeView;->setWillNotDraw(Z)V

    .line 80
    const/4 v0, 0x5

    new-array v0, v0, [I

    const v1, 0x7f110068

    .line 81
    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    aput v1, v0, v3

    const v1, 0x7f11018a

    .line 82
    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    aput v1, v0, v2

    const v1, 0x7f11006e

    .line 83
    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    aput v1, v0, v5

    const v1, 0x7f1100e4

    .line 84
    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    aput v1, v0, v4

    const v1, 0x7f1100e3

    .line 85
    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    aput v1, v0, v6

    .line 87
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 88
    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget v0, v0, v1

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->d:I

    .line 90
    sget-object v0, Lcom/twitter/android/bi$a;->QRCodeView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 93
    const/4 v0, 0x4

    const v2, 0x7f110195

    .line 94
    :try_start_0
    invoke-static {p1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 93
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->e:I

    .line 95
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->f:I

    .line 96
    const/4 v0, 0x3

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->g:I

    .line 97
    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->h:I

    .line 98
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 103
    iget v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->f:I

    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1}, Lcom/twitter/media/util/a;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->a:Landroid/graphics/Bitmap;

    .line 104
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    .line 105
    return-void

    .line 100
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method static a(I)I
    .locals 3

    .prologue
    .line 168
    div-int/lit8 v0, p0, 0x3

    .line 169
    rem-int/lit8 v1, v0, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lkv;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 157
    sget-object v1, Lcom/google/zxing/EncodeHintType;->b:Lcom/google/zxing/EncodeHintType;

    const-string/jumbo v2, "UTF-8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    :try_start_0
    sget-object v1, Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;->d:Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;

    invoke-static {p0, v1, v0}, Lkw;->a(Ljava/lang/String;Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;Ljava/util/Map;)Lkz;

    move-result-object v0

    invoke-virtual {v0}, Lkz;->a()Lkv;
    :try_end_0
    .catch Lcom/google/zxing/WriterException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 162
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 13

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    invoke-virtual {v0}, Lkv;->b()I

    move-result v4

    .line 190
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    invoke-virtual {v0}, Lkv;->a()I

    move-result v5

    .line 191
    new-instance v6, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->a:Landroid/graphics/Bitmap;

    invoke-direct {v6, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 192
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 195
    iget v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->n:I

    sub-int v0, v4, v0

    div-int/lit8 v1, v0, 0x2

    .line 196
    iget v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->n:I

    sub-int v0, v5, v0

    div-int/lit8 v3, v0, 0x2

    move v0, v1

    .line 197
    :goto_0
    iget v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->n:I

    add-int/2addr v2, v1

    if-ge v0, v2, :cond_1

    move v2, v3

    .line 198
    :goto_1
    iget v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->n:I

    add-int/2addr v7, v3

    if-ge v2, v7, :cond_0

    .line 199
    iget-object v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v2, v8}, Lkv;->a(IIZ)V

    .line 198
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 197
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_1
    add-int/lit8 v0, v4, -0x6

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_3

    .line 204
    add-int/lit8 v0, v5, -0x5

    :goto_3
    if-ge v0, v5, :cond_2

    .line 205
    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Lkv;->a(IIZ)V

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 203
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 209
    :cond_3
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 210
    const/16 v0, 0x8

    new-array v3, v0, [F

    .line 211
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->e:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 212
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 213
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v4, :cond_a

    .line 214
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v5, :cond_9

    .line 215
    iget-object v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    invoke-virtual {v7, v1, v0}, Lkv;->a(II)B

    move-result v7

    if-lez v7, :cond_8

    .line 216
    iget v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->g:I

    int-to-float v7, v7

    invoke-static {v3, v7}, Ljava/util/Arrays;->fill([FF)V

    .line 217
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->a(II)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 218
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/4 v11, 0x0

    aput v11, v3, v10

    aput v11, v3, v9

    aput v11, v3, v8

    aput v11, v3, v7

    .line 220
    :cond_4
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->b(II)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 221
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x0

    aput v11, v3, v10

    aput v11, v3, v9

    aput v11, v3, v8

    aput v11, v3, v7

    .line 223
    :cond_5
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->c(II)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 224
    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x0

    aput v11, v3, v10

    aput v11, v3, v9

    aput v11, v3, v8

    aput v11, v3, v7

    .line 226
    :cond_6
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->d(II)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 227
    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/4 v11, 0x0

    aput v11, v3, v10

    aput v11, v3, v9

    aput v11, v3, v8

    aput v11, v3, v7

    .line 229
    :cond_7
    int-to-float v7, v1

    iget v8, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->m:F

    mul-float/2addr v7, v8

    .line 230
    int-to-float v8, v0

    iget v9, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->m:F

    mul-float/2addr v8, v9

    .line 231
    iget-object v9, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->b:Landroid/graphics/RectF;

    .line 232
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v10

    int-to-float v10, v10

    .line 233
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v11

    int-to-float v11, v11

    iget v12, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->m:F

    add-float/2addr v7, v12

    .line 234
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v7, v7

    iget v12, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->m:F

    add-float/2addr v8, v12

    .line 235
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    int-to-float v8, v8

    .line 231
    invoke-virtual {v9, v10, v11, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 236
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 237
    iget-object v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->b:Landroid/graphics/RectF;

    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v7, v3, v8}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 238
    iget-object v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    invoke-virtual {v6, v2, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 214
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_5

    .line 213
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_4

    .line 242
    :cond_a
    return-void
.end method

.method private a(II)Z
    .locals 2

    .prologue
    .line 173
    add-int/lit8 v0, p1, -0x1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2}, Lkv;->a(II)B

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 246
    const/high16 v1, 0x40a00000    # 5.0f

    iget v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->m:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 247
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->k:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->l:Lcom/twitter/media/ui/image/UserImageView;

    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->n:I

    add-int/lit8 v1, v1, -0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->m:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 250
    return-void
.end method

.method private b(II)Z
    .locals 2

    .prologue
    .line 177
    add-int/lit8 v0, p2, -0x1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lkv;->a(II)B

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(II)Z
    .locals 2

    .prologue
    .line 181
    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    invoke-virtual {v1}, Lkv;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1, p2}, Lkv;->a(II)B

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(II)Z
    .locals 2

    .prologue
    .line 185
    add-int/lit8 v0, p2, 0x1

    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    invoke-virtual {v1}, Lkv;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, p1, v1}, Lkv;->a(II)B

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getQRCodeMatrix()Lkv;
    .locals 1

    .prologue
    .line 254
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 255
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 117
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->getWidth()I

    move-result v0

    .line 119
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->getHeight()I

    move-result v1

    .line 120
    iget v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->f:I

    sub-int v2, v0, v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    .line 121
    iget v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->f:I

    sub-int v3, v1, v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 123
    iget-object v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->b:Landroid/graphics/RectF;

    iget v5, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->i:I

    int-to-float v5, v5

    iget v6, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->i:I

    int-to-float v6, v6

    iget v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->i:I

    sub-int/2addr v0, v7

    int-to-float v0, v0

    iget v7, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->i:I

    sub-int/2addr v1, v7

    int-to-float v1, v1

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->b:Landroid/graphics/RectF;

    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->h:I

    int-to-float v1, v1

    iget v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->h:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 131
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->e:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 133
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->i:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->b:Landroid/graphics/RectF;

    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->h:I

    int-to-float v1, v1

    iget v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->h:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 136
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->a:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 138
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 109
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 110
    const v0, 0x7f130161

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->k:Landroid/widget/ImageView;

    .line 111
    const v0, 0x7f130097

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->l:Lcom/twitter/media/ui/image/UserImageView;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->l:Lcom/twitter/media/ui/image/UserImageView;

    const v1, 0x7f0e04b3

    const v2, 0x7f110173

    invoke-virtual {v0, v1, v2}, Lcom/twitter/media/ui/image/UserImageView;->b(II)V

    .line 113
    return-void
.end method

.method public setLogo(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 266
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 267
    iput-object p1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->k:Landroid/widget/ImageView;

    .line 268
    return-void
.end method

.method public setUser(Lcom/twitter/model/core/TwitterUser;)V
    .locals 3

    .prologue
    .line 141
    if-eqz p1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->l:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 143
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->a(Ljava/lang/String;)Lkv;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    .line 144
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->j:Lkv;

    invoke-virtual {v0}, Lkv;->b()I

    move-result v0

    .line 146
    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->f:I

    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->m:F

    .line 147
    invoke-static {v0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->n:I

    .line 148
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->a()V

    .line 149
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/view/QRCodeView;->b()V

    .line 152
    :cond_0
    return-void
.end method

.method public setUserImageView(Lcom/twitter/media/ui/image/UserImageView;)V
    .locals 0

    .prologue
    .line 260
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 261
    iput-object p1, p0, Lcom/twitter/android/qrcodes/view/QRCodeView;->l:Lcom/twitter/media/ui/image/UserImageView;

    .line 262
    return-void
.end method
