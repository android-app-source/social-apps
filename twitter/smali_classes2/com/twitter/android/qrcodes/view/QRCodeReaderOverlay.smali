.class public Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    .line 51
    invoke-virtual {p0, v1}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->setWillNotDraw(Z)V

    .line 52
    sget-object v0, Lcom/twitter/android/bi$a;->QRCodeReaderOverlay:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 55
    const/4 v0, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->a:I

    .line 56
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->b:I

    .line 57
    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->d:Landroid/graphics/Paint;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->d:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 63
    return-void

    .line 59
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(II)V
    .locals 6

    .prologue
    .line 101
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->e:Landroid/graphics/Bitmap;

    .line 102
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->e:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 103
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->e:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 104
    iget v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 105
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->d:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 106
    return-void
.end method


# virtual methods
.method public getCutoutRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 94
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->a(II)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->e:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 98
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 68
    const v0, 0x7f1307ad

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    .line 69
    const v0, 0x7f1307af

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->g:Landroid/view/View;

    .line 70
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 74
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    .line 76
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 77
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    .line 78
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 80
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/graphics/Rect;->set(IIII)V

    .line 81
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->b:I

    iget v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->a:I

    invoke-static {v2, v3}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/util/math/b;->a(Landroid/graphics/Rect;Lcom/twitter/util/math/Size;)Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    .line 82
    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/twitter/util/math/b;->a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    .line 84
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 85
    iget-object v0, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    .line 86
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    .line 87
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    .line 88
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->f:Landroid/view/View;

    .line 89
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v5

    .line 85
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 90
    return-void
.end method
