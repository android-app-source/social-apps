.class Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/qrcodes/QRCodeReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

.field private final b:Landroid/os/HandlerThread;

.field private final c:Landroid/os/Handler;

.field private d:I


# direct methods
.method constructor <init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)V
    .locals 2

    .prologue
    .line 363
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->d:I

    .line 364
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "QR_CODE_READER_THREAD"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->b:Landroid/os/HandlerThread;

    .line 365
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 366
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->c:Landroid/os/Handler;

    .line 367
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;[B)V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a([B)V

    return-void
.end method

.method private a([B)V
    .locals 9

    .prologue
    .line 391
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->c(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Lcom/twitter/android/media/camera/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->d(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Lcom/twitter/util/math/Size;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 392
    new-instance v0, Lcom/google/zxing/e;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    .line 393
    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->d(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Lcom/twitter/util/math/Size;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->a()I

    move-result v2

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->d(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Lcom/twitter/util/math/Size;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->b()I

    move-result v3

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    .line 394
    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/graphics/Rect;

    move-result-object v1

    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/graphics/Rect;

    move-result-object v1

    iget v5, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v7

    const/4 v8, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/zxing/e;-><init>([BIIIIIIZ)V

    .line 396
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a:Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    invoke-static {v1, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Lcom/google/zxing/d;)Z

    .line 398
    :cond_0
    return-void
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 3

    .prologue
    .line 371
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c$1;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;[B)V

    .line 378
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-gt v1, v2, :cond_1

    .line 379
    iget v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->d:I

    rem-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->d:I

    .line 380
    iget v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->d:I

    if-nez v1, :cond_0

    .line 381
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 383
    :cond_0
    iget v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->d:I

    .line 387
    :goto_0
    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 388
    return-void

    .line 385
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
