.class public abstract Lcom/twitter/android/qrcodes/QRCodeFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/qrcodes/QRCodeFragment$a;
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/android/qrcodes/QRCodeFragment$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    .line 23
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/twitter/android/qrcodes/QRCodeFragment$a;

    move-object v1, v0

    iput-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeFragment;->a:Lcom/twitter/android/qrcodes/QRCodeFragment$a;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    return-void

    .line 24
    :catch_0
    move-exception v1

    .line 25
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " must implement QRCodeFragmentListener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f1307ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/twitter/android/qrcodes/QRCodeFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/qrcodes/QRCodeFragment$1;-><init>(Lcom/twitter/android/qrcodes/QRCodeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    return-void
.end method
