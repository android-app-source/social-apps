.class public Lcom/twitter/android/qrcodes/QRCodeReaderFragment;
.super Lcom/twitter/android/qrcodes/QRCodeFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/qrcodes/QRCodeReaderFragment$b;,
        Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;,
        Lcom/twitter/android/qrcodes/QRCodeReaderFragment$a;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final d:Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;

.field private final e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment$a;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/zxing/DecodeHintType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lkt;

.field private h:Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;

.field private i:Landroid/view/View;

.field private j:Landroid/view/animation/Animation;

.field private k:Lcom/twitter/android/media/camera/c;

.field private l:Lcom/twitter/util/math/Size;

.field private m:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, "^https?://twitter\\.com/@?([a-zA-Z0-9_]+)/?$"

    .line 83
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->b:Ljava/util/regex/Pattern;

    .line 84
    const-string/jumbo v0, "^https?://.*$"

    .line 85
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->c:Ljava/util/regex/Pattern;

    .line 84
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeFragment;-><init>()V

    .line 87
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;

    invoke-direct {v0, p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->d:Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;

    .line 88
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$a;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Lcom/twitter/android/qrcodes/QRCodeReaderFragment$1;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment$a;

    .line 89
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->f:Ljava/util/Map;

    .line 90
    new-instance v0, Lkt;

    invoke-direct {v0}, Lkt;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->g:Lkt;

    return-void
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 234
    int-to-float v0, p0

    int-to-float v1, p1

    div-float/2addr v0, v1

    const/high16 v1, 0x44fa0000    # 2000.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a_:J

    return-wide v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {v0, p1, v1}, Lcom/twitter/media/model/MediaFile;->b(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$b;

    invoke-direct {v1, p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$b;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)V

    .line 428
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 429
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Lcom/twitter/media/model/MediaFile;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method

.method private a(Lcom/twitter/media/model/MediaFile;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 432
    invoke-static {p1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 433
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    if-eqz v1, :cond_0

    .line 434
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->d(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;

    .line 436
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_2

    .line 438
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 439
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 440
    mul-int v1, v3, v7

    new-array v1, v1, [I

    move v4, v2

    move v5, v2

    move v6, v3

    .line 441
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 442
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 443
    new-instance v0, Lcom/google/zxing/f;

    invoke-direct {v0, v3, v7, v1}, Lcom/google/zxing/f;-><init>(II[I)V

    .line 444
    invoke-direct {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Lcom/google/zxing/d;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 445
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->g()V

    .line 450
    :cond_1
    :goto_0
    return-void

    .line 448
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->g()V

    goto :goto_0
.end method

.method private a(Lcom/google/zxing/d;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 453
    const/4 v0, 0x2

    new-array v4, v0, [Lcom/google/zxing/d;

    invoke-virtual {p1}, Lcom/google/zxing/d;->b()Lcom/google/zxing/d;

    move-result-object v0

    aput-object v0, v4, v2

    aput-object p1, v4, v1

    .line 455
    array-length v5, v4

    move v3, v2

    move v0, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    .line 456
    new-instance v7, Lcom/google/zxing/b;

    new-instance v8, Lcom/google/zxing/common/i;

    invoke-direct {v8, v6}, Lcom/google/zxing/common/i;-><init>(Lcom/google/zxing/d;)V

    invoke-direct {v7, v8}, Lcom/google/zxing/b;-><init>(Lcom/google/zxing/a;)V

    .line 458
    :try_start_0
    iget-object v6, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->g:Lkt;

    iget-object v8, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->f:Ljava/util/Map;

    invoke-virtual {v6, v7, v8}, Lkt;->a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/g;

    move-result-object v6

    .line 459
    invoke-virtual {v6}, Lcom/google/zxing/g;->a()Ljava/lang/String;

    move-result-object v6

    .line 460
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e()V

    .line 461
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    new-instance v8, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$4;

    invoke-direct {v8, p0, v6}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$4;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lcom/google/zxing/FormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/zxing/ChecksumException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 455
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 469
    :catch_0
    move-exception v6

    .line 470
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 473
    :cond_0
    array-length v3, v4

    if-ge v0, v3, :cond_1

    move v0, v1

    :goto_3
    return v0

    :cond_1
    move v0, v2

    goto :goto_3

    .line 469
    :catch_1
    move-exception v6

    goto :goto_2

    :catch_2
    move-exception v6

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Lcom/google/zxing/d;)Z
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Lcom/google/zxing/d;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Lcom/twitter/android/media/camera/c;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 302
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "qr:qr_scan:::external_link"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 303
    new-instance v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$3;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Ljava/lang/String;)V

    .line 311
    const v1, 0x7f0a072f

    invoke-virtual {p0, v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p1, v0, v4}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Z)V

    .line 312
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a_:J

    return-wide v0
.end method

.method private f()V
    .locals 6

    .prologue
    .line 216
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    .line 217
    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->a()I

    move-result v2

    invoke-static {v1, v2}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(II)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    .line 218
    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->b()I

    move-result v3

    invoke-static {v2, v3}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(II)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    .line 219
    invoke-virtual {v4}, Lcom/twitter/util/math/Size;->a()I

    move-result v4

    invoke-static {v3, v4}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(II)I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    .line 220
    invoke-virtual {v5}, Lcom/twitter/util/math/Size;->b()I

    move-result v5

    invoke-static {v4, v5}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(II)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 222
    new-instance v1, Landroid/hardware/Camera$Area;

    const/16 v2, 0x3e8

    invoke-direct {v1, v0, v2}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    .line 223
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera$Area;)Z

    .line 224
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    const-string/jumbo v2, "continuous-picture"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera$Area;Ljava/lang/String;)Z

    .line 225
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->i:Landroid/view/View;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 316
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "qr:qr_scan:::invalid_qr_code"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 317
    const v0, 0x7f0a072d

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->b(I)V

    .line 318
    return-void
.end method

.method static synthetic h(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->j:Landroid/view/animation/Animation;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/media/camera/c;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    .line 177
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 271
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/qrcodes/QRCodeFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 272
    packed-switch p2, :pswitch_data_0

    .line 298
    :goto_0
    return-void

    .line 274
    :pswitch_0
    check-cast p1, Lbio;

    .line 275
    iget-object v1, p1, Lbio;->a:Lcom/twitter/model/core/TwitterUser;

    if-eqz v1, :cond_0

    .line 276
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a:Lcom/twitter/android/qrcodes/QRCodeFragment$a;

    iget-object v1, p1, Lbio;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-interface {v0, v1}, Lcom/twitter/android/qrcodes/QRCodeFragment$a;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_0

    .line 277
    :cond_0
    iget-object v1, p1, Lbio;->b:Lcom/twitter/model/core/z;

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p1, Lbio;->b:Lcom/twitter/model/core/z;

    invoke-static {v1}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v1

    .line 279
    array-length v2, v1

    if-eqz v2, :cond_1

    aget v0, v1, v0

    .line 280
    :cond_1
    packed-switch v0, :pswitch_data_1

    .line 286
    const v0, 0x7f0a09ce

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->b(I)V

    goto :goto_0

    .line 282
    :pswitch_1
    const v0, 0x7f0a0945

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->b(I)V

    goto :goto_0

    .line 290
    :cond_2
    const v0, 0x7f0a09cd

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->b(I)V

    goto :goto_0

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 280
    :pswitch_data_1
    .packed-switch 0x3f
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 246
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->g()V

    .line 260
    :goto_0
    return-void

    .line 249
    :cond_0
    sget-object v0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 250
    sget-object v1, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 251
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 252
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 253
    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    invoke-direct {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :cond_2
    const v0, 0x7f0a0734

    invoke-virtual {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, p1, v1, v2}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Z)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Z)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 329
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 330
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 331
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0616

    .line 332
    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 333
    if-eqz p4, :cond_0

    .line 334
    const v1, 0x7f0a00f6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 336
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 337
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e:Lcom/twitter/android/qrcodes/QRCodeReaderFragment$a;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 338
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 339
    return-void
.end method

.method public b(I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 322
    invoke-virtual {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v0, v2, v1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Z)V

    .line 323
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 264
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "qr:qr_scan:::user"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 265
    new-instance v1, Lbio;

    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    const-wide/16 v4, 0x0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lbio;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    .line 266
    invoke-virtual {p0, v1, v7, v7}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 267
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 181
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 182
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->i:Landroid/view/View;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 183
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->a()Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    .line 186
    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v1, v2}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->l:Lcom/twitter/util/math/Size;

    .line 187
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/ui/k;->d(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v1

    .line 188
    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->a()I

    move-result v2

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->b()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    .line 189
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 190
    iget-object v2, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->h:Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;

    invoke-virtual {v2}, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;->getCutoutRect()Landroid/graphics/Rect;

    move-result-object v2

    .line 191
    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 192
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v1, v4, v4, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    .line 209
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->f()V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->d:Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera$PreviewCallback;)V

    .line 213
    :cond_1
    return-void

    .line 194
    :cond_2
    iget v3, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v1

    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_3

    iget v3, v2, Landroid/graphics/Rect;->top:I

    .line 195
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v1

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_3

    .line 196
    new-instance v0, Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-int v3, v3

    iget v4, v2, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    div-float/2addr v4, v1

    float-to-int v4, v4

    iget v5, v2, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    div-float/2addr v5, v1

    float-to-int v5, v5

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    div-float v1, v2, v1

    float-to-int v1, v1

    invoke-direct {v0, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    goto :goto_0

    .line 202
    :cond_3
    new-instance v0, Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-int v3, v3

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    div-float/2addr v4, v1

    float-to-int v4, v4

    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    div-float/2addr v5, v1

    float-to-int v5, v5

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    div-float v1, v2, v1

    float-to-int v1, v1

    invoke-direct {v0, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->m:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->d:Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;->a(Lcom/twitter/android/qrcodes/QRCodeReaderFragment$c;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 239
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->k:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera$PreviewCallback;)V

    .line 242
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 162
    packed-switch p1, :pswitch_data_0

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 164
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 165
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 162
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 109
    const v0, 0x7f04033b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 148
    invoke-super {p0}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onStart()V

    .line 149
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "qr:qr_scan:::impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 150
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 154
    invoke-super {p0}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onStop()V

    .line 155
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->e()V

    .line 156
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 114
    invoke-super {p0, p1, p2}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->f:Ljava/util/Map;

    sget-object v1, Lcom/google/zxing/DecodeHintType;->e:Lcom/google/zxing/DecodeHintType;

    const-string/jumbo v2, "UTF-8"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->f:Ljava/util/Map;

    sget-object v1, Lcom/google/zxing/DecodeHintType;->d:Lcom/google/zxing/DecodeHintType;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    const v0, 0x7f1307b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 120
    new-instance v1, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$1;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const v0, 0x7f1307af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->i:Landroid/view/View;

    .line 130
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3e4ccccd    # 0.2f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->j:Landroid/view/animation/Animation;

    .line 131
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->j:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 134
    const v0, 0x7f1307ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->h:Lcom/twitter/android/qrcodes/view/QRCodeReaderOverlay;

    .line 136
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment$2;-><init>(Lcom/twitter/android/qrcodes/QRCodeReaderFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 143
    return-void
.end method
