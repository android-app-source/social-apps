.class Lcom/twitter/android/qrcodes/QRCodeActivity$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/camera/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/qrcodes/QRCodeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/qrcodes/QRCodeActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/qrcodes/QRCodeActivity;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/qrcodes/QRCodeActivity;Lcom/twitter/android/qrcodes/QRCodeActivity$1;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeActivity$a;-><init>(Lcom/twitter/android/qrcodes/QRCodeActivity;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f13014c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 209
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0619

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 210
    return-void
.end method

.method public a(Landroid/hardware/Camera;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->a(Lcom/twitter/android/qrcodes/QRCodeActivity;)Lcom/twitter/android/media/widget/CameraPreviewContainer;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a()V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->b(Lcom/twitter/android/qrcodes/QRCodeActivity;)Lcom/twitter/android/media/camera/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->g()V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->c(Lcom/twitter/android/qrcodes/QRCodeActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeActivity$a;->a:Lcom/twitter/android/qrcodes/QRCodeActivity;

    invoke-static {v0}, Lcom/twitter/android/qrcodes/QRCodeActivity;->d(Lcom/twitter/android/qrcodes/QRCodeActivity;)Lcom/twitter/android/qrcodes/QRCodeReaderFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/qrcodes/QRCodeReaderFragment;->d()V

    .line 204
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/media/model/ImageFile;)V
    .locals 0

    .prologue
    .line 231
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 219
    return-void
.end method

.method public a(Ljava/util/Set;Ljava/lang/CharSequence;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 215
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 227
    return-void
.end method
