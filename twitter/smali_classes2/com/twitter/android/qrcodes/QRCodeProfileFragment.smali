.class public Lcom/twitter/android/qrcodes/QRCodeProfileFragment;
.super Lcom/twitter/android/qrcodes/QRCodeFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/qrcodes/QRCodeProfileFragment$a;,
        Lcom/twitter/android/qrcodes/QRCodeProfileFragment$b;
    }
.end annotation


# instance fields
.field private b:Lcom/twitter/android/qrcodes/view/QRCodeView;

.field private c:Lcom/twitter/model/core/TwitterUser;

.field private d:Lcom/twitter/android/dialog/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/android/qrcodes/QRCodeFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->a_:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->a_:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->a_:J

    return-wide v0
.end method

.method static synthetic d(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)Lcom/twitter/android/qrcodes/view/QRCodeView;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->b:Lcom/twitter/android/qrcodes/view/QRCodeView;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->a_:J

    return-wide v0
.end method


# virtual methods
.method public d()V
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->d:Lcom/twitter/android/dialog/e;

    new-instance v2, Lcom/twitter/android/dialog/d;

    const v3, 0x7f0a07b3

    .line 90
    invoke-virtual {p0, v3}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$3;

    invoke-direct {v4, p0, v0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$3;-><init>(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;Landroid/content/Context;)V

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/twitter/android/dialog/d;

    const/4 v4, 0x0

    new-instance v5, Lcom/twitter/android/dialog/d;

    const v6, 0x7f0a08a6

    .line 98
    invoke-virtual {p0, v6}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;

    invoke-direct {v7, p0, v0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$4;-><init>(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;Landroid/content/Context;)V

    invoke-direct {v5, v6, v7}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    aput-object v5, v3, v4

    .line 89
    invoke-static {v2, v3}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/dialog/e;->a(Ljava/util/List;)Landroid/app/Dialog;

    .line 108
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->c:Lcom/twitter/model/core/TwitterUser;

    .line 43
    new-instance v0, Lcom/twitter/android/dialog/e;

    invoke-virtual {p0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/dialog/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->d:Lcom/twitter/android/dialog/e;

    .line 44
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 50
    const v0, 0x7f04033a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 82
    invoke-super {p0}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onStart()V

    .line 83
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "qr:qr_profile:::impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 84
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1, p2}, Lcom/twitter/android/qrcodes/QRCodeFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f130508

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$1;-><init>(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v0, 0x7f1307ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/qrcodes/view/QRCodeView;

    iput-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->b:Lcom/twitter/android/qrcodes/view/QRCodeView;

    .line 67
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->b:Lcom/twitter/android/qrcodes/view/QRCodeView;

    iget-object v1, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->c:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v1}, Lcom/twitter/android/qrcodes/view/QRCodeView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/qrcodes/QRCodeProfileFragment;->b:Lcom/twitter/android/qrcodes/view/QRCodeView;

    new-instance v1, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/qrcodes/QRCodeProfileFragment$2;-><init>(Lcom/twitter/android/qrcodes/QRCodeProfileFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/qrcodes/view/QRCodeView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 77
    return-void
.end method
