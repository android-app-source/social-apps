.class Lcom/twitter/android/ProfileActivity$g;
.super Lcom/twitter/android/AbsPagesAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ProfileActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "g"
.end annotation


# instance fields
.field final synthetic g:Lcom/twitter/android/ProfileActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/internal/android/widget/HorizontalListView;",
            "Lcom/twitter/android/at;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3005
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity$g;->g:Lcom/twitter/android/ProfileActivity;

    .line 3006
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/AbsPagesAdapter;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    .line 3007
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3010
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity$g;->b:Ljava/util/List;

    .line 3011
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity$g;->d()V

    .line 3012
    return-void
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3016
    .line 3017
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsPagesAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ScrollingHeaderListFragment;

    .line 3018
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity$g;->g:Lcom/twitter/android/ProfileActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 3019
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->d(Z)V

    .line 3022
    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->f(I)V

    .line 3024
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/ProfileActivity$g;->a(Lcom/twitter/app/common/base/BaseFragment;I)V

    .line 3025
    return-object v0

    .line 3019
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 3

    .prologue
    .line 3044
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageScrollStateChanged(I)V

    .line 3045
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 3046
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 3047
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity$g;->c(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    .line 3048
    iget-object v2, p0, Lcom/twitter/android/ProfileActivity$g;->g:Lcom/twitter/android/ProfileActivity;

    invoke-virtual {v2, v0}, Lcom/twitter/android/ProfileActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 3051
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3

    .prologue
    .line 3030
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$g;->e:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->a()I

    move-result v0

    .line 3031
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity$g;->g:Lcom/twitter/android/ProfileActivity;

    sget-object v2, Lcom/twitter/android/ProfileActivity;->b:Landroid/net/Uri;

    invoke-static {v1, p1, v2}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/android/ProfileActivity;ILandroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3032
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity$g;->g:Lcom/twitter/android/ProfileActivity;

    invoke-static {v1}, Lcom/twitter/android/ProfileActivity;->g(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/android/metrics/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/metrics/d;->k()V

    .line 3035
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageSelected(I)V

    .line 3036
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity$g;->c(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity$g;->a(Lcom/twitter/library/client/m;)Z

    .line 3037
    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileActivity$g;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity$g;->b(Lcom/twitter/library/client/m;)Z

    .line 3039
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$g;->g:Lcom/twitter/android/ProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/ProfileActivity;->y:Lcom/twitter/android/ScrollingHeaderActivity$d;

    invoke-virtual {v0}, Lcom/twitter/android/ScrollingHeaderActivity$d;->a()V

    .line 3040
    return-void
.end method
