.class Lcom/twitter/android/GalleryActivity$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/GalleryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lsl;",
        "Ljava/lang/Void;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lsj;


# direct methods
.method constructor <init>(Landroid/content/Context;Lsj;)V
    .locals 1

    .prologue
    .line 991
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 992
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity$a;->a:Landroid/content/Context;

    .line 993
    iput-object p2, p0, Lcom/twitter/android/GalleryActivity$a;->b:Lsj;

    .line 994
    return-void
.end method

.method private a(Lso;)Ljava/io/File;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1029
    invoke-virtual {p1}, Lso;->f()Landroid/widget/ImageView;

    move-result-object v0

    .line 1031
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1032
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    instance-of v2, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    .line 1033
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1035
    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity$a;->a:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 1036
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5f

    invoke-static {v0, v2, v3}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)Ljava/io/File;

    move-result-object v2

    .line 1037
    if-eqz v2, :cond_0

    .line 1039
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity$a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/media/util/o;->a(Landroid/content/Context;)Lcom/twitter/media/util/o;

    move-result-object v0

    new-instance v1, Lcom/twitter/media/util/j;

    invoke-direct {v1, v2}, Lcom/twitter/media/util/j;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/util/o;->b(Lcom/twitter/media/util/o$a;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1041
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->c()Lcqr;

    move-result-object v1

    invoke-interface {v1, v2}, Lcqr;->b(Ljava/io/File;)Z

    .line 1045
    :goto_1
    return-object v0

    .line 1041
    :catchall_0
    move-exception v0

    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->c()Lcqr;

    move-result-object v1

    invoke-interface {v1, v2}, Lcqr;->b(Ljava/io/File;)Z

    throw v0

    :cond_0
    move-object v0, v1

    .line 1045
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private static b(Ljava/io/File;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1049
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1050
    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1051
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1053
    iget-object v2, v1, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string/jumbo v2, "image/webp"

    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Lsl;)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 999
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Lso;

    .line 1001
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity$a;->b:Lsj;

    iget v3, v0, Lso;->a:I

    .line 1002
    invoke-virtual {v2, v3}, Lsj;->a(I)Lsn;

    move-result-object v2

    .line 1003
    :goto_0
    if-eqz v2, :cond_2

    .line 1004
    iget-object v3, p0, Lcom/twitter/android/GalleryActivity$a;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v3

    iget-object v4, v2, Lsn;->c:Lcom/twitter/media/request/a$a;

    .line 1005
    invoke-virtual {v3, v4}, Lcom/twitter/library/media/manager/g;->d(Lcom/twitter/media/request/a$a;)Ljava/io/File;

    move-result-object v3

    .line 1006
    if-eqz v3, :cond_2

    .line 1007
    iget-object v1, v2, Lsn;->c:Lcom/twitter/media/request/a$a;

    iget-object v1, v1, Lcom/twitter/media/request/a$a;->p:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 1008
    monitor-enter v3

    .line 1009
    :try_start_0
    invoke-static {v3}, Lcom/twitter/android/GalleryActivity$a;->b(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1013
    invoke-direct {p0, v0}, Lcom/twitter/android/GalleryActivity$a;->a(Lso;)Ljava/io/File;

    move-result-object v0

    monitor-exit v3

    .line 1022
    :goto_1
    return-object v0

    :cond_0
    move-object v2, v1

    .line 1002
    goto :goto_0

    .line 1015
    :cond_1
    new-instance v0, Lcom/twitter/media/util/j;

    invoke-direct {v0, v3}, Lcom/twitter/media/util/j;-><init>(Ljava/io/File;)V

    .line 1016
    iput-object v1, v0, Lcom/twitter/media/util/j;->k:Ljava/lang/String;

    .line 1017
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity$a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/media/util/o;->a(Landroid/content/Context;)Lcom/twitter/media/util/o;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/media/util/o;->a(Lcom/twitter/media/util/o$a;)Ljava/io/File;

    move-result-object v0

    monitor-exit v3

    goto :goto_1

    .line 1019
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    .line 1022
    goto :goto_1
.end method

.method protected a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 1058
    if-eqz p1, :cond_0

    const v0, 0x7f0a07b6

    .line 1059
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity$a;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1060
    return-void

    .line 1058
    :cond_0
    const v0, 0x7f0a07b5

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 987
    check-cast p1, [Lsl;

    invoke-virtual {p0, p1}, Lcom/twitter/android/GalleryActivity$a;->a([Lsl;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 987
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/twitter/android/GalleryActivity$a;->a(Ljava/io/File;)V

    return-void
.end method
