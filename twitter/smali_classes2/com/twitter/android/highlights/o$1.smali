.class Lcom/twitter/android/highlights/o$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/highlights/o;->b(Lcom/twitter/android/widget/highlights/StoriesViewPager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/highlights/StoriesViewPager;

.field final synthetic b:Lcom/twitter/android/highlights/o;

.field private c:I


# direct methods
.method constructor <init>(Lcom/twitter/android/highlights/o;Lcom/twitter/android/widget/highlights/StoriesViewPager;)V
    .locals 1

    .prologue
    .line 249
    iput-object p1, p0, Lcom/twitter/android/highlights/o$1;->b:Lcom/twitter/android/highlights/o;

    iput-object p2, p0, Lcom/twitter/android/highlights/o$1;->a:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/highlights/o$1;->c:I

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Lcom/twitter/android/highlights/o$1;->a:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/highlights/o$1;->a:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 257
    :goto_0
    if-eqz v0, :cond_3

    .line 258
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 259
    iget v1, p0, Lcom/twitter/android/highlights/o$1;->c:I

    sub-int/2addr v0, v1

    .line 260
    if-eqz v0, :cond_1

    .line 261
    iget-object v1, p0, Lcom/twitter/android/highlights/o$1;->a:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->b(F)V

    .line 263
    :cond_1
    iget v1, p0, Lcom/twitter/android/highlights/o$1;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/highlights/o$1;->c:I

    .line 268
    :goto_1
    return-void

    .line 256
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 266
    :cond_3
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_1
.end method
