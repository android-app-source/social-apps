.class public Lcom/twitter/android/highlights/ah;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/highlights/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 17
    const v0, 0x7f040157

    return v0
.end method

.method public a(Lcom/twitter/android/highlights/x;Lcom/twitter/android/highlights/y;Landroid/content/Context;Lcom/twitter/android/highlights/q$a;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    .line 47
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 48
    check-cast p1, Lcom/twitter/android/highlights/ai;

    .line 49
    check-cast p2, Lcom/twitter/android/highlights/ai$a;

    .line 51
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->c:Landroid/widget/TextView;

    const v1, 0x7f0a0b92

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    iget-object v4, v4, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v8, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->e:Landroid/widget/TextView;

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/highlights/ai;->a(Landroid/content/Context;Lcne;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->d:Landroid/widget/TextView;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->q:J

    invoke-static {v1, v2, v3}, Lcom/twitter/util/aa;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    .line 58
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->l:I

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 63
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 64
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setTag(Ljava/lang/Object;)V

    .line 65
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->g:Lcom/twitter/library/media/widget/TweetMediaView;

    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->f:Landroid/view/View;

    iget-object v2, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/highlights/af;->a(Lcom/twitter/library/media/widget/TweetMediaView;Landroid/view/View;Lcom/twitter/model/core/Tweet;)V

    .line 67
    new-instance v0, Lcom/twitter/android/highlights/u;

    iget-object v2, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    iget-object v3, p2, Lcom/twitter/android/highlights/ai$a;->h:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v4, p2, Lcom/twitter/android/highlights/ai$a;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 68
    invoke-static {p1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v5

    move-object v1, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/highlights/u;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->h:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 71
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 74
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->k:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->l:Landroid/widget/TextView;

    const v1, 0x7f0a0b92

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    iget-object v4, v4, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v8, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->n:Landroid/widget/TextView;

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/highlights/ai;->b(Landroid/content/Context;Lcne;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->m:Landroid/widget/TextView;

    .line 79
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->q:J

    .line 78
    invoke-static {v1, v2, v3}, Lcom/twitter/util/aa;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    .line 81
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 82
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->l:I

    .line 83
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    .line 84
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 86
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->j:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 87
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->j:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setTag(Ljava/lang/Object;)V

    .line 88
    iget-object v0, p2, Lcom/twitter/android/highlights/ai$a;->H:Lcom/twitter/library/media/widget/TweetMediaView;

    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->o:Landroid/view/View;

    iget-object v2, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/highlights/af;->a(Lcom/twitter/library/media/widget/TweetMediaView;Landroid/view/View;Lcom/twitter/model/core/Tweet;)V

    .line 90
    new-instance v0, Lcom/twitter/android/highlights/u;

    iget-object v2, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    iget-object v3, p2, Lcom/twitter/android/highlights/ai$a;->p:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v4, p2, Lcom/twitter/android/highlights/ai$a;->G:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 91
    invoke-static {p1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v5

    move-object v1, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/highlights/u;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->p:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 94
    iget-object v1, p2, Lcom/twitter/android/highlights/ai$a;->G:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 95
    return-void
.end method

.method public a(Lcom/twitter/android/highlights/y;Landroid/view/LayoutInflater;Lcom/twitter/android/highlights/q$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 29
    check-cast p1, Lcom/twitter/android/highlights/ai$a;

    .line 31
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->D:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ObservableScrollView;->setTag(Ljava/lang/Object;)V

    .line 32
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->D:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v0, p3}, Lcom/twitter/library/widget/ObservableScrollView;->setObservableScrollViewListener(Lcom/twitter/library/widget/ObservableScrollView$a;)V

    .line 33
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p3}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->j:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p3}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->g:Lcom/twitter/library/media/widget/TweetMediaView;

    invoke-virtual {v0, p3}, Lcom/twitter/library/media/widget/TweetMediaView;->setOnMediaClickListener(Lcom/twitter/library/media/widget/TweetMediaView$b;)V

    .line 36
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->h:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-static {v1, v1, v0, p3}, Lcom/twitter/android/highlights/z;->a(ZZLandroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 37
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-static {v1, v1, v0, p3}, Lcom/twitter/android/highlights/z;->a(ZZLandroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 38
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->H:Lcom/twitter/library/media/widget/TweetMediaView;

    invoke-virtual {v0, p3}, Lcom/twitter/library/media/widget/TweetMediaView;->setOnMediaClickListener(Lcom/twitter/library/media/widget/TweetMediaView$b;)V

    .line 39
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->p:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-static {v1, v1, v0, p3}, Lcom/twitter/android/highlights/z;->a(ZZLandroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 40
    iget-object v0, p1, Lcom/twitter/android/highlights/ai$a;->G:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-static {v1, v1, v0, p3}, Lcom/twitter/android/highlights/z;->a(ZZLandroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 23
    const v0, 0x7f0a03fd

    return v0
.end method
