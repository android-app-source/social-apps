.class public Lcom/twitter/android/highlights/ae$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/twitter/android/highlights/ae$b;

.field public b:J

.field private c:Z

.field private final d:Lcom/twitter/android/highlights/ae$c;


# direct methods
.method public constructor <init>(Lcom/twitter/android/highlights/ae;Landroid/content/Context;Lcom/twitter/android/highlights/q$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lcom/twitter/android/highlights/ae$c;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/highlights/ae$c;-><init>(Lcom/twitter/android/highlights/ae;Landroid/content/Context;Lcom/twitter/android/highlights/q$a;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    iget-object v1, p1, Lcom/twitter/android/highlights/ae;->s:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/twitter/android/highlights/ae$c;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 70
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    invoke-virtual {v0, v6}, Lcom/twitter/android/highlights/ae$c;->a(Z)V

    .line 71
    iput-boolean v6, p0, Lcom/twitter/android/highlights/ae$a;->c:Z

    .line 72
    new-instance v0, Lcom/twitter/android/highlights/ae$b;

    iget-object v1, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    invoke-direct {v0, v1}, Lcom/twitter/android/highlights/ae$b;-><init>(Lcom/twitter/android/highlights/ae$c;)V

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$a;->a:Lcom/twitter/android/highlights/ae$b;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/highlights/ae$a;)Lcom/twitter/android/highlights/ae$c;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 92
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/highlights/ae$a;->c:Z

    if-nez v0, :cond_1

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/highlights/ae$c;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/highlights/ae$a;->c:Z

    .line 96
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/android/highlights/ae;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    if-nez p1, :cond_1

    .line 82
    iget-object v2, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/highlights/ae$c;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 83
    iput-boolean v1, p0, Lcom/twitter/android/highlights/ae$a;->c:Z

    .line 88
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    iget-boolean v3, p0, Lcom/twitter/android/highlights/ae$a;->c:Z

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/twitter/android/highlights/ae$a;->b:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    :goto_1
    invoke-virtual {v2, v0}, Lcom/twitter/android/highlights/ae$c;->a(Z)V

    .line 89
    return-void

    .line 84
    :cond_1
    iget-boolean v2, p0, Lcom/twitter/android/highlights/ae$a;->c:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    invoke-virtual {v2}, Lcom/twitter/android/highlights/ae$c;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_0

    .line 85
    :cond_2
    iget-object v2, p0, Lcom/twitter/android/highlights/ae$a;->d:Lcom/twitter/android/highlights/ae$c;

    iget-object v3, p1, Lcom/twitter/android/highlights/ae;->s:Landroid/database/Cursor;

    invoke-virtual {v2, v3}, Lcom/twitter/android/highlights/ae$c;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 86
    iput-boolean v0, p0, Lcom/twitter/android/highlights/ae$a;->c:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 88
    goto :goto_1
.end method
