.class public Lcom/twitter/android/highlights/ae$c;
.super Landroid/widget/CursorAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/highlights/ae$c$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/highlights/ae;

.field private final b:Lcom/twitter/android/highlights/q$a;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/highlights/ae;Landroid/content/Context;Lcom/twitter/android/highlights/q$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 175
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 176
    iput-object p1, p0, Lcom/twitter/android/highlights/ae$c;->a:Lcom/twitter/android/highlights/ae;

    .line 177
    iput-object p3, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    .line 178
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$c;->c:Landroid/view/LayoutInflater;

    .line 179
    iput-object p4, p0, Lcom/twitter/android/highlights/ae$c;->d:Ljava/lang/String;

    .line 180
    iput-object p5, p0, Lcom/twitter/android/highlights/ae$c;->e:Ljava/lang/String;

    .line 181
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 287
    iput-boolean p1, p0, Lcom/twitter/android/highlights/ae$c;->f:Z

    .line 288
    invoke-virtual {p0}, Lcom/twitter/android/highlights/ae$c;->notifyDataSetChanged()V

    .line 289
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 233
    sget-object v0, Lbtd;->a:Lbtd;

    invoke-virtual {v0, p3}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v2

    .line 234
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/twitter/android/highlights/ae$d;

    .line 235
    iget-object v0, v8, Lcom/twitter/android/highlights/ae$d;->k:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/twitter/android/highlights/ae$d;->k:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->G:J

    iget-wide v6, v2, Lcom/twitter/model/core/Tweet;->G:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    :cond_0
    move v0, v1

    .line 236
    :goto_0
    if-nez v0, :cond_1

    iget-object v3, v8, Lcom/twitter/android/highlights/ae$d;->k:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2, v3}, Lcom/twitter/model/core/Tweet;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_1
    move v10, v1

    .line 237
    :goto_1
    if-eqz v0, :cond_7

    .line 239
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    iget-wide v4, v2, Lcom/twitter/model/core/Tweet;->s:J

    .line 240
    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v3, v2, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 241
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v3, v2, Lcom/twitter/model/core/Tweet;->l:I

    .line 242
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v3, v2, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    .line 243
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 245
    iget-object v3, v8, Lcom/twitter/android/highlights/ae$d;->b:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v3, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 246
    iget-object v3, v8, Lcom/twitter/android/highlights/ae$d;->b:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v3, v0}, Lcom/twitter/media/ui/image/UserImageView;->setTag(Ljava/lang/Object;)V

    .line 247
    iget-object v0, v8, Lcom/twitter/android/highlights/ae$d;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    invoke-static {p2, v2, v1, v3}, Lcom/twitter/android/highlights/aj;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;ZLcne;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    new-instance v0, Lcom/twitter/android/highlights/u;

    iget-object v3, v8, Lcom/twitter/android/highlights/ae$d;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v4, v8, Lcom/twitter/android/highlights/ae$d;->j:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/ae$c;->a:Lcom/twitter/android/highlights/ae;

    .line 249
    invoke-static {v1, v2}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/highlights/ae$c;->d:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/highlights/ae$c;->e:Ljava/lang/String;

    move-object v1, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/highlights/u;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    iget-object v1, v8, Lcom/twitter/android/highlights/ae$d;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 251
    iget-object v1, v8, Lcom/twitter/android/highlights/ae$d;->j:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 267
    :cond_2
    :goto_2
    if-eqz v10, :cond_4

    .line 268
    iget-object v0, v8, Lcom/twitter/android/highlights/ae$d;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v1, v8, Lcom/twitter/android/highlights/ae$d;->d:Landroid/view/View;

    iget-boolean v0, v2, Lcom/twitter/model/core/Tweet;->L:Z

    if-eqz v0, :cond_9

    move v0, v9

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 270
    iget-object v0, v8, Lcom/twitter/android/highlights/ae$d;->e:Landroid/widget/TextView;

    invoke-static {p2, v2}, Lcom/twitter/android/highlights/z;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, v8, Lcom/twitter/android/highlights/ae$d;->h:Lcom/twitter/library/media/widget/TweetMediaView;

    iget-object v1, v8, Lcom/twitter/android/highlights/ae$d;->g:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/highlights/af;->a(Lcom/twitter/library/media/widget/TweetMediaView;Landroid/view/View;Lcom/twitter/model/core/Tweet;)V

    .line 274
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    if-eqz v0, :cond_3

    .line 275
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    iget-object v1, p0, Lcom/twitter/android/highlights/ae$c;->a:Lcom/twitter/android/highlights/ae;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/highlights/q$a;->a(Lcom/twitter/android/highlights/ae;Lcom/twitter/model/core/Tweet;)V

    .line 277
    :cond_3
    iput-object v2, v8, Lcom/twitter/android/highlights/ae$d;->k:Lcom/twitter/model/core/Tweet;

    .line 279
    :cond_4
    return-void

    :cond_5
    move v0, v9

    .line 235
    goto/16 :goto_0

    :cond_6
    move v10, v9

    .line 236
    goto/16 :goto_1

    .line 252
    :cond_7
    if-eqz v10, :cond_2

    .line 255
    iget-object v0, v8, Lcom/twitter/android/highlights/ae$d;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/u;

    .line 256
    iget-boolean v1, v0, Lcom/twitter/android/highlights/u;->c:Z

    if-eqz v1, :cond_8

    .line 259
    iput-boolean v9, v0, Lcom/twitter/android/highlights/u;->c:Z

    goto :goto_2

    .line 261
    :cond_8
    invoke-virtual {v0, v2}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/model/core/Tweet;)V

    goto :goto_2

    .line 269
    :cond_9
    const/16 v0, 0x8

    goto :goto_3
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/twitter/android/highlights/ae$c;->f:Z

    if-eqz v0, :cond_0

    .line 186
    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 188
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/twitter/android/highlights/ae$c;->f:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 199
    const/4 v0, 0x1

    .line 201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 207
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/ae$c;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 208
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$c;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0402e6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 210
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x2

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 216
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$c;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f04015e

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 217
    new-instance v0, Lcom/twitter/android/highlights/ae$d;

    invoke-direct {v0, v1}, Lcom/twitter/android/highlights/ae$d;-><init>(Landroid/view/View;)V

    .line 218
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 219
    iget-object v2, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    iget-object v2, v0, Lcom/twitter/android/highlights/ae$d;->h:Lcom/twitter/library/media/widget/TweetMediaView;

    iget-object v3, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    invoke-virtual {v2, v3}, Lcom/twitter/library/media/widget/TweetMediaView;->setOnMediaClickListener(Lcom/twitter/library/media/widget/TweetMediaView$b;)V

    .line 221
    iget-object v2, v0, Lcom/twitter/android/highlights/ae$d;->b:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v3, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    invoke-virtual {v2, v3}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    iget-object v2, v0, Lcom/twitter/android/highlights/ae$d;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v3, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    invoke-static {v4, v4, v2, v3}, Lcom/twitter/android/highlights/z;->a(ZZLandroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 223
    iget-object v0, v0, Lcom/twitter/android/highlights/ae$d;->j:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v2, p0, Lcom/twitter/android/highlights/ae$c;->b:Lcom/twitter/android/highlights/q$a;

    invoke-static {v4, v4, v0, v2}, Lcom/twitter/android/highlights/z;->a(ZZLandroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 224
    const v0, 0x7f13043c

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 225
    if-eqz v0, :cond_0

    .line 226
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 228
    :cond_0
    return-object v1
.end method
