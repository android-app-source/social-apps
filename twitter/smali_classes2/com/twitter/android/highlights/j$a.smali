.class public Lcom/twitter/android/highlights/j$a;
.super Lcom/twitter/android/highlights/y;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/Button;

.field public final c:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 100
    const/16 v0, 0xd

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/highlights/y;-><init>(ILandroid/view/View;)V

    .line 101
    const v0, 0x7f1303f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/j$a;->a:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f1303f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/highlights/j$a;->b:Landroid/widget/Button;

    .line 103
    const v0, 0x7f130064

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/highlights/j$a;->c:Landroid/widget/ProgressBar;

    .line 104
    return-void
.end method
