.class public Lcom/twitter/android/highlights/u;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/highlights/u$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field public final b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field public c:Z

.field private final d:Landroid/content/Context;

.field private final e:Lcom/twitter/analytics/model/ScribeItem;

.field private final f:Lcom/twitter/android/highlights/u$a;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private i:Lcom/twitter/model/core/Tweet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/u;->d:Landroid/content/Context;

    .line 61
    iput-object p5, p0, Lcom/twitter/android/highlights/u;->e:Lcom/twitter/analytics/model/ScribeItem;

    .line 62
    iput-object p3, p0, Lcom/twitter/android/highlights/u;->a:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 63
    iput-object p4, p0, Lcom/twitter/android/highlights/u;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 64
    invoke-virtual {p0, p2}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/model/core/Tweet;)V

    .line 65
    new-instance v0, Lcom/twitter/android/highlights/u$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/highlights/u$a;-><init>(Lcom/twitter/android/highlights/u;)V

    iput-object v0, p0, Lcom/twitter/android/highlights/u;->f:Lcom/twitter/android/highlights/u$a;

    .line 66
    iput-object p6, p0, Lcom/twitter/android/highlights/u;->g:Ljava/lang/String;

    .line 67
    iput-object p7, p0, Lcom/twitter/android/highlights/u;->h:Ljava/lang/String;

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/highlights/u;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/highlights/u;Z)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/twitter/android/highlights/u;->a(Z)V

    return-void
.end method

.method private a(Lcom/twitter/library/widget/CompoundDrawableAnimButton;I)V
    .locals 4

    .prologue
    .line 125
    if-lez p2, :cond_0

    .line 126
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    int-to-long v2, p2

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 135
    iget-object v2, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget v3, v2, Lcom/twitter/model/core/Tweet;->k:I

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    add-int/2addr v0, v3

    iput v0, v2, Lcom/twitter/model/core/Tweet;->k:I

    .line 136
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    if-nez p1, :cond_1

    :goto_1
    iput-boolean v1, v0, Lcom/twitter/model/core/Tweet;->c:Z

    .line 137
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->k:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/widget/CompoundDrawableAnimButton;I)V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-boolean v1, v1, Lcom/twitter/model/core/Tweet;->c:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setChecked(Z)V

    .line 139
    return-void

    :cond_0
    move v0, v1

    .line 135
    goto :goto_0

    .line 136
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->f:Lcom/twitter/android/highlights/u$a;

    invoke-virtual {v0, p2}, Lcom/twitter/android/highlights/u$a;->a(Lcom/twitter/library/client/Session;)V

    .line 117
    new-instance v0, Lcom/twitter/android/bm$a;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, p1, v1}, Lcom/twitter/android/bm$a;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    const/16 v1, 0xc

    .line 118
    invoke-virtual {v0, v1}, Lcom/twitter/android/bm$a;->a(I)Lcom/twitter/android/bm$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->f:Lcom/twitter/android/highlights/u$a;

    .line 119
    invoke-virtual {v0, v1}, Lcom/twitter/android/bm$a;->a(Lcom/twitter/android/bm$b;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Lcom/twitter/android/bm$a;->a()Lcom/twitter/android/bm;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/twitter/android/bm;->a()V

    .line 122
    return-void
.end method

.method a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 160
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/highlights/u;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/highlights/u;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const/4 v2, 0x4

    aput-object p4, v1, v2

    .line 161
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->e:Lcom/twitter/analytics/model/ScribeItem;

    .line 162
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 163
    iget-object v1, p0, Lcom/twitter/android/highlights/u;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 164
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 165
    return-void
.end method

.method public a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 89
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 90
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/highlights/u;->a:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v2}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->toggle()V

    .line 94
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Z)V

    .line 95
    const-string/jumbo v3, "story"

    const-string/jumbo v4, "tweet"

    if-eqz v0, :cond_1

    const-string/jumbo v2, "favorite"

    :goto_1
    invoke-virtual {p0, p2, v3, v4, v2}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v3, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget v4, v3, Lcom/twitter/model/core/Tweet;->n:I

    if-eqz v0, :cond_2

    move v2, v1

    :goto_2
    add-int/2addr v2, v4

    iput v2, v3, Lcom/twitter/model/core/Tweet;->n:I

    .line 100
    iget-object v2, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iput-boolean v0, v2, Lcom/twitter/model/core/Tweet;->a:Z

    .line 101
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->a:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v2, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget v2, v2, Lcom/twitter/model/core/Tweet;->n:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/widget/CompoundDrawableAnimButton;I)V

    .line 102
    iput-boolean v1, p0, Lcom/twitter/android/highlights/u;->c:Z

    .line 103
    return-void

    .line 89
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 95
    :cond_1
    const-string/jumbo v2, "unfavorite"

    goto :goto_1

    .line 99
    :cond_2
    const/4 v2, -0x1

    goto :goto_2
.end method

.method protected a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Z)V
    .locals 8

    .prologue
    .line 145
    if-eqz p3, :cond_0

    .line 146
    new-instance v1, Lbfm;

    iget-object v2, p0, Lcom/twitter/android/highlights/u;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->G:J

    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v6, v0, Lcom/twitter/model/core/Tweet;->u:J

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lbfm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    .line 148
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfm;->a(Lcgi;)Lbfm;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    .line 149
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->m()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfm;->a(Ljava/lang/Boolean;)Lbfm;

    move-result-object v0

    .line 154
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 155
    return-void

    .line 151
    :cond_0
    new-instance v1, Lbfp;

    iget-object v2, p0, Lcom/twitter/android/highlights/u;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->G:J

    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-wide v6, v0, Lcom/twitter/model/core/Tweet;->u:J

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lbfp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    iget-object v0, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    .line 152
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfp;->a(Lcgi;)Lbfp;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 71
    iput-object p1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->a:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-boolean v1, v1, Lcom/twitter/model/core/Tweet;->a:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setChecked(Z)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->a:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->n:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/widget/CompoundDrawableAnimButton;I)V

    .line 74
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget-boolean v1, v1, Lcom/twitter/model/core/Tweet;->c:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setChecked(Z)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/highlights/u;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/u;->i:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->k:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/widget/CompoundDrawableAnimButton;I)V

    .line 76
    return-void
.end method
