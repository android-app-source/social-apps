.class public Lcom/twitter/android/highlights/p;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/highlights/p$a;
    }
.end annotation


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Lcom/twitter/database/model/f;",
            "Lcbi",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Lcom/twitter/database/model/f;",
            "Lcbi",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/highlights/p;->a:Lauj;

    .line 40
    return-void
.end method

.method public static a()Lcom/twitter/android/highlights/p;
    .locals 3

    .prologue
    .line 31
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 30
    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 32
    new-instance v1, Lauo;

    .line 33
    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    const-class v2, Laxq;

    invoke-interface {v0, v2}, Lcom/twitter/database/schema/TwitterSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxq;

    invoke-interface {v0}, Laxq;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/highlights/p$a;

    invoke-direct {v2}, Lcom/twitter/android/highlights/p$a;-><init>()V

    invoke-direct {v1, v0, v2}, Lauo;-><init>(Lcom/twitter/database/model/l;Lcbr;)V

    .line 35
    new-instance v0, Lcom/twitter/android/highlights/p;

    invoke-direct {v0, v1}, Lcom/twitter/android/highlights/p;-><init>(Lauj;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/highlights/p;->a:Lauj;

    new-instance v1, Lcom/twitter/database/model/f$a;

    invoke-direct {v1}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v2, "story_source_id"

    .line 49
    invoke-static {v2, p1}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    const-string/jumbo v2, "1"

    .line 50
    invoke-virtual {v1, v2}, Lcom/twitter/database/model/f$a;->d(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v1

    .line 48
    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/highlights/p$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/highlights/p$1;-><init>(Lcom/twitter/android/highlights/p;)V

    .line 52
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    const/4 v1, 0x0

    .line 58
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 48
    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
