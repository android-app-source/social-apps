.class public abstract Lcom/twitter/android/highlights/StoriesActivity;
.super Lcom/twitter/app/common/base/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Lcom/twitter/android/highlights/o$c;
.implements Lcom/twitter/android/highlights/q$a;
.implements Lcom/twitter/android/widget/highlights/StoriesViewPager$b;
.implements Lcom/twitter/android/widget/highlights/StoriesViewPager$f;
.implements Lcom/twitter/ui/widget/FullScreenFrameLayout$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/highlights/StoriesActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/BaseFragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Lcom/twitter/android/highlights/o$c;",
        "Lcom/twitter/android/highlights/q$a;",
        "Lcom/twitter/android/widget/highlights/StoriesViewPager$b;",
        "Lcom/twitter/android/widget/highlights/StoriesViewPager$f;",
        "Lcom/twitter/ui/widget/FullScreenFrameLayout$a;"
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/library/client/v;

.field protected b:Lcom/twitter/android/highlights/t;

.field protected c:Lcom/twitter/android/highlights/q;

.field protected d:Z

.field protected e:Z

.field protected f:Lcom/twitter/android/highlights/o;

.field protected g:Lcom/twitter/library/client/p;

.field protected h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

.field protected i:I

.field protected j:Landroid/view/View;

.field protected k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected l:I

.field protected m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/highlights/ae;",
            ">;"
        }
    .end annotation
.end field

.field protected o:Z

.field protected final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/highlights/ae;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation
.end field

.field protected q:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Landroid/view/ViewGroup;

.field private t:Z

.field private u:Lcom/twitter/android/highlights/x;

.field private v:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;-><init>()V

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->d:Z

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->i:I

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->n:Ljava/util/Set;

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->p:Ljava/util/Map;

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->t:Z

    return-void
.end method

.method private a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/ad;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 956
    invoke-virtual {p1}, Lcom/twitter/android/highlights/x;->a()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/android/highlights/StoriesActivity;->c(I)Ljava/lang/String;

    move-result-object v1

    .line 957
    if-eqz v1, :cond_1

    .line 958
    iget-object v0, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 960
    :goto_0
    const-string/jumbo v2, "story"

    const-string/jumbo v3, "open_link"

    invoke-virtual {p0, v2, v1, v3}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 962
    :goto_1
    return-object v0

    .line 958
    :cond_0
    iget-object v0, p2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    goto :goto_0

    .line 962
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/android/highlights/x;Lcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 624
    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    iget-object v4, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget v7, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    move-object v1, p0

    move-object v8, v5

    move-object v9, v5

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    .line 626
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 628
    invoke-static {p2}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 627
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 629
    return-void
.end method

.method private static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 990
    packed-switch p0, :pswitch_data_0

    .line 1010
    :pswitch_0
    const/4 v0, 0x0

    .line 1014
    :goto_0
    return-object v0

    .line 1001
    :pswitch_1
    const-string/jumbo v0, "tweet"

    goto :goto_0

    .line 1006
    :pswitch_2
    const-string/jumbo v0, "user"

    goto :goto_0

    .line 990
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected abstract a()I
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 968
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 969
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const/4 v2, 0x4

    aput-object p3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 968
    return-object v0
.end method

.method protected a(Landroid/view/View;)Lcom/twitter/android/highlights/x;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 632
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->getCurrentItem()I

    move-result v3

    .line 633
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v0, v3}, Lcom/twitter/android/highlights/q;->a(I)Lcom/twitter/android/highlights/x;

    move-result-object v2

    .line 634
    if-nez v2, :cond_0

    move-object v0, v1

    .line 645
    :goto_0
    return-object v0

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 639
    if-eqz v0, :cond_1

    .line 640
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/y;

    .line 641
    iget v0, v0, Lcom/twitter/android/highlights/y;->F:I

    if-eq v0, v3, :cond_1

    move-object v0, v1

    .line 642
    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 645
    goto :goto_0
.end method

.method public a(F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 855
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v0}, Lcom/twitter/android/highlights/q;->b()I

    move-result v0

    .line 856
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->a()I

    move-result v1

    if-ne v1, v3, :cond_1

    const/16 v1, 0x64

    if-eq v0, v1, :cond_0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_1

    .line 868
    :cond_0
    :goto_0
    return-void

    .line 860
    :cond_1
    iget v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->v:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 863
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->c()V

    .line 864
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    .line 865
    const-string/jumbo v0, "exit"

    invoke-virtual {p0, v4, v4, v0}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const-string/jumbo v1, "swipe"

    .line 866
    invoke-static {v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 865
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 376
    iget v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->i:I

    if-eq v0, p1, :cond_1

    .line 377
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->u:Lcom/twitter/android/highlights/x;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->u:Lcom/twitter/android/highlights/x;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/android/highlights/x;Z)V

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v0, p1}, Lcom/twitter/android/highlights/q;->a(I)Lcom/twitter/android/highlights/x;

    move-result-object v0

    .line 381
    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->u:Lcom/twitter/android/highlights/x;

    .line 382
    if-eqz v0, :cond_1

    .line 385
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/android/highlights/x;Z)V

    .line 386
    iput p1, p0, Lcom/twitter/android/highlights/StoriesActivity;->i:I

    .line 389
    :cond_1
    return-void
.end method

.method public a(IFI)V
    .locals 2

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->e:Z

    if-nez v0, :cond_0

    .line 363
    const-string/jumbo v0, "StoriesActivity"

    const-string/jumbo v1, "initializing ViewPager"

    invoke-static {v0, v1}, Lcqj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->a(I)V

    .line 369
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->e:Z

    .line 370
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, v1}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;)V

    .line 372
    :cond_0
    return-void
.end method

.method public a(IIII)V
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 354
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;J)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 911
    invoke-static {p2}, Lcom/twitter/library/client/BrowserDataSourceFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/client/BrowserDataSource;

    move-result-object v2

    iget-object v8, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v1, p1

    move-object v3, p3

    move-wide v4, p4

    move-object v7, v6

    move-object v9, v6

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 913
    return-void
.end method

.method abstract a(Landroid/content/Intent;JI)V
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/16 v0, 0x68

    const/4 v3, 0x0

    .line 1099
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 1102
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->a()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    .line 1103
    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->b()I

    move-result v1

    const/16 v2, 0x65

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    .line 1104
    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->b()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_1

    .line 1105
    :cond_0
    iput-boolean v3, p0, Lcom/twitter/android/highlights/StoriesActivity;->e:Z

    .line 1106
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->f:Lcom/twitter/android/highlights/o;

    invoke-virtual {v1}, Lcom/twitter/android/highlights/o;->a()V

    .line 1108
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->a()I

    move-result v1

    iget-boolean v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->d:Z

    if-eqz v2, :cond_2

    const/16 v0, 0x66

    :cond_2
    invoke-virtual {p0, p1, v1, v0}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/database/Cursor;II)V

    .line 1112
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->k()V

    .line 1120
    :goto_0
    return-void

    .line 1115
    :cond_3
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->d:Z

    if-eqz v2, :cond_4

    const/16 v0, 0x67

    :cond_4
    invoke-virtual {p0, v1, v3, v0}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/database/Cursor;II)V

    goto :goto_0
.end method

.method a(Landroid/database/Cursor;II)V
    .locals 1

    .prologue
    .line 1129
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->i:I

    .line 1130
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/highlights/q;->a(Landroid/database/Cursor;II)V

    .line 1131
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1052
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    .line 1053
    packed-switch v0, :pswitch_data_0

    .line 1064
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 1065
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->m:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1066
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->b:Lcom/twitter/android/highlights/t;

    invoke-virtual {v1, v0}, Lcom/twitter/android/highlights/t;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/ae$a;

    move-result-object v0

    .line 1067
    if-eqz v0, :cond_0

    .line 1068
    invoke-virtual {v0, p2}, Lcom/twitter/android/highlights/ae$a;->a(Landroid/database/Cursor;)V

    .line 1074
    :cond_0
    :goto_0
    return-void

    .line 1055
    :pswitch_0
    const-string/jumbo v0, "StoriesActivity"

    const-string/jumbo v1, "Story load finished"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    iget-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->o:Z

    if-eqz v0, :cond_1

    .line 1057
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->a()I

    move-result v1

    const/16 v2, 0x65

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/database/Cursor;II)V

    goto :goto_0

    .line 1059
    :cond_1
    invoke-virtual {p0, p2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 1053
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcax;)V
    .locals 0

    .prologue
    .line 711
    return-void
.end method

.method public a(Lcom/twitter/android/highlights/ae;Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->p:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 896
    if-nez v0, :cond_0

    .line 897
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 898
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->p:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 900
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 901
    return-void
.end method

.method public a(Lcom/twitter/android/highlights/h;Lcom/twitter/android/highlights/h$c;)V
    .locals 0

    .prologue
    .line 905
    return-void
.end method

.method a(Lcom/twitter/android/highlights/x;)V
    .locals 3

    .prologue
    .line 439
    const-string/jumbo v0, "story"

    const/4 v1, 0x0

    const-string/jumbo v2, "impression"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 440
    invoke-static {p1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    .line 439
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 441
    invoke-virtual {p1}, Lcom/twitter/android/highlights/x;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 476
    :goto_0
    :pswitch_0
    return-void

    .line 450
    :pswitch_1
    const-string/jumbo v0, "story"

    const-string/jumbo v1, "tweet"

    const-string/jumbo v2, "impression"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 451
    invoke-static {p1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    .line 450
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 458
    :pswitch_2
    check-cast p1, Lcom/twitter/android/highlights/ai;

    .line 459
    const-string/jumbo v0, "story"

    const-string/jumbo v1, "tweet"

    const-string/jumbo v2, "impression"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->b:Lcom/twitter/model/core/Tweet;

    .line 460
    invoke-static {p1, v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    .line 459
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 461
    const-string/jumbo v0, "story"

    const-string/jumbo v1, "tweet"

    const-string/jumbo v2, "impression"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ai;->a:Lcom/twitter/model/core/Tweet;

    .line 462
    invoke-static {p1, v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    .line 461
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 467
    :pswitch_3
    const-string/jumbo v0, "story"

    const-string/jumbo v1, "user"

    const-string/jumbo v2, "impression"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 468
    invoke-static {p1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    .line 467
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 441
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Lcom/twitter/android/highlights/x;Z)V
    .locals 7

    .prologue
    .line 404
    invoke-virtual {p1}, Lcom/twitter/android/highlights/x;->a()I

    move-result v0

    .line 406
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->q:Ljava/util/HashSet;

    iget-object v2, p1, Lcom/twitter/android/highlights/x;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p1, Lcom/twitter/android/highlights/x;->k:Z

    if-nez v1, :cond_0

    .line 408
    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    .line 409
    const-string/jumbo v1, "StoriesActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Logging impression and setting read state for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/android/highlights/x;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->g:Lcom/twitter/library/client/p;

    new-instance v2, Lbcn;

    .line 411
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    .line 412
    invoke-virtual {v4}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v4

    iget-object v5, p1, Lcom/twitter/android/highlights/x;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->a()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Lbcn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    .line 410
    invoke-virtual {v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 413
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/android/highlights/x;)V

    .line 417
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->q:Ljava/util/HashSet;

    iget-object v2, p1, Lcom/twitter/android/highlights/x;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_0
    sparse-switch v0, :sswitch_data_0

    .line 431
    :goto_0
    return-void

    .line 423
    :sswitch_0
    check-cast p1, Lcom/twitter/android/highlights/l;

    .line 424
    invoke-virtual {p1, p2}, Lcom/twitter/android/highlights/l;->a(Z)V

    goto :goto_0

    .line 420
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0xb -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcax;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 720
    invoke-virtual {p1}, Lcom/twitter/library/media/widget/TweetMediaView;->getId()I

    move-result v0

    const v2, 0x7f130404

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 721
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/library/media/widget/TweetMediaView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/Tweet;

    .line 722
    if-nez v0, :cond_0

    invoke-static {v2}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723
    :cond_0
    new-instance v0, Lcom/twitter/app/common/base/h;

    invoke-direct {v0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 724
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    const-class v1, Lcom/twitter/android/TweetActivity;

    .line 725
    invoke-virtual {v0, p0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "tw"

    .line 726
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 727
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 723
    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 743
    :goto_1
    return-void

    .line 720
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 729
    :cond_2
    new-instance v0, Lcom/twitter/model/core/ad$c;

    invoke-direct {v0}, Lcom/twitter/model/core/ad$c;-><init>()V

    invoke-virtual {p2}, Lcax;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$c;->e(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$c;

    invoke-virtual {v0}, Lcom/twitter/model/core/ad$c;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/ad;

    .line 730
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->b(Landroid/view/View;)Lcom/twitter/android/highlights/x;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_3

    .line 732
    invoke-direct {p0, v0, v3}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/ad;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 733
    if-eqz v1, :cond_3

    .line 734
    invoke-static {v0, v2}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 735
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 741
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;J)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcom/twitter/model/core/MediaEntity;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 748
    invoke-virtual {p1}, Lcom/twitter/library/media/widget/TweetMediaView;->getId()I

    move-result v0

    const v1, 0x7f130404

    if-ne v0, v1, :cond_0

    move v1, v2

    .line 749
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/library/media/widget/TweetMediaView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 750
    if-nez v0, :cond_1

    .line 751
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "A tweet is required. Be sure that the view is properly configured."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 753
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 754
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 755
    throw v0

    :cond_0
    move v1, v3

    .line 748
    goto :goto_0

    .line 760
    :cond_1
    if-nez v1, :cond_2

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v4, Lcom/twitter/model/core/MediaEntity$Type;->c:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v1, v4, :cond_4

    .line 761
    :cond_2
    new-instance v1, Lcom/twitter/app/common/base/h;

    invoke-direct {v1}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 762
    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TweetActivity;

    .line 763
    invoke-virtual {v1, p0, v2}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "tw"

    .line 764
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 765
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 766
    invoke-virtual {p0, v1}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 792
    :goto_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->b(Landroid/view/View;)Lcom/twitter/android/highlights/x;

    move-result-object v1

    .line 793
    if-eqz v1, :cond_3

    .line 794
    invoke-virtual {v1}, Lcom/twitter/android/highlights/x;->a()I

    move-result v2

    invoke-static {v2}, Lcom/twitter/android/highlights/StoriesActivity;->c(I)Ljava/lang/String;

    move-result-object v2

    .line 795
    if-eqz v2, :cond_3

    .line 796
    const-string/jumbo v3, "story"

    const-string/jumbo v4, "image_click"

    invoke-virtual {p0, v3, v2, v4}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    .line 797
    invoke-static {v1, v0}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    .line 796
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 800
    :cond_3
    return-void

    .line 767
    :cond_4
    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v4, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v1, v4, :cond_5

    .line 768
    new-instance v1, Lcom/twitter/android/av/ad;

    invoke-direct {v1}, Lcom/twitter/android/av/ad;-><init>()V

    .line 769
    invoke-virtual {v1, v0}, Lcom/twitter/android/av/ad;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/ab;

    move-result-object v1

    .line 770
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/ab;->d(Z)Lcom/twitter/library/av/ab;

    move-result-object v1

    .line 771
    invoke-virtual {v1, p0}, Lcom/twitter/library/av/ab;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 773
    :cond_5
    new-instance v1, Lcom/twitter/app/common/base/h;

    invoke-direct {v1}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 774
    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    const-class v2, Lcom/twitter/android/GalleryActivity;

    .line 775
    invoke-virtual {v1, p0, v2}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "media"

    sget-object v4, Lcom/twitter/model/core/MediaEntity;->a:Lcom/twitter/util/serialization/l;

    .line 777
    invoke-static {p2, v4}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v4

    .line 776
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "source_tweet_id"

    iget-wide v4, p2, Lcom/twitter/model/core/MediaEntity;->j:J

    .line 778
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "statusId"

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->G:J

    .line 780
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "show_tw"

    .line 781
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 782
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 785
    invoke-virtual {p1, p2}, Lcom/twitter/library/media/widget/TweetMediaView;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/ui/image/BaseMediaImageView;

    move-result-object v2

    .line 786
    if-eqz v2, :cond_6

    .line 787
    invoke-static {p0, v1, v2}, Lcom/twitter/android/GalleryActivity;->a(Landroid/app/Activity;Landroid/content/Intent;Lcom/twitter/media/ui/image/BaseMediaImageView;)V

    goto/16 :goto_1

    .line 789
    :cond_6
    invoke-virtual {p0, v1}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcom/twitter/model/media/EditableMedia;)V
    .locals 0

    .prologue
    .line 805
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 1040
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->a(Z)V

    .line 1041
    return-void
.end method

.method public a(Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 0

    .prologue
    .line 823
    return-void
.end method

.method public a(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 809
    invoke-virtual {p1}, Lcom/twitter/library/widget/ObservableScrollView;->getId()I

    move-result v0

    const v1, 0x7f13040c

    if-ne v0, v1, :cond_0

    .line 810
    invoke-virtual {p1}, Lcom/twitter/library/widget/ObservableScrollView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/y;

    .line 811
    invoke-virtual {p1}, Lcom/twitter/library/widget/ObservableScrollView;->getHeight()I

    move-result v1

    .line 812
    invoke-virtual {p1, v4}, Lcom/twitter/library/widget/ObservableScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 813
    add-int v3, p3, v1

    if-lt v3, v2, :cond_1

    .line 814
    iget-object v0, v0, Lcom/twitter/android/highlights/y;->E:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 815
    :cond_1
    add-int/2addr v1, p5

    if-lt v1, v2, :cond_0

    .line 816
    iget-object v0, v0, Lcom/twitter/android/highlights/y;->E:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/MediaEntity;)V
    .locals 0

    .prologue
    .line 715
    return-void
.end method

.method public a(Lcom/twitter/model/core/ad;)V
    .locals 6

    .prologue
    .line 690
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    iget v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->i:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/highlights/q;->a(I)Lcom/twitter/android/highlights/x;

    move-result-object v0

    .line 691
    if-eqz v0, :cond_0

    .line 692
    invoke-direct {p0, v0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/ad;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 693
    if-eqz v1, :cond_0

    .line 694
    invoke-static {v0}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 695
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 701
    :cond_0
    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;J)V

    .line 702
    return-void
.end method

.method public a(Lcom/twitter/model/core/b;)V
    .locals 3

    .prologue
    .line 666
    .line 667
    invoke-static {p0, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/b;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "source_association"

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 668
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 666
    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 669
    return-void
.end method

.method public a(Lcom/twitter/model/core/h;)V
    .locals 3

    .prologue
    .line 673
    .line 674
    invoke-static {p0, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/h;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "source_association"

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 675
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 673
    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 676
    return-void
.end method

.method public a(Lcom/twitter/model/core/q;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 658
    const-wide/16 v2, 0x0

    iget-object v4, p1, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    const/4 v7, -0x1

    move-object v1, p0

    move-object v6, v5

    move-object v8, v5

    move-object v9, v5

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 660
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 661
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 662
    return-void
.end method

.method public a(Lcom/twitter/model/geo/TwitterPlace;)V
    .locals 0

    .prologue
    .line 680
    return-void
.end method

.method protected a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 239
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/content/Intent;JI)V

    .line 242
    invoke-virtual {p0, v5}, Lcom/twitter/android/highlights/StoriesActivity;->setIntent(Landroid/content/Intent;)V

    .line 247
    iget-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->e:Z

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setEnabled(Z)V

    .line 250
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 251
    return-void
.end method

.method a(Z)V
    .locals 0

    .prologue
    .line 941
    iput-boolean p1, p0, Lcom/twitter/android/highlights/StoriesActivity;->d:Z

    .line 942
    return-void
.end method

.method public b(Landroid/view/View;)Lcom/twitter/android/highlights/x;
    .locals 2

    .prologue
    .line 945
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 946
    if-eqz v0, :cond_0

    .line 947
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/y;

    .line 948
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    iget v0, v0, Lcom/twitter/android/highlights/y;->F:I

    invoke-virtual {v1, v0}, Lcom/twitter/android/highlights/q;->a(I)Lcom/twitter/android/highlights/x;

    move-result-object v0

    .line 950
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 393
    return-void
.end method

.method public b(J)V
    .locals 0

    .prologue
    .line 654
    return-void
.end method

.method public b(Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 0

    .prologue
    .line 827
    return-void
.end method

.method public b(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
    .locals 2

    .prologue
    .line 832
    invoke-virtual {p1}, Lcom/twitter/library/widget/ObservableScrollView;->getId()I

    move-result v0

    const v1, 0x7f13040c

    if-ne v0, v1, :cond_0

    .line 834
    new-instance v0, Lcom/twitter/android/highlights/StoriesActivity$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/twitter/android/highlights/StoriesActivity$1;-><init>(Lcom/twitter/android/highlights/StoriesActivity;Lcom/twitter/library/widget/ObservableScrollView;I)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/ObservableScrollView;->post(Ljava/lang/Runnable;)Z

    .line 849
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/core/ad;)Z
    .locals 1

    .prologue
    .line 706
    const/4 v0, 0x1

    return v0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 889
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->finish()V

    .line 890
    return-void
.end method

.method public d(J)V
    .locals 0

    .prologue
    .line 650
    return-void
.end method

.method e()V
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->o:Z

    .line 480
    return-void
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 290
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->finish()V

    .line 291
    const/4 v0, 0x0

    const v1, 0x7f050032

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/highlights/StoriesActivity;->overridePendingTransition(II)V

    .line 292
    return-void
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 1044
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->a(Z)V

    .line 1045
    return-void
.end method

.method i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1018
    iput-boolean v3, p0, Lcom/twitter/android/highlights/StoriesActivity;->o:Z

    .line 1019
    new-instance v0, Laut;

    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 1020
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    sget-object v2, Lcom/twitter/database/schema/a$w;->a:Landroid/net/Uri;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 1021
    invoke-virtual {v0}, Laut;->a()V

    .line 1022
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 872
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    if-eqz v0, :cond_1

    .line 877
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 878
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->i:I

    .line 879
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->a(I)V

    .line 883
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setEnabled(Z)V

    .line 885
    :cond_1
    return-void
.end method

.method k()V
    .locals 3

    .prologue
    .line 1029
    const-string/jumbo v0, "story"

    const/4 v1, 0x0

    const-string/jumbo v2, "load_finished"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 1030
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v2}, Lcom/twitter/android/highlights/q;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1031
    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v2, v0}, Lcom/twitter/android/highlights/q;->a(I)Lcom/twitter/android/highlights/x;

    move-result-object v2

    .line 1032
    if-eqz v2, :cond_0

    .line 1033
    invoke-static {v2}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 1030
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1036
    :cond_1
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 1037
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v0}, Lcom/twitter/android/highlights/q;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 264
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onBackPressed()V

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->j:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 501
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/view/View;)Lcom/twitter/android/highlights/x;

    move-result-object v1

    .line 502
    if-nez v1, :cond_1

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 506
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 510
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 511
    const-string/jumbo v2, "story"

    const-string/jumbo v3, "user"

    const-string/jumbo v4, "name_click"

    invoke-virtual {p0, v2, v3, v4}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/android/highlights/x;Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    goto :goto_0

    .line 518
    :sswitch_1
    invoke-virtual {v1, p0}, Lcom/twitter/android/highlights/x;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 519
    const-string/jumbo v0, "story"

    const-string/jumbo v3, "launch"

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 520
    invoke-static {v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 519
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 525
    :sswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/u;

    .line 526
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->g:Lcom/twitter/library/client/p;

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 531
    :sswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/u;

    .line 532
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/highlights/u;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 536
    :sswitch_4
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/v;

    .line 537
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->g:Lcom/twitter/library/client/p;

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/highlights/v;->a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 542
    :sswitch_5
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 543
    const-string/jumbo v2, "story"

    const-string/jumbo v3, "user"

    const-string/jumbo v4, "image_click"

    invoke-virtual {p0, v2, v3, v4}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/android/highlights/x;Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    goto/16 :goto_0

    .line 549
    :sswitch_6
    check-cast p1, Lcom/twitter/media/ui/image/BaseMediaImageView;

    .line 550
    invoke-virtual {p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getImageRequest()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 551
    if-eqz v0, :cond_0

    .line 552
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 553
    new-instance v3, Lcom/twitter/app/common/base/h;

    invoke-direct {v3}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 554
    invoke-virtual {v3, v4}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v3

    const-class v4, Lcom/twitter/android/ImageActivity;

    .line 555
    invoke-virtual {v3, p0, v4}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    .line 556
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "image_url"

    .line 557
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 553
    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 558
    const-string/jumbo v0, "story"

    const-string/jumbo v3, "image_click"

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 559
    invoke-static {v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 558
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    :sswitch_7
    move-object v0, v1

    .line 564
    check-cast v0, Lcom/twitter/android/highlights/l;

    .line 565
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/highlights/y;

    invoke-virtual {v0, v1}, Lcom/twitter/android/highlights/l;->a(Lcom/twitter/android/highlights/y;)Z

    goto/16 :goto_0

    .line 569
    :sswitch_8
    check-cast v1, Lcom/twitter/android/highlights/l;

    .line 570
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/y;

    invoke-virtual {v1, v0, v4}, Lcom/twitter/android/highlights/l;->a(Lcom/twitter/android/highlights/y;Z)V

    goto/16 :goto_0

    .line 574
    :sswitch_9
    check-cast v1, Lcom/twitter/android/highlights/l;

    .line 575
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/y;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/highlights/l;->a(Lcom/twitter/android/highlights/y;Z)V

    goto/16 :goto_0

    .line 579
    :sswitch_a
    check-cast v1, Lcom/twitter/android/highlights/ak;

    .line 580
    new-instance v0, Lcom/twitter/app/common/base/h;

    invoke-direct {v0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 581
    invoke-virtual {v0, v4}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    const-class v2, Lcom/twitter/android/TweetActivity;

    .line 582
    invoke-virtual {v0, p0, v2}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "tw"

    iget-object v1, v1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    .line 583
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 580
    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_b
    move-object v6, v1

    .line 587
    check-cast v6, Lcom/twitter/android/highlights/ab;

    .line 595
    iget-boolean v0, v6, Lcom/twitter/android/highlights/ab;->r:Z

    if-eqz v0, :cond_2

    .line 597
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/highlights/ab;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    .line 603
    :goto_1
    const-string/jumbo v0, "story"

    const-string/jumbo v1, "open_link"

    invoke-virtual {p0, v0, v2, v1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 604
    invoke-static {v6}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, v6, Lcom/twitter/android/highlights/ab;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 603
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 600
    :cond_2
    new-instance v0, Lcom/twitter/model/core/ad$c;

    invoke-direct {v0}, Lcom/twitter/model/core/ad$c;-><init>()V

    iget-object v1, v6, Lcom/twitter/android/highlights/ab;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$c;->e(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$c;

    invoke-virtual {v0}, Lcom/twitter/model/core/ad$c;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/ad;

    .line 601
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;J)V

    goto :goto_1

    .line 608
    :sswitch_c
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/ae$d;

    .line 609
    new-instance v1, Lcom/twitter/app/common/base/h;

    invoke-direct {v1}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 610
    invoke-virtual {v1, v4}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TweetActivity;

    .line 611
    invoke-virtual {v1, p0, v2}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "tw"

    iget-object v0, v0, Lcom/twitter/android/highlights/ae$d;->k:Lcom/twitter/model/core/Tweet;

    .line 612
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 613
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 609
    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 506
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f130058 -> :sswitch_0
        0x7f13005a -> :sswitch_0
        0x7f130152 -> :sswitch_b
        0x7f1301d9 -> :sswitch_7
        0x7f1302ef -> :sswitch_5
        0x7f13035d -> :sswitch_6
        0x7f130402 -> :sswitch_8
        0x7f130403 -> :sswitch_9
        0x7f130404 -> :sswitch_a
        0x7f130408 -> :sswitch_3
        0x7f130409 -> :sswitch_2
        0x7f13040a -> :sswitch_4
        0x7f13040b -> :sswitch_1
        0x7f13040d -> :sswitch_6
        0x7f13040f -> :sswitch_0
        0x7f130410 -> :sswitch_6
        0x7f130412 -> :sswitch_5
        0x7f13041f -> :sswitch_3
        0x7f130420 -> :sswitch_2
        0x7f130421 -> :sswitch_c
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 296
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 300
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->t:Z

    .line 301
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x1

    .line 157
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 158
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    .line 159
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    const-string/jumbo v0, "StoriesActivity"

    const-string/jumbo v1, "No logged in user; falling back to log in."

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    .line 211
    :goto_0
    return-void

    .line 166
    :cond_0
    const-string/jumbo v1, "StoriesActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Active user: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x6

    .line 169
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 170
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 171
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->g:Lcom/twitter/library/client/p;

    .line 173
    if-eqz p1, :cond_1

    .line 174
    const-string/jumbo v0, "STATE_STORIES_VISITED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->q:Ljava/util/HashSet;

    .line 175
    const-string/jumbo v0, "STATE_NEXT_LOADER_ID"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->l:I

    .line 176
    const-string/jumbo v0, "STATE_AGGREGATE_STORY_IDS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->m:Ljava/util/HashMap;

    .line 184
    :goto_1
    const v0, 0x7f040143

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->setContentView(I)V

    .line 185
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 186
    const v0, 0x7f1100b2

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 187
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    const v0, 0x7f1303d7

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->s:Landroid/view/ViewGroup;

    .line 191
    const v0, 0x7f1303d5

    .line 192
    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/FullScreenFrameLayout;

    .line 193
    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/FullScreenFrameLayout;->setFitSystemWindowListener(Lcom/twitter/ui/widget/FullScreenFrameLayout$a;)V

    .line 196
    const v0, 0x7f100006

    invoke-virtual {v7, v0, v4, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->v:F

    .line 197
    const v0, 0x7f1303d8

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->j:Landroid/view/View;

    .line 198
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    const v0, 0x7f1303d9

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/StoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    .line 202
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setOffscreenPageLimit(I)V

    .line 203
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setOnPageChangeListener(Lcom/twitter/android/widget/highlights/StoriesViewPager$f;)V

    .line 204
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setEdgeListener(Lcom/twitter/android/widget/highlights/StoriesViewPager$b;)V

    .line 205
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 206
    new-instance v0, Lcom/twitter/android/highlights/t;

    invoke-direct {v0}, Lcom/twitter/android/highlights/t;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->b:Lcom/twitter/android/highlights/t;

    .line 207
    new-instance v0, Lcom/twitter/android/highlights/q;

    iget-object v2, p0, Lcom/twitter/android/highlights/StoriesActivity;->b:Lcom/twitter/android/highlights/t;

    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->c()Ljava/lang/String;

    move-result-object v5

    .line 208
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->a()I

    move-result v6

    move-object v1, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/highlights/q;-><init>(Landroid/content/Context;Lcom/twitter/android/highlights/t;Lcom/twitter/android/highlights/q$a;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    .line 209
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setAdapter(Lcom/twitter/android/highlights/q;)V

    .line 210
    new-instance v0, Lcom/twitter/android/highlights/o;

    invoke-direct {v0, v7, p0}, Lcom/twitter/android/highlights/o;-><init>(Landroid/content/res/Resources;Lcom/twitter/android/highlights/o$c;)V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->f:Lcom/twitter/android/highlights/o;

    goto/16 :goto_0

    .line 178
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->q:Ljava/util/HashSet;

    .line 179
    iput v5, p0, Lcom/twitter/android/highlights/StoriesActivity;->l:I

    .line 180
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->m:Ljava/util/HashMap;

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->b:Lcom/twitter/android/highlights/t;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->b:Lcom/twitter/android/highlights/t;

    invoke-virtual {v0}, Lcom/twitter/android/highlights/t;->a()V

    .line 284
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onDestroy()V

    .line 285
    return-void
.end method

.method public onGlobalLayout()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 306
    iget-boolean v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->t:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 307
    invoke-virtual {p0}, Lcom/twitter/android/highlights/StoriesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 308
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 309
    const v1, 0x7f0e0240

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 310
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v3

    .line 311
    const v0, 0x7f0e0271

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 317
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, v7}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v5

    .line 318
    int-to-float v0, v5

    mul-float/2addr v0, v3

    float-to-int v6, v0

    .line 321
    const v0, 0x7f0e0256

    .line 322
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    mul-int/lit8 v1, v0, -0x1

    .line 323
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v0, v6, v0

    add-int/2addr v0, v4

    .line 327
    if-le v0, v1, :cond_0

    .line 333
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v0, v1

    sub-int/2addr v0, v4

    .line 335
    int-to-float v0, v0

    div-float/2addr v0, v3

    float-to-int v0, v0

    .line 336
    sub-int v0, v5, v0

    div-int/lit8 v3, v0, 0x2

    .line 337
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    .line 338
    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 339
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 340
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 341
    iget-object v3, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v3, v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v1

    .line 345
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setPageMargin(I)V

    .line 346
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->f:Lcom/twitter/android/highlights/o;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v1, v6

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/twitter/android/highlights/o;->a:I

    .line 347
    iput-boolean v7, p0, Lcom/twitter/android/highlights/StoriesActivity;->t:Z

    .line 349
    :cond_1
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 89
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1078
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    .line 1079
    packed-switch v0, :pswitch_data_0

    .line 1086
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 1087
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->m:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1088
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->b:Lcom/twitter/android/highlights/t;

    invoke-virtual {v1, v0}, Lcom/twitter/android/highlights/t;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/ae$a;

    move-result-object v0

    .line 1089
    if-eqz v0, :cond_0

    .line 1090
    invoke-virtual {v0, v2}, Lcom/twitter/android/highlights/ae$a;->a(Lcom/twitter/android/highlights/ae;)V

    .line 1096
    :cond_0
    :goto_0
    return-void

    .line 1081
    :pswitch_0
    const/4 v0, 0x0

    const/16 v1, 0x65

    invoke-virtual {p0, v2, v0, v1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Landroid/database/Cursor;II)V

    goto :goto_0

    .line 1079
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 225
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onResume()V

    .line 228
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v1

    .line 230
    if-nez v1, :cond_0

    .line 231
    const-string/jumbo v0, "StoriesActivity"

    const-string/jumbo v1, "Current logged in user was removed; falling back to log in."

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    .line 236
    :goto_0
    return-void

    .line 235
    :cond_0
    invoke-virtual {v1}, Lakm;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/highlights/StoriesActivity;->a(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 272
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 273
    const-string/jumbo v0, "STATE_STORIES_VISITED"

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->q:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 274
    const-string/jumbo v0, "STATE_NEXT_LOADER_ID"

    iget v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    const-string/jumbo v0, "STATE_AGGREGATE_STORY_IDS"

    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity;->m:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 276
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 985
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 981
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 256
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 257
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onStop()V

    .line 258
    return-void
.end method
