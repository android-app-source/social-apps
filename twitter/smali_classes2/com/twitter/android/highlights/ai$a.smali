.class public Lcom/twitter/android/highlights/ai$a;
.super Lcom/twitter/android/highlights/y;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/ai;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final G:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field public final H:Lcom/twitter/library/media/widget/TweetMediaView;

.field public final a:Lcom/twitter/media/ui/image/UserImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/view/View;

.field public final g:Lcom/twitter/library/media/widget/TweetMediaView;

.field public final h:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field public final i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field public final j:Lcom/twitter/media/ui/image/UserImageView;

.field public final k:Landroid/widget/TextView;

.field public final l:Landroid/widget/TextView;

.field public final m:Landroid/widget/TextView;

.field public final n:Landroid/widget/TextView;

.field public final o:Landroid/view/View;

.field public final p:Lcom/twitter/library/widget/CompoundDrawableAnimButton;


# direct methods
.method public constructor <init>(ILandroid/view/View;)V
    .locals 5

    .prologue
    const v4, 0x7f130420

    const v3, 0x7f13041f

    .line 121
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/highlights/y;-><init>(ILandroid/view/View;)V

    .line 122
    const v0, 0x7f1302ef

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->a:Lcom/twitter/media/ui/image/UserImageView;

    .line 123
    const v0, 0x7f130058

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->b:Landroid/widget/TextView;

    .line 124
    const v0, 0x7f13040f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->c:Landroid/widget/TextView;

    .line 125
    const v0, 0x7f13041a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->d:Landroid/widget/TextView;

    .line 126
    const v0, 0x7f1303f2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->e:Landroid/widget/TextView;

    .line 127
    iget-object v0, p0, Lcom/twitter/android/highlights/ai$a;->e:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 128
    const v0, 0x7f1301d9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->f:Landroid/view/View;

    .line 129
    const v0, 0x7f130411

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/widget/TweetMediaView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->g:Lcom/twitter/library/media/widget/TweetMediaView;

    .line 130
    const v0, 0x7f13041b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 131
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->h:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 133
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 135
    const v0, 0x7f130412

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->j:Lcom/twitter/media/ui/image/UserImageView;

    .line 136
    const v0, 0x7f130413

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->k:Landroid/widget/TextView;

    .line 137
    const v0, 0x7f130414

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->l:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f130415

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->m:Landroid/widget/TextView;

    .line 139
    const v0, 0x7f130419

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->n:Landroid/widget/TextView;

    .line 140
    iget-object v0, p0, Lcom/twitter/android/highlights/ai$a;->n:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 141
    const v0, 0x7f130417

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->o:Landroid/view/View;

    .line 142
    const v0, 0x7f130418

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/widget/TweetMediaView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->H:Lcom/twitter/library/media/widget/TweetMediaView;

    .line 143
    const v0, 0x7f130416

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 144
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->p:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 146
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iput-object v0, p0, Lcom/twitter/android/highlights/ai$a;->G:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 150
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f11000e

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 151
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1, v0}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 152
    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1, v0}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 153
    return-void
.end method
