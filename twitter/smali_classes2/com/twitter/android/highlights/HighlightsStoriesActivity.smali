.class public Lcom/twitter/android/highlights/HighlightsStoriesActivity;
.super Lcom/twitter/android/highlights/StoriesActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/highlights/HighlightsStoriesActivity$a;,
        Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;
    }
.end annotation


# instance fields
.field private A:I

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Lcom/twitter/android/highlights/p;

.field private final s:Lcom/twitter/android/client/m;

.field private t:Lcom/twitter/util/a;

.field private u:Lcom/twitter/library/provider/t;

.field private v:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private w:Z

.field private x:Ljava/lang/String;

.field private y:Landroid/view/ViewStub;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/twitter/android/highlights/StoriesActivity;-><init>()V

    .line 106
    new-instance v0, Lcom/twitter/android/highlights/f;

    invoke-direct {v0}, Lcom/twitter/android/highlights/f;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->s:Lcom/twitter/android/client/m;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/highlights/HighlightsStoriesActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 360
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 361
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 362
    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "highlights_last_user_view_time"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 363
    new-instance v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 364
    invoke-virtual {v0, v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a(Z)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    move-result-object v0

    .line 365
    invoke-virtual {v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a()Landroid/content/Intent;

    move-result-object v0

    .line 363
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 366
    return-void
.end method

.method public static a(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    .line 348
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 350
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 351
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 352
    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "highlights_last_user_view_time"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 354
    :cond_0
    new-instance v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;-><init>(Landroid/content/Context;)V

    .line 355
    invoke-virtual {v0, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a(I)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    move-result-object v0

    .line 356
    invoke-virtual {v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a()Landroid/content/Intent;

    move-result-object v0

    .line 354
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 357
    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 484
    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 485
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/StatusBarNotif;

    move-result-object v0

    .line 486
    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 492
    :cond_0
    const-string/jumbo v0, "EXTRA_HIGHLIGHTS_SCRIBE_LOG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 493
    if-eqz v0, :cond_1

    .line 494
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 500
    :cond_1
    const-string/jumbo v0, "sb_notification"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 501
    const-string/jumbo v0, "EXTRA_HIGHLIGHTS_SCRIBE_LOG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 503
    :cond_2
    return-void
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 690
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->getCurrentItem()I

    move-result v0

    iget v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->A:I

    if-ge v0, v2, :cond_0

    .line 691
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->y:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 692
    const v0, 0x7f130401

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 693
    const v0, 0x7f130400

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 694
    const v0, 0x7f1303ff

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a03f6

    .line 696
    invoke-virtual {p0, v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    .line 695
    invoke-static {v2, v3}, Lcom/twitter/util/y;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 694
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 697
    iput-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->z:Z

    move v0, v1

    .line 700
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method a(Landroid/content/Intent;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 369
    .line 370
    iget-object v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->t:Lcom/twitter/util/a;

    invoke-static {v2}, Lcom/twitter/android/highlights/e;->a(Lcom/twitter/util/a;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 372
    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->D:Z

    move v2, v3

    .line 376
    :goto_0
    if-eqz p1, :cond_5

    .line 377
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 378
    const-string/jumbo v5, "EXTRA_HIGHLIGHTS_FORCE_STATE"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 379
    const-string/jumbo v2, "EXTRA_HIGHLIGHTS_FORCE_STATE"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 380
    if-ne v2, v3, :cond_0

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->D:Z

    move v0, v2

    .line 392
    :goto_2
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->b(Landroid/content/Intent;)V

    .line 394
    const-string/jumbo v2, "EXTRA_HIGHLIGHTS_SAMPLE_STORIES"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->B:Z

    .line 399
    const-string/jumbo v1, "impression"

    invoke-virtual {p0, v6, v6, v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 401
    :goto_3
    return v0

    :cond_0
    move v0, v1

    .line 380
    goto :goto_1

    .line 381
    :cond_1
    if-eqz v4, :cond_4

    .line 382
    const-string/jumbo v5, "promptbird"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    move v0, v3

    .line 383
    goto :goto_2

    .line 384
    :cond_2
    const-string/jumbo v3, "true"

    const-string/jumbo v5, "allow_optout"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 385
    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->C:Z

    move v0, v2

    goto :goto_2

    .line 386
    :cond_3
    const-string/jumbo v0, "true"

    const-string/jumbo v3, "ignore_nux"

    invoke-virtual {v4, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 387
    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v2, v1

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->i:I

    if-eq v0, p1, :cond_0

    .line 235
    iget-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->C:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->A:I

    if-lt p1, v0, :cond_0

    .line 236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->C:Z

    .line 239
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->a(I)V

    .line 240
    return-void
.end method

.method a(Landroid/content/Intent;JI)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 519
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->w:Z

    if-eqz v1, :cond_1

    const/4 v4, 0x1

    .line 520
    :goto_0
    if-eqz v4, :cond_0

    .line 524
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f()V

    .line 526
    :cond_0
    packed-switch p4, :pswitch_data_0

    .line 570
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid override state specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v4, v0

    .line 519
    goto :goto_0

    .line 528
    :pswitch_1
    const/4 v1, 0x0

    const/16 v2, 0x67

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/database/Cursor;II)V

    .line 573
    :goto_1
    return-void

    .line 534
    :pswitch_2
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 535
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 539
    :goto_2
    new-instance v1, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;

    move-object v2, p0

    move v3, p4

    move-object v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;-><init>(Lcom/twitter/android/highlights/HighlightsStoriesActivity;IZLandroid/content/Intent;J)V

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    goto :goto_1

    .line 537
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->E:Lcom/twitter/android/highlights/p;

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/highlights/p;->a(Ljava/lang/String;)Lrx/g;

    move-result-object v0

    goto :goto_2

    .line 526
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/highlights/h;Lcom/twitter/android/highlights/h$c;)V
    .locals 2

    .prologue
    .line 776
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/android/highlights/h;->a(I)V

    .line 777
    invoke-static {p1, p2, p0, p0}, Lcom/twitter/android/highlights/g;->a(Lcom/twitter/android/highlights/h;Lcom/twitter/android/highlights/h$c;Landroid/content/Context;Lcom/twitter/android/highlights/h$a;)V

    .line 778
    iget-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->D:Z

    if-eqz v0, :cond_0

    .line 779
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, v1}, Lcom/twitter/android/highlights/o;->b(Lcom/twitter/android/widget/highlights/StoriesViewPager;)V

    .line 781
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/android/highlights/x;Z)V
    .locals 2

    .prologue
    .line 706
    invoke-super {p0, p1, p2}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/android/highlights/x;Z)V

    .line 707
    invoke-virtual {p1}, Lcom/twitter/android/highlights/x;->a()I

    move-result v0

    .line 708
    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->c:Lcom/twitter/android/highlights/q;

    .line 709
    invoke-virtual {v0}, Lcom/twitter/android/highlights/q;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 710
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->t:Lcom/twitter/util/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/highlights/e;->a(Lcom/twitter/util/a;Z)V

    .line 712
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 785
    invoke-super {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/library/service/s;)V

    .line 786
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 787
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 788
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 791
    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "highlights_last_user_view_time"

    .line 792
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 793
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 794
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 271
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->e()V

    .line 272
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->g:Lcom/twitter/library/client/p;

    new-instance v1, Lbco;

    iget-object v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    .line 273
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lbco;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 274
    invoke-virtual {v1, p1}, Lbco;->a(Ljava/lang/String;)Lbco;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->B:Z

    .line 275
    invoke-virtual {v1, v2}, Lbco;->a(Z)Lbco;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/highlights/StoriesActivity$a;

    invoke-direct {v2, p0}, Lcom/twitter/android/highlights/StoriesActivity$a;-><init>(Lcom/twitter/android/highlights/StoriesActivity;)V

    .line 272
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 277
    return-void
.end method

.method protected a(Ljava/lang/String;J)V
    .locals 10

    .prologue
    .line 407
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->s:Lcom/twitter/android/client/m;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/m;)V

    .line 410
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 411
    invoke-virtual {p0, v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/content/Intent;)I

    move-result v2

    .line 412
    if-eqz v1, :cond_2

    const-string/jumbo v0, "EXTRA_HIGHLIGHTS_FORCE_STATE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    if-ne v2, v0, :cond_2

    const/4 v0, 0x1

    .line 416
    :goto_0
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/twitter/android/client/l;->a(J)V

    .line 417
    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->t:Lcom/twitter/util/a;

    const-string/jumbo v4, "highlights_last_user_view_time"

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v4, v6, v7}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 420
    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->t:Lcom/twitter/util/a;

    invoke-virtual {v3}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v3

    const-string/jumbo v6, "highlights_last_user_view_time"

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/util/a$a;->apply()V

    .line 421
    invoke-virtual {p0, v1, v4, v5, v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/content/Intent;JI)V

    .line 424
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->setIntent(Landroid/content/Intent;)V

    .line 429
    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->e:Z

    if-eqz v1, :cond_0

    .line 430
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setEnabled(Z)V

    .line 433
    :cond_0
    if-nez v0, :cond_1

    .line 434
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 436
    :cond_1
    return-void

    .line 412
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    const-string/jumbo v0, "highlights"

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 244
    invoke-super {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->b(I)V

    .line 247
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->D:Z

    .line 250
    :cond_0
    return-void
.end method

.method b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 439
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 440
    const-string/jumbo v1, "EXTRA_HIGHLIGHTS_TAPPED_STORY_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->w:Z

    .line 441
    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->w:Z

    if-eqz v1, :cond_0

    .line 442
    const-string/jumbo v0, "EXTRA_HIGHLIGHTS_TAPPED_STORY_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    .line 449
    :goto_0
    return-void

    .line 443
    :cond_0
    if-eqz v0, :cond_1

    .line 444
    invoke-static {v0}, Lcom/twitter/android/highlights/r;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    goto :goto_0

    .line 447
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    goto :goto_0
.end method

.method b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 467
    iput-boolean v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->d:Z

    .line 468
    const-string/jumbo v0, "InvalidStoryId"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    const-string/jumbo v0, "HighlightsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Activity started from notification, will move "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to front!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    new-instance v0, Lbcm;

    .line 471
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lbcm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 473
    new-instance v1, Lcom/twitter/android/highlights/HighlightsStoriesActivity$a;

    invoke-direct {v1, p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$a;-><init>(Lcom/twitter/android/highlights/StoriesActivity;)V

    invoke-virtual {v0, v1}, Lbcm;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 474
    iput-boolean v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->o:Z

    .line 475
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->g:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 477
    :cond_0
    return-void
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    const-string/jumbo v0, "storystream"

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->z:Z

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/highlights/StoriesViewPager;->setVisibility(I)V

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->d()V

    goto :goto_0
.end method

.method e()V
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;)V

    .line 268
    return-void
.end method

.method f()V
    .locals 4

    .prologue
    .line 591
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 592
    new-instance v1, Lbfr;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, p0, v0, v2, v3}, Lbfr;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;I)V

    const-string/jumbo v0, "push_foreground"

    .line 593
    invoke-virtual {v1, v0}, Lbfr;->c(Ljava/lang/String;)Lbge;

    move-result-object v0

    .line 594
    invoke-static {}, Lcom/twitter/android/av/m;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lbge;->e(I)Lbge;

    move-result-object v0

    .line 595
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->g:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 596
    return-void
.end method

.method protected h()V
    .locals 4

    .prologue
    .line 798
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->h()V

    .line 799
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 800
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 804
    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "highlights_last_user_view_time"

    .line 805
    invoke-virtual {v0, v1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 806
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 807
    return-void
.end method

.method i()V
    .locals 1

    .prologue
    .line 811
    iget-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->d:Z

    if-eqz v0, :cond_0

    .line 813
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    .line 815
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->i()V

    .line 816
    return-void
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 190
    .line 191
    const-string/jumbo v0, "exit"

    .line 192
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 194
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->onBackPressed()V

    move-object v1, v2

    .line 211
    :goto_0
    invoke-virtual {p0, v2, v1, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const-string/jumbo v1, "back_button"

    .line 212
    invoke-static {v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 211
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 213
    return-void

    .line 195
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->C:Z

    if-eqz v1, :cond_3

    .line 196
    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->z:Z

    if-eqz v1, :cond_1

    .line 197
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->onBackPressed()V

    .line 198
    const-string/jumbo v1, "opt_out_prompt"

    .line 199
    const-string/jumbo v0, "dismiss"

    goto :goto_0

    .line 200
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 202
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, v1, v2, v5}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    .line 203
    const-string/jumbo v1, "opt_out_prompt"

    .line 204
    const-string/jumbo v0, "impression"

    goto :goto_0

    .line 206
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iget-object v4, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->j:Landroid/view/View;

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    move-object v1, v2

    goto :goto_0

    .line 209
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iget-object v4, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->j:Landroid/view/View;

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    move-object v1, v2

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 610
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 651
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/view/View;)Lcom/twitter/android/highlights/x;

    move-result-object v0

    .line 652
    if-nez v0, :cond_4

    .line 684
    :goto_0
    return-void

    .line 614
    :sswitch_0
    const-string/jumbo v0, "exit"

    .line 615
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->c:Lcom/twitter/android/highlights/q;

    invoke-virtual {v1}, Lcom/twitter/android/highlights/q;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 617
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->finish()V

    move-object v1, v2

    .line 634
    :goto_1
    invoke-virtual {p0, v2, v1, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const-string/jumbo v1, "x_button"

    .line 635
    invoke-static {v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 634
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 618
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->C:Z

    if-eqz v1, :cond_3

    .line 619
    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->z:Z

    if-eqz v1, :cond_1

    .line 620
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->finish()V

    .line 621
    const-string/jumbo v1, "opt_out_prompt"

    .line 622
    const-string/jumbo v0, "dismiss"

    goto :goto_1

    .line 623
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 625
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    invoke-virtual {v0, v1, v2, v6}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    .line 626
    const-string/jumbo v1, "opt_out_prompt"

    .line 627
    const-string/jumbo v0, "impression"

    goto :goto_1

    .line 629
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iget-object v4, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->j:Landroid/view/View;

    invoke-virtual {v1, v3, v4, v6}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    move-object v1, v2

    goto :goto_1

    .line 632
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->f:Lcom/twitter/android/highlights/o;

    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->h:Lcom/twitter/android/widget/highlights/StoriesViewPager;

    iget-object v4, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->j:Landroid/view/View;

    invoke-virtual {v1, v3, v4, v6}, Lcom/twitter/android/highlights/o;->a(Lcom/twitter/android/widget/highlights/StoriesViewPager;Landroid/view/View;Z)V

    move-object v1, v2

    goto :goto_1

    .line 639
    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 640
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "highlights"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string/jumbo v5, "storystream"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v2, v3, v4

    const/4 v2, 0x3

    const-string/jumbo v4, "opt_out_prompt"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string/jumbo v4, "opt_out"

    aput-object v4, v3, v2

    .line 641
    invoke-static {v3}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 640
    invoke-static {p0, v0, v1, v6, v2}, Lcom/twitter/android/highlights/e;->a(Landroid/content/Context;JZLjava/lang/String;)Z

    .line 643
    const v0, 0x7f0a03fc

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 644
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->finish()V

    goto/16 :goto_0

    .line 656
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 681
    invoke-super {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->onClick(Landroid/view/View;)V

    goto/16 :goto_0

    .line 658
    :pswitch_0
    check-cast v0, Lcom/twitter/android/highlights/b;

    .line 659
    iget v0, v0, Lcom/twitter/android/highlights/b;->a:I

    packed-switch v0, :pswitch_data_1

    .line 677
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v6, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_0

    .line 662
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 667
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/people/PeopleDiscoveryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 610
    :sswitch_data_0
    .sparse-switch
        0x7f1303d8 -> :sswitch_0
        0x7f130400 -> :sswitch_0
        0x7f130401 -> :sswitch_1
    .end sparse-switch

    .line 656
    :pswitch_data_0
    .packed-switch 0x7f1303df
        :pswitch_0
    .end packed-switch

    .line 659
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 130
    invoke-super {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->onCreate(Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 135
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    iput-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->t:Lcom/twitter/util/a;

    .line 136
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->u:Lcom/twitter/library/provider/t;

    .line 137
    if-eqz p1, :cond_1

    .line 138
    const-string/jumbo v0, "STATE_SEARCH_IDS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->v:Ljava/util/HashSet;

    .line 139
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->u:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->v:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;)V

    .line 140
    const-string/jumbo v0, "STATE_SHOULD_NUDGE_USER_IN_INTRO"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->D:Z

    .line 145
    :goto_1
    const v0, 0x7f1303d6

    invoke-virtual {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->y:Landroid/view/ViewStub;

    .line 146
    const-string/jumbo v0, "highlights_opt_out_after_story_count"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->A:I

    .line 150
    invoke-static {}, Lcom/twitter/android/highlights/p;->a()Lcom/twitter/android/highlights/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->E:Lcom/twitter/android/highlights/p;

    .line 154
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->c(Landroid/content/Intent;)V

    goto :goto_0

    .line 142
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->v:Ljava/util/HashSet;

    .line 143
    iput-boolean v4, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->D:Z

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 913
    packed-switch p1, :pswitch_data_0

    .line 922
    const/16 v0, 0x64

    if-lt p1, v0, :cond_0

    .line 923
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 924
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->b:Lcom/twitter/android/highlights/t;

    invoke-virtual {v1, v0}, Lcom/twitter/android/highlights/t;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/ae$a;

    move-result-object v1

    .line 925
    if-eqz v1, :cond_0

    .line 926
    sget-object v2, Lcom/twitter/database/schema/a$w;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 927
    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    .line 928
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 927
    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 929
    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v3, "search_id"

    iget-wide v4, v1, Lcom/twitter/android/highlights/ae$a;->b:J

    .line 931
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 930
    invoke-virtual {v0, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 932
    new-instance v3, Lcom/twitter/util/android/d;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    move-object v4, p0

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-direct/range {v3 .. v9}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 935
    :goto_0
    return-object v0

    .line 915
    :pswitch_0
    sget-object v0, Lcom/twitter/database/schema/a$w;->a:Landroid/net/Uri;

    .line 916
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    .line 917
    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 916
    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 918
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lbuc;->a:[Ljava/lang/String;

    const-string/jumbo v4, "story_tag=?"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 919
    invoke-virtual {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move-object v0, v6

    .line 935
    goto :goto_0

    .line 913
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->u:Lcom/twitter/library/provider/t;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->u:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->v:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/t;->b(Ljava/util/Collection;)V

    .line 229
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->onDestroy()V

    .line 230
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 181
    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->setIntent(Landroid/content/Intent;)V

    .line 182
    invoke-direct {p0, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->c(Landroid/content/Intent;)V

    .line 183
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 454
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->onPause()V

    .line 455
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->s:Lcom/twitter/android/client/m;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/l;->b(Lcom/twitter/android/client/m;)V

    .line 456
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 218
    const-string/jumbo v0, "STATE_SEARCH_IDS"

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->v:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 219
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 19

    .prologue
    .line 717
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AbsListView;->getId()I

    move-result v2

    const v3, 0x7f130088

    if-ne v2, v3, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 772
    :cond_0
    :goto_0
    return-void

    .line 726
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AbsListView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/highlights/ab$a;

    .line 727
    if-nez p2, :cond_3

    .line 728
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->c:Lcom/twitter/android/highlights/q;

    iget v4, v2, Lcom/twitter/android/highlights/ab$a;->F:I

    .line 729
    invoke-virtual {v3, v4}, Lcom/twitter/android/highlights/q;->a(I)Lcom/twitter/android/highlights/x;

    move-result-object v3

    move-object v15, v3

    check-cast v15, Lcom/twitter/android/highlights/ab;

    .line 730
    if-eqz v15, :cond_3

    .line 731
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->b:Lcom/twitter/android/highlights/t;

    iget-object v4, v15, Lcom/twitter/android/highlights/ab;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/android/highlights/t;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/ae$a;

    move-result-object v16

    .line 732
    if-eqz v16, :cond_2

    move-object/from16 v0, v16

    iget-wide v4, v0, Lcom/twitter/android/highlights/ae$a;->b:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, v15, Lcom/twitter/android/highlights/ab;->b:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 734
    sget-object v3, Lcom/twitter/util/y;->a:Ljava/security/SecureRandom;

    invoke-virtual {v3}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v4

    move-object/from16 v0, v16

    iput-wide v4, v0, Lcom/twitter/android/highlights/ae$a;->b:J

    .line 735
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->v:Ljava/util/HashSet;

    move-object/from16 v0, v16

    iget-wide v4, v0, Lcom/twitter/android/highlights/ae$a;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 736
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->l:I

    move/from16 v17, v0

    add-int/lit8 v3, v17, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->l:I

    .line 737
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->m:Ljava/util/HashMap;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, v15, Lcom/twitter/android/highlights/ab;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 740
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->u:Lcom/twitter/library/provider/t;

    move-object/from16 v0, v16

    iget-wide v4, v0, Lcom/twitter/android/highlights/ae$a;->b:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/provider/t;->m(J)V

    .line 743
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->g:Lcom/twitter/library/client/p;

    move-object/from16 v18, v0

    new-instance v3, Lcom/twitter/library/api/search/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a:Lcom/twitter/library/client/v;

    .line 744
    invoke-virtual {v4}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v5

    move-object/from16 v0, v16

    iget-wide v6, v0, Lcom/twitter/android/highlights/ae$a;->b:J

    iget-object v8, v15, Lcom/twitter/android/highlights/ab;->b:Ljava/lang/String;

    const/4 v9, 0x0

    const-string/jumbo v10, "timeline"

    iget-object v11, v15, Lcom/twitter/android/highlights/ab;->b:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v14}, Lcom/twitter/library/api/search/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 750
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/twitter/library/api/search/d;->a(IZZZ)Lcom/twitter/library/api/search/d;

    move-result-object v3

    const/16 v4, 0xa

    .line 751
    invoke-virtual {v3, v4}, Lcom/twitter/library/api/search/d;->a(I)Lcom/twitter/library/api/search/d;

    move-result-object v3

    const/4 v4, 0x0

    .line 752
    invoke-virtual {v3, v4}, Lcom/twitter/library/api/search/d;->a(Z)Lcom/twitter/library/api/search/d;

    move-result-object v3

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/twitter/android/highlights/ae$a;->a:Lcom/twitter/android/highlights/ae$b;

    .line 743
    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 754
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v3, v0, v4, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 758
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->n:Ljava/util/Set;

    invoke-interface {v3, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 761
    :cond_3
    add-int v3, p2, p3

    move/from16 v0, p4

    if-ne v3, v0, :cond_0

    .line 763
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 764
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Landroid/widget/AbsListView;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 765
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AbsListView;->getPaddingBottom()I

    move-result v4

    .line 766
    if-le v3, v4, :cond_4

    .line 767
    iget-object v2, v2, Lcom/twitter/android/highlights/ab$a;->E:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 768
    :cond_4
    if-ne v3, v4, :cond_0

    .line 769
    iget-object v2, v2, Lcom/twitter/android/highlights/ab$a;->E:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected onStop()V
    .locals 7

    .prologue
    .line 944
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/ae;

    .line 945
    const-string/jumbo v2, "story"

    const/4 v3, 0x0

    const-string/jumbo v4, "scroll"

    invoke-virtual {p0, v2, v3, v4}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    .line 946
    invoke-static {v0}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 945
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 950
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 951
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/highlights/ae;

    .line 952
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 953
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 954
    const-string/jumbo v4, "story"

    const-string/jumbo v5, "tweet"

    const-string/jumbo v6, "impression"

    invoke-virtual {p0, v4, v5, v6}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v4

    .line 955
    invoke-static {v1, v0}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 954
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    .line 958
    :cond_2
    invoke-super {p0}, Lcom/twitter/android/highlights/StoriesActivity;->onStop()V

    .line 959
    return-void
.end method
