.class Lcom/twitter/android/highlights/u$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/bm$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/highlights/u;

.field private b:Lcom/twitter/library/client/Session;


# direct methods
.method constructor <init>(Lcom/twitter/android/highlights/u;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/twitter/android/highlights/u$a;->a:Lcom/twitter/android/highlights/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(JLcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 191
    iget-object v1, p0, Lcom/twitter/android/highlights/u$a;->b:Lcom/twitter/library/client/Session;

    const-string/jumbo v2, "story"

    const-string/jumbo v3, "tweet"

    if-eqz p4, :cond_0

    const-string/jumbo v0, "unretweet"

    :goto_0
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/twitter/android/highlights/u$a;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/twitter/android/highlights/u$a;->a:Lcom/twitter/android/highlights/u;

    invoke-static {v0, p4}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/android/highlights/u;Z)V

    .line 193
    iget-object v0, p0, Lcom/twitter/android/highlights/u$a;->a:Lcom/twitter/android/highlights/u;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/android/highlights/u;->c:Z

    .line 194
    return-void

    .line 191
    :cond_0
    const-string/jumbo v0, "retweet"

    goto :goto_0
.end method

.method public a(JZZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 199
    if-nez p3, :cond_0

    if-nez p5, :cond_0

    .line 200
    iget-object v3, p0, Lcom/twitter/android/highlights/u$a;->a:Lcom/twitter/android/highlights/u;

    if-nez p4, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/android/highlights/u;Z)V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/highlights/u$a;->a:Lcom/twitter/android/highlights/u;

    iput-boolean v2, v0, Lcom/twitter/android/highlights/u;->c:Z

    .line 202
    if-eqz p4, :cond_2

    const v0, 0x7f0a0994

    .line 204
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/highlights/u$a;->a:Lcom/twitter/android/highlights/u;

    invoke-static {v2}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/android/highlights/u;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 206
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 200
    goto :goto_0

    .line 202
    :cond_2
    const v0, 0x7f0a099d

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/twitter/android/highlights/u$a;->b:Lcom/twitter/library/client/Session;

    .line 179
    return-void
.end method

.method protected a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/android/highlights/u$a;->a:Lcom/twitter/android/highlights/u;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/android/highlights/u;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/highlights/u$a;->b:Lcom/twitter/library/client/Session;

    const-string/jumbo v1, "retweet_dialog"

    const/4 v2, 0x0

    const-string/jumbo v3, "dismiss"

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/highlights/u$a;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method public b(JLcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 211
    iget-object v0, p0, Lcom/twitter/android/highlights/u$a;->b:Lcom/twitter/library/client/Session;

    const-string/jumbo v1, "story"

    const-string/jumbo v2, "tweet"

    const-string/jumbo v3, "quote"

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/highlights/u$a;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/android/highlights/u$a;->b:Lcom/twitter/library/client/Session;

    const-string/jumbo v1, "retweet_dialog"

    const/4 v2, 0x0

    const-string/jumbo v3, "impression"

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/highlights/u$a;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    return-void
.end method
