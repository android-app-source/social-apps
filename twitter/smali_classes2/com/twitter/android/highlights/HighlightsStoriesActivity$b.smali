.class public Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/HighlightsStoriesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:I

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/analytics/feature/model/ClientEventLog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 824
    iput v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->b:I

    .line 825
    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->c:Z

    .line 826
    iput-boolean v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->d:Z

    .line 832
    iput-object p1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a:Landroid/content/Context;

    .line 833
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 879
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x20000000

    .line 880
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 881
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 882
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 884
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->c:Z

    if-eqz v1, :cond_2

    .line 886
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 887
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 890
    const-string/jumbo v1, "InvalidStoryId"

    iput-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->e:Ljava/lang/String;

    .line 892
    :cond_1
    const-string/jumbo v1, "EXTRA_HIGHLIGHTS_TAPPED_STORY_ID"

    iget-object v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 894
    :cond_2
    iget v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->b:I

    packed-switch v1, :pswitch_data_0

    .line 905
    :goto_0
    const-string/jumbo v1, "EXTRA_HIGHLIGHTS_SAMPLE_STORIES"

    iget-boolean v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 906
    const-string/jumbo v1, "EXTRA_HIGHLIGHTS_SCRIBE_LOG"

    iget-object v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->g:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 907
    return-object v0

    .line 897
    :pswitch_0
    const-string/jumbo v1, "EXTRA_HIGHLIGHTS_FORCE_STATE"

    iget v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 894
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(I)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;
    .locals 0

    .prologue
    .line 841
    iput p1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->b:I

    .line 842
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;
    .locals 0

    .prologue
    .line 874
    iput-object p1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->g:Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 875
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;
    .locals 0

    .prologue
    .line 866
    iput-object p1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->f:Ljava/lang/String;

    .line 867
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;
    .locals 0

    .prologue
    .line 846
    iput-boolean p1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->d:Z

    .line 847
    return-object p0
.end method

.method public a(ZLjava/lang/String;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;
    .locals 0

    .prologue
    .line 857
    iput-boolean p1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->c:Z

    .line 858
    iput-object p2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->e:Ljava/lang/String;

    .line 859
    return-object p0
.end method
