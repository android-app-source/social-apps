.class Lcom/twitter/android/highlights/ak$a;
.super Lcom/twitter/android/highlights/y;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/ak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field public final f:Lcom/twitter/media/ui/image/UserImageView;

.field public final g:Landroid/widget/TextView;

.field public final h:Landroid/view/View;

.field public final i:Landroid/widget/TextView;

.field public final j:Landroid/widget/TextView;

.field public final k:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/highlights/y;-><init>(ILandroid/view/View;)V

    .line 99
    const v0, 0x7f1302ef

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ak$a;->f:Lcom/twitter/media/ui/image/UserImageView;

    .line 100
    const v0, 0x7f130058

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ak$a;->g:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f13040e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ak$a;->h:Landroid/view/View;

    .line 102
    const v0, 0x7f130073

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ak$a;->i:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f1303f2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ak$a;->j:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/twitter/android/highlights/ak$a;->j:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 105
    const v0, 0x7f13005a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/highlights/ak$a;->k:Landroid/view/ViewGroup;

    .line 106
    return-void
.end method
