.class Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/content/Intent;JI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Z

.field final synthetic c:Landroid/content/Intent;

.field final synthetic d:J

.field final synthetic e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/highlights/HighlightsStoriesActivity;IZLandroid/content/Intent;J)V
    .locals 1

    .prologue
    .line 539
    iput-object p1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    iput p2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->a:I

    iput-boolean p3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->b:Z

    iput-object p4, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->c:Landroid/content/Intent;

    iput-wide p5, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->d:J

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x64

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 542
    iget v0, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->a:I

    if-nez v0, :cond_2

    move v0, v1

    .line 544
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->b:Z

    if-eqz v3, :cond_3

    .line 545
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    iget-object v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-static {v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Lcom/twitter/android/highlights/HighlightsStoriesActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->b(Ljava/lang/String;)V

    .line 548
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-virtual {v1, v11, v0, v10}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/database/Cursor;II)V

    .line 565
    :cond_1
    :goto_1
    return-void

    .line 542
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 549
    :cond_3
    iget-object v3, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->c:Landroid/content/Intent;

    if-eqz v3, :cond_1

    .line 550
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->d:J

    const-wide/32 v8, 0xdbba0

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    move v1, v2

    .line 552
    :cond_4
    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-static {v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Lcom/twitter/android/highlights/HighlightsStoriesActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 553
    :cond_5
    const-string/jumbo v1, "HighlightsActivity"

    const-string/jumbo v2, "Non-notification launch, with stale data or story ID in URI. Will refresh."

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    iget-object v2, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-static {v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Lcom/twitter/android/highlights/HighlightsStoriesActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Ljava/lang/String;)V

    .line 563
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-virtual {v1, v11, v0, v10}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/database/Cursor;II)V

    goto :goto_1

    .line 561
    :cond_6
    iget-object v1, p0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->e:Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    iput-boolean v2, v1, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->d:Z

    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 539
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$1;->a(Ljava/lang/Boolean;)V

    return-void
.end method
