.class Lcom/twitter/android/highlights/StoriesActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/highlights/StoriesActivity;->b(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/ObservableScrollView;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/highlights/StoriesActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/highlights/StoriesActivity;Lcom/twitter/library/widget/ObservableScrollView;I)V
    .locals 0

    .prologue
    .line 834
    iput-object p1, p0, Lcom/twitter/android/highlights/StoriesActivity$1;->c:Lcom/twitter/android/highlights/StoriesActivity;

    iput-object p2, p0, Lcom/twitter/android/highlights/StoriesActivity$1;->a:Lcom/twitter/library/widget/ObservableScrollView;

    iput p3, p0, Lcom/twitter/android/highlights/StoriesActivity$1;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 837
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity$1;->c:Lcom/twitter/android/highlights/StoriesActivity;

    invoke-virtual {v0}, Lcom/twitter/android/highlights/StoriesActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 838
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity$1;->a:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ObservableScrollView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/y;

    .line 839
    iget-object v1, p0, Lcom/twitter/android/highlights/StoriesActivity$1;->a:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-virtual {v1, v3}, Lcom/twitter/library/widget/ObservableScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 840
    iget v2, p0, Lcom/twitter/android/highlights/StoriesActivity$1;->b:I

    if-le v1, v2, :cond_1

    .line 841
    iget-object v0, v0, Lcom/twitter/android/highlights/y;->E:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 843
    :cond_1
    iget-object v0, v0, Lcom/twitter/android/highlights/y;->E:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
