.class public Lcom/twitter/android/highlights/h$c;
.super Lcom/twitter/android/highlights/y;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/widget/ImageView;

.field public final c:Landroid/view/View;

.field public final d:Lcom/twitter/library/media/player/InlineVideoView;

.field public final e:Landroid/view/View;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/view/View;

.field public final h:Landroid/view/View;

.field public final i:Landroid/view/View;

.field public final j:Landroid/view/View;

.field public final k:Landroid/view/View;

.field public final l:Landroid/view/View;

.field public final m:Landroid/view/View;

.field public final n:Landroid/view/View;

.field public final o:Landroid/view/View;

.field public final p:Lcom/twitter/android/widget/highlights/IntroStorySpinner;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 184
    const/16 v0, 0xa

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/highlights/y;-><init>(ILandroid/view/View;)V

    .line 185
    const v0, 0x7f1303db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->a:Landroid/view/View;

    .line 186
    const v0, 0x7f130406

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->b:Landroid/widget/ImageView;

    .line 187
    const v0, 0x7f1303e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->c:Landroid/view/View;

    .line 188
    const v0, 0x7f1303e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/player/InlineVideoView;

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->d:Lcom/twitter/library/media/player/InlineVideoView;

    .line 189
    const v0, 0x7f1303e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->e:Landroid/view/View;

    .line 190
    const v0, 0x7f1303e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->f:Landroid/widget/TextView;

    .line 191
    const v0, 0x7f1303e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->g:Landroid/view/View;

    .line 192
    const v0, 0x7f1303e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->h:Landroid/view/View;

    .line 193
    const v0, 0x7f1303e7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->i:Landroid/view/View;

    .line 194
    const v0, 0x7f1303e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->j:Landroid/view/View;

    .line 195
    const v0, 0x7f1303e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->k:Landroid/view/View;

    .line 196
    const v0, 0x7f1303ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->l:Landroid/view/View;

    .line 197
    const v0, 0x7f1303eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->m:Landroid/view/View;

    .line 198
    const v0, 0x7f1303ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->n:Landroid/view/View;

    .line 199
    const v0, 0x7f1303ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->o:Landroid/view/View;

    .line 200
    const v0, 0x7f1303ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/highlights/IntroStorySpinner;

    iput-object v0, p0, Lcom/twitter/android/highlights/h$c;->p:Lcom/twitter/android/widget/highlights/IntroStorySpinner;

    .line 201
    return-void
.end method
