.class public Lcom/twitter/android/highlights/StoriesActivity$a;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/StoriesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/highlights/StoriesActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/highlights/StoriesActivity;)V
    .locals 1

    .prologue
    .line 1136
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 1137
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity$a;->a:Ljava/lang/ref/WeakReference;

    .line 1138
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 1133
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/highlights/StoriesActivity$a;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 2

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/twitter/android/highlights/StoriesActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/highlights/StoriesActivity;

    .line 1143
    if-nez v0, :cond_0

    .line 1153
    :goto_0
    return-void

    .line 1146
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    .line 1147
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1148
    invoke-virtual {v0, p1}, Lcom/twitter/android/highlights/StoriesActivity;->a(Lcom/twitter/library/service/s;)V

    .line 1152
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/android/highlights/StoriesActivity;->i()V

    goto :goto_0

    .line 1150
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/highlights/StoriesActivity;->h()V

    goto :goto_1
.end method
