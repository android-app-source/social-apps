.class public Lcom/twitter/android/highlights/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/highlights/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 18
    const v0, 0x7f040152

    return v0
.end method

.method public a(Lcom/twitter/android/highlights/x;Lcom/twitter/android/highlights/y;Landroid/content/Context;Lcom/twitter/android/highlights/q$a;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 38
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 39
    check-cast p1, Lcom/twitter/android/highlights/d;

    .line 40
    check-cast p2, Lcom/twitter/android/highlights/d$a;

    .line 42
    iget-object v1, p2, Lcom/twitter/android/highlights/d$a;->c:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/twitter/android/highlights/d;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v1, p2, Lcom/twitter/android/highlights/d$a;->d:Landroid/widget/TextView;

    const v2, 0x7f0a0b92

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/twitter/android/highlights/d;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v4, v4, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v0, p1, Lcom/twitter/android/highlights/d;->a:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->e:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 49
    :goto_0
    iget-boolean v0, p1, Lcom/twitter/android/highlights/d;->d:Z

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->f:Landroid/widget/TextView;

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/highlights/d;->a(Landroid/content/Context;Lcne;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    :goto_1
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->g:Landroid/widget/TextView;

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/highlights/d;->b(Landroid/content/Context;Lcne;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    new-instance v0, Lcom/twitter/android/highlights/v;

    iget-object v2, p1, Lcom/twitter/android/highlights/d;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, p2, Lcom/twitter/android/highlights/d$a;->A:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 57
    invoke-static {p1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v4

    move-object v1, p3

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/highlights/v;-><init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v1, p2, Lcom/twitter/android/highlights/d$a;->A:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 59
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->a:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p1, Lcom/twitter/android/highlights/d;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 60
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->a:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p1, Lcom/twitter/android/highlights/d;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 61
    invoke-static {v1}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 62
    iget-boolean v0, p1, Lcom/twitter/android/highlights/d;->c:Z

    if-eqz v0, :cond_2

    .line 63
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p1, Lcom/twitter/android/highlights/d;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    .line 64
    invoke-static {v1}, Lcom/twitter/android/profiles/f;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 68
    :goto_2
    return-void

    .line 47
    :cond_0
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->e:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 53
    :cond_1
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 66
    :cond_2
    iget-object v0, p2, Lcom/twitter/android/highlights/d$a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    goto :goto_2
.end method

.method public a(Lcom/twitter/android/highlights/y;Landroid/view/LayoutInflater;Lcom/twitter/android/highlights/q$a;)V
    .locals 1

    .prologue
    .line 29
    check-cast p1, Lcom/twitter/android/highlights/d$a;

    .line 30
    iget-object v0, p1, Lcom/twitter/android/highlights/d$a;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, p3}, Lcom/twitter/media/ui/image/MediaImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    iget-object v0, p1, Lcom/twitter/android/highlights/d$a;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, p3}, Lcom/twitter/media/ui/image/MediaImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    return-void
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 23
    const v0, 0x7f0a0401

    return v0
.end method
