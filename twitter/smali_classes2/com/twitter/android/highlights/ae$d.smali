.class public Lcom/twitter/android/highlights/ae$d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Lcom/twitter/media/ui/image/UserImageView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/view/View;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/view/View;

.field public final h:Lcom/twitter/library/media/widget/TweetMediaView;

.field public final i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field public final j:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field public k:Lcom/twitter/model/core/Tweet;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p1, p0, Lcom/twitter/android/highlights/ae$d;->a:Landroid/view/View;

    .line 142
    const v0, 0x7f1302ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->b:Lcom/twitter/media/ui/image/UserImageView;

    .line 143
    const v0, 0x7f130058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->c:Landroid/widget/TextView;

    .line 144
    const v0, 0x7f13040e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->d:Landroid/view/View;

    .line 145
    const v0, 0x7f130073

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->e:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f1303f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->f:Landroid/widget/TextView;

    .line 147
    iget-object v0, p0, Lcom/twitter/android/highlights/ae$d;->f:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 148
    const v0, 0x7f1301d9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->g:Landroid/view/View;

    .line 149
    const v0, 0x7f130411

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/widget/TweetMediaView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->h:Lcom/twitter/library/media/widget/TweetMediaView;

    .line 150
    const v0, 0x7f130420

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->i:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 151
    const v0, 0x7f13041f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iput-object v0, p0, Lcom/twitter/android/highlights/ae$d;->j:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 152
    return-void
.end method
