.class public Lcom/twitter/android/highlights/v;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/model/core/TwitterUser;

.field public final b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/twitter/analytics/model/ScribeItem;

.field private final e:Z

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 46
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/highlights/v;-><init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/v;->c:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    .line 54
    iput-object p3, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 55
    iput-boolean p7, p0, Lcom/twitter/android/highlights/v;->e:Z

    .line 56
    iget v0, p2, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    .line 57
    iget-object v1, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setChecked(Z)V

    .line 58
    invoke-direct {p0, v0}, Lcom/twitter/android/highlights/v;->a(Z)V

    .line 59
    iput-object p4, p0, Lcom/twitter/android/highlights/v;->d:Lcom/twitter/analytics/model/ScribeItem;

    .line 60
    iput-object p5, p0, Lcom/twitter/android/highlights/v;->f:Ljava/lang/String;

    .line 61
    iput-object p6, p0, Lcom/twitter/android/highlights/v;->g:Ljava/lang/String;

    .line 62
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const v1, 0x7f110195

    const v2, 0x7f110190

    .line 65
    iget-boolean v0, p0, Lcom/twitter/android/highlights/v;->e:Z

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/twitter/android/highlights/v;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 67
    iget-object v4, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    if-eqz p1, :cond_0

    const v0, 0x7f0a0935

    :goto_0
    invoke-virtual {v4, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setText(I)V

    .line 68
    iget-object v4, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTextColor(I)V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setBackgroundColor(I)V

    .line 78
    :goto_3
    return-void

    .line 67
    :cond_0
    const v0, 0x7f0a03a5

    goto :goto_0

    :cond_1
    move v0, v2

    .line 68
    goto :goto_1

    :cond_2
    move v2, v1

    .line 69
    goto :goto_2

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->R:I

    if-lez v0, :cond_4

    .line 72
    iget-object v0, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v1, p0, Lcom/twitter/android/highlights/v;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->R:I

    int-to-long v2, v2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 75
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method


# virtual methods
.method a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 130
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/highlights/v;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/highlights/v;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "story"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "user"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    aput-object p2, v1, v2

    .line 131
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/highlights/v;->d:Lcom/twitter/analytics/model/ScribeItem;

    .line 132
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 133
    iget-object v1, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v1, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    iget-object v4, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v4}, Lcom/twitter/model/core/TwitterUser;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v3, v1, v4}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 134
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 135
    return-void
.end method

.method public a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 84
    iget-object v0, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 85
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/highlights/v;->b:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v2}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->toggle()V

    .line 87
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/highlights/v;->a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Z)V

    .line 88
    if-eqz v0, :cond_1

    const-string/jumbo v2, "follow"

    :goto_1
    invoke-virtual {p0, p2, v2}, Lcom/twitter/android/highlights/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 91
    iget-object v3, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget v4, v3, Lcom/twitter/model/core/TwitterUser;->R:I

    if-eqz v0, :cond_2

    move v2, v1

    :goto_2
    add-int/2addr v2, v4

    iput v2, v3, Lcom/twitter/model/core/TwitterUser;->R:I

    .line 101
    if-eqz v0, :cond_3

    .line 102
    iget-object v2, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget v3, v3, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v3, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v1

    iput v1, v2, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 108
    :goto_3
    invoke-direct {p0, v0}, Lcom/twitter/android/highlights/v;->a(Z)V

    .line 109
    return-void

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_1
    const-string/jumbo v2, "unfollow"

    goto :goto_1

    .line 91
    :cond_2
    const/4 v2, -0x1

    goto :goto_2

    .line 105
    :cond_3
    iget-object v2, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget v3, v3, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v3, v1}, Lcom/twitter/model/core/g;->b(II)I

    move-result v1

    iput v1, v2, Lcom/twitter/model/core/TwitterUser;->U:I

    goto :goto_3
.end method

.method a(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Z)V
    .locals 7

    .prologue
    .line 118
    if-eqz p3, :cond_0

    .line 119
    new-instance v1, Lbhq;

    iget-object v2, p0, Lcom/twitter/android/highlights/v;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v0, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v6, v0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 125
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 126
    return-void

    .line 122
    :cond_0
    new-instance v1, Lbhs;

    iget-object v2, p0, Lcom/twitter/android/highlights/v;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v0, p0, Lcom/twitter/android/highlights/v;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v6, v0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    goto :goto_0
.end method
