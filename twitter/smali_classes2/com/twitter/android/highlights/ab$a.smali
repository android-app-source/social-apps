.class public Lcom/twitter/android/highlights/ab$a;
.super Lcom/twitter/android/highlights/ae$e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/highlights/ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:Lcom/twitter/media/ui/image/BaseMediaImageView;

.field public b:Landroid/widget/ImageView;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p2, p1}, Lcom/twitter/android/highlights/ae$e;-><init>(ILandroid/view/View;)V

    .line 161
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 164
    const v0, 0x7f13035d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/BaseMediaImageView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ab$a;->a:Lcom/twitter/media/ui/image/BaseMediaImageView;

    .line 165
    const v0, 0x7f13041c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ab$a;->b:Landroid/widget/ImageView;

    .line 166
    const v0, 0x7f13041d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ab$a;->c:Landroid/view/View;

    .line 167
    const v0, 0x7f130152

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ab$a;->d:Landroid/view/View;

    .line 168
    const v0, 0x7f13041e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ab$a;->e:Landroid/widget/TextView;

    .line 169
    const v0, 0x7f1301e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ab$a;->f:Landroid/widget/TextView;

    .line 170
    const v0, 0x7f13030b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/highlights/ab$a;->g:Landroid/widget/TextView;

    .line 171
    return-void
.end method
