.class public Lcom/twitter/android/highlights/aj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/highlights/w;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-boolean v0, Lcom/twitter/android/highlights/aj;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;ZLcne;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 75
    sget-boolean v0, Lcom/twitter/android/highlights/aj;->a:Z

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 77
    const v1, 0x7f1100c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/twitter/android/highlights/aj;->b:I

    .line 78
    const v1, 0x7f1100c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/twitter/android/highlights/aj;->c:I

    .line 79
    sput-boolean v3, Lcom/twitter/android/highlights/aj;->a:Z

    .line 80
    const-class v0, Lcom/twitter/android/highlights/aj;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 84
    :cond_0
    if-eqz p2, :cond_2

    .line 85
    invoke-static {p1}, Lcom/twitter/model/util/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 88
    invoke-virtual {v0, v3}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 89
    invoke-virtual {v0, v2}, Lcom/twitter/model/util/a;->f(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 90
    invoke-virtual {v0, v2}, Lcom/twitter/model/util/a;->d(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    .line 95
    :goto_0
    iget-object v1, v0, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    invoke-static {v1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    .line 96
    invoke-virtual {v1, v0}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v0

    .line 97
    invoke-virtual {v0, p3}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v0

    sget v1, Lcom/twitter/android/highlights/aj;->b:I

    .line 98
    invoke-virtual {v0, v1}, Lcnf;->a(I)Lcnf;

    move-result-object v0

    sget v1, Lcom/twitter/android/highlights/aj;->c:I

    .line 99
    invoke-virtual {v0, v1}, Lcnf;->b(I)Lcnf;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 101
    invoke-static {}, Lcom/twitter/library/view/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-static {p0, v1, v0, v3}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 105
    :cond_1
    return-object v0

    .line 93
    :cond_2
    new-instance v0, Lcom/twitter/model/core/e;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/core/e;-><init>(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f040154

    return v0
.end method

.method public a(Lcom/twitter/android/highlights/x;Lcom/twitter/android/highlights/y;Landroid/content/Context;Lcom/twitter/android/highlights/q$a;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    .line 50
    check-cast p1, Lcom/twitter/android/highlights/ak;

    .line 51
    check-cast p2, Lcom/twitter/android/highlights/ak$a;

    .line 52
    iget-object v0, p2, Lcom/twitter/android/highlights/ak$a;->g:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v1, p2, Lcom/twitter/android/highlights/ak$a;->h:Landroid/view/View;

    iget-object v0, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->L:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-object v0, p2, Lcom/twitter/android/highlights/ak$a;->i:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    .line 55
    invoke-static {p3, v1}, Lcom/twitter/android/highlights/z;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p2, Lcom/twitter/android/highlights/ak$a;->j:Landroid/widget/TextView;

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/highlights/ak;->a(Landroid/content/Context;Lcne;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    iget-object v1, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    .line 58
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->l:I

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 63
    iget-object v1, p2, Lcom/twitter/android/highlights/ak$a;->f:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 64
    iget-object v1, p2, Lcom/twitter/android/highlights/ak$a;->f:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setTag(Ljava/lang/Object;)V

    .line 65
    iget-object v1, p2, Lcom/twitter/android/highlights/ak$a;->k:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 67
    new-instance v0, Lcom/twitter/android/highlights/u;

    iget-object v2, p1, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-object v3, p2, Lcom/twitter/android/highlights/ak$a;->y:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    iget-object v4, p2, Lcom/twitter/android/highlights/ak$a;->z:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    .line 68
    invoke-static {p1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/highlights/x;)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v5

    move-object v1, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/highlights/u;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/library/widget/CompoundDrawableAnimButton;Lcom/twitter/analytics/model/ScribeItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v1, p2, Lcom/twitter/android/highlights/ak$a;->y:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 70
    iget-object v1, p2, Lcom/twitter/android/highlights/ak$a;->z:Lcom/twitter/library/widget/CompoundDrawableAnimButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/CompoundDrawableAnimButton;->setTag(Ljava/lang/Object;)V

    .line 71
    return-void

    .line 53
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/highlights/y;Landroid/view/LayoutInflater;Lcom/twitter/android/highlights/q$a;)V
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lcom/twitter/android/highlights/ak$a;

    .line 42
    iget-object v0, p1, Lcom/twitter/android/highlights/ak$a;->f:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p3}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    iget-object v0, p1, Lcom/twitter/android/highlights/ak$a;->k:Landroid/view/ViewGroup;

    invoke-virtual {v0, p3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-void
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f0a0403

    return v0
.end method
