.class public Lcom/twitter/android/highlights/ak;
.super Lcom/twitter/android/highlights/x;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/highlights/ak$a;
    }
.end annotation


# instance fields
.field private final a:Z

.field public final b:Lcom/twitter/model/core/Tweet;

.field private c:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLbcq;ZLjava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Z)V
    .locals 12

    .prologue
    .line 37
    const/16 v6, 0xe

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/highlights/x;-><init>(Ljava/lang/String;JILbcq;ZLjava/lang/String;Ljava/lang/String;)V

    .line 39
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    .line 40
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/twitter/android/highlights/ak;->a:Z

    .line 41
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Lcom/twitter/app/common/base/h;

    invoke-direct {v0}, Lcom/twitter/app/common/base/h;-><init>()V

    const/4 v1, 0x1

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    const-class v1, Lcom/twitter/android/TweetActivity;

    .line 79
    invoke-virtual {v0, p1, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "tw"

    iget-object v2, p0, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    .line 80
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method public a(Landroid/content/res/Resources;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/android/highlights/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/highlights/ae$a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;)",
            "Lcom/twitter/android/highlights/w;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/android/highlights/aj;

    invoke-direct {v0}, Lcom/twitter/android/highlights/aj;-><init>()V

    return-object v0
.end method

.method public a(Landroid/view/View;)Lcom/twitter/android/highlights/y;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/android/highlights/ak$a;

    invoke-virtual {p0}, Lcom/twitter/android/highlights/ak;->a()I

    move-result v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/android/highlights/ak$a;-><init>(ILandroid/view/View;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcne;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/highlights/ak;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/twitter/android/highlights/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-boolean v1, p0, Lcom/twitter/android/highlights/ak;->a:Z

    invoke-static {p1, v0, v1, p2}, Lcom/twitter/android/highlights/aj;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;ZLcne;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/highlights/ak;->c:Ljava/lang/CharSequence;

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/highlights/ak;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string/jumbo v0, "TYPE_TWEET_NO_MEDIA"

    return-object v0
.end method
