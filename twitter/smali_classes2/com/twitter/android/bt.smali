.class public Lcom/twitter/android/bt;
.super Lcjr;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/bt$c;,
        Lcom/twitter/android/bt$a;,
        Lcom/twitter/android/bt$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/android/bu;",
        ">;",
        "Lcom/twitter/android/client/j;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

.field private final c:Lcom/twitter/library/client/v;

.field private final d:Lcom/twitter/model/util/FriendshipCache;

.field private final e:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/widget/AdapterView$OnItemClickListener;

.field private final g:Lcom/twitter/android/cx$c;

.field private final h:Landroid/view/View$OnClickListener;

.field private final i:Lcom/twitter/util/collection/ReferenceList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/ReferenceList",
            "<",
            "Lcom/twitter/android/cu;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/client/e;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/String;

.field private final l:I

.field private final m:I

.field private final n:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final o:Z

.field private final p:Z

.field private q:Lcom/twitter/library/view/d;

.field private r:Lcom/twitter/library/view/d;

.field private s:Lcom/twitter/library/view/d;

.field private t:Lcom/twitter/library/view/d;

.field private u:Landroid/database/Cursor;

.field private v:Z

.field private final w:Ljava/lang/String;

.field private final x:Z

.field private final y:Lcom/twitter/android/cx$d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "map_pin"

    const v2, 0x7f020828

    .line 90
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "newspaper"

    const v2, 0x7f020830

    .line 91
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "speech_bubble"

    const v2, 0x7f020819

    .line 92
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "head"

    const v2, 0x7f020835

    .line 93
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "megaphone"

    const v2, 0x7f02082c

    .line 94
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/android/bt;->a:Ljava/util/Map;

    .line 89
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Ljava/lang/String;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/av;Landroid/widget/AdapterView$OnItemClickListener;Lcom/twitter/android/cx$c;IZLjava/lang/String;ZILandroid/view/View$OnClickListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
            "Ljava/lang/String;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/widget/AdapterView$OnItemClickListener;",
            "Lcom/twitter/android/cx$c;",
            "IZ",
            "Ljava/lang/String;",
            "ZI",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 105
    invoke-static {}, Lcom/twitter/util/collection/ReferenceList;->a()Lcom/twitter/util/collection/ReferenceList;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/bt;->i:Lcom/twitter/util/collection/ReferenceList;

    .line 106
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/bt;->j:Ljava/util/List;

    .line 124
    new-instance v2, Lcom/twitter/android/bt$c;

    invoke-direct {v2}, Lcom/twitter/android/bt$c;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/bt;->y:Lcom/twitter/android/cx$d;

    .line 134
    iput-object p1, p0, Lcom/twitter/android/bt;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 135
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/bt;->c:Lcom/twitter/library/client/v;

    .line 136
    iput-object p2, p0, Lcom/twitter/android/bt;->k:Ljava/lang/String;

    .line 137
    iput-object p3, p0, Lcom/twitter/android/bt;->d:Lcom/twitter/model/util/FriendshipCache;

    .line 138
    iput-object p4, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    .line 139
    iput-object p5, p0, Lcom/twitter/android/bt;->f:Landroid/widget/AdapterView$OnItemClickListener;

    .line 140
    iput-object p6, p0, Lcom/twitter/android/bt;->g:Lcom/twitter/android/cx$c;

    .line 141
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/bt;->h:Landroid/view/View$OnClickListener;

    .line 142
    iput p7, p0, Lcom/twitter/android/bt;->l:I

    .line 143
    move/from16 v0, p11

    iput v0, p0, Lcom/twitter/android/bt;->m:I

    .line 144
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/twitter/android/bt;->p:Z

    .line 145
    iget v2, p0, Lcom/twitter/android/bt;->l:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 146
    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v3, 0x5

    .line 147
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p0, Lcom/twitter/android/bt;->c:Lcom/twitter/library/client/v;

    .line 148
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, "search"

    .line 149
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, "people"

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v2, p0, Lcom/twitter/android/bt;->n:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 155
    :goto_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/android/bt;->u:Landroid/database/Cursor;

    .line 156
    iput-boolean p8, p0, Lcom/twitter/android/bt;->o:Z

    .line 157
    iput-object p9, p0, Lcom/twitter/android/bt;->w:Ljava/lang/String;

    .line 158
    invoke-static {p1}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/k;->a()Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/bt;->x:Z

    .line 159
    return-void

    .line 151
    :cond_0
    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v3, 0x6

    .line 152
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, "search"

    .line 153
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, "universal"

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v2, p0, Lcom/twitter/android/bt;->n:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    goto :goto_0
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 823
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 824
    const v0, 0x7f04011b

    .line 826
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f04039e

    goto :goto_0
.end method

.method private a(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 254
    if-nez p4, :cond_6

    .line 255
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040137

    const/4 v2, 0x0

    .line 256
    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 257
    new-instance v1, Lcom/twitter/android/bt$a;

    new-instance v2, Lcom/twitter/android/cu;

    invoke-direct {v2, v0}, Lcom/twitter/android/cu;-><init>(Landroid/view/View;)V

    invoke-direct {v1, v2}, Lcom/twitter/android/bt$a;-><init>(Lcom/twitter/android/cu;)V

    .line 258
    iget v2, p3, Lcom/twitter/android/bu;->b:I

    sparse-switch v2, :sswitch_data_0

    .line 275
    iget-object v2, v1, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v2, v2, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/bt;->q:Lcom/twitter/library/view/d;

    .line 276
    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    .line 280
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/bt;->i:Lcom/twitter/util/collection/ReferenceList;

    iget-object v3, v1, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    invoke-virtual {v2, v3}, Lcom/twitter/util/collection/ReferenceList;->b(Ljava/lang/Object;)V

    .line 281
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object v7, v0

    move-object v8, v1

    .line 302
    :goto_1
    iput-object p3, v8, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 303
    iget-object v1, p0, Lcom/twitter/android/bt;->u:Landroid/database/Cursor;

    .line 304
    iget v0, p3, Lcom/twitter/android/bu;->d:I

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 305
    sget v0, Lbtv;->E:I

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 306
    sget-object v0, Lcom/twitter/android/bt;->a:Ljava/util/Map;

    sget v2, Lbtv;->F:I

    .line 307
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 308
    invoke-static {v9}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    .line 309
    iget-object v2, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v2, v2, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2, v9}, Lcom/twitter/library/widget/TweetView;->setReason(Ljava/lang/String;)V

    .line 310
    iget-object v2, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v2, v2, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/widget/TweetView;->setReasonIconResId(I)V

    .line 315
    :goto_2
    iget-object v0, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    sget v2, Lcni;->a:F

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    .line 316
    sget-object v0, Lbtd;->a:Lbtd;

    invoke-virtual {v0, v1}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v3

    .line 318
    invoke-static {p1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v2

    .line 319
    iget-boolean v0, p0, Lcom/twitter/android/bt;->x:Z

    if-eqz v0, :cond_9

    invoke-virtual {v2, v3}, Lbaa;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    move v1, v0

    .line 320
    :goto_3
    if-eqz v1, :cond_a

    invoke-virtual {v3}, Lcom/twitter/model/core/Tweet;->r()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    .line 321
    :goto_4
    iget-object v4, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v4, v4, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4, v0}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 322
    iget-object v4, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v4, v4, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4, v0}, Lcom/twitter/library/widget/TweetView;->setExpandCardMedia(Z)V

    .line 323
    if-nez v1, :cond_1

    .line 324
    iget v1, v3, Lcom/twitter/model/core/Tweet;->d:I

    and-int/lit8 v1, v1, -0x9

    iput v1, v3, Lcom/twitter/model/core/Tweet;->d:I

    .line 326
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/bt;->d:Lcom/twitter/model/util/FriendshipCache;

    if-eqz v1, :cond_2

    .line 327
    iget-object v1, p0, Lcom/twitter/android/bt;->d:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v1, v3}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/Tweet;)V

    .line 329
    :cond_2
    iget-object v1, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v1, v1, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v4, p0, Lcom/twitter/android/bt;->d:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v1, v4}, Lcom/twitter/library/widget/TweetView;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 331
    const-string/jumbo v1, "cards_forward_in_search_enabled"

    .line 332
    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    .line 334
    invoke-virtual {v2}, Lbaa;->b()Z

    move-result v1

    invoke-static {v3, v0, v1}, Lbwr;->a(Lcom/twitter/model/core/Tweet;ZZ)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v1, 0x1

    .line 336
    :goto_5
    new-instance v0, Lbxy;

    iget-object v2, p0, Lcom/twitter/android/bt;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v5, p0, Lcom/twitter/android/bt;->n:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbxy;-><init>(ZLandroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 339
    iget-object v1, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v1, v1, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setShowSocialBadge(Z)V

    .line 340
    iget-object v1, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v1, v1, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v2, p0, Lcom/twitter/android/bt;->v:Z

    invoke-virtual {v1, v3, v2, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;ZLbxy;)V

    .line 341
    iget-object v0, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v0, :cond_3

    .line 342
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 343
    const-string/jumbo v1, "reason_text"

    invoke-virtual {v0, v1, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 345
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v2, 0x0

    invoke-interface {v1, v7, v2, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 348
    :cond_3
    iget-object v0, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    .line 349
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 350
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    .line 352
    :cond_4
    return-object v7

    .line 260
    :sswitch_0
    iget-object v2, v1, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v2, v2, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/bt;->r:Lcom/twitter/library/view/d;

    .line 261
    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    goto/16 :goto_0

    .line 265
    :sswitch_1
    iget-object v2, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget v2, v2, Lcom/twitter/model/search/d;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 266
    iget-object v2, v1, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v2, v2, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/bt;->s:Lcom/twitter/library/view/d;

    .line 267
    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    goto/16 :goto_0

    .line 268
    :cond_5
    iget-object v2, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget v2, v2, Lcom/twitter/model/search/d;->b:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 269
    iget-object v2, v1, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v2, v2, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/bt;->t:Lcom/twitter/library/view/d;

    .line 270
    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    goto/16 :goto_0

    .line 283
    :cond_6
    instance-of v0, p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v0, :cond_7

    move-object v0, p4

    .line 284
    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 300
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/bt$a;

    move-object v7, v0

    move-object v8, v1

    goto/16 :goto_1

    .line 289
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "All views in the SearchResultAdapter are defined to be of type GroupedRowView but this view was of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 291
    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 292
    new-instance v1, Lcpb;

    invoke-direct {v1}, Lcpb;-><init>()V

    const-string/jumbo v2, "item type"

    iget v3, p3, Lcom/twitter/android/bu;->b:I

    .line 293
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    const-string/jumbo v2, "item style"

    iget v3, p3, Lcom/twitter/android/bu;->f:I

    .line 294
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    const-string/jumbo v2, "row position"

    .line 295
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    const-string/jumbo v2, "view class"

    .line 296
    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    .line 297
    invoke-virtual {v1, v0}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v1

    .line 292
    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    .line 298
    throw v0

    .line 312
    :cond_8
    iget-object v0, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setReason(Ljava/lang/String;)V

    .line 313
    iget-object v0, v8, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setReasonIconResId(I)V

    goto/16 :goto_2

    .line 319
    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_3

    .line 320
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 334
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 258
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 523
    if-nez p4, :cond_2

    .line 524
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    .line 525
    invoke-virtual {v0, p6, p5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 526
    new-instance v2, Lcom/twitter/android/bt$a;

    const v1, 0x7f1303c6

    .line 527
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-direct {v2, v1}, Lcom/twitter/android/bt$a;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    .line 528
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p4, v0

    move-object v1, v2

    .line 533
    :goto_0
    iput-object p3, v1, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 536
    iget-object v0, v1, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ce;

    .line 537
    if-nez v0, :cond_0

    .line 538
    new-instance v0, Lcom/twitter/android/ce;

    invoke-direct {v0, p1, v6}, Lcom/twitter/android/ce;-><init>(Landroid/content/Context;Z)V

    .line 539
    iget-object v2, v1, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 540
    iget-object v2, v1, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v3, p0, Lcom/twitter/android/bt;->f:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 541
    iget-object v2, p0, Lcom/twitter/android/bt;->j:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 543
    :cond_0
    new-instance v2, Lcom/twitter/library/provider/p;

    iget-object v3, p0, Lcom/twitter/android/bt;->u:Landroid/database/Cursor;

    iget v4, p3, Lcom/twitter/android/bu;->d:I

    iget v5, p3, Lcom/twitter/android/bu;->e:I

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/library/provider/p;-><init>(Landroid/database/Cursor;II)V

    .line 544
    invoke-virtual {v0}, Lcom/twitter/android/ce;->k()Lcjt;

    move-result-object v0

    new-instance v3, Lcbe;

    invoke-direct {v3, v2}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v0, v3}, Lcjt;->a(Lcbi;)Lcbi;

    .line 545
    iget-object v0, v1, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0, p3}, Lcom/twitter/internal/android/widget/HorizontalListView;->setTag(Ljava/lang/Object;)V

    .line 547
    iget-object v0, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v0, :cond_1

    .line 548
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 549
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 550
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v2, 0x0

    invoke-interface {v1, p4, v2, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 552
    :cond_1
    return-object p4

    .line 530
    :cond_2
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 531
    invoke-virtual {p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    move-object v1, v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;
    .locals 3

    .prologue
    .line 504
    if-nez p3, :cond_0

    .line 505
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04012a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 507
    new-instance v1, Lcom/twitter/android/bt$a;

    const v0, 0x7f13011b

    .line 508
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f1303c7

    .line 509
    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/bt$a;-><init>(Landroid/widget/TextView;Landroid/view/View;)V

    .line 510
    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 514
    :goto_0
    iput-object p2, v0, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 515
    iget-object v0, v0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    return-object p3

    .line 512
    :cond_0
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/bu;Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 4

    .prologue
    .line 858
    sget v0, Lbtv;->s:I

    invoke-interface {p5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 859
    sget v1, Lbtv;->r:I

    invoke-interface {p5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 861
    if-nez p1, :cond_1

    .line 862
    invoke-direct {p0, v0}, Lcom/twitter/android/bt;->a(I)I

    move-result v0

    .line 863
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    .line 864
    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 865
    const v1, 0x7f130156

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/TopicView;

    .line 866
    new-instance v2, Lcom/twitter/android/bt$a;

    invoke-direct {v2, v1}, Lcom/twitter/android/bt$a;-><init>(Lcom/twitter/android/widget/TopicView;)V

    .line 867
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    .line 873
    :goto_0
    iput-object p3, v2, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 874
    invoke-direct {p0, v1, p5}, Lcom/twitter/android/bt;->a(Lcom/twitter/android/widget/TopicView;Landroid/database/Cursor;)V

    .line 876
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    .line 877
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {v1, v0, v2, v3}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 879
    :cond_0
    return-object v0

    .line 869
    :cond_1
    check-cast p1, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 870
    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    .line 871
    iget-object v1, v0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    move-object v2, v0

    move-object v0, p1

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 671
    if-nez p2, :cond_0

    .line 672
    new-instance p2, Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 673
    new-instance v0, Lcom/twitter/android/bt$a;

    invoke-direct {v0}, Lcom/twitter/android/bt$a;-><init>()V

    .line 674
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 678
    :goto_0
    iput-object p1, v0, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 679
    return-object p2

    .line 676
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/bt;)Lcom/twitter/android/av;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    return-object v0
.end method

.method private a(Lcom/twitter/android/widget/TopicView;Landroid/database/Cursor;)V
    .locals 24

    .prologue
    .line 831
    sget v2, Lbtv;->s:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 832
    sget v2, Lbtv;->z:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 833
    sget v2, Lbtv;->w:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 834
    sget v2, Lbtv;->C:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 835
    sget v2, Lbtv;->r:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 836
    sget v2, Lbtv;->A:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 837
    sget v2, Lbtv;->B:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 838
    sget v2, Lbtv;->u:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 839
    sget v2, Lbtv;->v:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 840
    sget v2, Lbtv;->D:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v16

    .line 841
    sget v2, Lbtv;->t:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 842
    sget v2, Lbtv;->x:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 843
    sget v2, Lbtv;->y:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 844
    sget v2, Lbtv;->f:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 845
    sget v17, Lbtv;->h:I

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 846
    sget v17, Lbtv;->i:I

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 847
    sget v17, Lbtv;->g:I

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 848
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/bt;->o:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    const/16 v17, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/bt;->o:Z

    move/from16 v18, v0

    if-nez v18, :cond_1

    const/16 v18, 0x1

    .line 850
    :goto_1
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v3, p1

    .line 848
    invoke-virtual/range {v3 .. v23}, Lcom/twitter/android/widget/TopicView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    return-void

    .line 848
    :cond_0
    const/16 v17, 0x0

    goto :goto_0

    :cond_1
    const/16 v18, 0x0

    goto :goto_1
.end method

.method private a(Lcom/twitter/internal/android/widget/GroupedRowView;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 202
    packed-switch p2, :pswitch_data_0

    .line 237
    :goto_0
    return-void

    .line 204
    :pswitch_0
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    .line 205
    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 209
    :pswitch_1
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    .line 210
    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    .line 211
    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->a()V

    goto :goto_0

    .line 215
    :pswitch_2
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    .line 216
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 220
    :pswitch_3
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    .line 221
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 225
    :pswitch_4
    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    goto :goto_0

    .line 229
    :pswitch_5
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    .line 230
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/android/bt;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/twitter/android/bt;->l:I

    return v0
.end method

.method private b(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    .line 360
    if-nez p4, :cond_1

    .line 361
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040139

    const/4 v3, 0x0

    .line 362
    move-object/from16 v0, p5

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 363
    const v1, 0x7f130378

    invoke-virtual {v9, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Landroid/support/v4/view/ViewPager;

    .line 364
    new-instance v1, Lcom/twitter/android/cx;

    iget-object v2, p0, Lcom/twitter/android/bt;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    iget-object v3, p0, Lcom/twitter/android/bt;->d:Lcom/twitter/model/util/FriendshipCache;

    iget-object v5, p0, Lcom/twitter/android/bt;->y:Lcom/twitter/android/cx$d;

    iget-object v6, p0, Lcom/twitter/android/bt;->g:Lcom/twitter/android/cx$c;

    .line 365
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    move v4, p2

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/cx;-><init>(Landroid/content/Context;Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/android/cx$d;Lcom/twitter/android/cx$c;Landroid/view/LayoutInflater;)V

    .line 366
    invoke-virtual {v10, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 367
    iget-object v2, p0, Lcom/twitter/android/bt;->j:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    const v2, 0x7f1303cb

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/twitter/android/widget/PipView;

    .line 369
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    .line 370
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    .line 371
    new-instance v2, Lcom/twitter/android/bt$1;

    move-object v3, p0

    move-object/from16 v4, p5

    move v6, p2

    move-object v7, v9

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/bt$1;-><init>(Lcom/twitter/android/bt;Landroid/view/ViewParent;IILcom/twitter/internal/android/widget/GroupedRowView;Lcom/twitter/android/widget/PipView;)V

    invoke-virtual {v10, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 385
    new-instance v2, Lcom/twitter/android/bt$a;

    invoke-direct {v2, v10, v8}, Lcom/twitter/android/bt$a;-><init>(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/widget/PipView;)V

    .line 386
    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    .line 392
    :goto_0
    iput-object p3, v2, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 394
    new-instance v3, Lcom/twitter/library/provider/p;

    iget-object v4, p0, Lcom/twitter/android/bt;->u:Landroid/database/Cursor;

    iget v5, p3, Lcom/twitter/android/bu;->d:I

    iget v6, p3, Lcom/twitter/android/bu;->e:I

    invoke-direct {v3, v4, v5, v6}, Lcom/twitter/library/provider/p;-><init>(Landroid/database/Cursor;II)V

    .line 395
    invoke-virtual {v1, v3}, Lcom/twitter/android/cx;->a(Landroid/database/Cursor;)Lcbi;

    .line 396
    invoke-virtual {v1}, Lcom/twitter/android/cx;->getCount()I

    move-result v1

    .line 397
    const/4 v3, 0x1

    if-le v1, v3, :cond_2

    .line 398
    iget-object v3, v2, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v3, v1}, Lcom/twitter/android/widget/PipView;->setPipCount(I)V

    .line 399
    iget-object v1, v2, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    .line 405
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v1, :cond_0

    .line 406
    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 407
    const-string/jumbo v2, "position"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 408
    const-string/jumbo v2, "page"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 409
    iget-object v2, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v3, 0x0

    invoke-interface {v2, v9, v3, v1}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 411
    :cond_0
    return-object v9

    .line 388
    :cond_1
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 389
    invoke-virtual/range {p4 .. p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/bt$a;

    .line 390
    iget-object v2, v1, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/cx;

    move-object/from16 v9, p4

    move-object v11, v2

    move-object v2, v1

    move-object v1, v11

    goto :goto_0

    .line 401
    :cond_2
    iget-object v1, v2, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    goto :goto_1
.end method

.method private c(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    .line 419
    if-nez p4, :cond_2

    .line 420
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04013b

    const/4 v2, 0x0

    .line 421
    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 422
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/user/UserSocialView;

    .line 423
    const v2, 0x7f0200b1

    invoke-virtual {v1, v2}, Lcom/twitter/ui/user/UserSocialView;->setFollowBackgroundResource(I)V

    .line 424
    new-instance v2, Lcom/twitter/android/bt$a;

    new-instance v3, Lcom/twitter/android/db;

    invoke-direct {v3, v1}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;)V

    new-instance v4, Lcom/twitter/android/bt$b;

    invoke-direct {v4, p0}, Lcom/twitter/android/bt$b;-><init>(Lcom/twitter/android/bt;)V

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/bt$a;-><init>(Lcom/twitter/android/db;Lcom/twitter/android/bt$b;)V

    .line 426
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p4, v0

    move-object v0, v1

    .line 432
    :goto_0
    iput-object p3, v2, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 433
    iget-object v3, p0, Lcom/twitter/android/bt;->u:Landroid/database/Cursor;

    .line 434
    iget v1, p3, Lcom/twitter/android/bu;->d:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 435
    sget v1, Lbtv;->f:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 436
    invoke-virtual {v0, v4, v5}, Lcom/twitter/ui/user/UserSocialView;->setUserId(J)V

    .line 437
    iget-object v1, v2, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    iput-wide v4, v1, Lcom/twitter/android/db;->e:J

    .line 439
    sget v1, Lbtv;->i:I

    .line 440
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 441
    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setUserImageUrl(Ljava/lang/String;)V

    .line 443
    sget v1, Lbtv;->g:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v6, Lbtv;->h:I

    .line 444
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 443
    invoke-virtual {v0, v1, v6}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    sget v1, Lbtv;->j:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 446
    and-int/lit8 v1, v6, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setProtected(Z)V

    .line 447
    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setVerified(Z)V

    .line 451
    sget v1, Lbtv;->p:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 453
    if-eqz v1, :cond_5

    const-string/jumbo v6, "[^\\S]"

    const-string/jumbo v7, " "

    .line 454
    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 455
    :goto_3
    sget v6, Lbtv;->q:I

    .line 456
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/model/core/v;->a([B)Lcom/twitter/model/core/v;

    move-result-object v6

    .line 455
    invoke-virtual {v0, v1, v6}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 459
    sget v1, Lbtv;->l:I

    .line 460
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    sget-object v6, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 459
    invoke-static {v1, v6}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcgi;

    .line 461
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v6

    invoke-virtual {v0, v1, v6}, Lcom/twitter/ui/user/UserSocialView;->a(Lcgi;Z)V

    .line 462
    iget-object v6, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v6, :cond_0

    .line 463
    new-instance v6, Landroid/os/Bundle;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(I)V

    .line 464
    const-string/jumbo v7, "position"

    invoke-virtual {v6, v7, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 465
    iget-object v7, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v8, 0x0

    invoke-interface {v7, p4, v8, v6}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 468
    :cond_0
    iget-object v6, p0, Lcom/twitter/android/bt;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v6}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_6

    .line 469
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setFollowVisibility(I)V

    .line 498
    :cond_1
    :goto_4
    return-object p4

    .line 428
    :cond_2
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 429
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    .line 430
    invoke-virtual {p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/bt$a;

    move-object v2, v1

    goto/16 :goto_0

    .line 446
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 447
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 454
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 471
    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/twitter/ui/user/UserSocialView;->setFollowVisibility(I)V

    .line 472
    iget-object v6, v2, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    invoke-virtual {v6, v1}, Lcom/twitter/android/bt$b;->a(Lcgi;)V

    .line 473
    iget-object v1, v2, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    invoke-virtual {v1, p2}, Lcom/twitter/android/bt$b;->a(I)V

    .line 474
    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setFollowBackgroundResource(I)V

    .line 475
    const v1, 0x7f0200b0

    iget-object v6, v2, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    invoke-virtual {v0, v1, v6}, Lcom/twitter/ui/user/UserSocialView;->a(ILcom/twitter/ui/user/BaseUserView$a;)V

    .line 477
    iget-object v1, p0, Lcom/twitter/android/bt;->d:Lcom/twitter/model/util/FriendshipCache;

    .line 478
    sget v6, Lbtv;->k:I

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 479
    if-eqz v1, :cond_7

    .line 480
    invoke-virtual {v1, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 481
    invoke-virtual {v1, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    .line 486
    :cond_7
    :goto_5
    iget-object v1, v2, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    iput v6, v1, Lcom/twitter/android/db;->f:I

    .line 487
    invoke-static {v6}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 488
    const v1, 0x7f020835

    .line 489
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    .line 488
    invoke-virtual {v0, v1, v6, v2}, Lcom/twitter/ui/user/UserSocialView;->a(IIZ)V

    goto :goto_4

    .line 483
    :cond_8
    invoke-static {v6}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    goto :goto_5

    .line 491
    :cond_9
    const/16 v1, 0x28

    const v2, 0x7f020835

    sget v4, Lbtv;->n:I

    .line 492
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 494
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v5

    .line 491
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/user/UserSocialView;->a(IILjava/lang/String;IZ)V

    goto :goto_4
.end method

.method static synthetic c(Lcom/twitter/android/bt;)Lcom/twitter/app/common/base/TwitterFragmentActivity;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/bt;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    return-object v0
.end method

.method private d(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 559
    if-nez p4, :cond_1

    .line 560
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040130

    const/4 v2, 0x0

    .line 561
    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 562
    new-instance v2, Lcom/twitter/android/bt$a;

    const v1, 0x7f13011b

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {v2, v1}, Lcom/twitter/android/bt$a;-><init>(Landroid/widget/TextView;)V

    .line 563
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 568
    :goto_0
    iput-object p3, v1, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 569
    iget-object v1, v1, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    iget-object v2, p3, Lcom/twitter/android/bu;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v1, :cond_0

    .line 571
    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 572
    const-string/jumbo v2, "position"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 573
    iget-object v2, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3, v1}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 575
    :cond_0
    return-object v0

    .line 565
    :cond_1
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 566
    invoke-virtual {p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    move-object v1, v0

    move-object v0, p4

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/bt;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/bt;->c:Lcom/twitter/library/client/v;

    return-object v0
.end method

.method private e(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 582
    if-nez p4, :cond_2

    .line 583
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403cb

    .line 584
    invoke-virtual {v0, v1, p5, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 585
    new-instance v2, Lcom/twitter/android/bt$a;

    const v0, 0x7f1307f8

    .line 586
    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1307f9

    .line 587
    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/bt$a;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 588
    invoke-virtual {p4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 593
    :goto_0
    iput-object p3, v1, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 594
    new-instance v2, Landroid/text/SpannableString;

    iget-object v0, p3, Lcom/twitter/android/bu;->g:Lcom/twitter/model/search/c;

    iget-object v0, v0, Lcom/twitter/model/search/c;->b:Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 595
    iget-object v0, p3, Lcom/twitter/android/bu;->g:Lcom/twitter/model/search/c;

    iget-object v0, v0, Lcom/twitter/model/search/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 596
    iget-object v0, p3, Lcom/twitter/android/bu;->g:Lcom/twitter/model/search/c;

    iget-object v0, v0, Lcom/twitter/model/search/c;->c:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 597
    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x3

    invoke-direct {v3, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aget v4, v0, v5

    aget v0, v0, v6

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 600
    :cond_0
    iget-object v0, v1, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    const v3, 0x7f0a0916

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 601
    iget-object v0, v1, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    const v1, 0x7f0a0917

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/bt;->k:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 602
    iget-object v0, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v0, :cond_1

    .line 603
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 604
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 605
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v2, 0x0

    invoke-interface {v1, p4, v2, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 607
    :cond_1
    return-object p4

    .line 591
    :cond_2
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/bt;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/bt;->d:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method private f(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 613
    if-nez p4, :cond_1

    .line 614
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403a5

    .line 615
    invoke-virtual {v0, v1, p5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 616
    new-instance v3, Lcom/twitter/android/bt$a;

    const v0, 0x7f1307e9

    .line 617
    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1307eb

    .line 618
    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f1307ea

    .line 619
    invoke-virtual {p4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/android/bt$a;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;)V

    .line 620
    invoke-virtual {p4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v3

    .line 624
    :goto_0
    const v1, 0x7f0a07eb

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget-object v3, v3, Lcom/twitter/model/search/d;->d:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 626
    iget-object v2, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget-object v2, v2, Lcom/twitter/model/search/d;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 627
    iget-object v3, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget-object v3, v3, Lcom/twitter/model/search/d;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v2

    .line 628
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 629
    new-instance v1, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v5, 0x2

    invoke-direct {v1, p1, v5}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4, v1, v2, v3, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 631
    iput-object p3, v0, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 632
    iget-object v1, v0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    iget-object v2, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget-object v2, v2, Lcom/twitter/model/search/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 633
    iget-object v1, v0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 634
    iget-object v1, v0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/bt;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 635
    iget-object v0, v0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    iget-object v1, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    invoke-virtual {v1}, Lcom/twitter/model/search/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 636
    iget-object v0, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v0, :cond_0

    .line 637
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v7}, Landroid/os/Bundle;-><init>(I)V

    .line 638
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 639
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v2, 0x0

    invoke-interface {v1, p4, v2, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 641
    :cond_0
    return-object p4

    .line 622
    :cond_1
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/bt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/bt;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/bt;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/twitter/android/bt;->m:I

    return v0
.end method

.method private g(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 647
    if-nez p4, :cond_2

    .line 648
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403a6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 650
    new-instance v1, Lcom/twitter/android/bt$a;

    const v0, 0x7f13011b

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Lcom/twitter/android/bt$a;-><init>(Landroid/widget/TextView;)V

    .line 651
    invoke-virtual {p4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 655
    :goto_0
    iput-object p3, v0, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 656
    iget-object v1, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget v1, v1, Lcom/twitter/model/search/d;->b:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 657
    iget-object v0, v0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    const v1, 0x7f0a07f0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 661
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    if-eqz v0, :cond_1

    .line 662
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 663
    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 664
    iget-object v1, p0, Lcom/twitter/android/bt;->e:Lcom/twitter/android/av;

    const/4 v2, 0x0

    invoke-interface {v1, p4, v2, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 666
    :cond_1
    return-object p4

    .line 653
    :cond_2
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    goto :goto_0

    .line 658
    :cond_3
    iget-object v1, p3, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    iget v1, v1, Lcom/twitter/model/search/d;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 659
    iget-object v0, v0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    const v1, 0x7f0a07f1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method static synthetic h(Lcom/twitter/android/bt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/bt;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/bt;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/bt;->n:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method


# virtual methods
.method public a(J)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 185
    .line 187
    invoke-virtual {p0}, Lcom/twitter/android/bt;->g()Lcbi;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v0}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bu;

    .line 190
    iget v0, v0, Lcom/twitter/android/bu;->c:I

    int-to-long v4, v0

    cmp-long v0, v4, p1

    if-nez v0, :cond_1

    move v2, v1

    .line 198
    :cond_0
    return v2

    .line 194
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 195
    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lcom/twitter/android/bu;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1271
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    check-cast p2, Lcom/twitter/android/bu;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/bt;->a(Landroid/content/Context;Lcom/twitter/android/bu;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 162
    iput-object p1, p0, Lcom/twitter/android/bt;->u:Landroid/database/Cursor;

    .line 169
    iget-object v0, p0, Lcom/twitter/android/bt;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/e;

    .line 170
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/twitter/android/client/e;->a(Landroid/database/Cursor;)Lcbi;

    goto :goto_0

    .line 172
    :cond_0
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/bu;)V
    .locals 0

    .prologue
    .line 1279
    return-void
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 87
    check-cast p3, Lcom/twitter/android/bu;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/bt;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/bu;)V

    return-void
.end method

.method public a(Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/twitter/android/bt;->q:Lcom/twitter/library/view/d;

    .line 179
    iput-object p2, p0, Lcom/twitter/android/bt;->r:Lcom/twitter/library/view/d;

    .line 180
    iput-object p3, p0, Lcom/twitter/android/bt;->s:Lcom/twitter/library/view/d;

    .line 181
    iput-object p4, p0, Lcom/twitter/android/bt;->t:Lcom/twitter/library/view/d;

    .line 182
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 893
    iget-boolean v0, p0, Lcom/twitter/android/bt;->v:Z

    if-eq v0, p1, :cond_0

    .line 894
    iput-boolean p1, p0, Lcom/twitter/android/bt;->v:Z

    .line 895
    iget-boolean v0, p0, Lcom/twitter/android/bt;->v:Z

    if-nez v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/twitter/android/bt;->i:Lcom/twitter/util/collection/ReferenceList;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cu;

    .line 897
    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->k()V

    goto :goto_0

    .line 901
    :cond_0
    return-void
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 246
    invoke-virtual {p0, p1}, Lcom/twitter/android/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bu;

    .line 247
    iget v0, v0, Lcom/twitter/android/bu;->b:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v4, 0x7f13011b

    const v2, 0x7f0a0107

    const/4 v5, 0x0

    .line 684
    invoke-virtual {p0, p1}, Lcom/twitter/android/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/bu;

    .line 685
    iget-object v1, p0, Lcom/twitter/android/bt;->b:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 688
    iget v0, v3, Lcom/twitter/android/bu;->b:I

    packed-switch v0, :pswitch_data_0

    .line 813
    :pswitch_0
    invoke-direct {p0, v3, p2, p3}, Lcom/twitter/android/bt;->a(Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    :goto_0
    move-object v0, v1

    .line 818
    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    iget v2, v3, Lcom/twitter/android/bu;->f:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/bt;->a(Lcom/twitter/internal/android/widget/GroupedRowView;I)V

    .line 819
    return-object v1

    :pswitch_1
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    .line 693
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bt;->a(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 694
    goto :goto_0

    :pswitch_2
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    .line 697
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bt;->b(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 698
    goto :goto_0

    :pswitch_3
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    .line 701
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bt;->c(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 702
    goto :goto_0

    .line 705
    :pswitch_4
    const v0, 0x7f0a07f5

    .line 706
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v4, p0

    move-object v5, v1

    move-object v6, v3

    move-object v7, p2

    move-object v8, p3

    .line 705
    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/bt;->a(Landroid/content/Context;Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 707
    goto :goto_0

    .line 710
    :pswitch_5
    const v0, 0x7f0a07dd

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/bt;->k:Ljava/lang/String;

    aput-object v4, v2, v5

    .line 711
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object v4, p0

    move-object v5, v1

    move-object v6, v3

    move-object v7, p2

    move-object v8, p3

    .line 710
    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/bt;->a(Landroid/content/Context;Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 712
    goto :goto_0

    .line 715
    :pswitch_6
    const v6, 0x7f040128

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bt;->a(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 717
    goto :goto_0

    .line 720
    :pswitch_7
    const v0, 0x7f0a07f6

    .line 721
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v4, p0

    move-object v5, v1

    move-object v6, v3

    move-object v7, p2

    move-object v8, p3

    .line 720
    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/bt;->a(Landroid/content/Context;Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 722
    goto :goto_0

    .line 726
    :pswitch_8
    if-nez p2, :cond_0

    .line 727
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040125

    .line 728
    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 729
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a0750

    .line 730
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 731
    new-instance v1, Lcom/twitter/android/bt$a;

    invoke-direct {v1}, Lcom/twitter/android/bt$a;-><init>()V

    .line 732
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p2, v0

    move-object v0, v1

    .line 737
    :goto_1
    iput-object v3, v0, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    move-object v1, p2

    .line 739
    goto/16 :goto_0

    .line 735
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    goto :goto_1

    .line 743
    :pswitch_9
    iget-object v0, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    if-eqz v0, :cond_3

    .line 744
    iget-object v0, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    iget-boolean v0, v0, Lcom/twitter/model/search/a;->b:Z

    if-eqz v0, :cond_1

    .line 745
    const v0, 0x7f0a07f3

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    :goto_2
    move-object v4, p0

    move-object v5, v1

    move-object v6, v3

    move-object v7, p2

    move-object v8, p3

    .line 754
    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/bt;->a(Landroid/content/Context;Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 755
    goto/16 :goto_0

    .line 746
    :cond_1
    iget-object v0, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    iget-boolean v0, v0, Lcom/twitter/model/search/a;->c:Z

    if-eqz v0, :cond_2

    .line 747
    const v0, 0x7f0a07f4

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 749
    :cond_2
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 752
    :cond_3
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    :pswitch_a
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    .line 758
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bt;->d(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 759
    goto/16 :goto_0

    :pswitch_b
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    .line 762
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bt;->e(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 763
    goto/16 :goto_0

    :pswitch_c
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    .line 766
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bt;->f(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 767
    goto/16 :goto_0

    :pswitch_d
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    .line 770
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bt;->g(Landroid/content/Context;ILcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 771
    goto/16 :goto_0

    .line 775
    :pswitch_e
    if-nez p2, :cond_4

    .line 776
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040130

    .line 777
    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 778
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a07db

    .line 779
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 780
    new-instance v1, Lcom/twitter/android/bt$a;

    invoke-direct {v1}, Lcom/twitter/android/bt$a;-><init>()V

    .line 781
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p2, v0

    move-object v0, v1

    .line 786
    :goto_3
    iput-object v3, v0, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    move-object v1, p2

    .line 788
    goto/16 :goto_0

    .line 784
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    goto :goto_3

    .line 792
    :pswitch_f
    iget-object v9, p0, Lcom/twitter/android/bt;->u:Landroid/database/Cursor;

    .line 793
    iget v0, v3, Lcom/twitter/android/bu;->d:I

    invoke-interface {v9, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 794
    sget v0, Lbtv;->s:I

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 795
    packed-switch v0, :pswitch_data_1

    .line 805
    :cond_5
    :goto_4
    if-eqz p2, :cond_6

    move-object v1, p2

    .line 806
    goto/16 :goto_0

    :pswitch_10
    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, v3

    move-object v8, v1

    .line 797
    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/bt;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/bu;Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;

    move-result-object p2

    goto :goto_4

    .line 808
    :cond_6
    const/4 v0, 0x0

    invoke-direct {p0, v3, v0, p3}, Lcom/twitter/android/bt;->a(Lcom/twitter/android/bu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    .line 810
    goto/16 :goto_0

    .line 688
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_b
        :pswitch_a
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_f
        :pswitch_c
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_f
        :pswitch_d
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_e
    .end packed-switch

    .line 795
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_10
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 241
    const/16 v0, 0x16

    return v0
.end method
