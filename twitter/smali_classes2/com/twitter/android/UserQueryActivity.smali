.class public abstract Lcom/twitter/android/UserQueryActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/cz;

.field protected b:J

.field protected c:Ljava/lang/String;

.field d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/service/s;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const v2, 0x7f0a09cd

    .line 101
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 102
    packed-switch p2, :pswitch_data_0

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 104
    :pswitch_0
    iput-boolean v1, p0, Lcom/twitter/android/UserQueryActivity;->d:Z

    .line 106
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 107
    sparse-switch v0, :sswitch_data_0

    move v0, v2

    .line 137
    :goto_1
    if-lez v0, :cond_0

    .line 138
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->finish()V

    goto :goto_0

    .line 109
    :sswitch_0
    check-cast p1, Lbio;

    iget-object v0, p1, Lbio;->a:Lcom/twitter/model/core/TwitterUser;

    .line 110
    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p0, v0}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    move v0, v1

    .line 112
    goto :goto_1

    :cond_1
    move v0, v2

    .line 116
    goto :goto_1

    .line 119
    :sswitch_1
    const v0, 0x7f0a09ce

    .line 120
    goto :goto_1

    .line 123
    :sswitch_2
    check-cast p1, Lbio;

    iget-object v0, p1, Lbio;->b:Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    .line 124
    const/16 v1, 0x3f

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 125
    const v0, 0x7f0a0945

    goto :goto_1

    :cond_2
    move v0, v2

    .line 129
    goto :goto_1

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 107
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x193 -> :sswitch_2
        0x194 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 41
    if-eqz p1, :cond_0

    .line 42
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iput-wide v0, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    .line 43
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    .line 45
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/cz;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/twitter/android/cz;

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/cz;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;I)V

    iput-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/cz;

    .line 70
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/cz;

    new-instance v1, Lcom/twitter/android/UserQueryActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/UserQueryActivity$1;-><init>(Lcom/twitter/android/UserQueryActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/cz;->a(Lcom/twitter/android/cz$a;)V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/cz;

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/cz;->a(J)V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/cz;

    iget-object v1, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/cz;->a(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/cz;

    iget-wide v2, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/cz;->b(J)V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/cz;

    invoke-virtual {v0}, Lcom/twitter/android/cz;->a()V

    .line 89
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 49
    if-eqz p1, :cond_0

    .line 50
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    .line 51
    const-string/jumbo v0, "username"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    .line 57
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 58
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 54
    const-string/jumbo v1, "user_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    .line 55
    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method protected o_()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 92
    iget-boolean v0, p0, Lcom/twitter/android/UserQueryActivity;->d:Z

    if-nez v0, :cond_0

    .line 93
    iput-boolean v7, p0, Lcom/twitter/android/UserQueryActivity;->d:Z

    .line 94
    new-instance v1, Lbio;

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    iget-object v6, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lbio;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    .line 95
    invoke-virtual {p0, v1, v7}, Lcom/twitter/android/UserQueryActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 97
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 63
    const-string/jumbo v0, "user_id"

    iget-wide v2, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 64
    const-string/jumbo v0, "username"

    iget-object v1, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method
