.class public Lcom/twitter/android/client/c;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    .line 52
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 300
    return-void
.end method

.method private varargs a(I[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 294
    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 295
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 296
    return-void
.end method

.method private a(Lbhq;Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 5

    .prologue
    const v4, 0x7f0a09df

    const/high16 v3, 0x10000000

    .line 245
    invoke-virtual {p1}, Lbhq;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 246
    invoke-virtual {p1}, Lbhq;->g()[I

    move-result-object v1

    .line 247
    const/16 v2, 0x193

    if-ne v0, v2, :cond_a

    .line 248
    const/16 v0, 0xe2

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DialogActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "blocked_spammer_follow"

    .line 251
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 252
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 249
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    const/16 v0, 0xe1

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DialogActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "blocked_automated_spammer"

    .line 256
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 257
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 254
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 258
    :cond_2
    const/16 v0, 0xa2

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 259
    const v0, 0x7f0a09e0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    .line 260
    :cond_3
    const/16 v0, 0x158

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 261
    invoke-static {p2}, Lcom/twitter/android/dialog/RateLimitDialogFragmentActivity;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 262
    :cond_4
    const/16 v0, 0xa1

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 263
    invoke-static {p2}, Lcom/twitter/android/dialog/FollowingExceededDialogFragmentActivity;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 264
    :cond_5
    const/16 v0, 0xa0

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 265
    const v0, 0x7f0a09de

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    .line 266
    :cond_6
    const/16 v0, 0xfa

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 267
    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/z;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 268
    const v0, 0x7f0a0053

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    .line 270
    :cond_7
    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 272
    const-string/jumbo v1, "user_id"

    invoke-virtual {p1}, Lbhq;->t()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 274
    invoke-virtual {p1}, Lbhq;->u()Lcgi;

    move-result-object v1

    .line 275
    if-eqz v1, :cond_8

    .line 276
    const-string/jumbo v2, "impression_id"

    iget-object v3, v1, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string/jumbo v2, "earned"

    invoke-virtual {v1}, Lcgi;->c()Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 279
    :cond_8
    const-string/jumbo v1, "age_before_timestamp"

    invoke-virtual {p1}, Lbhq;->h()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 280
    invoke-static {p2}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/twitter/android/client/l;->a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    .line 284
    :cond_9
    invoke-direct {p0, v4}, Lcom/twitter/android/client/c;->a(I)V

    .line 285
    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v0

    .line 286
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a([I)V

    goto/16 :goto_0

    .line 289
    :cond_a
    invoke-direct {p0, v4}, Lcom/twitter/android/client/c;->a(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const v8, 0x7f0a00aa

    const/4 v7, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 92
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 93
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    .line 94
    if-nez v2, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v2, v2, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v5

    .line 98
    if-eqz v5, :cond_0

    .line 101
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 108
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/library/service/u;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 109
    iget-object v2, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v2

    .line 110
    invoke-virtual {v2, v5, v0}, Lcom/twitter/android/client/z;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/service/u;)V

    move v2, v3

    .line 116
    :goto_1
    instance-of v6, p1, Lbfa;

    if-eqz v6, :cond_4

    .line 117
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    const v0, 0x7f0a05b9

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    :cond_2
    move v2, v4

    .line 113
    goto :goto_1

    .line 119
    :cond_3
    if-nez v2, :cond_0

    .line 120
    const v0, 0x7f0a05b4

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    .line 122
    :cond_4
    instance-of v6, p1, Lbff;

    if-eqz v6, :cond_6

    .line 123
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 124
    const v0, 0x7f0a09bc

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    .line 125
    :cond_5
    if-nez v2, :cond_0

    .line 126
    const v0, 0x7f0a09ba

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    .line 128
    :cond_6
    instance-of v6, p1, Lbes;

    if-eqz v6, :cond_c

    .line 129
    check-cast p1, Lbes;

    .line 130
    invoke-virtual {p1}, Lbes;->L()I

    move-result v1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_8

    .line 131
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 132
    const v0, 0x7f0a09ab

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto :goto_0

    .line 133
    :cond_7
    if-nez v2, :cond_0

    .line 134
    const v0, 0x7f0a0a02

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    .line 135
    invoke-virtual {p1}, Lbes;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Ljava/lang/String;)Lcom/twitter/android/client/g;

    move-result-object v0

    iget-wide v2, p1, Lbes;->b:J

    .line 136
    invoke-virtual {v0, v2, v3, v7}, Lcom/twitter/android/client/g;->a(JI)V

    goto/16 :goto_0

    .line 138
    :cond_8
    invoke-virtual {p1}, Lbes;->L()I

    move-result v1

    if-ne v1, v3, :cond_a

    .line 139
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 140
    invoke-direct {p0, v8}, Lcom/twitter/android/client/c;->a(I)V

    goto/16 :goto_0

    .line 141
    :cond_9
    if-nez v2, :cond_0

    .line 142
    const v0, 0x7f0a09d9

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    .line 143
    invoke-virtual {p1}, Lbes;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Ljava/lang/String;)Lcom/twitter/android/client/g;

    move-result-object v0

    iget-wide v2, p1, Lbes;->b:J

    .line 144
    invoke-virtual {v0, v2, v3, v7}, Lcom/twitter/android/client/g;->b(JI)V

    goto/16 :goto_0

    .line 146
    :cond_a
    invoke-virtual {p1}, Lbes;->L()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 147
    iget-boolean v1, p1, Lbes;->g:Z

    .line 148
    iget-object v3, p1, Lbes;->j:Ljava/lang/String;

    .line 149
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 150
    const-string/jumbo v0, "abuse"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    if-eqz v1, :cond_0

    .line 152
    invoke-direct {p0, v8}, Lcom/twitter/android/client/c;->a(I)V

    goto/16 :goto_0

    .line 155
    :cond_b
    if-nez v2, :cond_0

    .line 156
    const v0, 0x7f0a09f8

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->a(I)V

    goto/16 :goto_0

    .line 159
    :cond_c
    instance-of v6, p1, Lbhq;

    if-eqz v6, :cond_e

    move-object v1, p1

    .line 160
    check-cast v1, Lbhq;

    .line 162
    iget-object v6, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    .line 163
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 164
    invoke-virtual {v1}, Lbhq;->s()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lbhq;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const v0, 0x7f0a09e1

    new-array v2, v3, [Ljava/lang/Object;

    .line 167
    invoke-virtual {v1}, Lbhq;->s()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    .line 166
    invoke-direct {p0, v0, v2}, Lcom/twitter/android/client/c;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 171
    :cond_d
    invoke-static {v5}, Lcom/twitter/android/client/h;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/g;

    move-result-object v0

    .line 172
    invoke-virtual {v1}, Lbhq;->t()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9, v3}, Lcom/twitter/android/client/g;->b(JI)V

    .line 174
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    .line 175
    invoke-direct {p0, v1, v6, v5}, Lcom/twitter/android/client/c;->a(Lbhq;Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    .line 178
    :cond_e
    instance-of v6, p1, Lbio;

    if-eqz v6, :cond_f

    .line 179
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    check-cast p1, Lbio;

    .line 181
    iget-object v0, p1, Lbio;->a:Lcom/twitter/model/core/TwitterUser;

    .line 182
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 183
    invoke-virtual {v5, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 184
    invoke-static {v0}, Lcom/twitter/library/util/b;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto/16 :goto_0

    .line 187
    :cond_f
    instance-of v6, p1, Lbhs;

    if-eqz v6, :cond_10

    .line 188
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    invoke-static {v5}, Lcom/twitter/android/client/h;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/g;

    move-result-object v0

    check-cast p1, Lbhs;

    .line 190
    invoke-virtual {p1}, Lbhs;->g()J

    move-result-wide v4

    .line 189
    invoke-virtual {v0, v4, v5, v3}, Lcom/twitter/android/client/g;->a(JI)V

    goto/16 :goto_0

    .line 192
    :cond_10
    instance-of v6, p1, Lbhx;

    if-eqz v6, :cond_11

    .line 193
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    check-cast p1, Lbhx;

    .line 195
    iget-object v0, p1, Lbhx;->b:Lcom/twitter/model/profile/ExtendedProfile;

    .line 196
    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 197
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-wide v2, v0, Lcom/twitter/model/profile/ExtendedProfile;->b:J

    iget-wide v6, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    .line 198
    new-instance v2, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v2, v1}, Lcom/twitter/model/core/TwitterUser$a;-><init>(Lcom/twitter/model/core/TwitterUser;)V

    .line 199
    invoke-virtual {v2, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/ExtendedProfile;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 200
    invoke-virtual {v5, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 201
    invoke-static {v0}, Lcom/twitter/library/util/b;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto/16 :goto_0

    .line 204
    :cond_11
    instance-of v6, p1, Lbhd;

    if-eqz v6, :cond_12

    .line 205
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_0

    if-nez v2, :cond_0

    .line 206
    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v0

    check-cast p1, Lbhd;

    .line 208
    invoke-virtual {p1}, Lbhd;->g()[I

    move-result-object v1

    .line 207
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a([I)V

    goto/16 :goto_0

    .line 210
    :cond_12
    instance-of v6, p1, Lbfc;

    if-eqz v6, :cond_13

    .line 211
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    .line 212
    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    const v1, 0x7f0a098a

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 215
    :cond_13
    instance-of v2, p1, Lbhr;

    if-eqz v2, :cond_14

    .line 216
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    invoke-static {v5}, Lcom/twitter/android/client/h;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/g;

    move-result-object v0

    .line 219
    check-cast p1, Lbhr;

    invoke-virtual {p1}, Lbhr;->a()[J

    move-result-object v1

    .line 220
    array-length v2, v1

    :goto_2
    if-ge v4, v2, :cond_0

    aget-wide v6, v1, v4

    .line 221
    invoke-virtual {v0, v6, v7, v3}, Lcom/twitter/android/client/g;->b(JI)V

    .line 220
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 224
    :cond_14
    instance-of v2, p1, Lbaj;

    if-eqz v2, :cond_0

    .line 225
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 227
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "extra_settings"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ClientConfiguration;

    .line 229
    if-eqz v0, :cond_16

    iget-object v2, v0, Lcom/twitter/library/api/ClientConfiguration;->a:Lcom/twitter/model/client/UrlConfiguration;

    .line 231
    :goto_3
    if-eqz v2, :cond_15

    .line 232
    iget-object v3, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/twitter/android/client/y;->a(Landroid/content/Context;)Lcom/twitter/android/client/y;

    move-result-object v3

    .line 233
    invoke-virtual {v3, v2}, Lcom/twitter/android/client/y;->a(Lcom/twitter/model/client/UrlConfiguration;)V

    .line 235
    :cond_15
    if-eqz v0, :cond_17

    iget-object v0, v0, Lcom/twitter/library/api/ClientConfiguration;->b:Lcom/twitter/library/api/v;

    .line 237
    :goto_4
    iget-object v1, p0, Lcom/twitter/android/client/c;->a:Landroid/content/Context;

    invoke-static {v1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    .line 238
    invoke-virtual {v1, v0}, Lbaa;->a(Lcom/twitter/library/api/v;)V

    goto/16 :goto_0

    :cond_16
    move-object v2, v1

    .line 229
    goto :goto_3

    :cond_17
    move-object v0, v1

    .line 235
    goto :goto_4
.end method

.method public bridge synthetic b(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public b(Lcom/twitter/library/service/s;)V
    .locals 7

    .prologue
    const/4 v4, 0x4

    const/4 v6, 0x1

    .line 56
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 58
    instance-of v1, p1, Lbes;

    if-eqz v1, :cond_3

    .line 59
    check-cast p1, Lbes;

    .line 60
    invoke-virtual {p1}, Lbes;->L()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 61
    invoke-virtual {p1}, Lbes;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Ljava/lang/String;)Lcom/twitter/android/client/g;

    move-result-object v0

    iget-wide v2, p1, Lbes;->b:J

    .line 62
    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/g;->b(JI)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {p1}, Lbes;->L()I

    move-result v0

    if-eq v0, v6, :cond_2

    iget-boolean v0, p1, Lbes;->g:Z

    if-eqz v0, :cond_0

    .line 65
    :cond_2
    invoke-virtual {p1}, Lbes;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Ljava/lang/String;)Lcom/twitter/android/client/g;

    move-result-object v0

    iget-wide v2, p1, Lbes;->b:J

    .line 66
    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/g;->a(JI)V

    goto :goto_0

    .line 68
    :cond_3
    instance-of v1, p1, Lbhq;

    if-eqz v1, :cond_4

    .line 69
    if-eqz v0, :cond_0

    .line 70
    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/g;

    move-result-object v0

    check-cast p1, Lbhq;

    .line 71
    invoke-virtual {p1}, Lbhq;->t()J

    move-result-wide v2

    .line 70
    invoke-virtual {v0, v2, v3, v6}, Lcom/twitter/android/client/g;->a(JI)V

    goto :goto_0

    .line 73
    :cond_4
    instance-of v1, p1, Lbhs;

    if-eqz v1, :cond_5

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/g;

    move-result-object v0

    check-cast p1, Lbhs;

    .line 76
    invoke-virtual {p1}, Lbhs;->g()J

    move-result-wide v2

    .line 75
    invoke-virtual {v0, v2, v3, v6}, Lcom/twitter/android/client/g;->b(JI)V

    goto :goto_0

    .line 78
    :cond_5
    instance-of v1, p1, Lbhr;

    if-eqz v1, :cond_0

    .line 79
    if-eqz v0, :cond_0

    .line 81
    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/g;

    move-result-object v1

    .line 82
    check-cast p1, Lbhr;

    invoke-virtual {p1}, Lbhr;->a()[J

    move-result-object v2

    .line 83
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    aget-wide v4, v2, v0

    .line 84
    invoke-virtual {v1, v4, v5, v6}, Lcom/twitter/android/client/g;->a(JI)V

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
