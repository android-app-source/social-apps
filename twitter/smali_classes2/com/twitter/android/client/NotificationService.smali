.class public Lcom/twitter/android/client/NotificationService;
.super Landroid/app/Service;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/NotificationService$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static final h:Ljava/lang/String;

.field public static final i:Ljava/lang/String;

.field public static final j:Ljava/lang/String;

.field private static final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private l:Lcom/twitter/android/client/NotificationService$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.dismiss"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->a:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.undo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->b:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.reply"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->c:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.retweet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->d:Ljava/lang/String;

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.favorite"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->e:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.follow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->f:Ljava/lang/String;

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.follow.accept"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.follow.decline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.tweetTo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->i:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".notif.open"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->j:Ljava/lang/String;

    .line 115
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->a:Ljava/lang/String;

    const/4 v2, 0x0

    .line 116
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->b:Ljava/lang/String;

    const/4 v2, 0x1

    .line 117
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->c:Ljava/lang/String;

    .line 118
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->d:Ljava/lang/String;

    .line 119
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->e:Ljava/lang/String;

    .line 120
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->f:Ljava/lang/String;

    .line 121
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    .line 122
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->h:Ljava/lang/String;

    .line 123
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->i:Ljava/lang/String;

    .line 124
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/NotificationService;->j:Ljava/lang/String;

    const/4 v2, 0x3

    .line 125
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/android/client/NotificationService;->k:Ljava/util/Map;

    .line 115
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a(Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 270
    :try_start_0
    invoke-virtual {p1}, Landroid/app/PendingIntent;->send()V

    .line 274
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/NotificationService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 381
    const-string/jumbo v0, "sb_account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 382
    invoke-static {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/StatusBarNotif;

    move-result-object v1

    .line 385
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/android/b;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 386
    const-string/jumbo v0, "notif_scribe_log"

    .line 390
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 391
    if-eqz v0, :cond_0

    .line 392
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 395
    :cond_0
    if-eqz v1, :cond_1

    .line 396
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->y()[I

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/l;->a([IJ)V

    .line 398
    :cond_1
    return-void

    .line 388
    :cond_2
    const-string/jumbo v0, "notif_scribe_log_from_background"

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 281
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    const-string/jumbo v1, "sb_account_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 282
    const/4 v1, 0x0

    .line 283
    sget-object v0, Lcom/twitter/android/client/NotificationService;->e:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    new-instance v1, Lbfm;

    const-string/jumbo v0, "status_id"

    .line 285
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string/jumbo v0, "rt_status_id"

    .line 286
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lbfm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 302
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 303
    const-string/jumbo v0, "Notification actions are triggered by user actions. The app may or may not be visible."

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/s;->m(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 305
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 307
    :cond_1
    return-void

    .line 287
    :cond_2
    sget-object v0, Lcom/twitter/android/client/NotificationService;->f:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 288
    const-string/jumbo v0, "user_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 289
    new-instance v1, Lbhq;

    const/4 v6, 0x0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    goto :goto_0

    .line 290
    :cond_3
    sget-object v0, Lcom/twitter/android/client/NotificationService;->d:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 291
    const-string/jumbo v0, "status_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 292
    const-string/jumbo v0, "ref_status_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 293
    new-instance v1, Lbhd;

    const/4 v8, 0x0

    move-object v2, p1

    invoke-direct/range {v1 .. v8}, Lbhd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcgi;)V

    goto :goto_0

    .line 294
    :cond_4
    sget-object v0, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/lang/String;

    .line 295
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    :cond_5
    const-string/jumbo v0, "user_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 297
    sget-object v0, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v6, 0x1

    .line 300
    :goto_1
    new-instance v1, Lbia;

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lbia;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    goto :goto_0

    .line 297
    :cond_6
    const/4 v6, 0x2

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;ILcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 6

    .prologue
    .line 311
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v1

    .line 312
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/client/NotificationService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/twitter/android/client/NotificationService;->b:Ljava/lang/String;

    .line 313
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v2, Lcom/twitter/database/schema/a$q;->a:Landroid/net/Uri;

    .line 315
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 314
    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "sb_notification"

    .line 316
    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "notif_scribe_log"

    .line 317
    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 318
    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 320
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f04027a

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 321
    const v3, 0x7f1305ec

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 322
    const v0, 0x7f13007d

    invoke-virtual {v2, v0, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 323
    const v0, 0x7f1300fa

    invoke-virtual {v2, v0, p3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 324
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 325
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->r()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 326
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->q()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 327
    invoke-virtual {v0, p3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 328
    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 329
    const-string/jumbo v0, "notification"

    .line 330
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 331
    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 332
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/NotificationService;Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 135
    new-instance v0, Lcom/twitter/android/client/NotificationService$a;

    invoke-virtual {p0}, Lcom/twitter/android/client/NotificationService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/client/NotificationService$a;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/client/NotificationService;->l:Lcom/twitter/android/client/NotificationService$a;

    .line 136
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 166
    if-nez p1, :cond_0

    .line 167
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Intent is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 168
    invoke-virtual {p0, p3}, Lcom/twitter/android/client/NotificationService;->stopSelf(I)V

    move v0, v7

    .line 265
    :goto_0
    return v0

    .line 171
    :cond_0
    sget-object v0, Lcom/twitter/android/client/NotificationService;->k:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 172
    if-nez v0, :cond_1

    .line 173
    invoke-virtual {p1, v6}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "IMAGE-818: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 176
    invoke-virtual {p0, p3}, Lcom/twitter/android/client/NotificationService;->stopSelf(I)V

    move v0, v7

    .line 177
    goto :goto_0

    .line 179
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 180
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 181
    invoke-static {v3}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v1

    .line 183
    invoke-static {v4}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/StatusBarNotif;

    move-result-object v8

    .line 184
    if-nez v8, :cond_2

    .line 185
    invoke-virtual {p0, p3}, Lcom/twitter/android/client/NotificationService;->stopSelf(I)V

    move v0, v7

    .line 186
    goto :goto_0

    .line 189
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 264
    :cond_3
    :goto_1
    invoke-virtual {p0, p3}, Lcom/twitter/android/client/NotificationService;->stopSelf(I)V

    move v0, v7

    .line 265
    goto :goto_0

    .line 191
    :pswitch_0
    invoke-static {v3, v4}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 192
    instance-of v0, v8, Lcom/twitter/android/client/notifications/StoriesNotif;

    if-eqz v0, :cond_3

    .line 193
    invoke-virtual {v8}, Lcom/twitter/android/client/notifications/StatusBarNotif;->o()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/l;->b(J)V

    goto :goto_1

    .line 198
    :pswitch_1
    invoke-virtual {v8}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v0

    .line 199
    iget-object v3, p0, Lcom/twitter/android/client/NotificationService;->l:Lcom/twitter/android/client/NotificationService$a;

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/NotificationService$a;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 200
    iget-object v3, p0, Lcom/twitter/android/client/NotificationService;->l:Lcom/twitter/android/client/NotificationService$a;

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/NotificationService$a;->removeMessages(I)V

    .line 201
    const-string/jumbo v0, "notif_scribe_log"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 202
    if-eqz v0, :cond_4

    .line 203
    new-array v2, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_undo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 204
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 206
    :cond_4
    invoke-virtual {v1, v8}, Lcom/twitter/android/client/l;->b(Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_1

    .line 212
    :pswitch_2
    const-string/jumbo v0, "notif_scribe_log"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 213
    const-string/jumbo v1, "notif_scribe_action_tap"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 214
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 215
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a()Ljava/lang/String;

    move-result-object v1

    .line 216
    new-array v5, v2, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "_tap"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v6

    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 217
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 218
    new-array v5, v2, [Ljava/lang/String;

    aput-object v1, v5, v6

    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 220
    :cond_5
    const-string/jumbo v1, "action_intent"

    .line 221
    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    .line 222
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 223
    if-eqz v1, :cond_6

    .line 224
    :goto_2
    const-string/jumbo v9, "undo_allowed"

    invoke-virtual {v4, v9, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 225
    if-eqz v2, :cond_7

    .line 226
    invoke-direct {p0, v1}, Lcom/twitter/android/client/NotificationService;->a(Landroid/app/PendingIntent;)V

    goto/16 :goto_1

    :cond_6
    move v2, v6

    .line 223
    goto :goto_2

    .line 227
    :cond_7
    if-eqz v9, :cond_8

    .line 228
    const-string/jumbo v1, "undo_text"

    .line 229
    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "undo_icon"

    .line 230
    invoke-virtual {v4, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 228
    invoke-static {v3, v8, v1, v2, v0}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;ILcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 232
    new-instance v0, Lcom/twitter/android/client/NotificationService$1;

    move-object v1, p0

    move v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/NotificationService$1;-><init>(Lcom/twitter/android/client/NotificationService;ILandroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/twitter/android/client/NotificationService;->l:Lcom/twitter/android/client/NotificationService$a;

    iget-object v2, p0, Lcom/twitter/android/client/NotificationService;->l:Lcom/twitter/android/client/NotificationService$a;

    .line 243
    invoke-virtual {v8}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Lcom/twitter/android/client/NotificationService$a;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x1388

    .line 242
    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/client/NotificationService$a;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 245
    const/4 v0, 0x3

    goto/16 :goto_0

    .line 247
    :cond_8
    invoke-static {v3, v4}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 248
    invoke-direct {p0, v3, v4, v5}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 253
    :pswitch_3
    invoke-static {v3, v4}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 254
    const-string/jumbo v0, "action_intent"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 255
    if-eqz v0, :cond_3

    .line 256
    invoke-direct {p0, v0}, Lcom/twitter/android/client/NotificationService;->a(Landroid/app/PendingIntent;)V

    goto/16 :goto_1

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
