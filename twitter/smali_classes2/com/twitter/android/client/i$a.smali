.class Lcom/twitter/android/client/i$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/client/i;

.field private final b:Lcom/twitter/app/home/HomeTimelineFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/i;Lcom/twitter/app/home/HomeTimelineFragment;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/twitter/android/client/i$a;->a:Lcom/twitter/android/client/i;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 251
    iput-object p2, p0, Lcom/twitter/android/client/i$a;->b:Lcom/twitter/app/home/HomeTimelineFragment;

    .line 252
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 256
    iget-object v1, p0, Lcom/twitter/android/client/i$a;->a:Lcom/twitter/android/client/i;

    .line 257
    invoke-static {v1}, Lcom/twitter/android/client/i;->a(Lcom/twitter/android/client/i;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 258
    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->k()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/twitter/android/client/i$a;->b:Lcom/twitter/app/home/HomeTimelineFragment;

    invoke-virtual {v0}, Lcom/twitter/app/home/HomeTimelineFragment;->aW()V

    .line 265
    iget-object v0, p0, Lcom/twitter/android/client/i$a;->a:Lcom/twitter/android/client/i;

    invoke-static {v0}, Lcom/twitter/android/client/i;->b(Lcom/twitter/android/client/i;)V

    .line 267
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/i$a;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 246
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/i$a;->a(Ljava/lang/Boolean;)V

    return-void
.end method
