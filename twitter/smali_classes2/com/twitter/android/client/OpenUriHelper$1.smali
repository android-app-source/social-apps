.class final Lcom/twitter/android/client/OpenUriHelper$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/android/client/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/client/f;

.field final synthetic b:Lbaa;

.field final synthetic c:J


# direct methods
.method constructor <init>(Lcom/twitter/android/client/f;Lbaa;J)V
    .locals 1

    .prologue
    .line 400
    iput-object p1, p0, Lcom/twitter/android/client/OpenUriHelper$1;->a:Lcom/twitter/android/client/f;

    iput-object p2, p0, Lcom/twitter/android/client/OpenUriHelper$1;->b:Lbaa;

    iput-wide p3, p0, Lcom/twitter/android/client/OpenUriHelper$1;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 403
    const/4 v0, -0x1

    if-ne v0, p2, :cond_2

    .line 404
    check-cast p1, Landroid/app/AlertDialog;

    const v0, 0x7f130335

    .line 405
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    check-cast v0, Landroid/widget/CheckBox;

    .line 406
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$1;->a:Lcom/twitter/android/client/f;

    invoke-interface {v0}, Lcom/twitter/android/client/f;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 428
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$1;->a:Lcom/twitter/android/client/f;

    invoke-interface {v0}, Lcom/twitter/android/client/f;->a()V

    .line 429
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$1;->a:Lcom/twitter/android/client/f;

    invoke-interface {v0}, Lcom/twitter/android/client/f;->b()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 430
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/client/OpenUriHelper$1;->c:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:accept_data:redirect::impression"

    aput-object v2, v1, v5

    .line 431
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 430
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 439
    :cond_1
    :goto_1
    return-void

    .line 409
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$1;->b:Lbaa;

    invoke-virtual {v0, v5, v4}, Lbaa;->a(ZZ)V

    .line 410
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/client/OpenUriHelper$1;->c:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:accept_data:accept::impression"

    aput-object v2, v1, v5

    .line 411
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 410
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 415
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$1;->b:Lbaa;

    .line 416
    invoke-virtual {v0, v5, v4}, Lbaa;->d(ZZ)V

    goto :goto_0

    .line 420
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$1;->b:Lbaa;

    invoke-virtual {v0, v5, v4}, Lbaa;->b(ZZ)V

    goto :goto_0

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$1;->a:Lcom/twitter/android/client/f;

    invoke-interface {v0}, Lcom/twitter/android/client/f;->b()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 435
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/client/OpenUriHelper$1;->c:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:accept_data:close::impression"

    aput-object v2, v1, v5

    .line 436
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 435
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
