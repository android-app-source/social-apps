.class public Lcom/twitter/android/client/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/android/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/d$a;
    }
.end annotation


# instance fields
.field a:J

.field b:J

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/twitter/android/client/d;->c:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/twitter/android/client/d;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 44
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/android/client/d;->a:J

    sub-long/2addr v2, v4

    .line 46
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "app::::become_inactive"

    aput-object v5, v0, v4

    .line 47
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 48
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 49
    iget-object v1, p0, Lcom/twitter/android/client/d;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/az;->a(Landroid/content/Context;)Lcom/twitter/android/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/az;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 50
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/d;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/f;->a(Landroid/content/Context;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()V

    .line 55
    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 60
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 61
    iput-wide v2, p0, Lcom/twitter/android/client/d;->a:J

    .line 63
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "app::::become_active"

    aput-object v5, v1, v4

    .line 64
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 65
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 67
    const-string/jumbo v0, "app_event_track_non_referred_open_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/twitter/android/client/d;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;)V

    .line 73
    :cond_0
    iget-wide v0, p0, Lcom/twitter/android/client/d;->b:J

    sub-long v0, v2, v0

    const-wide/16 v4, 0x7d0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 74
    iput-wide v2, p0, Lcom/twitter/android/client/d;->b:J

    .line 78
    new-instance v0, Lcom/twitter/android/client/d$a;

    iget-object v1, p0, Lcom/twitter/android/client/d;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/android/client/d$a;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 80
    :cond_1
    return-void
.end method
