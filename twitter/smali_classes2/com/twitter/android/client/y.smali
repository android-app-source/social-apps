.class public Lcom/twitter/android/client/y;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/client/y;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/library/client/Session;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/twitter/model/client/UrlConfiguration;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/y;->c:Ljava/util/Map;

    .line 58
    iput-object p1, p0, Lcom/twitter/android/client/y;->b:Landroid/content/Context;

    .line 59
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/android/client/y;
    .locals 3

    .prologue
    .line 45
    const-class v1, Lcom/twitter/android/client/y;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/y;->a:Lcom/twitter/android/client/y;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/twitter/android/client/y;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/client/y;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/client/y;->a:Lcom/twitter/android/client/y;

    .line 47
    const-class v0, Lcom/twitter/android/client/y;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 49
    :cond_0
    sget-object v0, Lcom/twitter/android/client/y;->a:Lcom/twitter/android/client/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c()Lcom/twitter/model/client/UrlConfiguration;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 124
    iget-object v0, p0, Lcom/twitter/android/client/y;->d:Lcom/twitter/model/client/UrlConfiguration;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/twitter/android/client/y;->d:Lcom/twitter/model/client/UrlConfiguration;

    .line 147
    :goto_0
    return-object v0

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/y;->b:Landroid/content/Context;

    const-string/jumbo v1, "config"

    .line 128
    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 130
    const-string/jumbo v1, "short_url_len"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 131
    const-string/jumbo v2, "short_url_len_https"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 132
    const-string/jumbo v3, "scribe_url"

    const-string/jumbo v4, "https://twitter.com/scribe"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 133
    const-string/jumbo v4, "url_whitelist"

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 136
    if-eqz v0, :cond_1

    .line 138
    new-instance v5, Ljava/util/StringTokenizer;

    const-string/jumbo v6, ","

    invoke-direct {v5, v0, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/y;->a()V

    .line 146
    :cond_2
    new-instance v0, Lcom/twitter/model/client/UrlConfiguration;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/model/client/UrlConfiguration;-><init>(IILjava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/client/y;->d:Lcom/twitter/model/client/UrlConfiguration;

    .line 147
    iget-object v0, p0, Lcom/twitter/android/client/y;->d:Lcom/twitter/model/client/UrlConfiguration;

    goto :goto_0
.end method


# virtual methods
.method public a(Z)I
    .locals 3

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/twitter/android/client/y;->c()Lcom/twitter/model/client/UrlConfiguration;

    move-result-object v1

    .line 94
    iget v0, v1, Lcom/twitter/model/client/UrlConfiguration;->a:I

    if-lez v0, :cond_1

    iget v0, v1, Lcom/twitter/model/client/UrlConfiguration;->a:I

    .line 95
    :goto_0
    if-eqz p1, :cond_0

    .line 96
    iget v2, v1, Lcom/twitter/model/client/UrlConfiguration;->b:I

    if-lez v2, :cond_2

    iget v0, v1, Lcom/twitter/model/client/UrlConfiguration;->b:I

    .line 98
    :cond_0
    :goto_1
    return v0

    .line 94
    :cond_1
    const/16 v0, 0x16

    goto :goto_0

    .line 96
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a()V
    .locals 8

    .prologue
    .line 77
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 78
    iget-object v0, p0, Lcom/twitter/android/client/y;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 79
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 80
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/32 v6, 0xea60

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/y;->c:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lcom/twitter/android/client/y;->b:Landroid/content/Context;

    invoke-static {v0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lbaa;->m()Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    new-instance v3, Lbaj;

    iget-object v4, p0, Lcom/twitter/android/client/y;->b:Landroid/content/Context;

    invoke-direct {v3, v4, v1, v0}, Lbaj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 85
    invoke-virtual {v2, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 87
    :cond_1
    return-void
.end method

.method a(Lcom/twitter/model/client/UrlConfiguration;)V
    .locals 5

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/client/y;->b:Landroid/content/Context;

    const-string/jumbo v1, "config"

    const/4 v2, 0x0

    .line 111
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 113
    iput-object p1, p0, Lcom/twitter/android/client/y;->d:Lcom/twitter/model/client/UrlConfiguration;

    .line 114
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "short_url_len"

    iget v2, p1, Lcom/twitter/model/client/UrlConfiguration;->a:I

    .line 115
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "short_url_len_https"

    iget v2, p1, Lcom/twitter/model/client/UrlConfiguration;->b:I

    .line 116
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "url_whitelist"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ","

    iget-object v4, p1, Lcom/twitter/model/client/UrlConfiguration;->d:Ljava/util/List;

    .line 117
    invoke-static {v3, v4}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "scribe_url"

    iget-object v2, p1, Lcom/twitter/model/client/UrlConfiguration;->c:Ljava/lang/String;

    .line 118
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 119
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 120
    return-void
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/twitter/android/client/y;->c()Lcom/twitter/model/client/UrlConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/client/UrlConfiguration;->d:Ljava/util/List;

    return-object v0
.end method
