.class public Lcom/twitter/android/client/v;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/v$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lbaa;

.field private final c:Lcom/twitter/android/client/v$a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lbaa;Lcom/twitter/android/client/v$a;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/twitter/android/client/v;->a:Landroid/app/Activity;

    .line 28
    iput-object p2, p0, Lcom/twitter/android/client/v;->b:Lbaa;

    .line 29
    iput-object p3, p0, Lcom/twitter/android/client/v;->c:Lcom/twitter/android/client/v$a;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/v;)Lbaa;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/client/v;->b:Lbaa;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)Lcom/twitter/android/client/v;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/android/client/v;

    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/v$a;

    invoke-direct {v2}, Lcom/twitter/android/client/v$a;-><init>()V

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/client/v;-><init>(Landroid/app/Activity;Lbaa;Lcom/twitter/android/client/v$a;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/client/v;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/client/v;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/client/v;)Lcom/twitter/android/client/v$a;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/client/v;->c:Lcom/twitter/android/client/v$a;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/android/client/v$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/v$1;-><init>(Lcom/twitter/android/client/v;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/client/v;->b:Lbaa;

    invoke-virtual {v0}, Lbaa;->b()Z

    move-result v0

    return v0
.end method
