.class public Lcom/twitter/android/client/x;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/x$c;,
        Lcom/twitter/android/client/x$b;,
        Lcom/twitter/android/client/x$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/client/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/twitter/android/client/x;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/n;
    .locals 3

    .prologue
    .line 49
    sget-object v1, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 50
    :try_start_0
    sget-object v0, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/n;

    monitor-exit v1

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;Lcom/twitter/android/client/x$a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;Lcom/twitter/android/client/x$a;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lcom/twitter/android/client/notifications/c;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/notifications/c;-><init>(Landroid/content/Context;)V

    .line 97
    const v1, 0x7f0a0606

    invoke-virtual {v0, v1, p1}, Lcom/twitter/android/client/notifications/c;->a(ILcom/twitter/library/client/Session;)V

    .line 99
    new-instance v0, Lcom/twitter/library/api/upload/r;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/api/upload/r;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 100
    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/r;->a(Lcom/twitter/library/client/n;)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/x;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 101
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/k;->c(I)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    const/4 v1, 0x1

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/k;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/r;

    .line 104
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/r;->a()I

    move-result v1

    invoke-static {p1, p2, v1}, Lcom/twitter/android/client/x;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;I)V

    .line 106
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/x$b;

    invoke-direct {v2, p0, p3}, Lcom/twitter/android/client/x$b;-><init>(Landroid/content/Context;Lcom/twitter/android/client/x$a;)V

    .line 107
    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    .line 106
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/twitter/android/client/x$c;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/client/x$c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)V

    .line 123
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 124
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/client/Session;I)V
    .locals 0

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/twitter/android/client/x;->b(Lcom/twitter/library/client/Session;I)V

    return-void
.end method

.method private static a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;I)V
    .locals 3

    .prologue
    .line 55
    iput p2, p1, Lcom/twitter/library/client/n;->n:I

    .line 57
    sget-object v1, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 58
    :try_start_0
    sget-object v0, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    monitor-exit v1

    .line 60
    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Lcom/twitter/library/client/Session;I)V
    .locals 3

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v1

    .line 65
    sget-object v2, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 66
    :try_start_0
    sget-object v0, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/n;

    .line 68
    if-eqz v0, :cond_0

    iget v0, v0, Lcom/twitter/library/client/n;->n:I

    if-ne v0, p1, :cond_0

    .line 69
    sget-object v0, Lcom/twitter/android/client/x;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :cond_0
    monitor-exit v2

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
