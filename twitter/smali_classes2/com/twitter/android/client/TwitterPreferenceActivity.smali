.class public abstract Lcom/twitter/android/client/TwitterPreferenceActivity;
.super Lcom/twitter/app/common/abs/AbsPreferenceActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ae$a;


# instance fields
.field protected d:Lcom/twitter/library/client/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/preference/PreferenceGroup;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    invoke-virtual {p2}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v4

    move v0, v2

    .line 82
    :goto_0
    if-ge v0, v4, :cond_1

    .line 83
    invoke-virtual {p2, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    .line 84
    invoke-virtual {v3}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    .line 86
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 87
    invoke-virtual {p2, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    move v0, v1

    .line 100
    :goto_1
    return v0

    .line 82
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v3, v2

    .line 92
    :goto_2
    if-ge v3, v4, :cond_3

    .line 93
    invoke-virtual {p2, v3}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    .line 94
    instance-of v5, v0, Landroid/preference/PreferenceGroup;

    if-eqz v5, :cond_2

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 95
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->a(Ljava/lang/String;Landroid/preference/PreferenceGroup;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 96
    goto :goto_1

    .line 92
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 100
    goto :goto_1
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    return v0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f0402c0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->setContentView(I)V

    .line 46
    return-void
.end method

.method protected b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->a(Ljava/lang/String;Landroid/preference/PreferenceGroup;)Z

    move-result v0

    return v0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->b()V

    .line 38
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->b()V

    .line 32
    new-instance v0, Lcom/twitter/android/an;

    invoke-direct {v0, p0}, Lcom/twitter/android/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/client/TwitterPreferenceActivity;->d:Lcom/twitter/library/client/e;

    .line 33
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->onPause()V

    .line 59
    iget-object v0, p0, Lcom/twitter/android/client/TwitterPreferenceActivity;->d:Lcom/twitter/library/client/e;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/twitter/android/client/TwitterPreferenceActivity;->d:Lcom/twitter/library/client/e;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->b(Lcom/twitter/library/client/e;)V

    .line 62
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->onResume()V

    .line 51
    iget-object v0, p0, Lcom/twitter/android/client/TwitterPreferenceActivity;->d:Lcom/twitter/library/client/e;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/twitter/android/client/TwitterPreferenceActivity;->d:Lcom/twitter/library/client/e;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(Lcom/twitter/library/client/e;)V

    .line 54
    :cond_0
    return-void
.end method
