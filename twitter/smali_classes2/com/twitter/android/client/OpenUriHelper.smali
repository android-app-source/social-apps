.class public Lcom/twitter/android/client/OpenUriHelper;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/OpenUriHelper$a;,
        Lcom/twitter/android/client/OpenUriHelper$b;,
        Lcom/twitter/android/client/OpenUriHelper$LinkHandler;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Lcom/twitter/android/client/OpenUriHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 74
    const-string/jumbo v0, "^https?://twitter\\.com(/#!)?/(mentions|i/connect)$"

    .line 75
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/OpenUriHelper;->a:Ljava/util/regex/Pattern;

    .line 76
    const-string/jumbo v0, "com.android.chrome"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "com.chrome.beta"

    aput-object v2, v1, v4

    const-string/jumbo v2, "com.chrome.dev"

    aput-object v2, v1, v5

    const-string/jumbo v2, "com.android.browser"

    aput-object v2, v1, v6

    const/4 v2, 0x3

    const-string/jumbo v3, "org.mozilla.firefox"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "com.opera.mini.android"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "com.opera.browser"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "mobi.mgeek.TunnyBrowser"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "com.UCMobile.intl"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/OpenUriHelper;->b:Ljava/util/Set;

    .line 86
    const-string/jumbo v0, "www.periscope.tv"

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "periscope.tv"

    aput-object v2, v1, v4

    const-string/jumbo v2, "vine.co"

    aput-object v2, v1, v5

    .line 87
    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/OpenUriHelper;->c:Ljava/util/Set;

    .line 86
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;)Lcom/twitter/android/client/OpenUriHelper$LinkHandler;
    .locals 2

    .prologue
    .line 300
    instance-of v0, p2, Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 301
    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbxd;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper$LinkHandler;->a:Lcom/twitter/android/client/OpenUriHelper$LinkHandler;

    .line 320
    :goto_0
    return-object v0

    .line 304
    :cond_0
    iget-object v0, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 305
    :goto_1
    sget-object v1, Lcom/twitter/model/util/g;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 306
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper$LinkHandler;->e:Lcom/twitter/android/client/OpenUriHelper$LinkHandler;

    goto :goto_0

    .line 304
    :cond_1
    iget-object v0, p2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    goto :goto_1

    .line 308
    :cond_2
    sget-object v1, Lcom/twitter/android/client/OpenUriHelper;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 309
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper$LinkHandler;->f:Lcom/twitter/android/client/OpenUriHelper$LinkHandler;

    goto :goto_0

    .line 311
    :cond_3
    iget-object v0, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 312
    iget-object v0, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/client/OpenUriHelper;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 313
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper$LinkHandler;->b:Lcom/twitter/android/client/OpenUriHelper$LinkHandler;

    goto :goto_0

    .line 316
    :cond_4
    iget-object v0, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/ac;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 317
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper$LinkHandler;->d:Lcom/twitter/android/client/OpenUriHelper$LinkHandler;

    goto :goto_0

    .line 320
    :cond_5
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper$LinkHandler;->c:Lcom/twitter/android/client/OpenUriHelper$LinkHandler;

    goto :goto_0
.end method

.method public static declared-synchronized a()Lcom/twitter/android/client/OpenUriHelper;
    .locals 2

    .prologue
    .line 100
    const-class v1, Lcom/twitter/android/client/OpenUriHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper;->d:Lcom/twitter/android/client/OpenUriHelper;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/twitter/android/client/OpenUriHelper;

    invoke-direct {v0}, Lcom/twitter/android/client/OpenUriHelper;-><init>()V

    sput-object v0, Lcom/twitter/android/client/OpenUriHelper;->d:Lcom/twitter/android/client/OpenUriHelper;

    .line 102
    const-class v0, Lcom/twitter/android/client/OpenUriHelper;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 104
    :cond_0
    sget-object v0, Lcom/twitter/android/client/OpenUriHelper;->d:Lcom/twitter/android/client/OpenUriHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Lcom/twitter/model/core/ad;JLandroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-static {v0, p1, p2, p3}, Lcom/twitter/android/client/OpenUriHelper;->a(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 502
    const-string/jumbo v0, "tel:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    :goto_0
    return-object p0

    .line 505
    :cond_0
    const-string/jumbo v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 506
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 507
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 510
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 524
    invoke-static {p3, p1, p2}, Lrr;->a(Landroid/content/Context;J)Lrr;

    move-result-object v0

    .line 525
    invoke-static {}, Lrr;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/ac;->d(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 526
    invoke-virtual {v0}, Lrr;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527
    invoke-virtual {v0, p0}, Lrr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 531
    :cond_0
    invoke-static {p0}, Lcom/twitter/android/client/OpenUriHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 294
    invoke-static {p0}, Lcom/twitter/android/util/n;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 295
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 331
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 332
    invoke-static {p1}, Lcom/twitter/util/u;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335
    const-string/jumbo v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :goto_0
    return-void

    .line 338
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/client/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 396
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    .line 397
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 398
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 399
    new-instance v1, Lcom/twitter/android/client/OpenUriHelper$1;

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/twitter/android/client/OpenUriHelper$1;-><init>(Lcom/twitter/android/client/f;Lbaa;J)V

    .line 441
    invoke-static {p0, v1}, Lmk;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 442
    invoke-interface {p1}, Lcom/twitter/android/client/f;->b()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 443
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "tweet:accept_data:::impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 445
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 211
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 212
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->b:Lcom/twitter/library/api/PromotedEvent;

    .line 213
    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v3

    .line 212
    invoke-static {v2, v3}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v2

    iget-object v3, p2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    .line 214
    invoke-virtual {v2, v3}, Lbsq$a;->a(Ljava/lang/String;)Lbsq$a;

    move-result-object v2

    invoke-virtual {v2}, Lbsq$a;->a()Lbsq;

    move-result-object v2

    .line 215
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 217
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;)Lcom/twitter/android/client/OpenUriHelper$LinkHandler;

    move-result-object v2

    .line 219
    const/4 v3, 0x0

    .line 220
    sget-object v4, Lcom/twitter/android/client/OpenUriHelper$3;->a:[I

    invoke-virtual {v2}, Lcom/twitter/android/client/OpenUriHelper$LinkHandler;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 251
    :goto_0
    if-eqz p5, :cond_1

    .line 252
    if-eqz p1, :cond_2

    .line 253
    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->d()Lcom/twitter/library/scribe/ScribeItemsProvider;

    move-result-object v2

    .line 254
    :goto_1
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4, p3, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 255
    const/4 v5, 0x0

    move-object/from16 v0, p7

    invoke-static {v4, p0, v2, v0, v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/library/scribe/ScribeItemsProvider;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 257
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p5, v2, v5

    invoke-virtual {v4, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 258
    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 259
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v4, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 260
    :goto_2
    invoke-virtual {v2, v4, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 261
    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 257
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 263
    :cond_1
    return-void

    .line 222
    :pswitch_0
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/BrowserDataSource;

    move-object/from16 v0, p7

    invoke-static {p0, v2, p2, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto :goto_0

    .line 226
    :pswitch_1
    iget-object v2, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    .line 230
    :pswitch_2
    invoke-static {p2, p3, p4, p0}, Lcom/twitter/android/client/OpenUriHelper;->a(Lcom/twitter/model/core/ad;JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 231
    iget-object v5, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    new-instance v8, Lcom/twitter/android/client/OpenUriHelper$a;

    invoke-direct {v8, p0, v4, p1}, Lcom/twitter/android/client/OpenUriHelper$a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v3, p0

    move-wide v6, p3

    invoke-static/range {v3 .. v10}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/f;ZLjava/lang/String;)V

    move-object v3, v4

    .line 233
    goto :goto_0

    .line 236
    :pswitch_3
    invoke-static {p0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 240
    :pswitch_4
    invoke-static {p0, p2, p3, p4}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/model/core/ad;J)V

    goto :goto_0

    .line 244
    :pswitch_5
    const v2, 0x7f0a0486

    invoke-static {p0, v2}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 253
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 259
    :cond_3
    iget-object v3, p2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    goto :goto_2

    .line 220
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 6

    .prologue
    .line 270
    new-instance v0, Lcom/twitter/android/client/OpenUriHelper$b;

    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->e()J

    move-result-wide v2

    move-object v5, p2

    check-cast v5, Lcom/twitter/model/core/MediaEntity;

    move-object v1, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/OpenUriHelper$b;-><init>(Landroid/content/Context;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/core/MediaEntity;)V

    .line 272
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    invoke-virtual {v1}, Lbaa;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    const-string/jumbo v2, "pic.twitter.com"

    .line 273
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    invoke-static {p0, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/android/client/f;)V

    .line 278
    :goto_0
    return-void

    .line 276
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/client/OpenUriHelper$b;->a()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 11

    .prologue
    .line 141
    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-static/range {v1 .. v10}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLjava/lang/String;)V

    .line 142
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Z)V
    .locals 11

    .prologue
    .line 147
    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-static/range {v1 .. v10}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLjava/lang/String;)V

    .line 148
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLjava/lang/String;)V
    .locals 11

    .prologue
    .line 168
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 169
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v3

    invoke-static {v2, v3}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v2

    .line 170
    invoke-virtual {v2}, Lbsq$a;->a()Lbsq;

    move-result-object v2

    .line 169
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 172
    :cond_0
    invoke-static {p2, p3, p4, p0}, Lcom/twitter/android/client/OpenUriHelper;->a(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 173
    const/4 v5, 0x0

    new-instance v8, Lcom/twitter/android/client/OpenUriHelper$a;

    invoke-direct {v8, p0, v4, p1}, Lcom/twitter/android/client/OpenUriHelper$a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V

    move-object v3, p0

    move-wide v6, p3

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v3 .. v10}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/f;ZLjava/lang/String;)V

    .line 175
    if-eqz p1, :cond_2

    .line 176
    invoke-interface {p1}, Lcom/twitter/library/client/BrowserDataSource;->d()Lcom/twitter/library/scribe/ScribeItemsProvider;

    move-result-object v2

    .line 177
    :goto_0
    if-eqz p5, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v3, p3, v4

    if-eqz v3, :cond_1

    .line 178
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, p3, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 179
    const/4 v4, 0x0

    move-object/from16 v0, p7

    invoke-static {v3, p0, v2, v0, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/library/scribe/ScribeItemsProvider;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 181
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p5, v2, v4

    invoke-virtual {v3, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 182
    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 183
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 184
    invoke-virtual {v2, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 185
    invoke-virtual {v2, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 181
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 187
    :cond_1
    return-void

    .line 176
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/model/core/ad;J)V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p1, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 282
    :goto_0
    sget-object v1, Lcom/twitter/model/util/g;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 285
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/twitter/database/schema/a$v;->b:Landroid/net/Uri;

    .line 286
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const/16 v3, 0x2f

    .line 287
    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v2, "ownerId"

    .line 288
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 286
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 285
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 291
    :cond_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p1, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;JLcom/twitter/library/client/BrowserDataSource;)V

    .line 130
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JLcom/twitter/library/client/BrowserDataSource;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 134
    invoke-static {p1, p2, p3, p0}, Lcom/twitter/android/client/OpenUriHelper;->a(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 135
    new-instance v6, Lcom/twitter/android/client/OpenUriHelper$a;

    invoke-direct {v6, p0, v2, p4}, Lcom/twitter/android/client/OpenUriHelper$a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v4, p2

    move-object v8, v3

    invoke-static/range {v1 .. v8}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/f;ZLjava/lang/String;)V

    .line 136
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V
    .locals 2

    .prologue
    .line 487
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/browser/BrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 488
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "browser_data_source"

    .line 489
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 487
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 490
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/f;ZLjava/lang/String;)V
    .locals 5

    .prologue
    .line 355
    if-nez p2, :cond_0

    .line 356
    :goto_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 357
    sget-object v1, Lcom/twitter/model/util/g;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 358
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 359
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 360
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/twitter/database/schema/a$v;->b:Landroid/net/Uri;

    .line 361
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const/16 v3, 0x2f

    .line 362
    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v2, "ownerId"

    .line 364
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 363
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 365
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 361
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 360
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 384
    :goto_1
    return-void

    :cond_0
    move-object p1, p2

    .line 355
    goto :goto_0

    .line 366
    :cond_1
    sget-object v1, Lcom/twitter/android/client/OpenUriHelper;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 367
    invoke-static {p0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;)V

    goto :goto_1

    .line 368
    :cond_2
    invoke-static {p0, v0}, Lcom/twitter/android/client/OpenUriHelper;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 369
    invoke-static {p0, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_1

    .line 372
    :cond_3
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    .line 373
    invoke-static {v0, p6}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 374
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/UrlInterpreterActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "source"

    .line 375
    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 376
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "is_from_umf_prompt"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 374
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 378
    :cond_4
    invoke-virtual {v1}, Lbaa;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 379
    invoke-static {p0, p5}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/android/client/f;)V

    goto :goto_1

    .line 381
    :cond_5
    invoke-interface {p5}, Lcom/twitter/android/client/f;->a()V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 448
    invoke-static {p1}, Lcom/twitter/util/ac;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v0

    .line 451
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 452
    invoke-static {}, Lcom/twitter/android/client/OpenUriHelper;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0, v1}, Lcom/twitter/android/client/OpenUriHelper;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/twitter/android/client/OpenUriHelper;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 563
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_0

    sget-object v1, Lcom/twitter/android/client/OpenUriHelper;->c:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 457
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 466
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 467
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 468
    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 469
    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v2, :cond_1

    .line 480
    :cond_0
    :goto_0
    return v0

    .line 472
    :cond_1
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 473
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "com.twitter.android"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 477
    const-string/jumbo v2, "android"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 480
    invoke-static {p1}, Lcom/twitter/util/u;->a(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v3, Lcom/twitter/android/client/OpenUriHelper;->b:Ljava/util/Set;

    .line 481
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v2, :cond_4

    .line 482
    :cond_2
    invoke-static {p1}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v2, :cond_4

    :cond_3
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/client/OpenUriHelper;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 582
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 583
    if-eqz v1, :cond_0

    .line 584
    const-string/jumbo v2, "ad_formats_android_in_app_browser_unsupported_domains"

    .line 585
    invoke-static {v2}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 586
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 588
    :cond_0
    return v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 574
    if-eqz p0, :cond_1

    const-string/jumbo v0, "youtube.com/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "youtu.be/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 461
    invoke-static {}, Lcom/twitter/android/client/OpenUriHelper;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->b()Lcqs;

    move-result-object v1

    const-string/jumbo v2, "in_app_browser"

    .line 462
    invoke-interface {v1, v2, v0}, Lcqs;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 461
    :goto_0
    return v0

    .line 462
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/app/Activity;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 543
    invoke-static {p1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    invoke-virtual {v0}, Lbaa;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "twitter_access_android_media_forward_enabled"

    .line 544
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545
    new-instance v0, Lcom/twitter/android/client/OpenUriHelper$2;

    invoke-direct {v0, p0, p2}, Lcom/twitter/android/client/OpenUriHelper$2;-><init>(Lcom/twitter/android/client/OpenUriHelper;Ljava/lang/Runnable;)V

    invoke-static {p1, v0}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/android/client/f;)V

    .line 560
    :goto_0
    return-void

    .line 558
    :cond_0
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
