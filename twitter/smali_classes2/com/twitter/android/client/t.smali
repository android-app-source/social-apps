.class public Lcom/twitter/android/client/t;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/widget/ListView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/client/t;->a:Landroid/view/View;

    .line 26
    iput-object p2, p0, Lcom/twitter/android/client/t;->b:Landroid/widget/ListView;

    .line 27
    iget-object v0, p0, Lcom/twitter/android/client/t;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 28
    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/twitter/android/client/t;
    .locals 3

    .prologue
    .line 18
    const v0, 0x7f1307e7

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 19
    const v0, 0x7f1307e8

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 20
    new-instance v2, Lcom/twitter/android/client/t;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/android/client/t;-><init>(Landroid/view/View;Landroid/widget/ListView;Landroid/view/View;)V

    return-object v2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/client/t;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 32
    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/client/t;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 44
    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/client/t;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 48
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/client/t;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 40
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/client/t;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 36
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/client/t;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/t;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
