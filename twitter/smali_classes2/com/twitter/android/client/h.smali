.class public Lcom/twitter/android/client/h;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/client/g;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lcom/twitter/android/client/g;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/g;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/twitter/android/client/h;->b:Lcom/twitter/android/client/g;

    if-eqz v0, :cond_0

    .line 34
    sget-object v0, Lcom/twitter/android/client/h;->b:Lcom/twitter/android/client/g;

    .line 36
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/h;->a(Ljava/lang/String;)Lcom/twitter/android/client/g;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/android/client/g;
    .locals 2

    .prologue
    .line 41
    sget-object v0, Lcom/twitter/android/client/h;->b:Lcom/twitter/android/client/g;

    if-eqz v0, :cond_1

    .line 42
    sget-object v0, Lcom/twitter/android/client/h;->b:Lcom/twitter/android/client/g;

    .line 49
    :cond_0
    :goto_0
    return-object v0

    .line 44
    :cond_1
    sget-object v0, Lcom/twitter/android/client/h;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/g;

    .line 45
    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/twitter/android/client/g;

    invoke-direct {v0}, Lcom/twitter/android/client/g;-><init>()V

    .line 47
    sget-object v1, Lcom/twitter/android/client/h;->a:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/client/h;->a:Ljava/util/Map;

    .line 21
    const-class v0, Lcom/twitter/android/client/h;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 22
    return-void
.end method
