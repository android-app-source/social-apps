.class public Lcom/twitter/android/client/chrome/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/chrome/a$a;
    }
.end annotation


# direct methods
.method private static a(Landroid/app/Activity;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItemsProvider;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 105
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/client/chrome/ChromeCustomTabsActionReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 106
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_items_provider"

    .line 107
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 108
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Landroid/app/Activity;Ljava/lang/String;ZLcom/twitter/library/scribe/ScribeItemsProvider;)Landroid/support/customtabs/CustomTabsIntent;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 61
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 62
    invoke-static {v3}, Lcom/twitter/library/client/k;->a(Landroid/content/Context;)Lcom/twitter/library/client/k;

    move-result-object v1

    .line 63
    if-eqz p2, :cond_0

    .line 64
    invoke-virtual {v1, p1, v3}, Lcom/twitter/library/client/k;->a(Ljava/lang/String;Landroid/content/Context;)Z

    .line 67
    :cond_0
    invoke-virtual {v1, p1, v3}, Lcom/twitter/library/client/k;->b(Ljava/lang/String;Landroid/content/Context;)Landroid/support/customtabs/CustomTabsIntent$Builder;

    move-result-object v1

    const v4, 0x7f110015

    .line 68
    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/support/customtabs/CustomTabsIntent$Builder;->setToolbarColor(I)Landroid/support/customtabs/CustomTabsIntent$Builder;

    move-result-object v1

    .line 69
    invoke-virtual {v1, v9}, Landroid/support/customtabs/CustomTabsIntent$Builder;->setShowTitle(Z)Landroid/support/customtabs/CustomTabsIntent$Builder;

    move-result-object v4

    .line 71
    invoke-static {}, Lcom/twitter/android/client/chrome/CustomTabsAction;->values()[Lcom/twitter/android/client/chrome/CustomTabsAction;

    move-result-object v5

    array-length v6, v5

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_2

    aget-object v7, v5, v1

    .line 72
    iget-object v8, v7, Lcom/twitter/android/client/chrome/CustomTabsAction;->id:Ljava/lang/String;

    invoke-static {p0, v8, p3}, Lcom/twitter/android/client/chrome/a;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItemsProvider;)Landroid/app/PendingIntent;

    move-result-object v8

    .line 73
    if-eqz v8, :cond_1

    .line 74
    invoke-virtual {v7, p0}, Lcom/twitter/android/client/chrome/CustomTabsAction;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7, v8}, Landroid/support/customtabs/CustomTabsIntent$Builder;->addMenuItem(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/customtabs/CustomTabsIntent$Builder;

    .line 71
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :cond_2
    invoke-static {}, Lbpx;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 81
    invoke-static {v3, v9}, Lcom/twitter/android/client/chrome/a$a;->a(Landroid/content/Context;Z)Lcom/twitter/android/client/chrome/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/chrome/a$a;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 82
    const-string/jumbo v0, "compose_icon"

    .line 88
    :goto_1
    if-eqz v1, :cond_3

    .line 90
    invoke-static {p0, v0, p3}, Lcom/twitter/android/client/chrome/a;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItemsProvider;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 91
    sget-object v2, Lcom/twitter/android/client/chrome/CustomTabsAction;->a:Lcom/twitter/android/client/chrome/CustomTabsAction;

    .line 92
    invoke-virtual {v2, v3}, Lcom/twitter/android/client/chrome/CustomTabsAction;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 91
    invoke-virtual {v4, v1, v2, v0}, Landroid/support/customtabs/CustomTabsIntent$Builder;->setActionButton(Landroid/graphics/Bitmap;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/customtabs/CustomTabsIntent$Builder;

    .line 97
    :cond_3
    const v0, 0x7f05005e

    const v1, 0x7f050032

    invoke-virtual {v4, p0, v0, v1}, Landroid/support/customtabs/CustomTabsIntent$Builder;->setStartAnimations(Landroid/content/Context;II)Landroid/support/customtabs/CustomTabsIntent$Builder;

    .line 98
    const v0, 0x7f050030

    const v1, 0x7f050059

    invoke-virtual {v4, p0, v0, v1}, Landroid/support/customtabs/CustomTabsIntent$Builder;->setExitAnimations(Landroid/content/Context;II)Landroid/support/customtabs/CustomTabsIntent$Builder;

    .line 100
    invoke-virtual {v4}, Landroid/support/customtabs/CustomTabsIntent$Builder;->build()Landroid/support/customtabs/CustomTabsIntent;

    move-result-object v0

    return-object v0

    .line 83
    :cond_4
    invoke-static {}, Lbpx;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 84
    invoke-static {v3, v2}, Lcom/twitter/android/client/chrome/a$a;->a(Landroid/content/Context;Z)Lcom/twitter/android/client/chrome/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/chrome/a$a;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 85
    const-string/jumbo v0, "tweet_text_icon"

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V
    .locals 3

    .prologue
    .line 46
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/twitter/library/client/BrowserDataSource;->d()Lcom/twitter/library/scribe/ScribeItemsProvider;

    move-result-object v0

    .line 47
    :goto_0
    invoke-static {p0}, Lcom/twitter/library/client/k;->a(Landroid/content/Context;)Lcom/twitter/library/client/k;

    move-result-object v1

    const/4 v2, 0x0

    .line 50
    invoke-static {p0, p1, v2, v0}, Lcom/twitter/android/client/chrome/a;->a(Landroid/app/Activity;Ljava/lang/String;ZLcom/twitter/library/scribe/ScribeItemsProvider;)Landroid/support/customtabs/CustomTabsIntent;

    move-result-object v0

    .line 47
    invoke-virtual {v1, p0, p1, v0, p2}, Lcom/twitter/library/client/k;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/support/customtabs/CustomTabsIntent;Lcom/twitter/library/client/BrowserDataSource;)V

    .line 52
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
