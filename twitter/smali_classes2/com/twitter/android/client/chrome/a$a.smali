.class Lcom/twitter/android/client/chrome/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/chrome/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static a:Lcom/twitter/android/client/chrome/a$a;


# instance fields
.field private final b:Z

.field private final c:Z

.field private d:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Landroid/content/Context;ZZ)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-boolean p3, p0, Lcom/twitter/android/client/chrome/a$a;->c:Z

    .line 125
    iput-boolean p2, p0, Lcom/twitter/android/client/chrome/a$a;->b:Z

    .line 126
    if-eqz p2, :cond_0

    .line 127
    invoke-static {p1, p3}, Lcom/twitter/android/client/chrome/a$a;->b(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/chrome/a$a;->d:Landroid/graphics/Bitmap;

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/chrome/a$a;->a(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/chrome/a$a;->d:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static a(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/high16 v7, 0x42400000    # 48.0f

    .line 175
    const v0, 0x7f0e012f

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 176
    const v1, 0x7f0e0130

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 177
    new-instance v2, Landroid/text/TextPaint;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(I)V

    .line 181
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 182
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 183
    const-string/jumbo v4, "TWEET"

    const/4 v5, 0x0

    const-string/jumbo v6, "TWEET"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v4, v5, v6, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 184
    int-to-float v4, v1

    mul-float/2addr v4, v7

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v4, v3

    .line 186
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 187
    const v3, 0x7f110190

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 189
    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    neg-float v3, v3

    .line 190
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 191
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 192
    const-string/jumbo v5, "TWEET"

    const/4 v6, 0x0

    int-to-float v0, v0

    add-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    invoke-virtual {v4, v5, v6, v0, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 194
    return-object v1
.end method

.method public static a(Landroid/content/Context;Z)Lcom/twitter/android/client/chrome/a$a;
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/d;->a(Landroid/content/res/Resources;)Z

    move-result v0

    .line 136
    sget-object v1, Lcom/twitter/android/client/chrome/a$a;->a:Lcom/twitter/android/client/chrome/a$a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/client/chrome/a$a;->a:Lcom/twitter/android/client/chrome/a$a;

    iget-boolean v1, v1, Lcom/twitter/android/client/chrome/a$a;->c:Z

    if-ne v1, v0, :cond_0

    sget-object v1, Lcom/twitter/android/client/chrome/a$a;->a:Lcom/twitter/android/client/chrome/a$a;

    iget-boolean v1, v1, Lcom/twitter/android/client/chrome/a$a;->b:Z

    if-eq v1, p1, :cond_1

    .line 138
    :cond_0
    const-class v1, Lcom/twitter/android/client/chrome/a$a;

    invoke-static {v1}, Lcru;->a(Ljava/lang/Class;)V

    .line 139
    new-instance v1, Lcom/twitter/android/client/chrome/a$a;

    invoke-direct {v1, p0, p1, v0}, Lcom/twitter/android/client/chrome/a$a;-><init>(Landroid/content/Context;ZZ)V

    sput-object v1, Lcom/twitter/android/client/chrome/a$a;->a:Lcom/twitter/android/client/chrome/a$a;

    .line 141
    :cond_1
    sget-object v0, Lcom/twitter/android/client/chrome/a$a;->a:Lcom/twitter/android/client/chrome/a$a;

    return-object v0
.end method

.method private static b(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 149
    if-eqz p1, :cond_0

    const v0, 0x7f020817

    .line 151
    :goto_0
    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 153
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 154
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 166
    :goto_1
    return-object v0

    .line 149
    :cond_0
    const v0, 0x7f020814

    goto :goto_0

    .line 155
    :cond_1
    instance-of v1, v0, Landroid/graphics/drawable/VectorDrawable;

    if-nez v1, :cond_2

    instance-of v1, v0, Landroid/support/graphics/drawable/VectorDrawableCompat;

    if-eqz v1, :cond_3

    .line 156
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 157
    const v2, 0x7f0e012f

    .line 158
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0e0130

    .line 159
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 157
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 160
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 161
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 162
    invoke-virtual {v0, v4, v4, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 163
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object v0, v1

    .line 164
    goto :goto_1

    .line 166
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/client/chrome/a$a;->d:Landroid/graphics/Bitmap;

    return-object v0
.end method
