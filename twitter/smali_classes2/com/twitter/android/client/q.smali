.class public abstract Lcom/twitter/android/client/q;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()V
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/q;->a(Lcom/twitter/library/card/ae;)V

    .line 63
    invoke-static {}, Lcom/twitter/library/card/v;->a()Lcom/twitter/library/card/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/q;->a(Lcom/twitter/library/card/v;)V

    .line 64
    return-void
.end method

.method private static a(Lcom/twitter/library/card/ae;)V
    .locals 11

    .prologue
    const/4 v5, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 79
    const-string/jumbo v0, "summary"

    new-instance v2, Lcom/twitter/android/card/ac;

    invoke-direct {v2}, Lcom/twitter/android/card/ac;-><init>()V

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v10

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->e:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 81
    const-string/jumbo v0, "summary_large_image"

    new-instance v2, Lcom/twitter/android/card/ad;

    invoke-direct {v2}, Lcom/twitter/android/card/ad;-><init>()V

    new-array v3, v10, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->e:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 84
    const-string/jumbo v0, "summary_large_image"

    new-instance v2, Lcom/twitter/android/card/ac;

    invoke-direct {v2}, Lcom/twitter/android/card/ac;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 85
    const-string/jumbo v0, "appplayer"

    new-instance v2, Lcom/twitter/android/revenue/card/ao;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/ao;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 86
    const-string/jumbo v0, "promo_website"

    new-instance v2, Lcom/twitter/android/revenue/card/as;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/as;-><init>()V

    new-array v3, v5, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->e:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v10

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 89
    const-string/jumbo v0, "promo_website"

    new-instance v2, Lcom/twitter/android/card/ac;

    invoke-direct {v2}, Lcom/twitter/android/card/ac;-><init>()V

    new-array v3, v8, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 90
    const-string/jumbo v0, "promo_image_app"

    new-instance v2, Lcom/twitter/android/revenue/card/ad;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/ad;-><init>()V

    new-array v3, v10, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 92
    const-string/jumbo v0, "2485840682:flock"

    new-instance v2, Lcom/twitter/android/revenue/card/s;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/s;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 93
    const-string/jumbo v0, "2485840682:flock_v2"

    new-instance v2, Lcom/twitter/android/revenue/card/s;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/s;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 94
    const-string/jumbo v0, "promo_app"

    new-instance v2, Lcom/twitter/android/revenue/card/d;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/d;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 95
    const-string/jumbo v0, "2586390716:buy_now"

    new-instance v2, Lcom/twitter/android/commerce/card/e;

    new-instance v3, Lcom/twitter/android/commerce/card/d;

    invoke-direct {v3}, Lcom/twitter/android/commerce/card/d;-><init>()V

    invoke-direct {v2, v3}, Lcom/twitter/android/commerce/card/e;-><init>(Lcom/twitter/android/revenue/card/i;)V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 97
    const-string/jumbo v0, "2586390716:buy_now_offers"

    new-instance v2, Lcom/twitter/android/commerce/card/f;

    new-instance v3, Lcom/twitter/android/commerce/card/h;

    invoke-direct {v3}, Lcom/twitter/android/commerce/card/h;-><init>()V

    invoke-direct {v2, v3}, Lcom/twitter/android/commerce/card/f;-><init>(Lcom/twitter/android/revenue/card/i;)V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 99
    const-string/jumbo v0, "2586390716:product_ad"

    new-instance v2, Lcom/twitter/android/commerce/card/j;

    invoke-direct {v2}, Lcom/twitter/android/commerce/card/j;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 100
    const-string/jumbo v0, "2586390716:authenticated_web_view"

    new-instance v2, Lcom/twitter/android/revenue/card/af;

    new-instance v3, Lcom/twitter/android/commerce/card/b;

    invoke-direct {v3}, Lcom/twitter/android/commerce/card/b;-><init>()V

    invoke-direct {v2, v3}, Lcom/twitter/android/revenue/card/af;-><init>(Lcom/twitter/android/revenue/card/i;)V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 102
    const-string/jumbo v0, "2586390716:message_me"

    new-instance v2, Lrc;

    invoke-direct {v2}, Lrc;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 104
    const-string/jumbo v0, "promotion"

    new-instance v2, Lcom/twitter/android/revenue/card/x;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/x;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 105
    const-string/jumbo v0, "app"

    new-instance v2, Lcom/twitter/android/revenue/card/d;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/d;-><init>()V

    new-array v3, v8, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 106
    const-string/jumbo v0, "app"

    new-instance v2, Lcom/twitter/android/revenue/card/ab;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/ab;-><init>()V

    new-array v3, v8, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->c:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 107
    const-string/jumbo v0, "audio"

    new-instance v2, Lcom/twitter/android/av/card/b;

    invoke-direct {v2}, Lcom/twitter/android/av/card/b;-><init>()V

    new-array v3, v5, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->e:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v10

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 109
    const-string/jumbo v0, "amplify"

    new-instance v2, Lcom/twitter/android/av/card/e;

    invoke-direct {v2}, Lcom/twitter/android/av/card/e;-><init>()V

    new-array v3, v10, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 110
    const-string/jumbo v0, "player"

    new-instance v2, Lcom/twitter/android/card/y;

    invoke-direct {v2}, Lcom/twitter/android/card/y;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 111
    const-string/jumbo v0, "player"

    new-instance v2, Lcom/twitter/android/card/ad;

    invoke-direct {v2}, Lcom/twitter/android/card/ad;-><init>()V

    new-array v3, v8, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->e:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 112
    const-string/jumbo v0, "player"

    new-instance v2, Lcom/twitter/android/card/ac;

    invoke-direct {v2}, Lcom/twitter/android/card/ac;-><init>()V

    new-array v3, v8, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 113
    const-string/jumbo v0, "direct_store_link_app"

    new-instance v2, Lcom/twitter/android/revenue/card/d;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/d;-><init>()V

    new-array v3, v8, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 114
    const-string/jumbo v0, "3260518932:moment"

    new-instance v2, Lcom/twitter/android/card/s;

    invoke-direct {v2}, Lcom/twitter/android/card/s;-><init>()V

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v10

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->e:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 116
    const-string/jumbo v0, "745291183405076480:live_video"

    new-instance v2, Lrk;

    invoke-direct {v2}, Lrk;-><init>()V

    new-array v3, v10, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 119
    sget-object v0, Lcom/twitter/android/card/PollCard$Configuration;->a:Lcom/twitter/android/card/PollCard$Configuration;

    .line 120
    iget-object v2, v0, Lcom/twitter/android/card/PollCard$Configuration;->name:Ljava/lang/String;

    new-instance v3, Lcom/twitter/android/card/ab;

    invoke-direct {v3, v0}, Lcom/twitter/android/card/ab;-><init>(Lcom/twitter/android/card/PollCard$Configuration;)V

    new-array v0, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v8

    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 123
    sget-object v0, Lcom/twitter/android/card/PollCard$Configuration;->b:Lcom/twitter/android/card/PollCard$Configuration;

    .line 124
    iget-object v2, v0, Lcom/twitter/android/card/PollCard$Configuration;->name:Ljava/lang/String;

    new-instance v3, Lcom/twitter/android/card/ab;

    invoke-direct {v3, v0}, Lcom/twitter/android/card/ab;-><init>(Lcom/twitter/android/card/PollCard$Configuration;)V

    new-array v0, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v8

    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 127
    sget-object v0, Lcom/twitter/android/card/PollCard$Configuration;->d:Lcom/twitter/android/card/PollCard$Configuration;

    .line 128
    iget-object v2, v0, Lcom/twitter/android/card/PollCard$Configuration;->name:Ljava/lang/String;

    new-instance v3, Lcom/twitter/android/card/ab;

    invoke-direct {v3, v0}, Lcom/twitter/android/card/ab;-><init>(Lcom/twitter/android/card/PollCard$Configuration;)V

    new-array v0, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v8

    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 131
    sget-object v0, Lcom/twitter/android/card/PollCard$Configuration;->c:Lcom/twitter/android/card/PollCard$Configuration;

    .line 132
    iget-object v2, v0, Lcom/twitter/android/card/PollCard$Configuration;->name:Ljava/lang/String;

    new-instance v3, Lcom/twitter/android/card/ab;

    invoke-direct {v3, v0}, Lcom/twitter/android/card/ab;-><init>(Lcom/twitter/android/card/PollCard$Configuration;)V

    new-array v0, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v0, v8

    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 135
    const-string/jumbo v0, "2427656750:poll_choice4_rc"

    new-instance v2, Lcom/twitter/android/card/aa;

    invoke-direct {v2}, Lcom/twitter/android/card/aa;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 138
    new-array v2, v10, [Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    sget-object v0, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->a:Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    aput-object v0, v2, v1

    sget-object v0, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->b:Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    aput-object v0, v2, v8

    sget-object v0, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->c:Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    aput-object v0, v2, v9

    .line 143
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 144
    iget-object v5, v4, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->modelName:Ljava/lang/String;

    new-instance v6, Lcom/twitter/android/card/l;

    invoke-direct {v6, v4}, Lcom/twitter/android/card/l;-><init>(Lcom/twitter/android/card/ConsumerPollCard$Configuration;)V

    new-array v4, v10, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v7, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v7, v4, v1

    sget-object v7, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v7, v4, v8

    sget-object v7, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->h:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v7, v4, v9

    invoke-virtual {p0, v5, v6, v4}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    new-array v2, v10, [Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    sget-object v0, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->d:Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    aput-object v0, v2, v1

    sget-object v0, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->e:Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    aput-object v0, v2, v8

    sget-object v0, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->f:Lcom/twitter/android/card/ConsumerPollCard$Configuration;

    aput-object v0, v2, v9

    .line 153
    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 154
    iget-object v5, v4, Lcom/twitter/android/card/ConsumerPollCard$Configuration;->modelName:Ljava/lang/String;

    new-instance v6, Lcom/twitter/android/card/l;

    invoke-direct {v6, v4}, Lcom/twitter/android/card/l;-><init>(Lcom/twitter/android/card/ConsumerPollCard$Configuration;)V

    new-array v4, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v7, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v7, v4, v1

    sget-object v7, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v7, v4, v8

    invoke-virtual {p0, v5, v6, v4}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 157
    :cond_1
    const-string/jumbo v0, "3691233323:periscope_broadcast"

    new-instance v2, Lcom/twitter/android/card/ac;

    invoke-direct {v2}, Lcom/twitter/android/card/ac;-><init>()V

    new-array v3, v8, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 158
    const-string/jumbo v0, "3691233323:periscope_broadcast"

    new-instance v2, Lcom/twitter/android/card/v;

    invoke-direct {v2}, Lcom/twitter/android/card/v;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 162
    const-string/jumbo v0, "promo_image_convo"

    new-instance v2, Lcom/twitter/android/revenue/card/p;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/p;-><init>()V

    new-array v3, v10, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 164
    const-string/jumbo v0, "promo_video_convo"

    new-instance v2, Lcom/twitter/android/revenue/card/p;

    invoke-direct {v2}, Lcom/twitter/android/revenue/card/p;-><init>()V

    new-array v3, v10, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v8

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v9

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 167
    const-string/jumbo v0, "4889131224:vine"

    new-instance v2, Lcom/twitter/android/card/ag;

    invoke-direct {v2}, Lcom/twitter/android/card/ag;-><init>()V

    new-array v3, v9, [Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v4, v3, v1

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    aput-object v1, v3, v8

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 168
    return-void
.end method

.method private static a(Lcom/twitter/library/card/v;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 176
    const-string/jumbo v0, "audio"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_native_audio_card_android_2758"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_audio"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 179
    const-string/jumbo v0, "audio"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_native_audio_card_android_2758"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_audio"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 183
    const-string/jumbo v0, "amplify"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_native_amplify_card_android_2798"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_amplify_enabled"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 186
    const-string/jumbo v0, "amplify"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_native_amplify_card_android_2798"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_amplify_enabled"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 190
    const-string/jumbo v0, "2586390716:buy_now"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_commerce_native_cards_android_2836"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_buy_now"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 193
    const-string/jumbo v0, "2586390716:buy_now"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_commerce_native_cards_android_2836"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_buy_now"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 197
    const-string/jumbo v0, "2586390716:buy_now_offers"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_commerce_native_cards_android_2836"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_offer"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 200
    const-string/jumbo v0, "2586390716:buy_now_offers"

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v2, "card_registry_commerce_native_cards_android_2836"

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_offer"

    aput-object v4, v3, v5

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V

    .line 203
    return-void
.end method
