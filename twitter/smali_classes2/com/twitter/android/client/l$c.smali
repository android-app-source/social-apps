.class public abstract Lcom/twitter/android/client/l$c;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"

# interfaces
.implements Lcom/twitter/async/service/AsyncOperation$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/async/service/AsyncOperation",
        "<TP;TS;>;",
        "Lcom/twitter/async/service/AsyncOperation$b",
        "<TP;",
        "Lcom/twitter/android/client/l$c",
        "<TP;TS;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/client/l;

.field private final b:Lcom/twitter/android/client/notifications/StatusBarNotif;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/StatusBarNotif;)V
    .locals 0

    .prologue
    .line 895
    invoke-direct {p0, p1}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 896
    iput-object p2, p0, Lcom/twitter/android/client/l$c;->a:Lcom/twitter/android/client/l;

    .line 897
    iput-object p3, p0, Lcom/twitter/android/client/l$c;->b:Lcom/twitter/android/client/notifications/StatusBarNotif;

    .line 898
    invoke-virtual {p0, p0}, Lcom/twitter/android/client/l$c;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 899
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/android/client/notifications/StatusBarNotif;
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/twitter/android/client/l$c;->b:Lcom/twitter/android/client/notifications/StatusBarNotif;

    return-object v0
.end method

.method public final a(Lcom/twitter/android/client/l$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/client/l$c",
            "<TP;TS;>;)V"
        }
    .end annotation

    .prologue
    .line 920
    return-void
.end method

.method protected abstract a(Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/StatusBarNotif;Lcom/twitter/async/service/j;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/client/l;",
            "Lcom/twitter/android/client/notifications/StatusBarNotif;",
            "Lcom/twitter/async/service/j",
            "<TS;>;)V"
        }
    .end annotation
.end method

.method public synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 877
    check-cast p1, Lcom/twitter/android/client/l$c;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l$c;->b(Lcom/twitter/android/client/l$c;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/twitter/android/client/l$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/twitter/android/client/l$c",
            "<TP;TS;>;)V"
        }
    .end annotation

    .prologue
    .line 925
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 877
    check-cast p2, Lcom/twitter/android/client/l$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/l$c;->a(Ljava/lang/Object;Lcom/twitter/android/client/l$c;)V

    return-void
.end method

.method public final b(Lcom/twitter/android/client/l$c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/client/l$c",
            "<TP;TS;>;)V"
        }
    .end annotation

    .prologue
    .line 930
    iget-object v0, p0, Lcom/twitter/android/client/l$c;->b:Lcom/twitter/android/client/notifications/StatusBarNotif;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v0

    .line 931
    iget-object v1, p0, Lcom/twitter/android/client/l$c;->a:Lcom/twitter/android/client/l;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/l;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 936
    :goto_0
    return-void

    .line 934
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/l$c;->a:Lcom/twitter/android/client/l;

    iget-object v2, p0, Lcom/twitter/android/client/l$c;->b:Lcom/twitter/android/client/notifications/StatusBarNotif;

    invoke-virtual {p1}, Lcom/twitter/android/client/l$c;->l()Lcom/twitter/async/service/j;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/client/l$c;->a(Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/StatusBarNotif;Lcom/twitter/async/service/j;)V

    .line 935
    iget-object v1, p0, Lcom/twitter/android/client/l$c;->a:Lcom/twitter/android/client/l;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/l;->a(I)V

    goto :goto_0
.end method

.method public synthetic b(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 877
    check-cast p1, Lcom/twitter/android/client/l$c;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l$c;->a(Lcom/twitter/android/client/l$c;)V

    return-void
.end method
