.class Lcom/twitter/android/client/r$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/android/client/r;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/client/r$1;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/twitter/android/client/r$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/android/client/r;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 77
    new-instance v2, Lcom/twitter/android/client/r;

    const-class v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 79
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 78
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 80
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v3

    sget-object v1, Lcom/twitter/model/search/viewmodel/g;->a:Lcom/twitter/util/serialization/l;

    .line 81
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/search/viewmodel/g;

    invoke-direct {v2, v0, v3, v1}, Lcom/twitter/android/client/r;-><init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Lcom/twitter/model/search/viewmodel/g;)V

    .line 77
    return-object v2
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/android/client/r;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p2, Lcom/twitter/android/client/r;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-class v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 68
    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 67
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/android/client/r;->c:Ljava/lang/String;

    .line 69
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    sget-object v2, Lcom/twitter/model/search/viewmodel/g;->a:Lcom/twitter/util/serialization/l;

    .line 70
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 71
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    check-cast p2, Lcom/twitter/android/client/r;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/r$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/android/client/r;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/r$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/android/client/r;

    move-result-object v0

    return-object v0
.end method
