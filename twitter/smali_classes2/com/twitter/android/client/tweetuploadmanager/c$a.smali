.class public Lcom/twitter/android/client/tweetuploadmanager/c$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/tweetuploadmanager/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:[I

.field private c:Lcom/twitter/library/service/u;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538
    sget-object v0, Lcom/twitter/model/core/z;->b:[I

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->b:[I

    .line 539
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->a:Ljava/util/List;

    .line 540
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->d:Z

    .line 541
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V
    .locals 2

    .prologue
    .line 544
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->a:Ljava/util/List;

    invoke-virtual {p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545
    return-void
.end method

.method public a(Lcom/twitter/library/service/u;)V
    .locals 0

    .prologue
    .line 566
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->c:Lcom/twitter/library/service/u;

    .line 567
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 570
    iput-boolean p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->d:Z

    .line 571
    return-void
.end method

.method public a([I)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->b:[I

    .line 563
    return-void
.end method

.method public a()[I
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->b:[I

    return-object v0
.end method

.method public b()Lcom/twitter/library/service/u;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->c:Lcom/twitter/library/service/u;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 558
    iget-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->d:Z

    return v0
.end method

.method public d()Lorg/json/JSONObject;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 578
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 579
    const-string/jumbo v0, "shouldScribeNetworkSuccess"

    iget-boolean v2, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->d:Z

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 581
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->c:Lcom/twitter/library/service/u;

    if-eqz v0, :cond_1

    .line 582
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 583
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->c:Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 584
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 585
    const-string/jumbo v3, "url"

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->c:Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_2

    iget v0, v0, Lcom/twitter/network/l;->a:I

    .line 590
    :goto_0
    const-string/jumbo v3, "statusCode"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 593
    :cond_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 594
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 595
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 589
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 597
    :cond_3
    const-string/jumbo v0, "statesExecuted"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 599
    return-object v1
.end method
