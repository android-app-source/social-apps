.class Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/concurrent/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/concurrent/d",
        "<",
        "Ljava/lang/Exception;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/client/tweetuploadmanager/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 0

    .prologue
    .line 1053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1054
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$c;->a:Lcom/twitter/android/client/tweetuploadmanager/c;

    .line 1055
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1063
    instance-of v0, p1, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;

    if-eqz v0, :cond_0

    .line 1064
    check-cast p1, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;

    .line 1068
    :goto_0
    invoke-static {p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;)V

    .line 1069
    return-void

    .line 1066
    :cond_0
    new-instance v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;

    iget-object v1, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$c;->a:Lcom/twitter/android/client/tweetuploadmanager/c;

    invoke-direct {v0, v1, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1050
    check-cast p1, Ljava/lang/Exception;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$c;->a(Ljava/lang/Exception;)V

    return-void
.end method
