.class public Lcom/twitter/android/client/tweetuploadmanager/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/resilient/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/tweetuploadmanager/c$a;
    }
.end annotation


# static fields
.field public static final a:J


# instance fields
.field private volatile b:Z

.field private c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private e:Lru;

.field private f:Lcom/twitter/util/concurrent/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/g",
            "<*>;"
        }
    .end annotation
.end field

.field private final g:J

.field private final h:Z

.field private i:Z

.field private j:Lcom/twitter/model/drafts/a;

.field private final k:Lcom/twitter/android/client/tweetuploadmanager/c$a;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/upload/x;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/twitter/model/core/ac;

.field private final n:Lcom/twitter/library/resilient/d;

.field private o:Lcom/twitter/library/api/progress/a;

.field private p:I

.field private final q:Lcom/twitter/library/client/Session;

.field private r:Lorg/json/JSONArray;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 72
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xb

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/android/client/tweetuploadmanager/c;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    iput-boolean v1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->b:Z

    .line 230
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->d:Landroid/content/Context;

    .line 231
    iput-object p2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->q:Lcom/twitter/library/client/Session;

    .line 232
    iput-wide p3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->g:J

    .line 233
    iput-boolean v1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->h:Z

    .line 234
    iput-boolean v1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->i:Z

    .line 235
    iput p5, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->p:I

    .line 236
    new-instance v0, Lcom/twitter/android/client/tweetuploadmanager/c$a;

    invoke-direct {v0}, Lcom/twitter/android/client/tweetuploadmanager/c$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->k:Lcom/twitter/android/client/tweetuploadmanager/c$a;

    .line 238
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/resilient/f;->a(Landroid/content/Context;)Lcom/twitter/library/resilient/f;

    move-result-object v0

    .line 239
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 240
    invoke-virtual {v0, p0, v4, v2, v3}, Lcom/twitter/library/resilient/f;->a(Lcom/twitter/library/resilient/b;IJ)Lcom/twitter/library/resilient/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->n:Lcom/twitter/library/resilient/d;

    .line 245
    new-instance v0, Lcom/twitter/library/api/progress/a;

    .line 246
    invoke-static {}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->values()[Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v1

    array-length v1, v1

    iget-wide v2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->g:J

    .line 247
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/progress/a;-><init>(ILjava/lang/String;IZ)V

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->o:Lcom/twitter/library/api/progress/a;

    .line 250
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/resilient/d;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 258
    iput-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->b:Z

    .line 259
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->d:Landroid/content/Context;

    .line 260
    iput-object p2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->q:Lcom/twitter/library/client/Session;

    .line 261
    iput-object p3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->n:Lcom/twitter/library/resilient/d;

    .line 262
    iput-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->i:Z

    .line 264
    invoke-virtual {p3}, Lcom/twitter/library/resilient/d;->a()Lorg/json/JSONObject;

    move-result-object v2

    .line 265
    const-string/jumbo v3, "draftId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->g:J

    .line 266
    const-string/jumbo v3, "remainingResetsAllowed"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->p:I

    .line 267
    new-instance v3, Lcom/twitter/android/client/tweetuploadmanager/c$a;

    invoke-direct {v3}, Lcom/twitter/android/client/tweetuploadmanager/c$a;-><init>()V

    iput-object v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->k:Lcom/twitter/android/client/tweetuploadmanager/c$a;

    .line 269
    const-string/jumbo v3, "tweetMediaInfo"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->r:Lorg/json/JSONArray;

    .line 271
    const-string/jumbo v3, "cardUri"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->c:Ljava/lang/String;

    .line 273
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 274
    iget-object v4, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->n:Lcom/twitter/library/resilient/d;

    invoke-virtual {v4}, Lcom/twitter/library/resilient/d;->c()J

    move-result-wide v4

    sget-wide v6, Lcom/twitter/android/client/tweetuploadmanager/c;->a:J

    add-long/2addr v4, v6

    cmp-long v2, v4, v2

    if-gez v2, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->h:Z

    .line 279
    new-instance v0, Lcom/twitter/library/api/progress/a;

    .line 280
    invoke-static {}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->values()[Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v2

    array-length v2, v2

    iget-wide v4, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->g:J

    .line 281
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/twitter/library/api/progress/a;-><init>(ILjava/lang/String;IZ)V

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->o:Lcom/twitter/library/api/progress/a;

    .line 284
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->o:Lcom/twitter/library/api/progress/a;

    invoke-virtual {v0}, Lcom/twitter/library/api/progress/a;->a()V

    .line 288
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 382
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 383
    return-void
.end method

.method public a(Lcom/twitter/model/core/ac;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->m:Lcom/twitter/model/core/ac;

    .line 434
    return-void
.end method

.method public a(Lcom/twitter/model/drafts/a;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 401
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->j:Lcom/twitter/model/drafts/a;

    .line 402
    if-nez p1, :cond_0

    .line 403
    iput-object v8, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->l:Ljava/util/List;

    .line 426
    :goto_0
    return-void

    .line 407
    :cond_0
    iget-object v2, p1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    .line 408
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 409
    invoke-static {v3}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 410
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->r:Lorg/json/JSONArray;

    if-nez v0, :cond_1

    .line 411
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 412
    new-instance v2, Lcom/twitter/library/api/upload/x;

    invoke-direct {v2, v0}, Lcom/twitter/library/api/upload/x;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    invoke-virtual {v4, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 415
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    .line 417
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->r:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 418
    new-instance v6, Lcom/twitter/library/api/upload/x;

    iget-object v7, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->d:Landroid/content/Context;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v6, v7, v0, v5}, Lcom/twitter/library/api/upload/x;-><init>(Landroid/content/Context;Lcom/twitter/model/drafts/DraftAttachment;Lorg/json/JSONObject;)V

    invoke-virtual {v4, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 415
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 419
    :catch_0
    move-exception v0

    .line 420
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 423
    :cond_2
    iput-object v8, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->r:Lorg/json/JSONArray;

    .line 425
    :cond_3
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->l:Ljava/util/List;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->c:Ljava/lang/String;

    .line 392
    return-void
.end method

.method public a(Lorg/json/JSONObject;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 464
    const-string/jumbo v0, "persistenceVersion"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 465
    const-string/jumbo v0, "draftId"

    iget-wide v4, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->g:J

    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 466
    const-string/jumbo v0, "remainingResetsAllowed"

    iget v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->p:I

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 467
    const-string/jumbo v0, "sessionUserId"

    iget-object v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->q:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 468
    const-string/jumbo v0, "cardUri"

    iget-object v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 470
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->l:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 471
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 472
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/x;

    .line 473
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/x;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 475
    :cond_0
    const-string/jumbo v0, "tweetMediaInfo"

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 478
    :cond_1
    if-eqz p2, :cond_2

    .line 479
    const-string/jumbo v3, "loadedDraftTweetSet"

    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->j:Lcom/twitter/model/drafts/a;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p1, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 480
    const-string/jumbo v0, "loggingInfo"

    iget-object v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->k:Lcom/twitter/android/client/tweetuploadmanager/c$a;

    invoke-virtual {v3}, Lcom/twitter/android/client/tweetuploadmanager/c$a;->d()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 481
    const-string/jumbo v0, "outputStatusSet"

    iget-object v3, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->m:Lcom/twitter/model/core/ac;

    if-eqz v3, :cond_4

    :goto_2
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 483
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 479
    goto :goto_1

    :cond_4
    move v1, v2

    .line 481
    goto :goto_2
.end method

.method a(Lru;Lcom/twitter/util/concurrent/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru;",
            "Lcom/twitter/util/concurrent/g",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 396
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->e:Lru;

    .line 397
    iput-object p2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->f:Lcom/twitter/util/concurrent/g;

    .line 398
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 429
    iput-boolean p1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->i:Z

    .line 430
    return-void
.end method

.method public b(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 491
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 492
    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/client/tweetuploadmanager/c;->a(Lorg/json/JSONObject;Z)V

    .line 493
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 496
    :goto_0
    return-object v0

    .line 494
    :catch_0
    move-exception v0

    .line 495
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 496
    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->o:Lcom/twitter/library/api/progress/a;

    invoke-virtual {v0}, Lcom/twitter/library/api/progress/a;->b()V

    .line 292
    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 387
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 388
    return-void
.end method

.method declared-synchronized c()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 299
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 304
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->b:Z

    .line 306
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->o:Lcom/twitter/library/api/progress/a;

    invoke-virtual {v0}, Lcom/twitter/library/api/progress/a;->a()V

    .line 308
    const/4 v0, 0x0

    .line 309
    iget-object v2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->e:Lru;

    if-eqz v2, :cond_1

    .line 310
    iget-object v2, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->f:Lcom/twitter/util/concurrent/g;

    if-eqz v2, :cond_0

    .line 311
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->f:Lcom/twitter/util/concurrent/g;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/twitter/util/concurrent/g;->cancel(Z)Z

    move v0, v1

    .line 314
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->e:Lru;

    invoke-virtual {v1, p0}, Lru;->a(Lcom/twitter/android/client/tweetuploadmanager/c;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    :cond_1
    monitor-exit p0

    return v0

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Landroid/content/Context;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->d:Landroid/content/Context;

    return-object v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 331
    iget-wide v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->g:J

    return-wide v0
.end method

.method public g()Lcom/twitter/model/drafts/a;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->j:Lcom/twitter/model/drafts/a;

    return-object v0
.end method

.method public h()Lcom/twitter/android/client/tweetuploadmanager/c$a;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->k:Lcom/twitter/android/client/tweetuploadmanager/c$a;

    return-object v0
.end method

.method public i()Lcom/twitter/model/core/ac;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->m:Lcom/twitter/model/core/ac;

    return-object v0
.end method

.method public j()Lcom/twitter/library/resilient/d;
    .locals 2

    .prologue
    .line 353
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->n:Lcom/twitter/library/resilient/d;

    invoke-virtual {v0}, Lcom/twitter/library/resilient/d;->a()Lorg/json/JSONObject;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/tweetuploadmanager/c;->a(Lorg/json/JSONObject;Z)V

    .line 354
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->n:Lcom/twitter/library/resilient/d;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :goto_0
    return-object v0

    .line 355
    :catch_0
    move-exception v0

    .line 356
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 358
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Lcom/twitter/util/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->o:Lcom/twitter/library/api/progress/a;

    return-object v0
.end method

.method public l()Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->q:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->b:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->h:Z

    return v0
.end method

.method public o()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/upload/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 438
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->l:Ljava/util/List;

    if-nez v0, :cond_0

    .line 439
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Accessing uploadable media before draft tweet is loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->l:Ljava/util/List;

    return-object v0
.end method

.method p()I
    .locals 1

    .prologue
    .line 457
    iget v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/android/client/tweetuploadmanager/c;->p:I

    return v0
.end method
