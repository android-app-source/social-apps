.class final Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(JZ)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Z


# direct methods
.method constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 400
    iput-wide p1, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;->a:J

    iput-boolean p3, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 403
    invoke-static {}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a()Ljava/util/Map;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/tweetuploadmanager/c;

    .line 404
    if-eqz v0, :cond_1

    .line 405
    invoke-virtual {v0}, Lcom/twitter/android/client/tweetuploadmanager/c;->c()Z

    move-result v1

    .line 406
    if-nez v1, :cond_0

    .line 407
    invoke-static {v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->c(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 410
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 426
    :goto_0
    return-object v0

    .line 417
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;->b:Z

    if-eqz v0, :cond_2

    .line 418
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Tweet request not found for pending tweet with draft ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 421
    :cond_2
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 422
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 423
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 424
    iget-wide v2, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;->a:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(JJ)V

    .line 426
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
