.class Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/model/drafts/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/Session;

.field private final b:J

.field private final c:J


# direct methods
.method constructor <init>(Lcom/twitter/library/client/Session;J)V
    .locals 2

    .prologue
    .line 1115
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1116
    iput-object p1, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->a:Lcom/twitter/library/client/Session;

    .line 1117
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->b:J

    .line 1118
    iput-wide p2, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->c:J

    .line 1119
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/model/drafts/a;
    .locals 4

    .prologue
    .line 1123
    iget-wide v0, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->b:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v0

    .line 1124
    iget-wide v2, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/h;->b(J)Lcom/twitter/model/drafts/a;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/model/drafts/a;)V
    .locals 7

    .prologue
    .line 1129
    if-eqz p1, :cond_0

    .line 1130
    invoke-static {}, Lcom/twitter/android/client/notifications/h;->a()Lcom/twitter/android/client/notifications/h;

    move-result-object v0

    .line 1131
    iget-object v1, p0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->a:Lcom/twitter/library/client/Session;

    iget-wide v2, p1, Lcom/twitter/model/drafts/a;->b:J

    iget-object v4, p1, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    const v5, 0x7f0a06c1

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1138
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1110
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->a([Ljava/lang/Void;)Lcom/twitter/model/drafts/a;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1110
    check-cast p1, Lcom/twitter/model/drafts/a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->a(Lcom/twitter/model/drafts/a;)V

    return-void
.end method
