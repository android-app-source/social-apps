.class public Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;,
        Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$d;,
        Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$c;,
        Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$b;,
        Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;
    }
.end annotation


# static fields
.field public static final a:I

.field private static final b:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/android/client/tweetuploadmanager/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    invoke-static {}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->values()[Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a:I

    .line 300
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 301
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b:Ljava/util/Map;

    .line 300
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;
        }
    .end annotation

    .prologue
    .line 510
    invoke-static {p0, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v0

    .line 511
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->h()Lcom/twitter/android/client/tweetuploadmanager/c$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/tweetuploadmanager/c$a;->a(Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V

    .line 515
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->e()Landroid/content/Context;

    move-result-object v1

    .line 516
    invoke-static {v1}, Lcom/twitter/library/resilient/f;->a(Landroid/content/Context;)Lcom/twitter/library/resilient/f;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/library/resilient/f;->b(Lcom/twitter/library/resilient/b;)V

    .line 518
    return-object v0
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b:Ljava/util/Map;

    return-object v0
.end method

.method public static a(J)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(JZ)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(JZ)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    new-instance v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$1;-><init>(JZ)V

    invoke-static {v0}, Lcre;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(JJ)V
    .locals 0

    .prologue
    .line 78
    invoke-static {p0, p1, p2, p3}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b(JJ)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V
    .locals 8

    .prologue
    .line 320
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 324
    new-instance v1, Lcom/twitter/android/client/tweetuploadmanager/c;

    sget v6, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a:I

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/client/tweetuploadmanager/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    .line 330
    invoke-static {}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->values()[Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-static {v1, v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->d(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V

    .line 331
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;)V
    .locals 0

    .prologue
    .line 78
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b(Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;)V

    return-void
.end method

.method static a(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 2

    .prologue
    .line 347
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 353
    invoke-static {}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->values()[Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v0

    .line 354
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 355
    invoke-static {p0, v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->d(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V

    .line 356
    return-void
.end method

.method private static a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/library/service/u;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 946
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/library/service/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 947
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->e()Landroid/content/Context;

    move-result-object v4

    .line 948
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 949
    invoke-static {v4}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v0

    invoke-virtual {v0, v1, p1}, Lcom/twitter/android/client/z;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/service/u;)V

    .line 951
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    .line 952
    invoke-static {}, Lcom/twitter/android/client/notifications/h;->a()Lcom/twitter/android/client/notifications/h;

    move-result-object v0

    .line 953
    invoke-static {v4, p1}, Lcom/twitter/android/BouncerWebViewActivity;->a(Landroid/content/Context;Lcom/twitter/library/service/u;)Landroid/content/Intent;

    move-result-object v7

    .line 954
    const v5, 0x7f0a06c1

    move-object v4, p2

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZLandroid/content/Intent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 956
    const/4 v6, 0x1

    .line 958
    :cond_0
    return v6
.end method

.method static b(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;
        }
    .end annotation

    .prologue
    .line 536
    invoke-static {}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->values()[Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v4

    .line 537
    const/4 v1, 0x0

    .line 538
    array-length v2, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v4, v0

    .line 540
    if-ne v3, p1, :cond_0

    .line 565
    :goto_1
    if-nez p1, :cond_4

    .line 566
    new-instance v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;

    const-string/jumbo v1, "Could not find a valid tweet upload state"

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;Ljava/lang/String;)V

    throw v0

    .line 544
    :cond_0
    invoke-virtual {v3, p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->b(Lcom/twitter/android/client/tweetuploadmanager/c;)Z

    move-result v5

    .line 545
    if-nez v5, :cond_3

    .line 546
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->p()I

    move-result v0

    if-gtz v0, :cond_1

    .line 547
    new-instance v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;

    const-string/jumbo v1, "Limit of tweet upload state resets exceeded"

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;Ljava/lang/String;)V

    throw v0

    .line 553
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->ordinal()I

    move-result v3

    move v2, v3

    .line 554
    :goto_2
    if-ltz v2, :cond_5

    .line 555
    aget-object v0, v4, v3

    .line 556
    invoke-virtual {v0, p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->a(Lcom/twitter/android/client/tweetuploadmanager/c;)Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_3
    move-object p1, v0

    .line 561
    goto :goto_1

    .line 554
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    .line 538
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 569
    :cond_4
    return-object p1

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    move-object p1, v1

    goto :goto_1
.end method

.method private static b(JJ)V
    .locals 6

    .prologue
    .line 725
    invoke-static {p0, p1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 726
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/twitter/library/provider/t;->b(JJ)Ljava/lang/Long;

    move-result-object v1

    .line 727
    if-eqz v1, :cond_0

    .line 728
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/library/provider/t;->a(JLaut;)I

    .line 729
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/o;->a(J)V

    .line 731
    :cond_0
    return-void
.end method

.method private static b(Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;)V
    .locals 1

    .prologue
    .line 616
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 617
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;->a()Lcom/twitter/android/client/tweetuploadmanager/c;

    move-result-object v0

    .line 618
    invoke-static {v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->o(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 619
    invoke-static {v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->m(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 620
    invoke-virtual {v0}, Lcom/twitter/android/client/tweetuploadmanager/c;->b()V

    .line 621
    invoke-static {v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->i(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 622
    return-void
.end method

.method static b(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 6

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 601
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 602
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v4

    .line 603
    invoke-static {v2, v3}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v1

    .line 604
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v4, v5, v2, v3}, Lcom/twitter/library/provider/h;->a(JILaut;)Z

    .line 605
    new-instance v1, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;

    invoke-direct {v1, v0, v4, v5}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;-><init>(Lcom/twitter/library/client/Session;J)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 606
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 0

    .prologue
    .line 78
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->f(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {p0, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->e(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 0

    .prologue
    .line 78
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->h(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    return-void
.end method

.method private static d(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V
    .locals 1

    .prologue
    .line 361
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 366
    :try_start_0
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->e(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 369
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->g(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 372
    invoke-static {p0, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->e(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V
    :try_end_0
    .catch Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    :goto_0
    return-void

    .line 373
    :catch_0
    move-exception v0

    .line 374
    invoke-static {v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b(Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;)V

    goto :goto_0
.end method

.method private static e(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;
        }
    .end annotation

    .prologue
    .line 462
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 465
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 466
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 467
    invoke-static {v0, v1}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v0

    .line 468
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/h;->b(J)Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 469
    if-nez v0, :cond_0

    .line 470
    new-instance v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;

    const-string/jumbo v1, "Draft Tweet not found for given ID"

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;Ljava/lang/String;)V

    throw v0

    .line 472
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/client/tweetuploadmanager/c;->a(Lcom/twitter/model/drafts/a;)V

    .line 473
    return-void
.end method

.method private static e(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;
        }
    .end annotation

    .prologue
    .line 486
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 489
    invoke-static {p0, p1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;

    move-result-object v0

    .line 492
    invoke-virtual {v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;->a()Lru;

    move-result-object v1

    .line 494
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->k()Lcom/twitter/util/q;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Lru;->a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/util/q;)Lcom/twitter/util/concurrent/g;

    move-result-object v2

    .line 495
    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/client/tweetuploadmanager/c;->a(Lru;Lcom/twitter/util/concurrent/g;)V

    .line 497
    new-instance v1, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$c;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$c;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    invoke-interface {v2, v1}, Lcom/twitter/util/concurrent/g;->c(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 498
    new-instance v1, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$b;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$b;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    invoke-interface {v2, v1}, Lcom/twitter/util/concurrent/g;->d(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 499
    new-instance v1, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$d;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$d;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$TweetUploadState;)V

    invoke-interface {v2, v1}, Lcom/twitter/util/concurrent/g;->b(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 500
    return-void
.end method

.method private static f(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 0

    .prologue
    .line 584
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 585
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->o(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 586
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->n(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 587
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->a()V

    .line 588
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->i(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 589
    return-void
.end method

.method private static g(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 9

    .prologue
    .line 631
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 633
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    .line 634
    sget-object v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 635
    const-class v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 637
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 638
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 640
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->g()Lcom/twitter/model/drafts/a;

    move-result-object v8

    .line 641
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 642
    invoke-static {v6, v7}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v0

    .line 643
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/library/provider/h;->a(JILaut;)Z

    .line 645
    invoke-static {}, Lcom/twitter/android/client/notifications/h;->a()Lcom/twitter/android/client/notifications/h;

    move-result-object v0

    .line 646
    iget-object v4, v8, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    const v5, 0x7f0a05f8

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 653
    invoke-static {v6, v7}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 654
    invoke-virtual {v0, v6, v7, v2, v3}, Lcom/twitter/library/provider/t;->b(JJ)Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_0

    .line 655
    invoke-virtual {v0, v6, v7}, Lcom/twitter/library/provider/t;->b(J)Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 656
    if-nez v1, :cond_1

    .line 657
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Could not find user: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    invoke-virtual {v0, v1, v8}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/drafts/a;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 661
    invoke-static {v0}, Lcom/twitter/library/provider/o;->a(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method private static h(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 0

    .prologue
    .line 674
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 675
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->o(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 676
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->l(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 677
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->b()V

    .line 678
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->i(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 679
    return-void
.end method

.method private static i(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 6

    .prologue
    .line 683
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 686
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->e()Landroid/content/Context;

    move-result-object v0

    .line 687
    invoke-static {v0}, Lcom/twitter/library/resilient/f;->a(Landroid/content/Context;)Lcom/twitter/library/resilient/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/resilient/f;->a(Lcom/twitter/library/resilient/b;)V

    .line 691
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->g()Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 692
    if-eqz v0, :cond_0

    .line 693
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 694
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 695
    invoke-static {v2, v3}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v1

    .line 696
    iget-wide v2, v0, Lcom/twitter/model/drafts/a;->b:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/provider/h;->a(JILaut;)Z

    .line 697
    invoke-virtual {v0}, Lcom/twitter/model/drafts/a;->c()V

    .line 699
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->o()Ljava/util/List;

    move-result-object v0

    .line 700
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/x;

    .line 701
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/x;->h()V

    goto :goto_0

    .line 705
    :cond_0
    sget-object v0, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    return-void
.end method

.method private static j(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 6

    .prologue
    .line 709
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->e()Landroid/content/Context;

    move-result-object v0

    .line 710
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 711
    invoke-static {v2, v3}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v1

    .line 712
    new-instance v2, Laut;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v2, v0}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 713
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v4

    .line 714
    const/4 v0, 0x1

    invoke-virtual {v1, v4, v5, v2, v0}, Lcom/twitter/library/provider/h;->a(JLaut;Z)Z

    .line 715
    invoke-virtual {v2}, Laut;->a()V

    .line 716
    return-void
.end method

.method private static k(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 4

    .prologue
    .line 719
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 720
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    .line 721
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->b(JJ)V

    .line 722
    return-void
.end method

.method private static l(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 10

    .prologue
    .line 734
    const/4 v6, 0x0

    .line 736
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->e()Landroid/content/Context;

    move-result-object v7

    .line 737
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 738
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 739
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    .line 741
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->g()Lcom/twitter/model/drafts/a;

    move-result-object v4

    .line 742
    invoke-static {}, Lcom/twitter/android/client/notifications/h;->a()Lcom/twitter/android/client/notifications/h;

    move-result-object v0

    .line 743
    iget-object v4, v4, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    const v5, 0x7f0a05f9

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/notifications/h;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 749
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->j(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 750
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->p(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 752
    new-instance v1, Laut;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v1, v0}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 753
    invoke-static {v8, v9}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 755
    invoke-virtual {v0, v8, v9, v2, v3}, Lcom/twitter/library/provider/t;->b(JJ)Ljava/lang/Long;

    move-result-object v2

    .line 756
    if-eqz v2, :cond_0

    .line 757
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5, v1}, Lcom/twitter/library/provider/t;->a(JLaut;)I

    .line 759
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->i()Lcom/twitter/model/core/ac;

    move-result-object v3

    .line 760
    if-eqz v3, :cond_2

    .line 761
    iget-wide v4, v3, Lcom/twitter/model/core/ac;->a:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/t;->e(J)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 762
    if-eqz v0, :cond_3

    .line 763
    new-instance v3, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$2;

    invoke-direct {v3, v1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager$2;-><init>(Laut;)V

    .line 770
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 769
    invoke-static {v4, v5, v0, v3}, Lcom/twitter/library/provider/o;->a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z

    move-result v0

    :goto_0
    move v6, v0

    .line 779
    :cond_0
    :goto_1
    if-nez v6, :cond_1

    .line 780
    invoke-virtual {v1}, Laut;->a()V

    .line 782
    :cond_1
    return-void

    .line 775
    :cond_2
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->k(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_0
.end method

.method private static m(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 15

    .prologue
    const/high16 v14, 0x10000000

    const/4 v6, 0x1

    const v5, 0x7f0a06c1

    const/4 v13, 0x0

    .line 785
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->e()Landroid/content/Context;

    move-result-object v7

    .line 786
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 787
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 788
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    .line 790
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->h()Lcom/twitter/android/client/tweetuploadmanager/c$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/tweetuploadmanager/c$a;->a()[I

    move-result-object v10

    .line 794
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->k(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 796
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->g()Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 797
    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    .line 799
    :goto_0
    invoke-static {}, Lcom/twitter/android/client/notifications/h;->a()Lcom/twitter/android/client/notifications/h;

    move-result-object v0

    .line 800
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->h()Lcom/twitter/android/client/tweetuploadmanager/c$a;

    move-result-object v11

    .line 801
    invoke-virtual {v11}, Lcom/twitter/android/client/tweetuploadmanager/c$a;->b()Lcom/twitter/library/service/u;

    move-result-object v11

    .line 802
    if-eqz v11, :cond_c

    .line 803
    new-instance v12, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v12, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v8, v6, [Ljava/lang/String;

    const-string/jumbo v9, ":composition:send_tweet:save_draft:complete"

    aput-object v9, v8, v13

    .line 804
    invoke-virtual {v12, v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v8

    invoke-static {v8}, Lcpm;->a(Lcpk;)V

    .line 805
    invoke-static {p0, v11, v4}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/library/service/u;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 922
    :goto_1
    return-void

    .line 797
    :cond_0
    const-string/jumbo v4, ""

    goto :goto_0

    .line 808
    :cond_1
    invoke-virtual {v11}, Lcom/twitter/library/service/u;->d()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    move v6, v13

    .line 905
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 810
    :pswitch_0
    const/16 v8, 0xbb

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 811
    const v5, 0x7f0a0328

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 817
    :cond_2
    const/16 v8, 0x173

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 818
    const v5, 0x7f0a053c

    move v6, v13

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 824
    :cond_3
    const/16 v8, 0x174

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 825
    const v5, 0x7f0a09c8

    move v6, v13

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 831
    :cond_4
    const/16 v8, 0x175

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 832
    const v5, 0x7f0a03df

    move v6, v13

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 838
    :cond_5
    const/16 v8, 0x17f

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 839
    const v5, 0x7f0a0102

    move v6, v13

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 845
    :cond_6
    const/16 v8, 0x180

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 846
    const v5, 0x7f0a03de

    move v6, v13

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 852
    :cond_7
    const/16 v8, 0x181

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 853
    const v5, 0x7f0a0765

    move v6, v13

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 859
    :cond_8
    const/16 v8, 0xe0

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_9

    move v6, v13

    .line 860
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 866
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DialogActivity;

    invoke-direct {v0, v7, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "blocked_spammer_tweet"

    .line 867
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 868
    invoke-virtual {v0, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 866
    invoke-virtual {v7, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 869
    :cond_9
    const/16 v8, 0xdf

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_a

    move v6, v13

    .line 870
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 876
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/DialogActivity;

    invoke-direct {v0, v7, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "blocked_automated_spammer"

    .line 877
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 878
    invoke-virtual {v0, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 876
    invoke-virtual {v7, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 879
    :cond_a
    const/16 v8, 0x158

    invoke-static {v10, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v8

    if-eqz v8, :cond_b

    move v6, v13

    .line 880
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 886
    invoke-static {v7}, Lcom/twitter/android/dialog/RateLimitDialogFragmentActivity;->a(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 888
    :cond_b
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 894
    invoke-static {v7}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v0

    .line 895
    invoke-virtual {v0, v10}, Lcom/twitter/android/client/z;->a([I)V

    goto/16 :goto_1

    :cond_c
    move v6, v13

    .line 915
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto/16 :goto_1

    .line 808
    :pswitch_data_0
    .packed-switch 0x193
        :pswitch_0
    .end packed-switch
.end method

.method private static n(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 5

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 926
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 927
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->f()J

    move-result-wide v2

    .line 930
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->k(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 931
    invoke-static {p0}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->j(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 933
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->m()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 934
    invoke-static {}, Lcom/twitter/android/client/notifications/h;->a()Lcom/twitter/android/client/notifications/h;

    move-result-object v4

    .line 935
    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/twitter/android/client/notifications/h;->a(JJ)V

    .line 937
    :cond_0
    return-void
.end method

.method private static o(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 969
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->h()Lcom/twitter/android/client/tweetuploadmanager/c$a;

    move-result-object v2

    .line 970
    invoke-virtual {v2}, Lcom/twitter/android/client/tweetuploadmanager/c$a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "success"

    .line 971
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->g()Lcom/twitter/model/drafts/a;

    move-result-object v1

    .line 972
    if-eqz v1, :cond_3

    iget-object v1, v1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "has_media"

    .line 974
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 975
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "app:twitter_service:tweet:create"

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v0, v4, v5

    .line 976
    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 977
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 979
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->i()Lcom/twitter/model/core/ac;

    move-result-object v1

    .line 980
    if-eqz v1, :cond_0

    .line 981
    new-instance v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 982
    iget-wide v4, v1, Lcom/twitter/model/core/ac;->a:J

    iput-wide v4, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 983
    iput v6, v3, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 984
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 987
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/android/client/tweetuploadmanager/c$a;->b()Lcom/twitter/library/service/u;

    move-result-object v1

    .line 988
    if-eqz v1, :cond_1

    .line 989
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v2

    .line 990
    if-eqz v2, :cond_1

    .line 991
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    .line 992
    invoke-static {v0, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;)V

    .line 993
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    .line 996
    :cond_1
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 997
    return-void

    .line 970
    :cond_2
    const-string/jumbo v0, "failure"

    goto :goto_0

    .line 972
    :cond_3
    const-string/jumbo v1, "no_media"

    goto :goto_1
.end method

.method private static p(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1006
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->i()Lcom/twitter/model/core/ac;

    move-result-object v0

    .line 1007
    if-eqz v0, :cond_1

    .line 1008
    iget-object v1, v0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v1, v1, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v1}, Lcom/twitter/model/core/f;->b()I

    move-result v2

    .line 1009
    iget-object v1, v0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v1, v1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v1}, Lcom/twitter/model/core/f;->b()I

    move-result v3

    .line 1010
    iget-wide v0, v0, Lcom/twitter/model/core/ac;->j:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    const-string/jumbo v0, ":composition:send_reply:"

    move-object v1, v0

    .line 1012
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 1013
    if-lez v2, :cond_0

    .line 1014
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v6, v10, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "mentions:count"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    .line 1015
    invoke-virtual {v0, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    int-to-long v6, v2

    .line 1016
    invoke-virtual {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1014
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1018
    :cond_0
    if-lez v3, :cond_1

    .line 1019
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v10, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "hashtags:count"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v9

    .line 1020
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    int-to-long v2, v3

    .line 1021
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1019
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1024
    :cond_1
    return-void

    .line 1010
    :cond_2
    const-string/jumbo v0, ":composition:send_tweet:"

    move-object v1, v0

    goto :goto_0
.end method
