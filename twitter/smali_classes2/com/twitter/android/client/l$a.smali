.class Lcom/twitter/android/client/l$a;
.super Lcom/twitter/library/service/w;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/w",
        "<",
        "Ljava/lang/Void;",
        "Lcom/twitter/library/service/q;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/client/l;


# direct methods
.method private constructor <init>(Lcom/twitter/android/client/l;)V
    .locals 0

    .prologue
    .line 964
    iput-object p1, p0, Lcom/twitter/android/client/l$a;->a:Lcom/twitter/android/client/l;

    invoke-direct {p0}, Lcom/twitter/library/service/w;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/client/l;Lcom/twitter/android/client/l$1;)V
    .locals 0

    .prologue
    .line 964
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l$a;-><init>(Lcom/twitter/android/client/l;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 964
    check-cast p1, Lcom/twitter/library/service/q;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l$a;->a(Lcom/twitter/library/service/q;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/q;)V
    .locals 3

    .prologue
    .line 967
    invoke-virtual {p1}, Lcom/twitter/library/service/q;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 968
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 969
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 972
    iget-object v2, p0, Lcom/twitter/android/client/l$a;->a:Lcom/twitter/android/client/l;

    invoke-static {v2}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 973
    iget-object v2, p0, Lcom/twitter/android/client/l$a;->a:Lcom/twitter/android/client/l;

    invoke-static {v2}, Lcom/twitter/android/client/l;->b(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0

    .line 975
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/l$a;->a:Lcom/twitter/android/client/l;

    invoke-static {v0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 976
    iget-object v0, p0, Lcom/twitter/android/client/l$a;->a:Lcom/twitter/android/client/l;

    invoke-static {v0}, Lcom/twitter/android/client/l;->d(Lcom/twitter/android/client/l;)Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/l$a;->a:Lcom/twitter/android/client/l;

    invoke-static {v1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/android/client/l;)Lcom/twitter/library/client/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 984
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/l$a;->a:Lcom/twitter/android/client/l;

    invoke-static {v0}, Lcom/twitter/android/client/l;->e(Lcom/twitter/android/client/l;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;)V

    .line 985
    return-void
.end method
