.class public Lcom/twitter/android/client/AppBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 55
    invoke-static {v3}, Lcom/twitter/util/collection/i;->a(I)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/platform/TwitterDataSyncService;->a:Ljava/lang/String;

    const/4 v2, 0x1

    .line 56
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    const/4 v2, 0x2

    .line 57
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/database/schema/a;->b:Ljava/lang/String;

    const/4 v2, 0x3

    .line 58
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/card/e;->a:Ljava/lang/String;

    const/4 v2, 0x4

    .line 59
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/platform/PushService;->a:Ljava/lang/String;

    const/4 v2, 0x5

    .line 60
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/platform/PushService;->b:Ljava/lang/String;

    .line 61
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/Map;

    .line 64
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/AppBroadcastReceiver;->c:Z

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    const-string/jumbo v0, "owner_id"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 161
    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    invoke-static {v2, v4, v5, v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(ZJLjava/util/List;)V

    .line 167
    invoke-static {v4, v5}, Lcom/twitter/library/util/b;->a(J)Lakm;

    move-result-object v3

    .line 168
    if-eqz v3, :cond_0

    .line 170
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 171
    invoke-virtual {v3}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v0

    sget-object v6, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_3

    .line 172
    invoke-virtual {v3}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v0

    sget-object v6, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 173
    :goto_1
    const-string/jumbo v6, "unread_notifications_count"

    invoke-virtual {p1, v6, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 174
    const-string/jumbo v6, "show_notif"

    .line 175
    invoke-virtual {p1, v6, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 176
    if-lez v2, :cond_2

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 177
    invoke-static {}, Lcom/twitter/android/client/notifications/a;->a()Lcom/twitter/android/client/notifications/a;

    move-result-object v0

    invoke-virtual {v3}, Lakm;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v5, v2}, Lcom/twitter/android/client/notifications/a;->a(Ljava/lang/String;JI)V

    .line 181
    :cond_2
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 182
    invoke-static {p0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 172
    goto :goto_1
.end method

.method private static a(Landroid/content/Context;[Ljava/lang/String;[I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 148
    if-eqz p1, :cond_2

    array-length v0, p1

    if-lez v0, :cond_2

    if-eqz p2, :cond_2

    array-length v0, p2

    if-lez v0, :cond_2

    .line 149
    new-instance v3, Ljava/util/ArrayList;

    array-length v0, p1

    array-length v2, p2

    mul-int/2addr v0, v2

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 150
    array-length v4, p1

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, p1, v2

    .line 151
    array-length v6, p2

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_0

    aget v7, p2, v0

    .line 152
    invoke-static {v5, v7}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 150
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 155
    :cond_1
    invoke-static {p0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/g;->c()Lcom/twitter/library/media/manager/e;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/library/media/manager/e;->a(Ljava/util/Collection;)Ljava/util/concurrent/Future;

    .line 157
    :cond_2
    return-void
.end method

.method public static a(Lcom/twitter/library/client/e;)V
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_0
    return-void
.end method

.method private static a(ZJLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 238
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/e;

    invoke-virtual {v0, p1, p2, p0}, Lcom/twitter/library/client/e;->a(JZ)V

    .line 237
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 240
    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 188
    const-string/jumbo v0, "push_data"

    .line 189
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/library/platform/notifications/u;->a:Lcom/twitter/util/serialization/l;

    .line 188
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/u;

    .line 190
    if-nez v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v1, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget v1, v1, Lcom/twitter/library/platform/notifications/r;->h:I

    packed-switch v1, :pswitch_data_0

    .line 212
    :cond_2
    :goto_1
    :pswitch_0
    iget-wide v2, v0, Lcom/twitter/library/platform/notifications/u;->c:J

    invoke-static {v2, v3}, Lcom/twitter/library/util/b;->a(J)Lakm;

    move-result-object v1

    .line 213
    if-eqz v1, :cond_0

    .line 215
    invoke-virtual {v1}, Lakm;->b()Lcnz;

    move-result-object v1

    invoke-virtual {v1}, Lcnz;->b()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v1

    .line 216
    invoke-virtual {v1}, Lcom/twitter/library/platform/notifications/t;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/library/platform/notifications/u;)V

    .line 222
    :cond_3
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/twitter/library/platform/notifications/u;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 224
    iget-object v1, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget v1, v1, Lcom/twitter/library/platform/notifications/r;->l:I

    if-ltz v1, :cond_4

    iget-wide v2, v0, Lcom/twitter/library/platform/notifications/u;->c:J

    const-string/jumbo v1, "oem_launcher_badge_push_update"

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Lcoj;->a(JLjava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 226
    iget-object v0, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget v0, v0, Lcom/twitter/library/platform/notifications/r;->l:I

    invoke-static {p0, v0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 196
    :pswitch_1
    const/4 v1, 0x1

    iget-wide v2, v0, Lcom/twitter/library/platform/notifications/u;->c:J

    sget-object v4, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(ZJLjava/util/List;)V

    goto :goto_1

    .line 200
    :pswitch_2
    sget-object v1, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_2
    if-ltz v2, :cond_2

    .line 201
    sget-object v1, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/e;

    iget-object v3, v0, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/twitter/library/client/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 200
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_2

    .line 229
    :cond_4
    invoke-static {p0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 194
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Lcom/twitter/library/client/e;)V
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method


# virtual methods
.method a()Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 144
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    .line 80
    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/Map;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 81
    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 87
    if-ne v0, v5, :cond_2

    .line 88
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v1

    .line 90
    new-instance v2, Lbsg;

    invoke-direct {v2, p1}, Lbsg;-><init>(Landroid/content/Context;)V

    .line 91
    invoke-static {}, Lcom/twitter/util/connectivity/a;->a()Lcom/twitter/util/connectivity/a;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/util/connectivity/a;->a(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V

    .line 94
    invoke-static {p1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v2

    .line 95
    invoke-virtual {v1}, Lcrr;->c()Z

    move-result v3

    iget-boolean v4, p0, Lcom/twitter/android/client/AppBroadcastReceiver;->c:Z

    invoke-virtual {v2, v3, v4}, Lbaa;->e(ZZ)V

    .line 96
    invoke-virtual {v1}, Lcrr;->c()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/client/AppBroadcastReceiver;->c:Z

    .line 99
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/client/AppBroadcastReceiver;->a()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 100
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 102
    :pswitch_1
    invoke-static {p1, p2}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 106
    :pswitch_2
    invoke-static {p1, p2}, Lcom/twitter/android/client/AppBroadcastReceiver;->b(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 110
    :pswitch_3
    const-string/jumbo v0, "url"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_0

    invoke-static {p1, v0, v1}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(Landroid/content/Context;[Ljava/lang/String;[I)V

    goto :goto_0

    .line 115
    :pswitch_4
    const-string/jumbo v0, "scribe_log"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 116
    const-string/jumbo v0, "scribe_download_log"

    .line 117
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 118
    new-instance v0, Lcom/twitter/android/revenue/f;

    invoke-direct {v0}, Lcom/twitter/android/revenue/f;-><init>()V

    .line 119
    const-string/jumbo v1, "app_id"

    .line 120
    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v1, "promoted_content"

    sget-object v5, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 121
    invoke-static {p2, v1, v5}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcgi;

    const-string/jumbo v1, "timestamp"

    const-wide/16 v6, 0x0

    .line 123
    invoke-virtual {p2, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string/jumbo v1, "timeframe"

    const-wide/32 v8, 0x1b7740

    .line 124
    invoke-virtual {p2, v1, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string/jumbo v1, "polling_interval"

    const-wide/32 v10, 0x927c0

    .line 126
    invoke-virtual {p2, v1, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    move-object v1, p1

    .line 119
    invoke-virtual/range {v0 .. v11}, Lcom/twitter/android/revenue/f;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;Lcgi;JJJ)V

    goto/16 :goto_0

    .line 134
    :cond_3
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 135
    const-string/jumbo v0, "push_data"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/library/platform/notifications/u;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/u;

    .line 137
    invoke-static {p1}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/l;->b(Lcom/twitter/library/platform/notifications/u;)V

    goto/16 :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch

    .line 110
    :array_0
    .array-data 4
        -0x3
        -0x1
    .end array-data
.end method
