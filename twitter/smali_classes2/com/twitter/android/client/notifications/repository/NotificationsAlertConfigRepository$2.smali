.class Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->a(J)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/g$a",
        "<",
        "Lcfx;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;J)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;->b:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;

    iput-wide p2, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lrx/h;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/h",
            "<-",
            "Lcfx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;->b:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;

    invoke-static {v0}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->a(Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;)Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;

    move-result-object v0

    const-class v1, Laxb;

    new-instance v2, Lcom/twitter/database/model/f$a;

    invoke-direct {v2}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v3, "account_id"

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;->a:J

    .line 51
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    .line 52
    invoke-virtual {v2}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v2

    const-class v3, Lcfx;

    .line 48
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfx;

    .line 54
    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$AlertConigfNotFoundException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$AlertConigfNotFoundException;-><init>(Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$1;)V

    invoke-virtual {p1, v0}, Lrx/h;->a(Ljava/lang/Throwable;)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p1, v0}, Lrx/h;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lrx/h;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;->a(Lrx/h;)V

    return-void
.end method
