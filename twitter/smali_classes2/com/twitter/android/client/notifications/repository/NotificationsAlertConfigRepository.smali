.class public Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$AlertConigfNotFoundException;,
        Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;

.field private final b:Lrx/f;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;Lrx/f;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->a:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;

    .line 40
    iput-object p2, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->b:Lrx/f;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;)Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->a:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;

    return-object v0
.end method

.method public static a()Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lalg;->ag()Lalg;

    move-result-object v0

    invoke-virtual {v0}, Lalg;->b()Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/g",
            "<",
            "Lcfx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$2;-><init>(Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;J)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->b:Lrx/f;

    .line 61
    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$1;-><init>(Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;)V

    .line 62
    invoke-virtual {v0, v1}, Lrx/g;->d(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 44
    return-object v0
.end method
