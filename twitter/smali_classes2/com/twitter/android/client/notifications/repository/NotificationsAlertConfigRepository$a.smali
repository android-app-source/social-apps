.class public Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/hydrator/c;


# direct methods
.method public constructor <init>(Lcom/twitter/library/provider/j;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-virtual {p1}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;->a:Lcom/twitter/database/hydrator/c;

    .line 77
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/twitter/database/model/k;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Lcom/twitter/database/model/f;",
            "Ljava/lang/Class",
            "<TD;>;)TD;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository$a;->a:Lcom/twitter/database/hydrator/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/database/hydrator/c;->a(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
