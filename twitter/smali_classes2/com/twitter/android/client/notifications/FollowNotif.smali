.class public Lcom/twitter/android/client/notifications/FollowNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/client/notifications/FollowNotif;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final e:Lcom/twitter/android/client/notifications/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/android/client/notifications/FollowNotif$1;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/FollowNotif$1;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/FollowNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    .line 30
    new-instance v0, Lcom/twitter/android/client/notifications/j;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->e:Lcom/twitter/android/client/notifications/j;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 30
    new-instance v0, Lcom/twitter/android/client/notifications/j;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->e:Lcom/twitter/android/client/notifications/j;

    .line 35
    return-void
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    iget-boolean v0, v0, Lcfv;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/FollowNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a05fa

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 98
    invoke-virtual {v3}, Lcom/twitter/library/platform/notifications/r;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 96
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0a05fb

    goto :goto_0
.end method

.method public av_()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/FollowNotif;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->e:Lcom/twitter/android/client/notifications/j;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/FollowNotif;->b:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/client/notifications/j;->a(J)Ljava/lang/String;

    move-result-object v3

    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 66
    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    :goto_1
    return-object v0

    .line 54
    :sswitch_0
    const-string/jumbo v4, "cleaned"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v4, "challenger1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string/jumbo v4, "challenger2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 57
    :pswitch_0
    const v0, 0x7f0a071c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 60
    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/FollowNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0715

    :goto_2
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 63
    invoke-virtual {v3}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 60
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const v0, 0x7f0a071d

    goto :goto_2

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/r;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 54
    :sswitch_data_0
    .sparse-switch
        -0x2913025e -> :sswitch_1
        -0x2913025d -> :sswitch_2
        0x331154a8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected c(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->e:Lcom/twitter/android/client/notifications/j;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/FollowNotif;->b:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/client/notifications/j;->a(J)Ljava/lang/String;

    move-result-object v4

    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 88
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/FollowNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0a05ee

    :goto_1
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    .line 73
    :sswitch_0
    const-string/jumbo v5, "cleaned"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v5, "challenger1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string/jumbo v5, "challenger2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    goto :goto_0

    .line 76
    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/FollowNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0714

    :goto_3
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 77
    invoke-virtual {v3}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 76
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_1
    const v0, 0x7f0a071a

    goto :goto_3

    .line 80
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    const v0, 0x7f0a071b

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v4}, Lcom/twitter/library/platform/notifications/r;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v1, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->g:Ljava/lang/String;

    aput-object v1, v3, v2

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 84
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 88
    :cond_3
    const v0, 0x7f0a05ef

    goto :goto_1

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        -0x2913025e -> :sswitch_1
        -0x2913025d -> :sswitch_2
        0x331154a8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 103
    const v0, 0x7f02066b

    return v0
.end method

.method protected e(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    iget-object v2, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 109
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "com.twitter.android.home.fol."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/FollowNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 110
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 108
    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/FollowNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "followed_back"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "followed"

    goto :goto_0
.end method

.method public g()Lcom/twitter/android/client/notifications/StatusBarNotif$a;
    .locals 6

    .prologue
    .line 49
    new-instance v0, Lcom/twitter/android/client/notifications/d;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/FollowNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/FollowNotif;->c:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/FollowNotif;->b:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/android/client/notifications/d;-><init>(Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V

    return-object v0
.end method

.method protected h(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    const v0, 0x7f0a0708

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    const-string/jumbo v0, "follow"

    return-object v0
.end method
