.class public Lcom/twitter/android/client/notifications/h;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/notifications/h$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/NotificationManagerCompat;

.field private final c:Landroid/os/Handler;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/library/media/manager/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/h;->c:Landroid/os/Handler;

    .line 43
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/h;->d:Ljava/util/Map;

    .line 53
    iput-object p1, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    .line 54
    invoke-static {p1}, Landroid/support/v4/app/NotificationManagerCompat;->from(Landroid/content/Context;)Landroid/support/v4/app/NotificationManagerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/h;->b:Landroid/support/v4/app/NotificationManagerCompat;

    .line 55
    invoke-static {p1}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/h;->e:Lcom/twitter/library/media/manager/g;

    .line 56
    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;JLcom/twitter/android/client/notifications/h$a;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 8

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 142
    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    iget v1, p4, Lcom/twitter/android/client/notifications/h$a;->h:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f02066b

    .line 144
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    .line 145
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 146
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 147
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p4, Lcom/twitter/android/client/notifications/h$a;->g:Ljava/lang/String;

    .line 148
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-boolean v1, p4, Lcom/twitter/android/client/notifications/h$a;->a:Z

    .line 149
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 151
    iget-object v0, p4, Lcom/twitter/android/client/notifications/h$a;->c:Landroid/content/Intent;

    if-eqz v0, :cond_4

    iget-object v0, p4, Lcom/twitter/android/client/notifications/h$a;->c:Landroid/content/Intent;

    .line 153
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    invoke-static {v2, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 155
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->e:Lcom/twitter/library/media/manager/g;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    const/4 v3, -0x3

    .line 158
    invoke-static {v0, v3}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 157
    invoke-virtual {v2, v0}, Lcom/twitter/library/media/manager/g;->b(Lcom/twitter/media/request/a$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 164
    :cond_0
    iget-boolean v0, p4, Lcom/twitter/android/client/notifications/h$a;->f:Z

    if-eqz v0, :cond_1

    .line 166
    const/16 v0, 0x64

    const/4 v2, 0x1

    invoke-virtual {v1, v5, v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 169
    :cond_1
    iget-object v0, p4, Lcom/twitter/android/client/notifications/h$a;->b:Landroid/content/Intent;

    if-eqz v0, :cond_2

    .line 170
    const v0, 0x7f020226

    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    const v3, 0x7f0a00f6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    iget-object v4, p4, Lcom/twitter/android/client/notifications/h$a;->b:Landroid/content/Intent;

    .line 171
    invoke-static {v3, v5, v4, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 170
    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 175
    :cond_2
    iget-object v0, p4, Lcom/twitter/android/client/notifications/h$a;->e:Landroid/content/Intent;

    if-eqz v0, :cond_3

    .line 176
    const v0, 0x7f02024e

    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    const v3, 0x7f0a05ec

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    iget-object v4, p4, Lcom/twitter/android/client/notifications/h$a;->e:Landroid/content/Intent;

    .line 177
    invoke-static {v3, v5, v4, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 176
    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 181
    :cond_3
    monitor-enter p0

    .line 182
    :try_start_0
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 183
    invoke-static {v2, v3, p2, p3}, Lcom/twitter/android/client/notifications/h$a;->a(JJ)Ljava/lang/String;

    move-result-object v2

    .line 184
    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->d:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 185
    iget-object v3, p0, Lcom/twitter/android/client/notifications/h;->c:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 187
    iget-boolean v0, p4, Lcom/twitter/android/client/notifications/h$a;->d:Z

    if-eqz v0, :cond_5

    .line 188
    new-instance v0, Lcom/twitter/android/client/notifications/h$2;

    invoke-direct {v0, p0, v2, v1}, Lcom/twitter/android/client/notifications/h$2;-><init>(Lcom/twitter/android/client/notifications/h;Ljava/lang/String;Landroid/support/v4/app/NotificationCompat$Builder;)V

    .line 195
    iget-object v3, p0, Lcom/twitter/android/client/notifications/h;->d:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->c:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 200
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    return-object v1

    .line 151
    :cond_4
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    goto/16 :goto_0

    .line 198
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->b:Landroid/support/v4/app/NotificationManagerCompat;

    const/16 v3, 0x3e9

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/app/NotificationManagerCompat;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_1

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/twitter/android/client/notifications/h;)Landroid/support/v4/app/NotificationManagerCompat;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->b:Landroid/support/v4/app/NotificationManagerCompat;

    return-object v0
.end method

.method public static a()Lcom/twitter/android/client/notifications/h;
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lalg;->ag()Lalg;

    move-result-object v0

    invoke-virtual {v0}, Lalg;->a()Lcom/twitter/android/client/notifications/h;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 64
    new-instance v0, Lcom/twitter/android/client/notifications/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/client/notifications/h$a;-><init>(Lcom/twitter/android/client/notifications/h$1;)V

    .line 65
    iput-boolean v2, v0, Lcom/twitter/android/client/notifications/h$a;->a:Z

    .line 66
    iput-boolean v2, v0, Lcom/twitter/android/client/notifications/h$a;->d:Z

    .line 67
    iput-boolean v2, v0, Lcom/twitter/android/client/notifications/h$a;->f:Z

    .line 68
    iput-object p4, v0, Lcom/twitter/android/client/notifications/h$a;->g:Ljava/lang/String;

    .line 69
    iput p5, v0, Lcom/twitter/android/client/notifications/h$a;->h:I

    .line 71
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLcom/twitter/android/client/notifications/h$a;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZ)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 8

    .prologue
    .line 89
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZLandroid/content/Intent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;IZLandroid/content/Intent;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 99
    new-instance v0, Lcom/twitter/android/client/notifications/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/client/notifications/h$a;-><init>(Lcom/twitter/android/client/notifications/h$1;)V

    .line 100
    iput-boolean v3, v0, Lcom/twitter/android/client/notifications/h$a;->a:Z

    .line 101
    iput-object p4, v0, Lcom/twitter/android/client/notifications/h$a;->g:Ljava/lang/String;

    .line 102
    iput p5, v0, Lcom/twitter/android/client/notifications/h$a;->h:I

    .line 104
    iget-object v1, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    .line 105
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-static {v1, v3, v2, v4, v5}, Lcom/twitter/app/drafts/DraftsActivity;->a(Landroid/content/Context;ZLjava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/client/notifications/h$a;->c:Landroid/content/Intent;

    .line 107
    if-eqz p6, :cond_1

    .line 108
    iget-object v1, p0, Lcom/twitter/android/client/notifications/h;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v1, p2, p3, v2, v3}, Lcom/twitter/android/client/TweetUploadService;->a(Landroid/content/Context;JJ)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/client/notifications/h$a;->e:Landroid/content/Intent;

    .line 114
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLcom/twitter/android/client/notifications/h$a;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    return-object v0

    .line 109
    :cond_1
    if-eqz p7, :cond_0

    .line 111
    iput-object p7, v0, Lcom/twitter/android/client/notifications/h$a;->c:Landroid/content/Intent;

    goto :goto_0
.end method

.method public a(JJ)V
    .locals 3

    .prologue
    .line 75
    monitor-enter p0

    .line 76
    :try_start_0
    invoke-static {p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/h$a;->a(JJ)Ljava/lang/String;

    move-result-object v1

    .line 77
    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 78
    iget-object v2, p0, Lcom/twitter/android/client/notifications/h;->c:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 79
    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->b:Landroid/support/v4/app/NotificationManagerCompat;

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/NotificationManagerCompat;->cancel(Ljava/lang/String;I)V

    .line 80
    monitor-exit p0

    .line 81
    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 6

    .prologue
    .line 120
    new-instance v0, Lcom/twitter/android/client/notifications/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/client/notifications/h$a;-><init>(Lcom/twitter/android/client/notifications/h$1;)V

    .line 121
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/android/client/notifications/h$a;->a:Z

    .line 122
    iput-object p4, v0, Lcom/twitter/android/client/notifications/h$a;->g:Ljava/lang/String;

    .line 123
    iput p5, v0, Lcom/twitter/android/client/notifications/h$a;->h:I

    .line 125
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/client/Session;JLcom/twitter/android/client/notifications/h$a;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/twitter/android/client/notifications/h;->c:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/android/client/notifications/h$1;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/twitter/android/client/notifications/h$1;-><init>(Lcom/twitter/android/client/notifications/h;Lcom/twitter/library/client/Session;J)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 136
    return-object v0
.end method
