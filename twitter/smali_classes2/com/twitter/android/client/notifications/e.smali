.class public Lcom/twitter/android/client/notifications/e;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method protected static a(Landroid/content/Context;Landroid/support/v4/app/NotificationCompat$Builder;Lcfx;)V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 29
    iget-boolean v1, p2, Lcfx;->e:Z

    if-eqz v1, :cond_0

    .line 30
    const/4 v0, 0x2

    .line 32
    :cond_0
    sget-object v1, Lcfx;->a:Lcfx;

    if-ne p2, v1, :cond_1

    .line 33
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 35
    :cond_1
    invoke-virtual {p1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 36
    iget-boolean v0, p2, Lcfx;->c:Z

    if-eqz v0, :cond_2

    .line 37
    const v0, -0xff0100

    const/16 v1, 0x1f4

    const/16 v2, 0x7d0

    invoke-virtual {p1, v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setLights(III)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 39
    :cond_2
    sget-boolean v0, Lcni;->b:Z

    if-eqz v0, :cond_3

    .line 40
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 41
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-nez v0, :cond_3

    .line 42
    iget-object v0, p2, Lcfx;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 43
    iget-object v0, p2, Lcfx;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSound(Landroid/net/Uri;I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 47
    :cond_3
    return-void
.end method
