.class public Lcom/twitter/android/client/notifications/DMNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/notifications/DMNotif$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/client/notifications/DMNotif;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/android/client/notifications/DMNotif$1;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/DMNotif$1;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/DMNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 55
    return-void
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 128
    new-instance v0, Lcom/twitter/app/dm/j$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 129
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/j$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 130
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->c(Z)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 128
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "com.twitter.android.home.messages."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 128
    return-object v0
.end method


# virtual methods
.method protected a()Lcom/twitter/android/client/notifications/DMNotif$a;
    .locals 6

    .prologue
    .line 64
    new-instance v0, Lcom/twitter/android/client/notifications/DMNotif$a;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/DMNotif;->c:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/DMNotif;->b:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/DMNotif$a;-><init>(Lcom/twitter/android/client/notifications/DMNotif;Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/media/request/a$a;
    .locals 4

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/library/network/t;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/DMNotif;->o()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/model/account/OAuthToken;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 101
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/media/request/a$a;->a(Ljava/lang/Object;)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/DMNotif;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    return-object v0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/DMNotif;->v()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method

.method public e()I
    .locals 1
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 117
    const v0, 0x7f02065d

    return v0
.end method

.method protected e(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/twitter/android/client/notifications/DMNotif;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method f(Landroid/content/Context;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 3

    .prologue
    .line 214
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/support/v4/app/NotificationCompat$WearableExtender;

    invoke-direct {v1}, Landroid/support/v4/app/NotificationCompat$WearableExtender;-><init>()V

    .line 215
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/DMNotif;->g(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$WearableExtender;->addActions(Ljava/util/List;)Landroid/support/v4/app/NotificationCompat$WearableExtender;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->extend(Landroid/support/v4/app/NotificationCompat$Extender;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 214
    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string/jumbo v0, "message"

    return-object v0
.end method

.method protected synthetic g()Lcom/twitter/android/client/notifications/StatusBarNotif$a;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/DMNotif;->a()Lcom/twitter/android/client/notifications/DMNotif$a;

    move-result-object v0

    return-object v0
.end method

.method protected g(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/NotificationCompat$Action;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v8, 0x8000000

    .line 221
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 222
    invoke-static {}, Lcom/twitter/library/dm/d;->c()Z

    move-result v2

    .line 225
    const v0, 0x7f0a00c9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 226
    new-instance v0, Landroid/support/v4/app/RemoteInput$Builder;

    const-string/jumbo v4, "extra_notification_reply"

    invoke-direct {v0, v4}, Landroid/support/v4/app/RemoteInput$Builder;-><init>(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v0, v3}, Landroid/support/v4/app/RemoteInput$Builder;->setLabel(Ljava/lang/CharSequence;)Landroid/support/v4/app/RemoteInput$Builder;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Landroid/support/v4/app/RemoteInput$Builder;->build()Landroid/support/v4/app/RemoteInput;

    move-result-object v4

    .line 229
    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/twitter/app/dm/DMNotifReplyIntentService;

    invoke-direct {v0, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v5, "extra_owner_id"

    .line 230
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/DMNotif;->o()J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "extra_conversation_id"

    iget-object v6, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v6, v6, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    .line 231
    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "extra_notification_id"

    iget-object v6, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget v6, v6, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 232
    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 233
    const/4 v5, 0x0

    invoke-static {p1, v5, v0, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 235
    invoke-static {}, Lcmj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f020667

    .line 237
    :goto_0
    new-instance v6, Landroid/support/v4/app/NotificationCompat$Action$Builder;

    invoke-direct {v6, v0, v3, v5}, Landroid/support/v4/app/NotificationCompat$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 239
    invoke-virtual {v6, v4}, Landroid/support/v4/app/NotificationCompat$Action$Builder;->addRemoteInput(Landroid/support/v4/app/RemoteInput;)Landroid/support/v4/app/NotificationCompat$Action$Builder;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Action$Builder;->build()Landroid/support/v4/app/NotificationCompat$Action;

    move-result-object v0

    .line 241
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 243
    if-eqz v2, :cond_0

    .line 245
    const v0, 0x7f0a00c4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 246
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/app/dm/DMNotifMuteIntentService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "extra_owner_id"

    .line 247
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/DMNotif;->o()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "extra_conversation_id"

    iget-object v4, p0, Lcom/twitter/android/client/notifications/DMNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v4, v4, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    .line 248
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "extra_notification_id"

    .line 249
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/DMNotif;->y()[I

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    move-result-object v2

    .line 250
    const/4 v3, 0x1

    invoke-static {p1, v3, v2, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 253
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Action$Builder;

    const v4, 0x7f020833

    invoke-direct {v3, v4, v0, v2}, Landroid/support/v4/app/NotificationCompat$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 255
    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Action$Builder;->build()Landroid/support/v4/app/NotificationCompat$Action;

    move-result-object v0

    .line 256
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 259
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    .line 235
    :cond_1
    const v0, 0x7f02082e

    goto :goto_0
.end method
