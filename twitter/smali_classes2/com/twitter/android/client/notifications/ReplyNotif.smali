.class public Lcom/twitter/android/client/notifications/ReplyNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/client/notifications/ReplyNotif;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/android/client/notifications/ReplyNotif$1;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/ReplyNotif$1;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/ReplyNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/r;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcmj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f020667

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020666

    goto :goto_0
.end method

.method protected e(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 68
    invoke-virtual {v1}, Lcom/twitter/library/platform/notifications/r;->d()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->b:J

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/database/schema/a;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "com.twitter.android.home.replies."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string/jumbo v0, "reply"

    return-object v0
.end method

.method public g()Lcom/twitter/android/client/notifications/StatusBarNotif$a;
    .locals 6

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/android/client/notifications/d;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->c:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/ReplyNotif;->b:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/android/client/notifications/d;-><init>(Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V

    return-object v0
.end method

.method protected h(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const v0, 0x7f0a070a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const-string/jumbo v0, "mention"

    return-object v0
.end method
