.class public Lcom/twitter/android/client/notifications/GenericNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/client/notifications/GenericNotif;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/android/client/notifications/GenericNotif$1;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/GenericNotif$1;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/GenericNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 43
    return-void
.end method

.method static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    .line 73
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public av_()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->c:Ljava/lang/String;

    .line 63
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v1

    invoke-virtual {v1}, Lakn;->b()I

    move-result v1

    .line 62
    invoke-static {v0, v1}, Lcom/twitter/android/client/notifications/GenericNotif;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected aw_()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const-wide/16 v4, 0x0

    .line 110
    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/r;->e()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/r;->d()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 111
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 112
    iget-object v1, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v1}, Lcom/twitter/library/platform/notifications/r;->e()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 113
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 114
    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r;->e()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 115
    const-string/jumbo v2, "sender_id"

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 116
    iput v6, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 117
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v1}, Lcom/twitter/library/platform/notifications/r;->d()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 120
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 121
    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r;->d()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 122
    const-string/jumbo v2, "status_id"

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 123
    const/4 v2, 0x0

    iput v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 124
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_2
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 128
    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 129
    const/4 v2, 0x6

    iput v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 130
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :goto_0
    return-object v0

    .line 132
    :cond_3
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    .line 54
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a0061

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 79
    const v0, 0x7f02066b

    return v0
.end method

.method protected e(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    .line 86
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 87
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    .line 91
    if-nez v1, :cond_0

    .line 92
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    :cond_0
    const-string/jumbo v1, "reason"

    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "notification_setting_key"

    iget-object v3, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v3, Lcom/twitter/library/platform/notifications/r;->c:Ljava/lang/String;

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string/jumbo v0, "generic"

    return-object v0
.end method
