.class public Lcom/twitter/android/client/notifications/c;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/NotificationManagerCompat;

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/c;->c:Landroid/os/Handler;

    .line 36
    iput-object p1, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    .line 37
    invoke-static {p1}, Landroid/support/v4/app/NotificationManagerCompat;->from(Landroid/content/Context;)Landroid/support/v4/app/NotificationManagerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/c;->b:Landroid/support/v4/app/NotificationManagerCompat;

    .line 38
    return-void
.end method

.method private a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 80
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 81
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "failure"

    .line 82
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "AbsFragmentActivity_account_name"

    .line 83
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "avatar_media_file"

    const-string/jumbo v3, "avatar_media"

    .line 85
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    .line 84
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "header_media_file"

    const-string/jumbo v3, "header_media"

    .line 87
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    .line 86
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$z;->b:Landroid/net/Uri;

    .line 88
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    .line 89
    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 88
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 91
    const-string/jumbo v1, "name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "desc"

    .line 92
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "url"

    .line 93
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "place"

    .line 94
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    :cond_0
    const-string/jumbo v1, "update_profile"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "name"

    const-string/jumbo v3, "name"

    .line 97
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "description"

    const-string/jumbo v3, "desc"

    .line 99
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 98
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "url"

    const-string/jumbo v3, "url"

    .line 101
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 100
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "location"

    const-string/jumbo v3, "place"

    .line 103
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 102
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    :cond_1
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 108
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/notifications/c;)Landroid/support/v4/app/NotificationManagerCompat;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/client/notifications/c;->b:Landroid/support/v4/app/NotificationManagerCompat;

    return-object v0
.end method


# virtual methods
.method public a(ILcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 41
    iget-object v0, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 42
    invoke-static {v1, v3, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 41
    invoke-static {v0, p1, v1}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 43
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/twitter/android/client/notifications/g;

    invoke-direct {v1}, Lcom/twitter/android/client/notifications/g;-><init>()V

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/notifications/g;->a(J)Lcom/twitter/android/client/notifications/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/g;->toString()Ljava/lang/String;

    move-result-object v1

    .line 45
    iget-object v2, p0, Lcom/twitter/android/client/notifications/c;->b:Landroid/support/v4/app/NotificationManagerCompat;

    const/16 v3, 0x3ec

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v2, v1, v3, v0}, Landroid/support/v4/app/NotificationManagerCompat;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 46
    return-void
.end method

.method public a(ZILandroid/os/Bundle;Lcom/twitter/library/client/Session;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/16 v6, 0x3ec

    const/4 v5, 0x0

    .line 52
    new-instance v0, Lcom/twitter/android/client/notifications/g;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/g;-><init>()V

    invoke-virtual {p4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/client/notifications/g;->a(J)Lcom/twitter/android/client/notifications/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/g;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    if-eqz p1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 57
    invoke-static {v2, v5, v3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 56
    invoke-static {v1, p2, v2}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 58
    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 59
    iget-object v2, p0, Lcom/twitter/android/client/notifications/c;->b:Landroid/support/v4/app/NotificationManagerCompat;

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v2, v0, v6, v1}, Landroid/support/v4/app/NotificationManagerCompat;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 60
    iget-object v1, p0, Lcom/twitter/android/client/notifications/c;->c:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/android/client/notifications/c$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/client/notifications/c$1;-><init>(Lcom/twitter/android/client/notifications/c;Ljava/lang/String;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 77
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p4}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p4}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/c;->a:Landroid/content/Context;

    .line 70
    invoke-direct {p0, p3, p4}, Lcom/twitter/android/client/notifications/c;->a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    .line 69
    invoke-static {v2, v5, v3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 68
    invoke-static {v1, p2, v2}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 72
    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lcom/twitter/android/client/notifications/c;->b:Landroid/support/v4/app/NotificationManagerCompat;

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v2, v0, v6, v1}, Landroid/support/v4/app/NotificationManagerCompat;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_0

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/client/notifications/c;->b:Landroid/support/v4/app/NotificationManagerCompat;

    invoke-virtual {v1, v0, v6}, Landroid/support/v4/app/NotificationManagerCompat;->cancel(Ljava/lang/String;I)V

    goto :goto_0
.end method
