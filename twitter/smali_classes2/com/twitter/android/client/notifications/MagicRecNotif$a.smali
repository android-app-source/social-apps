.class Lcom/twitter/android/client/notifications/MagicRecNotif$a;
.super Lcom/twitter/android/client/l$c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/notifications/MagicRecNotif;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/client/l$c",
        "<",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/MagicRecNotif;Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/client/l;",
            "Lcom/twitter/android/client/notifications/MagicRecNotif;",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 917
    const-string/jumbo v0, "com.twitter.android.client.notifications.MagicRecNotif.FacePileOperation"

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/android/client/l$c;-><init>(Ljava/lang/String;Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    .line 918
    iput-object p3, p0, Lcom/twitter/android/client/notifications/MagicRecNotif$a;->a:Landroid/content/res/Resources;

    .line 919
    iput-object p4, p0, Lcom/twitter/android/client/notifications/MagicRecNotif$a;->b:Ljava/util/List;

    .line 920
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/StatusBarNotif;Lcom/twitter/async/service/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/client/l;",
            "Lcom/twitter/android/client/notifications/StatusBarNotif;",
            "Lcom/twitter/async/service/j",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 935
    move-object v0, p2

    check-cast v0, Lcom/twitter/android/client/notifications/MagicRecNotif;

    invoke-virtual {p3}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/MagicRecNotif;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 936
    invoke-virtual {p1, p2}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    .line 937
    return-void
.end method

.method protected b()Landroid/graphics/Bitmap;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 924
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif$a;->a:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MagicRecNotif$a;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lwd;->a(Landroid/content/res/Resources;Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 906
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif$a;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 906
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif$a;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 929
    const/4 v0, 0x0

    return-object v0
.end method
