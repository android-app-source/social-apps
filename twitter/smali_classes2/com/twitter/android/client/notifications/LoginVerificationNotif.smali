.class public Lcom/twitter/android/client/notifications/LoginVerificationNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/client/notifications/LoginVerificationNotif;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/android/client/notifications/LoginVerificationNotif$1;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/LoginVerificationNotif$1;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 33
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a05f5

    .line 48
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 58
    const v0, 0x7f02066b

    return v0
.end method

.method protected e(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->b:J

    .line 65
    invoke-static {p1, v0, v1}, Lbaw;->e(Landroid/content/Context;J)Z

    move-result v0

    .line 68
    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginVerificationActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 70
    const-string/jumbo v1, "lv_account_name"

    iget-object v2, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    :goto_0
    return-object v0

    .line 72
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/SecuritySettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    const-string/jumbo v1, "SecuritySettingsActivity_account_name"

    iget-object v2, p0, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method
