.class public abstract Lcom/twitter/android/client/notifications/MagicRecNotif;
.super Lcom/twitter/android/client/notifications/GenericNotif;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/notifications/MagicRecNotif$a;
    }
.end annotation


# instance fields
.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/media/request/a;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/media/request/a;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Object;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/media/request/a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/graphics/Bitmap;

.field private j:I


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/GenericNotif;-><init>(Landroid/os/Parcel;)V

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->e:Ljava/util/Map;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->f:Ljava/util/Map;

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->g:Ljava/lang/Object;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    .line 137
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/GenericNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->e:Ljava/util/Map;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->f:Ljava/util/Map;

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->g:Ljava/lang/Object;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    .line 133
    return-void
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->w()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 612
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->x()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 613
    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 614
    iput-object p3, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 615
    if-eqz v0, :cond_0

    .line 616
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 618
    :cond_0
    if-eqz v1, :cond_1

    .line 619
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 621
    :cond_1
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;ILcom/twitter/analytics/model/ScribeLog;Lcom/twitter/analytics/model/ScribeLog;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/notifications/MagicRecNotif;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->i:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private static a(Landroid/content/res/Resources;Ljava/lang/String;I)Lcom/twitter/media/request/a;
    .locals 1

    .prologue
    .line 797
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    const v0, 0x7f0e02ca

    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 799
    invoke-static {p1, v0}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    return-object v0

    .line 797
    :cond_0
    const v0, 0x7f0e02cc

    goto :goto_0
.end method

.method static a(Landroid/app/Notification;)V
    .locals 4

    .prologue
    const v3, 0x1020006

    const/4 v2, 0x0

    .line 722
    iget-object v0, p0, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 734
    :goto_0
    return-void

    .line 728
    :cond_0
    iget-object v0, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v0, v3, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 733
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v0, v3, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method protected static a(Landroid/app/Notification;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x1020015

    .line 633
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const-string/jumbo v1, "setSingleLine"

    invoke-virtual {v0, v3, v1, v4}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 635
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const-string/jumbo v1, "setMaxLines"

    const/4 v2, 0x2

    invoke-virtual {v0, v3, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 636
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v3, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 637
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 638
    return-void
.end method

.method static a(Landroid/content/Context;Landroid/app/Notification;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 745
    if-nez p2, :cond_0

    .line 765
    :goto_0
    return-void

    .line 748
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    .line 749
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f04018f

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 750
    const v1, 0x7f130472

    iget-object v2, p1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 752
    const v1, 0x7f130473

    invoke-virtual {v0, v1, p2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 757
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-gt v1, v2, :cond_1

    .line 758
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 759
    const-string/jumbo v2, "status_bar_latest_event_content"

    const-string/jumbo v3, "id"

    const-string/jumbo v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 760
    const-string/jumbo v2, "setBackgroundResource"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 764
    :cond_1
    iput-object v0, p1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    goto :goto_0
.end method

.method static a(Landroid/content/res/Resources;Landroid/app/Notification;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 695
    const-string/jumbo v0, "big_text"

    const-string/jumbo v1, "id"

    const-string/jumbo v3, "android"

    invoke-virtual {p0, v0, v1, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 696
    const v0, 0x7f0e02ce

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 698
    const v0, 0x7f0e02cd

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 700
    iget-object v0, p1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 706
    iget-object v0, p1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const/4 v2, 0x2

    const v3, 0x7f0f002f

    .line 708
    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    .line 706
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 709
    iget-object v0, p1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const-string/jumbo v2, "setMaxLines"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 712
    iget-object v0, p1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const v2, 0x7f110023

    .line 713
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 712
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 714
    return-void
.end method

.method static a(Landroid/content/res/Resources;Landroid/app/Notification;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 656
    invoke-static {p2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 684
    :cond_0
    :goto_0
    return-void

    .line 659
    :cond_1
    const-string/jumbo v0, "line3"

    const-string/jumbo v1, "id"

    const-string/jumbo v3, "android"

    invoke-virtual {p0, v0, v1, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 660
    const-string/jumbo v0, "text"

    const-string/jumbo v3, "id"

    const-string/jumbo v4, "android"

    invoke-virtual {p0, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 663
    iget-object v0, p1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 665
    if-eqz p3, :cond_0

    .line 666
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_2

    .line 670
    iget-object v0, p1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 675
    :cond_2
    const v0, 0x7f0e02d4

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 677
    const v0, 0x7f0e02d3

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 679
    const v0, 0x7f110020

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 680
    iget-object v0, p1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 681
    iget-object v0, p1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v6, v7}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;)V
    .locals 4

    .prologue
    .line 807
    iget-object v2, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 809
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->p()I

    move-result v0

    .line 808
    invoke-virtual {p1, p2, v0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/media/request/a;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 810
    iget-object v1, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->e:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 812
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->f:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    if-eqz v0, :cond_1

    const-string/jumbo v0, "image_downloaded"

    move-object v1, v0

    .line 816
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "user_image"

    .line 818
    :goto_2
    invoke-static {p0, v1, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    monitor-exit v2

    .line 820
    return-void

    .line 811
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 815
    :cond_1
    const-string/jumbo v0, "image_queued"

    move-object v1, v0

    goto :goto_1

    .line 817
    :cond_2
    invoke-virtual {p0, p2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/media/request/a;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 819
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 561
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 562
    if-eqz v0, :cond_1

    .line 563
    if-eqz p2, :cond_0

    .line 564
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 565
    iput-object p2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 566
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 568
    :cond_0
    const-string/jumbo v1, "MagicRecNotif"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Scribing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 571
    :cond_1
    return-void
.end method

.method private b(Landroid/content/Context;Lcfx;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 7

    .prologue
    .line 510
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 511
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 512
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 513
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->av_()Ljava/lang/String;

    move-result-object v3

    .line 516
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 517
    new-instance v5, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v5, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f02066b

    .line 518
    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const v6, 0x7f11010c

    .line 519
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    .line 520
    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 521
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 522
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 523
    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 524
    invoke-virtual {v0, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 525
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 528
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->g(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 529
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 530
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/NotificationCompat$Action;

    .line 531
    invoke-virtual {p0, p1, v1, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;ILandroid/support/v4/app/NotificationCompat$Action;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 532
    iget v5, v0, Landroid/support/v4/app/NotificationCompat$Action;->icon:I

    iget-object v0, v0, Landroid/support/v4/app/NotificationCompat$Action;->actionIntent:Landroid/app/PendingIntent;

    invoke-virtual {v2, v5, v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 529
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 534
    :cond_0
    invoke-static {p1, v2, p2}, Lcom/twitter/android/client/notifications/e;->a(Landroid/content/Context;Landroid/support/v4/app/NotificationCompat$Builder;Lcfx;)V

    .line 535
    return-object v2
.end method

.method static k(Landroid/content/Context;)Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 777
    const-string/jumbo v0, "window"

    .line 778
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 777
    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v0

    .line 779
    iget v0, v0, Landroid/graphics/Point;->x:I

    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Context;Lcfx;)Landroid/app/Notification;
    .locals 4

    .prologue
    .line 481
    .line 482
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->b(Landroid/content/Context;Lcfx;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 483
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 484
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 487
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->av_()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/res/Resources;Landroid/app/Notification;Ljava/lang/String;Z)V

    .line 490
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->b(Landroid/content/Context;Landroid/app/Notification;)V

    .line 491
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->q(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 494
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;Landroid/app/Notification;)V

    .line 495
    return-object v0
.end method

.method a(Landroid/content/Context;Lcfx;Ljava/util/Map;Landroid/graphics/Bitmap;)Landroid/app/Notification;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcfx;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/media/request/a;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")",
            "Landroid/app/Notification;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 400
    .line 403
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move-object v3, v2

    move v4, v7

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    .line 404
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    move v4, v8

    .line 408
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->b(Lcom/twitter/media/request/a;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v3, v0

    .line 413
    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->c(Lcom/twitter/media/request/a;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 418
    :goto_1
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    move-object v1, v0

    .line 422
    :cond_2
    if-eqz p4, :cond_4

    move v10, v8

    .line 423
    :goto_2
    if-nez v4, :cond_5

    if-nez v10, :cond_5

    move v0, v8

    .line 424
    :goto_3
    if-eqz v0, :cond_6

    .line 425
    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    .line 430
    const-string/jumbo v0, "preload_abort"

    invoke-static {p0, v0, v2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :goto_4
    return-object v2

    :cond_3
    move-object v1, v0

    .line 421
    goto :goto_0

    :cond_4
    move v10, v7

    .line 422
    goto :goto_2

    :cond_5
    move v0, v7

    .line 423
    goto :goto_3

    .line 433
    :cond_6
    if-eqz v3, :cond_7

    .line 434
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v3, v0

    .line 435
    :goto_5
    if-eqz v3, :cond_8

    move v5, v8

    .line 436
    :goto_6
    if-eqz v1, :cond_9

    .line 437
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    .line 438
    :goto_7
    if-eqz v1, :cond_a

    move v9, v8

    .line 439
    :goto_8
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 440
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->b(Landroid/content/Context;Lcfx;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 441
    invoke-virtual {v2, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    if-eqz v5, :cond_b

    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    invoke-direct {v0}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;-><init>()V

    .line 443
    invoke-virtual {v0, v3}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v0

    .line 442
    :goto_9
    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 445
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 448
    invoke-static {v2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/app/Notification;)V

    .line 452
    invoke-static {p1, v2, v1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;Landroid/app/Notification;Landroid/graphics/Bitmap;)V

    .line 455
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->av_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1, v5}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/res/Resources;Landroid/app/Notification;Ljava/lang/String;Z)V

    .line 458
    invoke-virtual {p0, p1, v2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->b(Landroid/content/Context;Landroid/app/Notification;)V

    .line 459
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->q(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, v2, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 462
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->v:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    move v6, v8

    :goto_a
    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    .line 463
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;Landroid/app/Notification;Ljava/util/Map;Landroid/graphics/Bitmap;ZZ)Z

    move-result v0

    .line 465
    if-eqz v10, :cond_d

    if-eqz v9, :cond_d

    if-eqz v5, :cond_d

    if-eqz v0, :cond_d

    .line 467
    :goto_b
    if-eqz v8, :cond_e

    const/4 v0, 0x6

    :goto_c
    iput v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    goto :goto_4

    :cond_7
    move-object v3, v2

    .line 434
    goto :goto_5

    :cond_8
    move v5, v7

    .line 435
    goto :goto_6

    :cond_9
    move-object v1, v2

    .line 437
    goto :goto_7

    :cond_a
    move v9, v7

    .line 438
    goto :goto_8

    .line 443
    :cond_b
    new-instance v3, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v3}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 444
    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    goto :goto_9

    :cond_c
    move v6, v7

    .line 462
    goto :goto_a

    :cond_d
    move v8, v7

    .line 465
    goto :goto_b

    .line 467
    :cond_e
    const/4 v0, 0x5

    goto :goto_c

    :cond_f
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/twitter/android/client/l;Lcfx;Landroid/graphics/Bitmap;)Landroid/app/Notification;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 274
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 275
    iget-object v6, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->g:Ljava/lang/Object;

    monitor-enter v6

    .line 277
    :try_start_0
    iget v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    if-nez v0, :cond_4

    .line 278
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    .line 280
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->i(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_0

    .line 282
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    .line 283
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;)V

    .line 284
    invoke-virtual {p0, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->b(Lcom/twitter/media/request/a;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_1
    move v2, v0

    .line 287
    goto :goto_0

    :cond_0
    move v2, v4

    .line 291
    :cond_1
    if-nez v2, :cond_2

    .line 292
    const-string/jumbo v0, "no_image"

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->e:Ljava/util/List;

    .line 297
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 298
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfs;

    .line 299
    iget-object v0, v0, Lcfs;->d:Ljava/lang/String;

    .line 300
    invoke-static {v5, v0, v2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/res/Resources;Ljava/lang/String;I)Lcom/twitter/media/request/a;

    move-result-object v0

    .line 301
    iget-object v8, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;)V

    goto :goto_2

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 307
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 308
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    .line 311
    :cond_4
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    iget v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    if-lt v0, v9, :cond_9

    .line 316
    iget v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    if-lt v0, v10, :cond_6

    move v0, v1

    .line 317
    :goto_3
    if-nez v0, :cond_8

    .line 318
    iput v10, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    .line 323
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 331
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 332
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_4
    if-ltz v2, :cond_7

    .line 333
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    .line 334
    iget-object v7, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->e:Ljava/util/Map;

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 335
    if-eqz v0, :cond_5

    .line 336
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    :cond_5
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_4

    :cond_6
    move v0, v4

    .line 316
    goto :goto_3

    .line 342
    :cond_7
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 343
    new-instance v0, Lcom/twitter/android/client/notifications/MagicRecNotif$a;

    invoke-direct {v0, p2, p0, v5, v6}, Lcom/twitter/android/client/notifications/MagicRecNotif$a;-><init>(Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/MagicRecNotif;Landroid/content/res/Resources;Ljava/util/List;)V

    .line 345
    invoke-virtual {p2, v0, p3}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/l$c;Lcfx;)V

    .line 352
    :goto_5
    if-eqz v1, :cond_8

    move-object v0, v3

    .line 362
    :goto_6
    return-object v0

    .line 358
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->e:Ljava/util/Map;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->i:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p3, v0, v1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;Lcfx;Ljava/util/Map;Landroid/graphics/Bitmap;)Landroid/app/Notification;

    move-result-object v0

    goto :goto_6

    .line 362
    :cond_9
    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;Lcfx;)Landroid/app/Notification;

    move-result-object v0

    goto :goto_6

    :cond_a
    move v1, v4

    goto :goto_5

    :cond_b
    move v0, v2

    goto/16 :goto_1
.end method

.method protected a(Landroid/content/Context;ILandroid/support/v4/app/NotificationCompat$Action;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p3, Landroid/support/v4/app/NotificationCompat$Action;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected abstract a(Lcom/twitter/media/request/a;)Ljava/lang/String;
.end method

.method protected a(Landroid/content/Context;Landroid/app/Notification;)V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 235
    iget-object v1, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 237
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->f:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->e:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    if-eqz p3, :cond_2

    .line 243
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "user_image"

    .line 245
    :goto_0
    const-string/jumbo v1, "image_downloaded"

    invoke-static {p0, v1, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    .line 259
    invoke-virtual {p1, p0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    .line 261
    :cond_0
    return-void

    .line 239
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 244
    :cond_1
    invoke-virtual {p0, p2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/media/request/a;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 249
    :cond_2
    const-string/jumbo v0, "image_download_failed"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected abstract a(Landroid/content/Context;Landroid/app/Notification;Ljava/util/Map;Landroid/graphics/Bitmap;ZZ)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/Notification;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/media/request/a;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            "ZZ)Z"
        }
    .end annotation
.end method

.method public ax_()Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method protected b(Landroid/content/Context;Landroid/app/Notification;)V
    .locals 4

    .prologue
    .line 583
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "status_bar_latest_event_content"

    const-string/jumbo v2, "id"

    const-string/jumbo v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 584
    iget-object v1, p2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v2, 0x15ca9d0

    const-string/jumbo v3, "collapsed"

    .line 585
    invoke-direct {p0, p1, v2, v3}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 584
    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 586
    iget-object v1, p2, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const v2, 0x15ca9d1

    const-string/jumbo v3, "expanded"

    .line 587
    invoke-direct {p0, p1, v2, v3}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 586
    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 588
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 592
    iget-object v1, p2, Landroid/app/Notification;->headsUpContentView:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_0

    .line 593
    iget-object v1, p2, Landroid/app/Notification;->headsUpContentView:Landroid/widget/RemoteViews;

    const v2, 0x15ca9d2

    const-string/jumbo v3, "heads_up"

    .line 594
    invoke-direct {p0, p1, v2, v3}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 593
    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 598
    :cond_0
    return-void
.end method

.method protected abstract b(Lcom/twitter/media/request/a;)Z
.end method

.method protected c(Lcom/twitter/media/request/a;)Z
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/MagicRecNotif;->b(Lcom/twitter/media/request/a;)Z

    move-result v0

    return v0
.end method

.method protected abstract i(Landroid/content/Context;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/media/request/a;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract j(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 265
    iget v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 369
    iget v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 370
    const-string/jumbo v0, "enhance_partial"

    invoke-static {p0, v0, v2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 372
    const-string/jumbo v0, "enhance"

    invoke-static {p0, v0, v2}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method n()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 824
    iget-object v2, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 826
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 827
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 828
    monitor-exit v2

    .line 832
    :goto_0
    return v1

    .line 831
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 832
    iget v0, p0, Lcom/twitter/android/client/notifications/MagicRecNotif;->j:I

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    .line 831
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    .line 832
    goto :goto_1
.end method
