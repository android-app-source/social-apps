.class Lcom/twitter/android/client/notifications/DMNotif$a;
.super Lcom/twitter/android/client/notifications/StatusBarNotif$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/notifications/DMNotif;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/client/notifications/DMNotif;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/client/notifications/DMNotif;Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 146
    iput-object p1, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->a:Lcom/twitter/android/client/notifications/DMNotif;

    .line 147
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;-><init>(Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V

    .line 148
    iget-object v0, p2, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(I)Lcom/twitter/util/collection/o;

    move-result-object v1

    .line 149
    iget-object v0, p2, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/h;

    .line 150
    iget-object v0, v0, Lcom/twitter/library/platform/notifications/h;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto :goto_0

    .line 152
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->e:Ljava/util/Set;

    .line 153
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 1

    .prologue
    .line 207
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->b:Lcfv;

    iget-object v0, v0, Lcfv;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v1, v1, Lcfw;->b:Lcfv;

    iget-object v1, v1, Lcfv;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2007

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 174
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 172
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 157
    iget-object v1, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->e:Ljava/util/Set;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->a:Lcom/twitter/android/client/notifications/DMNotif;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/DMNotif;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/platform/notifications/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 189
    const v0, 0x7f02065d

    return v0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->a:Lcom/twitter/android/client/notifications/DMNotif;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/DMNotif;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DMNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/twitter/android/client/notifications/DMNotif;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    const-string/jumbo v0, "message"

    return-object v0
.end method
