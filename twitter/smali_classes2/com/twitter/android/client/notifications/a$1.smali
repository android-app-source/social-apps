.class Lcom/twitter/android/client/notifications/a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/client/notifications/a;->a(Ljava/lang/String;JI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/b",
        "<",
        "Lcfx;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/android/client/notifications/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/notifications/a;Ljava/lang/String;IJ)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/android/client/notifications/a$1;->d:Lcom/twitter/android/client/notifications/a;

    iput-object p2, p0, Lcom/twitter/android/client/notifications/a$1;->a:Ljava/lang/String;

    iput p3, p0, Lcom/twitter/android/client/notifications/a$1;->b:I

    iput-wide p4, p0, Lcom/twitter/android/client/notifications/a$1;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcfx;)V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/client/notifications/a$1;->d:Lcom/twitter/android/client/notifications/a;

    .line 68
    invoke-static {v0}, Lcom/twitter/android/client/notifications/a;->a(Lcom/twitter/android/client/notifications/a;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/a$1;->a:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/client/notifications/a$1;->b:I

    invoke-static {v0, v1, v2}, Lcom/twitter/android/client/notifications/a;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/twitter/android/client/notifications/a$1;->d:Lcom/twitter/android/client/notifications/a;

    invoke-static {v1}, Lcom/twitter/android/client/notifications/a;->a(Lcom/twitter/android/client/notifications/a;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/twitter/android/client/notifications/e;->a(Landroid/content/Context;Landroid/support/v4/app/NotificationCompat$Builder;Lcfx;)V

    .line 71
    iget-object v1, p0, Lcom/twitter/android/client/notifications/a$1;->d:Lcom/twitter/android/client/notifications/a;

    invoke-static {v1}, Lcom/twitter/android/client/notifications/a;->b(Lcom/twitter/android/client/notifications/a;)Landroid/support/v4/app/NotificationManagerCompat;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/client/notifications/a$1;->c:J

    invoke-static {v2, v3}, Lcom/twitter/android/client/notifications/a;->b(J)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3f3

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationManagerCompat;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 72
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Lcfx;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/a$1;->a(Lcfx;)V

    return-void
.end method
