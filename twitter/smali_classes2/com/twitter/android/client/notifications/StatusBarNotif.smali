.class public abstract Lcom/twitter/android/client/notifications/StatusBarNotif;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/notifications/StatusBarNotif$b;,
        Lcom/twitter/android/client/notifications/StatusBarNotif$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/library/platform/notifications/r;

.field protected final b:J

.field protected final c:Ljava/lang/String;

.field protected final d:J

.field private final e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

.field private f:Lcfx;


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    .line 106
    sget-object v0, Lcom/twitter/library/platform/notifications/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v5

    move-object v0, p0

    .line 106
    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;J)V

    .line 108
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 94
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;J)V

    .line 95
    return-void
.end method

.method private constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;J)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 99
    iput-wide p2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    .line 100
    iput-object p4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    .line 101
    iput-wide p5, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:J

    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->g()Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 103
    return-void
.end method

.method private D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->i()Ljava/lang/String;

    move-result-object v0

    .line 726
    :goto_0
    return-object v0

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 724
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->k:Ljava/lang/String;

    goto :goto_0

    .line 726
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 661
    invoke-direct {p0, p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->b(Landroid/os/Bundle;)V

    .line 662
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/client/NotificationService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 663
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/twitter/database/schema/a$q;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget v2, v2, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 665
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 664
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 666
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 667
    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 5

    .prologue
    .line 739
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->f(Landroid/content/Context;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 740
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->q()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 741
    invoke-virtual {p2, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 742
    invoke-virtual {p2, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->q(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 743
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 744
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 745
    invoke-virtual {p2, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 746
    invoke-virtual {p2, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 747
    invoke-virtual {p2, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 748
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->av_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 749
    invoke-virtual {p2, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->l(Landroid/content/Context;)Landroid/support/v4/app/NotificationCompat$Style;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 750
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->s()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setNumber(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 751
    invoke-virtual {p2, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->g(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/NotificationCompat$Action;

    .line 752
    iget v3, v0, Landroid/support/v4/app/NotificationCompat$Action;->icon:I

    iget-object v4, v0, Landroid/support/v4/app/NotificationCompat$Action;->title:Ljava/lang/CharSequence;

    iget-object v0, v0, Landroid/support/v4/app/NotificationCompat$Action;->actionIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v3, v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_0

    .line 754
    :cond_0
    return-object v1
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/StatusBarNotif;
    .locals 4

    .prologue
    .line 679
    :try_start_0
    const-string/jumbo v0, "sb_notification"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    .line 682
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    if-nez v1, :cond_0

    .line 683
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Failed to deserialize StatusBarNotif"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    :goto_0
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 687
    :catch_0
    move-exception v0

    .line 688
    new-instance v1, Lcpb;

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Failed to deserialize StatusBarNotif"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v2, "exception"

    .line 689
    invoke-virtual {v1, v2, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 688
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0
.end method

.method private static a(Lcfu;Lcfv;Lcfv;)Lcom/twitter/model/core/Tweet;
    .locals 6

    .prologue
    .line 622
    new-instance v0, Lcom/twitter/model/core/Tweet$a;

    invoke-direct {v0}, Lcom/twitter/model/core/Tweet$a;-><init>()V

    iget-wide v2, p1, Lcfv;->b:J

    .line 623
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/Tweet$a;->f(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-object v1, p1, Lcfv;->c:Ljava/lang/String;

    .line 624
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-wide v2, p2, Lcfv;->b:J

    .line 625
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/Tweet$a;->d(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-object v1, p2, Lcfv;->c:Ljava/lang/String;

    .line 626
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-wide v2, p0, Lcfu;->b:J

    .line 627
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/Tweet$a;->g(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-wide v2, p0, Lcfu;->b:J

    .line 628
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/Tweet$a;->h(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-wide v2, p0, Lcfu;->c:J

    .line 629
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/Tweet$a;->e(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v1

    iget-wide v2, p0, Lcfu;->b:J

    iget-wide v4, p0, Lcfu;->c:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 630
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/Tweet$a;->c(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-object v1, p0, Lcfu;->e:Ljava/lang/String;

    .line 631
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v1

    new-instance v0, Lcom/twitter/model/core/v$a;

    invoke-direct {v0}, Lcom/twitter/model/core/v$a;-><init>()V

    iget-object v2, p0, Lcfu;->h:Lcom/twitter/model/core/f;

    .line 633
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/v$a;->b(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    move-result-object v0

    .line 634
    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    .line 632
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    .line 635
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet$a;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 622
    return-object v0

    .line 629
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/support/v4/app/NotificationCompat$Builder;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 895
    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    invoke-direct {v0}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;-><init>()V

    .line 896
    invoke-virtual {v0, p3}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v0

    .line 897
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v0

    .line 898
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v0

    .line 895
    invoke-virtual {p2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 899
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 671
    const-string/jumbo v0, "sb_account_id"

    iget-wide v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 672
    const-string/jumbo v0, "sb_notification"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 673
    return-void
.end method

.method private i(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 308
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->d(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 330
    :goto_0
    return-object v0

    .line 311
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->e(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 313
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 314
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->j()Ljava/lang/String;

    move-result-object v2

    .line 315
    if-eqz v0, :cond_2

    .line 316
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 317
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v3

    invoke-virtual {v3}, Lakn;->b()I

    move-result v3

    .line 318
    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 319
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 321
    :cond_1
    const-string/jumbo v3, "NotificationSettingsActivity_text"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "NotificationSettingsActivity_username"

    iget-object v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    .line 322
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "NotificationSettingsActivity_user_id"

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    .line 323
    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "NotificationSettingsActivity_notif_type"

    .line 324
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "NotificationSettingsActivity_scribe_component"

    .line 326
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->k()Ljava/lang/String;

    move-result-object v3

    .line 325
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "NotificationSettingsActivity_notif_random_id"

    .line 328
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 327
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    move-object v0, v1

    .line 330
    goto :goto_0
.end method

.method private n()I
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 337
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->h()I

    move-result v0

    .line 336
    :goto_0
    return v0

    .line 337
    :cond_0
    const/high16 v0, 0x14000000

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-boolean v0, v0, Lcom/twitter/library/platform/notifications/r;->f:Z

    return v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->k:Ljava/lang/String;

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/android/client/l;Lcfx;Landroid/graphics/Bitmap;)Landroid/app/Notification;
    .locals 4

    .prologue
    .line 835
    invoke-direct {p0, p1, p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 836
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 837
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->b()Ljava/lang/String;

    move-result-object v2

    .line 838
    if-eqz v2, :cond_0

    .line 839
    const v3, 0x1050006

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 840
    invoke-static {v2, v3}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;

    move-result-object v2

    const/4 v3, 0x1

    .line 841
    invoke-virtual {v2, v3}, Lcom/twitter/media/request/a$a;->b(Z)Lcom/twitter/media/request/a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v2

    .line 842
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v3

    invoke-virtual {p2, v2, v3}, Lcom/twitter/android/client/l;->a(Lcom/twitter/media/request/a;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 843
    if-eqz v2, :cond_0

    .line 844
    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 847
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 850
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->v()Ljava/lang/String;

    move-result-object v2

    .line 851
    if-eqz p4, :cond_2

    .line 853
    :goto_0
    if-eqz p4, :cond_1

    .line 854
    invoke-direct {p0, p1, v0, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Landroid/support/v4/app/NotificationCompat$Builder;Landroid/graphics/Bitmap;)V

    .line 857
    :cond_1
    invoke-static {p1, v0, p3}, Lcom/twitter/android/client/notifications/e;->a(Landroid/content/Context;Landroid/support/v4/app/NotificationCompat$Builder;Lcfx;)V

    .line 858
    const v2, 0x7f11010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 859
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 852
    :cond_2
    invoke-virtual {p0, p1, v2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v3

    .line 851
    invoke-virtual {p2, v2, v3}, Lcom/twitter/android/client/l;->a(Lcom/twitter/media/request/a;I)Landroid/graphics/Bitmap;

    move-result-object p4

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;ILcom/twitter/analytics/model/ScribeLog;Lcom/twitter/analytics/model/ScribeLog;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 265
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 266
    if-eqz p3, :cond_0

    .line 267
    const-string/jumbo v1, "notif_scribe_log"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 270
    :cond_0
    if-eqz p4, :cond_1

    .line 271
    const-string/jumbo v1, "notif_scribe_log_from_background"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 275
    :cond_1
    invoke-direct {p0, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->b(Landroid/os/Bundle;)V

    .line 276
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->i(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Landroid/content/Intent;II)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Landroid/content/Intent;II)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 282
    const-string/jumbo v0, "ref_event"

    const-string/jumbo v1, "notification::::open"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "AbsFragmentActivity_account_name"

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    .line 283
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 284
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->n()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 285
    invoke-static {p1}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    .line 286
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 287
    if-eqz v1, :cond_2

    .line 288
    invoke-virtual {v0, v1}, Landroid/support/v4/app/TaskStackBuilder;->addParentStack(Landroid/content/ComponentName;)Landroid/support/v4/app/TaskStackBuilder;

    .line 299
    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/app/TaskStackBuilder;->getIntentCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 300
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/TaskStackBuilder;->editIntentAt(I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "AbsFragmentActivity_account_name"

    iget-object v3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    :cond_1
    invoke-virtual {v0, p2}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    .line 303
    invoke-virtual {v0, p3, p4}, Landroid/support/v4/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    .line 293
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    .line 294
    if-eqz v1, :cond_0

    const-class v2, Lcom/twitter/app/main/MainActivity;

    .line 295
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 296
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    goto :goto_0
.end method

.method a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/analytics/feature/model/ClientEventLog;)Landroid/app/PendingIntent;
    .locals 6

    .prologue
    const/high16 v5, 0x10000000

    const/4 v4, 0x0

    .line 643
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 644
    invoke-direct {p0, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->b(Landroid/os/Bundle;)V

    .line 645
    const-string/jumbo v1, "notif_scribe_log"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 646
    const-string/jumbo v1, "notif_scribe_log_from_background"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 647
    sget-object v1, Lcom/twitter/database/schema/a$q;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget v2, v2, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 648
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 647
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 648
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 649
    if-eqz p2, :cond_0

    .line 651
    invoke-static {p1}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    .line 652
    invoke-virtual {v0, p2}, Landroid/support/v4/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    .line 653
    invoke-virtual {v0, p3}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    .line 654
    invoke-virtual {v0, v4}, Landroid/support/v4/app/TaskStackBuilder;->editIntentAt(I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "AbsFragmentActivity_account_name"

    iget-object v3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 655
    invoke-virtual {v0, v4, v5}, Landroid/support/v4/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v0

    .line 657
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, v4, p3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 704
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->D()Ljava/lang/String;

    move-result-object v1

    .line 705
    if-eqz v1, :cond_0

    .line 706
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v7, [Ljava/lang/String;

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "notification"

    aput-object v5, v4, v6

    const-string/jumbo v5, "status_bar"

    aput-object v5, v4, v7

    aput-object v0, v4, v8

    const/4 v0, 0x3

    aput-object v1, v4, v0

    const/4 v0, 0x4

    aput-object p1, v4, v0

    .line 707
    invoke-static {v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->b:Ljava/lang/String;

    .line 708
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 709
    invoke-virtual {v0, v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 710
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->aw_()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/model/ScribeItem;

    .line 711
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 716
    :cond_0
    return-object v0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 241
    invoke-static {p2}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcfx;)V
    .locals 0

    .prologue
    .line 816
    iput-object p1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->f:Lcfx;

    .line 817
    return-void
.end method

.method public a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 797
    return-void
.end method

.method public av_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method protected aw_()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 733
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public ax_()Z
    .locals 1

    .prologue
    .line 779
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->u()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    return-object v0
.end method

.method protected c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 763
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->v()Ljava/lang/String;

    move-result-object v3

    .line 764
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->o()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 765
    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->k:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 766
    :goto_0
    iget-object v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v4}, Lcom/twitter/library/platform/notifications/r;->f()Z

    move-result v4

    .line 767
    if-eqz v4, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    .line 768
    :goto_1
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 765
    goto :goto_0

    :cond_2
    move v0, v2

    .line 767
    goto :goto_1

    :cond_3
    move v1, v2

    .line 768
    goto :goto_2
.end method

.method public d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 863
    const/4 v0, 0x0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract e()I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end method

.method protected abstract e(Landroid/content/Context;)Landroid/content/Intent;
.end method

.method f(Landroid/content/Context;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1

    .prologue
    .line 759
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected g()Lcom/twitter/android/client/notifications/StatusBarNotif$a;
    .locals 6

    .prologue
    .line 137
    new-instance v0, Lcom/twitter/android/client/notifications/StatusBarNotif$b;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/android/client/notifications/StatusBarNotif$b;-><init>(Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V

    return-object v0
.end method

.method protected g(Landroid/content/Context;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/NotificationCompat$Action;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391
    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 392
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->v:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 616
    :goto_0
    return-object v0

    .line 396
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 397
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->D()Ljava/lang/String;

    move-result-object v4

    .line 398
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfr;

    .line 399
    iget-object v1, v0, Lcfr;->h:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 400
    iget v0, v0, Lcfr;->g:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 402
    :pswitch_1
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    .line 403
    iget-object v6, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v6, v6, Lcfw;->c:Lcfv;

    .line 404
    if-eqz v0, :cond_2

    if-eqz v6, :cond_2

    .line 407
    iget-object v7, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v7, v7, Lcfw;->d:Lcfv;

    .line 408
    invoke-static {v0, v6, v7}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcfu;Lcfv;Lcfv;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 409
    if-eqz v1, :cond_3

    .line 410
    const/4 v6, 0x0

    .line 411
    invoke-static {p1, v0, v6}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v6

    .line 410
    invoke-virtual {v1, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 414
    :cond_3
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v6

    .line 415
    invoke-virtual {v6, v0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v6, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    .line 416
    invoke-virtual {v0, v6}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 417
    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 418
    const-class v6, Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {p0, p1, v6, v0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/analytics/feature/model/ClientEventLog;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 420
    new-instance v6, Lcom/twitter/android/client/notifications/f;

    sget-object v7, Lcom/twitter/android/client/NotificationService;->c:Ljava/lang/String;

    invoke-direct {v6, p1, p0, v7}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 422
    invoke-virtual {v6, v1, v1}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v1

    const/4 v6, 0x1

    .line 423
    invoke-virtual {v1, v6}, Lcom/twitter/android/client/notifications/f;->a(Z)Lcom/twitter/android/client/notifications/f;

    move-result-object v1

    .line 424
    invoke-virtual {v1, v0}, Lcom/twitter/android/client/notifications/f;->a(Landroid/app/PendingIntent;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 425
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v1

    .line 426
    new-instance v6, Landroid/support/v4/app/NotificationCompat$Action;

    .line 427
    invoke-static {}, Lcmj;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f020667

    :goto_2
    const v7, 0x7f0a00c9

    .line 429
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v0, v7, v1}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 426
    invoke-virtual {v3, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 427
    :cond_4
    const v0, 0x7f020666

    goto :goto_2

    .line 434
    :pswitch_2
    iget-object v6, v2, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    .line 435
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    .line 436
    if-eqz v6, :cond_2

    if-eqz v0, :cond_2

    iget-boolean v1, v0, Lcfv;->h:Z

    if-nez v1, :cond_2

    .line 439
    iget-object v1, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v1, v1, Lcfw;->d:Lcfv;

    invoke-static {v6, v0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcfu;Lcfv;Lcfv;)Lcom/twitter/model/core/Tweet;

    move-result-object v7

    .line 442
    if-eqz v4, :cond_5

    .line 443
    const/4 v0, 0x0

    .line 444
    invoke-static {p1, v7, v0}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 445
    const-string/jumbo v0, "retweet"

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 446
    const-string/jumbo v8, "quote"

    invoke-virtual {p0, v8}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v8

    invoke-virtual {v8, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 451
    :goto_3
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 452
    const-string/jumbo v9, "ref_status_id"

    iget-wide v10, v6, Lcfu;->b:J

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 453
    const-string/jumbo v9, "status_id"

    iget-wide v10, v6, Lcfu;->c:J

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 459
    new-instance v6, Lcom/twitter/android/client/notifications/f;

    sget-object v9, Lcom/twitter/android/client/NotificationService;->d:Ljava/lang/String;

    invoke-direct {v6, p1, p0, v9}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 461
    invoke-virtual {v6, v0, v0}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const v6, 0x7f020668

    const v9, 0x7f0a060c

    .line 463
    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 462
    invoke-virtual {v0, v6, v9}, Lcom/twitter/android/client/notifications/f;->a(ILjava/lang/String;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 465
    invoke-virtual {v0, v8}, Lcom/twitter/android/client/notifications/f;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 470
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->a()Landroid/content/Intent;

    move-result-object v6

    .line 475
    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/twitter/android/dialog/NotifyRetweetDialogFragmentActivity;

    invoke-direct {v8, p1, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v9, "tweet"

    .line 477
    invoke-virtual {v8, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "retweet_service_intent"

    .line 478
    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v6

    .line 480
    const/4 v7, 0x0

    .line 481
    invoke-virtual {p0, p1, v7, v6, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/analytics/feature/model/ClientEventLog;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 487
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/notifications/f;->a(Landroid/app/PendingIntent;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 488
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/notifications/f;->a(Z)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 489
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v0

    .line 490
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v6, 0x7f020668

    const v7, 0x7f0a00ca

    .line 491
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 490
    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_1

    .line 448
    :cond_5
    const/4 v0, 0x0

    .line 449
    const/4 v1, 0x0

    goto :goto_3

    .line 496
    :pswitch_3
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    .line 497
    iget-object v6, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v6, v6, Lcfw;->c:Lcfv;

    .line 498
    if-eqz v0, :cond_2

    if-eqz v6, :cond_2

    .line 501
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 502
    const-string/jumbo v8, "status_id"

    iget-wide v10, v0, Lcfu;->c:J

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 503
    const-string/jumbo v8, "rt_status_id"

    iget-wide v10, v0, Lcfu;->b:J

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 504
    if-eqz v1, :cond_6

    .line 505
    iget-object v8, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v8, v8, Lcfw;->d:Lcfv;

    invoke-static {v0, v6, v8}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcfu;Lcfv;Lcfv;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 506
    const/4 v6, 0x0

    invoke-static {p1, v0, v6}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 509
    :cond_6
    new-instance v0, Lcom/twitter/android/client/notifications/f;

    sget-object v6, Lcom/twitter/android/client/NotificationService;->e:Ljava/lang/String;

    invoke-direct {v0, p1, p0, v6}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 511
    invoke-virtual {v0, v1, v1}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 512
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/notifications/f;->a(Z)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const v1, 0x7f020665

    const v6, 0x7f0a060b

    .line 514
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 513
    invoke-virtual {v0, v1, v6}, Lcom/twitter/android/client/notifications/f;->a(ILjava/lang/String;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 515
    invoke-virtual {v0, v7}, Lcom/twitter/android/client/notifications/f;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 516
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v0

    .line 517
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v6, 0x7f020665

    const v7, 0x7f0a00c3

    .line 518
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 517
    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_1

    .line 523
    :pswitch_4
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    .line 524
    if-eqz v0, :cond_2

    iget-boolean v6, v0, Lcfv;->i:Z

    if-nez v6, :cond_2

    .line 527
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 528
    const-string/jumbo v7, "user_id"

    iget-wide v8, v0, Lcfv;->b:J

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 529
    const-string/jumbo v7, "owner_id"

    iget-wide v8, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 531
    iget-boolean v0, v0, Lcfv;->h:Z

    if-eqz v0, :cond_7

    .line 532
    const v0, 0x7f0a060a

    .line 536
    :goto_4
    new-instance v7, Lcom/twitter/android/client/notifications/f;

    sget-object v8, Lcom/twitter/android/client/NotificationService;->f:Ljava/lang/String;

    invoke-direct {v7, p1, p0, v8}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 538
    invoke-virtual {v7, v1, v1}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v1

    const/4 v7, 0x1

    .line 539
    invoke-virtual {v1, v7}, Lcom/twitter/android/client/notifications/f;->a(Z)Lcom/twitter/android/client/notifications/f;

    move-result-object v1

    const v7, 0x7f020664

    .line 541
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 540
    invoke-virtual {v1, v7, v0}, Lcom/twitter/android/client/notifications/f;->a(ILjava/lang/String;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 542
    invoke-virtual {v0, v6}, Lcom/twitter/android/client/notifications/f;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 543
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v0

    .line 544
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v6, 0x7f020663

    const v7, 0x7f0a03a5

    .line 545
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 544
    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_1

    .line 534
    :cond_7
    const v0, 0x7f0a0607

    goto :goto_4

    .line 549
    :pswitch_5
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    .line 550
    if-eqz v0, :cond_2

    .line 553
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "@"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v0, v0, Lcfv;->c:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x0

    .line 554
    invoke-virtual {v6, v0, v7}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 555
    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 556
    const-class v6, Lcom/twitter/android/composer/ComposerActivity;

    invoke-virtual {p0, p1, v6, v0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/analytics/feature/model/ClientEventLog;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 558
    new-instance v6, Lcom/twitter/android/client/notifications/f;

    sget-object v7, Lcom/twitter/android/client/NotificationService;->i:Ljava/lang/String;

    invoke-direct {v6, p1, p0, v7}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 560
    invoke-virtual {v6, v1, v1}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v1

    const/4 v6, 0x1

    .line 561
    invoke-virtual {v1, v6}, Lcom/twitter/android/client/notifications/f;->a(Z)Lcom/twitter/android/client/notifications/f;

    move-result-object v1

    .line 562
    invoke-virtual {v1, v0}, Lcom/twitter/android/client/notifications/f;->a(Landroid/app/PendingIntent;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v0

    .line 564
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v6, 0x7f020377

    const v7, 0x7f0a060e

    .line 565
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 564
    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_1

    .line 570
    :pswitch_6
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    .line 571
    if-eqz v0, :cond_2

    .line 574
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 575
    const-string/jumbo v7, "user_id"

    iget-wide v8, v0, Lcfv;->b:J

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 576
    new-instance v0, Lcom/twitter/android/client/notifications/f;

    sget-object v7, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    invoke-direct {v0, p1, p0, v7}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 578
    invoke-virtual {v0, v1, v1}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 579
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/notifications/f;->a(Z)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const v1, 0x7f020661

    const v7, 0x7f0a0608

    .line 581
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 580
    invoke-virtual {v0, v1, v7}, Lcom/twitter/android/client/notifications/f;->a(ILjava/lang/String;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 583
    invoke-virtual {v0, v6}, Lcom/twitter/android/client/notifications/f;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 584
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v0

    .line 585
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v6, 0x7f020661

    const v7, 0x7f0a06fc

    .line 586
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 585
    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_1

    .line 591
    :pswitch_7
    iget-object v0, v2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    .line 592
    if-eqz v0, :cond_2

    .line 595
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 596
    const-string/jumbo v7, "user_id"

    iget-wide v8, v0, Lcfv;->b:J

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 597
    new-instance v0, Lcom/twitter/android/client/notifications/f;

    sget-object v7, Lcom/twitter/android/client/NotificationService;->h:Ljava/lang/String;

    invoke-direct {v0, p1, p0, v7}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 599
    invoke-virtual {v0, v1, v1}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 600
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/notifications/f;->a(Z)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    const v1, 0x7f020662

    const v7, 0x7f0a0609

    .line 602
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 601
    invoke-virtual {v0, v1, v7}, Lcom/twitter/android/client/notifications/f;->a(ILjava/lang/String;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 604
    invoke-virtual {v0, v6}, Lcom/twitter/android/client/notifications/f;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 605
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v0

    .line 606
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v6, 0x7f020662

    const v7, 0x7f0a06fd

    .line 607
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 606
    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_1

    .line 616
    :cond_8
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto/16 :goto_0

    .line 400
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected h(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    return-object v0
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x0

    return-object v0
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final l(Landroid/content/Context;)Landroid/support/v4/app/NotificationCompat$Style;
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 143
    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->e(Landroid/content/Context;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .line 143
    :cond_0
    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 144
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 808
    const/4 v0, 0x0

    return v0
.end method

.method public final m(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 151
    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 150
    :goto_0
    return-object v0

    .line 151
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public m()V
    .locals 0

    .prologue
    .line 906
    return-void
.end method

.method final n(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 162
    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    .line 162
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public o()J
    .locals 2

    .prologue
    .line 124
    iget-wide v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    return-wide v0
.end method

.method public final o(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->e()[I

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 174
    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    .line 174
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget v0, v0, Lcom/twitter/library/platform/notifications/r;->t:I

    return v0
.end method

.method public final p(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 250
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->w()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->x()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;ILcom/twitter/analytics/model/ScribeLog;Lcom/twitter/analytics/model/ScribeLog;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:J

    return-wide v0
.end method

.method public final q(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 374
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 375
    const-string/jumbo v1, "dismiss"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 376
    if-eqz v1, :cond_0

    .line 377
    const-string/jumbo v2, "notif_scribe_log"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 380
    :cond_0
    const-string/jumbo v1, "background_dismiss"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 381
    if-eqz v1, :cond_1

    .line 382
    const-string/jumbo v2, "notif_scribe_log_from_background"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 386
    :cond_1
    sget-object v1, Lcom/twitter/android/client/NotificationService;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 183
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->g()I

    move-result v0

    .line 182
    :goto_0
    return v0

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget v0, v0, Lcom/twitter/library/platform/notifications/r;->d:I

    goto :goto_0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 197
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->f()I

    move-result v0

    .line 196
    :goto_0
    return v0

    .line 197
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final t()I
    .locals 1
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 203
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->c()I

    move-result v0

    .line 202
    :goto_0
    return v0

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->e()I

    move-result v0

    goto :goto_0
.end method

.method protected u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    invoke-virtual {v0}, Lcfw;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->s:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final w()Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 347
    const-string/jumbo v0, "open"

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    sget-object v1, Lcom/twitter/library/platform/notifications/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 113
    iget-wide v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    iget-wide v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 116
    return-void
.end method

.method protected final x()Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 356
    const-string/jumbo v0, "background_open"

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    return-object v0
.end method

.method public y()[I
    .locals 3

    .prologue
    .line 696
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/StatusBarNotif$a;

    .line 697
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->e()[I

    move-result-object v0

    .line 696
    :goto_0
    return-object v0

    .line 697
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget v2, v2, Lcom/twitter/library/platform/notifications/r;->t:I

    aput v2, v0, v1

    goto :goto_0
.end method

.method public z()Lcfx;
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->f:Lcfx;

    return-object v0
.end method
