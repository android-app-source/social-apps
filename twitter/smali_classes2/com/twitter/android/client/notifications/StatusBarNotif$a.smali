.class public abstract Lcom/twitter/android/client/notifications/StatusBarNotif$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/notifications/StatusBarNotif;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# instance fields
.field protected final b:Lcom/twitter/library/platform/notifications/r;

.field protected final c:Ljava/lang/String;

.field protected final d:J


# direct methods
.method protected constructor <init>(Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917
    iput-object p1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    .line 918
    iput-object p2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->c:Ljava/lang/String;

    .line 919
    iput-wide p3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->d:J

    .line 920
    return-void
.end method

.method protected static a(Landroid/content/Context;Landroid/text/SpannableString;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 972
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1060005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 973
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 975
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/twitter/library/platform/notifications/h;)Landroid/text/SpannableString;
    .locals 2

    .prologue
    .line 954
    iget-object v0, p2, Lcom/twitter/library/platform/notifications/h;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 959
    :goto_0
    iget-object v1, p2, Lcom/twitter/library/platform/notifications/h;->e:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0

    .line 957
    :cond_0
    iget-object v0, p2, Lcom/twitter/library/platform/notifications/h;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 3

    .prologue
    .line 965
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2007

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 966
    invoke-static {p1, v0, p2}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a(Landroid/content/Context;Landroid/text/SpannableString;Ljava/lang/String;)V

    .line 967
    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 982
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()Z
.end method

.method public abstract b(Landroid/content/Context;)Ljava/lang/String;
.end method

.method protected b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/platform/notifications/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 926
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    return-object v0
.end method

.method public abstract c()I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end method

.method public abstract c(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract d(Landroid/content/Context;)Landroid/content/Intent;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public e(Landroid/content/Context;)Landroid/support/v4/app/NotificationCompat$InboxStyle;
    .locals 3

    .prologue
    .line 942
    new-instance v1, Landroid/support/v4/app/NotificationCompat$InboxStyle;

    invoke-direct {v1}, Landroid/support/v4/app/NotificationCompat$InboxStyle;-><init>()V

    .line 943
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/h;

    .line 944
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a(Landroid/content/Context;Lcom/twitter/library/platform/notifications/h;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    goto :goto_0

    .line 947
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    move-result-object v0

    .line 948
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    move-result-object v0

    .line 946
    return-object v0
.end method

.method public e()[I
    .locals 5

    .prologue
    .line 931
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b()Ljava/util/List;

    move-result-object v2

    .line 932
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 933
    new-array v4, v3, [I

    .line 934
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 935
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/h;

    iget v0, v0, Lcom/twitter/library/platform/notifications/h;->d:I

    aput v0, v4, v1

    .line 934
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 937
    :cond_0
    return-object v4
.end method

.method public f()I
    .locals 1

    .prologue
    .line 978
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget v0, v0, Lcom/twitter/library/platform/notifications/r;->o:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 990
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->b:Lcom/twitter/library/platform/notifications/r;

    iget v0, v0, Lcom/twitter/library/platform/notifications/r;->d:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 999
    const/high16 v0, 0x4000000

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1005
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
