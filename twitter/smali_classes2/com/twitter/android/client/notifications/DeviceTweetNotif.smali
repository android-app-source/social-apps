.class public Lcom/twitter/android/client/notifications/DeviceTweetNotif;
.super Lcom/twitter/android/client/notifications/TweetNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/client/notifications/DeviceTweetNotif;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final e:Lcom/twitter/android/client/notifications/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/android/client/notifications/DeviceTweetNotif$1;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/DeviceTweetNotif$1;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/TweetNotif;-><init>(Landroid/os/Parcel;)V

    .line 27
    new-instance v0, Lcom/twitter/android/client/notifications/j;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->e:Lcom/twitter/android/client/notifications/j;

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/TweetNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 27
    new-instance v0, Lcom/twitter/android/client/notifications/j;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->e:Lcom/twitter/android/client/notifications/j;

    .line 32
    return-void
.end method

.method private i(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 57
    const v0, 0x7f0a0721

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v3}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private j(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const v6, 0x7f0a0712

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->e:Lcom/twitter/android/client/notifications/j;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->b:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/client/notifications/j;->a(J)Ljava/lang/String;

    move-result-object v3

    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 83
    invoke-super {p0, p1}, Lcom/twitter/android/client/notifications/TweetNotif;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 62
    :sswitch_0
    const-string/jumbo v4, "cleaned"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v4, "challenger1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string/jumbo v4, "challenger2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 64
    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const v0, 0x7f0a070f

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 66
    invoke-virtual {v3}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 65
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 68
    :cond_1
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 72
    :pswitch_1
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 75
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v0

    .line 76
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->n()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 77
    const v3, 0x7f0a070d

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v1

    invoke-virtual {p1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 79
    :cond_2
    const v3, 0x7f0a0713

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v1

    invoke-virtual {p1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 62
    :sswitch_data_0
    .sparse-switch
        -0x2913025e -> :sswitch_1
        -0x2913025d -> :sswitch_2
        0x331154a8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private n()Z
    .locals 4

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    iget-wide v0, v0, Lcfu;->b:J

    iget-object v2, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    iget-wide v2, v2, Lcfu;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 124
    const v0, 0x7f0a05f6

    return v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget v0, v0, Lcom/twitter/library/platform/notifications/r;->i:I

    packed-switch v0, :pswitch_data_0

    .line 50
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 47
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x133
        :pswitch_0
    .end packed-switch
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const v6, 0x7f0a0711

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->e:Lcom/twitter/android/client/notifications/j;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->b:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/client/notifications/j;->a(J)Ljava/lang/String;

    move-result-object v4

    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 117
    invoke-super {p0, p1}, Lcom/twitter/android/client/notifications/TweetNotif;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 90
    :sswitch_0
    const-string/jumbo v5, "cleaned"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v5, "challenger1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string/jumbo v5, "challenger2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    goto :goto_0

    .line 92
    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 95
    :cond_1
    const v0, 0x7f0a0710

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v4}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v4, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v4, v4, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    .line 96
    invoke-virtual {v1, v4}, Lcom/twitter/library/platform/notifications/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    .line 95
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 100
    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    const v0, 0x7f0a070e

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 102
    invoke-virtual {v4}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v4, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v4, v4, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/twitter/library/platform/notifications/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    .line 101
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 104
    :cond_2
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 105
    invoke-virtual {v3}, Lcom/twitter/library/platform/notifications/r;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v3, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/twitter/library/platform/notifications/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 104
    invoke-virtual {p1, v6, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 109
    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    iget-object v0, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 112
    :cond_3
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    .line 113
    invoke-virtual {v3}, Lcom/twitter/library/platform/notifications/r;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v3, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/twitter/library/platform/notifications/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 112
    invoke-virtual {p1, v6, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 90
    :sswitch_data_0
    .sparse-switch
        -0x2913025e -> :sswitch_1
        -0x2913025d -> :sswitch_2
        0x331154a8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public e()I
    .locals 1

    .prologue
    .line 139
    const v0, 0x7f02066b

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const-string/jumbo v0, "tweet"

    return-object v0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 129
    const v0, 0x7f0a05f6

    return v0
.end method

.method protected h(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    const v0, 0x7f0a070c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected i()I
    .locals 1

    .prologue
    .line 134
    const v0, 0x7f0a0601

    return v0
.end method

.method protected j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "tweet_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->a:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v1}, Lcom/twitter/library/platform/notifications/r;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    const-string/jumbo v0, "tweet"

    return-object v0
.end method
