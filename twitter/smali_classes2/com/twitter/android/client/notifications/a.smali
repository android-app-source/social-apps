.class public Lcom/twitter/android/client/notifications/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/NotificationManagerCompat;

.field private final c:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;

.field private final d:Lrx/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;Lrx/f;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/twitter/android/client/notifications/a;->a:Landroid/content/Context;

    .line 56
    invoke-static {p1}, Landroid/support/v4/app/NotificationManagerCompat;->from(Landroid/content/Context;)Landroid/support/v4/app/NotificationManagerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/a;->b:Landroid/support/v4/app/NotificationManagerCompat;

    .line 57
    iput-object p2, p0, Lcom/twitter/android/client/notifications/a;->c:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;

    .line 58
    iput-object p3, p0, Lcom/twitter/android/client/notifications/a;->d:Lrx/f;

    .line 59
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 106
    invoke-static {p0}, Lcom/twitter/android/util/n;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ref_event"

    const-string/jumbo v2, "notification::::open"

    .line 107
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "AbsFragmentActivity_account_name"

    .line 108
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4000000

    .line 109
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 110
    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/notifications/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/client/notifications/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1

    .prologue
    .line 39
    invoke-static {p0, p1, p2}, Lcom/twitter/android/client/notifications/a;->b(Landroid/content/Context;Ljava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/twitter/android/client/notifications/a;
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lalg;->ag()Lalg;

    move-result-object v0

    invoke-virtual {v0}, Lalg;->c()Lcom/twitter/android/client/notifications/a;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;I)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 83
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 84
    const v1, 0x7f0c0004

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 85
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v2, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 87
    invoke-static {p0, p1}, Lcom/twitter/android/client/notifications/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 88
    invoke-virtual {v2, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v3, 0x7f02066b

    .line 89
    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 90
    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 91
    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 93
    invoke-virtual {v1, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setNumber(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 95
    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const v2, 0x7f11010c

    .line 96
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 85
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/client/notifications/a;)Landroid/support/v4/app/NotificationManagerCompat;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/client/notifications/a;->b:Landroid/support/v4/app/NotificationManagerCompat;

    return-object v0
.end method

.method static synthetic b(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/twitter/android/client/notifications/a;->c(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Lcom/twitter/android/client/notifications/g;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/g;-><init>()V

    invoke-virtual {v0, p0, p1}, Lcom/twitter/android/client/notifications/g;->a(J)Lcom/twitter/android/client/notifications/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/android/client/notifications/a;->b:Landroid/support/v4/app/NotificationManagerCompat;

    invoke-static {p1, p2}, Lcom/twitter/android/client/notifications/a;->c(J)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3f3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/NotificationManagerCompat;->cancel(Ljava/lang/String;I)V

    .line 78
    return-void
.end method

.method public a(Ljava/lang/String;JI)V
    .locals 8

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/client/notifications/a;->c:Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->a(J)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/a;->d:Lrx/f;

    .line 63
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v6

    new-instance v0, Lcom/twitter/android/client/notifications/a$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/a$1;-><init>(Lcom/twitter/android/client/notifications/a;Ljava/lang/String;IJ)V

    .line 64
    invoke-virtual {v6, v0}, Lrx/g;->a(Lrx/functions/b;)Lrx/j;

    .line 74
    return-void
.end method
