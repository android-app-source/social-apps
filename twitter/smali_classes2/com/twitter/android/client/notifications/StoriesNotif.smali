.class public Lcom/twitter/android/client/notifications/StoriesNotif;
.super Lcom/twitter/android/client/notifications/GenericNotif;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/notifications/StoriesNotif$b;,
        Lcom/twitter/android/client/notifications/StoriesNotif$a;
    }
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/client/notifications/StoriesNotif$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private h:Z

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/media/request/a;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/twitter/android/client/notifications/StoriesNotif$b;


# direct methods
.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/GenericNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->h:Z

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    .line 111
    return-void
.end method

.method private D()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 433
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 434
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v2

    .line 435
    :goto_0
    const/4 v0, 0x2

    if-ge v1, v0, :cond_0

    if-ge v1, v3, :cond_0

    .line 436
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->n()Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    invoke-virtual {v5, v0, v2, v1}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/client/notifications/StoriesNotif$a;ZI)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 438
    :cond_0
    return-object v4
.end method

.method static synthetic a(Lcom/twitter/android/client/notifications/StoriesNotif;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->f:I

    return p1
.end method

.method private a(Landroid/content/Context;Z)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 392
    new-instance v0, Lcom/twitter/android/highlights/StoryScribeItem;

    invoke-direct {v0}, Lcom/twitter/android/highlights/StoryScribeItem;-><init>()V

    invoke-virtual {v0, p2}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Z)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    .line 393
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->w()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 394
    if-eqz v1, :cond_0

    .line 395
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 397
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->x()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    .line 398
    if-eqz v2, :cond_1

    .line 399
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 405
    :cond_1
    new-instance v0, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    invoke-direct {v0, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    .line 406
    invoke-virtual {v0, v3, v4}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a(ZLjava/lang/String;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v3, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    .line 407
    invoke-virtual {v0, v3}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    move-result-object v0

    .line 408
    invoke-virtual {v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a()Landroid/content/Intent;

    move-result-object v0

    .line 409
    const-class v3, Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-virtual {p0, p1, v3, v0, v4}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/analytics/feature/model/ClientEventLog;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 411
    new-instance v3, Lcom/twitter/android/client/notifications/f;

    sget-object v4, Lcom/twitter/android/client/NotificationService;->j:Ljava/lang/String;

    invoke-direct {v3, p1, p0, v4}, Lcom/twitter/android/client/notifications/f;-><init>(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;)V

    .line 412
    invoke-virtual {v3, v1, v2}, Lcom/twitter/android/client/notifications/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/client/notifications/f;

    move-result-object v1

    .line 413
    invoke-virtual {v1, v0}, Lcom/twitter/android/client/notifications/f;->a(Landroid/app/PendingIntent;)Lcom/twitter/android/client/notifications/f;

    move-result-object v0

    .line 414
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/f;->b()Landroid/app/PendingIntent;

    move-result-object v0

    .line 411
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/notifications/StoriesNotif;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    return-object p1
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 323
    move v6, v9

    :goto_0
    const/4 v0, 0x2

    if-ge v6, v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_4

    .line 324
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    .line 331
    if-nez v6, :cond_2

    .line 332
    const v3, 0x7f1303f5

    .line 333
    const v2, 0x7f1303f6

    .line 334
    const v1, 0x7f1303f4

    .line 335
    const v0, 0x7f1303fd

    move v7, v1

    move v1, v2

    move v2, v3

    .line 342
    :goto_1
    iget-object v3, v4, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    .line 345
    if-eqz p3, :cond_0

    .line 346
    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 350
    :cond_0
    iget-object v0, v4, Lcom/twitter/android/client/notifications/StoriesNotif$a;->g:Lcom/twitter/media/request/a;

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    iget-object v5, v4, Lcom/twitter/android/client/notifications/StoriesNotif$a;->g:Lcom/twitter/media/request/a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 352
    if-eqz v0, :cond_1

    .line 353
    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 358
    :cond_1
    if-eqz p3, :cond_3

    .line 359
    iget-object v0, v4, Lcom/twitter/android/client/notifications/StoriesNotif$a;->f:Ljava/lang/String;

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 372
    :goto_2
    const-string/jumbo v0, "setVisibility"

    invoke-virtual {p2, v7, v0, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 376
    add-int/lit8 v0, v6, 0x2

    .line 377
    if-eqz p3, :cond_5

    .line 378
    add-int/lit8 v0, v0, 0x2

    move v8, v0

    .line 382
    :goto_3
    iget-wide v2, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->b:J

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Landroid/content/Context;JLcom/twitter/android/client/notifications/StoriesNotif$a;ZI)Landroid/content/Intent;

    move-result-object v0

    .line 383
    const/high16 v1, 0x10000000

    .line 384
    invoke-virtual {p0, p1, v0, v8, v1}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Landroid/content/Context;Landroid/content/Intent;II)Landroid/app/PendingIntent;

    move-result-object v0

    .line 383
    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 323
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 337
    :cond_2
    const v3, 0x7f1303f8

    .line 338
    const v2, 0x7f1303f9

    .line 339
    const v1, 0x7f1303f7

    .line 340
    const v0, 0x7f1303fe

    move v7, v1

    move v1, v2

    move v2, v3

    goto :goto_1

    .line 361
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v4, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v4, Lcom/twitter/android/client/notifications/StoriesNotif$a;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    const-string/jumbo v3, ":"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    .line 364
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 365
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v0, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v5, 0x21

    invoke-virtual {v3, v0, v9, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 367
    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_2

    .line 387
    :cond_4
    return-void

    :cond_5
    move v8, v0

    goto :goto_3
.end method

.method private a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;)V
    .locals 2

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->p()I

    move-result v0

    invoke-virtual {p1, p2, v0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/media/request/a;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    if-eqz v0, :cond_0

    const-string/jumbo v0, "image_downloaded"

    .line 211
    :goto_0
    const-string/jumbo v1, "tweet_media"

    invoke-static {p0, v0, v1}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-void

    .line 210
    :cond_0
    const-string/jumbo v0, "image_queued"

    goto :goto_0
.end method

.method public static a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 450
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_1

    .line 452
    if-eqz p2, :cond_0

    .line 453
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 454
    iput-object p2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 455
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 457
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 459
    :cond_1
    return-void
.end method

.method private n()Lcom/twitter/android/highlights/StoryScribeItem;
    .locals 3

    .prologue
    .line 423
    new-instance v0, Lcom/twitter/android/highlights/StoryScribeItem;

    invoke-direct {v0}, Lcom/twitter/android/highlights/StoryScribeItem;-><init>()V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    .line 424
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 423
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/highlights/StoryScribeItem;->a(II)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/android/client/l;Lcfx;Landroid/graphics/Bitmap;)Landroid/app/Notification;
    .locals 10

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->j:Lcom/twitter/android/client/notifications/StoriesNotif$b;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/twitter/android/client/notifications/StoriesNotif$b;

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->b:J

    move-object v1, p2

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/StoriesNotif$b;-><init>(Lcom/twitter/android/client/l;Lcom/twitter/android/client/notifications/StoriesNotif;Landroid/content/Context;J)V

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->j:Lcom/twitter/android/client/notifications/StoriesNotif$b;

    .line 225
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->j:Lcom/twitter/android/client/notifications/StoriesNotif$b;

    invoke-virtual {p2, v0, p3}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/l$c;Lcfx;)V

    .line 230
    :cond_0
    const/4 v0, 0x0

    .line 312
    :goto_0
    return-object v0

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->g:Z

    .line 236
    const-string/jumbo v0, "preload_aborted"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v0, 0x0

    goto :goto_0

    .line 240
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    monitor-enter v1

    .line 242
    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->h:Z

    if-nez v0, :cond_4

    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->h:Z

    .line 244
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    .line 245
    iget-object v3, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->g:Lcom/twitter/media/request/a;

    if-eqz v3, :cond_3

    .line 246
    iget-object v0, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->g:Lcom/twitter/media/request/a;

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;)V

    goto :goto_1

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->l()Z

    move-result v0

    if-nez v0, :cond_5

    .line 253
    const/4 v0, 0x0

    goto :goto_0

    .line 257
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 260
    const v0, 0x7f0a03f4

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 262
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_7

    .line 263
    iget v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->f:I

    if-lez v0, :cond_6

    .line 264
    const v3, 0x7f0a03f3

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    const/4 v6, 0x0

    .line 265
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    iget-object v0, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    iget-object v0, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x2

    iget v5, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->f:I

    .line 266
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    .line 264
    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 282
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 285
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 286
    new-instance v5, Landroid/widget/RemoteViews;

    const v6, 0x7f04014a

    invoke-direct {v5, v4, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 288
    const/4 v6, 0x0

    invoke-direct {p0, p1, v5, v6}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 291
    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v6, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 292
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->q()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 293
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/notifications/StoriesNotif;->q(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 294
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->r()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 295
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->t()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 296
    invoke-virtual {v6, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 297
    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 298
    invoke-virtual {v6, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    .line 299
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->av_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    .line 300
    invoke-virtual {v3, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const v5, 0x7f11010c

    .line 301
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 302
    invoke-static {p1, v1, p3}, Lcom/twitter/android/client/notifications/e;->a(Landroid/content/Context;Landroid/support/v4/app/NotificationCompat$Builder;Lcfx;)V

    .line 303
    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 304
    iget-object v3, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v5, 0x7f1303f3

    const/4 v6, 0x0

    .line 305
    invoke-direct {p0, p1, v6}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 304
    invoke-virtual {v3, v5, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 306
    new-instance v3, Landroid/widget/RemoteViews;

    const v5, 0x7f04014b

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 307
    const v4, 0x7f1303fa

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 308
    const v2, 0x7f1303fb

    invoke-virtual {v3, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 309
    const/4 v0, 0x1

    invoke-direct {p0, p1, v3, v0}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 310
    iput-object v3, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 311
    iget-object v0, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const v2, 0x7f1303f3

    const/4 v3, 0x1

    invoke-direct {p0, p1, v3}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    move-object v0, v1

    .line 312
    goto/16 :goto_0

    .line 268
    :cond_6
    const v3, 0x7f0a03f2

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    const/4 v6, 0x0

    .line 269
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    iget-object v0, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    iget-object v0, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    .line 268
    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 272
    :cond_7
    iget v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->f:I

    if-lez v0, :cond_8

    .line 273
    const v3, 0x7f0a03f1

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    const/4 v6, 0x0

    .line 274
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    iget-object v0, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget v5, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    .line 273
    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 276
    :cond_8
    const v3, 0x7f0a03f0

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    const/4 v6, 0x0

    .line 277
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;

    iget-object v0, v0, Lcom/twitter/android/client/notifications/StoriesNotif$a;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    .line 276
    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public a(Landroid/content/Context;JLcom/twitter/android/client/notifications/StoriesNotif$a;ZI)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 475
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "notification:status_bar::story:open_tap"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 477
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->n()Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v1, p4, p5, p6}, Lcom/twitter/android/highlights/StoryScribeItem;->a(Lcom/twitter/android/client/notifications/StoriesNotif$a;ZI)Lcom/twitter/android/highlights/StoryScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 479
    new-instance v1, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    invoke-direct {v1, p1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;-><init>(Landroid/content/Context;)V

    iget-object v2, p4, Lcom/twitter/android/client/notifications/StoriesNotif$a;->a:Ljava/lang/String;

    .line 480
    invoke-virtual {v1, v4, v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a(ZLjava/lang/String;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->a:Lcom/twitter/library/platform/notifications/r;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    .line 481
    invoke-virtual {v1, v2}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a(Ljava/lang/String;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    move-result-object v1

    .line 482
    invoke-virtual {v1, v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    move-result-object v0

    .line 483
    invoke-virtual {v0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a()Landroid/content/Intent;

    move-result-object v0

    .line 479
    return-object v0
.end method

.method public a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->h:Z

    if-nez v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    if-eqz p3, :cond_2

    .line 172
    iget-object v1, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    monitor-enter v1

    .line 173
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    const-string/jumbo v0, "image_downloaded"

    const-string/jumbo v1, "tweet_media"

    invoke-static {p0, v0, v1}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {p1, p0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 185
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    monitor-enter v1

    .line 188
    :try_start_2
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    .line 189
    iget-object v3, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 191
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_3
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 194
    const-string/jumbo v0, "image_download_failed"

    const-string/jumbo v1, "tweet_media"

    invoke-static {p0, v0, v1}, Lcom/twitter/android/client/notifications/StoriesNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected aw_()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    invoke-super {p0}, Lcom/twitter/android/client/notifications/GenericNotif;->aw_()Ljava/util/List;

    move-result-object v1

    .line 122
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 123
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StoriesNotif;->D()Ljava/util/List;

    move-result-object v0

    .line 124
    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-ne v1, v2, :cond_0

    .line 130
    :goto_0
    return-object v0

    .line 127
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public ax_()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    const-string/jumbo v0, "highlights"

    return-object v0
.end method

.method public l()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->g:Z

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x1

    .line 157
    :goto_0
    return v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->e:Ljava/util/List;

    if-nez v0, :cond_1

    move v0, v1

    .line 145
    goto :goto_0

    .line 148
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    monitor-enter v2

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 150
    if-nez v0, :cond_2

    .line 151
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 157
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/client/notifications/StoriesNotif;->h:Z

    monitor-exit v2

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
