.class public Lcom/twitter/android/client/notifications/d;
.super Lcom/twitter/android/client/notifications/StatusBarNotif$a;
.source "Twttr"


# direct methods
.method public constructor <init>(Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif$a;-><init>(Lcom/twitter/library/platform/notifications/r;Ljava/lang/String;J)V

    .line 21
    return-void
.end method

.method static a(ILjava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    packed-switch p0, :pswitch_data_0

    .line 73
    :goto_0
    return-object p1

    .line 64
    :pswitch_0
    const v0, 0x7f0a05f1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 67
    :pswitch_1
    const v0, 0x7f0a05f7

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 70
    :pswitch_2
    const v0, 0x7f0a05ef

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/twitter/library/platform/notifications/h;)Landroid/text/SpannableString;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p2, Lcom/twitter/library/platform/notifications/h;->f:Ljava/lang/String;

    iget v1, p2, Lcom/twitter/library/platform/notifications/h;->c:I

    iget-object v2, p2, Lcom/twitter/library/platform/notifications/h;->e:Ljava/lang/String;

    invoke-static {v1, v2, p1}, Lcom/twitter/android/client/notifications/d;->a(ILjava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/client/notifications/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 25
    iget-object v1, p0, Lcom/twitter/android/client/notifications/d;->b:Lcom/twitter/library/platform/notifications/r;

    iget v1, v1, Lcom/twitter/library/platform/notifications/r;->o:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 30
    const v0, 0x7f0a05f3

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/client/notifications/d;->b:Lcom/twitter/library/platform/notifications/r;

    iget v3, v3, Lcom/twitter/library/platform/notifications/r;->o:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 50
    const v0, 0x7f02066b

    return v0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 40
    invoke-static {p1}, Lcom/twitter/android/util/n;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string/jumbo v0, "interactions"

    return-object v0
.end method
