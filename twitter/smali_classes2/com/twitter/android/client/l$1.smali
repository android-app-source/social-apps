.class Lcom/twitter/android/client/l$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/client/l;->a(Lcom/twitter/library/platform/notifications/u;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/b",
        "<",
        "Lcfx;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/platform/notifications/u;

.field final synthetic b:Lcom/twitter/android/client/l;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iput-object p2, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcfx;)V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 228
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->b(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 229
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 230
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->d(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 231
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->e(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 232
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->f(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 233
    invoke-static {}, Lbsd;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->g(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 239
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/client/l$1;->b:Lcom/twitter/android/client/l;

    iget-object v1, p0, Lcom/twitter/android/client/l$1;->a:Lcom/twitter/library/platform/notifications/u;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/l;->h(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    .line 240
    return-void

    .line 236
    :cond_0
    const-string/jumbo v0, "NotificationController"

    const-string/jumbo v1, "Received Moments push payload, but feature switch is disabled. Ignoring push payload."

    invoke-static {v0, v1}, Lcqj;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 224
    check-cast p1, Lcfx;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l$1;->a(Lcfx;)V

    return-void
.end method
