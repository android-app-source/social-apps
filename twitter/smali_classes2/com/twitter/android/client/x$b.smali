.class Lcom/twitter/android/client/x$b;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/client/x$a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/x$a;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/x$b;->a:Landroid/content/Context;

    .line 139
    iput-object p2, p0, Lcom/twitter/android/client/x$b;->b:Lcom/twitter/android/client/x$a;

    .line 140
    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/k;Lcom/twitter/library/service/u;)V
    .locals 12

    .prologue
    .line 156
    new-instance v6, Lcom/twitter/android/client/notifications/c;

    iget-object v0, p0, Lcom/twitter/android/client/x$b;->a:Landroid/content/Context;

    invoke-direct {v6, v0}, Lcom/twitter/android/client/notifications/c;-><init>(Landroid/content/Context;)V

    .line 157
    iget-object v7, p2, Lcom/twitter/library/api/upload/k;->o:Landroid/os/Bundle;

    .line 158
    invoke-virtual {p3}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    .line 161
    if-eqz v1, :cond_7

    .line 162
    const-string/jumbo v0, "user"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    move-object v5, v0

    .line 167
    :goto_0
    if-eqz v1, :cond_8

    if-eqz v5, :cond_8

    const/4 v0, 0x1

    move v4, v0

    .line 168
    :goto_1
    if-eqz v5, :cond_9

    iget-wide v0, v5, Lcom/twitter/model/core/TwitterUser;->b:J

    move-wide v2, v0

    .line 170
    :goto_2
    const-string/jumbo v0, "avatar_media"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 171
    if-eqz v0, :cond_a

    if-eqz v4, :cond_a

    .line 172
    iget-object v1, p0, Lcom/twitter/android/client/x$b;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v1

    iget-object v8, v5, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    const/4 v9, -0x3

    .line 173
    invoke-static {v8, v9}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v8

    .line 172
    invoke-virtual {v1, v8}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/media/request/a;)Lrx/a;

    .line 178
    :goto_3
    new-instance v8, Lcom/twitter/android/profiles/g;

    iget-object v1, p0, Lcom/twitter/android/client/x$b;->a:Landroid/content/Context;

    invoke-direct {v8, v1}, Lcom/twitter/android/profiles/g;-><init>(Landroid/content/Context;)V

    .line 180
    invoke-virtual {p2}, Lcom/twitter/library/api/upload/k;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {p2}, Lcom/twitter/library/api/upload/k;->s()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v4, :cond_2

    .line 181
    :cond_1
    iget-wide v10, v5, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v8, v10, v11}, Lcom/twitter/android/profiles/g;->b(J)V

    .line 182
    iget-object v1, p0, Lcom/twitter/android/client/x$b;->a:Landroid/content/Context;

    iget-wide v10, v5, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v1, v10, v11}, Lcom/twitter/media/util/l;->d(Landroid/content/Context;J)Lrx/g;

    .line 186
    :cond_2
    const-string/jumbo v1, "header_media"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/model/MediaFile;

    .line 187
    if-eqz v1, :cond_3

    if-eqz v4, :cond_3

    .line 188
    iget-wide v10, v5, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v8, v10, v11}, Lcom/twitter/android/profiles/g;->a(J)V

    .line 192
    :cond_3
    if-eqz v0, :cond_4

    if-eqz v4, :cond_4

    .line 193
    invoke-virtual {v0}, Lcom/twitter/media/model/MediaFile;->c()Lrx/g;

    .line 196
    :cond_4
    invoke-virtual {p2}, Lcom/twitter/library/api/upload/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/twitter/android/client/x;->a(Lcom/twitter/library/client/Session;I)V

    .line 200
    if-eqz v4, :cond_b

    .line 201
    const v0, 0x7f0a0605

    .line 202
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    cmp-long v1, v2, v8

    if-nez v1, :cond_5

    .line 203
    invoke-virtual {p1, v5}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 206
    :cond_5
    iget-object v1, p0, Lcom/twitter/android/client/x$b;->b:Lcom/twitter/android/client/x$a;

    if-eqz v1, :cond_6

    .line 207
    iget-object v1, p0, Lcom/twitter/android/client/x$b;->b:Lcom/twitter/android/client/x$a;

    invoke-interface {v1}, Lcom/twitter/android/client/x$a;->a()V

    .line 228
    :cond_6
    :goto_4
    invoke-virtual {v6, v4, v0, v7, p1}, Lcom/twitter/android/client/notifications/c;->a(ZILandroid/os/Bundle;Lcom/twitter/library/client/Session;)V

    .line 229
    return-void

    .line 164
    :cond_7
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 165
    const-string/jumbo v2, "user"

    invoke-virtual {v7, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v5, v0

    goto/16 :goto_0

    .line 167
    :cond_8
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_1

    .line 168
    :cond_9
    const-wide/16 v0, 0x0

    move-wide v2, v0

    goto/16 :goto_2

    .line 175
    :cond_a
    invoke-static {}, Lcom/twitter/media/util/s;->a()Lcom/twitter/media/util/s;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lcom/twitter/media/util/s;->b(J)V

    goto :goto_3

    .line 210
    :cond_b
    iget-object v0, p0, Lcom/twitter/android/client/x$b;->b:Lcom/twitter/android/client/x$a;

    if-eqz v0, :cond_c

    .line 211
    iget-object v0, p0, Lcom/twitter/android/client/x$b;->b:Lcom/twitter/android/client/x$a;

    invoke-interface {v0}, Lcom/twitter/android/client/x$a;->b()V

    .line 213
    :cond_c
    invoke-virtual {p3}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 223
    const v0, 0x7f0a0604

    goto :goto_4

    .line 215
    :sswitch_0
    const v0, 0x7f0a0603

    .line 216
    goto :goto_4

    .line 219
    :sswitch_1
    const v0, 0x7f0a0602

    .line 220
    goto :goto_4

    .line 213
    nop

    :sswitch_data_0
    .sparse-switch
        0x1a6 -> :sswitch_1
        0x1f7 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 132
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/x$b;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 144
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 146
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 147
    iget-wide v0, v1, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 152
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 150
    check-cast v0, Lcom/twitter/library/api/upload/k;

    .line 151
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    .line 150
    invoke-direct {p0, v2, v0, v1}, Lcom/twitter/android/client/x$b;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/k;Lcom/twitter/library/service/u;)V

    goto :goto_0
.end method
