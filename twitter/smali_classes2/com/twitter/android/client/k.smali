.class public Lcom/twitter/android/client/k;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/client/k;


# instance fields
.field private final b:Lcom/twitter/library/client/v;

.field private c:Z

.field private final d:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/k;->b:Lcom/twitter/library/client/v;

    .line 50
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 51
    const-string/jumbo v1, "media_forward"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/client/k;->a(ZZ)V

    .line 53
    new-instance v1, Lcom/twitter/android/client/k$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/client/k$1;-><init>(Lcom/twitter/android/client/k;Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/twitter/android/client/k;->d:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 64
    iget-object v1, p0, Lcom/twitter/android/client/k;->d:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 65
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/android/client/k;
    .locals 3

    .prologue
    .line 35
    const-class v1, Lcom/twitter/android/client/k;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/k;->a:Lcom/twitter/android/client/k;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/twitter/android/client/k;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/client/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/client/k;->a:Lcom/twitter/android/client/k;

    .line 37
    const-class v0, Lcom/twitter/android/client/k;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 39
    :cond_0
    sget-object v0, Lcom/twitter/android/client/k;->a:Lcom/twitter/android/client/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/twitter/android/client/k;ZZ)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/k;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/twitter/android/client/k;->c:Z

    if-eq v0, p1, :cond_0

    .line 83
    iput-boolean p1, p0, Lcom/twitter/android/client/k;->c:Z

    .line 84
    if-eqz p2, :cond_0

    .line 85
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v0, p0, Lcom/twitter/android/client/k;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "settings::::"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p1, :cond_1

    const-string/jumbo v0, "enable_media_forward"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 86
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "network_type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 89
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",change"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 85
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 93
    :cond_0
    return-void

    .line 85
    :cond_1
    const-string/jumbo v0, "disable_media_forward"

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/twitter/android/client/k;->c:Z

    return v0
.end method
