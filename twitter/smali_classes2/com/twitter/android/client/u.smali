.class public Lcom/twitter/android/client/u;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/u$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/android/client/u;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Z

.field private final d:J

.field private e:Lcom/twitter/android/client/u$a;

.field private f:J

.field private g:Landroid/app/PendingIntent;

.field private h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/client/u;->b:Landroid/content/Context;

    .line 45
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "android.permission.READ_SMS"

    aput-object v4, v3, v1

    invoke-virtual {v2, p1, v3}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "sms_delivery_should_scribe_phone_number"

    .line 46
    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/client/u;->c:Z

    .line 47
    const-string/jumbo v0, "sms_delivery_auto_process_timeout"

    const-wide/16 v2, 0x12c

    .line 48
    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/android/client/u;->d:J

    .line 50
    return-void

    :cond_0
    move v0, v1

    .line 46
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/client/u;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/u;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/twitter/android/client/u;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/u;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/u;

    .line 56
    const-class v0, Lcom/twitter/android/client/u;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 58
    :cond_0
    sget-object v0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/u;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/client/u;->b:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 140
    iget-object v1, p0, Lcom/twitter/android/client/u;->g:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 141
    invoke-direct {p0, p1}, Lcom/twitter/android/client/u;->d(Ljava/lang/String;)V

    .line 142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/client/u;->f:J

    .line 143
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 146
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/client/u;->f:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    .line 148
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 149
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "app"

    aput-object v5, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v5, "sms_receiver"

    aput-object v5, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v5, "verification_code"

    aput-object v5, v0, v1

    const/4 v1, 0x3

    iget-object v5, p0, Lcom/twitter/android/client/u;->h:Ljava/lang/String;

    aput-object v5, v0, v1

    const/4 v1, 0x4

    aput-object p1, v0, v1

    .line 150
    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 151
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/client/u;->b:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/twitter/android/client/u;->c:Z

    .line 152
    invoke-virtual {v0, v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Landroid/content/Context;Z)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 153
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 87
    const-string/jumbo v0, "manual_entry"

    invoke-direct {p0, v0}, Lcom/twitter/android/client/u;->c(Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public a(Lcom/twitter/android/client/u$a;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/client/u;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/s;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 100
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/client/u;->e:Lcom/twitter/android/client/u$a;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 75
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/client/u;->f:J

    .line 76
    iput-object p1, p0, Lcom/twitter/android/client/u;->h:Ljava/lang/String;

    .line 77
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/u;->b:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/client/SmsReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "isTimeOut"

    const/4 v2, 0x1

    .line 78
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/twitter/android/client/u;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/u;->g:Landroid/app/PendingIntent;

    .line 80
    iget-object v0, p0, Lcom/twitter/android/client/u;->b:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 81
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/twitter/android/client/u;->f:J

    iget-wide v4, p0, Lcom/twitter/android/client/u;->d:J

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/twitter/android/client/u;->g:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 82
    const-string/jumbo v0, "start"

    invoke-direct {p0, v0}, Lcom/twitter/android/client/u;->d(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/client/u;->e:Lcom/twitter/android/client/u$a;

    .line 108
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/twitter/android/client/u;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 123
    const-string/jumbo v0, "received"

    invoke-direct {p0, v0}, Lcom/twitter/android/client/u;->c(Ljava/lang/String;)V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/u;->e:Lcom/twitter/android/client/u$a;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/twitter/android/client/u;->e:Lcom/twitter/android/client/u$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/client/u$a;->a(Ljava/lang/String;)V

    .line 128
    :cond_1
    return-void
.end method

.method public c()Z
    .locals 4

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/twitter/android/client/u;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 132
    iget-wide v0, p0, Lcom/twitter/android/client/u;->f:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 133
    const-string/jumbo v0, "timeout"

    invoke-direct {p0, v0}, Lcom/twitter/android/client/u;->d(Ljava/lang/String;)V

    .line 134
    iput-wide v2, p0, Lcom/twitter/android/client/u;->f:J

    .line 136
    :cond_0
    return-void
.end method
