.class public Lcom/twitter/android/client/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/i$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/core/TwitterUser;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/support/v4/app/FragmentManager;

.field private final d:Lwl;

.field private e:Z

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/util/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Landroid/support/v4/app/FragmentManager;Lwl;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/i;->f:Ljava/util/Map;

    .line 73
    iput-object p1, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    .line 75
    iput-object p3, p0, Lcom/twitter/android/client/i;->c:Landroid/support/v4/app/FragmentManager;

    .line 76
    iput-object p4, p0, Lcom/twitter/android/client/i;->d:Lwl;

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/i;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    const v1, 0x7f1308a6

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0d023a

    .line 138
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a02f5

    .line 139
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 140
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/i;->c:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "dm_tooltip"

    .line 141
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 142
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/client/i;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/android/client/i;->b()V

    return-void
.end method

.method private d(Ljava/lang/String;)Lcom/twitter/android/util/h;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    .line 214
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 236
    const/4 v0, 0x0

    .line 240
    :goto_1
    if-eqz v0, :cond_1

    .line 241
    iget-object v1, p0, Lcom/twitter/android/client/i;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_1
    return-object v0

    .line 214
    :sswitch_0
    const-string/jumbo v1, "highlights_tooltip_overflow"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "news_tooltip"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v3

    goto :goto_0

    :sswitch_2
    const-string/jumbo v1, "news_tooltip_in"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v1, "dm_tooltip"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 216
    :pswitch_0
    new-instance v0, Lcom/twitter/android/util/h;

    iget-object v1, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    const-string/jumbo v2, "highlights_overflow_fatigue"

    iget-object v6, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v6, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    goto :goto_1

    .line 221
    :pswitch_1
    new-instance v0, Lcom/twitter/android/util/h;

    iget-object v1, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    const-string/jumbo v2, "news_fatigue"

    iget-object v6, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v6, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    goto :goto_1

    .line 226
    :pswitch_2
    new-instance v0, Lcom/twitter/android/util/h;

    iget-object v1, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    const-string/jumbo v2, "news_fatigue_in"

    const/4 v3, 0x4

    const-wide/32 v4, 0x14997000

    iget-object v6, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v6, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    goto :goto_1

    .line 231
    :pswitch_3
    new-instance v0, Lcom/twitter/android/util/h;

    iget-object v1, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    const-string/jumbo v2, "dm_fatigue"

    iget-object v6, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v6, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    goto :goto_1

    .line 214
    nop

    :sswitch_data_0
    .sparse-switch
        -0x299115b3 -> :sswitch_3
        -0x3f55942 -> :sswitch_0
        0x7000d7cd -> :sswitch_2
        0x7e0c7e97 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/i;->e:Z

    .line 81
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const v4, 0x7f1307a9

    const v3, 0x7f0d023a

    const/4 v1, 0x1

    .line 89
    iget-boolean v0, p0, Lcom/twitter/android/client/i;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/i;->c:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/i;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iput-boolean v1, p0, Lcom/twitter/android/client/i;->e:Z

    .line 95
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 129
    :goto_2
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/i;->c(Ljava/lang/String;)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    goto :goto_0

    .line 95
    :sswitch_0
    const-string/jumbo v2, "highlights_tooltip_overflow"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v2, "news_tooltip"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "news_tooltip_in"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v2, "guide_tooltip"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    .line 97
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    const v2, 0x7f13005e

    invoke-static {v0, v2}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v2, 0x7f0a03fb

    .line 99
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/i;->c:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "highlights_tooltip_overflow"

    .line 101
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    goto :goto_2

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 106
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v2, 0x7f0a05d9

    .line 107
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/i;->c:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "news_tooltip"

    .line 109
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    goto :goto_2

    .line 113
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 114
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v2, 0x7f0a05da

    .line 115
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 116
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/i;->c:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "news_tooltip_in"

    .line 117
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    goto/16 :goto_2

    .line 121
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/client/i;->d:Lwl;

    invoke-virtual {v0}, Lwl;->b()V

    goto/16 :goto_2

    .line 95
    :sswitch_data_0
    .sparse-switch
        -0x3eff0a40 -> :sswitch_3
        -0x3f55942 -> :sswitch_0
        0x7000d7cd -> :sswitch_2
        0x7e0c7e97 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method b(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 152
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid tooltip name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :sswitch_0
    const-string/jumbo v3, "highlights_tooltip_overflow"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v3, "news_tooltip_in"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string/jumbo v3, "news_tooltip"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v3, "dm_tooltip"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string/jumbo v3, "guide_tooltip"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 154
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/i;->c(Ljava/lang/String;)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 156
    :goto_1
    new-instance v3, Lcom/twitter/util/a;

    iget-object v4, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v6, v5, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {v3, v4, v6, v7}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 157
    if-eqz v0, :cond_4

    invoke-static {v3}, Lcom/twitter/android/highlights/e;->a(Lcom/twitter/util/a;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 182
    :cond_2
    :goto_2
    return v2

    :cond_3
    move v0, v1

    .line 155
    goto :goto_1

    :cond_4
    move v2, v1

    .line 157
    goto :goto_2

    .line 161
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/i;->c(Ljava/lang/String;)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    move v0, v2

    .line 163
    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 164
    invoke-static {v4, v5}, Lcom/twitter/android/news/c;->b(J)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/client/i;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 165
    invoke-static {v4, v5}, Lbsi;->g(J)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_6
    move v2, v1

    goto :goto_2

    :cond_7
    move v0, v1

    .line 162
    goto :goto_3

    .line 168
    :pswitch_2
    const-string/jumbo v0, "dm_share_tweet_new_user_tooltip_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 169
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/i;->c(Ljava/lang/String;)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 170
    iget-object v2, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/dm/e;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 171
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 172
    iget-object v0, p0, Lcom/twitter/android/client/i;->b:Landroid/content/Context;

    check-cast v0, Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->i()Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    .line 173
    instance-of v2, v0, Lcom/twitter/app/home/HomeTimelineFragment;

    if-eqz v2, :cond_8

    .line 174
    new-instance v2, Lcom/twitter/android/client/i$a;

    check-cast v0, Lcom/twitter/app/home/HomeTimelineFragment;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/client/i$a;-><init>(Lcom/twitter/android/client/i;Lcom/twitter/app/home/HomeTimelineFragment;)V

    new-array v0, v1, [Ljava/lang/Void;

    .line 175
    invoke-virtual {v2, v0}, Lcom/twitter/android/client/i$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_8
    move v2, v1

    .line 179
    goto :goto_2

    .line 182
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/client/i;->d:Lwl;

    invoke-virtual {v0}, Lwl;->a()Z

    move-result v2

    goto :goto_2

    .line 152
    :sswitch_data_0
    .sparse-switch
        -0x3eff0a40 -> :sswitch_4
        -0x299115b3 -> :sswitch_3
        -0x3f55942 -> :sswitch_0
        0x7000d7cd -> :sswitch_1
        0x7e0c7e97 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method c(Ljava/lang/String;)Lcom/twitter/android/util/h;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/android/client/i;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/h;

    .line 199
    if-nez v0, :cond_0

    .line 200
    invoke-direct {p0, p1}, Lcom/twitter/android/client/i;->d(Ljava/lang/String;)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 202
    :cond_0
    return-object v0
.end method
