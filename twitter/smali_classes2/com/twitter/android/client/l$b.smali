.class Lcom/twitter/android/client/l$b;
.super Lcom/twitter/library/client/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/client/l;


# direct methods
.method private constructor <init>(Lcom/twitter/android/client/l;)V
    .locals 0

    .prologue
    .line 939
    iput-object p1, p0, Lcom/twitter/android/client/l$b;->a:Lcom/twitter/android/client/l;

    invoke-direct {p0}, Lcom/twitter/library/client/g;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/client/l;Lcom/twitter/android/client/l$1;)V
    .locals 0

    .prologue
    .line 939
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l$b;-><init>(Lcom/twitter/android/client/l;)V

    return-void
.end method

.method private a(Landroid/util/SparseArray;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/android/client/notifications/StatusBarNotif;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 954
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 955
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 956
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    .line 957
    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->o()J

    move-result-wide v4

    cmp-long v0, v4, p2

    if-nez v0, :cond_0

    .line 958
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 954
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 961
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 3

    .prologue
    .line 944
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 945
    iget-object v2, p0, Lcom/twitter/android/client/l$b;->a:Lcom/twitter/android/client/l;

    invoke-static {v2}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/twitter/android/client/l$b;->a(Landroid/util/SparseArray;J)V

    .line 946
    iget-object v2, p0, Lcom/twitter/android/client/l$b;->a:Lcom/twitter/android/client/l;

    invoke-static {v2}, Lcom/twitter/android/client/l;->b(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/twitter/android/client/l$b;->a(Landroid/util/SparseArray;J)V

    .line 947
    iget-object v0, p0, Lcom/twitter/android/client/l$b;->a:Lcom/twitter/android/client/l;

    invoke-static {v0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 948
    iget-object v0, p0, Lcom/twitter/android/client/l$b;->a:Lcom/twitter/android/client/l;

    invoke-static {v0}, Lcom/twitter/android/client/l;->d(Lcom/twitter/android/client/l;)Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/l$b;->a:Lcom/twitter/android/client/l;

    invoke-static {v1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/android/client/l;)Lcom/twitter/library/client/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 950
    :cond_0
    return-void
.end method
