.class public Lcom/twitter/android/client/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/a$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/l$a;,
        Lcom/twitter/android/client/l$b;,
        Lcom/twitter/android/client/l$c;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/android/client/l;


# instance fields
.field private b:Lcom/twitter/library/media/manager/g;

.field private final c:Landroid/content/Context;

.field private d:Landroid/app/NotificationManager;

.field private e:Lcom/twitter/library/client/v;

.field private final f:Lcom/twitter/android/client/l$a;

.field private final g:Lcom/twitter/library/client/u;

.field private final h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/android/client/notifications/StatusBarNotif;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/android/client/notifications/StatusBarNotif;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/twitter/library/client/p;

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/client/m;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Landroid/os/Handler;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 153
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v1

    .line 154
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 153
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/twitter/android/client/l;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/media/manager/g;Lcom/twitter/library/client/p;)V

    .line 155
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/media/manager/g;Lcom/twitter/library/client/p;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/l;->h:Landroid/util/SparseArray;

    .line 134
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    .line 137
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/l;->k:Ljava/util/List;

    .line 139
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/l;->l:Ljava/util/Map;

    .line 142
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/client/l;->m:Landroid/os/Handler;

    .line 159
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    .line 160
    const-string/jumbo v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    .line 161
    iput-object p2, p0, Lcom/twitter/android/client/l;->e:Lcom/twitter/library/client/v;

    .line 162
    iput-object p3, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/library/media/manager/g;

    .line 163
    iput-object p4, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    .line 164
    new-instance v0, Lcom/twitter/android/client/l$a;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/client/l$a;-><init>(Lcom/twitter/android/client/l;Lcom/twitter/android/client/l$1;)V

    iput-object v0, p0, Lcom/twitter/android/client/l;->f:Lcom/twitter/android/client/l$a;

    .line 165
    new-instance v0, Lcom/twitter/android/client/l$b;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/client/l$b;-><init>(Lcom/twitter/android/client/l;Lcom/twitter/android/client/l$1;)V

    iput-object v0, p0, Lcom/twitter/android/client/l;->g:Lcom/twitter/library/client/u;

    .line 166
    return-void
.end method

.method public static a(Landroid/content/Context;ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1

    .prologue
    .line 669
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 3

    .prologue
    .line 674
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f02066b

    .line 675
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 676
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 677
    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 678
    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 679
    invoke-virtual {v0, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 674
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/client/l;->h:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/android/client/l;
    .locals 3

    .prologue
    .line 145
    const-class v1, Lcom/twitter/android/client/l;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/l;->a:Lcom/twitter/android/client/l;

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Lcom/twitter/android/client/l;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/client/l;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/client/l;->a:Lcom/twitter/android/client/l;

    .line 147
    const-class v0, Lcom/twitter/android/client/l;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 149
    :cond_0
    sget-object v0, Lcom/twitter/android/client/l;->a:Lcom/twitter/android/client/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    .line 553
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->a(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private a(Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 548
    sget-object v0, Lcfx;->a:Lcfx;

    invoke-virtual {p0, v0, p1, p2}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    .line 549
    return-void
.end method

.method private a(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 6

    .prologue
    .line 256
    iget-object v0, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 257
    sget-object v1, Lrm;->a:Lrm;

    invoke-virtual {v1, p1}, Lrm;->a(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/client/l;->e:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 263
    iget-object v2, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 264
    iget-object v2, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    new-instance v3, Lcom/twitter/library/api/dm/j;

    iget-object v4, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    invoke-direct {v3, v4, v1}, Lcom/twitter/library/api/dm/j;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 267
    :cond_2
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/r;->e()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 272
    new-instance v1, Lcom/twitter/android/client/notifications/DMNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/DMNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 273
    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0
.end method

.method private b(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 729
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 730
    const-string/jumbo v1, "user_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 731
    const-string/jumbo v1, "username"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 732
    const-string/jumbo v4, "impression_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 733
    const-string/jumbo v5, "earned"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 734
    const-string/jumbo v6, "age_before_timestamp"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 736
    new-instance v8, Landroid/content/Intent;

    iget-object v9, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const-class v10, Lcom/twitter/android/AgeGateActivity;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v9, "user_id"

    .line 737
    invoke-virtual {v8, v9, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "user_name"

    .line 738
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "impression_id"

    .line 739
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "is_earned"

    .line 740
    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "age_gate_timestamp"

    .line 741
    invoke-virtual {v1, v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$z;->b:Landroid/net/Uri;

    .line 742
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    .line 743
    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 742
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 745
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 747
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/client/l;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->b(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private b(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 5

    .prologue
    .line 279
    iget-object v0, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 280
    iget v1, p1, Lcom/twitter/library/platform/notifications/u;->e:I

    .line 281
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/notifications/r;->h:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/notifications/r;->o:I

    if-lez v2, :cond_0

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_0

    .line 284
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/LoginVerificationNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/LoginVerificationNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 289
    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/client/l;)Lcom/twitter/library/client/u;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/client/l;->g:Lcom/twitter/library/client/u;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private c(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 293
    iget-object v8, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 294
    iget v2, p1, Lcom/twitter/library/platform/notifications/u;->e:I

    .line 295
    and-int/lit8 v3, v2, 0x2

    if-eqz v3, :cond_3

    move v7, v1

    .line 296
    :goto_0
    and-int/lit16 v3, v2, 0x800

    if-eqz v3, :cond_4

    move v6, v1

    .line 297
    :goto_1
    and-int/lit8 v3, v2, 0x8

    if-eqz v3, :cond_5

    move v5, v1

    .line 298
    :goto_2
    and-int/lit8 v3, v2, 0x10

    if-eqz v3, :cond_6

    move v4, v1

    .line 299
    :goto_3
    and-int/lit8 v3, v2, 0x20

    if-eqz v3, :cond_7

    move v3, v1

    .line 300
    :goto_4
    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_8

    move v2, v1

    .line 301
    :goto_5
    if-nez v7, :cond_0

    if-nez v6, :cond_0

    if-nez v5, :cond_0

    if-nez v4, :cond_0

    if-nez v3, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 303
    :cond_1
    if-eqz v8, :cond_2

    iget v1, v8, Lcom/twitter/library/platform/notifications/r;->h:I

    const/4 v9, 0x3

    if-ne v1, v9, :cond_2

    iget v1, v8, Lcom/twitter/library/platform/notifications/r;->o:I

    if-lez v1, :cond_2

    if-eqz v0, :cond_2

    .line 305
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 330
    :cond_2
    :goto_6
    return-void

    :cond_3
    move v7, v0

    .line 295
    goto :goto_0

    :cond_4
    move v6, v0

    .line 296
    goto :goto_1

    :cond_5
    move v5, v0

    .line 297
    goto :goto_2

    :cond_6
    move v4, v0

    .line 298
    goto :goto_3

    :cond_7
    move v3, v0

    .line 299
    goto :goto_4

    :cond_8
    move v2, v0

    .line 300
    goto :goto_5

    .line 310
    :cond_9
    if-eqz v7, :cond_a

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->f:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    .line 311
    new-instance v0, Lcom/twitter/android/client/notifications/MentionNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v8, v2, v3, v1}, Lcom/twitter/android/client/notifications/MentionNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 327
    :goto_7
    if-eqz v0, :cond_2

    .line 328
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_6

    .line 312
    :cond_a
    if-eqz v6, :cond_b

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->f:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_b

    .line 313
    new-instance v0, Lcom/twitter/android/client/notifications/ReplyNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v8, v2, v3, v1}, Lcom/twitter/android/client/notifications/ReplyNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_7

    .line 314
    :cond_b
    if-eqz v4, :cond_c

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->f:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_c

    .line 315
    new-instance v0, Lcom/twitter/android/client/notifications/FavoriteNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v8, v2, v3, v1}, Lcom/twitter/android/client/notifications/FavoriteNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_7

    .line 316
    :cond_c
    if-eqz v5, :cond_d

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->f:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_d

    .line 317
    new-instance v0, Lcom/twitter/android/client/notifications/RetweetNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v8, v2, v3, v1}, Lcom/twitter/android/client/notifications/RetweetNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_7

    .line 318
    :cond_d
    if-eqz v3, :cond_e

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->f:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_e

    .line 319
    new-instance v0, Lcom/twitter/android/client/notifications/FollowNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v8, v2, v3, v1}, Lcom/twitter/android/client/notifications/FollowNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_7

    .line 320
    :cond_e
    if-eqz v3, :cond_f

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->f:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_f

    .line 321
    new-instance v0, Lcom/twitter/android/client/notifications/FollowRequestNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v8, v2, v3, v1}, Lcom/twitter/android/client/notifications/FollowRequestNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_7

    .line 322
    :cond_f
    if-eqz v2, :cond_10

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->f:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_10

    .line 323
    new-instance v0, Lcom/twitter/android/client/notifications/MediaTagNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v8, v2, v3, v1}, Lcom/twitter/android/client/notifications/MediaTagNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_7

    .line 325
    :cond_10
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private c(Lcom/twitter/library/platform/notifications/u;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 246
    iget-object v0, p0, Lcom/twitter/android/client/l;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/m;

    .line 247
    invoke-interface {v0}, Lcom/twitter/android/client/m;->d()Lrn;

    move-result-object v4

    invoke-interface {v4, p1}, Lrn;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0, p1}, Lcom/twitter/android/client/m;->a(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    new-array v0, v1, [I

    iget-object v3, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget v3, v3, Lcom/twitter/library/platform/notifications/r;->t:I

    aput v3, v0, v2

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/android/client/l;->a([IJ)V

    move v0, v1

    .line 252
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/client/l;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/client/l;->e:Lcom/twitter/library/client/v;

    return-object v0
.end method

.method private d(J)Lcom/twitter/library/service/q;
    .locals 3

    .prologue
    .line 865
    new-instance v0, Lcom/twitter/library/service/q;

    iget-object v1, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/twitter/library/service/q;-><init>(Landroid/content/Context;J)V

    iget-object v1, p0, Lcom/twitter/android/client/l;->f:Lcom/twitter/android/client/l$a;

    .line 866
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/q;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/q;

    .line 865
    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->d(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private d(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 5

    .prologue
    .line 333
    iget-object v0, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 334
    iget v1, p1, Lcom/twitter/library/platform/notifications/u;->e:I

    .line 335
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/notifications/r;->h:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/notifications/r;->o:I

    if-lez v2, :cond_0

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 337
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/DeviceTweetNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 341
    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/client/l;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->e(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private e(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 5

    .prologue
    .line 345
    iget-object v0, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 346
    iget v1, p1, Lcom/twitter/library/platform/notifications/u;->e:I

    .line 347
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/notifications/r;->h:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/notifications/r;->o:I

    if-lez v2, :cond_0

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_0

    .line 350
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/LifelineTweetNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/LifelineTweetNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 356
    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->f(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private f(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 5

    .prologue
    .line 360
    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 361
    sget-object v0, Lro;->a:Lro;

    invoke-virtual {v0, p1}, Lro;->a(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/l;->l:Ljava/util/Map;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 368
    if-eqz v0, :cond_2

    .line 370
    iget-object v2, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 373
    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/client/l;->a(J)V

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/l;->l:Ljava/util/Map;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget v3, v1, Lcom/twitter/library/platform/notifications/r;->t:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    iget v0, v1, Lcom/twitter/library/platform/notifications/r;->h:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 379
    new-instance v0, Lcom/twitter/android/client/notifications/GenericNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/notifications/GenericNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 386
    :goto_1
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0

    .line 380
    :cond_3
    iget v0, v1, Lcom/twitter/library/platform/notifications/r;->h:I

    const/16 v2, 0x9

    if-ne v0, v2, :cond_4

    .line 382
    new-instance v0, Lcom/twitter/android/client/notifications/MagicRecTweetNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/notifications/MagicRecTweetNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_1

    .line 384
    :cond_4
    new-instance v0, Lcom/twitter/android/client/notifications/StoriesNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/notifications/StoriesNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic g(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->g(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private g(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 5

    .prologue
    .line 390
    iget-object v0, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 391
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/library/platform/notifications/r;->h:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/GenericNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/GenericNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 397
    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->h(Lcom/twitter/library/platform/notifications/u;Lcfx;)V

    return-void
.end method

.method private h(Lcom/twitter/library/platform/notifications/u;Lcfx;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 402
    iget-object v4, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 403
    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->e:I

    .line 404
    if-eqz v4, :cond_0

    iget v5, v4, Lcom/twitter/library/platform/notifications/r;->h:I

    if-ne v5, v2, :cond_0

    iget v5, v4, Lcom/twitter/library/platform/notifications/r;->o:I

    if-lez v5, :cond_0

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    .line 407
    invoke-direct {p0, p1}, Lcom/twitter/android/client/l;->c(Lcom/twitter/library/platform/notifications/u;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    invoke-static {v4}, Lcom/twitter/android/client/notifications/MagicRecTweetNotif;->a(Lcom/twitter/library/platform/notifications/r;)Z

    move-result v5

    .line 413
    invoke-static {v4}, Lcom/twitter/android/client/notifications/MagicRecFollowNotif;->a(Lcom/twitter/library/platform/notifications/r;)Z

    move-result v0

    .line 414
    if-nez v5, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    .line 415
    :goto_1
    invoke-static {v4}, Lcom/twitter/android/client/notifications/MagicRecHashtagNotif;->a(Lcom/twitter/library/platform/notifications/r;)Z

    move-result v6

    .line 416
    iget-object v7, v4, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 417
    if-eqz v7, :cond_4

    invoke-static {v7}, Lcom/twitter/android/highlights/r;->a(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 419
    :goto_2
    if-eqz v0, :cond_6

    .line 422
    if-eqz v5, :cond_5

    .line 423
    new-instance v0, Lcom/twitter/android/client/notifications/MagicRecTweetNotif;

    iget-wide v6, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v2, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v6, v7, v2}, Lcom/twitter/android/client/notifications/MagicRecTweetNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    .line 427
    :goto_3
    const-string/jumbo v2, "magic_rec_data_received"

    invoke-static {v0, v2, v3}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :goto_4
    if-nez v0, :cond_9

    .line 441
    invoke-virtual {p2, v1}, Lcfx;->a(Z)Lcfx;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/notifications/GenericNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v5, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v1, v4, v2, v3, v5}, Lcom/twitter/android/client/notifications/GenericNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 414
    goto :goto_1

    :cond_4
    move v2, v1

    .line 417
    goto :goto_2

    .line 425
    :cond_5
    new-instance v0, Lcom/twitter/android/client/notifications/MagicRecFollowNotif;

    iget-wide v6, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v2, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v6, v7, v2}, Lcom/twitter/android/client/notifications/MagicRecFollowNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_3

    .line 431
    :cond_6
    if-eqz v6, :cond_7

    .line 432
    new-instance v0, Lcom/twitter/android/client/notifications/MagicRecHashtagNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-object v5, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v2, v3, v5}, Lcom/twitter/android/client/notifications/MagicRecHashtagNotif;-><init>(Lcom/twitter/library/platform/notifications/r;JLjava/lang/String;)V

    goto :goto_4

    .line 433
    :cond_7
    if-eqz v2, :cond_8

    .line 434
    iget-object v0, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    new-instance v2, Lbco;

    iget-object v5, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    iget-object v6, p0, Lcom/twitter/android/client/l;->e:Lcom/twitter/library/client/v;

    .line 435
    invoke-virtual {v6}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lbco;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 436
    invoke-static {v7}, Lcom/twitter/android/highlights/r;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lbco;->a(Ljava/lang/String;)Lbco;

    move-result-object v2

    .line 434
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    :cond_8
    move-object v0, v3

    goto :goto_4

    .line 444
    :cond_9
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/a;I)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/library/media/manager/g;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/g;->c(Lcom/twitter/media/request/a;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 509
    if-nez v0, :cond_0

    .line 512
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/Object;)V

    .line 513
    invoke-virtual {p1, p0}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 514
    iget-object v1, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/library/media/manager/g;

    invoke-virtual {v1, p1}, Lcom/twitter/library/media/manager/g;->b(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;

    .line 516
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 645
    iget-object v0, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    .line 646
    new-instance v1, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    invoke-direct {v1}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;-><init>()V

    .line 647
    invoke-virtual {v1, v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x14000000

    .line 648
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 651
    const v2, 0x7f0a07c0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 652
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f02065e

    .line 653
    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    .line 654
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f11010c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    .line 655
    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    .line 656
    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v3, 0x7f0a07bf

    .line 657
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 658
    invoke-static {v0, v6, v1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 659
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 660
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 661
    iget-object v1, p0, Lcom/twitter/android/client/l;->e:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 662
    new-instance v1, Lcom/twitter/android/client/notifications/g;

    invoke-direct {v1}, Lcom/twitter/android/client/notifications/g;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/notifications/g;->a(J)Lcom/twitter/android/client/notifications/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/g;->toString()Ljava/lang/String;

    move-result-object v1

    .line 663
    iget-object v2, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v1, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 664
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/android/ContactsUploadService;->a(J)V

    .line 665
    return-void
.end method

.method protected a(I)V
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    .line 486
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 489
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 632
    iget-object v0, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->d(J)Lcom/twitter/library/service/q;

    move-result-object v1

    const/4 v2, 0x3

    .line 633
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/q;->a(I)Lcom/twitter/library/service/q;

    move-result-object v1

    .line 632
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 634
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/l;->b(J)V

    .line 635
    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 844
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->d(J)Lcom/twitter/library/service/q;

    move-result-object v0

    const/4 v1, 0x2

    .line 845
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/q;->a(I)Lcom/twitter/library/service/q;

    move-result-object v0

    .line 846
    invoke-virtual {v0, p3}, Lcom/twitter/library/service/q;->a(Ljava/lang/String;)Lcom/twitter/library/service/q;

    move-result-object v0

    .line 847
    iget-object v1, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 848
    return-void
.end method

.method public a(JZZ)V
    .locals 5

    .prologue
    const/16 v3, 0x200

    .line 795
    iget-object v0, p0, Lcom/twitter/android/client/l;->e:Lcom/twitter/library/client/v;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 796
    new-instance v1, Lbbf;

    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lbbf;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 797
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lbbf;->d(I)Lcom/twitter/library/service/j;

    .line 798
    if-eqz p4, :cond_0

    .line 799
    invoke-virtual {v1, v3}, Lbbf;->a(I)Lbbf;

    .line 803
    :goto_0
    iput-boolean p3, v1, Lbbf;->g:Z

    .line 804
    const/4 v0, 0x1

    iput-boolean v0, v1, Lbbf;->c:Z

    .line 805
    iget-object v0, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 806
    return-void

    .line 801
    :cond_0
    invoke-virtual {v1, v3}, Lbbf;->c(I)Lbbf;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V
    .locals 6

    .prologue
    .line 683
    iget-object v0, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    .line 684
    const v1, 0x7f0a0054

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 685
    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const/4 v3, 0x0

    .line 686
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->b(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;

    move-result-object v4

    const/high16 v5, 0x10000000

    invoke-static {v0, v3, v4, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 685
    invoke-static {v2, v1, v0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 688
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 690
    new-instance v1, Lcom/twitter/android/client/notifications/g;

    invoke-direct {v1}, Lcom/twitter/android/client/notifications/g;-><init>()V

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/notifications/g;->a(J)Lcom/twitter/android/client/notifications/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/g;->toString()Ljava/lang/String;

    move-result-object v1

    .line 691
    iget-object v2, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    const/16 v3, 0x3ef

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v2, v1, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 692
    return-void
.end method

.method a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 571
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v3

    .line 572
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->y()[I

    move-result-object v1

    .line 573
    array-length v4, v1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_1

    aget v5, v1, v0

    .line 574
    if-eq v5, v3, :cond_0

    .line 575
    const-string/jumbo v6, "NotificationController"

    const-string/jumbo v7, "Notification already displaying, removing old one"

    invoke-static {v6, v7}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    iget-object v6, p0, Lcom/twitter/android/client/l;->h:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->remove(I)V

    .line 577
    iget-object v6, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    invoke-virtual {v6, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 573
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 586
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    invoke-virtual {p2, v0, p0, p1, p3}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Lcom/twitter/android/client/l;Lcfx;Landroid/graphics/Bitmap;)Landroid/app/Notification;

    move-result-object v0

    .line 587
    invoke-virtual {p0, p2, p1}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Lcfx;)V

    .line 591
    if-eqz v0, :cond_3

    .line 592
    iget-object v1, p0, Lcom/twitter/android/client/l;->h:Landroid/util/SparseArray;

    invoke-virtual {v1, v3, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 593
    iget-object v1, p0, Lcom/twitter/android/client/l;->e:Lcom/twitter/library/client/v;

    iget-object v4, p0, Lcom/twitter/android/client/l;->g:Lcom/twitter/library/client/u;

    invoke-virtual {v1, v4}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 595
    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    invoke-virtual {v1, v3, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    :goto_1
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m()V

    .line 604
    const-string/jumbo v0, "NotificationController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "NotificationManager has been told to notify id "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    sget-object v0, Lcfx;->a:Lcfx;

    if-ne p1, v0, :cond_5

    const/4 v0, 0x1

    .line 608
    :goto_2
    if-nez v0, :cond_3

    .line 609
    invoke-static {p2}, Lcom/twitter/android/client/notifications/MagicRecTweetNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 610
    invoke-static {p2}, Lcom/twitter/android/client/notifications/MagicRecFollowNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 613
    :cond_2
    instance-of v0, p2, Lcom/twitter/android/client/notifications/MagicRecNotif;

    if-eqz v0, :cond_6

    const-string/jumbo v0, "rich"

    .line 615
    :goto_3
    const-string/jumbo v1, "impression"

    invoke-static {p2, v1, v0}, Lcom/twitter/android/client/notifications/MagicRecNotif;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :cond_3
    :goto_4
    return-void

    .line 596
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 597
    const-string/jumbo v0, "fail"

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 598
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a()Ljava/lang/String;

    move-result-object v0

    .line 599
    :goto_5
    new-instance v4, Lcpb;

    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->o()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Lcpb;-><init>(J)V

    const-string/jumbo v5, "notification-type"

    .line 600
    invoke-virtual {v4, v5, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 601
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 599
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_1

    .line 598
    :cond_4
    const-string/jumbo v0, "unknown"

    goto :goto_5

    :cond_5
    move v0, v2

    .line 607
    goto :goto_2

    .line 613
    :cond_6
    const-string/jumbo v0, "simple"

    goto :goto_3

    .line 617
    :cond_7
    const-string/jumbo v0, "impression"

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 618
    if-eqz v0, :cond_3

    .line 619
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_4
.end method

.method public a(Lcom/twitter/android/client/l$c;Lcfx;)V
    .locals 3

    .prologue
    .line 459
    invoke-virtual {p1}, Lcom/twitter/android/client/l$c;->a()Lcom/twitter/android/client/notifications/StatusBarNotif;

    move-result-object v0

    .line 460
    invoke-virtual {v0, p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcfx;)V

    .line 461
    iget-object v1, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 462
    iget-object v0, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 463
    return-void
.end method

.method public a(Lcom/twitter/android/client/m;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/android/client/l;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method public a(Lcom/twitter/android/client/notifications/StatusBarNotif;)V
    .locals 3

    .prologue
    .line 525
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->z()Lcfx;

    move-result-object v1

    .line 526
    if-nez v1, :cond_0

    .line 527
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Notification should have its config saved before calling this method"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 533
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v0

    .line 534
    iget-object v2, p0, Lcom/twitter/android/client/l;->h:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 535
    :goto_0
    if-eqz v0, :cond_2

    .line 536
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l;->b(Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    .line 540
    :goto_1
    return-void

    .line 534
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 538
    :cond_2
    invoke-direct {p0, v1, p1}, Lcom/twitter/android/client/l;->a(Lcfx;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_1
.end method

.method protected a(Lcom/twitter/android/client/notifications/StatusBarNotif;Lcfx;)V
    .locals 3

    .prologue
    .line 469
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->ax_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 471
    invoke-virtual {p1, p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcfx;)V

    .line 474
    iget-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    .line 475
    if-nez v0, :cond_0

    .line 476
    const-string/jumbo v0, "NotificationController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Preloading began for notification "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    iget-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 480
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 809
    iget-object v0, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    .line 810
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/LoginActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    .line 811
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 812
    invoke-static {v0, v6, v1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 813
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 814
    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const v3, 0x7f0a073e

    new-array v4, v7, [Ljava/lang/Object;

    .line 816
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 815
    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const v2, 0x7f0a073d

    .line 817
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 818
    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 819
    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 820
    new-instance v1, Lcom/twitter/android/client/notifications/g;

    invoke-direct {v1}, Lcom/twitter/android/client/notifications/g;-><init>()V

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/notifications/g;->a(J)Lcom/twitter/android/client/notifications/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/g;->toString()Ljava/lang/String;

    move-result-object v1

    .line 821
    iget-object v2, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    const/16 v3, 0x3f0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v2, v1, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 822
    return-void
.end method

.method public a(Lcom/twitter/library/platform/notifications/u;)V
    .locals 4

    .prologue
    .line 212
    iget-object v0, p1, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p1, Lcom/twitter/library/platform/notifications/u;->e:I

    if-nez v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    invoke-static {}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->a()Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/client/notifications/repository/NotificationsAlertConfigRepository;->a(J)Lrx/g;

    move-result-object v0

    .line 217
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/l$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/client/l$2;-><init>(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;)V

    .line 218
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/l$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/client/l$1;-><init>(Lcom/twitter/android/client/l;Lcom/twitter/library/platform/notifications/u;)V

    .line 224
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/b;)Lrx/j;

    goto :goto_0
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 4

    .prologue
    .line 170
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    .line 171
    const-string/jumbo v1, "NotificationController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Handling loaded image request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->A()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 173
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 176
    invoke-virtual {p0, v2, v0, v1}, Lcom/twitter/android/client/l;->a(ILcom/twitter/media/request/a;Landroid/graphics/Bitmap;)Z

    move-result v3

    .line 177
    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    .line 178
    iget-object v3, p0, Lcom/twitter/android/client/l;->h:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/client/notifications/StatusBarNotif;

    .line 179
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->ax_()Z

    move-result v3

    if-nez v3, :cond_0

    .line 180
    invoke-direct {p0, v2, v1}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    .line 183
    :cond_0
    const-string/jumbo v1, "NotificationController"

    const-string/jumbo v2, "Image(s) loaded."

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 185
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 105
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 699
    new-array v0, v6, [Ljava/lang/Object;

    aput-object p1, v0, v5

    invoke-static {p3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 701
    iget-object v1, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const-string/jumbo v3, "type_event_start_cricket_activity"

    const/4 v4, 0x2

    .line 703
    invoke-static {v2, v3, v4}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 702
    invoke-static {v1, p2, v2}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 707
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v2, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v2}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 708
    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 709
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 710
    invoke-virtual {v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const v1, 0x7f020368

    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const v3, 0x7f0a094c

    .line 712
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const-string/jumbo v4, "type_event_disable"

    .line 713
    invoke-static {v3, v4, v5}, Lcom/twitter/android/platform/TwitterAccessCwcNotificationReceiver;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 711
    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 716
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 718
    iget-object v1, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3f1

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 719
    return-void
.end method

.method public a([IJ)V
    .locals 2

    .prologue
    .line 857
    invoke-direct {p0, p2, p3}, Lcom/twitter/android/client/l;->d(J)Lcom/twitter/library/service/q;

    move-result-object v0

    .line 858
    invoke-virtual {v0, p1}, Lcom/twitter/library/service/q;->a([I)Lcom/twitter/library/service/q;

    move-result-object v0

    const/4 v1, 0x0

    .line 859
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/q;->a(I)Lcom/twitter/library/service/q;

    move-result-object v0

    .line 860
    iget-object v1, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 861
    return-void
.end method

.method a(ILcom/twitter/media/request/a;Landroid/graphics/Bitmap;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    .line 198
    if-nez v0, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 203
    :goto_0
    return v0

    .line 201
    :cond_0
    invoke-virtual {v0, p0, p2, p3}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcom/twitter/android/client/l;Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;)V

    .line 202
    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l;->a(I)V

    .line 203
    const/4 v0, 0x1

    goto :goto_0
.end method

.method a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 756
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 762
    :cond_0
    :goto_0
    return v0

    .line 759
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    .line 760
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 761
    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    .line 762
    if-eqz v1, :cond_0

    const-class v2, Lcom/twitter/android/UrlInterpreterActivity;

    .line 763
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 725
    iget-object v0, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    const/16 v1, 0x3f1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 726
    return-void
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 638
    iget-object v0, p0, Lcom/twitter/android/client/l;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    return-void
.end method

.method public b(Lcom/twitter/android/client/m;)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/android/client/l;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 193
    return-void
.end method

.method b(Lcom/twitter/android/client/notifications/StatusBarNotif;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 544
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    .line 545
    return-void
.end method

.method public b(Lcom/twitter/library/platform/notifications/u;)V
    .locals 6

    .prologue
    const/high16 v5, 0x10000000

    const/4 v4, 0x1

    .line 770
    iget-object v0, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 771
    iget-object v1, v0, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/l;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 772
    new-instance v1, Lcom/twitter/library/util/InvalidDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid logged out notification uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 789
    :goto_0
    return-void

    .line 776
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/UrlInterpreterActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "android.intent.action.VIEW"

    .line 777
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    .line 778
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 779
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 780
    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 782
    iget-object v2, p0, Lcom/twitter/android/client/l;->c:Landroid/content/Context;

    iget-object v3, v0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    .line 783
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v2, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v2}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    .line 784
    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 785
    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x2

    .line 786
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 787
    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 788
    iget-object v1, p0, Lcom/twitter/android/client/l;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3f2

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method protected b(I)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/android/client/l;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(J)V
    .locals 3

    .prologue
    .line 830
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/l;->d(J)Lcom/twitter/library/service/q;

    move-result-object v0

    const/4 v1, 0x1

    .line 831
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/q;->a(I)Lcom/twitter/library/service/q;

    move-result-object v0

    .line 832
    iget-object v1, p0, Lcom/twitter/android/client/l;->j:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 834
    invoke-static {}, Lcom/twitter/android/client/notifications/a;->a()Lcom/twitter/android/client/notifications/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/client/notifications/a;->a(J)V

    .line 835
    return-void
.end method
