.class Lcom/twitter/android/client/x$c;
.super Lcom/twitter/android/composer/y;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)V
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/composer/y;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;Z)V

    .line 241
    iput-object p1, p0, Lcom/twitter/android/client/x$c;->a:Landroid/content/Context;

    .line 242
    iput-object p2, p0, Lcom/twitter/android/client/x$c;->b:Lcom/twitter/library/client/Session;

    .line 243
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    invoke-super {p0, p1}, Lcom/twitter/android/composer/y;->a(Lcom/twitter/async/service/j;)V

    .line 249
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 251
    invoke-virtual {p0}, Lcom/twitter/android/client/x$c;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/twitter/android/client/x$c;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/x$c;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    .line 254
    :cond_0
    return-void
.end method
