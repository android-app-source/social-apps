.class Lcom/twitter/android/client/OpenUriHelper$a;
.super Lcom/twitter/android/client/f$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/client/OpenUriHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/twitter/library/client/BrowserDataSource;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V
    .locals 0

    .prologue
    .line 660
    invoke-direct {p0}, Lcom/twitter/android/client/f$a;-><init>()V

    .line 661
    iput-object p1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    .line 662
    iput-object p2, p0, Lcom/twitter/android/client/OpenUriHelper$a;->b:Ljava/lang/String;

    .line 663
    iput-object p3, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    .line 664
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 711
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    invoke-interface {v0}, Lcom/twitter/library/client/BrowserDataSource;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    .line 712
    invoke-interface {v0}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 713
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->am:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    .line 714
    invoke-interface {v1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v1

    .line 713
    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    .line 714
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 713
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 715
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->an:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    .line 716
    invoke-interface {v1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v1

    .line 715
    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    .line 716
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 715
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 717
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->ao:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    .line 718
    invoke-interface {v1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v1

    .line 717
    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    .line 718
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 717
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 720
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 668
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/client/chrome/a;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V

    .line 698
    :goto_0
    return-void

    .line 671
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 672
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/k;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 676
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 677
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 678
    const-string/jumbo v3, "year_class"

    invoke-static {}, Lcob;->a()Lcob;

    move-result-object v4

    invoke-virtual {v4}, Lcob;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 679
    const-string/jumbo v3, "os_version"

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    const-string/jumbo v3, "manufacturer"

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "chrome::::fallback"

    aput-object v4, v0, v1

    .line 682
    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 683
    invoke-static {v2}, Lmg;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 684
    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    if-eqz v1, :cond_1

    .line 685
    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    .line 687
    invoke-interface {v2}, Lcom/twitter/library/client/BrowserDataSource;->d()Lcom/twitter/library/scribe/ScribeItemsProvider;

    move-result-object v2

    .line 686
    invoke-static {v0, v1, v2, v5, v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/library/scribe/ScribeItemsProvider;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 689
    :cond_1
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 692
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/client/OpenUriHelper$a;->c:Lcom/twitter/library/client/BrowserDataSource;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/BrowserDataSource;)V

    goto/16 :goto_0

    .line 695
    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/client/OpenUriHelper$a;->c()V

    .line 696
    iget-object v0, p0, Lcom/twitter/android/client/OpenUriHelper$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/OpenUriHelper$a;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 702
    const/4 v0, 0x1

    return v0
.end method
