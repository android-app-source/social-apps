.class public Lcom/twitter/android/client/r;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/client/r$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/android/client/r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/twitter/model/search/viewmodel/g;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/twitter/android/client/r$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/client/r$a;-><init>(Lcom/twitter/android/client/r$1;)V

    sput-object v0, Lcom/twitter/android/client/r;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Lcom/twitter/model/search/viewmodel/g;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/twitter/android/client/r;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 57
    iput-object p2, p0, Lcom/twitter/android/client/r;->c:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    .line 59
    return-void
.end method

.method public static a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)Lcom/twitter/android/client/r;
    .locals 4

    .prologue
    .line 37
    instance-of v0, p0, Lcom/twitter/model/search/viewmodel/h;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/twitter/model/search/viewmodel/h;

    .line 38
    invoke-virtual {v0}, Lcom/twitter/model/search/viewmodel/h;->a()Lcom/twitter/model/search/viewmodel/g;

    move-result-object v0

    .line 40
    :goto_0
    new-instance v1, Lcom/twitter/android/client/r;

    invoke-virtual {p0}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/twitter/android/client/r;-><init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Lcom/twitter/model/search/viewmodel/g;)V

    return-object v1

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/android/client/r;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/android/client/r;

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->a:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/twitter/android/client/r;-><init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Lcom/twitter/model/search/viewmodel/g;)V

    return-object v0
.end method
