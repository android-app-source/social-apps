.class Lcom/twitter/android/GeoDebugActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/maps/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/GeoDebugActivity;->j()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/GeoDebugActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/GeoDebugActivity;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/c;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 158
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;Lcom/google/android/gms/maps/c;)Lcom/google/android/gms/maps/c;

    .line 159
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/c$e;)V

    .line 160
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/c;->a(Z)V

    .line 161
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/h;->b(Z)V

    .line 162
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/h;->c(Z)V

    .line 163
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/h;->d(Z)V

    .line 164
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/h;->a(Z)V

    .line 165
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/c$c;)V

    .line 166
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->b(Lcom/twitter/android/GeoDebugActivity;)Lbqo;

    move-result-object v0

    invoke-virtual {v0}, Lbqo;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-static {v0}, Lcom/twitter/android/GeoDebugActivity;->c(Lcom/twitter/android/GeoDebugActivity;)Lbqn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity$1;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-virtual {v0, v1}, Lbqn;->a(Lbqk;)V

    .line 169
    :cond_0
    return-void
.end method
