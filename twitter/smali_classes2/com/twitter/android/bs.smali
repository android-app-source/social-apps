.class public Lcom/twitter/android/bs;
.super Lcom/twitter/app/common/list/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/bs$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/android/bs;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/bs;

    invoke-direct {v0, p0}, Lcom/twitter/android/bs;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/bs$a;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/bs$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/bs$a;-><init>(Lcom/twitter/android/bs;)V

    return-object v0
.end method

.method public b()I
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/bs;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "search_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/bs;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "recent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/bs;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "search_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public synthetic h()Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/bs;->a()Lcom/twitter/android/bs$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/twitter/app/common/base/b$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/bs;->a()Lcom/twitter/android/bs$a;

    move-result-object v0

    return-object v0
.end method
