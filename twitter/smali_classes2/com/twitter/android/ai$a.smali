.class public Lcom/twitter/android/ai$a;
.super Lcom/twitter/app/common/timeline/c$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ai;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/timeline/c$a",
        "<",
        "Lcom/twitter/android/ai$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/c$a;-><init>(Landroid/os/Bundle;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/ai;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/c$a;-><init>(Lcom/twitter/app/common/timeline/c;)V

    .line 74
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/android/ai$a;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/ai$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-object p0
.end method

.method public a()Lcom/twitter/android/ai;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lcom/twitter/android/ai;

    iget-object v1, p0, Lcom/twitter/android/ai$a;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/android/ai;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/ai$a;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/ai$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "hashtag"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    return-object p0
.end method

.method public synthetic b()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/ai$a;->a()Lcom/twitter/android/ai;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/ai$a;->a()Lcom/twitter/android/ai;

    move-result-object v0

    return-object v0
.end method
