.class Lcom/twitter/android/ck$10;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ck;->c(Lcom/twitter/model/core/Tweet;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/util/SparseArray;

.field final synthetic b:Landroid/support/v4/app/FragmentActivity;

.field final synthetic c:Lcom/twitter/model/core/r;

.field final synthetic d:Lcom/twitter/model/core/Tweet;

.field final synthetic e:J

.field final synthetic f:J

.field final synthetic g:Lcom/twitter/android/ck;


# direct methods
.method constructor <init>(Lcom/twitter/android/ck;Landroid/util/SparseArray;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/r;Lcom/twitter/model/core/Tweet;JJ)V
    .locals 0

    .prologue
    .line 653
    iput-object p1, p0, Lcom/twitter/android/ck$10;->g:Lcom/twitter/android/ck;

    iput-object p2, p0, Lcom/twitter/android/ck$10;->a:Landroid/util/SparseArray;

    iput-object p3, p0, Lcom/twitter/android/ck$10;->b:Landroid/support/v4/app/FragmentActivity;

    iput-object p4, p0, Lcom/twitter/android/ck$10;->c:Lcom/twitter/model/core/r;

    iput-object p5, p0, Lcom/twitter/android/ck$10;->d:Lcom/twitter/model/core/Tweet;

    iput-wide p6, p0, Lcom/twitter/android/ck$10;->e:J

    iput-wide p8, p0, Lcom/twitter/android/ck$10;->f:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 657
    sget-object v1, Lcom/twitter/android/ck$2;->a:[I

    iget-object v0, p0, Lcom/twitter/android/ck$10;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v0}, Lcom/twitter/model/core/TweetActionType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 674
    const/4 v1, 0x0

    .line 675
    iget-object v0, p0, Lcom/twitter/android/ck$10;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TweetActionType;

    .line 676
    if-nez v0, :cond_1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 677
    :goto_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unhandled QuoteView Long Click Choice:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 682
    :goto_1
    if-eqz v0, :cond_0

    .line 683
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ck$10;->f:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ck$10;->g:Lcom/twitter/android/ck;

    iget-object v3, v3, Lcom/twitter/android/ck;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "share_sheet"

    const-string/jumbo v5, "quoted_tweet"

    .line 684
    invoke-static {v3, v4, v5, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 683
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 687
    :cond_0
    return-void

    .line 659
    :pswitch_0
    const-string/jumbo v0, "share_via_dm"

    .line 660
    iget-object v1, p0, Lcom/twitter/android/ck$10;->g:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/ck$10;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/twitter/android/ck$10;->c:Lcom/twitter/model/core/r;

    iget-object v4, p0, Lcom/twitter/android/ck$10;->d:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/ck;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/r;Ljava/lang/String;)V

    goto :goto_1

    .line 664
    :pswitch_1
    const-string/jumbo v0, "share"

    .line 665
    iget-object v1, p0, Lcom/twitter/android/ck$10;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/ck$10;->c:Lcom/twitter/model/core/r;

    invoke-static {v1, v2, v6}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/r;Z)V

    goto :goto_1

    .line 669
    :pswitch_2
    const-string/jumbo v0, "click"

    .line 670
    iget-object v1, p0, Lcom/twitter/android/ck$10;->g:Lcom/twitter/android/ck;

    iget-wide v2, p0, Lcom/twitter/android/ck$10;->e:J

    iget-object v4, p0, Lcom/twitter/android/ck$10;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/ck;->a(JLandroid/support/v4/app/FragmentActivity;)V

    goto :goto_1

    .line 676
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/model/core/TweetActionType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 657
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
