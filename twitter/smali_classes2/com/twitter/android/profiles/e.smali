.class public Lcom/twitter/android/profiles/e;
.super Lcom/twitter/android/profiles/r;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/profiles/d;


# instance fields
.field private final f:Landroid/widget/ListView;

.field private final g:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/v;Lcom/twitter/android/profiles/t;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/profiles/r;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/v;Lcom/twitter/android/profiles/t;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 35
    iput-object p5, p0, Lcom/twitter/android/profiles/e;->g:Landroid/view/View;

    .line 36
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->g:Landroid/view/View;

    const v1, 0x7f130697

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    .line 38
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 39
    return-void
.end method

.method private a(Z)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    if-nez v0, :cond_0

    move v0, v2

    .line 186
    :goto_0
    return v0

    .line 164
    :cond_0
    if-eqz p1, :cond_1

    .line 165
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0}, Lcom/twitter/android/bh;->getCount()I

    move-result v0

    :goto_1
    move v3, v2

    move v4, v2

    .line 169
    :goto_2
    if-ge v3, v0, :cond_3

    .line 171
    if-eqz p1, :cond_2

    .line 172
    iget-object v1, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v3, v5, v6}, Lcom/twitter/android/bh;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 173
    invoke-virtual {v1, v2, v2}, Landroid/view/View;->measure(II)V

    .line 177
    :goto_3
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v4, v1

    .line 169
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    goto :goto_1

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_3

    .line 180
    :cond_3
    if-nez p1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    invoke-virtual {v1}, Lcom/twitter/android/bh;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 181
    add-int/lit8 v0, v0, 0x1

    .line 184
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v1

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, v1

    add-int/2addr v0, v4

    .line 186
    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/profiles/e;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/profiles/e;ZZ)Z
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/profiles/e;->a(ZZ)Z

    move-result v0

    return v0
.end method

.method private a(ZZ)Z
    .locals 2

    .prologue
    .line 199
    invoke-direct {p0, p2}, Lcom/twitter/android/profiles/e;->a(Z)I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_1

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 202
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    .line 203
    const/4 v0, 0x1

    .line 205
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/bh;->a(Landroid/database/Cursor;)V

    .line 110
    :cond_0
    return-void
.end method

.method public b()Lcom/twitter/android/bh;
    .locals 7

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/twitter/android/bj;

    iget-object v1, p0, Lcom/twitter/android/profiles/e;->c:Landroid/content/Context;

    const v2, 0x7f0200b0

    iget-object v3, p0, Lcom/twitter/android/profiles/e;->b:Lcom/twitter/android/profiles/t;

    .line 61
    invoke-virtual {v3}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v4

    const/4 v5, 0x1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bj;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Z)V

    .line 62
    invoke-virtual {v0, p0}, Lcom/twitter/android/bj;->a(Lcom/twitter/android/av;)V

    .line 63
    new-instance v1, Lcom/twitter/android/bh;

    iget-object v2, p0, Lcom/twitter/android/profiles/e;->c:Landroid/content/Context;

    .line 64
    invoke-virtual {p0}, Lcom/twitter/android/profiles/e;->d()I

    move-result v4

    const/16 v5, 0x13

    .line 68
    invoke-static {}, Lcom/twitter/android/revenue/k;->j()Z

    move-result v6

    move-object v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/bh;-><init>(Landroid/content/Context;Lcom/twitter/android/bj;IIZ)V

    iput-object v1, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    .line 70
    invoke-virtual {v0, p0}, Lcom/twitter/android/bh;->a(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    iget-object v1, p0, Lcom/twitter/android/profiles/e;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/profiles/e;->b:Lcom/twitter/android/profiles/t;

    .line 72
    invoke-virtual {v2}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v2

    .line 71
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/bh;->a(Lcom/twitter/model/core/TwitterUser;Z)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->e:Lcom/twitter/android/bh;

    return-object v0
.end method

.method protected c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/twitter/database/schema/a$y;->x:Landroid/net/Uri;

    return-object v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 86
    const/16 v0, 0x14

    return v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x3

    return v0
.end method

.method protected f()I
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x3

    return v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x3

    return v0
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0}, Lcom/twitter/android/profiles/r;->h()V

    .line 115
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    invoke-virtual {p0}, Lcom/twitter/android/profiles/e;->i()V

    .line 117
    return-void
.end method

.method protected i()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 121
    invoke-direct {p0, v0, v0}, Lcom/twitter/android/profiles/e;->a(ZZ)Z

    move-result v0

    .line 122
    if-nez v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/twitter/android/profiles/e$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/profiles/e$1;-><init>(Lcom/twitter/android/profiles/e;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    const-string/jumbo v0, "user_similarities_list"

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 151
    invoke-super {p0}, Lcom/twitter/android/profiles/r;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/e;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected l()V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f13001f

    if-ne v0, v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/twitter/android/profiles/e;->a()V

    .line 46
    :cond_0
    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/twitter/android/profiles/r;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    .line 51
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/profiles/e;->g()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/twitter/android/profiles/e;->a()V

    .line 54
    :cond_0
    return-void
.end method
