.class public Lcom/twitter/android/profiles/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/profiles/l;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Landroid/os/Bundle;

.field protected final c:Lcom/twitter/model/core/TwitterUser;

.field protected final d:Z

.field protected final e:Z

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/model/core/TwitterUser;ZZZ)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/android/profiles/n;->a:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/twitter/android/profiles/n;->b:Landroid/os/Bundle;

    .line 49
    iput-object p3, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 50
    iput-boolean p4, p0, Lcom/twitter/android/profiles/n;->d:Z

    .line 51
    iput-boolean p5, p0, Lcom/twitter/android/profiles/n;->e:Z

    .line 52
    iput-boolean p6, p0, Lcom/twitter/android/profiles/n;->f:Z

    .line 53
    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/twitter/android/profiles/n;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/profiles/n;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(I)Lcom/twitter/library/client/m;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Lcom/twitter/android/timeline/f$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->b:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/android/timeline/f$a;-><init>(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/f$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    const-string/jumbo v1, "is_me"

    iget-boolean v2, p0, Lcom/twitter/android/profiles/n;->d:Z

    .line 80
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/timeline/f$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    const-string/jumbo v1, "fragment_page_number"

    .line 81
    invoke-virtual {v0, v1, p1}, Lcom/twitter/android/timeline/f$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    const v1, 0x7f0a05ea

    .line 82
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/f$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 83
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/f$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    const-string/jumbo v1, "profile_without_replies"

    .line 84
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/f$a;->d(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 85
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/timeline/f$a;->c(J)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    const-string/jumbo v1, "statuses_count"

    iget-object v2, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->v:I

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/timeline/f$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/f$a;

    .line 87
    invoke-virtual {v0}, Lcom/twitter/android/timeline/f$a;->a()Lcom/twitter/android/timeline/f;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/twitter/library/client/m$a;

    sget-object v2, Lcom/twitter/android/ProfileActivity;->a:Landroid/net/Uri;

    const-class v3, Lcom/twitter/android/businessprofiles/BusinessProfileTimelineFragment;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/twitter/android/profiles/n;->a:Landroid/content/Context;

    const v3, 0x7f0a06ed

    .line 91
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v1

    .line 92
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 89
    return-object v0
.end method

.method public a(Lcom/twitter/library/client/m;Lcom/twitter/model/core/TwitterUser;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 159
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 160
    :cond_0
    const-string/jumbo v0, ""

    .line 181
    :goto_0
    return-object v0

    .line 164
    :cond_1
    iget-object v0, p1, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    sget-object v1, Lcom/twitter/android/ProfileActivity;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    const v0, 0x7f0c001e

    .line 166
    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->v:I

    .line 181
    :goto_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    int-to-long v4, v1

    .line 182
    invoke-static {p3, v4, v5}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 181
    invoke-virtual {p3, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 167
    :cond_2
    iget-object v0, p1, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    sget-object v1, Lcom/twitter/android/ProfileActivity;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 168
    iget v0, p2, Lcom/twitter/model/core/TwitterUser;->w:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 169
    const v0, 0x7f0a06ef

    .line 170
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172
    :cond_3
    const v0, 0x7f0c001d

    .line 173
    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->w:I

    goto :goto_1

    .line 174
    :cond_4
    iget-object v0, p1, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    sget-object v1, Lcom/twitter/android/ProfileActivity;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 175
    const v0, 0x7f0c001c

    .line 176
    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->z:I

    goto :goto_1

    .line 178
    :cond_5
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 59
    iget-boolean v1, p0, Lcom/twitter/android/profiles/n;->e:Z

    if-eqz v1, :cond_0

    .line 60
    invoke-virtual {p0, v2}, Lcom/twitter/android/profiles/n;->a(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-virtual {p0, v3}, Lcom/twitter/android/profiles/n;->b(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    invoke-virtual {p0, v4}, Lcom/twitter/android/profiles/n;->c(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    :goto_0
    return-object v0

    .line 63
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/profiles/n;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    invoke-virtual {p0, v2}, Lcom/twitter/android/profiles/n;->a(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {p0, v3}, Lcom/twitter/android/profiles/n;->b(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-virtual {p0, v4}, Lcom/twitter/android/profiles/n;->c(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    invoke-virtual {p0, v5}, Lcom/twitter/android/profiles/n;->d(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p0, v2}, Lcom/twitter/android/profiles/n;->b(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-virtual {p0, v3}, Lcom/twitter/android/profiles/n;->c(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-virtual {p0, v4}, Lcom/twitter/android/profiles/n;->d(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected b(I)Lcom/twitter/library/client/m;
    .locals 5

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/app/profile/e$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->b:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/profile/e$a;-><init>(Landroid/os/Bundle;)V

    const v1, 0x7f0a05ea

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/e$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    const/4 v1, 0x1

    .line 101
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/e$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    const-string/jumbo v1, "statuses_count"

    iget-object v2, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->v:I

    .line 102
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/profile/e$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 103
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/profile/e$a;->c(J)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 104
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/e$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    const-string/jumbo v1, "is_me"

    iget-boolean v2, p0, Lcom/twitter/android/profiles/n;->d:Z

    .line 105
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/profile/e$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    const-string/jumbo v1, "tweets"

    .line 106
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/e$a;->d(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    const-string/jumbo v1, "fragment_page_number"

    .line 107
    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/profile/e$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/e$a;

    .line 108
    invoke-virtual {v0}, Lcom/twitter/app/profile/e$a;->a()Lcom/twitter/app/profile/e;

    move-result-object v1

    .line 109
    invoke-direct {p0}, Lcom/twitter/android/profiles/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/twitter/android/profiles/n;->a:Landroid/content/Context;

    const v2, 0x7f0a06ee

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 114
    :goto_0
    new-instance v2, Lcom/twitter/library/client/m$a;

    sget-object v3, Lcom/twitter/android/ProfileActivity;->b:Landroid/net/Uri;

    const-class v4, Lcom/twitter/app/profile/ProfileTimelinesFragment;

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 115
    invoke-virtual {v2, v0}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 116
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 114
    return-object v0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profiles/n;->a:Landroid/content/Context;

    const v2, 0x7f0a06ed

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(I)Lcom/twitter/library/client/m;
    .locals 4

    .prologue
    .line 122
    new-instance v0, Lcom/twitter/app/common/list/i$b;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->b:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/common/list/i$b;-><init>(Landroid/os/Bundle;)V

    const v1, 0x7f0a05e7

    .line 123
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i$b;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const/4 v1, 0x1

    .line 124
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i$b;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 125
    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;J)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 126
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/common/list/i$b;->c(J)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v1, "arg_profile_user"

    iget-object v2, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 127
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v1, "is_me"

    iget-boolean v2, p0, Lcom/twitter/android/profiles/n;->d:Z

    .line 128
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v1, "fragment_page_number"

    .line 129
    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    .line 130
    invoke-virtual {v0}, Lcom/twitter/app/common/list/i$b;->b()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 132
    new-instance v1, Lcom/twitter/library/client/m$a;

    sget-object v2, Lcom/twitter/android/ProfileActivity;->c:Landroid/net/Uri;

    const-class v3, Lcom/twitter/android/ProfileMediaListFragment;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/twitter/android/profiles/n;->a:Landroid/content/Context;

    const v3, 0x7f0a06eb

    .line 133
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v1

    .line 134
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 132
    return-object v0
.end method

.method protected d(I)Lcom/twitter/library/client/m;
    .locals 4

    .prologue
    .line 140
    new-instance v0, Lcom/twitter/app/profile/d$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->b:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/profile/d$a;-><init>(Landroid/os/Bundle;)V

    const v1, 0x7f0a05e1

    .line 142
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/d$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/d$a;

    const/4 v1, 0x1

    .line 143
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/d$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/d$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 144
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/profile/d$a;->c(J)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/d$a;

    iget-object v1, p0, Lcom/twitter/android/profiles/n;->c:Lcom/twitter/model/core/TwitterUser;

    .line 145
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/d$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/d$a;

    const-string/jumbo v1, "is_me"

    iget-boolean v2, p0, Lcom/twitter/android/profiles/n;->d:Z

    .line 146
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/profile/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/d$a;

    const-string/jumbo v1, "favorites"

    .line 147
    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/d$a;->d(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/d$a;

    const-string/jumbo v1, "fragment_page_number"

    .line 148
    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/profile/d$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/profile/d$a;

    .line 150
    new-instance v1, Lcom/twitter/library/client/m$a;

    sget-object v2, Lcom/twitter/android/ProfileActivity;->d:Landroid/net/Uri;

    const-class v3, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/twitter/android/profiles/n;->a:Landroid/content/Context;

    const v3, 0x7f0a06e7

    .line 151
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v1

    .line 152
    invoke-virtual {v0}, Lcom/twitter/app/profile/d$a;->a()Lcom/twitter/app/profile/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 150
    return-object v0
.end method
