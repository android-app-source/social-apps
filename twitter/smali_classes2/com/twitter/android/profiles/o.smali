.class public Lcom/twitter/android/profiles/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/profiles/o$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/ui/widget/TwitterButton;

.field private final b:Lcom/twitter/ui/widget/TwitterButton;

.field private final c:Lcom/twitter/ui/widget/TwitterButton;

.field private final d:Lcom/twitter/ui/widget/TwitterButton;

.field private final e:Lcom/twitter/ui/widget/TwitterButton;

.field private final f:Lcom/twitter/ui/widget/TwitterButton;

.field private final g:Lcom/twitter/ui/widget/TwitterButton;

.field private final h:Lcom/twitter/ui/widget/TwitterButton;

.field private final i:Lcom/twitter/ui/widget/TwitterButton;

.field private final j:Lcom/twitter/ui/widget/TwitterButton;

.field private final k:Lcom/twitter/ui/widget/TwitterButton;

.field private final l:Landroid/widget/LinearLayout;

.field private final m:Z

.field private final n:Z

.field private final o:Landroid/content/Context;

.field private final p:Lcom/twitter/android/profiles/o$a;

.field private final q:Lcom/twitter/model/core/TwitterUser;

.field private r:Lcom/twitter/model/core/TwitterUser;

.field private s:Z

.field private t:I

.field private u:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/profiles/o$a;Landroid/widget/LinearLayout;Lcom/twitter/model/core/TwitterUser;Z)V
    .locals 7

    .prologue
    .line 65
    invoke-static {p1}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/profiles/o;-><init>(Landroid/content/Context;Lcom/twitter/android/profiles/o$a;Landroid/widget/LinearLayout;Lcom/twitter/model/core/TwitterUser;ZZ)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/profiles/o$a;Landroid/widget/LinearLayout;Lcom/twitter/model/core/TwitterUser;ZZ)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-boolean p5, p0, Lcom/twitter/android/profiles/o;->m:Z

    .line 71
    iput-object p4, p0, Lcom/twitter/android/profiles/o;->q:Lcom/twitter/model/core/TwitterUser;

    .line 72
    iput-object p1, p0, Lcom/twitter/android/profiles/o;->o:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lcom/twitter/android/profiles/o;->p:Lcom/twitter/android/profiles/o$a;

    .line 74
    iput-boolean p6, p0, Lcom/twitter/android/profiles/o;->n:Z

    .line 76
    const v0, 0x7f130691

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    .line 77
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v0, 0x7f13068c

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->b:Lcom/twitter/ui/widget/TwitterButton;

    .line 80
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v0, 0x7f13068d

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->c:Lcom/twitter/ui/widget/TwitterButton;

    .line 83
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->c:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v0, 0x7f13068e

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->d:Lcom/twitter/ui/widget/TwitterButton;

    .line 86
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v0, 0x7f13068f

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->e:Lcom/twitter/ui/widget/TwitterButton;

    .line 89
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->e:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v0, 0x7f130687

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->f:Lcom/twitter/ui/widget/TwitterButton;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->f:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v0, 0x7f130688

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->g:Lcom/twitter/ui/widget/TwitterButton;

    .line 95
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->g:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v0, 0x7f13068a

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->h:Lcom/twitter/ui/widget/TwitterButton;

    .line 98
    const v0, 0x7f13068b

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->i:Lcom/twitter/ui/widget/TwitterButton;

    .line 99
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->h:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->i:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v0, 0x7f130686

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->j:Lcom/twitter/ui/widget/TwitterButton;

    .line 103
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->j:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    const v0, 0x7f130683

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/profiles/o;->k:Lcom/twitter/ui/widget/TwitterButton;

    .line 106
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->k:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iput-object p3, p0, Lcom/twitter/android/profiles/o;->l:Landroid/widget/LinearLayout;

    .line 109
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->q:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/o;->q:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->v:I

    :goto_0
    iput v0, p0, Lcom/twitter/android/profiles/o;->t:I

    .line 110
    return-void

    .line 109
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/twitter/android/profiles/o;->m:Z

    if-eqz v0, :cond_0

    .line 182
    const/4 v0, 0x0

    .line 190
    :goto_0
    return v0

    .line 183
    :cond_0
    invoke-static {p1}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    const v0, 0x7f0a09ac

    goto :goto_0

    .line 185
    :cond_1
    invoke-static {p1}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    const v0, 0x7f0a09aa

    goto :goto_0

    .line 187
    :cond_2
    invoke-static {p1}, Lcom/twitter/model/core/g;->k(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    const v0, 0x7f0a0634

    goto :goto_0

    .line 190
    :cond_3
    const v0, 0x7f0a03a5

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->g:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->f:Lcom/twitter/ui/widget/TwitterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 207
    return-void
.end method

.method public a(I)V
    .locals 7

    .prologue
    const v6, 0x7f0a09aa

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v4, 0x8

    .line 120
    iget-boolean v0, p0, Lcom/twitter/android/profiles/o;->s:Z

    if-eqz v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/profiles/o;->d(I)I

    move-result v0

    .line 125
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v2}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 126
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->c:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v2}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 127
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->f:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v2}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 129
    if-lez v0, :cond_1

    .line 130
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 131
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->c:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 132
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 133
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->e:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 134
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->f:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 135
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->g:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 137
    const v3, 0x7f0a03a5

    if-ne v0, v3, :cond_5

    .line 138
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 148
    :cond_1
    :goto_1
    iget v3, p0, Lcom/twitter/android/profiles/o;->t:I

    invoke-virtual {p0, v3}, Lcom/twitter/android/profiles/o;->c(I)V

    .line 149
    iget-boolean v3, p0, Lcom/twitter/android/profiles/o;->m:Z

    if-eqz v3, :cond_8

    .line 150
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->f:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 165
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->o:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/profiles/o;->r:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v5, p0, Lcom/twitter/android/profiles/o;->m:Z

    invoke-static {v0, v3, p1, v5}, Lcom/twitter/android/profiles/v;->a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;IZ)Z

    move-result v5

    .line 166
    invoke-static {p1}, Lcom/twitter/model/core/g;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 167
    invoke-static {p1}, Lcom/twitter/model/core/g;->b(I)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_3
    move v0, v2

    .line 168
    :goto_3
    iget-object v6, p0, Lcom/twitter/android/profiles/o;->h:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v5, :cond_b

    if-nez v0, :cond_b

    move v3, v1

    :goto_4
    invoke-virtual {v6, v3}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 170
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->i:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v5, :cond_c

    if-eqz v0, :cond_c

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 172
    iget-boolean v0, p0, Lcom/twitter/android/profiles/o;->u:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/profiles/o;->r:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v3, p0, Lcom/twitter/android/profiles/o;->m:Z

    .line 173
    invoke-static {v0, p1, v3}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/core/TwitterUser;IZ)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 174
    :goto_6
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->j:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v2, :cond_4

    move v4, v1

    :cond_4
    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 139
    :cond_5
    const v3, 0x7f0a09ac

    if-ne v0, v3, :cond_6

    .line 140
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->c:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_1

    .line 141
    :cond_6
    const v3, 0x7f0a0634

    if-ne v0, v3, :cond_7

    .line 142
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_1

    .line 143
    :cond_7
    if-ne v0, v6, :cond_1

    .line 144
    iget-object v3, p0, Lcom/twitter/android/profiles/o;->e:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v3, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_1

    .line 153
    :cond_8
    const-string/jumbo v3, "legacy_deciders_alerts_activation_enabled"

    .line 154
    invoke-static {v3}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v3

    .line 155
    iget-object v5, p0, Lcom/twitter/android/profiles/o;->r:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v5, v5, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-eqz v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/profiles/o;->r:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/profiles/o;->n:Z

    if-eqz v0, :cond_2

    .line 158
    invoke-static {p1}, Lcom/twitter/model/core/g;->l(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/profiles/o;->b()V

    goto/16 :goto_2

    .line 161
    :cond_9
    invoke-virtual {p0}, Lcom/twitter/android/profiles/o;->a()V

    goto/16 :goto_2

    :cond_a
    move v0, v1

    .line 167
    goto :goto_3

    :cond_b
    move v3, v4

    .line 168
    goto :goto_4

    :cond_c
    move v0, v4

    .line 170
    goto :goto_5

    :cond_d
    move v2, v1

    .line 173
    goto :goto_6
.end method

.method public a(Lcom/twitter/model/ads/b;Z)V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->k:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, Lcom/twitter/android/profiles/o;->k:Lcom/twitter/ui/widget/TwitterButton;

    iget-object v0, p0, Lcom/twitter/android/profiles/o;->q:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, p0, Lcom/twitter/android/profiles/o;->r:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0, v2, p1, p2}, Lcom/twitter/android/ads/c;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/ads/b;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 202
    :cond_0
    return-void

    .line 197
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/twitter/android/profiles/o;->r:Lcom/twitter/model/core/TwitterUser;

    .line 114
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->f:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->g:Lcom/twitter/ui/widget/TwitterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 212
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/o;->s:Z

    .line 221
    invoke-virtual {p0, p1}, Lcom/twitter/android/profiles/o;->a(I)V

    .line 222
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/profiles/o;->s:Z

    .line 216
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->l:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 217
    return-void
.end method

.method public c(I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 225
    iget v0, p0, Lcom/twitter/android/profiles/o;->t:I

    if-eq v0, p1, :cond_0

    .line 226
    iput p1, p0, Lcom/twitter/android/profiles/o;->t:I

    .line 228
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/profiles/o;->m:Z

    if-eqz v0, :cond_3

    .line 229
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/t;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 230
    if-nez p1, :cond_1

    .line 231
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 243
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    const v1, 0x7f0a0376

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 234
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    const v1, 0x7f0a034a

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 238
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0

    .line 241
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()Lcom/twitter/ui/widget/TwitterButton;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->k:Lcom/twitter/ui/widget/TwitterButton;

    return-object v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 259
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/profiles/o;->u:Z

    .line 260
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->j:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 261
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/o;->u:Z

    .line 265
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->h:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/o;->i:Lcom/twitter/ui/widget/TwitterButton;

    .line 269
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 268
    :goto_0
    return v0

    .line 269
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/android/profiles/o;->p:Lcom/twitter/android/profiles/o$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/profiles/o$a;->onButtonBarItemClick(Landroid/view/View;)V

    .line 252
    return-void
.end method
