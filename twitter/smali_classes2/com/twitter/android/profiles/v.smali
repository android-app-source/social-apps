.class public Lcom/twitter/android/profiles/v;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(I)I
    .locals 1

    .prologue
    .line 221
    invoke-static {p0}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;I)I
    .locals 1

    .prologue
    .line 373
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->i:I

    if-eqz v0, :cond_0

    .line 374
    iget p1, p0, Lcom/twitter/model/core/TwitterUser;->i:I

    .line 376
    :cond_0
    return p1
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 369
    const v0, 0x7f110190

    invoke-static {p1, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-static {p0, v0}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/core/TwitterUser;I)I

    move-result v0

    return v0
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/android/profiles/t;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 252
    if-eqz p1, :cond_0

    .line 253
    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 255
    :cond_0
    return-object p0
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 3

    .prologue
    .line 261
    if-eqz p1, :cond_0

    .line 262
    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    sget-object v2, Lcom/twitter/model/businessprofiles/BusinessProfileState;->b:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    .line 263
    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 265
    :cond_0
    return-object p0

    .line 262
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/profile/ExtendedProfile;Landroid/content/Context;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 320
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 321
    iget v4, p0, Lcom/twitter/model/profile/ExtendedProfile;->e:I

    .line 322
    iget v0, p0, Lcom/twitter/model/profile/ExtendedProfile;->d:I

    .line 323
    iget v5, p0, Lcom/twitter/model/profile/ExtendedProfile;->c:I

    .line 324
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 325
    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6, v4, v7, v5}, Ljava/util/Calendar;->set(III)V

    .line 326
    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    move v0, v1

    .line 328
    :goto_0
    if-eqz v4, :cond_2

    .line 329
    if-eqz v0, :cond_1

    .line 331
    invoke-static {v1}, Ljava/text/SimpleDateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 332
    const v4, 0x7f0a00a4

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 343
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 326
    goto :goto_0

    .line 335
    :cond_1
    const v0, 0x7f0a00a5

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 337
    :cond_2
    if-eqz v0, :cond_3

    .line 340
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/16 v0, 0x18

    invoke-static {p1, v4, v5, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 341
    const v4, 0x7f0a00a3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 343
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 381
    invoke-static {p0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcrv;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    if-eqz p0, :cond_0

    const-string/jumbo v0, "me"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "profile"

    goto :goto_0
.end method

.method public static varargs a(JLcom/twitter/android/profiles/t;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 244
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    invoke-virtual {v0, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 245
    invoke-static {v0, p2}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/android/profiles/t;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 246
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 247
    return-void
.end method

.method public static a(JLcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 142
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p4, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    .line 143
    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pc"

    .line 145
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v2

    .line 144
    invoke-static {v2}, Lcgi;->a(Lcgi;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v1

    .line 146
    if-eqz p3, :cond_0

    .line 147
    const-string/jumbo v2, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, p3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const/4 v3, 0x1

    .line 149
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->t:J

    .line 150
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 147
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 153
    :cond_0
    invoke-virtual {p4, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 154
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/twitter/app/drafts/DraftsActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 279
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 302
    new-array v0, v4, [Ljava/lang/Object;

    const v1, 0x7f0a0bc7

    const v2, 0x7f1100c9

    .line 303
    invoke-static {p0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;II)Lcom/twitter/ui/view/a;

    move-result-object v1

    aput-object v1, v0, v3

    .line 305
    const v1, 0x7f0a00af

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    aput-object p2, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "{{}}"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    .line 308
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 309
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 106
    if-eqz p1, :cond_0

    .line 107
    new-instance v0, Lcom/twitter/app/dm/j$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/j$a;-><init>()V

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 109
    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/j$a;->a(J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 110
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->a(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 111
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 107
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    .line 117
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 118
    return-void

    .line 113
    :cond_0
    new-instance v0, Lcom/twitter/app/dm/b$b;

    invoke-direct {v0}, Lcom/twitter/app/dm/b$b;-><init>()V

    .line 114
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/b$b;->a(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/b$b;

    .line 115
    invoke-virtual {v0}, Lcom/twitter/app/dm/b$b;->a()Lcom/twitter/app/dm/b;

    move-result-object v0

    .line 113
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/twitter/media/ui/image/UserImageView;)V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 71
    const v0, 0x7f0e041d

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 73
    const v1, 0x7f0e041b

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 74
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    const v3, 0x7f0e03f4

    .line 75
    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 77
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 79
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v2, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 80
    const/16 v0, 0x9

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 81
    invoke-virtual {p1, v3}, Lcom/twitter/media/ui/image/UserImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    const v0, 0x7f0e041a

    const v1, 0x7f0e0419

    const/high16 v2, 0x7f110000

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/media/ui/image/UserImageView;III)V

    .line 84
    return-void
.end method

.method public static a(Lcom/twitter/app/common/abs/AbsFragmentActivity;JJ)V
    .locals 3

    .prologue
    .line 121
    invoke-static {p1, p2}, Lcom/twitter/app/lists/a;->a(J)Lcom/twitter/app/lists/a;

    move-result-object v0

    .line 122
    invoke-virtual {v0, p3, p4}, Lcom/twitter/app/lists/a;->b(J)Lcom/twitter/app/lists/a;

    move-result-object v0

    .line 123
    invoke-virtual {v0, p0}, Lcom/twitter/app/lists/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 124
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 125
    return-void
.end method

.method public static a(Lcom/twitter/app/common/abs/AbsFragmentActivity;Lcom/twitter/model/core/TwitterUser;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 4

    .prologue
    .line 129
    if-nez p1, :cond_0

    .line 130
    const v0, 0x7f0a09cf

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 138
    :goto_0
    return-void

    .line 133
    :cond_0
    new-instance v0, Lcom/twitter/android/ReportFlowWebViewActivity$a;

    invoke-direct {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;-><init>()V

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 134
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(J)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    .line 135
    invoke-virtual {v0, p2}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(I)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    .line 136
    invoke-virtual {v0, p3}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    .line 137
    invoke-virtual {v0, p0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x5

    .line 133
    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/client/Session;Ljava/lang/String;JLcom/twitter/android/profiles/t;Lcgi;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 10

    .prologue
    .line 227
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v9}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;JLcom/twitter/android/profiles/t;Lcgi;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/timeline/r;)V

    .line 228
    return-void
.end method

.method public static a(Lcom/twitter/library/client/Session;Ljava/lang/String;JLcom/twitter/android/profiles/t;Lcgi;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/timeline/r;)V
    .locals 10

    .prologue
    .line 234
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    move-wide v4, p2

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p9

    .line 235
    invoke-static/range {v3 .. v8}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;Lcom/twitter/model/timeline/r;)V

    .line 236
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    invoke-virtual {v3, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 237
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 238
    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 239
    invoke-static {v3, p4}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/android/profiles/t;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 240
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    .line 241
    return-void
.end method

.method public static a(Lcom/twitter/media/ui/image/UserImageView;III)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 88
    sget-object v0, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/media/ui/image/UserImageView;IIILcom/twitter/media/ui/image/config/g;)V

    .line 90
    return-void
.end method

.method public static a(Lcom/twitter/media/ui/image/UserImageView;IIILcom/twitter/media/ui/image/config/g;)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 96
    invoke-virtual {p0, p2, p3, p4}, Lcom/twitter/media/ui/image/UserImageView;->a(IILcom/twitter/media/ui/image/config/g;)V

    .line 97
    return-void
.end method

.method public static a(IZ)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 169
    if-nez p1, :cond_0

    invoke-static {p0}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/twitter/model/core/g;->h(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    invoke-static {v0, p0}, Lcom/twitter/android/profiles/v;->a(ZI)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 169
    :cond_0
    return v0
.end method

.method public static a(JLjava/lang/String;Lcom/twitter/library/client/Session;)Z
    .locals 4

    .prologue
    .line 313
    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    .line 314
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v1, p0, v2

    if-eqz v1, :cond_1

    .line 315
    :cond_0
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 314
    :goto_0
    return v0

    .line 315
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;J)Z
    .locals 1

    .prologue
    .line 212
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;IZ)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p3, :cond_0

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-nez v1, :cond_0

    .line 180
    invoke-static {p2}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 181
    invoke-static {v0, p2}, Lcom/twitter/android/profiles/v;->a(ZI)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 179
    :cond_0
    return v0
.end method

.method public static a(Lcom/twitter/android/profiles/t;ZZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 194
    invoke-virtual {p0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 195
    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    .line 199
    :cond_0
    :goto_0
    return v0

    .line 198
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/profiles/t;->d()I

    move-result v2

    .line 199
    const-string/jumbo v3, "device_follow_prompt_android_enabled"

    invoke-static {v3}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v1, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-nez v3, :cond_0

    .line 201
    invoke-static {v2}, Lcom/twitter/model/core/g;->f(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 202
    invoke-static {v2}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 203
    invoke-static {v2}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v2

    if-ne v2, p1, :cond_0

    iget-boolean v1, v1, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-nez v1, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;IZ)Z
    .locals 1

    .prologue
    .line 161
    invoke-static {p1, p2}, Lcom/twitter/android/profiles/v;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/profile/ExtendedProfile;Ljava/util/Date;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 347
    if-nez p0, :cond_1

    .line 358
    :cond_0
    :goto_0
    return v0

    .line 350
    :cond_1
    iget v1, p0, Lcom/twitter/model/profile/ExtendedProfile;->d:I

    .line 351
    iget v2, p0, Lcom/twitter/model/profile/ExtendedProfile;->c:I

    .line 352
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 353
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 354
    invoke-virtual {v3, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 355
    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v4, v2, :cond_0

    const/4 v2, 0x2

    .line 356
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v1, v1, -0x1

    if-ne v2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(ZI)Z
    .locals 1

    .prologue
    .line 282
    const-string/jumbo v0, "blocked_by_profile_bellbird_enabled"

    .line 283
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 284
    if-nez p0, :cond_0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/model/core/g;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(ZLcom/twitter/model/core/TwitterUser;)Z
    .locals 1

    .prologue
    .line 157
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(ZLcom/twitter/model/core/TwitterUser;I)Z
    .locals 1

    .prologue
    .line 288
    if-nez p0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterUser;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;J)V
    .locals 1

    .prologue
    .line 274
    invoke-static {}, Lcom/twitter/app/lists/a;->a()Lcom/twitter/app/lists/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/lists/a;->b(J)Lcom/twitter/app/lists/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/app/lists/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 275
    return-void
.end method

.method public static b(Lcom/twitter/model/core/TwitterUser;IZ)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 165
    invoke-static {p1, p2}, Lcom/twitter/android/profiles/v;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1, v0}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/core/TwitterUser;IZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static b(ZLcom/twitter/model/core/TwitterUser;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 292
    invoke-static {p0, p2}, Lcom/twitter/android/profiles/v;->a(ZI)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, p1, p2}, Lcom/twitter/android/profiles/v;->a(ZLcom/twitter/model/core/TwitterUser;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 296
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p0, :cond_0

    const-string/jumbo v1, "blocker_interstitial_bellbird_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    invoke-static {p2}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
