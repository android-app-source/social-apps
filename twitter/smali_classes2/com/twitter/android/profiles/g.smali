.class public Lcom/twitter/android/profiles/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/profiles/g$c;,
        Lcom/twitter/android/profiles/g$b;,
        Lcom/twitter/android/profiles/g$a;,
        Lcom/twitter/android/profiles/g$d;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/twitter/android/profiles/g$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profiles/g;->a:Landroid/content/Context;

    .line 36
    return-void
.end method

.method private b(Lcom/twitter/model/core/TwitterUser;)Z
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x1

    .line 62
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/profiles/g;->c(J)Lcom/twitter/util/a;

    move-result-object v0

    .line 63
    const-string/jumbo v1, "ht"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 64
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 65
    const-wide/32 v2, 0x927c0

    add-long/2addr v0, v2

    .line 66
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    move v0, v6

    .line 67
    :goto_0
    if-eqz v0, :cond_0

    .line 69
    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/profiles/g;->b(J)V

    .line 70
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v7

    .line 71
    new-instance v1, Lcom/twitter/library/service/v;

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v4, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/service/v;-><init>(JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Z)V

    .line 72
    new-instance v2, Lcom/twitter/android/profiles/g$d;

    iget-object v3, p0, Lcom/twitter/android/profiles/g;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/profiles/g$d;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    invoke-virtual {v7, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    :cond_0
    move v6, v0

    .line 76
    :cond_1
    return v6

    .line 66
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)Lcom/twitter/util/a;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lcom/twitter/util/a;

    iget-object v1, p0, Lcom/twitter/android/profiles/g;->a:Landroid/content/Context;

    const-string/jumbo v2, "profile"

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/profiles/g;->b(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/profiles/g;->a:Landroid/content/Context;

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v0, v2, v3}, Lcom/twitter/media/util/l;->b(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/profiles/g;->c(J)Lcom/twitter/util/a;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "ht"

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 44
    return-void
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/android/profiles/g$c;)V
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0, p2}, Lcom/twitter/android/profiles/g;->b(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/twitter/android/profiles/g$a;

    iget-wide v2, p2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {v0, p1, v2, v3, p3}, Lcom/twitter/android/profiles/g$a;-><init>(Landroid/app/Activity;JLcom/twitter/android/profiles/g$c;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/g$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Lcom/twitter/android/profiles/g$c;->a(Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/profiles/g$c;)V
    .locals 7

    .prologue
    .line 91
    new-instance v1, Lcom/twitter/android/profiles/g$b;

    iget-wide v4, p2, Lcom/twitter/model/core/TwitterUser;->b:J

    move-object v2, p1

    move-object v3, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/profiles/g$b;-><init>(Landroid/app/Activity;Lcom/twitter/model/media/EditableImage;JLcom/twitter/android/profiles/g$c;)V

    iput-object v1, p0, Lcom/twitter/android/profiles/g;->b:Lcom/twitter/android/profiles/g$b;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/profiles/g;->b:Lcom/twitter/android/profiles/g$b;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/g$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 93
    return-void
.end method

.method public a(Lcom/twitter/android/profiles/g$c;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/profiles/g;->b:Lcom/twitter/android/profiles/g$b;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/twitter/android/profiles/g;->b:Lcom/twitter/android/profiles/g$b;

    invoke-static {v0, p1}, Lcom/twitter/android/profiles/g$b;->a(Lcom/twitter/android/profiles/g$b;Lcom/twitter/android/profiles/g$c;)V

    .line 213
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/android/profiles/g;->b:Lcom/twitter/android/profiles/g$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/g;->b:Lcom/twitter/android/profiles/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/g$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/profiles/g;->c(J)Lcom/twitter/util/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "ht"

    invoke-virtual {v0, v1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 51
    return-void
.end method
