.class public Lcom/twitter/android/profiles/animation/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/profiles/animation/b$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/util/DisplayMetrics;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Lcom/twitter/android/profiles/animation/a$a;

.field private final f:I

.field private final g:Lcom/twitter/android/profiles/animation/b$a;

.field private final h:Lcom/twitter/android/profiles/animation/b$a;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/util/DisplayMetrics;IIILcom/twitter/android/profiles/animation/b$a;Lcom/twitter/android/profiles/animation/b$a;[FLcom/twitter/android/profiles/animation/a$a;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/twitter/android/profiles/animation/b;->a:Landroid/util/DisplayMetrics;

    .line 52
    iput p2, p0, Lcom/twitter/android/profiles/animation/b;->b:I

    .line 53
    iput p3, p0, Lcom/twitter/android/profiles/animation/b;->c:I

    .line 54
    iput p4, p0, Lcom/twitter/android/profiles/animation/b;->d:I

    .line 55
    iput-object p5, p0, Lcom/twitter/android/profiles/animation/b;->g:Lcom/twitter/android/profiles/animation/b$a;

    .line 56
    iput-object p6, p0, Lcom/twitter/android/profiles/animation/b;->h:Lcom/twitter/android/profiles/animation/b$a;

    .line 57
    iget v0, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/2addr v0, p4

    iput v0, p0, Lcom/twitter/android/profiles/animation/b;->f:I

    .line 58
    invoke-virtual {p0, p7}, Lcom/twitter/android/profiles/animation/b;->a([F)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profiles/animation/b;->i:Ljava/util/List;

    .line 59
    iput-object p8, p0, Lcom/twitter/android/profiles/animation/b;->e:Lcom/twitter/android/profiles/animation/a$a;

    .line 60
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)Lcom/twitter/android/profiles/animation/a;
    .locals 4

    .prologue
    .line 82
    iget v0, p0, Lcom/twitter/android/profiles/animation/b;->d:I

    rem-int v0, p2, v0

    .line 83
    iget v1, p0, Lcom/twitter/android/profiles/animation/b;->f:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/android/profiles/animation/b;->f:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 85
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/b;->i:Ljava/util/List;

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    iget-object v3, p0, Lcom/twitter/android/profiles/animation/b;->i:Ljava/util/List;

    .line 86
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 85
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 87
    iget-object v2, p0, Lcom/twitter/android/profiles/animation/b;->e:Lcom/twitter/android/profiles/animation/a$a;

    invoke-virtual {v2, p1}, Lcom/twitter/android/profiles/animation/a$a;->a(Landroid/graphics/drawable/Drawable;)Lcom/twitter/android/profiles/animation/a$a;

    move-result-object v2

    .line 88
    invoke-virtual {v2, v1}, Lcom/twitter/android/profiles/animation/a$a;->d(I)Lcom/twitter/android/profiles/animation/a$a;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/profiles/animation/b;->f:I

    add-int/2addr v1, v3

    .line 89
    invoke-virtual {v2, v1}, Lcom/twitter/android/profiles/animation/a$a;->e(I)Lcom/twitter/android/profiles/animation/a$a;

    move-result-object v1

    .line 90
    invoke-virtual {v1, v0}, Lcom/twitter/android/profiles/animation/a$a;->c(I)Lcom/twitter/android/profiles/animation/a$a;

    .line 91
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/b;->e:Lcom/twitter/android/profiles/animation/a$a;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/animation/a$a;->a()Lcom/twitter/android/profiles/animation/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/twitter/android/profiles/animation/b;->b:I

    return v0
.end method

.method a([F)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/b;->a:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/twitter/android/profiles/animation/b;->a:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    .line 65
    iget v1, p0, Lcom/twitter/android/profiles/animation/b;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 67
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, p1, v0

    .line 68
    int-to-float v5, v1

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    return-object v2
.end method

.method b()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/twitter/android/profiles/animation/b;->c:I

    return v0
.end method

.method public c()Lcom/twitter/android/profiles/animation/a;
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/b;->g:Lcom/twitter/android/profiles/animation/b$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/animation/b$a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/profiles/animation/b;->j:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/twitter/android/profiles/animation/b;->j:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/profiles/animation/b;->a(Landroid/graphics/drawable/Drawable;I)Lcom/twitter/android/profiles/animation/a;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/twitter/android/profiles/animation/a;
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/b;->h:Lcom/twitter/android/profiles/animation/b$a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/b;->h:Lcom/twitter/android/profiles/animation/b$a;

    .line 100
    invoke-interface {v0}, Lcom/twitter/android/profiles/animation/b$a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/profiles/animation/b;->k:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/twitter/android/profiles/animation/b;->k:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/profiles/animation/b;->a(Landroid/graphics/drawable/Drawable;I)Lcom/twitter/android/profiles/animation/a;

    move-result-object v0

    goto :goto_0
.end method
