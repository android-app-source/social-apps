.class Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView;->a(Landroid/graphics/Path;JLandroid/animation/TimeInterpolator;Landroid/widget/ImageView;J)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/ImageView;

.field final synthetic b:J

.field final synthetic c:Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView;


# direct methods
.method constructor <init>(Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView;Landroid/widget/ImageView;J)V
    .locals 1

    .prologue
    .line 192
    iput-object p1, p0, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;->c:Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView;

    iput-object p2, p0, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;->a:Landroid/widget/ImageView;

    iput-wide p3, p0, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;->c:Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView;

    iget-object v1, p0, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView;->removeView(Landroid/view/View;)V

    .line 236
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;->a:Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6$1;-><init>(Lcom/twitter/android/profiles/animation/FlyThroughSetAnimationView$6;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method
