.class public abstract Lcom/twitter/android/profiles/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/twitter/android/av;
.implements Lcom/twitter/android/profiles/q;
.implements Lcom/twitter/library/client/s;
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/twitter/android/av",
        "<",
        "Lcom/twitter/ui/user/BaseUserView;",
        "Lcgi;",
        ">;",
        "Lcom/twitter/android/profiles/q;",
        "Lcom/twitter/library/client/s;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/library/client/Session;

.field protected final b:Lcom/twitter/android/profiles/t;

.field protected final c:Landroid/content/Context;

.field protected final d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected e:Lcom/twitter/android/bh;

.field private final f:Lcom/twitter/library/client/v;

.field private final g:Landroid/support/v4/app/FragmentActivity;

.field private final h:Landroid/support/v4/app/LoaderManager;

.field private final i:Lcom/twitter/library/client/p;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private m:I

.field private final n:J

.field private o:Lcom/twitter/android/profiles/q$a;

.field private p:Lrx/j;


# direct methods
.method protected constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/v;Lcom/twitter/android/profiles/t;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 2

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/profiles/r;->j:Ljava/util/Set;

    .line 105
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/profiles/r;->k:Ljava/util/Set;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/profiles/r;->l:Ljava/util/List;

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/profiles/r;->m:I

    .line 115
    iput-object p1, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    .line 116
    iput-object p1, p0, Lcom/twitter/android/profiles/r;->g:Landroid/support/v4/app/FragmentActivity;

    .line 117
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profiles/r;->h:Landroid/support/v4/app/LoaderManager;

    .line 118
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profiles/r;->i:Lcom/twitter/library/client/p;

    .line 119
    iput-object p2, p0, Lcom/twitter/android/profiles/r;->f:Lcom/twitter/library/client/v;

    .line 120
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->f:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    .line 121
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/profiles/r;->n:J

    .line 122
    iput-object p3, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    .line 123
    iput-object p4, p0, Lcom/twitter/android/profiles/r;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 124
    return-void
.end method

.method private a(J)V
    .locals 13

    .prologue
    const-wide/16 v7, -0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 128
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 129
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 130
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 131
    :goto_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v12

    .line 132
    if-eqz v0, :cond_2

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v6

    move-wide v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/t;->a(JJIJ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 133
    iget-object v12, p0, Lcom/twitter/android/profiles/r;->i:Lcom/twitter/library/client/p;

    new-instance v1, Lbig;

    iget-object v2, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 134
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v6

    move-wide v9, p1

    invoke-direct/range {v1 .. v11}, Lbig;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)V

    const/4 v0, 0x4

    .line 133
    invoke-virtual {v12, v1, v0, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 145
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, v11

    .line 130
    goto :goto_0

    .line 136
    :cond_2
    if-nez v0, :cond_0

    invoke-virtual {v12}, Lcof;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v12}, Lcof;->b()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v12}, Lcof;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    :cond_3
    new-instance v1, Lcpb;

    invoke-direct {v1}, Lcpb;-><init>()V

    const-string/jumbo v2, "mProfile.null"

    iget-object v0, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    if-nez v0, :cond_4

    move v0, v9

    .line 138
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "mProfile.getUser().null"

    .line 139
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "this.getClass()"

    .line 140
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "mRecommendationsAdapter.null"

    iget-object v2, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    if-nez v2, :cond_5

    .line 141
    :goto_3
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Trying to replenish ProfileRecommendationModule when user is null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 137
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_1

    :cond_4
    move v0, v10

    goto :goto_2

    :cond_5
    move v9, v10

    .line 140
    goto :goto_3
.end method

.method static synthetic a(Lcom/twitter/android/profiles/r;J)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/profiles/r;->a(J)V

    return-void
.end method

.method private a(Lcom/twitter/ui/user/UserView;J)V
    .locals 10

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v0

    .line 166
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    .line 167
    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "type"

    .line 168
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    .line 169
    iget-object v1, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    .line 171
    const-string/jumbo v2, "friendship"

    invoke-virtual {v8, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 173
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v5

    .line 174
    if-eqz v5, :cond_1

    .line 175
    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v1, v5}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 176
    const-string/jumbo v1, "pc"

    invoke-static {v5}, Lcgi;->a(Lcgi;)[B

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v1

    invoke-static {v1}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v1

    .line 179
    const/16 v2, 0xa

    if-ne v0, v2, :cond_2

    .line 180
    const-string/jumbo v2, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v3, 0x5

    .line 182
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v6, p0, Lcom/twitter/android/profiles/r;->n:J

    .line 183
    invoke-virtual {v0, v6, v7}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 184
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 185
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 180
    invoke-virtual {v8, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "::user:profile_click"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 187
    invoke-static {v1, v2}, Lcom/twitter/android/profiles/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/profiles/r;->n:J

    iget-object v4, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    .line 189
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/db;

    iget-object v6, v6, Lcom/twitter/android/db;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/profiles/r;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 187
    invoke-static/range {v0 .. v7}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;JLcom/twitter/android/profiles/t;Lcgi;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 190
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->g:Landroid/support/v4/app/FragmentActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v8, v1}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 191
    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 491
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 493
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 494
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    .line 495
    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 492
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 496
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 498
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/library/service/s;)Z
    .locals 2

    .prologue
    .line 537
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/v;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    return v0
.end method

.method private b(J)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 527
    new-instance v0, Lbhz;

    iget-object v1, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    .line 528
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lbhz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 529
    const/4 v1, 0x0

    iput v1, v0, Lbhz;->c:I

    .line 530
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->f()I

    move-result v1

    iput v1, v0, Lbhz;->j:I

    .line 531
    iput-wide p1, v0, Lbhz;->b:J

    .line 532
    iget-object v1, p0, Lcom/twitter/android/profiles/r;->i:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0, v4, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 533
    iput v4, p0, Lcom/twitter/android/profiles/r;->m:I

    .line 534
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 244
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 459
    if-eqz p1, :cond_0

    .line 461
    const-string/jumbo v0, "state_recommendation_request_state"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/profiles/r;->m:I

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    iput v1, p0, Lcom/twitter/android/profiles/r;->m:I

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->g()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 226
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0, p2}, Lcom/twitter/android/bh;->a(Landroid/database/Cursor;)V

    .line 228
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->h()V

    .line 230
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->l()V

    .line 232
    :cond_1
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 76
    check-cast p1, Lcom/twitter/ui/user/BaseUserView;

    check-cast p2, Lcgi;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/profiles/r;->a(Lcom/twitter/ui/user/BaseUserView;Lcgi;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcom/twitter/android/profiles/q$a;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/twitter/android/profiles/r;->o:Lcom/twitter/android/profiles/q$a;

    .line 420
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 76
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/profiles/r;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/BaseUserView;Lcgi;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 352
    invoke-virtual {p1}, Lcom/twitter/ui/user/BaseUserView;->getUserId()J

    move-result-wide v2

    .line 353
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->j:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    invoke-virtual {p1}, Lcom/twitter/ui/user/BaseUserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    iget-object v0, v0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    const/4 v1, 0x0

    .line 354
    invoke-static {v2, v3, p2, v0, v1}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 356
    const-string/jumbo v1, "position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 357
    iget-object v1, p0, Lcom/twitter/android/profiles/r;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/profiles/r;->k:Ljava/util/Set;

    iget-object v1, p2, Lcgi;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v0, p2}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 362
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 14

    .prologue
    .line 374
    const v2, 0x7f130035

    move/from16 v0, p4

    if-ne v0, v2, :cond_4

    .line 375
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 376
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 377
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->i()V

    .line 378
    iget-object v2, p0, Lcom/twitter/android/profiles/r;->g:Landroid/support/v4/app/FragmentActivity;

    const/4 v3, 0x4

    .line 380
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getBestName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 378
    invoke-static {v2, v3, v4}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v8

    .line 383
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 384
    new-instance v3, Lbhs;

    iget-object v4, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    move-wide/from16 v6, p2

    invoke-direct/range {v3 .. v8}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 386
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v2

    invoke-virtual {v3, v2}, Lbhs;->a(I)Lbhs;

    move-result-object v2

    .line 387
    iget-object v3, p0, Lcom/twitter/android/profiles/r;->i:Lcom/twitter/library/client/p;

    const/4 v4, 0x3

    invoke-virtual {v3, v2, v4, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 388
    iget-object v2, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v2}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 389
    const-string/jumbo v2, "unfollow"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 403
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v2}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v2

    invoke-static {v2}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v11

    .line 404
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 405
    iget-object v3, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 406
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "::user:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 405
    invoke-static {v11, v2}, Lcom/twitter/android/profiles/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    .line 407
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/db;

    iget-object v9, v2, Lcom/twitter/android/db;->g:Ljava/lang/String;

    iget-object v10, p0, Lcom/twitter/android/profiles/r;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-wide/from16 v5, p2

    .line 405
    invoke-static/range {v3 .. v10}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;JLcom/twitter/android/profiles/t;Lcgi;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto :goto_2

    .line 391
    :cond_3
    new-instance v3, Lbhq;

    iget-object v4, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/twitter/android/profiles/r;->a:Lcom/twitter/library/client/Session;

    move-wide/from16 v6, p2

    invoke-direct/range {v3 .. v8}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    const/4 v2, 0x0

    .line 393
    invoke-virtual {v3, v2}, Lbhq;->a(Z)Lbhq;

    move-result-object v2

    .line 394
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Lbhq;->a(I)Lbhq;

    move-result-object v2

    .line 396
    iget-object v3, p0, Lcom/twitter/android/profiles/r;->i:Lcom/twitter/library/client/p;

    const/4 v4, 0x2

    invoke-virtual {v3, v2, v4, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 397
    iget-object v2, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v2}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 398
    const-string/jumbo v2, "follow"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/db;

    iget v2, v2, Lcom/twitter/android/db;->f:I

    invoke-static {v2}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 400
    const-string/jumbo v2, "follow_back"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 410
    :cond_4
    const v2, 0x7f130097

    move/from16 v0, p4

    if-ne v0, v2, :cond_0

    .line 411
    invoke-direct/range {p0 .. p3}, Lcom/twitter/android/profiles/r;->a(Lcom/twitter/ui/user/UserView;J)V

    goto/16 :goto_0
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 251
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->f:Lcom/twitter/library/client/v;

    invoke-virtual {v0, p2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 253
    packed-switch p1, :pswitch_data_0

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 255
    :pswitch_0
    if-eqz v0, :cond_0

    move-object v0, p2

    .line 256
    check-cast v0, Lbig;

    .line 258
    invoke-virtual {v0}, Lbig;->g()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 259
    if-eqz v1, :cond_2

    .line 260
    invoke-virtual {v0}, Lbig;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v2

    .line 261
    :goto_1
    invoke-direct {p0, p2}, Lcom/twitter/android/profiles/r;->a(Lcom/twitter/library/service/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    .line 263
    invoke-virtual {v0}, Lcom/twitter/android/bh;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bj;

    .line 265
    if-eqz v1, :cond_1

    .line 266
    invoke-virtual {v0}, Lcom/twitter/android/bj;->i()Landroid/database/Cursor;

    move-result-object v1

    .line 267
    if-eqz v1, :cond_1

    .line 269
    invoke-interface {v1}, Landroid/database/Cursor;->requery()Z

    .line 270
    invoke-virtual {v0}, Lcom/twitter/android/bj;->notifyDataSetChanged()V

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::user:replenish"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 273
    iget-object v1, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    .line 274
    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v1

    invoke-static {v1}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v1

    .line 273
    invoke-static {v1, v0}, Lcom/twitter/android/profiles/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 276
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/profiles/r;->n:J

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 280
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->i()V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 260
    goto :goto_1

    .line 285
    :pswitch_1
    invoke-virtual {p2}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 286
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/profiles/r;->m:I

    .line 287
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->r()V

    goto :goto_0

    .line 289
    :cond_3
    iput v3, p0, Lcom/twitter/android/profiles/r;->m:I

    .line 290
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->l()V

    goto/16 :goto_0

    .line 295
    :pswitch_2
    if-eqz v0, :cond_0

    .line 296
    check-cast p2, Lbhq;

    .line 297
    invoke-virtual {p2}, Lbhq;->v()I

    move-result v0

    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 299
    invoke-virtual {p2}, Lbhq;->t()J

    move-result-wide v0

    .line 301
    invoke-virtual {p2}, Lbhq;->T()Z

    move-result v2

    .line 302
    if-nez v2, :cond_5

    .line 303
    invoke-direct {p0, p2}, Lcom/twitter/android/profiles/r;->a(Lcom/twitter/library/service/s;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 304
    iget-object v2, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v2}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 305
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0}, Lcom/twitter/android/bh;->notifyDataSetChanged()V

    .line 317
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->i()V

    goto/16 :goto_0

    .line 308
    :cond_5
    new-instance v2, Lcom/twitter/android/profiles/r$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/android/profiles/r$1;-><init>(Lcom/twitter/android/profiles/r;J)V

    invoke-static {v2}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    .line 314
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 315
    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profiles/r;->p:Lrx/j;

    goto :goto_2

    .line 323
    :pswitch_3
    if-eqz v0, :cond_0

    move-object v0, p2

    .line 324
    check-cast v0, Lbhs;

    .line 326
    invoke-virtual {v0}, Lbhs;->h()I

    move-result v1

    .line 328
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    move-result v3

    if-ne v1, v3, :cond_0

    .line 329
    invoke-virtual {v0}, Lbhs;->g()J

    move-result-wide v4

    .line 330
    invoke-virtual {p2}, Lcom/twitter/library/service/s;->T()Z

    move-result v1

    if-nez v1, :cond_6

    .line 331
    invoke-direct {p0, v0}, Lcom/twitter/android/profiles/r;->a(Lcom/twitter/library/service/s;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 332
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 333
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0}, Lcom/twitter/android/bh;->notifyDataSetChanged()V

    .line 334
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    const v1, 0x7f0a09e3

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 337
    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->i()V

    goto/16 :goto_0

    .line 253
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 469
    const-string/jumbo v0, "state_recommendation_request_state"

    iget v1, p0, Lcom/twitter/android/profiles/r;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 470
    return-void
.end method

.method protected abstract c()Landroid/net/Uri;
.end method

.method protected abstract d()I
.end method

.method protected abstract e()I
.end method

.method protected abstract f()I
.end method

.method protected abstract g()I
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->o:Lcom/twitter/android/profiles/q$a;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->o:Lcom/twitter/android/profiles/q$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/q$a;->o()V

    .line 522
    :cond_0
    return-void
.end method

.method protected abstract i()V
.end method

.method protected abstract j()Ljava/lang/String;
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0}, Lcom/twitter/android/bh;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract l()V
.end method

.method public m()V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0}, Lcom/twitter/android/bh;->notifyDataSetChanged()V

    .line 450
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 424
    iget v0, p0, Lcom/twitter/android/profiles/r;->m:I

    packed-switch v0, :pswitch_data_0

    .line 437
    :goto_0
    :pswitch_0
    return-void

    .line 426
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->r()V

    goto :goto_0

    .line 430
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/profiles/r;->b(J)V

    goto :goto_0

    .line 424
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public o()V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0}, Lcom/twitter/android/bh;->notifyDataSetChanged()V

    .line 443
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->i()V

    .line 445
    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 195
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->g()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->c()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    .line 198
    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    .line 196
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "limit"

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 200
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    iget-wide v2, p0, Lcom/twitter/android/profiles/r;->n:J

    .line 203
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 202
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 207
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    const-string/jumbo v4, "(users_friendship IS NULL OR (users_friendship & 1 == 0)) AND user_groups_user_id!=?"

    .line 209
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    .line 217
    :goto_0
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v1, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lbun;->b:[Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v0, v7}, Lcom/twitter/util/android/d;->a(Z)Lcom/twitter/util/android/d;

    move-result-object v6

    .line 220
    :cond_0
    return-object v6

    :cond_1
    move-object v5, v6

    move-object v4, v6

    .line 212
    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->d()I

    .line 154
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0, p3}, Lcom/twitter/android/bh;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0, p3}, Lcom/twitter/android/bh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/twitter/android/profiles/r;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0, p3}, Lcom/twitter/android/bh;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0}, Lcom/twitter/android/bh;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bj;

    invoke-virtual {v0, p2}, Lcom/twitter/android/bj;->a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;

    move-result-object v0

    invoke-direct {p0, v0, p4, p5}, Lcom/twitter/android/profiles/r;->a(Lcom/twitter/ui/user/UserView;J)V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 76
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/profiles/r;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->p:Lrx/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/r;->p:Lrx/j;

    invoke-interface {v0}, Lrx/j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->p:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 477
    :cond_0
    return-void
.end method

.method public q()V
    .locals 3

    .prologue
    .line 484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":stream::results"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 485
    iget-object v1, p0, Lcom/twitter/android/profiles/r;->l:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/android/profiles/r;->b:Lcom/twitter/android/profiles/t;

    .line 487
    invoke-virtual {v2}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v2

    invoke-static {v2}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v2

    .line 486
    invoke-static {v2, v0}, Lcom/twitter/android/profiles/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 485
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/profiles/r;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 488
    return-void
.end method

.method protected r()V
    .locals 3

    .prologue
    .line 501
    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->b()Lcom/twitter/android/bh;

    .line 502
    iget-object v0, p0, Lcom/twitter/android/profiles/r;->h:Landroid/support/v4/app/LoaderManager;

    invoke-virtual {p0}, Lcom/twitter/android/profiles/r;->g()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 503
    return-void
.end method
