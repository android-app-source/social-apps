.class public Lcom/twitter/android/profiles/k;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/bl$a;
.implements Lcom/twitter/android/profiles/j;
.implements Lcom/twitter/android/profiles/t$a;


# instance fields
.field private final a:Lcom/twitter/android/bl;

.field private final b:Lcom/twitter/android/profiles/t;

.field private final c:Lcom/twitter/library/client/Session;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/twitter/android/profiles/j$a;


# direct methods
.method public constructor <init>(Lcom/twitter/android/profiles/j$a;Lcom/twitter/android/profiles/t;Lcom/twitter/library/client/Session;Landroid/content/Context;Lcom/twitter/android/bl;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    .line 34
    iput-object p2, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    .line 35
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, p0}, Lcom/twitter/android/profiles/t;->a(Lcom/twitter/android/profiles/t$a;)V

    .line 36
    iput-object p3, p0, Lcom/twitter/android/profiles/k;->c:Lcom/twitter/library/client/Session;

    .line 37
    iput-object p4, p0, Lcom/twitter/android/profiles/k;->d:Landroid/content/Context;

    .line 38
    iput-object p5, p0, Lcom/twitter/android/profiles/k;->a:Lcom/twitter/android/bl;

    .line 39
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/twitter/android/profiles/k;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->d()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j$a;->j()V

    .line 61
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j$a;->i()V

    goto :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j$a;->h()V

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 65
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    .line 66
    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->d()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->J:I

    .line 67
    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 72
    new-instance v0, Lbfa;

    iget-object v1, p0, Lcom/twitter/android/profiles/k;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/profiles/k;->c:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Lbfa;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v1, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    .line 73
    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbfa;->a(J)Lbeq;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/twitter/android/profiles/k;->a:Lcom/twitter/android/bl;

    const/16 v2, 0xfa0

    invoke-virtual {v1, v0, v2, p0}, Lcom/twitter/android/bl;->a(Lcom/twitter/library/service/s;ILcom/twitter/android/bl$a;)Z

    .line 75
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/t;->b(I)V

    .line 76
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j$a;->j()V

    .line 77
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 2

    .prologue
    const/16 v1, 0x2000

    .line 90
    invoke-virtual {p2}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    .line 91
    if-nez v0, :cond_0

    .line 92
    packed-switch p1, :pswitch_data_0

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 94
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/t;->c(I)V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j$a;->i()V

    goto :goto_0

    .line 99
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/t;->b(I)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j$a;->j()V

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/profiles/j$a;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/android/profiles/k;->c()V

    .line 44
    return-void
.end method

.method public a(Lcom/twitter/android/profiles/t;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/android/profiles/k;->c()V

    .line 49
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 81
    new-instance v0, Lbff;

    iget-object v1, p0, Lcom/twitter/android/profiles/k;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/profiles/k;->c:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Lbff;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v1, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    .line 82
    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbff;->a(J)Lbeq;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/twitter/android/profiles/k;->a:Lcom/twitter/android/bl;

    const/16 v2, 0xfa1

    invoke-virtual {v1, v0, v2, p0}, Lcom/twitter/android/bl;->a(Lcom/twitter/library/service/s;ILcom/twitter/android/bl$a;)Z

    .line 84
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->b:Lcom/twitter/android/profiles/t;

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/t;->c(I)V

    .line 85
    iget-object v0, p0, Lcom/twitter/android/profiles/k;->e:Lcom/twitter/android/profiles/j$a;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j$a;->i()V

    .line 86
    return-void
.end method
