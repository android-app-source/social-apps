.class public Lcom/twitter/android/profiles/p;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/os/Bundle;

.field private c:Lcom/twitter/model/core/TwitterUser;

.field private d:Z

.field private e:I

.field private f:Lbqn;

.field private g:Lcom/twitter/android/ProfileActivity$DisplayState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/profiles/p;->b:Landroid/os/Bundle;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/p;->d:Z

    .line 27
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->a:Lcom/twitter/android/ProfileActivity$DisplayState;

    iput-object v0, p0, Lcom/twitter/android/profiles/p;->g:Lcom/twitter/android/ProfileActivity$DisplayState;

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/android/profiles/l;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Lcom/twitter/android/profiles/p;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/profiles/p;->b:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/profiles/p;->c:Lcom/twitter/model/core/TwitterUser;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/profiles/p;->g:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v2, Lcom/twitter/android/ProfileActivity$DisplayState;->a:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-ne v1, v2, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :cond_1
    sget-object v1, Lcom/twitter/android/profiles/p$1;->a:[I

    iget-object v2, p0, Lcom/twitter/android/profiles/p;->g:Lcom/twitter/android/ProfileActivity$DisplayState;

    invoke-virtual {v2}, Lcom/twitter/android/ProfileActivity$DisplayState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 71
    :pswitch_0
    const/4 v5, 0x0

    .line 72
    iget-object v0, p0, Lcom/twitter/android/profiles/p;->c:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lbld;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    const/4 v5, 0x1

    .line 75
    :cond_2
    new-instance v0, Lcom/twitter/android/profiles/n;

    iget-object v1, p0, Lcom/twitter/android/profiles/p;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/profiles/p;->b:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/twitter/android/profiles/p;->c:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v4, p0, Lcom/twitter/android/profiles/p;->d:Z

    .line 76
    invoke-static {}, Lbpp;->a()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/profiles/n;-><init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/model/core/TwitterUser;ZZZ)V

    goto :goto_0

    .line 79
    :pswitch_1
    new-instance v0, Lcom/twitter/android/profiles/s;

    iget-object v1, p0, Lcom/twitter/android/profiles/p;->b:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/twitter/android/profiles/p;->c:Lcom/twitter/model/core/TwitterUser;

    sget-object v3, Lcom/twitter/android/ProfileActivity;->f:Landroid/net/Uri;

    const-class v4, Lcom/twitter/android/ProfileBlockedProfileFragment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/profiles/s;-><init>(Landroid/os/Bundle;Lcom/twitter/model/core/TwitterUser;Landroid/net/Uri;Ljava/lang/Class;)V

    goto :goto_0

    .line 86
    :pswitch_2
    new-instance v0, Lcom/twitter/android/profiles/s;

    iget-object v1, p0, Lcom/twitter/android/profiles/p;->b:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/twitter/android/profiles/p;->c:Lcom/twitter/model/core/TwitterUser;

    sget-object v3, Lcom/twitter/android/ProfileActivity;->g:Landroid/net/Uri;

    const-class v4, Lcom/twitter/android/ProfileBlockerInterstitialFragment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/profiles/s;-><init>(Landroid/os/Bundle;Lcom/twitter/model/core/TwitterUser;Landroid/net/Uri;Ljava/lang/Class;)V

    goto :goto_0

    .line 93
    :pswitch_3
    new-instance v0, Lcom/twitter/android/profiles/s;

    iget-object v1, p0, Lcom/twitter/android/profiles/p;->b:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/twitter/android/profiles/p;->c:Lcom/twitter/model/core/TwitterUser;

    sget-object v3, Lcom/twitter/android/ProfileActivity;->e:Landroid/net/Uri;

    const-class v4, Lcom/twitter/android/ProfileProtectedViewFragment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/profiles/s;-><init>(Landroid/os/Bundle;Lcom/twitter/model/core/TwitterUser;Landroid/net/Uri;Ljava/lang/Class;)V

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(I)Lcom/twitter/android/profiles/p;
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/twitter/android/profiles/p;->e:I

    .line 51
    return-object p0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/android/profiles/p;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/twitter/android/profiles/p;->a:Landroid/content/Context;

    .line 31
    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/profiles/p;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/twitter/android/profiles/p;->b:Landroid/os/Bundle;

    .line 36
    return-object p0
.end method

.method public a(Lbqn;)Lcom/twitter/android/profiles/p;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/android/profiles/p;->f:Lbqn;

    .line 56
    return-object p0
.end method

.method public a(Lcom/twitter/android/ProfileActivity$DisplayState;)Lcom/twitter/android/profiles/p;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/twitter/android/profiles/p;->g:Lcom/twitter/android/ProfileActivity$DisplayState;

    .line 61
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/android/profiles/p;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/twitter/android/profiles/p;->c:Lcom/twitter/model/core/TwitterUser;

    .line 41
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/profiles/p;
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/twitter/android/profiles/p;->d:Z

    .line 46
    return-object p0
.end method
