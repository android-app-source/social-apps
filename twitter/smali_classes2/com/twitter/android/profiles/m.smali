.class public Lcom/twitter/android/profiles/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/profiles/t$a;
.implements Lcom/twitter/library/client/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/twitter/android/profiles/t$a;",
        "Lcom/twitter/library/client/s;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/library/client/Session;

.field private final d:Landroid/support/v4/app/LoaderManager;

.field private final e:Lcom/twitter/android/profiles/t;

.field private final f:Z

.field private final g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final h:Lcom/twitter/android/ct;

.field private final i:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/twitter/library/client/p;

.field private k:Lcom/twitter/android/cv;

.field private l:J

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/v;Landroid/support/v4/app/LoaderManager;Lcom/twitter/library/client/p;Lcom/twitter/android/profiles/t;ZLcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/ct;Lcom/twitter/android/av;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/library/client/v;",
            "Landroid/support/v4/app/LoaderManager;",
            "Lcom/twitter/library/client/p;",
            "Lcom/twitter/android/profiles/t;",
            "Z",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/android/ct;",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/profiles/m;->a:Ljava/lang/ref/WeakReference;

    .line 75
    iput-object p2, p0, Lcom/twitter/android/profiles/m;->b:Lcom/twitter/library/client/v;

    .line 76
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profiles/m;->c:Lcom/twitter/library/client/Session;

    .line 77
    iput-object p3, p0, Lcom/twitter/android/profiles/m;->d:Landroid/support/v4/app/LoaderManager;

    .line 78
    iput-object p5, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    .line 79
    iput-boolean p6, p0, Lcom/twitter/android/profiles/m;->f:Z

    .line 80
    iput-object p7, p0, Lcom/twitter/android/profiles/m;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 81
    iput-object p8, p0, Lcom/twitter/android/profiles/m;->h:Lcom/twitter/android/ct;

    .line 82
    iput-object p9, p0, Lcom/twitter/android/profiles/m;->i:Lcom/twitter/android/av;

    .line 83
    iput-object p4, p0, Lcom/twitter/android/profiles/m;->j:Lcom/twitter/library/client/p;

    .line 85
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->L:J

    iput-wide v0, p0, Lcom/twitter/android/profiles/m;->l:J

    .line 88
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 125
    invoke-virtual {p0}, Lcom/twitter/android/profiles/m;->a()Lcom/twitter/android/cv;

    move-result-object v0

    .line 126
    if-nez v0, :cond_0

    .line 134
    :goto_0
    return v2

    .line 129
    :cond_0
    iget-object v3, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v3}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v3

    iget-wide v4, v3, Lcom/twitter/model/core/TwitterUser;->L:J

    .line 130
    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 131
    invoke-virtual {v0, v2}, Lcom/twitter/android/cv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 132
    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v6, v0, Lcom/twitter/model/core/Tweet;->G:J

    cmp-long v0, v6, v4

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 134
    :cond_2
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    :goto_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public a()Lcom/twitter/android/cv;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 92
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Activity;

    .line 93
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    move v7, v8

    .line 96
    :goto_0
    new-instance v0, Lcom/twitter/android/profiles/m$1;

    move-object v2, v1

    check-cast v2, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    iget-boolean v3, p0, Lcom/twitter/android/profiles/m;->f:Z

    iget-object v4, p0, Lcom/twitter/android/profiles/m;->h:Lcom/twitter/android/ct;

    iget-object v1, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    .line 98
    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/profiles/m;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    new-instance v1, Lcom/twitter/ui/view/h$a;

    invoke-direct {v1}, Lcom/twitter/ui/view/h$a;-><init>()V

    .line 100
    invoke-virtual {v1, v8}, Lcom/twitter/ui/view/h$a;->a(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v1

    .line 101
    invoke-virtual {v1, v7}, Lcom/twitter/ui/view/h$a;->f(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/profiles/m$1;-><init>(Lcom/twitter/android/profiles/m;Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/ui/view/h;)V

    iput-object v0, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    .line 110
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    iget-object v1, p0, Lcom/twitter/android/profiles/m;->i:Lcom/twitter/android/av;

    invoke-virtual {v0, v1}, Lcom/twitter/android/cv;->b(Lcom/twitter/android/av;)V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    return-object v0

    .line 95
    :cond_1
    const/4 v0, 0x0

    move v7, v0

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 169
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/profiles/m;->m:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 171
    iget-object v1, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    invoke-virtual {v1, p2}, Lcom/twitter/android/cv;->c(Landroid/database/Cursor;)V

    .line 172
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 173
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/profiles/m;->j:Lcom/twitter/library/client/p;

    new-instance v2, Lbga;

    iget-object v3, p0, Lcom/twitter/android/profiles/m;->c:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    .line 174
    invoke-virtual {v4}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/model/core/TwitterUser;->L:J

    invoke-direct {v2, v0, v3, v4, v5}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    const/4 v0, 0x1

    .line 173
    invoke-virtual {v1, v2, v0, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 177
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/android/profiles/t;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 140
    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->L:J

    iget-wide v2, p0, Lcom/twitter/android/profiles/m;->l:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/profiles/m;->m:Z

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->d:Landroid/support/v4/app/LoaderManager;

    invoke-virtual {v0, v4}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 142
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->d:Landroid/support/v4/app/LoaderManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 144
    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/twitter/android/profiles/m;->a()Lcom/twitter/android/cv;

    .line 117
    invoke-direct {p0}, Lcom/twitter/android/profiles/m;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->L:J

    iput-wide v0, p0, Lcom/twitter/android/profiles/m;->l:J

    .line 119
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->d:Landroid/support/v4/app/LoaderManager;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 120
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, p0}, Lcom/twitter/android/profiles/t;->a(Lcom/twitter/android/profiles/t$a;)V

    .line 122
    :cond_0
    return-void
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 196
    invoke-virtual {p2}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 197
    iget-object v1, p0, Lcom/twitter/android/profiles/m;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v1, p2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 199
    packed-switch p1, :pswitch_data_0

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 201
    :pswitch_0
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/profiles/m;->m:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/profiles/m;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->d:Landroid/support/v4/app/LoaderManager;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public c()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/profiles/m;->m:Z

    .line 231
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 149
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 150
    if-eqz v1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->L:J

    iput-wide v2, p0, Lcom/twitter/android/profiles/m;->l:J

    .line 152
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->e:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, p0}, Lcom/twitter/android/profiles/t;->a(Lcom/twitter/android/profiles/t$a;)V

    .line 153
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 154
    iget-wide v2, p0, Lcom/twitter/android/profiles/m;->l:J

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    .line 155
    sget-object v3, Lbuj;->b:[Ljava/lang/String;

    .line 156
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    .line 157
    new-instance v0, Lcom/twitter/util/android/d;

    iget-wide v6, p0, Lcom/twitter/android/profiles/m;->l:J

    iget-object v2, p0, Lcom/twitter/android/profiles/m;->c:Lcom/twitter/library/client/Session;

    .line 158
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 157
    invoke-static {v6, v7, v8, v9}, Lcom/twitter/database/schema/a;->b(JJ)Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v6, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v4

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 220
    iget-object v1, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    invoke-virtual {v1, p3}, Lcom/twitter/android/cv;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/cd;

    .line 221
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 222
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "tw"

    iget-object v1, v1, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 223
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/profiles/m;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 224
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 225
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 227
    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/profiles/m;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/twitter/android/profiles/m;->k:Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->k()Lcjt;

    move-result-object v0

    invoke-static {}, Lcom/twitter/android/timeline/bl;->bd_()Lcom/twitter/android/timeline/bl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 184
    :cond_0
    return-void
.end method
