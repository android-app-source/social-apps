.class public Lcom/twitter/android/profiles/z;
.super Lcom/twitter/android/profiles/r;
.source "Twttr"


# instance fields
.field private final f:Lcom/twitter/android/metrics/d;

.field private final g:Z


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/v;Lcom/twitter/android/profiles/t;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/metrics/d;Z)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/profiles/r;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/v;Lcom/twitter/android/profiles/t;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 36
    iput-object p5, p0, Lcom/twitter/android/profiles/z;->f:Lcom/twitter/android/metrics/d;

    .line 37
    iput-boolean p6, p0, Lcom/twitter/android/profiles/z;->g:Z

    .line 38
    return-void
.end method


# virtual methods
.method public b()Lcom/twitter/android/bh;
    .locals 8

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/profiles/z;->e:Lcom/twitter/android/bh;

    if-nez v0, :cond_2

    .line 68
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/profiles/z;->a:Lcom/twitter/library/client/Session;

    .line 69
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const v2, 0x7f0200b0

    .line 71
    :goto_0
    new-instance v0, Lcom/twitter/android/bj;

    iget-object v1, p0, Lcom/twitter/android/profiles/z;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/profiles/z;->b:Lcom/twitter/android/profiles/t;

    .line 72
    invoke-virtual {v3}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/twitter/android/profiles/z;->g:Z

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/bj;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;ZLcom/twitter/android/UsersAdapter$CheckboxConfig;Z)V

    .line 74
    invoke-virtual {v0, p0}, Lcom/twitter/android/bj;->a(Lcom/twitter/android/av;)V

    .line 75
    const/4 v5, 0x3

    .line 77
    iget-boolean v1, p0, Lcom/twitter/android/profiles/z;->g:Z

    if-eqz v1, :cond_1

    .line 78
    const/16 v5, 0x23

    .line 80
    :cond_1
    new-instance v1, Lcom/twitter/android/bh;

    iget-object v2, p0, Lcom/twitter/android/profiles/z;->c:Landroid/content/Context;

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/profiles/z;->d()I

    move-result v4

    .line 82
    invoke-static {}, Lcom/twitter/android/revenue/k;->j()Z

    move-result v6

    move-object v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/bh;-><init>(Landroid/content/Context;Lcom/twitter/android/bj;IIZ)V

    iput-object v1, p0, Lcom/twitter/android/profiles/z;->e:Lcom/twitter/android/bh;

    .line 83
    iget-object v0, p0, Lcom/twitter/android/profiles/z;->e:Lcom/twitter/android/bh;

    iget-object v1, p0, Lcom/twitter/android/profiles/z;->b:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/profiles/z;->b:Lcom/twitter/android/profiles/t;

    .line 84
    invoke-virtual {v2}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v2

    .line 83
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/bh;->a(Lcom/twitter/model/core/TwitterUser;Z)V

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/profiles/z;->e:Lcom/twitter/android/bh;

    return-object v0

    .line 69
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/twitter/database/schema/a$y;->w:Landroid/net/Uri;

    return-object v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 48
    const/16 v0, 0xa

    return v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x3

    return v0
.end method

.method protected f()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x6

    return v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x2

    return v0
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method protected j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const-string/jumbo v0, "similar_to"

    return-object v0
.end method

.method protected l()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/profiles/z;->f:Lcom/twitter/android/metrics/d;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/twitter/android/profiles/z;->f:Lcom/twitter/android/metrics/d;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/metrics/d;->a(I)V

    .line 121
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/android/profiles/z;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0, p3}, Lcom/twitter/android/bh;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/twitter/android/profiles/z;->e:Lcom/twitter/android/bh;

    invoke-virtual {v0, p3}, Lcom/twitter/android/bh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 97
    if-eqz v0, :cond_0

    .line 98
    iget-object v1, p0, Lcom/twitter/android/profiles/z;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/profiles/r;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0
.end method
