.class public Lcom/twitter/android/profiles/h;
.super Lcom/twitter/android/profiles/u;
.source "Twttr"


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z


# direct methods
.method public constructor <init>(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/core/TwitterUser;IZLcom/twitter/model/ads/b;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Lcom/twitter/android/profiles/u;-><init>()V

    .line 51
    invoke-static {p4, p3}, Lcom/twitter/android/profiles/v;->a(ZI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->a:Z

    .line 52
    invoke-static {p3}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->b:Z

    .line 53
    invoke-static {p3}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v3

    .line 54
    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->b:Z

    if-nez v0, :cond_0

    if-nez p4, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->d:Z

    .line 55
    if-eqz p2, :cond_1

    iget v0, p2, Lcom/twitter/model/core/TwitterUser;->J:I

    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->e:Z

    .line 56
    if-eqz p2, :cond_2

    if-eqz p4, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->f:Z

    .line 57
    if-eqz p2, :cond_3

    .line 58
    invoke-static {p4, p2, p3}, Lcom/twitter/android/profiles/v;->a(ZLcom/twitter/model/core/TwitterUser;I)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->g:Z

    .line 59
    if-eqz p2, :cond_4

    invoke-static {p3}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez v3, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->h:Z

    .line 60
    invoke-static {p3}, Lcom/twitter/model/core/g;->g(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->c:Z

    .line 61
    if-eqz p2, :cond_5

    if-nez p4, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->i:Z

    .line 62
    if-eqz p2, :cond_6

    invoke-static {p2, p3, p4}, Lcom/twitter/android/profiles/v;->b(Lcom/twitter/model/core/TwitterUser;IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->j:Z

    .line 63
    invoke-static {p1, p2, p5, p6}, Lcom/twitter/android/ads/c;->b(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/ads/b;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->k:Z

    .line 65
    invoke-static {}, Lbrz;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/profiles/h;->l:Z

    .line 66
    if-eqz p2, :cond_7

    if-eqz p4, :cond_7

    invoke-static {}, Lcom/twitter/android/qrcodes/a;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_7
    iput-boolean v1, p0, Lcom/twitter/android/profiles/h;->m:Z

    .line 67
    return-void

    :cond_0
    move v0, v2

    .line 54
    goto :goto_0

    :cond_1
    move v0, v2

    .line 55
    goto :goto_1

    :cond_2
    move v0, v2

    .line 56
    goto :goto_2

    :cond_3
    move v0, v2

    .line 58
    goto :goto_3

    :cond_4
    move v0, v2

    .line 59
    goto :goto_4

    :cond_5
    move v0, v2

    .line 61
    goto :goto_5

    :cond_6
    move v0, v2

    .line 62
    goto :goto_6

    :cond_7
    move v1, v2

    .line 66
    goto :goto_7
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    const v0, 0x7f1308b1

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/android/profiles/h;->j:Z

    invoke-virtual {v0, v3}, Lazv;->b(Z)Lazv;

    .line 72
    const v0, 0x7f1308c0

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lazv;->b(Z)Lazv;

    .line 73
    const v0, 0x7f1308c1

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->a:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lazv;->b(Z)Lazv;

    .line 74
    const v0, 0x7f1308bd

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->h:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->c:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->a:Z

    if-nez v0, :cond_2

    move v0, v1

    .line 75
    :goto_2
    invoke-virtual {v3, v0}, Lazv;->b(Z)Lazv;

    .line 76
    const v0, 0x7f1308be

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->h:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->c:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->a:Z

    if-nez v0, :cond_3

    move v0, v1

    .line 77
    :goto_3
    invoke-virtual {v3, v0}, Lazv;->b(Z)Lazv;

    .line 78
    const v0, 0x7f1308c4

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->e:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/android/profiles/h;->b:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Lazv;->b(Z)Lazv;

    .line 79
    const v0, 0x7f1308bf

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/android/profiles/h;->f:Z

    invoke-virtual {v0, v3}, Lazv;->b(Z)Lazv;

    .line 80
    const v0, 0x7f130883

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/android/profiles/h;->e:Z

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/twitter/android/profiles/h;->b:Z

    if-nez v3, :cond_5

    :goto_5
    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 81
    const v0, 0x7f1308c5

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/profiles/h;->i:Z

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 82
    const v0, 0x7f1308c6

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/profiles/h;->k:Z

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 83
    const v0, 0x7f1308c2

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/profiles/h;->l:Z

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 84
    const v0, 0x7f1308c7

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/profiles/h;->m:Z

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 85
    return-void

    :cond_0
    move v0, v2

    .line 72
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 73
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 74
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 76
    goto :goto_3

    :cond_4
    move v0, v2

    .line 78
    goto :goto_4

    :cond_5
    move v1, v2

    .line 80
    goto :goto_5
.end method
