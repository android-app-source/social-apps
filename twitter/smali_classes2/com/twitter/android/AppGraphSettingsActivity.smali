.class public Lcom/twitter/android/AppGraphSettingsActivity;
.super Lcom/twitter/android/client/TwitterPreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;-><init>()V

    return-void
.end method

.method private a(ZZI)V
    .locals 2

    .prologue
    .line 86
    const-string/jumbo v0, "allow_app_graph"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AppGraphSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 88
    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 89
    invoke-virtual {v0, p2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 90
    if-lez p3, :cond_0

    .line 91
    invoke-virtual {v0, p3}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/service/s;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 99
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 100
    if-ne p2, v7, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/AppGraphSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 102
    new-instance v2, Lcom/twitter/util/a;

    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/AppGraphSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v1, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 104
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 105
    check-cast p1, Lbhj;

    invoke-virtual {p1}, Lbhj;->g()Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v3, "app_graph_status"

    .line 108
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 107
    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 111
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "undetermined"

    .line 112
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113
    :cond_0
    const v0, 0x7f0a005f

    invoke-direct {p0, v6, v6, v0}, Lcom/twitter/android/AppGraphSettingsActivity;->a(ZZI)V

    .line 122
    :cond_1
    :goto_1
    return-void

    .line 108
    :cond_2
    const-string/jumbo v0, "undetermined"

    goto :goto_0

    .line 115
    :cond_3
    const-string/jumbo v0, "optin"

    .line 116
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 115
    invoke-direct {p0, v7, v0, v6}, Lcom/twitter/android/AppGraphSettingsActivity;->a(ZZI)V

    goto :goto_1

    .line 119
    :cond_4
    const v0, 0x7f0a005e

    invoke-direct {p0, v6, v6, v0}, Lcom/twitter/android/AppGraphSettingsActivity;->a(ZZI)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 32
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f0a080e

    invoke-virtual {p0, v0}, Lcom/twitter/android/AppGraphSettingsActivity;->setTitle(I)V

    .line 34
    invoke-virtual {p0}, Lcom/twitter/android/AppGraphSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 35
    const-string/jumbo v1, "AppGraphSettingsActivity_account_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 37
    const v2, 0x7f080004

    invoke-virtual {p0, v2}, Lcom/twitter/android/AppGraphSettingsActivity;->addPreferencesFromResource(I)V

    .line 39
    new-instance v2, Lcom/twitter/util/a;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    const-string/jumbo v0, "app_graph_status"

    const-string/jumbo v1, "undetermined"

    .line 40
    invoke-virtual {v2, v0, v1}, Lcom/twitter/util/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    const-string/jumbo v1, "allow_app_graph"

    invoke-virtual {p0, v1}, Lcom/twitter/android/AppGraphSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 43
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 45
    sget-object v1, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v1}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/api/c;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    const v0, 0x7f0a005d

    invoke-direct {p0, v4, v4, v0}, Lcom/twitter/android/AppGraphSettingsActivity;->a(ZZI)V

    .line 57
    :goto_0
    const-string/jumbo v0, "app_graph_learn_more"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AppGraphSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v2, 0x7f0a0c44

    .line 58
    invoke-virtual {p0, v2}, Lcom/twitter/android/AppGraphSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 59
    return-void

    .line 49
    :cond_0
    new-instance v1, Lbhj;

    invoke-virtual {p0}, Lcom/twitter/android/AppGraphSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/AppGraphSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lbhj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v1, v5}, Lcom/twitter/android/AppGraphSettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 51
    const-string/jumbo v1, "undetermined"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    const v0, 0x7f0a005f

    invoke-direct {p0, v4, v4, v0}, Lcom/twitter/android/AppGraphSettingsActivity;->a(ZZI)V

    goto :goto_0

    .line 54
    :cond_1
    const-string/jumbo v1, "optin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, v5, v0, v4}, Lcom/twitter/android/AppGraphSettingsActivity;->a(ZZI)V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 73
    if-nez v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    const-string/jumbo v2, "allow_app_graph"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "optin"

    .line 79
    :goto_1
    invoke-static {p0}, Lcom/twitter/library/client/c;->a(Landroid/content/Context;)Lcom/twitter/library/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/c;->a(Ljava/lang/String;)V

    .line 80
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    :cond_2
    const-string/jumbo v0, "optout"

    goto :goto_1
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onResume()V

    .line 64
    invoke-virtual {p0}, Lcom/twitter/android/AppGraphSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/AppGraphSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 68
    :cond_0
    return-void
.end method
