.class Lcom/twitter/android/TweetFragment$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/TweetFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    .prologue
    .line 2626
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/TweetFragment$1;)V
    .locals 0

    .prologue
    .line 2626
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment$c;-><init>(Lcom/twitter/android/TweetFragment;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x2

    .line 2631
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v2

    .line 2632
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 2634
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v4

    const v0, 0x7f13006e

    if-ne v3, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v4, v0}, Lcom/twitter/app/common/list/l;->b(I)I

    .line 2636
    sparse-switch v3, :sswitch_data_0

    .line 2701
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2634
    goto :goto_0

    .line 2638
    :sswitch_0
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2639
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->O(Lcom/twitter/android/TweetFragment;)Lati;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2640
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->O(Lcom/twitter/android/TweetFragment;)Lati;

    move-result-object v0

    invoke-interface {v0, v2}, Lati;->a(Lcom/twitter/model/core/Tweet;)V

    .line 2642
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "reply"

    invoke-static {v0, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V

    goto :goto_1

    .line 2644
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x3

    .line 2645
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    .line 2644
    invoke-static {v0, v1, v2}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_1

    .line 2650
    :sswitch_1
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2651
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/TweetFragment;->a(Landroid/view/View;)V

    goto :goto_1

    .line 2653
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->j(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/widget/ToggleImageButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/ToggleImageButton;->a()V

    .line 2654
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    .line 2655
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    .line 2654
    invoke-static {v0, v1, v2}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_1

    .line 2660
    :sswitch_2
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2661
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->ae_()V

    goto :goto_1

    .line 2663
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2664
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    .line 2663
    invoke-static {v0, v1, v2}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_1

    .line 2669
    :sswitch_3
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->t()V

    goto :goto_1

    .line 2673
    :sswitch_4
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2674
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "share_via_dm"

    invoke-static {v0, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V

    .line 2675
    new-instance v0, Lcom/twitter/model/core/r;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/core/r;-><init>(Lcom/twitter/model/core/Tweet;)V

    .line 2676
    new-instance v1, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    .line 2677
    invoke-virtual {v2}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v3}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V

    .line 2678
    invoke-virtual {v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2679
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a(Lcom/twitter/model/core/r;)V

    goto/16 :goto_1

    .line 2681
    :cond_5
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v2}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/twitter/app/dm/r$a;

    invoke-direct {v3}, Lcom/twitter/app/dm/r$a;-><init>()V

    .line 2683
    invoke-virtual {v3, v0}, Lcom/twitter/app/dm/r$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    .line 2684
    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/app/dm/r$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    .line 2685
    invoke-virtual {v0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    .line 2681
    invoke-static {v2, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/r;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 2688
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0xa

    .line 2689
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v2

    .line 2688
    invoke-static {v0, v1, v2}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto/16 :goto_1

    .line 2694
    :sswitch_5
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$c;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->q()V

    goto/16 :goto_1

    .line 2636
    :sswitch_data_0
    .sparse-switch
        0x7f130032 -> :sswitch_1
        0x7f13006e -> :sswitch_0
        0x7f130071 -> :sswitch_2
        0x7f130149 -> :sswitch_4
        0x7f13014a -> :sswitch_5
        0x7f13088e -> :sswitch_3
    .end sparse-switch
.end method
