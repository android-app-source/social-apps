.class Lcom/twitter/android/ck$5;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/core/Tweet;

.field final synthetic b:Lcom/twitter/model/util/FriendshipCache;

.field final synthetic c:Lcom/twitter/android/ck;


# direct methods
.method constructor <init>(Lcom/twitter/android/ck;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/twitter/android/ck$5;->c:Lcom/twitter/android/ck;

    iput-object p2, p0, Lcom/twitter/android/ck$5;->a:Lcom/twitter/model/core/Tweet;

    iput-object p3, p0, Lcom/twitter/android/ck$5;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 384
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ck$5;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 5

    .prologue
    .line 387
    iget-object v1, p0, Lcom/twitter/android/ck$5;->c:Lcom/twitter/android/ck;

    iget-object v0, p0, Lcom/twitter/android/ck$5;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    iget-object v4, p0, Lcom/twitter/android/ck$5;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/twitter/android/ck;->a(Lcom/twitter/android/ck;JLcom/twitter/library/service/u;Lcom/twitter/model/util/FriendshipCache;)V

    .line 389
    return-void
.end method
