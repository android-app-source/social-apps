.class Lcom/twitter/android/ImageActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/MediaImageView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ImageActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/ImageActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ImageActivity;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/twitter/android/ImageActivity$1;->a:Lcom/twitter/android/ImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 0

    .prologue
    .line 115
    check-cast p1, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ImageActivity$1;->a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 3

    .prologue
    .line 118
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 120
    iget-object v1, p0, Lcom/twitter/android/ImageActivity$1;->a:Lcom/twitter/android/ImageActivity;

    iget-object v1, v1, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 121
    iget-object v1, p0, Lcom/twitter/android/ImageActivity$1;->a:Lcom/twitter/android/ImageActivity;

    iput-object v0, v1, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    .line 122
    iget-object v1, p0, Lcom/twitter/android/ImageActivity$1;->a:Lcom/twitter/android/ImageActivity;

    invoke-virtual {v1}, Lcom/twitter/android/ImageActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->h()V

    .line 123
    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/twitter/android/ImageActivity$1;->a:Lcom/twitter/android/ImageActivity;

    const v1, 0x7f0a04b2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 127
    :cond_0
    return-void
.end method
