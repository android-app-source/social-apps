.class public Lcom/twitter/android/trends/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/twitter/android/trends/TrendBadgesView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/twitter/android/trends/TrendBadgesView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/android/trends/b;->a:Landroid/content/res/Resources;

    .line 48
    iput-object p2, p0, Lcom/twitter/android/trends/b;->b:Landroid/view/View;

    .line 49
    iput-object p3, p0, Lcom/twitter/android/trends/b;->c:Landroid/widget/TextView;

    .line 50
    iput-object p4, p0, Lcom/twitter/android/trends/b;->d:Landroid/widget/TextView;

    .line 51
    iput-object p5, p0, Lcom/twitter/android/trends/b;->e:Lcom/twitter/android/trends/TrendBadgesView;

    .line 52
    iput-object p6, p0, Lcom/twitter/android/trends/b;->f:Landroid/widget/TextView;

    .line 53
    iput-object p7, p0, Lcom/twitter/android/trends/b;->g:Landroid/widget/TextView;

    .line 54
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/content/res/Resources;)Lcom/twitter/android/trends/b;
    .locals 8

    .prologue
    .line 34
    const v0, 0x7f04020e

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 35
    const v0, 0x7f13052d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 36
    const v0, 0x7f130530

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 37
    const v0, 0x7f13052e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/trends/TrendBadgesView;

    .line 38
    const v0, 0x7f13052f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 39
    const v0, 0x7f130531

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 40
    new-instance v0, Lcom/twitter/android/trends/b;

    move-object v1, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/trends/b;-><init>(Landroid/content/res/Resources;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/twitter/android/trends/TrendBadgesView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/android/trends/b;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/trends/b;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/trends/b;->e:Lcom/twitter/android/trends/TrendBadgesView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/trends/TrendBadgesView;->setBadges(Ljava/util/List;)V

    .line 68
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/trends/b;->b:Landroid/view/View;

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/trends/b;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lcom/twitter/android/trends/b;->d:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/trends/b;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 73
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/trends/b;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/trends/b;->f:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/trends/b;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 88
    iget-object v0, p0, Lcom/twitter/android/trends/b;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    if-nez p1, :cond_0

    const v0, 0x7f0a06f3

    .line 90
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/trends/b;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/trends/b;->a:Landroid/content/res/Resources;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void

    .line 89
    :cond_0
    const v0, 0x7f0a06f4

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/trends/b;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/trends/b;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 107
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/twitter/library/view/b;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/twitter/library/view/b;-><init>(Ljava/lang/String;I)V

    .line 109
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v2, p0, Lcom/twitter/android/trends/b;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/trends/b;->c:Landroid/widget/TextView;

    invoke-static {v2, v0, v1, v3, v4}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Landroid/view/View;Z)I

    .line 112
    iget-object v1, p0, Lcom/twitter/android/trends/b;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    return-void
.end method
