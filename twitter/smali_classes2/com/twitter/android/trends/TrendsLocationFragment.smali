.class public Lcom/twitter/android/trends/TrendsLocationFragment;
.super Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/trends/TrendsLocationFragment$a;,
        Lcom/twitter/android/trends/TrendsLocationFragment$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/library/api/TwitterLocation;",
        ">;"
    }
.end annotation


# instance fields
.field private e:Lcom/twitter/android/trends/TrendsLocationFragment$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    const v0, 0x7f0403fb

    invoke-super {p0, p1, v0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->a(Landroid/view/LayoutInflater;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/trends/TrendsLocationFragment$a;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/twitter/android/trends/TrendsLocationFragment;->e:Lcom/twitter/android/trends/TrendsLocationFragment$a;

    .line 32
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;JLjava/lang/Object;I)Z
    .locals 6

    .prologue
    .line 23
    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    move-object v4, p4

    check-cast v4, Lcom/twitter/library/api/TwitterLocation;

    move-object v0, p0

    move-wide v2, p2

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/trends/TrendsLocationFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/TwitterLocation;I)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;JLcom/twitter/library/api/TwitterLocation;I)Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsLocationFragment;->e:Lcom/twitter/android/trends/TrendsLocationFragment$a;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsLocationFragment;->e:Lcom/twitter/android/trends/TrendsLocationFragment$a;

    invoke-interface {v0, p4}, Lcom/twitter/android/trends/TrendsLocationFragment$a;->a(Lcom/twitter/library/api/TwitterLocation;)V

    .line 77
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected e()Lna;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lna",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/api/TwitterLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lnc;

    new-instance v1, Lnb;

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsLocationFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsLocationFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 56
    invoke-static {}, Lcom/twitter/android/client/w;->h()I

    move-result v4

    int-to-long v4, v4

    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct/range {v1 .. v6}, Lnb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLandroid/os/Handler;)V

    invoke-direct {v0, v1}, Lnc;-><init>(Lnb;)V

    .line 55
    return-object v0
.end method

.method protected f()Lnh;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnh",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/android/trends/TrendsLocationFragment$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/trends/TrendsLocationFragment$b;-><init>(Lcom/twitter/android/trends/TrendsLocationFragment$1;)V

    return-object v0
.end method

.method protected g()Lmq;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmq",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/api/TwitterLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lmr;

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsLocationFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lmr;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a096f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 38
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->onStart()V

    .line 43
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsLocationFragment;->j()V

    .line 44
    return-void
.end method
