.class public Lcom/twitter/android/trends/TrendsPlusActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$a;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/trends/TrendsPlusActivity$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/trends/TrendsPlusActivity$a;

.field private final b:Lcom/twitter/android/trends/e;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    .line 43
    new-instance v0, Lcom/twitter/android/trends/TrendsPlusActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/trends/TrendsPlusActivity$a;-><init>(Lcom/twitter/android/trends/TrendsPlusActivity;Lcom/twitter/android/trends/TrendsPlusActivity$1;)V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->a:Lcom/twitter/android/trends/TrendsPlusActivity$a;

    .line 44
    new-instance v0, Lcom/twitter/android/trends/e;

    invoke-direct {v0}, Lcom/twitter/android/trends/e;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->b:Lcom/twitter/android/trends/e;

    return-void
.end method

.method private a(Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 115
    const v0, 0x7f1308d3

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 116
    invoke-static {p2}, Lcom/twitter/android/trends/g;->a(Lcom/twitter/library/client/Session;)Z

    move-result v1

    .line 115
    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 117
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/trends/TrendsPlusActivity;)Z
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->N()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/trends/TrendsPlusActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/trends/TrendsPlusActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/android/ListFragmentActivity$a;
    .locals 5

    .prologue
    .line 63
    new-instance v1, Lcom/twitter/android/trends/TrendsPlusFragment;

    invoke-direct {v1}, Lcom/twitter/android/trends/TrendsPlusFragment;-><init>()V

    .line 64
    new-instance v0, Lcom/twitter/android/timeline/cb$a;

    .line 65
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/timeline/cb$a;-><init>(Landroid/os/Bundle;)V

    const v2, 0x7f0a0977

    .line 66
    invoke-virtual {v0, v2}, Lcom/twitter/android/timeline/cb$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cb$a;

    const v2, 0x7f0a0978

    .line 67
    invoke-virtual {v0, v2}, Lcom/twitter/android/timeline/cb$a;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cb$a;

    const-string/jumbo v2, "show_header"

    const-string/jumbo v3, "show_header"

    const/4 v4, 0x1

    .line 69
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 68
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/timeline/cb$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cb$a;

    const-string/jumbo v2, "TRENDSPLUS"

    .line 70
    invoke-virtual {v0, v2}, Lcom/twitter/android/timeline/cb$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cb$a;

    .line 72
    invoke-virtual {v0}, Lcom/twitter/android/timeline/cb$a;->a()Lcom/twitter/android/timeline/cb;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/trends/TrendsPlusFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 73
    new-instance v0, Lcom/twitter/android/ListFragmentActivity$a;

    invoke-direct {v0, v1}, Lcom/twitter/android/ListFragmentActivity$a;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 80
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 81
    return-object p2
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 54
    const-string/jumbo v0, "show_header"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0965

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 201
    if-ne p2, v4, :cond_0

    .line 202
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 203
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v0, v2

    const-string/jumbo v2, "search"

    aput-object v2, v0, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "trends_dialog"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "keep_tailored_trends"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v0, v2

    .line 204
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 203
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 206
    :cond_0
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 189
    if-ne p2, v4, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 191
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/trends/TrendsLocationsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1, v4}, Lcom/twitter/android/trends/TrendsPlusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 193
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v0, v2

    const-string/jumbo v2, "search"

    aput-object v2, v0, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "trends_dialog"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "confirm_change_location"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v0, v2

    .line 194
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 193
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 196
    :cond_0
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 147
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 148
    const v2, 0x7f1308d3

    if-ne v0, v2, :cond_1

    .line 149
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v2

    .line 152
    if-eqz v2, :cond_0

    .line 153
    iput-boolean v1, v2, Lcom/twitter/model/account/UserSettings;->C:Z

    .line 154
    iget-object v3, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->G:Lcom/twitter/library/client/p;

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v1, v4}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)Lbbg;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 157
    :cond_0
    invoke-interface {p1, v6}, Lcmm;->f(Z)Lcmm;

    .line 158
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v0, v6

    const-string/jumbo v3, "search"

    aput-object v3, v0, v1

    const-string/jumbo v3, "menu"

    aput-object v3, v0, v7

    const-string/jumbo v3, "get_tailored_trends"

    aput-object v3, v0, v8

    const-string/jumbo v3, "click"

    aput-object v3, v0, v9

    .line 159
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 158
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v1

    .line 182
    :goto_0
    return v0

    .line 161
    :cond_1
    const v2, 0x7f1308d2

    if-ne v0, v2, :cond_3

    .line 162
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 163
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->C:Z

    if-eqz v0, :cond_2

    .line 166
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v3, 0x7f0a0972

    .line 167
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v3, 0x7f0a0971

    .line 168
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v3, 0x7f0a0970

    .line 169
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 170
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 171
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 172
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 173
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 178
    :goto_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v2, v6

    const-string/jumbo v3, "search"

    aput-object v3, v2, v1

    const-string/jumbo v3, "menu"

    aput-object v3, v2, v7

    const-string/jumbo v3, "change_location"

    aput-object v3, v2, v8

    const-string/jumbo v3, "click"

    aput-object v3, v2, v9

    .line 179
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 178
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v1

    .line 180
    goto :goto_0

    .line 175
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/trends/TrendsLocationsActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/trends/TrendsPlusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 182
    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->a(Lcmr;)Z

    .line 122
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const v0, 0x7f140030

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 125
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 4

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->b(Lcmr;)I

    .line 104
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 105
    const v1, 0x7f13088d

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 106
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 107
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lazv;->b(Z)Lazv;

    .line 108
    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 109
    invoke-direct {p0, v0, v2}, Lcom/twitter/android/trends/TrendsPlusActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/library/client/Session;)V

    .line 110
    const/4 v0, 0x2

    return v0
.end method

.method public b()Lcom/twitter/android/trends/TrendsPlusFragment;
    .locals 2

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f1302e4

    .line 218
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 219
    instance-of v1, v0, Lcom/twitter/android/trends/TrendsPlusFragment;

    if-eqz v1, :cond_0

    .line 220
    check-cast v0, Lcom/twitter/android/trends/TrendsPlusFragment;

    .line 222
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 3

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 131
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ed

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->c:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->c:Landroid/view/View;

    const v1, 0x7f130384

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->d:Landroid/widget/TextView;

    .line 133
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->c:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/trends/TrendsPlusActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/trends/TrendsPlusActivity$1;-><init>(Lcom/twitter/android/trends/TrendsPlusActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "focus_search_bar"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->e:Z

    .line 141
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    .line 210
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ListFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 211
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->b:Lcom/twitter/android/trends/e;

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v4

    .line 212
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v6

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusActivity;->b()Lcom/twitter/android/trends/TrendsPlusFragment;

    move-result-object v7

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p0

    .line 211
    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/trends/e;->a(IILandroid/content/Intent;Lcom/twitter/library/client/Session;Landroid/content/Context;Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/android/trends/TrendsPlusFragment;)V

    .line 213
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->G:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->a:Lcom/twitter/android/trends/TrendsPlusActivity$a;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 87
    invoke-super {p0}, Lcom/twitter/android/ListFragmentActivity;->onPause()V

    .line 88
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0}, Lcom/twitter/android/ListFragmentActivity;->onResume()V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->G:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->a:Lcom/twitter/android/trends/TrendsPlusActivity$a;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 94
    iget-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->e:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity;->e:Z

    .line 98
    :cond_0
    return-void
.end method
