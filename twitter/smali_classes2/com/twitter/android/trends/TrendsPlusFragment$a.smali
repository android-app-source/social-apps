.class Lcom/twitter/android/trends/TrendsPlusFragment$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/trends/TrendsPlusFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/av",
        "<",
        "Landroid/view/View;",
        "Lcgi;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/android/trends/TrendsPlusFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/trends/TrendsPlusFragment;)V
    .locals 1

    .prologue
    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a:Ljava/util/HashSet;

    .line 338
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->b:Ljava/util/HashSet;

    .line 341
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->c:Ljava/util/List;

    .line 343
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->d:Ljava/util/List;

    .line 348
    iput-object p1, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->e:Lcom/twitter/android/trends/TrendsPlusFragment;

    .line 349
    return-void
.end method

.method private static a(Lcom/twitter/android/cf$g;)Z
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/twitter/android/cf$g;->c:Lcom/twitter/android/cf$l;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/cf$g;->e:Lcom/twitter/android/cf$j;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/cf$g;->f:Lcom/twitter/android/cf$i;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/cf$g;->d:Lcom/twitter/android/cf$k;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 370
    if-eqz p1, :cond_1

    .line 371
    const-string/jumbo v0, "logged_promoted_trend_ids"

    .line 372
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 373
    if-eqz v0, :cond_0

    .line 374
    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 376
    :cond_0
    const-string/jumbo v0, "viewed_trend_ids"

    .line 377
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 378
    if-eqz v0, :cond_1

    .line 379
    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->b:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 382
    :cond_1
    return-void
.end method

.method public a(Landroid/view/View;Lcgi;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 397
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cf$g;

    .line 398
    const-string/jumbo v1, "trend_id"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 400
    invoke-static {v0}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a(Lcom/twitter/android/cf$g;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->b:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 401
    const-string/jumbo v0, "entity_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 402
    if-eqz v0, :cond_0

    .line 403
    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    :cond_0
    const-string/jumbo v0, "position"

    const/4 v1, -0x1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 406
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 407
    const-string/jumbo v2, "name"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 408
    const/16 v2, 0x8

    iput v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 409
    iput v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 410
    const-string/jumbo v2, "description"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 412
    if-eqz p2, :cond_1

    .line 413
    iget-wide v2, p2, Lcgi;->e:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 416
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    const-string/jumbo v1, "isread"

    const/4 v2, 0x1

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 419
    const-string/jumbo v1, "changes"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 420
    if-eqz v1, :cond_2

    if-ltz v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->e:Lcom/twitter/android/trends/TrendsPlusFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/trends/TrendsPlusFragment;->a(Landroid/view/View;)V

    .line 426
    :cond_2
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a:Ljava/util/HashSet;

    iget-wide v2, p2, Lcgi;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 427
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->e:Lcom/twitter/android/trends/TrendsPlusFragment;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    iget-wide v2, p2, Lcgi;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/trends/TrendsPlusFragment;->a(Lcom/twitter/library/api/PromotedEvent;J)V

    .line 429
    :cond_3
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 331
    check-cast p2, Lcgi;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a(Landroid/view/View;Lcgi;Landroid/os/Bundle;)V

    return-void
.end method

.method b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 358
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 385
    const-string/jumbo v0, "logged_promoted_trend_ids"

    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 386
    const-string/jumbo v0, "viewed_trend_ids"

    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->b:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 387
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 363
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment$a;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 367
    return-void
.end method
