.class public Lcom/twitter/android/trends/f;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/util/a;


# direct methods
.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;
    .locals 4

    .prologue
    .line 70
    sget-object v0, Lcom/twitter/android/trends/f;->a:Lcom/twitter/util/a;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/twitter/util/a;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v1, "trend_loc_prefs"

    invoke-direct {v0, p0, v2, v3, v1}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 74
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/android/trends/f;->a:Lcom/twitter/util/a;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/search/f;)V
    .locals 3

    .prologue
    .line 63
    invoke-static {p0, p1}, Lcom/twitter/android/trends/f;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "lang"

    .line 64
    invoke-virtual {p2}, Lcom/twitter/library/api/search/f;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "country"

    .line 65
    invoke-virtual {p2}, Lcom/twitter/library/api/search/f;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 67
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/Locale;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39
    if-eqz p2, :cond_0

    .line 40
    invoke-static {p0, p1}, Lcom/twitter/android/trends/f;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;

    move-result-object v0

    .line 41
    const-string/jumbo v1, "lang"

    invoke-virtual {v0, v1, v3}, Lcom/twitter/util/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 42
    const-string/jumbo v2, "country"

    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {p2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 48
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
