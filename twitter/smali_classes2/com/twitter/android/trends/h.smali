.class public Lcom/twitter/android/trends/h;
.super Lckb$a;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/trends/b;


# direct methods
.method public constructor <init>(Lcom/twitter/android/trends/b;)V
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/twitter/android/trends/b;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lckb$a;-><init>(Landroid/view/View;)V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    .line 26
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/view/ViewGroup;)Lcom/twitter/android/trends/h;
    .locals 2

    .prologue
    .line 19
    invoke-static {p0, p2, p1}, Lcom/twitter/android/trends/b;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/content/res/Resources;)Lcom/twitter/android/trends/b;

    move-result-object v0

    .line 20
    new-instance v1, Lcom/twitter/android/trends/h;

    invoke-direct {v1, v0}, Lcom/twitter/android/trends/h;-><init>(Lcom/twitter/android/trends/b;)V

    return-object v1
.end method


# virtual methods
.method public a(Lchy;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    iget-object v1, p1, Lchy;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/trends/a;->a(Lcom/twitter/android/trends/b;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    iget-object v1, p1, Lchy;->f:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->a(Ljava/util/List;)V

    .line 31
    iget-object v0, p1, Lchy;->g:Lchb;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    iget-object v1, p1, Lchy;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->c(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    iget-object v1, p1, Lchy;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->e(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {v0}, Lcom/twitter/android/trends/b;->b()V

    .line 40
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    new-instance v1, Lcom/twitter/android/trends/h$1;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/trends/h$1;-><init>(Lcom/twitter/android/trends/h;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->a(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    iget-object v1, p1, Lchy;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->b(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {v0}, Lcom/twitter/android/trends/b;->d()V

    .line 38
    iget-object v0, p0, Lcom/twitter/android/trends/h;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {v0}, Lcom/twitter/android/trends/b;->c()V

    goto :goto_0
.end method
