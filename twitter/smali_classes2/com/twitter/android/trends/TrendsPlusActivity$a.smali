.class Lcom/twitter/android/trends/TrendsPlusActivity$a;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/trends/TrendsPlusActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/trends/TrendsPlusActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/trends/TrendsPlusActivity;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/twitter/android/trends/TrendsPlusActivity$a;->a:Lcom/twitter/android/trends/TrendsPlusActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/trends/TrendsPlusActivity;Lcom/twitter/android/trends/TrendsPlusActivity$1;)V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lcom/twitter/android/trends/TrendsPlusActivity$a;-><init>(Lcom/twitter/android/trends/TrendsPlusActivity;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 225
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/trends/TrendsPlusActivity$a;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 229
    instance-of v0, p1, Lbbg;

    if-eqz v0, :cond_2

    .line 230
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    iget-object v2, p0, Lcom/twitter/android/trends/TrendsPlusActivity$a;->a:Lcom/twitter/android/trends/TrendsPlusActivity;

    invoke-static {v2}, Lcom/twitter/android/trends/TrendsPlusActivity;->c(Lcom/twitter/android/trends/TrendsPlusActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 232
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity$a;->a:Lcom/twitter/android/trends/TrendsPlusActivity;

    invoke-virtual {v0}, Lcom/twitter/android/trends/TrendsPlusActivity;->b()Lcom/twitter/android/trends/TrendsPlusFragment;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {v0}, Lcom/twitter/android/trends/TrendsPlusFragment;->bf_()V

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusActivity$a;->a:Lcom/twitter/android/trends/TrendsPlusActivity;

    const v1, 0x7f0a097a

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 243
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/service/t;->a(Lcom/twitter/library/service/s;)V

    goto :goto_0
.end method
