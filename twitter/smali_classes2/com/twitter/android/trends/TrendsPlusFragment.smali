.class public Lcom/twitter/android/trends/TrendsPlusFragment;
.super Lcom/twitter/app/common/timeline/TimelineFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/trends/TrendsPlusFragment$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

.field private c:Lcom/twitter/util/a;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Z

.field private t:I

.field private u:Z

.field private v:Lcom/twitter/android/trends/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "trendsplus"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "search"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "trendsplus"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "results"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/trends/TrendsPlusFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;-><init>()V

    .line 68
    new-instance v0, Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/trends/TrendsPlusFragment$a;-><init>(Lcom/twitter/android/trends/TrendsPlusFragment;)V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    return-void
.end method

.method private b(J)V
    .locals 5

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->b()Ljava/util/List;

    move-result-object v1

    .line 106
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/twitter/android/trends/TrendsPlusFragment;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 108
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 109
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 107
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->c()V

    .line 112
    return-void
.end method

.method private t()V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a()Ljava/util/List;

    move-result-object v1

    .line 121
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/twitter/library/api/search/e;

    .line 123
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/api/search/e;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/search/e;->d(I)Lcom/twitter/library/service/j;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/e;

    .line 125
    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/api/search/e;->b:Ljava/util/Collection;

    .line 126
    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->S:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 128
    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->s()Lcom/twitter/android/timeline/cb;

    move-result-object v0

    return-object v0
.end method

.method protected H_()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-virtual {v0}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->d()V

    .line 274
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/trends/TrendsPlusFragment;->b(J)V

    .line 275
    invoke-direct {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->t()V

    .line 276
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->H_()V

    .line 277
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->s()Lcom/twitter/android/timeline/cb;

    move-result-object v0

    return-object v0
.end method

.method public V_()V
    .locals 0

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->aD()V

    .line 182
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->V_()V

    .line 183
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 221
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 222
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    .line 223
    if-eqz v1, :cond_0

    .line 224
    new-instance v2, Lcom/twitter/android/trends/TrendsPlusFragment$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/trends/TrendsPlusFragment$1;-><init>(Lcom/twitter/android/trends/TrendsPlusFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    :cond_0
    return-object v0
.end method

.method protected a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;ZZ)Lcom/twitter/android/cf;
    .locals 2

    .prologue
    .line 318
    .line 319
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;ZZ)Lcom/twitter/android/cf;

    move-result-object v0

    .line 321
    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/av;)V

    .line 322
    return-object v0
.end method

.method protected a(JJ)V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(JJ)V

    .line 100
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/trends/TrendsPlusFragment;->b(J)V

    .line 101
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 300
    .line 301
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0207fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 303
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 304
    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 305
    return-void
.end method

.method protected a(Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcbi;)V

    .line 249
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    .line 250
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 251
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    sget v1, Lbue;->C:I

    .line 253
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/topic/b;->a:Lcom/twitter/util/serialization/l;

    .line 252
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/e;

    .line 255
    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/model/topic/e;->j:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->u:Z

    .line 257
    :cond_0
    return-void

    .line 250
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bl;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 255
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 237
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 238
    const v0, 0x7f040405

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 239
    const v0, 0x7f040341

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 240
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const v0, 0x7f0403e3

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 242
    const v0, 0x7f0400e1

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 244
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/api/PromotedEvent;J)V
    .locals 4

    .prologue
    .line 308
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbel;

    iget-object v2, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->T:Landroid/content/Context;

    .line 309
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1}, Lbel;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/PromotedEvent;)V

    .line 310
    invoke-virtual {v1, p2, p3}, Lbel;->a(J)Lbel;

    move-result-object v1

    .line 308
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 311
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 4

    .prologue
    .line 153
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 154
    const/16 v0, 0x1c

    if-ne p2, v0, :cond_1

    .line 155
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 156
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    instance-of v0, p1, Lbgg;

    if-eqz v0, :cond_0

    .line 158
    check-cast p1, Lbgg;

    .line 160
    iget-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->u:Z

    .line 161
    invoke-virtual {p1}, Lbgg;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->u:Z

    .line 164
    invoke-virtual {p1}, Lbgg;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lbgg;->N()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->at()V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->c:Lcom/twitter/util/a;

    .line 170
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->d:Ljava/lang/String;

    .line 171
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 175
    :cond_1
    return-void
.end method

.method protected aP_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 82
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aP_()V

    .line 83
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "enter"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 84
    return-void
.end method

.method public bf_()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->e:Z

    .line 116
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->at()V

    .line 117
    return-void
.end method

.method protected e(I)Lcom/twitter/library/service/s;
    .locals 4

    .prologue
    .line 281
    iget v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->J:I

    const/16 v1, 0x1c

    if-ne v0, v1, :cond_2

    .line 282
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 283
    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    .line 284
    :goto_0
    new-instance v1, Lbgg;

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lbgg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V

    .line 286
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 287
    const-string/jumbo v0, "Not triggered by a user action."

    invoke-virtual {v1, v0}, Lbgg;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 289
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, p1}, Lcom/twitter/android/trends/TrendsPlusFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 290
    const-string/jumbo v2, "scribe_event"

    invoke-virtual {v1, v2, v0}, Lbgg;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 292
    iput p1, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->t:I

    move-object v0, v1

    .line 295
    :goto_1
    return-object v0

    .line 283
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->e(I)Lcom/twitter/library/service/s;

    move-result-object v0

    goto :goto_1
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->s()Lcom/twitter/android/timeline/cb;

    move-result-object v0

    return-object v0
.end method

.method protected l()V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->onCreate(Landroid/os/Bundle;)V

    .line 191
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->f:Z

    .line 192
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 193
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->C:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 195
    :goto_1
    iget-object v4, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-virtual {v4, p1}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->a(Landroid/os/Bundle;)V

    .line 198
    if-eqz p1, :cond_4

    const-string/jumbo v4, "state_is_degraded"

    if-nez v0, :cond_3

    .line 199
    :goto_2
    invoke-virtual {p1, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    :cond_0
    :goto_3
    iput-boolean v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->u:Z

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->s()Lcom/twitter/android/timeline/cb;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/timeline/cb;->e:Ljava/lang/String;

    .line 202
    if-eqz v0, :cond_5

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "refresh_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->d:Ljava/lang/String;

    .line 208
    :goto_4
    new-instance v0, Lcom/twitter/util/a;

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v4, "trendsplus"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->c:Lcom/twitter/util/a;

    .line 209
    new-instance v0, Lcom/twitter/android/trends/g;

    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/trends/g;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->v:Lcom/twitter/android/trends/g;

    .line 210
    return-void

    :cond_1
    move v0, v2

    .line 191
    goto :goto_0

    :cond_2
    move v0, v2

    .line 194
    goto :goto_1

    :cond_3
    move v1, v2

    .line 198
    goto :goto_2

    .line 199
    :cond_4
    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_3

    .line 205
    :cond_5
    const-string/jumbo v0, "refresh_time"

    iput-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->d:Ljava/lang/String;

    goto :goto_4
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 214
    const-string/jumbo v0, "state_is_degraded"

    iget-boolean v1, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 215
    iget-object v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->b:Lcom/twitter/android/trends/TrendsPlusFragment$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/trends/TrendsPlusFragment$a;->b(Landroid/os/Bundle;)V

    .line 216
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 217
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 267
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->onStop()V

    .line 268
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->f:Z

    .line 269
    return-void
.end method

.method protected q_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "exit"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 91
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/trends/TrendsPlusFragment;->b(J)V

    .line 93
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->t()V

    .line 94
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->q_()V

    .line 95
    return-void
.end method

.method protected r()Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->t:I

    if-ne v0, v2, :cond_1

    .line 148
    :cond_0
    :goto_0
    return v3

    .line 138
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->e:Z

    if-eqz v0, :cond_2

    .line 139
    iput-boolean v3, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->e:Z

    move v3, v2

    .line 140
    goto :goto_0

    .line 143
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->u:Z

    if-eqz v0, :cond_4

    const-wide/32 v0, 0xdbba0

    .line 146
    :goto_1
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    iget-object v6, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->c:Lcom/twitter/util/a;

    iget-object v7, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->d:Ljava/lang/String;

    const-wide/16 v8, 0x0

    .line 147
    invoke-virtual {v6, v7, v8, v9}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    add-long/2addr v0, v6

    cmp-long v0, v4, v0

    if-lez v0, :cond_5

    move v1, v2

    .line 148
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/trends/TrendsPlusFragment;->f:Z

    if-eqz v0, :cond_0

    :cond_3
    move v3, v2

    goto :goto_0

    .line 143
    :cond_4
    const-wide/32 v0, 0x493e0

    goto :goto_1

    :cond_5
    move v1, v3

    .line 147
    goto :goto_2
.end method

.method public s()Lcom/twitter/android/timeline/cb;
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/twitter/android/trends/TrendsPlusFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/timeline/cb;->a(Landroid/os/Bundle;)Lcom/twitter/android/timeline/cb;

    move-result-object v0

    return-object v0
.end method
