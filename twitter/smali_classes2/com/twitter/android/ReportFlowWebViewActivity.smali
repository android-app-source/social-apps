.class public Lcom/twitter/android/ReportFlowWebViewActivity;
.super Lcom/twitter/android/client/TwitterWebViewActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/ReportFlowWebViewActivity$a;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterWebViewActivity;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity;->a:Z

    return-void
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 260
    invoke-virtual {p0, p1}, Lcom/twitter/android/ReportFlowWebViewActivity;->setResult(I)V

    .line 261
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->finish()V

    .line 262
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 266
    new-instance v1, Laut;

    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 267
    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/library/provider/t;->a(JLaut;)I

    .line 268
    invoke-virtual {v1}, Laut;->a()V

    .line 269
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 229
    invoke-direct {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->l()Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a()J

    move-result-wide v4

    .line 231
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->j()Lcgi;

    move-result-object v6

    .line 232
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 253
    :goto_1
    return-void

    .line 232
    :sswitch_0
    const-string/jumbo v1, "unfollow"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "mute"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v7

    goto :goto_0

    :sswitch_2
    const-string/jumbo v1, "block"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v8

    goto :goto_0

    .line 234
    :pswitch_0
    new-instance v1, Lbhs;

    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ReportFlowWebViewActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto :goto_1

    .line 239
    :pswitch_1
    new-instance v0, Lbfa;

    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbfa;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 240
    invoke-virtual {v0, v4, v5}, Lbfa;->a(J)Lbeq;

    move-result-object v0

    .line 239
    invoke-virtual {p0, v0, v7}, Lcom/twitter/android/ReportFlowWebViewActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto :goto_1

    .line 244
    :pswitch_2
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    invoke-virtual {p0, v1, v8}, Lcom/twitter/android/ReportFlowWebViewActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto :goto_1

    .line 232
    nop

    :sswitch_data_0
    .sparse-switch
        -0x16cbcc76 -> :sswitch_0
        0x335219 -> :sswitch_1
        0x597c48d -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private l()Lcom/twitter/android/ReportFlowWebViewActivity$a;
    .locals 1

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Landroid/content/Intent;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterWebViewActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 91
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 93
    return-object v0
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 175
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0a0c1d

    invoke-virtual {p0, v6}, Lcom/twitter/android/ReportFlowWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 176
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040343

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 178
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v6

    .line 179
    if-eqz v6, :cond_0

    .line 180
    new-instance v7, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v8, 0x5

    invoke-direct {v7, v8}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(I)V

    invoke-virtual {v6, v5, v7}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    .line 182
    :cond_0
    const v6, 0x7f130164

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/ReportFlowWebViewActivity$1;

    invoke-direct {v6, p0}, Lcom/twitter/android/ReportFlowWebViewActivity$1;-><init>(Lcom/twitter/android/ReportFlowWebViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iput-boolean v2, p0, Lcom/twitter/android/ReportFlowWebViewActivity;->a:Z

    .line 190
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 191
    const-string/jumbo v6, "action"

    invoke-virtual {v5, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 192
    if-eqz v6, :cond_4

    .line 193
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Landroid/content/Intent;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->i()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 194
    invoke-direct {p0, v6}, Lcom/twitter/android/ReportFlowWebViewActivity;->b(Ljava/lang/String;)V

    .line 196
    :cond_1
    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 214
    invoke-direct {p0, v1}, Lcom/twitter/android/ReportFlowWebViewActivity;->a(I)V

    .line 226
    :cond_3
    :goto_1
    :pswitch_0
    return-void

    .line 196
    :sswitch_0
    const-string/jumbo v5, "unfollow"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "mute"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string/jumbo v0, "block"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "error"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v4

    goto :goto_0

    .line 198
    :pswitch_1
    invoke-direct {p0, v2}, Lcom/twitter/android/ReportFlowWebViewActivity;->a(I)V

    goto :goto_1

    .line 202
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/twitter/android/ReportFlowWebViewActivity;->a(I)V

    goto :goto_1

    .line 206
    :pswitch_3
    invoke-direct {p0, v4}, Lcom/twitter/android/ReportFlowWebViewActivity;->a(I)V

    goto :goto_1

    .line 218
    :cond_4
    iget-wide v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 219
    const-string/jumbo v0, "report_type"

    invoke-virtual {v5, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    const-string/jumbo v1, "annoying"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "spam"

    .line 221
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 222
    :cond_5
    iget-wide v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity;->b:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ReportFlowWebViewActivity;->a(J)V

    goto :goto_1

    .line 196
    nop

    :sswitch_data_0
    .sparse-switch
        -0x16cbcc76 -> :sswitch_0
        0x335219 -> :sswitch_1
        0x597c48d -> :sswitch_2
        0x5c4d208 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/webkit/WebView;Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 155
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0c1f

    invoke-virtual {p0, v2}, Lcom/twitter/android/ReportFlowWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    invoke-static {p0, p2}, Lcom/twitter/android/BouncerWebViewActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    invoke-static {p0, p2}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 164
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 14

    .prologue
    .line 98
    invoke-super/range {p0 .. p2}, Lcom/twitter/android/client/TwitterWebViewActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 100
    const v0, 0x7f0a076a

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportFlowWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportFlowWebViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 102
    invoke-direct {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->l()Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v0

    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->c()J

    move-result-wide v4

    .line 106
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->b()J

    move-result-wide v6

    .line 107
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->e()Ljava/lang/String;

    move-result-object v3

    .line 108
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->g()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    .line 109
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->h()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    .line 110
    invoke-virtual {v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->d()Ljava/lang/String;

    move-result-object v0

    .line 112
    const v10, 0x7f0a0c1e

    invoke-virtual {p0, v10}, Lcom/twitter/android/ReportFlowWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 113
    invoke-virtual {v10}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v10

    .line 114
    const-string/jumbo v11, "source"

    invoke-virtual {v10, v11, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 115
    const-string/jumbo v3, "reporter_user_id"

    invoke-virtual {v10, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 116
    const-wide/16 v12, 0x0

    cmp-long v1, v4, v12

    if-eqz v1, :cond_0

    .line 117
    const-string/jumbo v1, "reported_tweet_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 119
    :cond_0
    const-string/jumbo v1, "reported_user_id"

    invoke-virtual {v10, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 120
    const-string/jumbo v1, "is_media"

    invoke-virtual {v10, v1, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 121
    const-string/jumbo v1, "is_promoted"

    invoke-virtual {v10, v1, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 122
    const-wide/16 v2, 0x0

    cmp-long v1, v6, v2

    if-eqz v1, :cond_1

    .line 123
    const-string/jumbo v1, "reported_moment_id"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 126
    :cond_1
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 127
    const-string/jumbo v1, "client_location"

    invoke-virtual {v10, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 130
    :cond_2
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportFlowWebViewActivity;->a(Ljava/lang/String;)V

    .line 132
    iput-wide v4, p0, Lcom/twitter/android/ReportFlowWebViewActivity;->b:J

    .line 133
    return-void
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->j()V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/TwitterWebViewActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/twitter/android/ReportFlowWebViewActivity;->a:Z

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/twitter/android/ReportFlowWebViewActivity;->finish()V

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/TwitterWebViewActivity;->p()V

    goto :goto_0
.end method
