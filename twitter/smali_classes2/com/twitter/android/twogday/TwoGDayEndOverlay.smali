.class public Lcom/twitter/android/twogday/TwoGDayEndOverlay;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method private static a(J)V
    .locals 4

    .prologue
    .line 51
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app:next_billion_day:::end"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 52
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 53
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;J)V
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/android/twogday/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/twogday/a$a;-><init>(I)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    .line 24
    invoke-virtual {v0, v1}, Lcom/twitter/android/twogday/a$a;->a(Landroid/widget/ImageView$ScaleType;)Lcom/twitter/android/dialog/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v1, "Your connection speed is back to its normal speedy self!"

    .line 25
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v1, "Vroom!"

    .line 26
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->c(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 27
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/twogday/TwoGDayEndOverlay;

    .line 29
    invoke-virtual {v0, p0}, Lcom/twitter/android/twogday/TwoGDayEndOverlay;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 30
    invoke-static {p1, p2}, Lcom/twitter/android/twogday/TwoGDayEndOverlay;->a(J)V

    .line 31
    return-void
.end method

.method private static m()V
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcom/twitter/android/twogday/b;->c(Lcqs;)V

    .line 48
    return-void
.end method


# virtual methods
.method protected final i()V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->i()V

    .line 42
    invoke-static {}, Lcom/twitter/android/twogday/TwoGDayEndOverlay;->m()V

    .line 43
    return-void
.end method

.method protected k()V
    .locals 0

    .prologue
    .line 35
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->k()V

    .line 36
    invoke-static {}, Lcom/twitter/android/twogday/TwoGDayEndOverlay;->m()V

    .line 37
    return-void
.end method
