.class public Lcom/twitter/android/twogday/TwoGDayStartOverlay;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method private static a(J)V
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app:next_billion_day:::start"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 67
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 68
    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 61
    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->e()V

    .line 62
    invoke-static {p0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/g;->g()V

    .line 63
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;J)V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/android/twogday/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/twogday/c$a;-><init>(I)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    .line 29
    invoke-virtual {v0, v1}, Lcom/twitter/android/twogday/c$a;->a(Landroid/widget/ImageView$ScaleType;)Lcom/twitter/android/dialog/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v1, "For 2G Day, bandwidth will be limited to 2G speeds. \nLearn more at go/nbd"

    .line 30
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v1, "OK"

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->c(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 32
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/twogday/TwoGDayStartOverlay;

    .line 33
    invoke-virtual {v0, p0}, Lcom/twitter/android/twogday/TwoGDayStartOverlay;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 34
    invoke-static {p1, p2}, Lcom/twitter/android/twogday/TwoGDayStartOverlay;->a(J)V

    .line 35
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/twogday/TwoGDayStartOverlay;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 51
    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->b()Lcqs;

    move-result-object v1

    .line 53
    invoke-static {v1}, Lcom/twitter/android/twogday/b;->b(Lcqs;)V

    .line 55
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/twogday/TwoGDayStartOverlay;->a(Landroid/content/Context;)V

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method protected final i()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->i()V

    .line 46
    invoke-direct {p0}, Lcom/twitter/android/twogday/TwoGDayStartOverlay;->m()V

    .line 47
    return-void
.end method

.method protected k()V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->k()V

    .line 40
    invoke-direct {p0}, Lcom/twitter/android/twogday/TwoGDayStartOverlay;->m()V

    .line 41
    return-void
.end method
