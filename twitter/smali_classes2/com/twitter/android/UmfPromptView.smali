.class public Lcom/twitter/android/UmfPromptView;
.super Lcom/twitter/ui/widget/PromptView;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/UmfPromptView$a;,
        Lcom/twitter/android/UmfPromptView$SavedState;
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/model/timeline/n;

.field private final d:Lcom/twitter/android/UmfPromptView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UmfPromptView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/UmfPromptView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/ui/widget/PromptView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    new-instance v0, Lcom/twitter/android/UmfPromptView$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/UmfPromptView$a;-><init>(Lcom/twitter/android/UmfPromptView;Lcom/twitter/android/UmfPromptView$1;)V

    iput-object v0, p0, Lcom/twitter/android/UmfPromptView;->d:Lcom/twitter/android/UmfPromptView$a;

    .line 63
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0, p0}, Lcom/twitter/android/UmfPromptView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getPromptSubtitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getPromptHeader()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 68
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/model/core/v;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 145
    const v1, 0x7f1100c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 147
    if-eqz p2, :cond_0

    .line 148
    invoke-static {p1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v1

    .line 149
    invoke-virtual {v1, p2}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/UmfPromptView;->d:Lcom/twitter/android/UmfPromptView$a;

    .line 150
    invoke-virtual {v1, v2}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v1

    .line 151
    invoke-virtual {v1, v0}, Lcnf;->b(I)Lcnf;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object p1

    .line 154
    :cond_0
    return-object p1
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 215
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 216
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    .line 217
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/n;

    iget v0, v0, Lcom/twitter/model/timeline/n;->c:I

    .line 216
    invoke-static {v2, v3, p1, v0}, Lcom/twitter/library/api/p;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;II)Lcom/twitter/library/api/p;

    move-result-object v0

    .line 215
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 218
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 160
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iget-object v0, v0, Lcom/twitter/model/timeline/n;->g:Ljava/lang/String;

    .line 162
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 164
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    .line 163
    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Z)V

    .line 167
    :cond_0
    invoke-direct {p0, v9}, Lcom/twitter/android/UmfPromptView;->a(I)V

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->f()V

    .line 170
    invoke-super {p0}, Lcom/twitter/ui/widget/PromptView;->a()V

    .line 171
    return-void
.end method

.method public a(Lcom/twitter/model/timeline/n;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UmfPromptView;->b:Z

    .line 106
    iput-object p1, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    .line 107
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iget-object v0, v0, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iget-object v1, v1, Lcom/twitter/model/timeline/n;->m:Lcom/twitter/model/core/v;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UmfPromptView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UmfPromptView;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iget-object v0, v0, Lcom/twitter/model/timeline/n;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iget-object v1, v1, Lcom/twitter/model/timeline/n;->l:Lcom/twitter/model/core/v;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UmfPromptView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UmfPromptView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iget-object v0, v0, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/UmfPromptView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getIconView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    .line 112
    iget-object v1, p1, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    sget-object v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->c:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 114
    iget-object v1, p1, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 115
    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 116
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getPromptHeader()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    :goto_0
    invoke-virtual {p0, v2}, Lcom/twitter/android/UmfPromptView;->setVisibility(I)V

    .line 124
    return-void

    .line 118
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 119
    invoke-virtual {v0, v3}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 120
    invoke-virtual {p0}, Lcom/twitter/android/UmfPromptView;->getPromptHeader()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    if-eqz v0, :cond_0

    .line 176
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/UmfPromptView;->a(I)V

    .line 177
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    invoke-virtual {v0}, Lcom/twitter/model/timeline/n;->h()V

    .line 179
    :cond_0
    invoke-super {p0}, Lcom/twitter/ui/widget/PromptView;->b()V

    .line 180
    return-void
.end method

.method public d()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 90
    iget-object v1, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    if-nez v1, :cond_1

    .line 91
    const/4 v0, 0x1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/UmfPromptView;->c:Landroid/text/format/Time;

    if-eqz v1, :cond_0

    .line 96
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 97
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/twitter/android/UmfPromptView;->c:Landroid/text/format/Time;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 98
    iget v3, v2, Landroid/text/format/Time;->second:I

    iget-object v4, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iget v4, v4, Lcom/twitter/model/timeline/n;->j:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/text/format/Time;->second:I

    .line 99
    invoke-virtual {v2, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 100
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 101
    invoke-virtual {v1, v2}, Landroid/text/format/Time;->after(Landroid/text/format/Time;)Z

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Lcom/twitter/ui/widget/PromptView;->e()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    .line 141
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 128
    invoke-super/range {p0 .. p5}, Lcom/twitter/ui/widget/PromptView;->onLayout(ZIIII)V

    .line 129
    iget-boolean v0, p0, Lcom/twitter/android/UmfPromptView;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    if-eqz v0, :cond_0

    .line 130
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/android/UmfPromptView;->a(I)V

    .line 131
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UmfPromptView;->c:Landroid/text/format/Time;

    .line 132
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView;->c:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/UmfPromptView;->b:Z

    .line 135
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 184
    iget-object v3, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    .line 185
    if-nez v3, :cond_0

    .line 211
    :goto_0
    return v1

    .line 188
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 189
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 193
    :try_start_0
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    .line 192
    invoke-virtual {v5, v0, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 194
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "\n\n--- User agent ---\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 198
    invoke-static {v4}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v7

    iget-object v7, v7, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\n\n--- Prompt ---\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 200
    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v8, "android.intent.action.SEND"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "text/plain"

    .line 201
    invoke-virtual {v7, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "android.intent.extra.TEXT"

    .line 202
    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string/jumbo v7, "android.intent.extra.SUBJECT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Debug: Android v"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v8, ", prompt id "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v3, Lcom/twitter/model/timeline/n;->c:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 203
    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "android.intent.extra.EMAIL"

    new-array v6, v2, [Ljava/lang/String;

    const-string/jumbo v7, "promptbird@twitter.com"

    aput-object v7, v6, v1

    .line 205
    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 206
    const/high16 v1, 0x10000

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 208
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 209
    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_1
    move v1, v2

    .line 211
    goto/16 :goto_0

    .line 195
    :catch_0
    move-exception v0

    move v0, v1

    goto/16 :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 80
    check-cast p1, Lcom/twitter/android/UmfPromptView$SavedState;

    .line 81
    invoke-virtual {p1}, Lcom/twitter/android/UmfPromptView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/twitter/ui/widget/PromptView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 82
    iget-boolean v0, p1, Lcom/twitter/android/UmfPromptView$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/twitter/android/UmfPromptView;->b:Z

    .line 83
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lcom/twitter/android/UmfPromptView$SavedState;

    invoke-super {p0}, Lcom/twitter/ui/widget/PromptView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/UmfPromptView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 73
    iget-object v1, p0, Lcom/twitter/android/UmfPromptView;->a:Lcom/twitter/model/timeline/n;

    iput-object v1, v0, Lcom/twitter/android/UmfPromptView$SavedState;->b:Lcom/twitter/model/timeline/n;

    .line 74
    iget-boolean v1, p0, Lcom/twitter/android/UmfPromptView;->b:Z

    iput-boolean v1, v0, Lcom/twitter/android/UmfPromptView$SavedState;->a:Z

    .line 75
    return-object v0
.end method
