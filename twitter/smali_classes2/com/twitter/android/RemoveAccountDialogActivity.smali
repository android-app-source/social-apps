.class public Lcom/twitter/android/RemoveAccountDialogActivity;
.super Lcom/twitter/app/common/abs/AbsFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ae$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/RemoveAccountDialogActivity$a;
    }
.end annotation


# instance fields
.field a:Z

.field b:Z

.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/android/RemoveAccountDialogActivity$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/RemoveAccountDialogActivity;)J
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    return-wide v0
.end method

.method static synthetic a(Lcom/twitter/android/RemoveAccountDialogActivity;Lcom/twitter/library/service/s;I)Z
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/RemoveAccountDialogActivity;->b(Lcom/twitter/library/service/s;I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/RemoveAccountDialogActivity;)J
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/android/RemoveAccountDialogActivity;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/RemoveAccountDialogActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "RemoveAccountDialogActivity_account_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    .line 52
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "RemoveAccountDialogActivity_account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Ljava/lang/String;

    .line 53
    new-instance v0, Lcom/twitter/android/RemoveAccountDialogActivity$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/RemoveAccountDialogActivity$a;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/android/RemoveAccountDialogActivity$a;

    .line 54
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 82
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 83
    iget-boolean v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->a:Z

    if-nez v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->removeDialog(I)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->a:Z

    .line 88
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->b()V

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->showDialog(I)V

    goto :goto_0
.end method

.method b()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 227
    iput-boolean v6, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->b:Z

    .line 228
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 229
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 230
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v4, v6, [Ljava/lang/String;

    const-string/jumbo v5, "settings::::logout"

    aput-object v5, v4, v7

    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 231
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->b()I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 232
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "settings::::logout_last"

    aput-object v3, v2, v7

    .line 233
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 234
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i()Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 232
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    .line 237
    iget-wide v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    invoke-static {p0, v0, v1}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iget-wide v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    invoke-static {p0, v0, v1}, Lbau;->b(Landroid/content/Context;J)Z

    .line 240
    :cond_1
    invoke-virtual {p0, v6}, Lcom/twitter/android/RemoveAccountDialogActivity;->showDialog(I)V

    .line 241
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x1040009

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 98
    new-instance v1, Lcom/twitter/android/RemoveAccountDialogActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/RemoveAccountDialogActivity$1;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    .line 108
    packed-switch p1, :pswitch_data_0

    .line 185
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 110
    :pswitch_0
    new-instance v0, Lcom/twitter/android/RemoveAccountDialogActivity$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/RemoveAccountDialogActivity$2;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    .line 130
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0408

    .line 131
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string/jumbo v3, ""

    .line 132
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1040013

    .line 133
    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 134
    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 135
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 136
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 140
    :pswitch_1
    new-instance v0, Lcom/twitter/android/RemoveAccountDialogActivity$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/RemoveAccountDialogActivity$3;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    .line 149
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a031d

    .line 150
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0409

    .line 151
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0209

    .line 152
    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 155
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 156
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 158
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 159
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 160
    new-instance v2, Lcom/twitter/android/RemoveAccountDialogActivity$4;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity$4;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;Landroid/widget/Button;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/Button;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 169
    :pswitch_2
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 170
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 171
    const v1, 0x7f0a0407

    invoke-virtual {p0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 173
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto/16 :goto_0

    .line 177
    :pswitch_3
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 178
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 179
    const v1, 0x7f0a0411

    invoke-virtual {p0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 180
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 181
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto/16 :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/android/RemoveAccountDialogActivity$a;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 76
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onPause()V

    .line 77
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 192
    packed-switch p1, :pswitch_data_0

    .line 218
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 222
    :goto_0
    return-void

    .line 194
    :pswitch_0
    iget-wide v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    invoke-static {p0, v0, v1}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v1

    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->F:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 198
    invoke-static {}, Lcom/twitter/library/resilient/e;->c()Lcom/twitter/library/resilient/e;

    move-result-object v0

    .line 199
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/resilient/e;->a(J)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 201
    :goto_1
    check-cast p2, Landroid/app/AlertDialog;

    .line 204
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 205
    const v0, 0x7f0a040c

    .line 214
    :goto_2
    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 199
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 206
    :cond_1
    if-eqz v1, :cond_2

    .line 207
    const v0, 0x7f0a040b

    goto :goto_2

    .line 208
    :cond_2
    if-eqz v0, :cond_3

    .line 209
    const v0, 0x7f0a040d

    goto :goto_2

    .line 211
    :cond_3
    const v0, 0x7f0a040a

    goto :goto_2

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onResume()V

    .line 69
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->showDialog(I)V

    .line 70
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/android/RemoveAccountDialogActivity$a;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 71
    return-void
.end method

.method protected p()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method protected p_()V
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 59
    return-void
.end method
