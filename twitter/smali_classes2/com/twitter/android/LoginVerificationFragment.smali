.class public Lcom/twitter/android/LoginVerificationFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/LoginVerificationFragment$b;,
        Lcom/twitter/android/LoginVerificationFragment$e;,
        Lcom/twitter/android/LoginVerificationFragment$d;,
        Lcom/twitter/android/LoginVerificationFragment$a;,
        Lcom/twitter/android/LoginVerificationFragment$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Landroid/database/Cursor;",
        "Lcjr",
        "<",
        "Landroid/database/Cursor;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/library/client/Session;

.field private b:[Landroid/text/style/StyleSpan;

.field private c:Lcom/twitter/android/LoginVerificationFragment$b;

.field private d:Landroid/app/ProgressDialog;

.field private final e:Lcom/twitter/library/client/e;

.field private f:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    .line 68
    new-instance v0, Lcom/twitter/android/LoginVerificationFragment$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/LoginVerificationFragment$c;-><init>(Lcom/twitter/android/LoginVerificationFragment;Lcom/twitter/android/LoginVerificationFragment$1;)V

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Lcom/twitter/library/client/e;

    return-void
.end method

.method private static a([I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 187
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget v0, p0, v0

    goto :goto_0
.end method

.method private a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 195
    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lbaw;->e(Landroid/content/Context;J)Z

    move-result v0

    .line 197
    if-eqz v0, :cond_0

    .line 198
    const v0, 0x7f0a04e4

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(I)V

    .line 204
    :goto_0
    return-void

    .line 200
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SecuritySettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "SecuritySettingsActivity_account_name"

    .line 201
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 200
    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/LoginVerificationFragment;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/twitter/android/LoginVerificationFragment;->f()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/LoginVerificationFragment;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/LoginVerificationFragment;->a(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/LoginVerificationFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/LoginVerificationFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/LoginVerificationFragment;)J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a_:J

    return-wide v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->T:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 192
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/LoginVerificationFragment$b;

    if-ne v0, v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    .line 142
    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/LoginVerificationRequest;

    .line 143
    if-eqz p1, :cond_3

    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/twitter/model/account/LoginVerificationRequest;->b:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 144
    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/LoginVerificationFragment$b;

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 140
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private b([I)V
    .locals 2

    .prologue
    .line 207
    invoke-static {p1}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v0

    .line 208
    packed-switch v0, :pswitch_data_0

    .line 224
    const v0, 0x7f0a04e4

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(I)V

    .line 228
    :goto_0
    return-void

    .line 210
    :pswitch_0
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a04f0

    .line 211
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a04ef

    .line 212
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x104000a

    .line 213
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->e(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 214
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 215
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 220
    :pswitch_1
    const v0, 0x7f0a04f3

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(I)V

    goto :goto_0

    .line 208
    :pswitch_data_0
    .packed-switch 0xeb
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/android/LoginVerificationFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/LoginVerificationFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/LoginVerificationFragment;)J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a_:J

    return-wide v0
.end method

.method static synthetic e(Lcom/twitter/android/LoginVerificationFragment;)J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a_:J

    return-wide v0
.end method

.method static synthetic f(Lcom/twitter/android/LoginVerificationFragment;)J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a_:J

    return-wide v0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 158
    new-instance v0, Lban;

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Lban;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v4, v1}, Lcom/twitter/android/LoginVerificationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 160
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login_verification::::get_newer"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 161
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/LoginVerificationFragment;)[Landroid/text/style/StyleSpan;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->b:[Landroid/text/style/StyleSpan;

    return-object v0
.end method


# virtual methods
.method protected H_()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/twitter/android/LoginVerificationFragment;->f()V

    .line 166
    return-void
.end method

.method a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 434
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 435
    const v1, 0x7f04001b

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 436
    const v0, 0x7f130142

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 437
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object v0, v1

    .line 439
    check-cast v0, Lcom/twitter/ui/user/UserView;

    .line 440
    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/ui/user/UserView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 441
    invoke-virtual {v0, v3}, Lcom/twitter/ui/user/UserView;->setVerified(Z)V

    .line 442
    invoke-virtual {v0, v3}, Lcom/twitter/ui/user/UserView;->setProtected(Z)V

    .line 443
    return-object v1
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 9
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/16 v5, 0x58

    const/16 v4, 0xc8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 242
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 243
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 244
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    iget v1, v0, Lcom/twitter/network/l;->a:I

    .line 245
    packed-switch p2, :pswitch_data_0

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 248
    :pswitch_0
    if-ne v1, v4, :cond_2

    .line 249
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "login_verification::get_requests::success"

    aput-object v3, v2, v7

    .line 250
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object v6, v0

    .line 255
    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 257
    check-cast p1, Lban;

    .line 259
    invoke-virtual {p1}, Lban;->b()Ljava/util/List;

    move-result-object v0

    .line 260
    if-ne v1, v4, :cond_3

    if-eqz v0, :cond_3

    .line 261
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 262
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v6, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    .line 263
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v5

    .line 264
    sget-object v1, Lcom/twitter/model/account/LoginVerificationRequest;->a:Lcom/twitter/model/account/LoginVerificationRequest;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 266
    new-instance v0, Lcom/twitter/android/LoginVerificationFragment$e;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->T:Landroid/content/Context;

    const v3, 0x7f04018e

    const v4, 0x7f130471

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/LoginVerificationFragment$e;-><init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    .line 286
    :cond_1
    :goto_2
    invoke-static {v6}, Lcpm;->a(Lcpk;)V

    .line 287
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 252
    :cond_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "login_verification::get_requests::failure"

    aput-object v3, v2, v7

    .line 253
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object v6, v0

    goto :goto_1

    .line 274
    :cond_3
    const-wide/16 v2, 0x0

    invoke-virtual {v6, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    .line 275
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/LoginVerificationFragment$b;

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->f:Landroid/widget/ArrayAdapter;

    .line 276
    if-eq v1, v4, :cond_1

    .line 277
    invoke-virtual {p1}, Lban;->e()[I

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v0

    .line 278
    if-ne v0, v5, :cond_4

    .line 279
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "login_verification::get_requests::rate_limit"

    aput-object v3, v2, v7

    .line 280
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 279
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 282
    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 283
    const v0, 0x7f0a04e4

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(I)V

    goto :goto_2

    .line 293
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->e()V

    .line 294
    if-ne v1, v4, :cond_5

    .line 295
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "login_verification::request:reject:success"

    aput-object v2, v1, v7

    .line 296
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 295
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 297
    const v0, 0x7f0a04f4

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(I)V

    .line 298
    check-cast p1, Lbaz;

    iget-object v0, p1, Lbaz;->a:Lbav;

    iget-object v0, v0, Lbav;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 300
    :cond_5
    check-cast p1, Lbaz;

    invoke-virtual {p1}, Lbaz;->b()[I

    move-result-object v0

    .line 301
    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b([I)V

    .line 303
    invoke-static {v0}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v2

    .line 304
    if-ne v2, v5, :cond_6

    .line 305
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::request:reject:rate_limit"

    aput-object v4, v3, v7

    .line 306
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 305
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 309
    :cond_6
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::request:reject:failure"

    aput-object v4, v3, v7

    .line 310
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 311
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 312
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 309
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 317
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->e()V

    .line 318
    if-ne v1, v4, :cond_7

    .line 319
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "login_verification::request:accept:success"

    aput-object v2, v1, v7

    .line 320
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 319
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 321
    const v0, 0x7f0a04f2

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(I)V

    .line 322
    check-cast p1, Lbae;

    iget-object v0, p1, Lbae;->a:Lbav;

    iget-object v0, v0, Lbav;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 324
    :cond_7
    check-cast p1, Lbae;

    invoke-virtual {p1}, Lbae;->b()[I

    move-result-object v0

    .line 325
    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b([I)V

    .line 327
    invoke-static {v0}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v2

    .line 328
    if-ne v2, v5, :cond_8

    .line 329
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::request:accept:rate_limit"

    aput-object v4, v3, v7

    .line 330
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 329
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 333
    :cond_8
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::request:accept:failure"

    aput-object v4, v3, v7

    .line 334
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 335
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 336
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 333
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 169
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    .line 172
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 175
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 176
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 178
    :cond_0
    return-void
.end method

.method protected aP_()V
    .locals 4

    .prologue
    .line 116
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aP_()V

    .line 117
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    .line 118
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login_verification::::impression"

    aput-object v3, v1, v2

    .line 119
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 120
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ".twitter."

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    const-string/jumbo v1, "settings"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 127
    :goto_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 128
    return-void

    .line 125
    :cond_0
    const-string/jumbo v1, "push"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->b()V

    .line 109
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Lcom/twitter/library/client/e;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(Lcom/twitter/library/client/e;)V

    .line 110
    invoke-direct {p0}, Lcom/twitter/android/LoginVerificationFragment;->f()V

    .line 111
    return-void
.end method

.method e()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 184
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v3, 0x7f0a04fb

    .line 94
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 96
    new-instance v5, Ljava/util/ArrayList;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 97
    invoke-virtual {p0, v3}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 98
    invoke-virtual {p0, v3}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 97
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 99
    new-instance v0, Lcom/twitter/android/LoginVerificationFragment$b;

    .line 100
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f04018d

    const v4, 0x7f130471

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/LoginVerificationFragment$b;-><init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/LoginVerificationFragment$b;

    .line 104
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 74
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0, v3}, Lcom/twitter/android/LoginVerificationFragment;->setRetainInstance(Z)V

    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "lv_account_name"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    .line 85
    :goto_0
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/text/style/StyleSpan;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v2, v0, v1

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v1, v0, v3

    const/4 v1, 0x2

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->b:[Landroid/text/style/StyleSpan;

    .line 90
    return-void

    .line 82
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    goto :goto_0
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Lcom/twitter/library/client/e;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->b(Lcom/twitter/library/client/e;)V

    .line 133
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->q_()V

    .line 134
    return-void
.end method
