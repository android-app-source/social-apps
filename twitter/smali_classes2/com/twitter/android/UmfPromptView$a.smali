.class Lcom/twitter/android/UmfPromptView$a;
.super Lcnc;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/UmfPromptView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/UmfPromptView;

.field private final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/twitter/android/UmfPromptView;)V
    .locals 1

    .prologue
    .line 254
    iput-object p1, p0, Lcom/twitter/android/UmfPromptView$a;->a:Lcom/twitter/android/UmfPromptView;

    invoke-direct {p0}, Lcnc;-><init>()V

    .line 255
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView$a;->a:Lcom/twitter/android/UmfPromptView;

    invoke-virtual {v0}, Lcom/twitter/android/UmfPromptView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/UmfPromptView;Lcom/twitter/android/UmfPromptView$1;)V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/twitter/android/UmfPromptView$a;-><init>(Lcom/twitter/android/UmfPromptView;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/ad;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 275
    iget-object v1, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    .line 276
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v3, p1

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    .line 275
    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method public a(Lcom/twitter/model/core/b;)V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/b;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 265
    return-void
.end method

.method public a(Lcom/twitter/model/core/h;)V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/h;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 260
    return-void
.end method

.method public a(Lcom/twitter/model/core/q;)V
    .locals 4

    .prologue
    .line 269
    iget-object v0, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/UmfPromptView$a;->b:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    iget-object v3, p1, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    .line 270
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 269
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 271
    return-void
.end method
