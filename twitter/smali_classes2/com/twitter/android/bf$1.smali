.class Lcom/twitter/android/bf$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/bf;->a(Lcom/twitter/model/core/Tweet;ZLcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/ClientEventLog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/Session;

.field final synthetic b:Lcom/twitter/model/core/Tweet;

.field final synthetic c:Z

.field final synthetic d:Lcom/twitter/analytics/feature/model/ClientEventLog;

.field final synthetic e:Lcom/twitter/android/bf;


# direct methods
.method constructor <init>(Lcom/twitter/android/bf;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;ZLcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/android/bf$1;->e:Lcom/twitter/android/bf;

    iput-object p2, p0, Lcom/twitter/android/bf$1;->a:Lcom/twitter/library/client/Session;

    iput-object p3, p0, Lcom/twitter/android/bf$1;->b:Lcom/twitter/model/core/Tweet;

    iput-boolean p4, p0, Lcom/twitter/android/bf$1;->c:Z

    iput-object p5, p0, Lcom/twitter/android/bf$1;->d:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    .prologue
    .line 54
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/bf$1;->e:Lcom/twitter/android/bf;

    invoke-static {v0}, Lcom/twitter/android/bf;->c(Lcom/twitter/android/bf;)Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbhc;

    iget-object v2, p0, Lcom/twitter/android/bf$1;->e:Lcom/twitter/android/bf;

    .line 56
    invoke-static {v2}, Lcom/twitter/android/bf;->a(Lcom/twitter/android/bf;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/bf$1;->a:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/bf$1;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v4, Lcom/twitter/model/core/Tweet;->G:J

    iget-boolean v6, p0, Lcom/twitter/android/bf$1;->c:Z

    invoke-direct/range {v1 .. v6}, Lbhc;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V

    new-instance v2, Lcom/twitter/android/bf$1$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/bf$1$1;-><init>(Lcom/twitter/android/bf$1;)V

    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/twitter/android/bf$1;->d:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 68
    :cond_0
    return-void
.end method
