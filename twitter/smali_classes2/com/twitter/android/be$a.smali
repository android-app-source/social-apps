.class public Lcom/twitter/android/be$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/MediaListFragment$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/be;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:J

.field public final b:Lcom/twitter/model/core/Tweet;

.field public final c:F

.field public final d:Z

.field public final e:Lcom/twitter/model/core/MediaEntity;

.field public f:Lcax;


# direct methods
.method constructor <init>(JLcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;Lcax;)V
    .locals 7

    .prologue
    .line 448
    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v6

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/be$a;-><init>(JLcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;Lcom/twitter/model/core/MediaEntity;)V

    .line 449
    iput-object p5, p0, Lcom/twitter/android/be$a;->f:Lcax;

    .line 450
    return-void
.end method

.method constructor <init>(JLcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;Lcom/twitter/model/core/MediaEntity;)V
    .locals 1

    .prologue
    .line 439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440
    iput-wide p1, p0, Lcom/twitter/android/be$a;->a:J

    .line 441
    iput-object p3, p0, Lcom/twitter/android/be$a;->b:Lcom/twitter/model/core/Tweet;

    .line 442
    invoke-virtual {p4}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    iput v0, p0, Lcom/twitter/android/be$a;->c:F

    .line 443
    invoke-static {p3}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/be$a;->d:Z

    .line 444
    iput-object p5, p0, Lcom/twitter/android/be$a;->e:Lcom/twitter/model/core/MediaEntity;

    .line 445
    return-void

    .line 443
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/twitter/android/be$a;->f:Lcax;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/twitter/android/be$a;->f:Lcax;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcax;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 459
    :goto_0
    return-object v0

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/be$a;->e:Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/twitter/android/be$a;->e:Lcom/twitter/model/core/MediaEntity;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    goto :goto_0

    .line 459
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 465
    iget-wide v0, p0, Lcom/twitter/android/be$a;->a:J

    return-wide v0
.end method

.method public d()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/twitter/android/be$a;->b:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public e()Lcom/twitter/model/core/MediaEntity;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/twitter/android/be$a;->e:Lcom/twitter/model/core/MediaEntity;

    return-object v0
.end method
