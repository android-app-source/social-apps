.class Lcom/twitter/android/bt$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/bt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/bt;

.field private b:Lcgi;

.field private c:I


# direct methods
.method constructor <init>(Lcom/twitter/android/bt;)V
    .locals 1

    .prologue
    .line 907
    iput-object p1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/bt$b;->b:Lcgi;

    .line 905
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/bt$b;->c:I

    .line 908
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 915
    iput p1, p0, Lcom/twitter/android/bt$b;->c:I

    .line 916
    return-void
.end method

.method public a(Lcgi;)V
    .locals 0

    .prologue
    .line 911
    iput-object p1, p0, Lcom/twitter/android/bt$b;->b:Lcgi;

    .line 912
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 903
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/bt$b;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 8

    .prologue
    .line 924
    iget-object v0, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v0}, Lcom/twitter/android/bt;->b(Lcom/twitter/android/bt;)I

    move-result v0

    .line 925
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 927
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->c(Lcom/twitter/android/bt;)Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 928
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v7

    .line 929
    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->d(Lcom/twitter/android/bt;)Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 931
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-nez v1, :cond_6

    .line 932
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 933
    new-instance v1, Lbhs;

    iget-object v6, p0, Lcom/twitter/android/bt$b;->b:Lcgi;

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v7, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 935
    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->e(Lcom/twitter/android/bt;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 936
    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->f(Lcom/twitter/android/bt;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 937
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "search:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->g(Lcom/twitter/android/bt;)I

    move-result v1

    invoke-static {v1}, Lcom/twitter/model/topic/TwitterTopic;->d(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":people_pivot:user:unfollow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 957
    :goto_1
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 958
    iget-object v2, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v2}, Lcom/twitter/android/bt;->c(Lcom/twitter/android/bt;)Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 959
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v4

    const/4 v5, 0x0

    iget v6, p0, Lcom/twitter/android/bt$b;->c:I

    move-wide v2, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;I)V

    .line 961
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    .line 962
    invoke-static {v1}, Lcom/twitter/android/bt;->i(Lcom/twitter/android/bt;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    .line 963
    invoke-static {v1}, Lcom/twitter/android/bt;->h(Lcom/twitter/android/bt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 961
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 970
    :goto_2
    return-void

    .line 925
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 939
    :cond_1
    if-eqz v0, :cond_2

    .line 940
    const-string/jumbo v0, "search:people:users:user:unfollow"

    goto :goto_1

    .line 942
    :cond_2
    const-string/jumbo v0, "search:universal::user:unfollow"

    goto :goto_1

    .line 945
    :cond_3
    new-instance v1, Lbhq;

    iget-object v6, p0, Lcom/twitter/android/bt$b;->b:Lcgi;

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v7, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 947
    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->e(Lcom/twitter/android/bt;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 948
    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->f(Lcom/twitter/android/bt;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 949
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "search:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v1}, Lcom/twitter/android/bt;->g(Lcom/twitter/android/bt;)I

    move-result v1

    invoke-static {v1}, Lcom/twitter/model/topic/TwitterTopic;->d(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":people_pivot:user:follow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 951
    :cond_4
    if-eqz v0, :cond_5

    .line 952
    const-string/jumbo v0, "search:people:users:user:follow"

    goto/16 :goto_1

    .line 954
    :cond_5
    const-string/jumbo v0, "search:universal::user:follow"

    goto/16 :goto_1

    .line 965
    :cond_6
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->i()V

    .line 966
    iget-object v0, p0, Lcom/twitter/android/bt$b;->a:Lcom/twitter/android/bt;

    invoke-static {v0}, Lcom/twitter/android/bt;->c(Lcom/twitter/android/bt;)Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    const/4 v1, 0x4

    .line 968
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getBestName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 966
    invoke-static {v0, v1, v2}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_2
.end method
