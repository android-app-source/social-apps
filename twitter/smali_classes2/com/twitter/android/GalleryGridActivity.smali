.class public Lcom/twitter/android/GalleryGridActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lbrp;
.implements Lcom/twitter/android/media/selection/a;
.implements Lcom/twitter/android/widget/GalleryGridFragment$a;
.implements Lcom/twitter/android/widget/m$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/GalleryGridActivity$b;,
        Lcom/twitter/android/GalleryGridActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/media/selection/c;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GalleryGridActivity;)Lcom/twitter/android/media/selection/c;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->a:Lcom/twitter/android/media/selection/c;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 84
    const v0, 0x7f040104

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 85
    return-object p2
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 95
    invoke-static {p0, p1}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->a:Lcom/twitter/android/media/selection/c;

    iget-boolean v1, p0, Lcom/twitter/android/GalleryGridActivity;->c:Z

    iget v2, p0, Lcom/twitter/android/GalleryGridActivity;->d:I

    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/android/media/selection/c;->a(ZII)V

    .line 101
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->b:Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryGridActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 110
    invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 111
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->c()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 132
    if-nez v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "media_attachment"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/GalleryGridActivity;->setResult(ILandroid/content/Intent;)V

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/GalleryGridActivity;->finish()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->a:Lcom/twitter/android/media/selection/c;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;)V

    .line 106
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    return v0
.end method

.method public aL_()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->a:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->b()V

    .line 91
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 12

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/twitter/android/GalleryGridActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 58
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    move v11, v0

    .line 59
    :goto_0
    const-string/jumbo v0, "permissions_event_prefix"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->b:Ljava/lang/String;

    .line 60
    const-string/jumbo v0, "is_video_allowed"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/GalleryGridActivity;->c:Z

    .line 61
    const-string/jumbo v0, "camera_initiator"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 62
    iput v0, p0, Lcom/twitter/android/GalleryGridActivity;->d:I

    .line 64
    new-instance v0, Lcom/twitter/android/media/selection/c;

    const-string/jumbo v2, "scribe_section"

    .line 67
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-boolean v1, p0, Lcom/twitter/android/GalleryGridActivity;->c:Z

    if-eqz v1, :cond_1

    sget-object v4, Lcom/twitter/media/model/MediaType;->h:Ljava/util/EnumSet;

    :goto_1
    const/4 v5, 0x1

    sget-object v6, Lcom/twitter/android/composer/ComposerType;->d:Lcom/twitter/android/composer/ComposerType;

    .line 71
    invoke-virtual {p0}, Lcom/twitter/android/GalleryGridActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v7

    move-object v1, p0

    move-object v2, p0

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/j;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->a:Lcom/twitter/android/media/selection/c;

    .line 74
    new-instance v0, Lcom/twitter/android/widget/o;

    iget-object v3, p0, Lcom/twitter/android/GalleryGridActivity;->a:Lcom/twitter/android/media/selection/c;

    const v4, 0x7f13039f

    new-instance v7, Lcom/twitter/android/GalleryGridActivity$a;

    invoke-direct {v7, p0}, Lcom/twitter/android/GalleryGridActivity$a;-><init>(Lcom/twitter/android/GalleryGridActivity;)V

    iget-boolean v8, p0, Lcom/twitter/android/GalleryGridActivity;->c:Z

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v1, p0

    move v2, v11

    move-object v5, p0

    move-object v6, p0

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/widget/o;-><init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;ZIZ)V

    .line 78
    const v1, 0x7f13039e

    invoke-virtual {p0, v1}, Lcom/twitter/android/GalleryGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/n;->a(Landroid/view/ViewGroup;)V

    .line 79
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    move v11, v0

    goto :goto_0

    .line 67
    :cond_1
    sget-object v4, Lcom/twitter/media/model/MediaType;->g:Ljava/util/EnumSet;

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity;->a:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/twitter/android/media/selection/c;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 118
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-nez p1, :cond_0

    .line 119
    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-static {p3}, Lcom/twitter/android/media/camera/e;->a(Landroid/content/Intent;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryGridActivity;->a(I)V

    .line 122
    :cond_0
    return-void
.end method
