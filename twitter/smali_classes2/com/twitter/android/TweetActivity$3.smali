.class Lcom/twitter/android/TweetActivity$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Latg$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/TweetActivity;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetActivity;

.field private b:J


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 2

    .prologue
    .line 432
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/TweetActivity$3;->b:J

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 436
    invoke-static {}, Lcom/twitter/util/aa;->e()J

    move-result-wide v0

    .line 437
    iget-wide v2, p0, Lcom/twitter/android/TweetActivity$3;->b:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 438
    iput-wide v0, p0, Lcom/twitter/android/TweetActivity$3;->b:J

    .line 439
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lata;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lata;->b(Z)V

    .line 441
    :cond_0
    return-void
.end method

.method public a([JLjava/util/List;JJJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;JJJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 476
    const-string/jumbo v0, "tweet"

    invoke-static {p7, p8, v0}, Lcom/twitter/android/composer/p;->a(JLjava/lang/String;)V

    .line 477
    iget-object v1, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    .line 479
    invoke-virtual {v0, p7, p8}, Lcom/twitter/app/users/f;->c(J)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 480
    invoke-virtual {v0, p1}, Lcom/twitter/app/users/f;->a([J)Lcom/twitter/app/users/f;

    move-result-object v0

    const/16 v2, 0x2a

    .line 481
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 482
    invoke-virtual {v0, p3, p4}, Lcom/twitter/app/users/f;->b(J)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 483
    invoke-virtual {v0, v6}, Lcom/twitter/app/users/f;->b(Z)Lcom/twitter/app/users/f;

    move-result-object v2

    .line 484
    invoke-static {}, Lbpj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    new-array v3, v6, [Ljava/lang/Long;

    const/4 v4, 0x0

    .line 488
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/twitter/util/collection/ImmutableList;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v6, p2, v3}, Lcom/twitter/android/UsersAdapter$CheckboxConfig;-><init>(ZLjava/util/List;Ljava/util/List;)V

    .line 484
    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/app/users/f;->a(Lcom/twitter/android/UsersAdapter$CheckboxConfig;)Lcom/twitter/app/users/f;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    .line 489
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0x6b

    .line 477
    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/TweetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 491
    return-void

    .line 488
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/android/TweetActivity;)V

    .line 446
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, v1}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Lcom/twitter/model/core/Tweet;)V

    .line 451
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 456
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ":composition::add_photo:click"

    aput-object v3, v1, v2

    .line 457
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 456
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 458
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 463
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ":composition::remove_photo:click"

    aput-object v3, v1, v2

    .line 464
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 463
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 465
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 469
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$3;->a:Lcom/twitter/android/TweetActivity;

    const/16 v1, 0x68

    sget-object v2, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/util/j;->a(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V

    .line 471
    return-void
.end method
