.class public Lcom/twitter/android/cm;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lrx/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cm;->a:Ljava/util/Map;

    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 35
    const-string/jumbo v1, "livepipeline_tweetengagement_ui_android_5316"

    invoke-static {v1}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "livepipeline_tweetengagement_details_darktraffic_enabled"

    .line 36
    invoke-static {v1, v0}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 35
    :cond_1
    return v0
.end method


# virtual methods
.method public a(JLcom/twitter/library/network/livepipeline/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/twitter/library/network/livepipeline/g",
            "<",
            "Lcom/twitter/model/livepipeline/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/ImmutableList;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/ImmutableList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/cm;->a(Ljava/util/List;)V

    .line 41
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/library/network/livepipeline/g",
            "<",
            "Lcom/twitter/model/livepipeline/c;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 45
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 46
    iget-object v2, p0, Lcom/twitter/android/cm;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/cm;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrx/j;

    invoke-interface {v2}, Lrx/j;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/network/livepipeline/g;

    .line 48
    invoke-static {}, Lcom/twitter/library/network/livepipeline/b;->a()Lcom/twitter/library/network/livepipeline/b;

    move-result-object v4

    .line 49
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/twitter/library/network/livepipeline/b;->a(J)Lrx/c;

    move-result-object v1

    .line 50
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v4

    invoke-virtual {v1, v4}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v1

    .line 51
    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v1

    .line 53
    iget-object v2, p0, Lcom/twitter/android/cm;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 56
    :cond_2
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/cm;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 60
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method
