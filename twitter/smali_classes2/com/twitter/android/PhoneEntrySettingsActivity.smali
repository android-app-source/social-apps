.class public Lcom/twitter/android/PhoneEntrySettingsActivity;
.super Lcom/twitter/android/PhoneEntryBaseActivity;
.source "Twttr"


# instance fields
.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/library/client/Session;

.field private h:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryBaseActivity;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 95
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "phone_association:add_phone:device_registration:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 96
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 95
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 97
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->c:Lcom/twitter/android/PhoneEntryFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhoneEntryFragment;->a(Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 187
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "settings:phone:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 188
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "verify_phone"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Lbht;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->g:Lcom/twitter/library/client/Session;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lbht;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ZZ)V

    .line 59
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/twitter/android/PhoneEntryBaseActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 67
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 68
    return-object v0
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 108
    invoke-super {p0, p1, p2}, Lcom/twitter/android/PhoneEntryBaseActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 109
    const/4 v0, 0x3

    if-ne p2, v0, :cond_6

    .line 110
    check-cast p1, Lbht;

    .line 111
    invoke-virtual {p1}, Lbht;->T()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 112
    invoke-virtual {p1}, Lbht;->e()Lbil;

    move-result-object v0

    invoke-virtual {v0}, Lbil;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 113
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 114
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbin;

    .line 115
    invoke-virtual {v0}, Lbin;->b()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    const v0, 0x7f0a0a09

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 118
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->finish()V

    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {v0}, Lbin;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->c:Lcom/twitter/android/PhoneEntryFragment;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->a:Lcom/twitter/android/util/s;

    invoke-interface {v1}, Lcom/twitter/android/util/s;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/PhoneEntryFragment;->a(Ljava/lang/String;Z)V

    .line 126
    :cond_2
    const-string/jumbo v0, "email_phone_info::success"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->d(Ljava/lang/String;)V

    .line 169
    :cond_3
    :goto_1
    return-void

    .line 128
    :cond_4
    invoke-virtual {p1}, Lbht;->b()[I

    move-result-object v0

    .line 129
    if-eqz v0, :cond_5

    const/16 v1, 0x58

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 130
    const-string/jumbo v0, "email_phone_info::rate_limit"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 132
    :cond_5
    const-string/jumbo v0, "email_phone_info::generic"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 135
    :cond_6
    if-ne p2, v3, :cond_3

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->h()V

    .line 137
    check-cast p1, Lbci;

    .line 138
    invoke-virtual {p1}, Lbci;->s()Lcbv;

    move-result-object v0

    .line 139
    invoke-virtual {p1}, Lbci;->e()[I

    move-result-object v1

    .line 140
    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lbci;->T()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 141
    invoke-static {p0}, Lcom/twitter/android/client/u;->a(Landroid/content/Context;)Lcom/twitter/android/client/u;

    move-result-object v1

    const-string/jumbo v2, "add_phone"

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/u;->a(Ljava/lang/String;)V

    .line 142
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/PhoneVerifySettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "liveFragment"

    .line 143
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "phone"

    iget-object v0, v0, Lcbv;->a:Ljava/lang/String;

    .line 145
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    iget-object v2, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->f:Ljava/lang/String;

    .line 146
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "update_phone"

    iget-boolean v2, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->d:Z

    .line 147
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 149
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "umf_flow"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 150
    const-string/jumbo v1, "umf_flow"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 151
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PhoneEntrySettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 155
    :goto_2
    const-string/jumbo v0, "begin:success"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 153
    :cond_7
    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 156
    :cond_8
    if-eqz v1, :cond_9

    const/16 v0, 0x11d

    .line 157
    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 158
    const v0, 0x7f0a0873

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 159
    const-string/jumbo v0, "begin:registered"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 160
    :cond_9
    if-eqz v1, :cond_a

    const/16 v0, 0x12b

    invoke-static {v1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 162
    const v0, 0x7f0a0874

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 163
    const-string/jumbo v0, "begin:rate_limit"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 165
    :cond_a
    const v0, 0x7f0a0656

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 166
    const-string/jumbo v0, "begin:failure"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public b()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->a:Lcom/twitter/android/util/s;

    iget-object v1, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->a:Lcom/twitter/android/util/s;

    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/twitter/android/util/s;->b(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 79
    invoke-interface {v0, v1}, Lcom/twitter/android/util/s;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Ljava/lang/String;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    iget-object v1, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->c:Lcom/twitter/android/PhoneEntryFragment;

    invoke-virtual {v1}, Lcom/twitter/android/PhoneEntryFragment;->i()Lcom/twitter/ui/widget/TwitterEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 83
    const v1, 0x7f0a08ce

    invoke-virtual {p0, v1}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(I)V

    .line 84
    iget-object v1, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->g:Lcom/twitter/library/client/Session;

    iget-boolean v2, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->d:Z

    invoke-static {p0, v1, v0, v2}, Lbci;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)Lbci;

    move-result-object v0

    .line 86
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 90
    :goto_0
    const-string/jumbo v0, "begin:attempt"

    invoke-direct {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->b(Ljava/lang/String;)V

    .line 91
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->c:Lcom/twitter/android/PhoneEntryFragment;

    invoke-virtual {v0}, Lcom/twitter/android/PhoneEntryFragment;->i()Lcom/twitter/ui/widget/TwitterEditText;

    move-result-object v0

    const v1, 0x7f0a0661

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/twitter/android/PhoneEntryBaseActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->f:Ljava/lang/String;

    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->g:Lcom/twitter/library/client/Session;

    .line 48
    iget-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntrySettingsActivity;->h:Ljava/lang/Long;

    .line 50
    const v0, 0x7f1301a7

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 51
    if-nez p1, :cond_0

    .line 52
    invoke-direct {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->i()V

    .line 54
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 174
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntrySettingsActivity;->finish()V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/PhoneEntryBaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public p()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/twitter/android/PhoneEntryBaseActivity;->onBackPressed()V

    .line 74
    return-void
.end method
