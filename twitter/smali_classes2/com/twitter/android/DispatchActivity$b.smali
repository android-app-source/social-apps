.class Lcom/twitter/android/DispatchActivity$b;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/DispatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 286
    iput-object p1, p0, Lcom/twitter/android/DispatchActivity$b;->a:Landroid/content/Context;

    .line 287
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 282
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/DispatchActivity$b;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 10

    .prologue
    .line 293
    iget-object v0, p0, Lcom/twitter/android/DispatchActivity$b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 295
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const-string/jumbo v2, "4"

    .line 296
    invoke-virtual {v1, v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 298
    iget-object v0, p0, Lcom/twitter/android/DispatchActivity$b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/api/c;->a(Landroid/content/Context;)Lcom/twitter/library/api/c;

    move-result-object v0

    .line 299
    if-eqz v0, :cond_0

    .line 300
    const-string/jumbo v1, "6"

    .line 301
    invoke-virtual {v0}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v2

    .line 300
    invoke-virtual {v9, v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 302
    invoke-virtual {v0}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 305
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 306
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 307
    check-cast p1, Lbcg;

    .line 308
    invoke-virtual {p1}, Lbcg;->e()Lcdk;

    move-result-object v8

    .line 310
    new-instance v0, Lcom/twitter/android/util/AppEventTrack$b;

    iget-object v1, v8, Lcdk;->a:Ljava/lang/String;

    iget-object v2, v8, Lcdk;->b:Ljava/lang/String;

    iget-object v3, v8, Lcdk;->c:Ljava/lang/String;

    iget-object v4, v8, Lcdk;->d:Ljava/lang/String;

    iget-object v5, v8, Lcdk;->e:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/AppEventTrack$b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 319
    iget-object v1, p0, Lcom/twitter/android/DispatchActivity$b;->a:Landroid/content/Context;

    .line 320
    invoke-virtual {v0}, Lcom/twitter/android/util/AppEventTrack$b;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/AppEventTrack;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 319
    invoke-static {v1, v0}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 323
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "external:::irs:referred"

    aput-object v2, v0, v1

    invoke-virtual {v9, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 324
    const/4 v1, 0x3

    iget-object v2, v8, Lcdk;->c:Ljava/lang/String;

    iget-object v3, v8, Lcdk;->d:Ljava/lang/String;

    iget-object v4, v8, Lcdk;->a:Ljava/lang/String;

    iget-object v5, v8, Lcdk;->e:Ljava/lang/String;

    iget-object v6, v8, Lcdk;->b:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 336
    :cond_1
    :goto_0
    invoke-static {v9}, Lcpm;->a(Lcpk;)V

    .line 337
    return-void

    .line 328
    :cond_2
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "external:::irs:error"

    aput-object v3, v1, v2

    invoke-virtual {v9, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 329
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v1

    .line 330
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 331
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 333
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 332
    invoke-static {v9, v0, v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    goto :goto_0
.end method
