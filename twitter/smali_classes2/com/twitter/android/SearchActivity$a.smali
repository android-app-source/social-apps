.class Lcom/twitter/android/SearchActivity$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/SearchFragment$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/SearchActivity;

.field private final b:Lcom/twitter/android/SearchFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/SearchActivity;Lcom/twitter/android/SearchFragment;)V
    .locals 0

    .prologue
    .line 1711
    iput-object p1, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712
    iput-object p2, p0, Lcom/twitter/android/SearchActivity$a;->b:Lcom/twitter/android/SearchFragment;

    .line 1713
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1737
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-boolean v0, v0, Lcom/twitter/android/SearchActivity;->i:Z

    if-eqz v0, :cond_1

    .line 1738
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->e(Lcom/twitter/android/SearchActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v1, v1, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v1}, Lcom/twitter/android/at;->a()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1739
    iput v2, v0, Lcom/twitter/library/client/m;->i:I

    .line 1740
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v0, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->notifyDataSetChanged()V

    .line 1747
    :cond_0
    :goto_0
    return-void

    .line 1741
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->f(Lcom/twitter/android/SearchActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1742
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->e(Lcom/twitter/android/SearchActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v1, v1, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v1}, Lcom/twitter/android/at;->a()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1743
    iput v2, v0, Lcom/twitter/library/client/m;->i:I

    .line 1744
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v0, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->notifyDataSetChanged()V

    .line 1745
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->d(Lcom/twitter/android/SearchActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setTopVisible(Z)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 1722
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-boolean v0, v0, Lcom/twitter/android/SearchActivity;->i:Z

    if-eqz v0, :cond_1

    .line 1723
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->e(Lcom/twitter/android/SearchActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v1, v1, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v1}, Lcom/twitter/android/at;->a()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1724
    iput p1, v0, Lcom/twitter/library/client/m;->i:I

    .line 1725
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/library/client/m;->h:Z

    .line 1726
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v0, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->notifyDataSetChanged()V

    .line 1732
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->d(Lcom/twitter/android/SearchActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DockLayout;->b()V

    .line 1733
    return-void

    .line 1727
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->f(Lcom/twitter/android/SearchActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1728
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0}, Lcom/twitter/android/SearchActivity;->e(Lcom/twitter/android/SearchActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v1, v1, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v1}, Lcom/twitter/android/at;->a()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1729
    iput p1, v0, Lcom/twitter/library/client/m;->i:I

    .line 1730
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->a:Lcom/twitter/android/SearchActivity;

    iget-object v0, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1717
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$a;->b:Lcom/twitter/android/SearchFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->G_()V

    .line 1718
    return-void
.end method
