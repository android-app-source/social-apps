.class Lcom/twitter/android/cl$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/cl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/FragmentActivity;Lbxb;Ljava/util/List;Ljava/util/List;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/util/FriendshipCache;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Lbxb;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/android/timeline/bk;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 397
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 398
    new-instance v1, Lcom/twitter/android/widget/aj$b;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 399
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-interface {p3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/aj$b;->a([Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 400
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v8

    new-instance v0, Lcom/twitter/android/cl$a$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/cl$a$1;-><init>(Lcom/twitter/android/cl$a;Lbxb;Ljava/util/List;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/util/FriendshipCache;Ljava/lang/String;)V

    .line 401
    invoke-virtual {v8, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 408
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 409
    return-void

    .line 397
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
