.class public Lcom/twitter/android/ProfilePhotoImageActivity;
.super Lcom/twitter/android/ImageActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/android/ImageActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ProfilePhotoImageActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoImageActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 20
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ImageActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 22
    const v0, 0x7f130427

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfilePhotoImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    .line 23
    new-instance v1, Lcom/twitter/android/ProfilePhotoImageActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/ProfilePhotoImageActivity$1;-><init>(Lcom/twitter/android/ProfilePhotoImageActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 37
    packed-switch p1, :pswitch_data_0

    .line 47
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ImageActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 39
    :pswitch_0
    if-ne p2, v2, :cond_0

    .line 40
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoImageActivity;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 41
    invoke-virtual {p0, v2, p3}, Lcom/twitter/android/ProfilePhotoImageActivity;->setResult(ILandroid/content/Intent;)V

    .line 42
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoImageActivity;->finish()V

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
