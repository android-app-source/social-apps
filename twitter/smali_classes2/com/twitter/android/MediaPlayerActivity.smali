.class public Lcom/twitter/android/MediaPlayerActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lajl;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/m;
.implements Lcom/twitter/android/widget/MediaPlayerView$d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
        "Lajl;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/twitter/android/m;",
        "Lcom/twitter/android/widget/MediaPlayerView$d;"
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/android/widget/MediaPlayerView;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lcom/twitter/library/media/player/InlineVideoView;

.field private d:Landroid/widget/ProgressBar;

.field private e:Lcom/twitter/media/ui/image/MediaImageView;

.field private f:Lcom/twitter/library/widget/TweetView;

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcom/twitter/model/core/Tweet;

.field private m:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private n:Landroid/net/Uri;

.field private o:Landroid/view/animation/Animation;

.field private p:Landroid/view/animation/Animation;

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Lajm;

.field private t:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private u:Lcom/twitter/android/MediaActionBarFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->q:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MediaPlayerActivity;)Lcom/twitter/library/media/player/InlineVideoView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 296
    new-instance v0, Lajm;

    invoke-direct {v0, p0}, Lajm;-><init>(Lajl;)V

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Lajm;

    .line 297
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Lajm;

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    invoke-virtual {v0, v1}, Lajm;->a(Lcom/twitter/library/media/player/InlineVideoView;)V

    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->Q()Lcom/twitter/library/media/manager/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/g;->e()Lcom/twitter/library/media/manager/m;

    move-result-object v1

    .line 299
    invoke-static {p1}, Lcom/twitter/library/media/manager/n;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/n$a;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/MediaPlayerActivity$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/MediaPlayerActivity$1;-><init>(Lcom/twitter/android/MediaPlayerActivity;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/manager/n$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/n$a;

    .line 316
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/n$a;->a()Lcom/twitter/library/media/manager/n;

    move-result-object v0

    .line 317
    invoke-virtual {v1, v0}, Lcom/twitter/library/media/manager/m;->d(Lcom/twitter/media/request/b;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Ljava/util/concurrent/Future;

    .line 318
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 514
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 515
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/twitter/android/MediaPlayerActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/MediaPlayerActivity$2;-><init>(Lcom/twitter/android/MediaPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 526
    :cond_0
    return-void
.end method


# virtual methods
.method public W_()V
    .locals 0

    .prologue
    .line 567
    return-void
.end method

.method public a()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 113
    const v1, 0x7f0401a8

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 114
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 115
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 116
    return-object v0
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 536
    if-nez p1, :cond_1

    .line 537
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->h()V

    .line 541
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    .line 542
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 543
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    .line 544
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 545
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 546
    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    .line 543
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 539
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->i()V

    goto :goto_0

    .line 549
    :cond_2
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 475
    if-eqz p2, :cond_0

    .line 476
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    sget-object v0, Lbtd;->a:Lbtd;

    invoke-virtual {v0, p2}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    .line 478
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->u:Lcom/twitter/android/MediaActionBarFragment;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->u:Lcom/twitter/android/MediaActionBarFragment;

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;)V

    .line 483
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 502
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->q:Z

    if-ne v0, p1, :cond_0

    .line 511
    :goto_0
    return-void

    .line 505
    :cond_0
    if-eqz p1, :cond_1

    .line 506
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 510
    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/MediaPlayerActivity;->q:Z

    goto :goto_0

    .line 508
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public a(Lcmm;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 264
    invoke-interface {p1}, Lcmm;->a()I

    move-result v2

    .line 265
    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    .line 266
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcax;->c()Ljava/lang/String;

    move-result-object v1

    .line 267
    :goto_0
    const v3, 0x7f1308af

    if-ne v2, v3, :cond_1

    .line 268
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v2

    const/4 v3, 0x0

    .line 269
    invoke-virtual {v2, v1, v3}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 270
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 271
    invoke-virtual {v1, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 268
    invoke-virtual {p0, v1}, Lcom/twitter/android/MediaPlayerActivity;->startActivity(Landroid/content/Intent;)V

    .line 276
    :goto_1
    return v0

    .line 266
    :cond_0
    const-string/jumbo v1, ""

    goto :goto_0

    .line 273
    :cond_1
    const v1, 0x7f13088e

    if-ne v2, v1, :cond_2

    .line 274
    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->showDialog(I)V

    .line 276
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_1
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 232
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    const v0, 0x7f14001a

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 236
    :cond_0
    const v0, 0x7f14000b

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 237
    const/4 v0, 0x1

    .line 239
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcmr;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 253
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Lcmr;)I

    move-result v4

    .line 254
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 255
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 256
    iget-object v5, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    iget-wide v6, v5, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-nez v1, :cond_0

    move v1, v2

    .line 257
    :goto_0
    const v5, 0x7f13088e

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    .line 258
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->u:Lcom/twitter/android/MediaActionBarFragment;

    if-eqz v1, :cond_1

    .line 257
    :goto_1
    invoke-virtual {v0, v2}, Lazv;->b(Z)Lazv;

    .line 259
    return v4

    :cond_0
    move v1, v3

    .line 256
    goto :goto_0

    :cond_1
    move v2, v3

    .line 258
    goto :goto_1
.end method

.method public b()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/twitter/util/d;->b(Landroid/view/View;)V

    .line 402
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->h()V

    .line 403
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x96

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 121
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 122
    const-string/jumbo v0, "aud"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->h:Z

    .line 123
    const-string/jumbo v0, "simple_controls"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->j:Z

    .line 125
    const-string/jumbo v0, "association"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->m:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 126
    const-string/jumbo v0, "tweet"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    .line 127
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    .line 128
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 127
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/database/schema/a;->b(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Landroid/net/Uri;

    .line 131
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 132
    const-string/jumbo v1, "media_forward"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 135
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    .line 137
    const-string/jumbo v0, "player_url"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 139
    const-string/jumbo v0, "player_stream_urls"

    .line 140
    invoke-virtual {v9, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 142
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 143
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaDescriptor;

    iget-object v0, v0, Lcom/twitter/media/model/MediaDescriptor;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 146
    :goto_1
    const v0, 0x7f130385

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    .line 147
    const v0, 0x7f130492

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaPlayerView;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    .line 148
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    iget-boolean v4, p0, Lcom/twitter/android/MediaPlayerActivity;->j:Z

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/MediaPlayerView;->setUseSimplePlayPauseControls(Z)V

    .line 149
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/MediaPlayerView;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 152
    const v0, 0x7f130645

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    .line 153
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    .line 154
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetView;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 155
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    .line 156
    const v0, 0x7f130495

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 157
    const v4, 0x7f130494

    invoke-virtual {p0, v4}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 158
    iget-object v5, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v5, v0, v4}, Lcom/twitter/android/widget/MediaPlayerView;->a(Landroid/widget/ImageButton;Landroid/view/View;)V

    .line 159
    const v0, 0x7f130493

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/player/InlineVideoView;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    .line 160
    invoke-static {v2}, Lcom/twitter/library/util/af;->d(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    .line 161
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    if-eqz v0, :cond_1

    .line 162
    iput-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Ljava/lang/String;

    .line 165
    :cond_1
    const v0, 0x7f130496

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    .line 170
    new-instance v0, Lcom/twitter/android/l;

    invoke-direct {v0, p0}, Lcom/twitter/android/l;-><init>(Lcom/twitter/android/m;)V

    .line 172
    const v2, 0x7f05002f

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Landroid/view/animation/Animation;

    .line 173
    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Landroid/view/animation/Animation;

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 174
    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Landroid/view/animation/Animation;

    invoke-virtual {v2, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 175
    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Landroid/view/animation/Animation;

    invoke-virtual {v2, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 177
    const v2, 0x7f050031

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Landroid/view/animation/Animation;

    .line 178
    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Landroid/view/animation/Animation;

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 179
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Landroid/view/animation/Animation;

    invoke-virtual {v0, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 180
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Landroid/view/animation/Animation;

    invoke-virtual {v0, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 182
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    if-eqz v0, :cond_6

    .line 183
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->b(Ljava/lang/String;)V

    .line 184
    const v1, 0x7f1302e4

    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->m:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, "tweet"

    const-string/jumbo v4, ""

    const-string/jumbo v5, "tweet"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/android/MediaActionBarFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->u:Lcom/twitter/android/MediaActionBarFragment;

    .line 187
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    :goto_2
    const-string/jumbo v0, "image_url"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 197
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    const v0, 0x7f13031d

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    .line 199
    invoke-virtual {v0, v7}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 200
    iget-boolean v2, p0, Lcom/twitter/android/MediaPlayerActivity;->h:Z

    if-eqz v2, :cond_7

    .line 201
    sget-object v2, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->c:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 205
    :goto_3
    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 206
    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->e:Lcom/twitter/media/ui/image/MediaImageView;

    .line 209
    :cond_2
    if-eqz p1, :cond_3

    .line 210
    const-string/jumbo v0, "seek"

    invoke-virtual {p1, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    .line 213
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Landroid/net/Uri;

    if-eqz v0, :cond_4

    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v6, v8, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 217
    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/MediaPlayerActivity;->i()V

    .line 218
    return-void

    :cond_5
    move v0, v7

    .line 135
    goto/16 :goto_0

    .line 189
    :cond_6
    const-string/jumbo v0, "video_index"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 190
    const-string/jumbo v1, "video_position"

    invoke-virtual {v9, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 191
    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v2, v3, v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->a(Ljava/util/ArrayList;II)V

    .line 192
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    const-string/jumbo v1, "is_looping"

    invoke-virtual {v9, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->setIsLooping(Z)V

    .line 193
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/MediaPlayerView;->setMediaControllerListener(Lcom/twitter/android/widget/MediaPlayerView$d;)V

    goto :goto_2

    .line 203
    :cond_7
    sget-object v2, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->a:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    goto :goto_3

    :cond_8
    move-object v1, v8

    goto/16 :goto_1
.end method

.method public c()V
    .locals 1

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->i()V

    .line 408
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    .line 409
    return-void
.end method

.method public c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 431
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 432
    packed-switch p1, :pswitch_data_0

    .line 448
    :pswitch_0
    const v0, 0x7f0a051f

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 452
    :goto_0
    return-void

    .line 434
    :pswitch_1
    const v0, 0x7f0a0520

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 438
    :pswitch_2
    const v0, 0x7f0a051e

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 439
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 443
    :pswitch_3
    const v0, 0x7f0a0513

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 444
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 432
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 370
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 371
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 373
    iput-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Ljava/util/concurrent/Future;

    .line 375
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    if-eqz v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    invoke-virtual {v0}, Lcom/twitter/library/media/player/InlineVideoView;->a()V

    .line 381
    :goto_0
    iput-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Lajm;

    .line 382
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 383
    return-void

    .line 378
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaPlayerView;->f()V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 387
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 388
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->finish()V

    .line 391
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 413
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 414
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->e:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 417
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    if-nez v0, :cond_2

    .line 418
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/MediaPlayerView;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    if-eqz v0, :cond_1

    .line 420
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/player/InlineVideoView;->setVisibility(I)V

    .line 427
    :cond_1
    :goto_0
    return-void

    .line 423
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/player/InlineVideoView;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    invoke-virtual {v0}, Lcom/twitter/library/media/player/InlineVideoView;->start()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->a(Z)V

    .line 228
    return-void

    .line 227
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Lcom/twitter/model/core/Tweet;

    invoke-static {p0, v0, p1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/core/Tweet;I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 463
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Landroid/net/Uri;

    sget-object v3, Lbuj;->b:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/MediaPlayerActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 346
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPause()V

    .line 347
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    invoke-virtual {v0}, Lcom/twitter/library/media/player/InlineVideoView;->pause()V

    .line 352
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaPlayerView;->pause()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 322
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 323
    iget v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    if-lez v0, :cond_1

    .line 324
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    iget v1, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->setStartPosition(I)V

    .line 327
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    .line 329
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 333
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 335
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Z

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lcom/twitter/library/media/player/InlineVideoView;

    invoke-virtual {v0}, Lcom/twitter/library/media/player/InlineVideoView;->getCurrentPosition()I

    move-result v0

    .line 340
    :goto_0
    iput v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    .line 341
    const-string/jumbo v1, "seek"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 342
    return-void

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaPlayerView;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x0

    return v0
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 356
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Lajm;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Lajm;

    invoke-virtual {v0}, Lajm;->a()I

    move-result v0

    .line 358
    if-lez v0, :cond_0

    .line 359
    invoke-static {p0}, Lcom/twitter/library/vineloops/VineLoopAggregator;->a(Landroid/content/Context;)Lcom/twitter/library/vineloops/VineLoopAggregator;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/vineloops/VineLoopAggregator;->a(Ljava/lang/String;I)V

    .line 361
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 362
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 360
    invoke-static {v0, v1}, Lcom/twitter/library/vineloops/a;->a(Landroid/content/Context;Lcom/twitter/library/client/p;)Lcom/twitter/library/vineloops/a;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Lcom/twitter/library/vineloops/a;->a()V

    .line 365
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 366
    return-void
.end method
