.class Lcom/twitter/android/ValidationState$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ValidationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/android/ValidationState;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ValidationState$1;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/twitter/android/ValidationState$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/android/ValidationState;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 93
    new-instance v2, Lcom/twitter/android/ValidationState;

    const-class v0, Lcom/twitter/android/ValidationState$State;

    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ValidationState$State;

    const-class v1, Lcom/twitter/android/ValidationState$Level;

    .line 94
    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    .line 93
    return-object v2
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/android/ValidationState;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p2, Lcom/twitter/android/ValidationState;->b:Lcom/twitter/android/ValidationState$State;

    const-class v1, Lcom/twitter/android/ValidationState$State;

    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/android/ValidationState;->c:Lcom/twitter/android/ValidationState$Level;

    const-class v2, Lcom/twitter/android/ValidationState$Level;

    .line 86
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 87
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    check-cast p2, Lcom/twitter/android/ValidationState;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ValidationState$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/android/ValidationState;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ValidationState$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/android/ValidationState;

    move-result-object v0

    return-object v0
.end method
