.class Lcom/twitter/android/TweetFragment$a;
.super Lcjr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/TweetFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;

.field private final b:Lcom/twitter/ui/view/h;

.field private final c:Lcom/twitter/ui/view/h;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Z

.field private h:Z


# direct methods
.method protected constructor <init>(Lcom/twitter/android/TweetFragment;Landroid/content/Context;Lcom/twitter/android/av;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2097
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    .line 2098
    invoke-direct {p0, p2}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 2080
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    .line 2082
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->c(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 2083
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->d(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 2084
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment$a;->b:Lcom/twitter/ui/view/h;

    .line 2085
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    .line 2087
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->c(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    const/4 v1, 0x0

    .line 2088
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->d(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 2089
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment$a;->c:Lcom/twitter/ui/view/h;

    .line 2092
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment$a;->f:Ljava/util/Set;

    .line 2099
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment$a;->d:Landroid/view/LayoutInflater;

    .line 2100
    iput-object p3, p0, Lcom/twitter/android/TweetFragment$a;->e:Lcom/twitter/android/av;

    .line 2101
    invoke-static {p2}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/k;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment$a;->g:Z

    .line 2102
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2370
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f040121

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2371
    new-instance v1, Latq;

    invoke-direct {v1, v0}, Latq;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2372
    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2376
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latq;

    .line 2377
    invoke-static {}, Lbpi;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Latq;->a(I)V

    .line 2378
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/cd;Z)V
    .locals 4

    .prologue
    .line 2424
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->e:Lcom/twitter/android/av;

    if-eqz v0, :cond_0

    .line 2425
    new-instance v2, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2427
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment$a;->g()Lcbi;

    move-result-object v0

    .line 2428
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$a;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 2429
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    invoke-virtual {v0}, Lcom/twitter/android/timeline/bl;->c()I

    move-result v0

    move v1, v0

    .line 2435
    :goto_0
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 2436
    :goto_1
    const-string/jumbo v3, "position"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2437
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->e:Lcom/twitter/android/av;

    invoke-interface {v1, p1, v0, v2}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 2439
    :cond_0
    return-void

    .line 2432
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 2435
    :cond_2
    iget-object v0, p2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    goto :goto_1
.end method

.method private a(Landroid/view/View;Lcom/twitter/model/timeline/ar;)V
    .locals 2

    .prologue
    .line 2398
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latl;

    .line 2399
    invoke-static {}, Lbpi;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Latl;->a(I)V

    .line 2400
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->K(Lcom/twitter/android/TweetFragment;)Latr;

    move-result-object v1

    invoke-virtual {v1, p2}, Latr;->a(Lcom/twitter/model/timeline/ar;)Z

    move-result v1

    invoke-virtual {v0, v1}, Latl;->a(Z)V

    .line 2401
    return-void
.end method

.method private a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2405
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;)V

    .line 2406
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/media/widget/TweetMediaView$a;

    move-result-object v10

    .line 2407
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment$a;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v11

    .line 2408
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 2409
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->k:Z

    if-eqz v0, :cond_0

    move v7, v8

    .line 2411
    :goto_0
    invoke-virtual {v11}, Lbaa;->b()Z

    move-result v0

    .line 2410
    invoke-static {p2, v7, v0}, Lbwr;->a(Lcom/twitter/model/core/Tweet;ZZ)Z

    move-result v1

    .line 2412
    new-instance v0, Lbxy;

    iget-boolean v2, p0, Lcom/twitter/android/TweetFragment$a;->g:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    move v1, v8

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    .line 2413
    invoke-virtual {v2}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->L(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->M(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v6

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lbxy;-><init>(ZLandroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 2414
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v10}, Lbxy;->a(ILjava/lang/Object;)V

    .line 2416
    invoke-virtual {v11, p2}, Lbaa;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    .line 2417
    invoke-virtual {p1, v7}, Lcom/twitter/library/widget/TweetView;->setDisplaySensitiveMedia(Z)V

    .line 2418
    iget-boolean v2, p0, Lcom/twitter/android/TweetFragment$a;->g:Z

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    :goto_2
    invoke-virtual {p1, v8}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 2419
    invoke-virtual {p1, p2, p3, v9, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;)V

    .line 2420
    return-void

    :cond_0
    move v7, v9

    .line 2409
    goto :goto_0

    :cond_1
    move v1, v9

    .line 2412
    goto :goto_1

    :cond_2
    move v8, v9

    .line 2418
    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment$a;)Z
    .locals 1

    .prologue
    .line 2070
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$a;->c()Z

    move-result v0

    return v0
.end method

.method private b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2381
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f04011f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2382
    new-instance v1, Latm;

    invoke-direct {v1, v0}, Latm;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2383
    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2387
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latm;

    .line 2388
    invoke-static {}, Lbpi;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Latm;->a(I)V

    .line 2389
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment$a;)Z
    .locals 1

    .prologue
    .line 2070
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$a;->d()Z

    move-result v0

    return v0
.end method

.method private c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2392
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f04011e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2393
    new-instance v1, Latl;

    invoke-direct {v1, v0}, Latl;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2394
    return-object v0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment$a;->g()Lcbi;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/timeline/bl;

    return v0
.end method

.method private d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2155
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment$a;->g()Lcbi;

    move-result-object v0

    .line 2156
    instance-of v2, v0, Lcom/twitter/android/timeline/bl;

    if-eqz v2, :cond_1

    .line 2157
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    .line 2158
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bl;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 2159
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2161
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2159
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2161
    goto :goto_0
.end method

.method private e()Lcbp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbp",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2451
    new-instance v0, Lcbn;

    invoke-static {}, Lcom/twitter/android/timeline/bj;->a()Lcbp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbn;-><init>(Lcbp;)V

    return-object v0
.end method


# virtual methods
.method public a(J)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2121
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment$a;->g()Lcbi;

    move-result-object v0

    .line 2122
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$a;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 2123
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    .line 2124
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bl;->be_()I

    move-result v3

    move v1, v2

    .line 2125
    :goto_0
    if-ge v1, v3, :cond_1

    .line 2126
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/bl;->c(I)J

    move-result-wide v4

    .line 2127
    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    move v0, v1

    .line 2132
    :goto_1
    return v0

    .line 2125
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2132
    goto :goto_1
.end method

.method protected a(Lcom/twitter/android/timeline/bk;)I
    .locals 3

    .prologue
    const/4 v1, 0x5

    const/4 v2, -0x1

    .line 2187
    instance-of v0, p1, Lcom/twitter/android/timeline/ak;

    if-eqz v0, :cond_0

    .line 2188
    const/4 v0, 0x2

    .line 2220
    :goto_0
    return v0

    .line 2189
    :cond_0
    instance-of v0, p1, Lcom/twitter/android/timeline/cd;

    if-eqz v0, :cond_2

    .line 2190
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 2191
    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2192
    const/4 v0, 0x0

    goto :goto_0

    .line 2194
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2196
    :cond_2
    instance-of v0, p1, Lcom/twitter/android/timeline/cf;

    if-eqz v0, :cond_3

    .line 2197
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 2198
    iget-object v0, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v0, v0, Lcom/twitter/model/timeline/ar;->c:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 2214
    goto :goto_0

    .line 2200
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 2204
    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 2211
    goto :goto_0

    .line 2217
    :cond_3
    instance-of v0, p1, Lcom/twitter/android/timeline/cj;

    if-eqz v0, :cond_4

    .line 2218
    const/4 v0, 0x6

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2220
    goto :goto_0

    .line 2198
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2070
    check-cast p1, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    return v0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 2226
    invoke-virtual {p0, p2}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    .line 2227
    packed-switch v0, :pswitch_data_0

    .line 2287
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    .line 2229
    :pswitch_0
    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment$a;->h:Z

    if-nez v0, :cond_0

    .line 2230
    const-string/jumbo v0, "urt_conv:focal:complete"

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    .line 2231
    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->y(Lcom/twitter/android/TweetFragment;)Lcom/twitter/metrics/j;

    move-result-object v1

    sget-object v2, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 2230
    invoke-static {v0, v1, v2}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;

    move-result-object v0

    .line 2232
    invoke-virtual {v0}, Lcom/twitter/metrics/e;->j()V

    .line 2233
    iput-boolean v3, p0, Lcom/twitter/android/TweetFragment$a;->h:Z

    .line 2235
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 2236
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Lcom/twitter/model/core/Tweet;)V

    .line 2237
    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->m:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 2239
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->z(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 2240
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2241
    new-instance v2, Lbhy;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v3}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lbhy;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 2243
    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->s:J

    iput-wide v4, v2, Lbhy;->a:J

    .line 2244
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->A(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 2251
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->C(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/card/d;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2252
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2253
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-virtual {v0}, Lcax;->b()Ljava/lang/String;

    move-result-object v0

    .line 2254
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->C(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/card/d;

    move-result-object v1

    const-string/jumbo v2, "show"

    const-string/jumbo v3, "platform_card"

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    goto :goto_0

    .line 2247
    :cond_3
    new-instance v1, Lbio;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v2}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->B(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v6, v0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lbio;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    .line 2249
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    const/4 v3, 0x5

    invoke-static {v2, v1, v3, v7}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/service/s;II)Z

    goto :goto_1

    .line 2253
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 2260
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f040120

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2261
    new-instance v1, Lcom/twitter/android/cu;

    invoke-direct {v1, v0}, Lcom/twitter/android/cu;-><init>(Landroid/view/View;)V

    .line 2262
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2266
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f0402c1

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2267
    new-instance v1, Lcom/twitter/android/bg;

    invoke-direct {v1, v0}, Lcom/twitter/android/bg;-><init>(Landroid/view/View;)V

    .line 2268
    iget-object v2, v1, Lcom/twitter/android/bg;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 2269
    iget-object v2, v1, Lcom/twitter/android/bg;->a:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 2270
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2274
    :pswitch_3
    invoke-direct {p0, p3}, Lcom/twitter/android/TweetFragment$a;->c(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2277
    :pswitch_4
    invoke-direct {p0, p3}, Lcom/twitter/android/TweetFragment$a;->b(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2280
    :pswitch_5
    invoke-direct {p0, p3}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2283
    :pswitch_6
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2227
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2070
    check-cast p2, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bl;
    .locals 2

    .prologue
    .line 2443
    new-instance v0, Lcom/twitter/android/timeline/bl;

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$a;->e()Lcbp;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/twitter/android/timeline/bl;-><init>(Landroid/database/Cursor;Lcbp;)V

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 2294
    invoke-virtual {p0, p3}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    .line 2295
    packed-switch v0, :pswitch_data_0

    .line 2367
    :goto_0
    :pswitch_0
    return-void

    .line 2297
    :pswitch_1
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetDetailView;

    .line 2298
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/twitter/android/timeline/cd;

    .line 2299
    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    .line 2301
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 2302
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->D(Lcom/twitter/android/TweetFragment;)Laji;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v4}, Lcom/twitter/android/TweetFragment;->E(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/cc;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    .line 2303
    invoke-static {v5}, Lcom/twitter/android/TweetFragment;->F(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$b;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v6}, Lcom/twitter/android/TweetFragment;->G(Lcom/twitter/android/TweetFragment;)Z

    move-result v6

    .line 2302
    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/core/Tweet;Lcne;Laji;Lcom/twitter/android/cc;Lbxm$a;ZZ)V

    .line 2309
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->q()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2310
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->q(Lcom/twitter/android/TweetFragment;)V

    .line 2312
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->H(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/api/ActivitySummary;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2313
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->H(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/api/ActivitySummary;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;Lcom/twitter/android/widget/TweetDetailView$b;)V

    .line 2315
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->I(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/widget/EngagementActionBar;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2316
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->I(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/widget/EngagementActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 2317
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->I(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/widget/EngagementActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->J(Lcom/twitter/android/TweetFragment;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/EngagementActionBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2319
    :cond_2
    invoke-direct {p0, p1, v8, v7}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/view/View;Lcom/twitter/android/timeline/cd;Z)V

    goto/16 :goto_0

    .line 2323
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cu;

    .line 2324
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/cd;

    .line 2326
    invoke-static {v1}, Lbxh;->c(Lcom/twitter/android/timeline/cd;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->c:Lcom/twitter/ui/view/h;

    .line 2328
    :goto_1
    iget-object v3, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v4, v1, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v3, v4, v2}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)V

    .line 2330
    invoke-static {v1}, Lbxh;->d(Lcom/twitter/android/timeline/cd;)Lbxh$a;

    move-result-object v3

    .line 2331
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/internal/android/widget/GroupedRowView;

    .line 2332
    invoke-static {v2, v3}, Lbxh;->a(Lcom/twitter/internal/android/widget/GroupedRowView;Lbxh$a;)V

    .line 2333
    iget-object v2, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v4, v3, Lbxh$a;->a:Z

    iget-boolean v3, v3, Lbxh$a;->b:Z

    invoke-virtual {v2, v4, v3}, Lcom/twitter/library/widget/TweetView;->a(ZZ)V

    .line 2335
    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, v7}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    .line 2336
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/view/View;Lcom/twitter/android/timeline/cd;Z)V

    goto/16 :goto_0

    .line 2326
    :cond_3
    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->b:Lcom/twitter/ui/view/h;

    goto :goto_1

    .line 2340
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bg;

    .line 2341
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/ak;

    .line 2342
    iget-object v2, v0, Lcom/twitter/android/bg;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, v1, Lcom/twitter/android/timeline/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment$a;->c:Lcom/twitter/ui/view/h;

    invoke-direct {p0, v2, v3, v4}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)V

    .line 2343
    iget-object v0, v0, Lcom/twitter/android/bg;->a:Lcom/twitter/library/widget/TweetView;

    iget-object v1, v1, Lcom/twitter/android/timeline/ak;->b:Lcom/twitter/model/core/Tweet;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$a;->c:Lcom/twitter/ui/view/h;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)V

    goto/16 :goto_0

    .line 2348
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment$a;->b(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2352
    :pswitch_5
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 2353
    iget-object v0, v0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/view/View;Lcom/twitter/model/timeline/ar;)V

    goto/16 :goto_0

    .line 2357
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2295
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2070
    check-cast p3, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;)V

    return-void
.end method

.method public a()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2105
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2117
    :cond_0
    :goto_0
    return v2

    .line 2108
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment$a;->g()Lcbi;

    move-result-object v4

    .line 2109
    if-nez v4, :cond_2

    move v1, v2

    :goto_1
    move v3, v2

    .line 2110
    :goto_2
    if-ge v3, v1, :cond_0

    .line 2111
    invoke-virtual {v4, v3}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 2112
    instance-of v5, v0, Lcom/twitter/android/timeline/cd;

    if-eqz v5, :cond_3

    .line 2113
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 2114
    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v2

    goto :goto_0

    .line 2109
    :cond_2
    invoke-virtual {v4}, Lcbi;->be_()I

    move-result v0

    move v1, v0

    goto :goto_1

    .line 2110
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2
.end method

.method public b()V
    .locals 0

    .prologue
    .line 2148
    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 2447
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment$a;->k()Lcjt;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment$a;->a(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 2448
    return-void
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 2166
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment$a;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 2167
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    .line 2172
    :goto_0
    return-wide v0

    .line 2168
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment$a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2169
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$a;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->x(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/timeline/bl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bl;->d(I)J

    move-result-wide v0

    goto :goto_0

    .line 2172
    :cond_1
    invoke-super {p0, p1}, Lcjr;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 2182
    const/4 v0, 0x7

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 2177
    const/4 v0, 0x1

    return v0
.end method
