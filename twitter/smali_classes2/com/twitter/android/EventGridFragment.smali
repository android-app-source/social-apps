.class public Lcom/twitter/android/EventGridFragment;
.super Lcom/twitter/android/SearchPhotosFragment;
.source "Twttr"


# instance fields
.field private aa:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/android/SearchPhotosFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public G_()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    .line 83
    iget-object v1, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    iget v1, p0, Lcom/twitter/android/EventGridFragment;->aa:I

    invoke-virtual {v0, v3, v1}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 88
    :cond_0
    iget v0, p0, Lcom/twitter/android/EventGridFragment;->n:I

    if-lez v0, :cond_1

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->s()V

    .line 90
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/EventGridFragment;->A:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 91
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->E()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string/jumbo v2, "new_tweet_prompt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "click"

    aput-object v2, v0, v1

    .line 90
    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventGridFragment;->a(Ljava/lang/String;)V

    .line 93
    :cond_1
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-super {p0, p1}, Lcom/twitter/android/SearchPhotosFragment;->a(Landroid/content/Context;)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->I_()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 32
    const v0, 0x7f040396

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v1

    .line 33
    invoke-virtual {p1}, Lcom/twitter/app/common/list/l$d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f040221

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 35
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const v0, 0x7f0403e0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 37
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 39
    :cond_0
    return-void

    .line 33
    :cond_1
    const v0, 0x7f040397

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 99
    instance-of v1, v0, Lcom/twitter/android/ScrollingHeaderActivity;

    if-eqz v1, :cond_0

    .line 100
    check-cast v0, Lcom/twitter/android/ScrollingHeaderActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Z)V

    .line 103
    :cond_0
    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/EventGridFragment;->E:Lcom/twitter/android/SearchFragment$b;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/twitter/android/EventGridFragment;->E:Lcom/twitter/android/SearchFragment$b;

    invoke-interface {v0}, Lcom/twitter/android/SearchFragment$b;->a()V

    .line 106
    :cond_1
    return-void
.end method

.method protected a(Lcom/twitter/model/topic/TwitterTopic;)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method protected ap_()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x7

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string/jumbo v0, "search_id=?"

    return-object v0
.end method

.method protected g()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/twitter/android/SearchPhotosFragment;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/twitter/android/EventGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0367

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/EventGridFragment;->aa:I

    .line 27
    return-void
.end method
