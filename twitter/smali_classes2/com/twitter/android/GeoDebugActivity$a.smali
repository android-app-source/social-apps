.class Lcom/twitter/android/GeoDebugActivity$a;
.super Lcom/twitter/app/common/dialog/BaseDialogFragment;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/GeoDebugActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/maps/model/d;

.field private final c:Landroid/location/Location;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/Spinner;

.field private g:Landroid/widget/Spinner;

.field private final h:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method constructor <init>(Lcom/google/android/gms/maps/model/d;Landroid/location/Location;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0

    .prologue
    .line 444
    invoke-direct {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;-><init>()V

    .line 445
    iput-object p1, p0, Lcom/twitter/android/GeoDebugActivity$a;->a:Lcom/google/android/gms/maps/model/d;

    .line 446
    iput-object p2, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    .line 447
    iput-object p3, p0, Lcom/twitter/android/GeoDebugActivity$a;->h:Landroid/content/DialogInterface$OnClickListener;

    .line 448
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GeoDebugActivity$a;)Lcom/google/android/gms/maps/model/d;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->a:Lcom/google/android/gms/maps/model/d;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/GeoDebugActivity$a;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 505
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->g:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 506
    if-nez v0, :cond_0

    .line 541
    :goto_0
    return-void

    .line 510
    :cond_0
    :try_start_0
    const-string/jumbo v1, "H-Accuracy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 511
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 512
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->removeAccuracy()V

    .line 537
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {v1}, Lbqm;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 538
    :catch_0
    move-exception v0

    goto :goto_0

    .line 514
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    goto :goto_1

    .line 539
    :catch_1
    move-exception v0

    goto :goto_0

    .line 516
    :cond_3
    const-string/jumbo v1, "Altitude"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 517
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 518
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->removeAltitude()V

    goto :goto_1

    .line 520
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setAltitude(D)V

    goto :goto_1

    .line 522
    :cond_5
    const-string/jumbo v1, "Speed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 523
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 524
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->removeSpeed()V

    goto :goto_1

    .line 526
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setSpeed(F)V

    goto :goto_1

    .line 528
    :cond_7
    const-string/jumbo v1, "Bearing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 529
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 530
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->removeBearing()V

    goto :goto_1

    .line 532
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setBearing(F)V

    goto :goto_1

    .line 534
    :cond_9
    const-string/jumbo v1, "Timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x1090009

    const v5, 0x1090008

    .line 453
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity$a;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 454
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity$a;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 455
    const v2, 0x7f040115

    invoke-virtual {v0, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 456
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 458
    const v0, 0x7f1303b5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->d:Landroid/widget/TextView;

    .line 459
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {v3}, Lbqm;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 461
    const v0, 0x7f1303b7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    .line 462
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/twitter/android/GeoDebugActivity$a$1;

    invoke-direct {v3, p0}, Lcom/twitter/android/GeoDebugActivity$a$1;-><init>(Lcom/twitter/android/GeoDebugActivity$a;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 469
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity$a;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 471
    invoke-virtual {v3, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 472
    const-string/jumbo v0, "gps"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 473
    const-string/jumbo v0, "network"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 474
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v0, v4, :cond_0

    .line 475
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity$a;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 476
    :cond_0
    const-string/jumbo v0, "fused"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 478
    :cond_1
    const v0, 0x7f1303b8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->f:Landroid/widget/Spinner;

    .line 479
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 480
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 482
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity$a;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 484
    invoke-virtual {v3, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 485
    const-string/jumbo v0, "Latitude"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 486
    const-string/jumbo v0, "Longitude"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 487
    const-string/jumbo v0, "H-Accuracy"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 488
    const-string/jumbo v0, "Altitude"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 489
    const-string/jumbo v0, "Speed"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 490
    const-string/jumbo v0, "Bearing"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 491
    const-string/jumbo v0, "Timestamp"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 492
    const-string/jumbo v0, "Source"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 493
    const v0, 0x7f1303b6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->g:Landroid/widget/Spinner;

    .line 494
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->g:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 495
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->g:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 497
    const v0, 0x7f0a0baa

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a0bb1

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->h:Landroid/content/DialogInterface$OnClickListener;

    .line 498
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a0262

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->h:Landroid/content/DialogInterface$OnClickListener;

    .line 499
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a00f6

    .line 500
    invoke-virtual {v0, v2, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 501
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 545
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 614
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 547
    :pswitch_1
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 548
    if-eqz v0, :cond_0

    .line 549
    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v1, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 550
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-static {v1}, Lbqm;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 555
    :pswitch_2
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 556
    if-eqz v0, :cond_0

    .line 559
    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 562
    const-string/jumbo v3, "Source"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 564
    :goto_1
    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->f:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v3

    invoke-interface {v3}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 565
    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/GeoDebugActivity$a;->f:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 566
    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->f:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 606
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 607
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_0

    .line 564
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 570
    :cond_3
    const-string/jumbo v3, "Latitude"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 571
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 572
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    move v6, v2

    move v2, v1

    move v1, v6

    .line 573
    goto :goto_2

    .line 574
    :cond_4
    const-string/jumbo v3, "Longitude"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 575
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 576
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    move v6, v2

    move v2, v1

    move v1, v6

    .line 577
    goto :goto_2

    .line 578
    :cond_5
    const-string/jumbo v3, "H-Accuracy"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 579
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    move v6, v2

    move v2, v1

    move v1, v6

    .line 580
    goto :goto_2

    .line 581
    :cond_6
    const-string/jumbo v3, "Altitude"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 582
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 583
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAltitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    move v6, v2

    move v2, v1

    move v1, v6

    .line 587
    goto/16 :goto_2

    .line 585
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 588
    :cond_8
    const-string/jumbo v3, "Speed"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 589
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 590
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getSpeed()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    move v6, v2

    move v2, v1

    move v1, v6

    .line 594
    goto/16 :goto_2

    .line 592
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 595
    :cond_a
    const-string/jumbo v3, "Bearing"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 596
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 597
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getBearing()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    move v6, v2

    move v2, v1

    move v1, v6

    .line 601
    goto/16 :goto_2

    .line 599
    :cond_b
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 602
    :cond_c
    const-string/jumbo v3, "Timestamp"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 603
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity$a;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity$a;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    move v6, v2

    move v2, v1

    move v1, v6

    .line 604
    goto/16 :goto_2

    :cond_d
    move v1, v2

    goto/16 :goto_2

    .line 545
    nop

    :pswitch_data_0
    .packed-switch 0x7f1303b6
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 618
    return-void
.end method
