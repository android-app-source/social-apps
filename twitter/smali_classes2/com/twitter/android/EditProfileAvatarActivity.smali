.class public Lcom/twitter/android/EditProfileAvatarActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$a;
.implements Lcom/twitter/app/common/dialog/b$d;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/twitter/android/profiles/t;

.field private c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/EditProfileAvatarActivity;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/EditProfileAvatarActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_upload_after_crop"

    .line 69
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 68
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 106
    const-string/jumbo v0, ":"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/EditProfileAvatarActivity;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 108
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/EditProfileAvatarActivity;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 109
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    .line 106
    invoke-static {v0, v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(J[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/EditProfileAvatarActivity;->b:Lcom/twitter/android/profiles/t;

    invoke-static {p1, p2, v0, p3}, Lcom/twitter/android/profiles/v;->a(JLcom/twitter/android/profiles/t;[Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EditProfileAvatarActivity;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->j()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EditProfileAvatarActivity;Lcom/twitter/media/model/MediaFile;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method

.method private a(Lcom/twitter/media/model/MediaFile;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x3

    .line 237
    if-eqz p1, :cond_1

    sget-object v0, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    .line 238
    invoke-static {p1, v0}, Lcom/twitter/model/media/EditableImage;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    move-object v1, v0

    .line 240
    :goto_0
    if-eqz p1, :cond_3

    .line 241
    invoke-static {}, Lbpr;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a06de

    .line 242
    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 244
    :cond_0
    const-string/jumbo v2, "profile"

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;FIZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 247
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "extra_upload_after_crop"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 248
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 249
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 250
    invoke-virtual {p0, v1, v7}, Lcom/twitter/android/EditProfileAvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 258
    :goto_1
    return-void

    :cond_1
    move-object v1, v6

    .line 238
    goto :goto_0

    .line 252
    :cond_2
    invoke-virtual {p0, v0, v7}, Lcom/twitter/android/EditProfileAvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 255
    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->j()V

    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    goto :goto_1
.end method

.method private a(Lcom/twitter/model/media/EditableImage;)V
    .locals 2

    .prologue
    .line 268
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 269
    const-string/jumbo v1, "extra_editable_image"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 270
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->setResult(ILandroid/content/Intent;)V

    .line 271
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    .line 272
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/EditProfileAvatarActivity;Lcom/twitter/media/model/MediaFile;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/android/EditProfileAvatarActivity;->b(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method

.method private b(Lcom/twitter/media/model/MediaFile;)V
    .locals 1

    .prologue
    .line 261
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 264
    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Lcom/twitter/model/media/EditableImage;)V

    .line 265
    return-void

    .line 261
    :cond_0
    sget-object v0, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    .line 263
    invoke-static {p1, v0}, Lcom/twitter/model/media/EditableImage;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lbrv;->a(Landroid/app/Activity;I)Z

    .line 116
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a06c6

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 277
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 77
    invoke-virtual {p0, v0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->overridePendingTransition(II)V

    .line 78
    const v0, 0x7f0400da

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 79
    return-object p2
.end method

.method public a(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    .line 121
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 127
    packed-switch p3, :pswitch_data_0

    .line 149
    :goto_0
    return-void

    .line 129
    :pswitch_0
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    const v1, 0x7f0a06df

    .line 130
    invoke-virtual {p0, v1}, Lcom/twitter/android/EditProfileAvatarActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/EditProfileAvatarActivity;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, p0, v2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v1, "change_avatar_dialog"

    const-string/jumbo v2, "take_photo"

    .line 131
    invoke-direct {p0, v1, v2}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v1, v4, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/EditProfileAvatarActivity;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v5, "change_avatar_dialog"

    const-string/jumbo v6, "take_photo"

    const-string/jumbo v7, "click"

    .line 134
    invoke-static {v4, v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v8

    .line 133
    invoke-direct {p0, v2, v3, v1}, Lcom/twitter/android/EditProfileAvatarActivity;->a(J[Ljava/lang/String;)V

    .line 135
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileAvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 139
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v4, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/EditProfileAvatarActivity;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "change_avatar_dialog"

    const-string/jumbo v5, "choose_photo"

    const-string/jumbo v6, "click"

    .line 140
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    .line 139
    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/EditProfileAvatarActivity;->a(J[Ljava/lang/String;)V

    .line 142
    invoke-direct {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->i()V

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 84
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 85
    new-instance v0, Lcom/twitter/android/profiles/t;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/profiles/t;-><init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Z)V

    iput-object v0, p0, Lcom/twitter/android/EditProfileAvatarActivity;->b:Lcom/twitter/android/profiles/t;

    .line 86
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v1, "edit_profile"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/EditProfileAvatarActivity;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 88
    invoke-static {p0}, Lcom/twitter/android/util/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0b000c

    .line 90
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->c(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 91
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 99
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/EditProfileAvatarActivity;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v5, "change_avatar_dialog"

    const-string/jumbo v6, "choose_photo"

    const-string/jumbo v7, "click"

    .line 96
    invoke-static {v4, v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 95
    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/EditProfileAvatarActivity;->a(J[Ljava/lang/String;)V

    .line 97
    invoke-direct {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->i()V

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 232
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 233
    invoke-virtual {p0, v0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->overridePendingTransition(II)V

    .line 234
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, -0x1

    .line 153
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 154
    packed-switch p1, :pswitch_data_0

    .line 224
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 156
    :pswitch_0
    if-ne p2, v0, :cond_2

    if-eqz p3, :cond_2

    .line 157
    const-string/jumbo v0, "media_file"

    .line 158
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 159
    const-string/jumbo v1, "profile_photo_crop_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 160
    invoke-direct {p0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0

    .line 162
    :cond_1
    invoke-direct {p0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->b(Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0

    .line 164
    :cond_2
    if-nez p2, :cond_3

    invoke-static {}, Lbpr;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 165
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lbrv;->a(Landroid/app/Activity;I)Z

    goto :goto_0

    .line 167
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    goto :goto_0

    .line 172
    :pswitch_1
    if-ne p2, v0, :cond_4

    if-eqz p3, :cond_4

    .line 173
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {p0, v0, v1}, Lcom/twitter/media/model/MediaFile;->b(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/EditProfileAvatarActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/EditProfileAvatarActivity$1;-><init>(Lcom/twitter/android/EditProfileAvatarActivity;)V

    .line 174
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 190
    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Lrx/j;)V

    goto :goto_0

    .line 192
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    goto :goto_0

    .line 197
    :pswitch_2
    if-ne p2, v0, :cond_6

    if-eqz p3, :cond_6

    .line 198
    invoke-static {p3}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Intent;)Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    .line 199
    invoke-static {p3}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->c(Landroid/content/Intent;)Lcom/twitter/media/model/MediaFile;

    move-result-object v1

    .line 200
    if-eqz v0, :cond_5

    .line 201
    invoke-direct {p0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Lcom/twitter/model/media/EditableImage;)V

    goto :goto_0

    .line 202
    :cond_5
    if-eqz v1, :cond_0

    .line 203
    invoke-direct {p0, v1}, Lcom/twitter/android/EditProfileAvatarActivity;->b(Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0

    .line 205
    :cond_6
    if-ne p2, v0, :cond_7

    .line 207
    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileAvatarActivity;->setResult(I)V

    .line 208
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    goto :goto_0

    .line 210
    :cond_7
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    goto :goto_0

    .line 215
    :pswitch_3
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/EditProfileAvatarActivity;->a:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 216
    const/4 v0, 0x0

    invoke-static {p0, v2, v0, v2}, Lcom/twitter/android/media/camera/CameraActivity;->a(Landroid/content/Context;IZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/EditProfileAvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 219
    :cond_8
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    goto/16 :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
