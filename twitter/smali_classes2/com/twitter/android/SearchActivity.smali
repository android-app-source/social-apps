.class public Lcom/twitter/android/SearchActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/twitter/android/ax;
.implements Lcom/twitter/android/bv;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/SearchActivity$a;,
        Lcom/twitter/android/SearchActivity$e;,
        Lcom/twitter/android/SearchActivity$d;,
        Lcom/twitter/android/SearchActivity$b;,
        Lcom/twitter/android/SearchActivity$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
        "Landroid/widget/RadioGroup$OnCheckedChangeListener;",
        "Lcom/twitter/android/ax;",
        "Lcom/twitter/android/bv;"
    }
.end annotation


# static fields
.field private static final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Lcom/twitter/library/widget/SlidingPanel;

.field private B:Landroid/widget/RelativeLayout;

.field private C:Landroid/widget/Switch;

.field private D:Landroid/view/View;

.field private E:Landroid/widget/TextView;

.field private K:Lcom/twitter/internal/android/widget/DockLayout;

.field private L:Landroid/support/v4/view/ViewPager;

.field private M:Lcom/twitter/android/r;

.field private N:Lcom/twitter/android/geo/a;

.field a:Lcom/twitter/android/SearchActivity$e;

.field b:I

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:Lcom/twitter/android/at;

.field k:Lcom/twitter/internal/android/widget/HorizontalListView;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/twitter/util/android/f;

.field private final o:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/twitter/library/provider/t;

.field private q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private r:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 219
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    .line 220
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.USER_SHOW"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.USER_SHOW_SEARCH_SUGGESTION"

    const/4 v2, 0x3

    .line 223
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 222
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_RECENT"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_TYPEAHEAD_TOPIC"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_QUERY_SAVED"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_TREND"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_TAKEOVER"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/List;

    .line 247
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->n:Lcom/twitter/util/android/f;

    .line 248
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Ljava/util/Stack;

    return-void
.end method

.method private a(IZ)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1311
    packed-switch p1, :pswitch_data_0

    .line 1340
    :pswitch_0
    const v0, 0x7f0a07de

    :goto_0
    return v0

    .line 1313
    :pswitch_1
    const v0, 0x7f0a0939

    goto :goto_0

    .line 1317
    :pswitch_2
    if-eqz p2, :cond_0

    const v0, 0x7f0a07df

    goto :goto_0

    :cond_0
    const v0, 0x7f0a07e1

    goto :goto_0

    .line 1321
    :pswitch_3
    const v0, 0x7f0a07e4

    goto :goto_0

    .line 1325
    :pswitch_4
    const v0, 0x7f0a07e3

    goto :goto_0

    .line 1329
    :pswitch_5
    const v0, 0x7f0a0938

    goto :goto_0

    .line 1334
    :pswitch_6
    const v0, 0x7f0a07e2

    goto :goto_0

    .line 1311
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 746
    const-string/jumbo v0, "search_box"

    invoke-static {p1, v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 748
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 749
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 750
    invoke-static {p4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 751
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v1, p5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 753
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;II)Lcom/twitter/library/client/m;
    .locals 1
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;",
            "Lcom/twitter/app/common/base/b;",
            "II)",
            "Lcom/twitter/library/client/m;"
        }
    .end annotation

    .prologue
    .line 1442
    invoke-virtual {p0, p3}, Lcom/twitter/android/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0, p4}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;Ljava/lang/String;I)Lcom/twitter/library/client/m;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;Ljava/lang/String;I)Lcom/twitter/library/client/m;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;",
            "Lcom/twitter/app/common/base/b;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/twitter/library/client/m;"
        }
    .end annotation

    .prologue
    .line 1448
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1449
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "search"

    .line 1450
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1451
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1452
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1453
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1454
    new-instance v1, Lcom/twitter/library/client/m$a;

    invoke-direct {v1, v0, p0}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 1455
    invoke-virtual {v1, p2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1456
    invoke-virtual {v0, p1}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1457
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 1454
    return-object v0
.end method

.method private a(Lcom/twitter/android/dogfood/a;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 926
    invoke-static {p0}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    .line 927
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Thanks for submitting a bad search!\n\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "What (user, tweet, image, etc): \n\n"

    .line 928
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "Expected results: \n\n"

    .line 929
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "Actual results: \n\n\n\n"

    .line 930
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "-------------------------\n\n"

    .line 931
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "Request URL:\n"

    .line 932
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 933
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/SearchFragment;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n\n"

    .line 934
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 935
    invoke-virtual {p1}, Lcom/twitter/android/dogfood/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n\n"

    .line 936
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    .line 937
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 938
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 927
    return-object v0
.end method

.method private static a(ZILandroid/content/Intent;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Lail;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1272
    if-eqz p0, :cond_0

    .line 1273
    new-instance v0, Lail;

    const-string/jumbo v1, "recent"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lail;-><init>(IZ)V

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1304
    :goto_0
    return-object v0

    .line 1275
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1304
    :pswitch_0
    new-instance v0, Lail;

    const-string/jumbo v1, "recent"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lail;-><init>(IZ)V

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1278
    :pswitch_1
    new-instance v0, Lail;

    invoke-direct {v0, v4, v4}, Lail;-><init>(IZ)V

    new-array v1, v5, [Lail;

    new-instance v2, Lail;

    invoke-direct {v2, v5, v5}, Lail;-><init>(IZ)V

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1284
    :pswitch_2
    new-instance v0, Lail;

    invoke-direct {v0, v3, v4}, Lail;-><init>(IZ)V

    new-array v1, v5, [Lail;

    new-instance v2, Lail;

    invoke-direct {v2, v3, v4}, Lail;-><init>(IZ)V

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1291
    :pswitch_3
    new-instance v0, Lail;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v4}, Lail;-><init>(IZ)V

    new-array v1, v5, [Lail;

    new-instance v2, Lail;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v4}, Lail;-><init>(IZ)V

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1298
    :pswitch_4
    new-instance v0, Lail;

    const/16 v1, 0xc

    invoke-direct {v0, v1, v4}, Lail;-><init>(IZ)V

    new-array v1, v5, [Lail;

    new-instance v2, Lail;

    const/16 v3, 0xd

    invoke-direct {v2, v3, v5}, Lail;-><init>(IZ)V

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1275
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private a(Landroid/content/Intent;Z)V
    .locals 18

    .prologue
    .line 504
    const/4 v11, 0x0

    .line 506
    sget-object v2, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 507
    const-string/jumbo v3, "query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 508
    invoke-static {v7}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 678
    :goto_0
    return-void

    .line 513
    :cond_0
    if-eqz v2, :cond_1

    .line 515
    const-string/jumbo v3, "user_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 516
    const-string/jumbo v3, "search_suggestion_position"

    const/4 v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 518
    const-string/jumbo v3, "search_suggestion_id"

    const-wide/16 v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    .line 520
    const-string/jumbo v3, "source_association"

    .line 521
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 522
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    move v3, v11

    .line 595
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 597
    if-nez v2, :cond_6

    .line 598
    const-string/jumbo v2, "search_type"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 599
    const-string/jumbo v4, "q_source"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 600
    const-string/jumbo v4, "q_source"

    const-string/jumbo v5, "typed_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    :cond_2
    :goto_2
    const-string/jumbo v4, "follows"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/SearchActivity;->c:Z

    .line 607
    const-string/jumbo v4, "near"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/SearchActivity;->d:Z

    .line 609
    const-string/jumbo v4, "terminal"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 610
    const-string/jumbo v5, "q_source"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 611
    move-object/from16 v0, p1

    invoke-static {v4, v2, v0}, Lcom/twitter/android/SearchActivity;->a(ZILandroid/content/Intent;)Ljava/util/List;

    move-result-object v13

    .line 613
    const/4 v4, 0x3

    if-ne v2, v4, :cond_7

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_7

    const/4 v2, 0x1

    .line 614
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    .line 615
    invoke-virtual {v4}, Lcom/twitter/internal/android/widget/DockLayout;->c()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0367

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 616
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 617
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v10, v2

    :goto_5
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lail;

    .line 618
    if-eqz v10, :cond_9

    const-class v2, Lcom/twitter/android/SearchPhotosFragment;

    move-object v5, v2

    .line 620
    :goto_6
    iget v15, v9, Lail;->a:I

    .line 621
    iget-boolean v0, v9, Lail;->b:Z

    move/from16 v16, v0

    .line 623
    new-instance v6, Laik;

    const-string/jumbo v2, "follows"

    const/4 v11, 0x0

    .line 624
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    const-string/jumbo v2, "near"

    const/4 v12, 0x0

    .line 625
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    invoke-direct/range {v6 .. v12}, Laik;-><init>(Ljava/lang/String;Ljava/lang/String;Lail;ZZZ)V

    invoke-virtual {v6}, Laik;->hashCode()I

    move-result v6

    .line 626
    invoke-static/range {p1 .. p1}, Lcom/twitter/android/bs$a;->a(Landroid/content/Intent;)Lcom/twitter/android/bs$a;

    move-result-object v2

    const v9, 0x7f0a07db

    .line 627
    invoke-virtual {v2, v9}, Lcom/twitter/android/bs$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/bs$a;

    const v9, 0x7f0a07dc

    .line 628
    invoke-virtual {v2, v9}, Lcom/twitter/android/bs$a;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/bs$a;

    const/4 v9, 0x1

    .line 629
    invoke-virtual {v2, v9}, Lcom/twitter/android/bs$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/bs$a;

    .line 630
    invoke-virtual {v2, v15}, Lcom/twitter/android/bs$a;->a(I)Lcom/twitter/android/bs$a;

    move-result-object v2

    .line 631
    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/twitter/android/bs$a;->a(Z)Lcom/twitter/android/bs$a;

    move-result-object v2

    const-string/jumbo v9, "is_saved"

    .line 632
    invoke-virtual {v2, v9, v3}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/bs$a;

    .line 634
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v9

    const/4 v11, 0x1

    if-gt v9, v11, :cond_3

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/twitter/android/SearchActivity;->z:Z

    if-eqz v9, :cond_4

    .line 635
    :cond_3
    invoke-virtual {v2, v4}, Lcom/twitter/android/bs$a;->e(I)Lcom/twitter/app/common/list/i$a;

    .line 638
    :cond_4
    if-eqz p2, :cond_5

    const-string/jumbo v9, "search_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 639
    sget-object v9, Lcom/twitter/util/y;->a:Ljava/security/SecureRandom;

    invoke-virtual {v9}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v16

    .line 640
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/SearchActivity;->s:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v9, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/SearchActivity;->p:Lcom/twitter/library/provider/t;

    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Lcom/twitter/library/provider/t;->m(J)V

    .line 642
    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/bs$a;->a(J)Lcom/twitter/android/bs$a;

    .line 645
    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/List;

    invoke-virtual {v2}, Lcom/twitter/android/bs$a;->a()Lcom/twitter/android/bs;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v10}, Lcom/twitter/android/SearchActivity;->a(IZ)I

    move-result v11

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v2, v11, v6}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;II)Lcom/twitter/library/client/m;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 649
    const/4 v2, 0x3

    if-ne v15, v2, :cond_a

    if-nez v10, :cond_a

    const/4 v10, 0x1

    goto/16 :goto_5

    .line 524
    :pswitch_0
    const-string/jumbo v4, "go_to_user"

    const-string/jumbo v5, "click"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "screen_name"

    .line 526
    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "association"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchActivity;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 527
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "expanded_search"

    const/4 v4, 0x1

    .line 528
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 525
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 529
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto/16 :goto_0

    .line 533
    :pswitch_1
    const-string/jumbo v4, "typeahead"

    const-string/jumbo v5, "profile_click"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 535
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "screen_name"

    .line 536
    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "association"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchActivity;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 537
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "expanded_search"

    const/4 v4, 0x1

    .line 538
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 535
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 539
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto/16 :goto_0

    .line 543
    :pswitch_2
    const-string/jumbo v4, "user"

    const-string/jumbo v5, "click"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "screen_name"

    .line 545
    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "association"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchActivity;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 546
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "expanded_search"

    const/4 v4, 0x1

    .line 547
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 544
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 548
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto/16 :goto_0

    .line 552
    :pswitch_3
    const-string/jumbo v2, "q_source"

    const-string/jumbo v4, "typed_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "search_box"

    .line 553
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 554
    const/4 v4, 0x0

    const-string/jumbo v5, "search"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v11

    .line 555
    goto/16 :goto_1

    .line 558
    :pswitch_4
    const-string/jumbo v2, "q_source"

    const-string/jumbo v4, "recent_search_click"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead_recent_search"

    .line 560
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 561
    const-string/jumbo v4, "recent"

    const-string/jumbo v5, "search"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v11

    .line 562
    goto/16 :goto_1

    .line 565
    :pswitch_5
    const-string/jumbo v2, "q_source"

    const-string/jumbo v4, "typeahead_click"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead"

    .line 566
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    const-string/jumbo v4, "typeahead"

    const-string/jumbo v5, "search"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V

    move v3, v11

    .line 569
    goto/16 :goto_1

    .line 572
    :pswitch_6
    const-string/jumbo v2, "q_source"

    const-string/jumbo v4, "saved_search_click"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead_saved_search"

    .line 574
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    const-string/jumbo v4, "saved_search"

    const-string/jumbo v5, "search"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 580
    const/4 v2, 0x1

    move v3, v2

    .line 581
    goto/16 :goto_1

    .line 584
    :pswitch_7
    const-string/jumbo v2, "q_source"

    const-string/jumbo v4, "typed_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead_cluster"

    .line 585
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 586
    const-string/jumbo v4, "cluster"

    const-string/jumbo v5, "search"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v11

    .line 587
    goto/16 :goto_1

    .line 603
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 613
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 615
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 618
    :cond_9
    const-class v2, Lcom/twitter/android/SearchResultsFragment;

    move-object v5, v2

    goto/16 :goto_6

    .line 649
    :cond_a
    const/4 v10, 0x0

    goto/16 :goto_5

    .line 654
    :cond_b
    if-eqz p2, :cond_c

    const-string/jumbo v2, "in_back_stack"

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 655
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchActivity;->o:Ljava/util/Stack;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 658
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    if-nez v2, :cond_d

    .line 659
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    new-instance v3, Lcom/twitter/android/ay;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v5, v4}, Lcom/twitter/android/ay;-><init>(Lcom/twitter/android/ax;Lcom/twitter/internal/android/widget/ToolBar;I)V

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/DockLayout;->a(Lcom/twitter/internal/android/widget/DockLayout$a;)V

    .line 661
    new-instance v2, Lcom/twitter/android/at;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/List;

    invoke-direct {v2, v3}, Lcom/twitter/android/at;-><init>(Ljava/util/List;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    .line 663
    const v2, 0x7f13037a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 664
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 665
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    .line 666
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v7, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 667
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 669
    const v2, 0x7f130378

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    .line 670
    new-instance v2, Lcom/twitter/android/SearchActivity$c;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    new-instance v10, Lcom/twitter/android/q;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-direct {v10, v3}, Lcom/twitter/android/q;-><init>(Lcom/twitter/internal/android/widget/DockLayout;)V

    move-object/from16 v3, p0

    move-object/from16 v4, p0

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/SearchActivity$c;-><init>(Lcom/twitter/android/SearchActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;Lcom/twitter/internal/android/widget/DockLayout;Lcom/twitter/android/q;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    .line 672
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    goto/16 :goto_0

    .line 674
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v2}, Lcom/twitter/android/at;->notifyDataSetChanged()V

    .line 675
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    invoke-virtual {v2}, Lcom/twitter/android/r;->notifyDataSetChanged()V

    .line 676
    const-string/jumbo v2, "show_alternate"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/twitter/android/SearchActivity;->d(Z)V

    goto/16 :goto_0

    .line 522
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 3

    .prologue
    .line 764
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 766
    const/16 v1, 0xc

    invoke-static {p7, p8, p5, v1, p6}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 768
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 769
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SearchActivity;Lcom/twitter/android/SearchFragment;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchFragment;)V

    return-void
.end method

.method private a(Lcom/twitter/android/SearchFragment;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 954
    if-nez p1, :cond_0

    .line 1052
    :goto_0
    return-void

    .line 958
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->e()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchActivity;->b:I

    .line 959
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->A()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->c:Z

    .line 960
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->z()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    .line 962
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchActivity$e;

    iget v3, p0, Lcom/twitter/android/SearchActivity;->b:I

    iget-boolean v4, p0, Lcom/twitter/android/SearchActivity;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/SearchActivity$e;->a(IZZ)V

    .line 963
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->D()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->w:Z

    .line 965
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->y()Ljava/lang/String;

    .line 966
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->t()Ljava/lang/String;

    move-result-object v6

    .line 967
    iget v7, p0, Lcom/twitter/android/SearchActivity;->b:I

    .line 968
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->C()Z

    move-result v8

    .line 973
    packed-switch v7, :pswitch_data_0

    .line 999
    :pswitch_0
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->c:Z

    if-eqz v0, :cond_3

    .line 1000
    const v0, 0x7f0a0935

    :goto_1
    move v3, v2

    move v4, v2

    move v5, v0

    .line 1011
    :goto_2
    if-nez v4, :cond_5

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->y:Z

    .line 1012
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v9

    .line 1013
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->g:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x3

    if-ne v7, v0, :cond_6

    move v0, v2

    .line 1015
    :goto_4
    const-string/jumbo v7, "search_alerts_enabled"

    .line 1016
    invoke-static {v7}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    if-eqz v3, :cond_7

    if-nez v8, :cond_7

    move v3, v2

    :goto_5
    iput-boolean v3, p0, Lcom/twitter/android/SearchActivity;->z:Z

    .line 1018
    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->z:Z

    if-nez v3, :cond_8

    if-eqz v4, :cond_8

    if-eqz v8, :cond_1

    if-eqz v0, :cond_8

    :cond_1
    :goto_6
    iput-boolean v2, p0, Lcom/twitter/android/SearchActivity;->i:Z

    .line 1021
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    iget-boolean v2, p0, Lcom/twitter/android/SearchActivity;->i:Z

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setTopVisible(Z)V

    .line 1023
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 1024
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->z:Z

    if-eqz v0, :cond_9

    .line 1025
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1026
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setTopView(Landroid/view/View;)V

    .line 1032
    :cond_2
    :goto_7
    if-eqz v8, :cond_b

    .line 1033
    invoke-virtual {v9, v10}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 1034
    invoke-virtual {p0, v6}, Lcom/twitter/android/SearchActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1035
    if-eqz v5, :cond_a

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->g:Z

    if-nez v0, :cond_a

    .line 1036
    invoke-virtual {p0, v5}, Lcom/twitter/android/SearchActivity;->k(I)V

    .line 1046
    :goto_8
    invoke-virtual {v9, v8}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    .line 1048
    new-instance v0, Lcom/twitter/android/SearchActivity$a;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/SearchActivity$a;-><init>(Lcom/twitter/android/SearchActivity;Lcom/twitter/android/SearchFragment;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/SearchFragment;->a(Lcom/twitter/android/SearchFragment$b;)V

    .line 1050
    iput-boolean v8, p0, Lcom/twitter/android/SearchActivity;->u:Z

    .line 1051
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    goto/16 :goto_0

    .line 975
    :pswitch_1
    const v0, 0x7f0a0939

    move v3, v1

    move v4, v1

    move v5, v0

    .line 977
    goto :goto_2

    .line 980
    :pswitch_2
    const v0, 0x7f0a093b

    move v3, v1

    move v4, v2

    move v5, v0

    .line 981
    goto :goto_2

    .line 985
    :pswitch_3
    const v0, 0x7f0a093d

    move v3, v1

    move v4, v2

    move v5, v0

    .line 986
    goto/16 :goto_2

    .line 990
    :pswitch_4
    const v0, 0x7f0a093a

    move v3, v1

    move v4, v2

    move v5, v0

    .line 991
    goto/16 :goto_2

    .line 994
    :pswitch_5
    const v0, 0x7f0a0938

    move v3, v1

    move v4, v1

    move v5, v0

    .line 996
    goto/16 :goto_2

    .line 1001
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    if-eqz v0, :cond_4

    .line 1002
    const v0, 0x7f0a0937

    goto/16 :goto_1

    :cond_4
    move v0, v1

    .line 1004
    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 1011
    goto/16 :goto_3

    :cond_6
    move v0, v1

    .line 1013
    goto/16 :goto_4

    :cond_7
    move v3, v1

    .line 1016
    goto/16 :goto_5

    :cond_8
    move v2, v1

    .line 1018
    goto :goto_6

    .line 1028
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_7

    .line 1038
    :cond_a
    invoke-virtual {p0, v10}, Lcom/twitter/android/SearchActivity;->b(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 1041
    :cond_b
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1042
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->D:Landroid/view/View;

    invoke-virtual {v9, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 1043
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->E:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1044
    invoke-virtual {p0, v10}, Lcom/twitter/android/SearchActivity;->b(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 973
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 791
    iput-boolean p1, p0, Lcom/twitter/android/SearchActivity;->v:Z

    .line 792
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->u:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/DockLayout;->setTopVisible(Z)V

    .line 793
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 794
    return-void

    .line 792
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/SearchActivity;)Z
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->N()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/SearchActivity;Lcom/twitter/library/service/s;I)Z
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/library/service/s;I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/SearchActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->E:Landroid/widget/TextView;

    return-object v0
.end method

.method private b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 758
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 760
    return-void
.end method

.method private b(Lcom/twitter/android/SearchFragment;)V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1346
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->d()Z

    .line 1348
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->e:Z

    if-eqz v0, :cond_1

    .line 1350
    iget v0, p0, Lcom/twitter/android/SearchActivity;->b:I

    if-ne v0, v7, :cond_2

    move v0, v7

    .line 1351
    :goto_0
    iget v1, p0, Lcom/twitter/android/SearchActivity;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/at;

    invoke-virtual {v1}, Lcom/twitter/android/at;->a()I

    move-result v1

    if-ne v1, v7, :cond_3

    move v4, v7

    .line 1353
    :goto_1
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1354
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->t()Ljava/lang/String;

    move-result-object v9

    .line 1355
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->v()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1356
    new-instance v3, Lail;

    iget v5, p0, Lcom/twitter/android/SearchActivity;->b:I

    invoke-direct {v3, v5, v0}, Lail;-><init>(IZ)V

    .line 1357
    new-instance v0, Laik;

    iget-boolean v5, p0, Lcom/twitter/android/SearchActivity;->c:Z

    iget-boolean v6, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-direct/range {v0 .. v6}, Laik;-><init>(Ljava/lang/String;Ljava/lang/String;Lail;ZZZ)V

    .line 1358
    invoke-virtual {v0}, Laik;->hashCode()I

    move-result v0

    .line 1359
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/twitter/android/SearchActivity;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v5, "query"

    .line 1360
    invoke-virtual {v3, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "query_name"

    .line 1361
    invoke-virtual {v1, v3, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "search_type"

    iget v5, p0, Lcom/twitter/android/SearchActivity;->b:I

    .line 1362
    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "follows"

    iget-boolean v5, p0, Lcom/twitter/android/SearchActivity;->c:Z

    .line 1363
    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "near"

    iget-boolean v5, p0, Lcom/twitter/android/SearchActivity;->d:Z

    .line 1364
    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "terminal"

    iget-boolean v5, p0, Lcom/twitter/android/SearchActivity;->u:Z

    .line 1365
    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "photo_list"

    if-nez v4, :cond_4

    .line 1366
    :goto_2
    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "in_back_stack"

    .line 1367
    invoke-virtual {v1, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "q_source"

    .line 1368
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1370
    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->s:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1371
    if-eqz v0, :cond_0

    .line 1373
    const-string/jumbo v2, "search_id"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1375
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 1376
    iput-boolean v8, p0, Lcom/twitter/android/SearchActivity;->e:Z

    .line 1378
    :cond_1
    return-void

    :cond_2
    move v0, v8

    .line 1350
    goto/16 :goto_0

    :cond_3
    move v4, v8

    .line 1351
    goto/16 :goto_1

    :cond_4
    move v7, v8

    .line 1365
    goto :goto_2
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 942
    if-eqz p1, :cond_0

    .line 943
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchActivity$e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/SearchActivity$e;->a(Z)V

    .line 944
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->b(I)V

    .line 945
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->b()Z

    .line 951
    :goto_0
    return-void

    .line 947
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->a(I)V

    .line 948
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchActivity$e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SearchActivity$e;->a(Z)V

    .line 949
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->a()Z

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/SearchActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/twitter/android/SearchFragment;)V
    .locals 4

    .prologue
    .line 1381
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->e()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchActivity;->b:I

    .line 1382
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->A()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->c:Z

    .line 1383
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->z()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    .line 1384
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchActivity$e;

    iget v1, p0, Lcom/twitter/android/SearchActivity;->b:I

    iget-boolean v2, p0, Lcom/twitter/android/SearchActivity;->c:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/SearchActivity$e;->a(IZZ)V

    .line 1385
    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->C:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->C:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1073
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->C:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 1074
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->C:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1076
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/SearchActivity;)Lcom/twitter/internal/android/widget/DockLayout;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    return-object v0
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 1388
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1389
    return-void

    .line 1388
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/SearchActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/SearchActivity;)Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->z:Z

    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1055
    const v0, 0x7f1307db

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1057
    if-eqz v0, :cond_0

    .line 1060
    :try_start_0
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    .line 1061
    const v0, 0x7f1307e2

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->C:Landroid/widget/Switch;

    .line 1062
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->C:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    :try_end_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1068
    :cond_0
    :goto_0
    return-void

    .line 1063
    :catch_0
    move-exception v0

    .line 1064
    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    .line 1065
    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->C:Landroid/widget/Switch;

    goto :goto_0
.end method

.method private l()Landroid/app/AlertDialog;
    .locals 8

    .prologue
    const/16 v7, 0x1e

    const/16 v6, 0xf

    const/4 v5, 0x0

    .line 1079
    .line 1080
    const-string/jumbo v0, "SpikingHawk"

    .line 1081
    const v0, 0x7f0a0c23

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1082
    const-string/jumbo v1, "SpikingHawk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1083
    const-string/jumbo v2, "SpikingHawk"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    .line 1084
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1085
    new-instance v0, Lcom/twitter/android/SearchActivity$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/SearchActivity$3;-><init>(Lcom/twitter/android/SearchActivity;)V

    .line 1092
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1093
    invoke-virtual {v3, v0, v1, v2, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1094
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1095
    invoke-virtual {v4, v7, v6, v7, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1096
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v4, v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1098
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 1097
    invoke-virtual {v4, v5, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1100
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0c24

    .line 1101
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0c22

    new-instance v2, Lcom/twitter/android/SearchActivity$4;

    invoke-direct {v2, p0}, Lcom/twitter/android/SearchActivity$4;-><init>(Lcom/twitter/android/SearchActivity;)V

    .line 1102
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00be

    const/4 v2, 0x0

    .line 1112
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1113
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1114
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1100
    return-object v0
.end method


# virtual methods
.method protected A_()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 701
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 702
    sget-object v0, Lcom/twitter/android/SearchActivity;->l:Ljava/util/Map;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 703
    if-eqz v0, :cond_0

    .line 704
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 718
    :cond_0
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "in_back_stack"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    :goto_0
    return-object v0

    .line 711
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 704
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/client/m;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    invoke-virtual {v0, p1}, Lcom/twitter/android/r;->c(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 273
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 274
    const v1, 0x7f04039a

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 275
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 276
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 278
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 279
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 280
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 282
    :cond_0
    return-object v0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1409
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1420
    :goto_0
    return-void

    .line 1414
    :cond_0
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1415
    const/4 v1, 0x3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1417
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1418
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1419
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 7

    .prologue
    const/16 v6, 0x64

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1234
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 1235
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1236
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/SearchFragment;->u()Ljava/lang/String;

    move-result-object v3

    .line 1237
    invoke-virtual {p0, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;)Z

    move-result v5

    .line 1238
    if-ne p2, v6, :cond_0

    const v3, 0x7f0a07b9

    move v4, v3

    .line 1240
    :goto_0
    if-ne p2, v6, :cond_1

    const v3, 0x7f0a07ba

    .line 1243
    :goto_1
    packed-switch p2, :pswitch_data_0

    .line 1268
    :goto_2
    return-void

    .line 1238
    :cond_0
    const v3, 0x7f0a0265

    move v4, v3

    goto :goto_0

    .line 1240
    :cond_1
    const v3, 0x7f0a0266

    goto :goto_1

    .line 1246
    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1247
    invoke-static {p0, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 1249
    :cond_2
    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1251
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->z:Z

    if-eqz v0, :cond_3

    .line 1252
    invoke-direct {p0, v5}, Lcom/twitter/android/SearchActivity;->c(Z)V

    goto :goto_2

    .line 1254
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    .line 1255
    const v3, 0x7f1308ce

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v3

    .line 1256
    const v4, 0x7f1308cd

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    .line 1257
    invoke-virtual {v3, v5}, Lazv;->b(Z)Lazv;

    move-result-object v0

    invoke-virtual {v0, v5}, Lazv;->c(Z)Lazv;

    .line 1258
    if-nez v5, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Lazv;->b(Z)Lazv;

    move-result-object v0

    if-nez v5, :cond_5

    :goto_4
    invoke-virtual {v0, v1}, Lazv;->c(Z)Lazv;

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    .line 1243
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(ZLandroid/widget/ListView;Lcom/twitter/android/SearchFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 407
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    if-eq p3, v0, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    if-eqz p1, :cond_2

    .line 411
    invoke-virtual {p0, v1, v4}, Lcom/twitter/android/SearchActivity;->a(ZLjava/lang/String;)Z

    .line 412
    invoke-direct {p0, v1}, Lcom/twitter/android/SearchActivity;->a(Z)V

    .line 413
    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchActivity;->h(Z)V

    .line 414
    invoke-virtual {p2, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 415
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 419
    :cond_2
    invoke-virtual {p0, v2, v4}, Lcom/twitter/android/SearchActivity;->a(ZLjava/lang/String;)Z

    .line 420
    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchActivity;->h(Z)V

    .line 421
    invoke-direct {p0, v2}, Lcom/twitter/android/SearchActivity;->a(Z)V

    .line 422
    invoke-virtual {p2, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 423
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->B:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcmm;)Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 871
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 872
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->u()Ljava/lang/String;

    move-result-object v3

    .line 873
    const-wide/16 v4, 0x0

    .line 874
    invoke-virtual {p0, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 875
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 878
    :cond_0
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 920
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    :goto_0
    return v0

    .line 880
    :sswitch_0
    new-instance v0, Lcom/twitter/library/api/search/b;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/search/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;J)V

    .line 881
    invoke-virtual {v0, v7}, Lcom/twitter/library/api/search/b;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    const/16 v1, 0x64

    .line 880
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 882
    invoke-interface {p1, v7}, Lcmm;->f(Z)Lcmm;

    move-result-object v0

    invoke-interface {v0, v7}, Lcmm;->e(Z)Lcmm;

    .line 883
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal::saved_search:add"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v6

    .line 884
    goto :goto_0

    .line 887
    :sswitch_1
    new-instance v0, Lcom/twitter/library/api/search/b;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/search/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;J)V

    .line 888
    invoke-virtual {v0, v6}, Lcom/twitter/library/api/search/b;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    const/16 v1, 0x65

    .line 887
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 889
    invoke-interface {p1, v7}, Lcmm;->f(Z)Lcmm;

    move-result-object v0

    invoke-interface {v0, v7}, Lcmm;->e(Z)Lcmm;

    .line 890
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal::saved_search:remove"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v6

    .line 891
    goto :goto_0

    .line 894
    :sswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->c(Lcom/twitter/android/SearchFragment;)V

    .line 895
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->t:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->b(Z)V

    .line 896
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal:filter_sheet::impression"

    aput-object v2, v1, v7

    .line 897
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 896
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v6

    .line 898
    goto :goto_0

    .line 901
    :sswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v3, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal::query:share"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v6

    .line 903
    goto/16 :goto_0

    .line 906
    :sswitch_4
    invoke-static {p0}, Lcom/twitter/android/dogfood/a;->a(Landroid/content/Context;)Lcom/twitter/android/dogfood/a;

    move-result-object v0

    .line 907
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0c1c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 908
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Bad search for ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/SearchFragment;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "] from Android"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 909
    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/dogfood/a;)Ljava/lang/String;

    move-result-object v3

    .line 910
    invoke-virtual {v0, v1, v2, v3, v7}, Lcom/twitter/android/dogfood/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/SearchActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/SearchActivity$2;-><init>(Lcom/twitter/android/SearchActivity;)V

    .line 911
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move v0, v6

    .line 917
    goto/16 :goto_0

    .line 878
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f1308bc -> :sswitch_3
        0x7f1308cc -> :sswitch_2
        0x7f1308cd -> :sswitch_0
        0x7f1308ce -> :sswitch_1
        0x7f1308d0 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 780
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    .line 781
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "search_features_bad_search_report_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 783
    const v0, 0x7f14002b

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 785
    :cond_0
    const v0, 0x7f140029

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 786
    const v0, 0x7f14002f

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 787
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1118
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;JLcom/twitter/model/topic/TwitterTopic;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 433
    if-nez p4, :cond_1

    .line 467
    :cond_0
    :goto_0
    return v7

    .line 436
    :cond_1
    const-string/jumbo v0, "search_ui_event_takeover_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    invoke-virtual {p4}, Lcom/twitter/model/topic/TwitterTopic;->b()Ljava/lang/String;

    move-result-object v1

    .line 439
    invoke-virtual {p4}, Lcom/twitter/model/topic/TwitterTopic;->d()Lcom/twitter/model/topic/TwitterTopic$a;

    move-result-object v0

    iget v2, v0, Lcom/twitter/model/topic/TwitterTopic$a;->b:I

    .line 441
    invoke-static {v1, v2}, Lcom/twitter/android/events/a;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p5}, Lcom/twitter/android/events/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    invoke-virtual {p4}, Lcom/twitter/model/topic/TwitterTopic;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {p4}, Lcom/twitter/model/topic/TwitterTopic;->h()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-direct {v8, p4}, Lcom/twitter/android/widget/TopicView$TopicData;-><init>(Lcom/twitter/model/topic/TwitterTopic;)V

    move-object v0, p0

    move-object v4, p1

    .line 446
    invoke-static/range {v0 .. v8}, Lcom/twitter/android/cj;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/android/widget/TopicView$TopicData;)Landroid/content/Intent;

    move-result-object v0

    .line 450
    const-string/jumbo v3, "com.twitter.android.action.SEARCH_TAKEOVER"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "search_takeover"

    .line 451
    invoke-virtual {v3, v4, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "event_page_type"

    .line 452
    invoke-virtual {v3, v4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "search_id"

    .line 453
    invoke-virtual {v3, v4, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "search_src_ref"

    .line 455
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "search_src_ref"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 454
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "com.twitter.android.action.SEARCH_TAKEOVER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 458
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 459
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->finish()V

    .line 460
    const v0, 0x7f050051

    const v3, 0x7f050052

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/SearchActivity;->overridePendingTransition(II)V

    .line 461
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "search::::takeover"

    aput-object v4, v3, v7

    .line 462
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 463
    invoke-static {v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 464
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 461
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v7, v9

    .line 465
    goto/16 :goto_0
.end method

.method public a(ZLjava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 395
    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->u:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->x:Z

    if-nez v1, :cond_3

    .line 396
    iput-boolean p1, p0, Lcom/twitter/android/SearchActivity;->i:Z

    .line 397
    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->i:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->y:Z

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->z:Z

    if-eqz v1, :cond_2

    .line 398
    :cond_1
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->i:Z

    .line 400
    :cond_2
    const/4 v0, 0x1

    .line 402
    :cond_3
    return v0
.end method

.method public b(Lcmr;)I
    .locals 7

    .prologue
    const v6, 0x7f1308cc

    const v5, 0x7f1308bc

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 811
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Lcmr;)I

    .line 812
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 813
    const v1, 0x7f13088d

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    .line 814
    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->v:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->u:Z

    if-nez v1, :cond_4

    .line 815
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->D:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 816
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 820
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->v:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->u:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->w:Z

    if-eqz v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lazv;->b(Z)Lazv;

    .line 821
    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 822
    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->v:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->u:Z

    if-nez v1, :cond_6

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lazv;->b(Z)Lazv;

    .line 826
    :cond_0
    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 827
    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->v:Z

    if-eqz v1, :cond_7

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->u:Z

    if-nez v1, :cond_7

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Lazv;->b(Z)Lazv;

    .line 829
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 830
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v1

    .line 831
    if-eqz v1, :cond_3

    .line 832
    const v4, 0x7f1308ce

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    .line 833
    const v5, 0x7f1308cd

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v5

    .line 834
    invoke-virtual {v1}, Lcom/twitter/android/SearchFragment;->u()Ljava/lang/String;

    move-result-object v0

    .line 835
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;)Z

    move-result v1

    .line 836
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->u:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->v:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "search_alerts_enabled"

    .line 837
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 838
    :cond_2
    invoke-virtual {v4, v3}, Lazv;->b(Z)Lazv;

    .line 839
    invoke-virtual {v5, v3}, Lazv;->b(Z)Lazv;

    .line 840
    invoke-direct {p0, v1}, Lcom/twitter/android/SearchActivity;->c(Z)V

    .line 847
    :cond_3
    :goto_4
    const/4 v0, 0x2

    return v0

    .line 818
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_5
    move v1, v3

    .line 820
    goto :goto_1

    :cond_6
    move v1, v3

    .line 822
    goto :goto_2

    :cond_7
    move v1, v3

    .line 827
    goto :goto_3

    .line 842
    :cond_8
    invoke-virtual {v4, v1}, Lazv;->b(Z)Lazv;

    move-result-object v0

    invoke-virtual {v0, v1}, Lazv;->c(Z)Lazv;

    .line 843
    if-nez v1, :cond_9

    move v0, v2

    :goto_5
    invoke-virtual {v5, v0}, Lazv;->b(Z)Lazv;

    move-result-object v0

    if-nez v1, :cond_a

    :goto_6
    invoke-virtual {v0, v2}, Lazv;->c(Z)Lazv;

    goto :goto_4

    :cond_9
    move v0, v3

    goto :goto_5

    :cond_a
    move v2, v3

    goto :goto_6
.end method

.method public b()V
    .locals 1

    .prologue
    .line 681
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    .line 682
    if-eqz v0, :cond_0

    .line 683
    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->aL()V

    .line 685
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 287
    const v0, 0x7f1302e3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/DockLayout;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->K:Lcom/twitter/internal/android/widget/DockLayout;

    .line 289
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->p:Lcom/twitter/library/provider/t;

    .line 291
    const-string/jumbo v0, "search_alerts_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    invoke-direct {p0}, Lcom/twitter/android/SearchActivity;->j()V

    .line 295
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0400ed

    invoke-virtual {v0, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->D:Landroid/view/View;

    .line 296
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->D:Landroid/view/View;

    const v3, 0x7f130384

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->E:Landroid/widget/TextView;

    .line 297
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->D:Landroid/view/View;

    new-instance v3, Lcom/twitter/android/SearchActivity$1;

    invoke-direct {v3, p0}, Lcom/twitter/android/SearchActivity$1;-><init>(Lcom/twitter/android/SearchActivity;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v3, "search"

    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 306
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v3}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 308
    const v0, 0x7f130640

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SlidingPanel;

    .line 309
    new-instance v3, Lcom/twitter/android/SearchActivity$e;

    invoke-direct {v3, v0, p0}, Lcom/twitter/android/SearchActivity$e;-><init>(Lcom/twitter/library/widget/SlidingPanel;Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 310
    iget-object v4, v3, Lcom/twitter/android/SearchActivity$e;->n:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311
    iget-object v4, v3, Lcom/twitter/android/SearchActivity$e;->o:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 312
    iget-object v4, v3, Lcom/twitter/android/SearchActivity$e;->p:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    iput-object v3, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchActivity$e;

    .line 315
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/SlidingPanel;->a(I)V

    .line 317
    new-instance v4, Lcom/twitter/android/SearchActivity$d;

    invoke-direct {v4, v0, v3}, Lcom/twitter/android/SearchActivity$d;-><init>(Lcom/twitter/library/widget/SlidingPanel;Lcom/twitter/android/SearchActivity$e;)V

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/SlidingPanel;->setPanelSlideListener(Lcom/twitter/library/widget/SlidingUpPanelLayout$c;)V

    .line 318
    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 319
    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    .line 321
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v6, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 322
    iput-boolean v2, p0, Lcom/twitter/android/SearchActivity;->g:Z

    .line 324
    if-nez p1, :cond_1

    .line 325
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    .line 326
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->s:Ljava/util/HashMap;

    .line 327
    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->v:Z

    .line 328
    const-string/jumbo v0, "search"

    invoke-static {p0, v0}, Lcom/twitter/android/al;->b(Landroid/app/Activity;Ljava/lang/String;)V

    .line 348
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-nez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-direct {p0, v3, v0}, Lcom/twitter/android/SearchActivity;->a(Landroid/content/Intent;Z)V

    .line 349
    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->i:Z

    .line 352
    new-instance v0, Lcom/twitter/android/geo/a;

    const-string/jumbo v2, "search_activity_location_dialog"

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->n:Lcom/twitter/util/android/f;

    invoke-direct {v0, p0, v2, v3, v1}, Lcom/twitter/android/geo/a;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Lcom/twitter/util/android/f;I)V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->N:Lcom/twitter/android/geo/a;

    .line 354
    return-void

    .line 330
    :cond_1
    const-string/jumbo v0, "search_saved_queries"

    .line 331
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    .line 332
    const-string/jumbo v0, "search_ids"

    .line 333
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->s:Ljava/util/HashMap;

    .line 334
    const-string/jumbo v0, "filter_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchActivity;->b:I

    .line 335
    const-string/jumbo v0, "filter_following"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->c:Z

    .line 336
    const-string/jumbo v0, "filter_near"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    .line 337
    const-string/jumbo v0, "filter_scope_alt"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->f:Z

    .line 338
    const-string/jumbo v0, "state_panel_maximized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->h:Z

    .line 339
    const-string/jumbo v0, "state_show_toolbar_content"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->v:Z

    .line 341
    const-string/jumbo v0, "backstack"

    .line 342
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 343
    if-eqz v0, :cond_2

    .line 344
    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->o:Ljava/util/Stack;

    invoke-virtual {v3, v0}, Ljava/util/Stack;->addAll(Ljava/util/Collection;)Z

    .line 346
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->p:Lcom/twitter/library/provider/t;

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->s:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 348
    goto :goto_1
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v1, :cond_0

    .line 388
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->p:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->s:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/t;->b(Ljava/util/Collection;)V

    .line 390
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 391
    return-void
.end method

.method public e_()V
    .locals 2

    .prologue
    .line 798
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e_()V

    .line 799
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/internal/android/widget/HorizontalListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    .line 800
    return-void
.end method

.method protected f()[I
    .locals 1

    .prologue
    .line 862
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public i()Lcom/twitter/android/SearchFragment;
    .locals 2

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 1473
    :cond_0
    const/4 v0, 0x0

    .line 1475
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/r;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/library/client/m;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SearchFragment;

    goto :goto_0
.end method

.method public j_()Lcom/twitter/android/AbsPagesAdapter;
    .locals 1

    .prologue
    .line 1430
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->M:Lcom/twitter/android/r;

    return-object v0
.end method

.method protected k()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 852
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->y()Ljava/lang/String;

    move-result-object v0

    .line 853
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x24

    if-ne v1, v2, :cond_1

    .line 854
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 856
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public k_()V
    .locals 2

    .prologue
    .line 804
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->k_()V

    .line 805
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/internal/android/widget/HorizontalListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    .line 806
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 691
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 697
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 695
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1207
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v6

    .line 1208
    if-eqz v6, :cond_2

    .line 1209
    invoke-virtual {v6}, Lcom/twitter/android/SearchFragment;->u()Ljava/lang/String;

    move-result-object v3

    .line 1210
    const-wide/16 v4, 0x0

    .line 1211
    invoke-virtual {p0, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1214
    :cond_0
    new-instance v0, Lcom/twitter/library/api/search/b;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/search/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;J)V

    .line 1215
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 1216
    if-eqz p2, :cond_3

    .line 1218
    invoke-virtual {v6}, Lcom/twitter/android/SearchFragment;->w()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1219
    invoke-direct {p0}, Lcom/twitter/android/SearchActivity;->l()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1221
    :cond_1
    invoke-virtual {v0, v7}, Lcom/twitter/library/api/search/b;->g(I)Lcom/twitter/library/service/s;

    .line 1222
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1223
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal::saved_search:add"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1230
    :cond_2
    :goto_0
    return-void

    .line 1225
    :cond_3
    invoke-virtual {v0, v8}, Lcom/twitter/library/api/search/b;->g(I)Lcom/twitter/library/service/s;

    .line 1226
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1227
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal::saved_search:remove"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1125
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v2

    .line 1126
    if-nez v2, :cond_1

    .line 1178
    :cond_0
    :goto_0
    return-void

    .line 1129
    :cond_1
    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->x:Z

    .line 1131
    invoke-virtual {v2}, Lcom/twitter/android/SearchFragment;->A()Z

    move-result v3

    .line 1132
    invoke-virtual {v2}, Lcom/twitter/android/SearchFragment;->z()Z

    move-result v4

    .line 1134
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getId()I

    move-result v5

    .line 1135
    const v6, 0x7f1307bb

    if-ne v5, v6, :cond_8

    .line 1137
    const v3, 0x7f1307bd

    if-ne p2, v3, :cond_4

    .line 1138
    const/4 v0, 0x3

    .line 1151
    :cond_2
    :goto_1
    iget v3, p0, Lcom/twitter/android/SearchActivity;->b:I

    if-eq v3, v0, :cond_3

    .line 1152
    iput v0, p0, Lcom/twitter/android/SearchActivity;->b:I

    .line 1153
    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->e:Z

    .line 1157
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1158
    invoke-direct {p0, v2}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/android/SearchFragment;)V

    goto :goto_0

    .line 1139
    :cond_4
    const v3, 0x7f1307be

    if-ne p2, v3, :cond_5

    .line 1140
    const/4 v0, 0x5

    goto :goto_1

    .line 1141
    :cond_5
    const v3, 0x7f1307bf

    if-ne p2, v3, :cond_6

    .line 1142
    const/16 v0, 0xc

    goto :goto_1

    .line 1143
    :cond_6
    const v3, 0x7f1307c0

    if-ne p2, v3, :cond_7

    .line 1144
    const/4 v0, 0x6

    goto :goto_1

    .line 1145
    :cond_7
    const v3, 0x7f1307c1

    if-ne p2, v3, :cond_2

    .line 1146
    const/4 v0, 0x2

    goto :goto_1

    .line 1160
    :cond_8
    const v2, 0x7f1307c2

    if-ne v5, v2, :cond_a

    .line 1161
    const v2, 0x7f1307c4

    if-ne p2, v2, :cond_9

    move v0, v1

    .line 1162
    :cond_9
    if-eq v3, v0, :cond_0

    .line 1163
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->c:Z

    .line 1164
    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->e:Z

    goto :goto_0

    .line 1166
    :cond_a
    const v2, 0x7f1307c5

    if-ne v5, v2, :cond_0

    .line 1167
    const v2, 0x7f1307c7

    if-ne p2, v2, :cond_b

    move v0, v1

    .line 1168
    :cond_b
    if-eq v4, v0, :cond_0

    .line 1169
    if-eqz v0, :cond_c

    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v2

    invoke-virtual {v2}, Lbqg;->e()Z

    move-result v2

    if-nez v2, :cond_c

    .line 1170
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->N:Lcom/twitter/android/geo/a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/geo/a;->a(I)V

    goto :goto_0

    .line 1173
    :cond_c
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    .line 1174
    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->e:Z

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 726
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 727
    const v1, 0x7f1307c8

    if-ne v0, v1, :cond_1

    .line 728
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal:filter_sheet:more:click"

    aput-object v2, v1, v4

    .line 729
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 728
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 730
    invoke-direct {p0, v4}, Lcom/twitter/android/SearchActivity;->b(Z)V

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 731
    :cond_1
    const v1, 0x7f1307ca

    if-ne v0, v1, :cond_2

    .line 732
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal:filter_sheet::apply"

    aput-object v2, v1, v4

    .line 733
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 732
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 734
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/android/SearchFragment;)V

    goto :goto_0

    .line 735
    :cond_2
    const v1, 0x7f1307cb

    if-ne v0, v1, :cond_0

    .line 736
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "search:universal:filter_sheet::cancel"

    aput-object v2, v1, v4

    .line 737
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 736
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 738
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->d()Z

    .line 739
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->c(Lcom/twitter/android/SearchFragment;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1393
    new-instance v0, Lcom/twitter/util/android/d;

    sget-object v1, Lcom/twitter/database/schema/a$s;->a:Landroid/net/Uri;

    .line 1394
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 1393
    invoke-static {v1, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/SearchActivity$b;->a:[Ljava/lang/String;

    const-string/jumbo v4, "type=? AND latitude IS NULL AND longitude IS NULL"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v6, 0x6

    .line 1396
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const-string/jumbo v6, "query_id DESC, time ASC"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    return-object v0
.end method

.method public onGlobalLayout()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 480
    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    .line 481
    const v0, 0x7f1307c9

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 482
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 483
    const v4, 0x7f1307ba

    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 484
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 485
    add-int/2addr v4, v0

    .line 486
    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/SlidingPanel;->setPanelPreviewHeight(I)V

    .line 487
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getHeight()I

    move-result v5

    add-int/2addr v0, v4

    if-le v5, v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->t:Z

    .line 490
    invoke-virtual {v3}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 492
    invoke-virtual {v3}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->h:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->t:Z

    if-eqz v0, :cond_2

    .line 494
    :goto_1
    invoke-direct {p0, v1}, Lcom/twitter/android/SearchActivity;->b(Z)V

    .line 495
    invoke-virtual {v3}, Lcom/twitter/library/widget/SlidingPanel;->requestLayout()V

    .line 497
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 487
    goto :goto_0

    :cond_2
    move v1, v2

    .line 493
    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1462
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 1463
    if-ne p3, v0, :cond_0

    .line 1464
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->b()V

    .line 1468
    :goto_0
    return-void

    .line 1466
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->L:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 121
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SearchActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1424
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1425
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 773
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 774
    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchActivity;->setIntent(Landroid/content/Intent;)V

    .line 775
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/SearchActivity;->a(Landroid/content/Intent;Z)V

    .line 776
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1183
    if-ne p1, v2, :cond_1

    .line 1184
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    const-string/jumbo v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/util/android/f;->a(Ljava/lang/String;[Ljava/lang/String;[I)Z

    move-result v0

    .line 1186
    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->d:Z

    if-eq v1, v0, :cond_0

    .line 1187
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    .line 1188
    iput-boolean v2, p0, Lcom/twitter/android/SearchActivity;->e:Z

    .line 1190
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchActivity$e;

    iget v2, p0, Lcom/twitter/android/SearchActivity;->b:I

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/SearchActivity$e;->a(IZZ)V

    .line 1192
    if-nez v0, :cond_1

    .line 1197
    invoke-static {p0}, Lcom/twitter/android/geo/a;->b(Landroid/content/Context;)V

    .line 1198
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqg;->a(Z)V

    .line 1201
    :cond_1
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 382
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 358
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->x:Z

    .line 360
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->i()Lcom/twitter/android/SearchFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchFragment;)V

    .line 361
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 365
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 366
    const-string/jumbo v0, "search_saved_queries"

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->r:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 367
    const-string/jumbo v0, "search_ids"

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->s:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 368
    const-string/jumbo v0, "filter_type"

    iget v1, p0, Lcom/twitter/android/SearchActivity;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 369
    const-string/jumbo v0, "filter_following"

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 370
    const-string/jumbo v0, "filter_near"

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 371
    const-string/jumbo v0, "filter_scope_alt"

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 372
    const-string/jumbo v1, "state_panel_maximized"

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    .line 373
    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 372
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 374
    const-string/jumbo v0, "state_show_toolbar_content"

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 375
    const-string/jumbo v0, "backstack"

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->o:Ljava/util/Stack;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 376
    return-void

    .line 373
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->A:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->d()Z

    .line 475
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->f()Z

    move-result v0

    return v0
.end method
