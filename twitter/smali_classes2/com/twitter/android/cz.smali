.class public Lcom/twitter/android/cz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/cz$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcbi",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:I

.field private d:Lcom/twitter/android/cz$a;

.field private e:J

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/twitter/android/cz;->a:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/twitter/android/cz;->b:Landroid/support/v4/app/LoaderManager;

    .line 42
    iput p3, p0, Lcom/twitter/android/cz;->c:I

    .line 43
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/cz;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/cz;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/cz;->h:Z

    .line 64
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/cz;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/cz;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 68
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/twitter/android/cz;->h:Z

    if-nez v0, :cond_0

    .line 72
    invoke-direct {p0}, Lcom/twitter/android/cz;->b()V

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/cz;->c()V

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/twitter/android/cz;->e:J

    .line 51
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/cz;->d:Lcom/twitter/android/cz$a;

    if-eqz v0, :cond_0

    .line 98
    iget-object v1, p0, Lcom/twitter/android/cz;->d:Lcom/twitter/android/cz$a;

    invoke-static {p2}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-interface {v1, v0}, Lcom/twitter/android/cz$a;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 100
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/cz$a;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/twitter/android/cz;->d:Lcom/twitter/android/cz$a;

    .line 47
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/cz;->g:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/twitter/android/cz;->f:J

    .line 55
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 80
    iget-object v0, p0, Lcom/twitter/android/cz;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "username"

    .line 81
    invoke-static {v2}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " COLLATE NOCASE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/cz;->g:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 85
    :goto_0
    iget-wide v2, p0, Lcom/twitter/android/cz;->e:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    .line 86
    new-instance v2, Lauz$a;

    iget-object v3, p0, Lcom/twitter/android/cz;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lauz$a;-><init>(Landroid/content/Context;Lcom/twitter/database/model/i;)V

    const-class v1, Laxp;

    .line 87
    invoke-virtual {v2, v1}, Lauz$a;->a(Ljava/lang/Class;)Lauz$a;

    move-result-object v1

    const-class v2, Lcom/twitter/model/core/TwitterUser;

    .line 88
    invoke-virtual {v1, v2}, Lauz$a;->b(Ljava/lang/Class;)Lauz$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$z;->a:Landroid/net/Uri;

    .line 89
    invoke-virtual {v1, v2}, Lauz$a;->a(Landroid/net/Uri;)Lauz$a;

    move-result-object v1

    .line 90
    invoke-virtual {v1, v0}, Lauz$a;->a(Lcom/twitter/database/model/f;)Lauz$a;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lauz$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/content/Loader;

    .line 86
    return-object v0

    .line 82
    :cond_0
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v1, "user_id"

    .line 83
    invoke-static {v1}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/twitter/android/cz;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p2, Lcbi;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/cz;->a(Landroid/support/v4/content/Loader;Lcbi;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 104
    return-void
.end method
