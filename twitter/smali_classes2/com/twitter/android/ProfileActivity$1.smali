.class Lcom/twitter/android/ProfileActivity$1;
.super Lcom/twitter/internal/android/widget/DockLayout$c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/android/widget/LoggedOutBar;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/LoggedOutBar;

.field final synthetic b:Lcom/twitter/android/ProfileActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/widget/LoggedOutBar;)V
    .locals 0

    .prologue
    .line 668
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity$1;->b:Lcom/twitter/android/ProfileActivity;

    iput-object p2, p0, Lcom/twitter/android/ProfileActivity$1;->a:Lcom/twitter/android/widget/LoggedOutBar;

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout$c;-><init>()V

    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    .line 689
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$1;->b:Lcom/twitter/android/ProfileActivity;

    invoke-static {v0}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/android/composer/ComposerDockLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerDockLayout;->getBottomDockView()Landroid/view/View;

    move-result-object v0

    .line 690
    if-eqz v0, :cond_0

    .line 691
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    .line 692
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 691
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/twitter/android/ProfileActivity$1;->b(IIII)V

    .line 694
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 671
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity$1;->g()V

    .line 672
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 676
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity$1;->g()V

    .line 677
    return-void
.end method

.method public b(IIII)V
    .locals 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$1;->b:Lcom/twitter/android/ProfileActivity;

    invoke-static {v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/android/composer/ComposerDockLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerDockLayout;->getHeight()I

    move-result v0

    sub-int v0, p4, v0

    .line 684
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity$1;->a:Lcom/twitter/android/widget/LoggedOutBar;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/LoggedOutBar;->setTranslationY(F)V

    .line 685
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity$1;->a:Lcom/twitter/android/widget/LoggedOutBar;

    invoke-virtual {v0}, Lcom/twitter/android/widget/LoggedOutBar;->bringToFront()V

    .line 686
    return-void
.end method
