.class public Lcom/twitter/android/profilecompletionmodule/d;
.super Laol;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/profilecompletionmodule/c;


# instance fields
.field private final a:Lcom/twitter/app/common/base/BaseFragmentActivity;


# direct methods
.method protected constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Laol;-><init>(Landroid/app/Activity;)V

    .line 33
    iput-object p1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 34
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-static {v0}, Lrg;->a(Landroid/content/Context;)V

    .line 108
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-static {v0, p1}, Lbrv;->a(Landroid/app/Activity;I)Z

    .line 66
    return-void
.end method

.method public a(ILcom/twitter/model/media/EditableImage;)V
    .locals 6

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const-string/jumbo v2, "profile"

    const/high16 v3, 0x40400000    # 3.0f

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object v1, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;FIZ)Landroid/content/Intent;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v1, v0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 98
    return-void
.end method

.method public a(IZ)V
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, p2}, Lcom/twitter/android/media/camera/CameraActivity;->a(Landroid/content/Context;IZZ)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 61
    return-void
.end method

.method public a(Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 75
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Lcom/twitter/library/client/n;

    .line 77
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v1

    .line 78
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->b()Lcom/twitter/media/model/MediaFile;

    move-result-object v2

    .line 81
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->c()Ljava/lang/String;

    move-result-object v5

    .line 83
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->e()Ljava/lang/String;

    move-result-object v7

    .line 84
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->f()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/util/collection/k;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v8

    .line 85
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->c()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    const/4 v9, 0x1

    .line 87
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;->d()Lcom/twitter/model/profile/ExtendedProfile;

    move-result-object v11

    move-object v6, v4

    move v10, v3

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/client/n;-><init>(Lcom/twitter/media/model/MediaFile;Lcom/twitter/media/model/MediaFile;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/util/collection/k;ZZLcom/twitter/model/profile/ExtendedProfile;)V

    .line 88
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;)Ljava/lang/String;

    .line 91
    :cond_0
    return-void

    :cond_1
    move v9, v3

    .line 85
    goto :goto_0
.end method

.method public a(Lcom/twitter/app/common/dialog/BaseDialogFragment;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-direct {v0, p1, v1, p2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    .line 53
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v1, v0, p3}, Lcom/twitter/app/common/base/BaseFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 55
    return-void
.end method

.method public a(Lrx/j;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->a(Lrx/j;)V

    .line 103
    return-void
.end method

.method public a(Z)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 38
    if-eqz p1, :cond_0

    .line 39
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 41
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v7, -0x1

    move-object v6, v5

    move-object v8, v5

    move-object v9, v5

    .line 40
    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x24000000

    .line 43
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->finish()V

    .line 47
    return-void
.end method
