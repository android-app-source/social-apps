.class public final Lcom/twitter/android/profilecompletionmodule/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/profilecompletionmodule/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/profilecompletionmodule/b$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private C:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private D:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lanr;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/e;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/addbio/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/addbio/AddBioState;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/addbio/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/addbirthday/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/addbirthday/AddBirthdayState;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/addbirthday/b;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooseavatar/a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooseavatar/ChooseAvatarState;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooseavatar/b;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooseheader/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooseheader/ChooseHeaderState;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooseheader/b;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooselocation/a;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooselocation/ChooseLocationState;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/chooselocation/b;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/profilepreview/b;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/profilecompletionmodule/profilepreview/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/twitter/android/profilecompletionmodule/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/profilecompletionmodule/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/profilecompletionmodule/b$a;)V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    sget-boolean v0, Lcom/twitter/android/profilecompletionmodule/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 112
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/profilecompletionmodule/b;->a(Lcom/twitter/android/profilecompletionmodule/b$a;)V

    .line 113
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/profilecompletionmodule/b$a;Lcom/twitter/android/profilecompletionmodule/b$1;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/twitter/android/profilecompletionmodule/b;-><init>(Lcom/twitter/android/profilecompletionmodule/b$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/android/profilecompletionmodule/b$a;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/profilecompletionmodule/b$a;-><init>(Lcom/twitter/android/profilecompletionmodule/b$1;)V

    return-object v0
.end method

.method private a(Lcom/twitter/android/profilecompletionmodule/b$a;)V
    .locals 3

    .prologue
    .line 122
    .line 125
    invoke-static {p1}, Lcom/twitter/android/profilecompletionmodule/b$a;->a(Lcom/twitter/android/profilecompletionmodule/b$a;)Lano;

    move-result-object v0

    .line 124
    invoke-static {v0}, Lanp;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 123
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->b:Lcta;

    .line 127
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->b:Lcta;

    .line 129
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/q;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->c:Lcta;

    .line 131
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->c:Lcta;

    .line 133
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/g;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 132
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->d:Lcta;

    .line 139
    invoke-static {p1}, Lcom/twitter/android/profilecompletionmodule/b$a;->a(Lcom/twitter/android/profilecompletionmodule/b$a;)Lano;

    move-result-object v0

    .line 138
    invoke-static {v0}, Lanq;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 137
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->e:Lcta;

    .line 141
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->e:Lcta;

    .line 143
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/h;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 142
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->f:Lcta;

    .line 149
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->d:Lcta;

    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/b;->f:Lcta;

    .line 148
    invoke-static {v0, v1, v2}, Lcom/twitter/android/profilecompletionmodule/addbio/c;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 147
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->g:Lcta;

    .line 153
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->g:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->h:Lcta;

    .line 155
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->c:Lcta;

    .line 157
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/i;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 156
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->i:Lcta;

    .line 160
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->e:Lcta;

    .line 162
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/j;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 161
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->j:Lcta;

    .line 168
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->i:Lcta;

    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/b;->j:Lcta;

    .line 167
    invoke-static {v0, v1, v2}, Lcom/twitter/android/profilecompletionmodule/addbirthday/c;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 166
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->k:Lcta;

    .line 172
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->k:Lcta;

    .line 173
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->l:Lcta;

    .line 175
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->c:Lcta;

    .line 177
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 176
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->m:Lcta;

    .line 180
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->e:Lcta;

    .line 182
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/l;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 181
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->n:Lcta;

    .line 188
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->m:Lcta;

    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/b;->n:Lcta;

    .line 187
    invoke-static {v0, v1, v2}, Lcom/twitter/android/profilecompletionmodule/chooseavatar/c;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 186
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->o:Lcta;

    .line 192
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->o:Lcta;

    .line 193
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->p:Lcta;

    .line 195
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->c:Lcta;

    .line 197
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/m;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 196
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->q:Lcta;

    .line 200
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->e:Lcta;

    .line 202
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/n;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 201
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->r:Lcta;

    .line 208
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->q:Lcta;

    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/b;->r:Lcta;

    .line 207
    invoke-static {v0, v1, v2}, Lcom/twitter/android/profilecompletionmodule/chooseheader/c;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 206
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->s:Lcta;

    .line 212
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->s:Lcta;

    .line 213
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->t:Lcta;

    .line 215
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->c:Lcta;

    .line 218
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/o;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 216
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->u:Lcta;

    .line 220
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->e:Lcta;

    .line 222
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/p;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 221
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->v:Lcta;

    .line 228
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->u:Lcta;

    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/b;->v:Lcta;

    .line 227
    invoke-static {v0, v1, v2}, Lcom/twitter/android/profilecompletionmodule/chooselocation/c;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 226
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->w:Lcta;

    .line 232
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->w:Lcta;

    .line 233
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->x:Lcta;

    .line 235
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->c:Lcta;

    .line 238
    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/r;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 236
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->y:Lcta;

    .line 243
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->y:Lcta;

    .line 242
    invoke-static {v0, v1}, Lcom/twitter/android/profilecompletionmodule/profilepreview/d;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 241
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->z:Lcta;

    .line 246
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->z:Lcta;

    .line 247
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->A:Lcta;

    .line 249
    const/4 v0, 0x6

    const/4 v1, 0x0

    .line 250
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->h:Lcta;

    .line 251
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->l:Lcta;

    .line 252
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->p:Lcta;

    .line 253
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->t:Lcta;

    .line 254
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->x:Lcta;

    .line 255
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/b;->A:Lcta;

    .line 256
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->B:Lcta;

    .line 259
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/b$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/profilecompletionmodule/b$1;-><init>(Lcom/twitter/android/profilecompletionmodule/b;Lcom/twitter/android/profilecompletionmodule/b$a;)V

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->C:Lcta;

    .line 272
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->C:Lcta;

    .line 274
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 273
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->D:Lcta;

    .line 275
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->B:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->D:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/android/profilecompletionmodule/addbio/b;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->g:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/addbio/b;

    return-object v0
.end method

.method public e()Lcom/twitter/android/profilecompletionmodule/addbirthday/b;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->k:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/addbirthday/b;

    return-object v0
.end method

.method public f()Lcom/twitter/android/profilecompletionmodule/chooseavatar/b;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->o:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/chooseavatar/b;

    return-object v0
.end method

.method public g()Lcom/twitter/android/profilecompletionmodule/chooseheader/b;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->s:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/chooseheader/b;

    return-object v0
.end method

.method public h()Lcom/twitter/android/profilecompletionmodule/chooselocation/b;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->w:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/chooselocation/b;

    return-object v0
.end method

.method public i()Lcom/twitter/android/profilecompletionmodule/profilepreview/c;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/b;->z:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/profilepreview/c;

    return-object v0
.end method
