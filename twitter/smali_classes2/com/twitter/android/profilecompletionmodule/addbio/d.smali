.class public Lcom/twitter/android/profilecompletionmodule/addbio/d;
.super Lbac;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/profilecompletionmodule/addbio/d$b;,
        Lcom/twitter/android/profilecompletionmodule/addbio/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbac",
        "<",
        "Lcom/twitter/android/profilecompletionmodule/addbio/d$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/profilecompletionmodule/addbio/d$a;

.field private final b:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/profilecompletionmodule/addbio/d$a;Lcbi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/profilecompletionmodule/addbio/d$a;",
            "Lcbi",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Lbac;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/profilecompletionmodule/addbio/d;->a:Lcom/twitter/android/profilecompletionmodule/addbio/d$a;

    .line 23
    iput-object p2, p0, Lcom/twitter/android/profilecompletionmodule/addbio/d;->b:Lcbi;

    .line 24
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/profilecompletionmodule/addbio/d;)Lcom/twitter/android/profilecompletionmodule/addbio/d$a;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/addbio/d;->a:Lcom/twitter/android/profilecompletionmodule/addbio/d$a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/addbio/d;->b:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    return v0
.end method

.method public synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/profilecompletionmodule/addbio/d;->b(Landroid/view/ViewGroup;I)Lcom/twitter/android/profilecompletionmodule/addbio/d$b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/twitter/android/profilecompletionmodule/addbio/d$b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/profilecompletionmodule/addbio/d;->a(Lcom/twitter/android/profilecompletionmodule/addbio/d$b;I)V

    return-void
.end method

.method public a(Lcom/twitter/android/profilecompletionmodule/addbio/d$b;I)V
    .locals 3

    .prologue
    .line 36
    invoke-virtual {p0, p2}, Lcom/twitter/android/profilecompletionmodule/addbio/d;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 37
    iget-object v1, p1, Lcom/twitter/android/profilecompletionmodule/addbio/d$b;->a:Lcom/twitter/internal/android/widget/PillToggleButton;

    .line 38
    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/PillToggleButton;->setText(Ljava/lang/CharSequence;)V

    .line 39
    new-instance v2, Lcom/twitter/android/profilecompletionmodule/addbio/d$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/profilecompletionmodule/addbio/d$1;-><init>(Lcom/twitter/android/profilecompletionmodule/addbio/d;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/PillToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    return-void
.end method

.method public b(Landroid/view/ViewGroup;I)Lcom/twitter/android/profilecompletionmodule/addbio/d$b;
    .locals 5

    .prologue
    .line 29
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/addbio/d$b;

    new-instance v1, Lcom/twitter/internal/android/widget/PillToggleButton;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    .line 31
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0d02d7

    invoke-direct {v2, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v2}, Lcom/twitter/internal/android/widget/PillToggleButton;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/twitter/android/profilecompletionmodule/addbio/d$b;-><init>(Lcom/twitter/internal/android/widget/PillToggleButton;)V

    .line 29
    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/addbio/d;->b:Lcbi;

    invoke-virtual {v0, p1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
