.class public Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;
.super Lcom/twitter/android/twitterflows/TwitterFlowsActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# instance fields
.field private final a:Lcom/twitter/android/profilecompletionmodule/c;

.field private final b:Lcom/twitter/android/twitterflows/b;

.field private c:Lcom/twitter/android/profilecompletionmodule/a;

.field private d:Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;

.field private e:Landroid/widget/FrameLayout;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/profilecompletionmodule/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;-><init>()V

    .line 49
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/d;

    invoke-direct {v0, p0}, Lcom/twitter/android/profilecompletionmodule/d;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a:Lcom/twitter/android/profilecompletionmodule/c;

    .line 50
    new-instance v0, Lcom/twitter/android/twitterflows/c;

    invoke-direct {v0}, Lcom/twitter/android/twitterflows/c;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->b:Lcom/twitter/android/twitterflows/b;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "intent_extra_scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_0

    .line 200
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 202
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/profilecompletionmodule/a;->a(Landroid/content/DialogInterface;II)V

    .line 217
    :cond_0
    return-void
.end method

.method public a(Lflow/Flow$c;Lflow/Flow$d;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 147
    iget-object v1, p1, Lflow/Flow$c;->a:Lflow/b;

    .line 148
    const/4 v0, 0x0

    .line 149
    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 150
    if-eqz v1, :cond_6

    .line 151
    invoke-virtual {v1}, Lflow/b;->e()Lflow/f;

    move-result-object v0

    .line 152
    iget-object v1, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v3, Lflow/Flow$Direction;->b:Lflow/Flow$Direction;

    if-eq v1, v3, :cond_0

    iget-object v1, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v3, Lflow/Flow$Direction;->c:Lflow/Flow$Direction;

    if-ne v1, v3, :cond_1

    .line 153
    :cond_0
    if-eqz v2, :cond_1

    .line 154
    invoke-interface {v0, v2}, Lflow/f;->a(Landroid/view/View;)V

    :cond_1
    move-object v4, v0

    .line 159
    :goto_0
    iget-object v0, p1, Lflow/Flow$c;->b:Lflow/b;

    .line 160
    invoke-virtual {v0}, Lflow/b;->e()Lflow/f;

    move-result-object v5

    .line 161
    invoke-virtual {v0}, Lflow/b;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/twitterflows/g;

    .line 162
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->f:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/twitter/android/twitterflows/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/profilecompletionmodule/a;

    iput-object v1, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    .line 163
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    invoke-virtual {v0}, Lcom/twitter/android/twitterflows/g;->c()I

    move-result v1

    .line 168
    if-lez v1, :cond_3

    .line 169
    new-instance v3, Landroid/support/v7/view/ContextThemeWrapper;

    invoke-direct {v3, p0, v1}, Landroid/support/v7/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 170
    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 175
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/android/twitterflows/g;->a()I

    move-result v0

    iget-object v3, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 176
    check-cast v0, Lcom/twitter/android/profilecompletionmodule/BaseProfileStepScreen;

    .line 178
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/profilecompletionmodule/BaseProfileStepScreen;->setPresenter(Lcom/twitter/android/profilecompletionmodule/a;)V

    .line 180
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/profilecompletionmodule/a;->a(Lcom/twitter/android/profilecompletionmodule/BaseProfileStepScreen;)V

    .line 181
    iget-object v0, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v1, Lflow/Flow$Direction;->c:Lflow/Flow$Direction;

    if-ne v0, v1, :cond_4

    if-eqz v4, :cond_4

    .line 183
    invoke-interface {v4, v3}, Lflow/f;->b(Landroid/view/View;)V

    .line 194
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->e:Landroid/widget/FrameLayout;

    iget-object v4, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    move-object v0, p0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Flow$Direction;Lflow/Flow$d;)V

    .line 195
    return-void

    .line 172
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    goto :goto_1

    .line 184
    :cond_4
    iget-object v0, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v1, Lflow/Flow$Direction;->a:Lflow/Flow$Direction;

    if-ne v0, v1, :cond_5

    .line 185
    invoke-interface {v5, v3}, Lflow/f;->b(Landroid/view/View;)V

    .line 186
    invoke-direct {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->i()V

    goto :goto_2

    .line 187
    :cond_5
    iget-object v0, p1, Lflow/Flow$c;->c:Lflow/Flow$Direction;

    sget-object v1, Lflow/Flow$Direction;->b:Lflow/Flow$Direction;

    if-ne v0, v1, :cond_2

    .line 191
    invoke-direct {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->i()V

    goto :goto_2

    :cond_6
    move-object v4, v0

    goto :goto_0
.end method

.method protected b()Lcom/twitter/android/twitterflows/d;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/u$c;

    invoke-direct {v0}, Lcom/twitter/android/profilecompletionmodule/u$c;-><init>()V

    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 63
    invoke-super {p0, p1, p2}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 64
    if-eqz p1, :cond_0

    .line 65
    const-string/jumbo v0, "pending_changes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->d:Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;

    .line 70
    :goto_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 71
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 72
    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "intent_extra_scribe_page"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 74
    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->W()Lanh;

    move-result-object v6

    .line 75
    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/s;

    .line 76
    const/4 v7, 0x6

    new-array v7, v7, [Lcom/twitter/android/profilecompletionmodule/a;

    .line 77
    invoke-interface {v0}, Lcom/twitter/android/profilecompletionmodule/s;->f()Lcom/twitter/android/profilecompletionmodule/chooseavatar/b;

    move-result-object v8

    aput-object v8, v7, v1

    .line 78
    invoke-interface {v0}, Lcom/twitter/android/profilecompletionmodule/s;->g()Lcom/twitter/android/profilecompletionmodule/chooseheader/b;

    move-result-object v8

    aput-object v8, v7, v11

    const/4 v8, 0x2

    .line 79
    invoke-interface {v0}, Lcom/twitter/android/profilecompletionmodule/s;->d()Lcom/twitter/android/profilecompletionmodule/addbio/b;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    .line 80
    invoke-interface {v0}, Lcom/twitter/android/profilecompletionmodule/s;->e()Lcom/twitter/android/profilecompletionmodule/addbirthday/b;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    .line 81
    invoke-interface {v0}, Lcom/twitter/android/profilecompletionmodule/s;->h()Lcom/twitter/android/profilecompletionmodule/chooselocation/b;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x5

    .line 82
    invoke-interface {v0}, Lcom/twitter/android/profilecompletionmodule/s;->i()Lcom/twitter/android/profilecompletionmodule/profilepreview/c;

    move-result-object v0

    aput-object v0, v7, v8

    .line 84
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v8

    .line 85
    array-length v9, v7

    move v0, v1

    :goto_1
    if-ge v0, v9, :cond_1

    aget-object v1, v7, v0

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/twitter/android/profilecompletionmodule/a;->a(Landroid/content/Context;)V

    .line 87
    iget-object v10, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a:Lcom/twitter/android/profilecompletionmodule/c;

    invoke-virtual {v1, v10}, Lcom/twitter/android/profilecompletionmodule/a;->a(Lcom/twitter/android/profilecompletionmodule/c;)V

    .line 88
    iget-object v10, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->d:Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;

    invoke-virtual {v1, v10}, Lcom/twitter/android/profilecompletionmodule/a;->a(Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;)V

    .line 89
    invoke-virtual {v1, v3}, Lcom/twitter/android/profilecompletionmodule/a;->a(Ljava/lang/String;)V

    .line 90
    invoke-virtual {v1, v4, v5}, Lcom/twitter/android/profilecompletionmodule/a;->a(J)V

    .line 91
    iget-object v10, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->b:Lcom/twitter/android/twitterflows/b;

    invoke-virtual {v1, v10}, Lcom/twitter/android/profilecompletionmodule/a;->a(Lcom/twitter/android/twitterflows/b;)V

    .line 92
    invoke-virtual {v6, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 93
    invoke-virtual {v1}, Lcom/twitter/android/profilecompletionmodule/a;->al_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10, v1}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 67
    :cond_0
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;

    invoke-direct {v0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->d:Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;

    goto/16 :goto_0

    .line 95
    :cond_1
    invoke-virtual {v8}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->f:Ljava/util/Map;

    .line 97
    const v0, 0x7f0402da

    invoke-virtual {p0, v0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->setContentView(I)V

    .line 98
    const v0, 0x7f1306a7

    invoke-virtual {p0, v0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->e:Landroid/widget/FrameLayout;

    .line 102
    new-instance v0, Lcom/twitter/util/a;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 103
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "profile_overlay"

    .line 104
    invoke-virtual {v0, v1, v11}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 107
    invoke-virtual {p0, p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a(Landroid/os/Bundle;)V

    .line 108
    return-void
.end method

.method protected d(Lank;)Lcom/twitter/android/profilecompletionmodule/s;
    .locals 6

    .prologue
    .line 113
    invoke-static {}, Lalg;->ag()Lalg;

    move-result-object v0

    invoke-virtual {v0}, Lalg;->X()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 114
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 115
    new-instance v2, Lcom/twitter/util/a;

    invoke-virtual {p0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 116
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 117
    invoke-static {v2}, Lta;->a(Lcom/twitter/util/a;)Ljava/util/List;

    move-result-object v1

    .line 118
    invoke-static {}, Lcom/twitter/android/profilecompletionmodule/b;->a()Lcom/twitter/android/profilecompletionmodule/b$a;

    move-result-object v2

    .line 119
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/profilecompletionmodule/b$a;->a(Lamu;)Lcom/twitter/android/profilecompletionmodule/b$a;

    move-result-object v2

    new-instance v3, Lano;

    new-instance v4, Lcom/twitter/android/profilecompletionmodule/e;

    invoke-direct {v4, v0, v1}, Lcom/twitter/android/profilecompletionmodule/e;-><init>(Lcom/twitter/model/core/TwitterUser;Ljava/util/List;)V

    invoke-direct {v3, p1, v4}, Lano;-><init>(Lank;Lanr;)V

    .line 120
    invoke-virtual {v2, v3}, Lcom/twitter/android/profilecompletionmodule/b$a;->a(Lano;)Lcom/twitter/android/profilecompletionmodule/b$a;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/twitter/android/profilecompletionmodule/b$a;->a()Lcom/twitter/android/profilecompletionmodule/s;

    move-result-object v0

    .line 118
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->d(Lank;)Lcom/twitter/android/profilecompletionmodule/s;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->d(Lank;)Lcom/twitter/android/profilecompletionmodule/s;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->d(Lank;)Lcom/twitter/android/profilecompletionmodule/s;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/profilecompletionmodule/a;->a(IILandroid/content/Intent;)V

    .line 210
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->c:Lcom/twitter/android/profilecompletionmodule/a;

    invoke-virtual {v0}, Lcom/twitter/android/profilecompletionmodule/a;->q()V

    .line 142
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->onBackPressed()V

    .line 143
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/twitter/android/twitterflows/TwitterFlowsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 134
    const-string/jumbo v0, "pending_changes"

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->d:Lcom/twitter/android/profilecompletionmodule/ProfileCompletionModulePendingChanges;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 135
    return-void
.end method
