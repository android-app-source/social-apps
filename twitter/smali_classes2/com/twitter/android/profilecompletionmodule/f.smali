.class public abstract Lcom/twitter/android/profilecompletionmodule/f;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Lank;)Lcom/twitter/android/profilecompletionmodule/chooseavatar/ChooseAvatarState;
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "presenter_choose_avatar"

    invoke-virtual {p0, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/chooseavatar/ChooseAvatarState;

    return-object v0
.end method

.method static a(Lcom/twitter/android/profilecompletionmodule/e;)Lcom/twitter/android/profilecompletionmodule/chooseavatar/a;
    .locals 2

    .prologue
    .line 57
    new-instance v1, Lcom/twitter/android/profilecompletionmodule/chooseavatar/a;

    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, v0}, Lcom/twitter/android/profilecompletionmodule/chooseavatar/a;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method static a(Lanr;)Lcom/twitter/android/profilecompletionmodule/e;
    .locals 1

    .prologue
    .line 49
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/e;

    return-object v0
.end method

.method static b(Lank;)Lcom/twitter/android/profilecompletionmodule/chooseheader/ChooseHeaderState;
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, "presenter_choose_header"

    invoke-virtual {p0, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/chooseheader/ChooseHeaderState;

    return-object v0
.end method

.method static b(Lcom/twitter/android/profilecompletionmodule/e;)Lcom/twitter/android/profilecompletionmodule/chooseheader/a;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/chooseheader/a;

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v0, v1}, Lcom/twitter/android/profilecompletionmodule/chooseheader/a;-><init>(Lcom/twitter/model/core/TwitterUser;)V

    return-object v0
.end method

.method static c(Lank;)Lcom/twitter/android/profilecompletionmodule/addbio/AddBioState;
    .locals 1

    .prologue
    .line 96
    const-string/jumbo v0, "presenter_add_bio"

    invoke-virtual {p0, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/addbio/AddBioState;

    return-object v0
.end method

.method static c(Lcom/twitter/android/profilecompletionmodule/e;)Lcom/twitter/android/profilecompletionmodule/addbio/a;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/addbio/a;

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/e;->b:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/profilecompletionmodule/addbio/a;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method static d(Lank;)Lcom/twitter/android/profilecompletionmodule/addbirthday/AddBirthdayState;
    .locals 1

    .prologue
    .line 113
    const-string/jumbo v0, "presenter_add_birthday"

    invoke-virtual {p0, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/addbirthday/AddBirthdayState;

    return-object v0
.end method

.method static d(Lcom/twitter/android/profilecompletionmodule/e;)Lcom/twitter/android/profilecompletionmodule/addbirthday/a;
    .locals 5

    .prologue
    .line 104
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/addbirthday/a;

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    iget-object v2, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->S:J

    iget-object v4, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v4, v4, Lcom/twitter/model/core/TwitterUser;->m:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/profilecompletionmodule/addbirthday/a;-><init>(Lcom/twitter/model/profile/ExtendedProfile;JZ)V

    return-object v0
.end method

.method static e(Lank;)Lcom/twitter/android/profilecompletionmodule/chooselocation/ChooseLocationState;
    .locals 1

    .prologue
    .line 131
    const-string/jumbo v0, "presenter_choose_location"

    invoke-virtual {p0, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profilecompletionmodule/chooselocation/ChooseLocationState;

    return-object v0
.end method

.method static e(Lcom/twitter/android/profilecompletionmodule/e;)Lcom/twitter/android/profilecompletionmodule/chooselocation/a;
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Lcom/twitter/util/collection/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    .line 122
    new-instance v1, Lcom/twitter/android/profilecompletionmodule/chooselocation/a;

    new-instance v2, Lcom/twitter/android/LocationState;

    invoke-direct {v2, v0, v0}, Lcom/twitter/android/LocationState;-><init>(Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/model/geo/TwitterPlace;)V

    iget-object v0, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/profilecompletionmodule/chooselocation/a;-><init>(Lcom/twitter/android/LocationState;Lcom/twitter/model/core/TwitterUser;)V

    return-object v1
.end method

.method static f(Lcom/twitter/android/profilecompletionmodule/e;)Lcom/twitter/android/profilecompletionmodule/profilepreview/b;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/twitter/android/profilecompletionmodule/profilepreview/b;

    iget-object v1, p0, Lcom/twitter/android/profilecompletionmodule/e;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v0, v1}, Lcom/twitter/android/profilecompletionmodule/profilepreview/b;-><init>(Lcom/twitter/model/core/TwitterUser;)V

    return-object v0
.end method
