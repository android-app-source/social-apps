.class Lcom/twitter/android/SearchResultsFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/SearchResultsFragment;->K()Laoy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/j",
        "<",
        "Lcom/twitter/util/android/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/SearchResultsFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/SearchResultsFragment;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/twitter/android/SearchResultsFragment$1;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/util/android/d;
    .locals 10

    .prologue
    .line 411
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment$1;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-virtual {v1}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$t;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment$1;->a:Lcom/twitter/android/SearchResultsFragment;

    .line 412
    invoke-static {v3}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/SearchResultsFragment;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lbtv;->a:[Ljava/lang/String;

    const-string/jumbo v4, "search_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/SearchResultsFragment$1;->a:Lcom/twitter/android/SearchResultsFragment;

    iget-wide v8, v7, Lcom/twitter/android/SearchResultsFragment;->r:J

    .line 414
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string/jumbo v6, "type_id ASC, _id ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment$1;->a()Lcom/twitter/util/android/d;

    move-result-object v0

    return-object v0
.end method
