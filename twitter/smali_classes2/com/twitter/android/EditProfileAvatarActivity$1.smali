.class Lcom/twitter/android/EditProfileAvatarActivity$1;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/EditProfileAvatarActivity;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<",
        "Lcom/twitter/media/model/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/EditProfileAvatarActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/EditProfileAvatarActivity;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/twitter/android/EditProfileAvatarActivity$1;->a:Lcom/twitter/android/EditProfileAvatarActivity;

    invoke-direct {p0}, Lcqy;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/model/MediaFile;)V
    .locals 1

    .prologue
    .line 177
    if-eqz p1, :cond_1

    .line 178
    const-string/jumbo v0, "profile_photo_crop_enabled"

    .line 179
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/twitter/android/EditProfileAvatarActivity$1;->a:Lcom/twitter/android/EditProfileAvatarActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Lcom/twitter/android/EditProfileAvatarActivity;Lcom/twitter/media/model/MediaFile;)V

    .line 188
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/EditProfileAvatarActivity$1;->a:Lcom/twitter/android/EditProfileAvatarActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditProfileAvatarActivity;->b(Lcom/twitter/android/EditProfileAvatarActivity;Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/EditProfileAvatarActivity$1;->a:Lcom/twitter/android/EditProfileAvatarActivity;

    invoke-static {v0}, Lcom/twitter/android/EditProfileAvatarActivity;->a(Lcom/twitter/android/EditProfileAvatarActivity;)V

    .line 186
    iget-object v0, p0, Lcom/twitter/android/EditProfileAvatarActivity$1;->a:Lcom/twitter/android/EditProfileAvatarActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditProfileAvatarActivity;->finish()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 174
    check-cast p1, Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/EditProfileAvatarActivity$1;->a(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method
