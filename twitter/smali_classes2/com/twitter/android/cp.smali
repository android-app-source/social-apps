.class public Lcom/twitter/android/cp;
.super Lcom/twitter/android/ah;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/ah",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/content/Context;

.field private final h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final i:Lcom/twitter/library/scribe/n;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/client/v;Lcom/twitter/library/scribe/n;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0, p4}, Lcom/twitter/android/ah;-><init>(Lcom/twitter/library/client/v;)V

    .line 58
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cp;->b:Ljava/util/List;

    .line 66
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cp;->c:Ljava/util/List;

    .line 73
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cp;->d:Ljava/util/List;

    .line 81
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cp;->e:Ljava/util/Set;

    .line 86
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cp;->f:Ljava/util/Set;

    .line 94
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/cp;->j:I

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cp;->g:Landroid/content/Context;

    .line 102
    iput-object p2, p0, Lcom/twitter/android/cp;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 103
    iput p3, p0, Lcom/twitter/android/cp;->j:I

    .line 104
    iput-object p5, p0, Lcom/twitter/android/cp;->i:Lcom/twitter/library/scribe/n;

    .line 105
    return-void
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const-string/jumbo v0, "focal"

    .line 222
    :goto_0
    return-object v0

    .line 219
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220
    const-string/jumbo v0, "ancestor"

    goto :goto_0

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/timeline/ap;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Z
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 255
    invoke-direct {p0, p2, p3}, Lcom/twitter/android/cp;->c(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    iget-object v2, p1, Lcom/twitter/android/timeline/ap;->b:Lcom/twitter/model/timeline/o;

    .line 261
    iget-object v0, v2, Lcom/twitter/model/timeline/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v2, Lcom/twitter/model/timeline/o;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-wide v10, p2, Lcom/twitter/model/core/Tweet;->t:J

    cmp-long v0, v8, v10

    if-nez v0, :cond_2

    .line 264
    iget-object v0, v2, Lcom/twitter/model/timeline/o;->d:Lcom/twitter/model/timeline/r;

    .line 265
    if-eqz v0, :cond_1

    iget-object v5, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 266
    :goto_0
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v8

    new-instance v0, Lcom/twitter/android/timeline/bf;

    iget-object v1, p0, Lcom/twitter/android/cp;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/cp;->a:Lcom/twitter/library/client/v;

    .line 267
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/cp;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v7, p0, Lcom/twitter/android/cp;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v7, :cond_0

    iget-object v4, p0, Lcom/twitter/android/cp;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 268
    invoke-virtual {v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v4

    .line 269
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/timeline/ap;->h()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/timeline/bf;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 266
    invoke-virtual {v8, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 273
    :goto_1
    return v6

    :cond_1
    move-object v5, v4

    .line 265
    goto :goto_0

    :cond_2
    move v6, v1

    .line 273
    goto :goto_1
.end method

.method private b(JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/android/cp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v1

    .line 112
    sget-object v0, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v0}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v2

    .line 113
    iget-object v0, p0, Lcom/twitter/android/cp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 114
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 115
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 116
    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 117
    const-string/jumbo v5, "app_download_client_event"

    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 118
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 119
    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/util/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    const-string/jumbo v5, "3"

    invoke-virtual {v4, v5, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 123
    const-string/jumbo v0, "4"

    invoke-virtual {v4, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 127
    :cond_1
    if-eqz v2, :cond_2

    .line 128
    const-string/jumbo v0, "6"

    .line 129
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v5

    .line 128
    invoke-virtual {v4, v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 130
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 132
    :cond_2
    invoke-static {v4}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    .line 134
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/cp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method private c(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Z
    .locals 6

    .prologue
    .line 161
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->e:Z

    if-nez v0, :cond_2

    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->O:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/cp;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/twitter/android/cp;->b:Ljava/util/List;

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->O:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/cp;->d(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    .line 165
    const/4 v0, 0x1

    .line 172
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    .line 173
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/android/cp;->e:Ljava/util/Set;

    iget-object v3, v1, Lcgi;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 174
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v2, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v2

    .line 177
    const-string/jumbo v3, "ad_formats_slot_details_promoted_event_enabled"

    invoke-static {v3}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 178
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lbsq$a;->a(J)Lbsq$a;

    .line 179
    const-string/jumbo v3, "ad_slot_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 180
    if-eqz v3, :cond_0

    .line 181
    invoke-static {v3}, Lbsm;->a(Ljava/lang/String;)Lbsm;

    move-result-object v3

    invoke-static {v3}, Lbsr;->a(Lbsr$a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbsq$a;->e(Ljava/lang/String;)Lbsq$a;

    .line 185
    :cond_0
    invoke-virtual {v2}, Lbsq$a;->a()Lbsq;

    move-result-object v2

    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 186
    iget-object v2, p0, Lcom/twitter/android/cp;->e:Ljava/util/Set;

    iget-object v1, v1, Lcgi;->c:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_1
    return v0

    .line 167
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 193
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/cp;->a(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lcom/twitter/android/cp;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    iget v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 196
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/twitter/android/cp;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lcom/twitter/android/cp;->i:Lcom/twitter/library/scribe/n;

    iget-object v1, p0, Lcom/twitter/android/cp;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/cp;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 204
    invoke-static {p1}, Lcom/twitter/android/cp;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    .line 203
    invoke-interface {v0, v1, p1, v2, v3}, Lcom/twitter/library/scribe/n;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 205
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 206
    const-string/jumbo v0, "position"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 207
    const-string/jumbo v0, "ad_slot_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ad_formats_slot_details_client_event_enabled"

    .line 208
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Lcom/twitter/analytics/feature/model/b$b;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/b$b;-><init>()V

    const-string/jumbo v2, "ad_slot_id"

    .line 210
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/b$b;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/b$b;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/b$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/b;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ap:Lcom/twitter/analytics/feature/model/b;

    .line 213
    :cond_0
    return-object v1
.end method

.method public a(J)V
    .locals 7

    .prologue
    .line 147
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/cp;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/twitter/android/cp;->b:Ljava/util/List;

    .line 152
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 153
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    new-instance v3, Lbfu;

    iget-object v4, p0, Lcom/twitter/android/cp;->g:Landroid/content/Context;

    iget v5, p0, Lcom/twitter/android/cp;->j:I

    invoke-direct {v3, v4, v0, v5, v1}, Lbfu;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 155
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/cp;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/cp;->c:Ljava/util/List;

    .line 140
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 139
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/cp;->b(JLjava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/twitter/android/cp;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 144
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 231
    const-string/jumbo v0, "ad_slot_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 233
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 235
    iget-object v2, p0, Lcom/twitter/android/cp;->f:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    invoke-static {v0, v1}, Lcom/twitter/library/scribe/b;->a(J)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 237
    const-string/jumbo v1, "position"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 238
    iget-object v1, p0, Lcom/twitter/android/cp;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 240
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 245
    instance-of v0, p1, Lcom/twitter/android/timeline/ap;

    if-eqz v0, :cond_0

    .line 246
    check-cast p1, Lcom/twitter/android/timeline/ap;

    .line 247
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/cp;->a(Lcom/twitter/android/timeline/ap;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Z

    move-result v0

    .line 249
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/cp;->c(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Z

    .line 228
    return-void
.end method
