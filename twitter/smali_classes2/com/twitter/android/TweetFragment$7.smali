.class Lcom/twitter/android/TweetFragment$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/media/widget/TweetMediaView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/media/widget/TweetMediaView$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/TweetView;

.field final synthetic b:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    .prologue
    .line 2045
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$7;->b:Lcom/twitter/android/TweetFragment;

    iput-object p2, p0, Lcom/twitter/android/TweetFragment$7;->a:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcax;Z)V
    .locals 3

    .prologue
    .line 2048
    if-eqz p3, :cond_0

    .line 2049
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$7;->b:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "show"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$7;->a:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 2051
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcom/twitter/model/core/MediaEntity;Z)V
    .locals 3

    .prologue
    .line 2055
    if-eqz p3, :cond_0

    .line 2056
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$7;->b:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "show"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$7;->a:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 2058
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/media/widget/TweetMediaView;Lcom/twitter/model/media/EditableMedia;Z)V
    .locals 3

    .prologue
    .line 2063
    if-eqz p3, :cond_0

    .line 2064
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$7;->b:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "show"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$7;->a:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 2066
    :cond_0
    return-void
.end method
