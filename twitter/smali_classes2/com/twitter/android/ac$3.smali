.class Lcom/twitter/android/ac$3;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ac;->d(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/android/timeline/bk;

.field final synthetic c:Lcom/twitter/android/ac;


# direct methods
.method constructor <init>(Lcom/twitter/android/ac;JLcom/twitter/android/timeline/bk;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/twitter/android/ac$3;->c:Lcom/twitter/android/ac;

    iput-wide p2, p0, Lcom/twitter/android/ac$3;->a:J

    iput-object p4, p0, Lcom/twitter/android/ac$3;->b:Lcom/twitter/android/timeline/bk;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 308
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ac$3;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 311
    iget-object v0, p0, Lcom/twitter/android/ac$3;->c:Lcom/twitter/android/ac;

    iget-wide v2, p0, Lcom/twitter/android/ac$3;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/ac;->b(J)Lcom/twitter/model/timeline/k;

    move-result-object v1

    .line 312
    iget-object v0, p0, Lcom/twitter/android/ac$3;->c:Lcom/twitter/android/ac;

    iget-wide v2, p0, Lcom/twitter/android/ac$3;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/ac;->f(J)Lcom/twitter/model/timeline/g;

    move-result-object v0

    .line 313
    iget-object v2, p0, Lcom/twitter/android/ac$3;->c:Lcom/twitter/android/ac;

    iget-wide v4, p0, Lcom/twitter/android/ac$3;->a:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/android/ac;->d(J)Z

    .line 315
    if-eqz v1, :cond_0

    .line 317
    if-eqz v0, :cond_1

    .line 322
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/ac$3;->c:Lcom/twitter/android/ac;

    iget-object v2, p0, Lcom/twitter/android/ac$3;->c:Lcom/twitter/android/ac;

    invoke-static {v2}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/ac;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/ac$3;->b:Lcom/twitter/android/timeline/bk;

    const-string/jumbo v4, "remove"

    invoke-static {v1, v2, v3, v0, v4}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/ac;Landroid/content/Context;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    .line 324
    :cond_0
    return-void

    .line 320
    :cond_1
    iget-object v0, v1, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    goto :goto_0
.end method
