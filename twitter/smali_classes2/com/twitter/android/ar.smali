.class public Lcom/twitter/android/ar;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/client/v;

.field private final b:Lcom/twitter/android/aq;

.field private final c:Lcom/twitter/android/as;

.field private final d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/as;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/android/aq;

    invoke-direct {v0, p1}, Lcom/twitter/android/aq;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2, p3}, Lcom/twitter/android/ar;-><init>(Lcom/twitter/android/aq;Lcom/twitter/android/as;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/aq;Lcom/twitter/android/as;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ar;->a:Lcom/twitter/library/client/v;

    .line 41
    iput-object p1, p0, Lcom/twitter/android/ar;->b:Lcom/twitter/android/aq;

    .line 42
    iput-object p2, p0, Lcom/twitter/android/ar;->c:Lcom/twitter/android/as;

    .line 43
    iput-object p3, p0, Lcom/twitter/android/ar;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 44
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ar;)Lcom/twitter/android/as;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/ar;->c:Lcom/twitter/android/as;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/widget/ToolBar;)Lazv;
    .locals 2

    .prologue
    .line 71
    const v0, 0x7f1308b4

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 75
    :cond_0
    return-object v0
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;Z)Lazv;
    .locals 2

    .prologue
    .line 57
    const v0, 0x7f1308b4

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    if-eqz p2, :cond_1

    .line 60
    const v1, 0x7f0a09b7

    invoke-virtual {v0, v1}, Lazv;->d(I)Lazv;

    .line 64
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 66
    :cond_0
    return-object v0

    .line 62
    :cond_1
    const v1, 0x7f0a05a5

    invoke-virtual {v0, v1}, Lazv;->d(I)Lazv;

    goto :goto_0
.end method

.method public a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 108
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/twitter/android/ar;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;ZZ)V

    .line 109
    return-void
.end method

.method public a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;Z)V
    .locals 7

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/android/ar;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 156
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v1

    .line 157
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ar;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v5, ""

    const-string/jumbo v6, "unmute_conversation"

    .line 158
    invoke-static {v4, v1, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 157
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 160
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 161
    new-instance v6, Lcom/twitter/android/ar$3;

    invoke-direct {v6, p0, v0, p3, p2}, Lcom/twitter/android/ar$3;-><init>(Lcom/twitter/android/ar;Ljava/lang/ref/WeakReference;ZLcom/twitter/model/core/Tweet;)V

    .line 183
    iget-object v1, p0, Lcom/twitter/android/ar;->b:Lcom/twitter/android/aq;

    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->u:J

    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->ag:J

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/aq;->b(JJLcom/twitter/android/aq$a;)V

    .line 184
    return-void
.end method

.method public a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;ZZ)V
    .locals 7

    .prologue
    .line 114
    if-eqz p4, :cond_0

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/ar;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ar;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 118
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v1

    .line 119
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ar;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v5, ""

    const-string/jumbo v6, "mute_conversation"

    .line 120
    invoke-static {v4, v1, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 119
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 122
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 123
    new-instance v6, Lcom/twitter/android/ar$2;

    invoke-direct {v6, p0, v0, p3, p2}, Lcom/twitter/android/ar$2;-><init>(Lcom/twitter/android/ar;Ljava/lang/ref/WeakReference;ZLcom/twitter/model/core/Tweet;)V

    .line 145
    iget-object v1, p0, Lcom/twitter/android/ar;->b:Lcom/twitter/android/aq;

    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->u:J

    iget-wide v4, p2, Lcom/twitter/model/core/Tweet;->ag:J

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/aq;->a(JJLcom/twitter/android/aq$a;)V

    goto :goto_0
.end method

.method public a(Lcmr;)V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lbph;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/twitter/android/ar;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const v0, 0x7f14001e

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 53
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 88
    invoke-static {}, Lbph;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/ar;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 90
    const-string/jumbo v1, "mute_conversation_prompt"

    .line 91
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 90
    invoke-static {p1, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ar;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    new-instance v2, Lcom/twitter/android/ar$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/ar$1;-><init>(Lcom/twitter/android/ar;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    invoke-static {v0, p3, v1, v2}, Lcom/twitter/android/widget/MuteConversationEducationOverlay;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/app/common/dialog/b$d;)V

    .line 101
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/ar;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;Z)V

    .line 150
    return-void
.end method
