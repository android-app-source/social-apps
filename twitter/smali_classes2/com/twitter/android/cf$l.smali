.class public Lcom/twitter/android/cf$l;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/cf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "l"
.end annotation


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;

.field public final c:Lcom/twitter/library/media/widget/TweetMediaView;

.field public final d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

.field public final e:Lcom/twitter/android/cf$h;

.field public final f:Landroid/view/View;

.field public final g:Landroid/widget/TextView;

.field public final h:Landroid/view/View;

.field public final i:Lcom/twitter/android/trends/TrendBadgesView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2189
    const v0, 0x7f130792

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cf$l;->a:Landroid/widget/TextView;

    .line 2190
    const v0, 0x7f1301e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cf$l;->g:Landroid/widget/TextView;

    .line 2191
    const v0, 0x7f1307cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/cf$l;->b:Landroid/widget/TextView;

    .line 2192
    const v0, 0x7f1304e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/widget/TweetMediaView;

    iput-object v0, p0, Lcom/twitter/android/cf$l;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    .line 2193
    const v0, 0x7f13051e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    iput-object v0, p0, Lcom/twitter/android/cf$l;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 2194
    const v0, 0x7f130815

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$l;->h:Landroid/view/View;

    .line 2195
    const v0, 0x7f130816

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$l;->f:Landroid/view/View;

    .line 2196
    new-instance v0, Lcom/twitter/android/cf$h;

    invoke-direct {v0, p1}, Lcom/twitter/android/cf$h;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/cf$l;->e:Lcom/twitter/android/cf$h;

    .line 2197
    const v0, 0x7f13052e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/trends/TrendBadgesView;

    iput-object v0, p0, Lcom/twitter/android/cf$l;->i:Lcom/twitter/android/trends/TrendBadgesView;

    .line 2198
    return-void
.end method
