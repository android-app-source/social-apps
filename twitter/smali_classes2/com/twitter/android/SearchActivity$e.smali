.class Lcom/twitter/android/SearchActivity$e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation


# instance fields
.field public final a:Landroid/widget/RadioGroup;

.field public final b:Landroid/widget/RadioButton;

.field public final c:Landroid/widget/RadioButton;

.field public final d:Landroid/widget/RadioButton;

.field public final e:Landroid/widget/RadioButton;

.field public final f:Landroid/widget/RadioButton;

.field public final g:Landroid/widget/RadioButton;

.field public final h:Landroid/widget/RadioGroup;

.field public final i:Landroid/widget/RadioButton;

.field public final j:Landroid/widget/RadioButton;

.field public final k:Landroid/widget/RadioGroup;

.field public final l:Landroid/widget/RadioButton;

.field public final m:Landroid/widget/RadioButton;

.field public final n:Landroid/view/View;

.field public final o:Landroid/view/View;

.field public final p:Landroid/view/View;

.field private final q:Landroid/widget/RadioGroup$OnCheckedChangeListener;


# direct methods
.method constructor <init>(Lcom/twitter/library/widget/SlidingPanel;Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1612
    const v0, 0x7f1307bb

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->a:Landroid/widget/RadioGroup;

    .line 1613
    const v0, 0x7f1307bc

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->b:Landroid/widget/RadioButton;

    .line 1614
    const v0, 0x7f1307bd

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->c:Landroid/widget/RadioButton;

    .line 1615
    const v0, 0x7f1307be

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->d:Landroid/widget/RadioButton;

    .line 1616
    const v0, 0x7f1307bf

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->e:Landroid/widget/RadioButton;

    .line 1617
    const v0, 0x7f1307c0

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->f:Landroid/widget/RadioButton;

    .line 1618
    const v0, 0x7f1307c1

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->g:Landroid/widget/RadioButton;

    .line 1620
    const v0, 0x7f1307c2

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->h:Landroid/widget/RadioGroup;

    .line 1621
    const v0, 0x7f1307c3

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->i:Landroid/widget/RadioButton;

    .line 1622
    const v0, 0x7f1307c4

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->j:Landroid/widget/RadioButton;

    .line 1624
    const v0, 0x7f1307c5

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->k:Landroid/widget/RadioGroup;

    .line 1625
    const v0, 0x7f1307c6

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->l:Landroid/widget/RadioButton;

    .line 1626
    const v0, 0x7f1307c7

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->m:Landroid/widget/RadioButton;

    .line 1628
    const v0, 0x7f1307c8

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->n:Landroid/view/View;

    .line 1629
    const v0, 0x7f1307cb

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->o:Landroid/view/View;

    .line 1630
    const v0, 0x7f1307ca

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity$e;->p:Landroid/view/View;

    .line 1632
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1633
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->h:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1634
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->k:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1635
    iput-object p2, p0, Lcom/twitter/android/SearchActivity$e;->q:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 1636
    return-void
.end method


# virtual methods
.method public a(IZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1639
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1640
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->h:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1641
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->k:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1642
    packed-switch p1, :pswitch_data_0

    .line 1666
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->b:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1671
    :goto_0
    if-eqz p2, :cond_0

    .line 1672
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1677
    :goto_1
    if-eqz p3, :cond_1

    .line 1678
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->m:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1682
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->a:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity$e;->q:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1683
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->h:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity$e;->q:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1684
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->k:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity$e;->q:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1685
    return-void

    .line 1644
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->c:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 1649
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->d:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 1654
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->e:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 1658
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->f:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 1662
    :pswitch_5
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->g:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 1674
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->i:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 1680
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->l:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2

    .line 1642
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1688
    if-eqz p1, :cond_1

    .line 1689
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1690
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 1691
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->h:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 1694
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->k:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 1695
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1696
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1704
    :goto_0
    return-void

    .line 1698
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1699
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->h:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 1700
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->k:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 1701
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1702
    iget-object v0, p0, Lcom/twitter/android/SearchActivity$e;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
