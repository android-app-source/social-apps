.class public Lcom/twitter/android/cl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/cl$a;,
        Lcom/twitter/android/cl$b;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/android/cl$a;


# instance fields
.field private final b:Lbxb;

.field private final c:Landroid/widget/ListView;

.field private d:I

.field private e:I

.field private final f:I

.field private g:Lcom/twitter/android/cl$b;

.field private final h:Lcom/twitter/app/common/list/TwitterListFragment;

.field private final i:Z

.field private final j:Z


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/list/TwitterListFragment;Lbxb;Landroid/widget/ListView;IZZ)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iput-object p2, p0, Lcom/twitter/android/cl;->b:Lbxb;

    .line 92
    iput-object p3, p0, Lcom/twitter/android/cl;->c:Landroid/widget/ListView;

    .line 93
    iput p4, p0, Lcom/twitter/android/cl;->f:I

    .line 94
    iget-object v0, p0, Lcom/twitter/android/cl;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 95
    iput-object p1, p0, Lcom/twitter/android/cl;->h:Lcom/twitter/app/common/list/TwitterListFragment;

    .line 96
    iput-boolean p5, p0, Lcom/twitter/android/cl;->i:Z

    .line 97
    iput-boolean p6, p0, Lcom/twitter/android/cl;->j:Z

    .line 98
    return-void
.end method

.method private static a(Ljava/util/Set;Landroid/content/res/Resources;Lcom/twitter/model/core/Tweet;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;",
            "Landroid/content/res/Resources;",
            "Lcom/twitter/model/core/Tweet;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 288
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 290
    iget-object v0, p2, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    .line 292
    :goto_0
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->E:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0845

    .line 294
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 293
    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->z:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a06f6

    .line 295
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->A:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0052

    .line 296
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->u:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0047

    .line 297
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->C:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a06f8

    .line 298
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->D:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a06f5

    .line 300
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 299
    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->n:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a07fd

    .line 301
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->h:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0623

    .line 302
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->v:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a021f

    .line 303
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->w:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a025b

    .line 305
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 304
    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->r:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a09c0

    .line 306
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->q:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0621

    .line 307
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->g:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a061e

    .line 308
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->e:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a061f

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    .line 309
    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->f:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0625

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    .line 310
    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->k:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0626

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    .line 311
    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->j:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0620

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    .line 312
    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->x:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a05a5

    .line 313
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->y:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a09b7

    .line 314
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->m:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a0624

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    .line 315
    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->l:Lcom/twitter/model/core/TweetActionType;

    const v3, 0x7f0a061d

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    .line 316
    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->t:Lcom/twitter/model/core/TweetActionType;

    const v2, 0x7f0a0622

    .line 317
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->B:Lcom/twitter/model/core/TweetActionType;

    const v2, 0x7f0a06f7

    .line 318
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->s:Lcom/twitter/model/core/TweetActionType;

    const-string/jumbo v2, "Debug"

    .line 319
    invoke-static {v1, v2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 322
    new-instance v1, Lcom/twitter/android/cl$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/cl$3;-><init>(Ljava/util/Set;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 290
    :cond_0
    iget-object v0, p2, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lbxb;Lcom/twitter/android/timeline/bk;ZZLjava/lang/String;Z)V
    .locals 10

    .prologue
    .line 128
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v3

    .line 130
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 131
    iget-wide v6, p0, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v0, v6, v4

    if-nez v0, :cond_e

    const/4 v0, 0x1

    .line 132
    :goto_0
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v6

    .line 133
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v7

    .line 134
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->t()Z

    move-result v1

    if-nez v1, :cond_f

    const/4 v1, 0x1

    .line 135
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v8

    .line 138
    if-eqz p4, :cond_10

    iget v2, p0, Lcom/twitter/model/core/Tweet;->P:I

    if-nez v2, :cond_10

    const/4 v2, 0x1

    .line 141
    :goto_2
    if-eqz p8, :cond_0

    .line 142
    sget-object v9, Lcom/twitter/model/core/TweetActionType;->E:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v9}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 144
    :cond_0
    if-eqz v7, :cond_1

    invoke-static {}, Lbrz;->a()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-boolean v9, p0, Lcom/twitter/model/core/Tweet;->F:Z

    if-nez v9, :cond_1

    .line 145
    sget-object v9, Lcom/twitter/model/core/TweetActionType;->u:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v9}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 147
    :cond_1
    if-eqz v7, :cond_2

    iget-boolean v9, p0, Lcom/twitter/model/core/Tweet;->F:Z

    if-nez v9, :cond_2

    if-eqz p5, :cond_2

    .line 148
    sget-object v9, Lcom/twitter/model/core/TweetActionType;->n:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v9}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 150
    :cond_2
    invoke-static {p0, v4, v5}, Lbxd;->b(Lcom/twitter/model/core/Tweet;J)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 151
    if-eqz v1, :cond_11

    .line 152
    sget-object v9, Lcom/twitter/model/core/TweetActionType;->C:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v9}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 157
    :cond_3
    :goto_3
    if-eqz v0, :cond_13

    if-eqz v3, :cond_13

    .line 158
    invoke-virtual {p0, v3}, Lcom/twitter/model/core/Tweet;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 159
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->r:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 166
    :cond_4
    :goto_4
    if-eqz v1, :cond_14

    .line 167
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->D:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 171
    :goto_5
    if-eqz v7, :cond_5

    invoke-static {p0, v4, v5}, Lbxd;->a(Lcom/twitter/model/core/Tweet;J)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 172
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->g:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 174
    :cond_5
    if-nez v0, :cond_9

    .line 176
    if-nez p1, :cond_15

    .line 177
    iget v0, p0, Lcom/twitter/model/core/Tweet;->l:I

    .line 182
    :goto_6
    if-eqz p4, :cond_6

    instance-of v3, p4, Lcom/twitter/android/timeline/k;

    if-eqz v3, :cond_7

    .line 184
    :cond_6
    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v3

    .line 185
    if-eqz v3, :cond_17

    .line 186
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->f:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 193
    :cond_7
    :goto_7
    invoke-static {v0}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 194
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->k:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 200
    :goto_8
    invoke-static {v0}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 201
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->m:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 207
    :goto_9
    if-eqz v1, :cond_1a

    .line 209
    if-eqz v2, :cond_8

    .line 210
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->z:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 212
    :cond_8
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->A:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 213
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->B:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 219
    :cond_9
    :goto_a
    if-eqz p6, :cond_a

    .line 220
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->A()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 221
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->y:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 229
    :cond_a
    :goto_b
    if-eqz v8, :cond_b

    if-eqz v1, :cond_b

    .line 230
    invoke-virtual {v6}, Lcom/twitter/util/collection/o;->i()Lcom/twitter/util/collection/o;

    .line 231
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->z:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 232
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->A:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 233
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->B:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 237
    :cond_b
    if-eqz p7, :cond_c

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 238
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->s:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 241
    :cond_c
    invoke-virtual {v6}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 242
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1c

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->h:Lcom/twitter/model/core/TweetActionType;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 244
    sget-object v1, Lcom/twitter/model/core/TweetActionType;->h:Lcom/twitter/model/core/TweetActionType;

    const/4 v5, 0x0

    move-object v0, p3

    move-object v2, p0

    move-object v3, p4

    move-object v4, p1

    invoke-interface/range {v0 .. v5}, Lbxb;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/util/FriendshipCache;Ljava/lang/String;)Z

    .line 277
    :cond_d
    :goto_c
    return-void

    .line 131
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 134
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 138
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 154
    :cond_11
    sget-object v9, Lcom/twitter/model/core/TweetActionType;->h:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v9}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_3

    .line 161
    :cond_12
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->q:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_4

    .line 163
    :cond_13
    if-nez v1, :cond_4

    if-eqz v2, :cond_4

    .line 164
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->w:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_4

    .line 169
    :cond_14
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->v:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_5

    .line 179
    :cond_15
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p1, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->s:J

    .line 180
    invoke-virtual {p1, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/16 :goto_6

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 188
    :cond_17
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->e:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_7

    .line 196
    :cond_18
    sget-object v3, Lcom/twitter/model/core/TweetActionType;->j:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_8

    .line 203
    :cond_19
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->l:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_9

    .line 215
    :cond_1a
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->t:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_a

    .line 223
    :cond_1b
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->x:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto/16 :goto_b

    .line 247
    :cond_1c
    invoke-static {p2}, Lcom/twitter/android/cl;->a(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 250
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 249
    invoke-static {v0, v1, p0}, Lcom/twitter/android/cl;->a(Ljava/util/Set;Landroid/content/res/Resources;Lcom/twitter/model/core/Tweet;)Ljava/util/List;

    move-result-object v0

    .line 251
    new-instance v1, Lcom/twitter/android/cl$1;

    invoke-direct {v1}, Lcom/twitter/android/cl$1;-><init>()V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v3

    .line 259
    new-instance v1, Lcom/twitter/android/cl$2;

    invoke-direct {v1}, Lcom/twitter/android/cl$2;-><init>()V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v4

    .line 268
    sget-object v0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/cl$a;

    if-eqz v0, :cond_1d

    .line 269
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 270
    sget-object v0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/cl$a;

    :goto_d
    move-object v1, p2

    move-object v2, p3

    move-object v5, p0

    move-object v6, p4

    move-object v7, p1

    move-object/from16 v8, p7

    .line 274
    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/cl$a;->a(Landroid/support/v4/app/FragmentActivity;Lbxb;Ljava/util/List;Ljava/util/List;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/util/FriendshipCache;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 272
    :cond_1d
    new-instance v0, Lcom/twitter/android/cl$a;

    invoke-direct {v0}, Lcom/twitter/android/cl$a;-><init>()V

    goto :goto_d
.end method

.method private static a(Landroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 112
    instance-of v0, p0, Lcom/twitter/app/common/util/i;

    if-eqz v0, :cond_0

    .line 113
    check-cast p0, Lcom/twitter/app/common/util/i;

    invoke-interface {p0}, Lcom/twitter/app/common/util/i;->f_()Z

    move-result v0

    .line 116
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/twitter/android/cl;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setPressed(Z)V

    .line 333
    return-void
.end method

.method public a(Lcom/twitter/android/cl$b;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/twitter/android/cl;->g:Lcom/twitter/android/cl$b;

    .line 337
    return-void
.end method

.method public a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 345
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 346
    instance-of v0, v0, Lcom/twitter/android/bt$a;

    .line 347
    if-eqz v0, :cond_0

    .line 348
    const/4 v0, 0x1

    .line 356
    :goto_0
    return v0

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/cl;->g:Lcom/twitter/android/cl$b;

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/twitter/android/cl;->g:Lcom/twitter/android/cl$b;

    invoke-interface {v0, p1}, Lcom/twitter/android/cl$b;->b(Landroid/view/View;)Lcom/twitter/android/cu;

    move-result-object v0

    .line 352
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lcom/twitter/android/cu;->c:Z

    if-nez v1, :cond_1

    .line 353
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/cl;->a(Lcom/twitter/android/cu;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 356
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/cu;Ljava/lang/String;)Z
    .locals 9

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/cl;->h:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->f_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p1, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/twitter/library/widget/TweetView;

    .line 103
    const v0, 0x7f13007e

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/TweetView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/timeline/bk;

    .line 104
    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getFriendshipCache()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/cl;->h:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v2}, Lcom/twitter/app/common/list/TwitterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/cl;->b:Lbxb;

    iget-boolean v5, p0, Lcom/twitter/android/cl;->i:Z

    iget-boolean v6, p0, Lcom/twitter/android/cl;->j:Z

    const/4 v8, 0x0

    move-object v7, p2

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/cl;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lbxb;Lcom/twitter/android/timeline/bk;ZZLjava/lang/String;Z)V

    .line 108
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 341
    invoke-virtual {p0, p2}, Lcom/twitter/android/cl;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 361
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 362
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 363
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 364
    packed-switch v0, :pswitch_data_0

    .line 380
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 366
    :pswitch_1
    iput v2, p0, Lcom/twitter/android/cl;->d:I

    .line 367
    iput v1, p0, Lcom/twitter/android/cl;->e:I

    goto :goto_0

    .line 371
    :pswitch_2
    iget v0, p0, Lcom/twitter/android/cl;->d:I

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/twitter/android/cl;->f:I

    if-gt v0, v2, :cond_1

    iget v0, p0, Lcom/twitter/android/cl;->e:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/cl;->f:I

    if-le v0, v1, :cond_0

    .line 372
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/cl;->a()V

    goto :goto_0

    .line 364
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
