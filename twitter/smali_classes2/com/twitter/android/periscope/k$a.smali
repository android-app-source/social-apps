.class final Lcom/twitter/android/periscope/k$a;
.super Lcom/twitter/library/client/g;
.source "Twttr"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/periscope/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

.field private final b:Lafb;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Lafb;)V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/twitter/library/client/g;-><init>()V

    .line 243
    iput-object p1, p0, Lcom/twitter/android/periscope/k$a;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    .line 244
    iput-object p2, p0, Lcom/twitter/android/periscope/k$a;->b:Lafb;

    .line 245
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/twitter/android/periscope/k$a;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-virtual {v0, p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c(Lcom/twitter/library/client/Session;)V

    .line 250
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 4

    .prologue
    .line 254
    iget-object v0, p0, Lcom/twitter/android/periscope/k$a;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b()V

    .line 255
    iget-object v0, p0, Lcom/twitter/android/periscope/k$a;->b:Lafb;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lafb;->b(J)V

    .line 256
    iget-object v0, p0, Lcom/twitter/android/periscope/k$a;->b:Lafb;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lafb;->d(J)V

    .line 257
    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 261
    invoke-static {p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/twitter/android/periscope/k$a;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b()V

    .line 264
    :cond_0
    return-void
.end method
