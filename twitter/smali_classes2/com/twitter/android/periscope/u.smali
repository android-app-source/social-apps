.class Lcom/twitter/android/periscope/u;
.super Ltv/periscope/android/api/PublicApiManager;
.source "Twttr"


# instance fields
.field final a:Ltv/periscope/android/api/ApiEventHandler;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/p;


# direct methods
.method constructor <init>(Landroid/content/Context;Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/api/PublicApiService;Ltv/periscope/android/signer/SignerService;Lcyn;Lcyw;)V
    .locals 10

    .prologue
    .line 37
    new-instance v8, Landroid/os/Handler;

    .line 38
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    .line 37
    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/periscope/u;-><init>(Landroid/content/Context;Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/api/PublicApiService;Ltv/periscope/android/signer/SignerService;Lcyn;Lcyw;Landroid/os/Handler;Lcom/twitter/library/client/p;)V

    .line 39
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/api/PublicApiService;Ltv/periscope/android/signer/SignerService;Lcyn;Lcyw;Landroid/os/Handler;Lcom/twitter/library/client/p;)V
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 46
    invoke-direct/range {p0 .. p5}, Ltv/periscope/android/api/PublicApiManager;-><init>(Landroid/content/Context;Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/api/PublicApiService;Ltv/periscope/android/signer/SignerService;)V

    .line 47
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/twitter/android/periscope/u;->b:Lcom/twitter/library/client/p;

    .line 48
    new-instance v1, Lcom/twitter/android/periscope/u$1;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p6

    move-object v5, p7

    move-object v6, p2

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/periscope/u$1;-><init>(Lcom/twitter/android/periscope/u;Landroid/content/Context;Lcyn;Lcyw;Lde/greenrobot/event/c;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/twitter/android/periscope/u;->a:Ltv/periscope/android/api/ApiEventHandler;

    .line 61
    iget-object v1, p0, Lcom/twitter/android/periscope/u;->a:Ltv/periscope/android/api/ApiEventHandler;

    invoke-virtual {p0, v1}, Lcom/twitter/android/periscope/u;->registerApiEventHandler(Ltv/periscope/android/api/ApiEventHandler;)V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/u;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/periscope/u;->b:Lcom/twitter/library/client/p;

    return-object v0
.end method


# virtual methods
.method public bind()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method protected execute(Ltv/periscope/android/api/ApiRunnable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/periscope/u;->b:Lcom/twitter/library/client/p;

    new-instance v1, Lcom/twitter/android/periscope/b;

    invoke-direct {v1, p1}, Lcom/twitter/android/periscope/b;-><init>(Ltv/periscope/android/api/ApiRunnable;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unbind()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method
