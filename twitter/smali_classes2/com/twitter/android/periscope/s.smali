.class Lcom/twitter/android/periscope/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;
.implements Ltv/periscope/android/ui/broadcast/aq;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/periscope/s$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ltv/periscope/android/api/ApiManager;

.field private final c:Lcyn;

.field private d:Lcom/twitter/library/client/Session;


# direct methods
.method constructor <init>(Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyn;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/twitter/android/periscope/s;->b:Ltv/periscope/android/api/ApiManager;

    .line 58
    iput-object p3, p0, Lcom/twitter/android/periscope/s;->c:Lcyn;

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/s;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/periscope/s;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/periscope/s;)Ltv/periscope/android/api/ApiManager;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->b:Ltv/periscope/android/api/ApiManager;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    .line 174
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 129
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    if-nez v0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->c:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_0

    .line 136
    new-instance v1, Lbhd;

    iget-object v2, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    .line 137
    invoke-virtual {v0}, Ltv/periscope/model/p;->D()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v7}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v4

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lbhd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcgi;)V

    .line 139
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    if-nez v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1, v1, p2, p3}, Ltv/periscope/android/util/aj;->a(Landroid/content/res/Resources;IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    new-instance v1, Lcom/twitter/model/drafts/a$a;

    invoke-direct {v1}, Lcom/twitter/model/drafts/a$a;-><init>()V

    .line 103
    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    .line 104
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->c(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 105
    invoke-virtual {v0, v5}, Lcom/twitter/model/drafts/a$a;->b(Z)Lcom/twitter/model/drafts/a$a;

    move-result-object v1

    .line 106
    if-eqz p4, :cond_1

    const-string/jumbo v0, "e_broadcast_location_geotag"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    const-string/jumbo v0, "e_broadcast_location_geotag"

    .line 108
    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v2, Lcom/twitter/model/geo/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/c;

    .line 107
    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/geo/c;)Lcom/twitter/model/drafts/a$a;

    .line 112
    :cond_1
    new-instance v0, Lcom/twitter/android/periscope/s$2;

    iget-object v2, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    .line 113
    invoke-virtual {v1}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/drafts/a;

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/periscope/s$2;-><init>(Lcom/twitter/android/periscope/s;Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;ZLjava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/ui/broadcast/aq$a;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    if-nez v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1, v1, p1, p2}, Ltv/periscope/android/util/aj;->a(Landroid/content/res/Resources;IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/twitter/model/drafts/a$a;

    invoke-direct {v1}, Lcom/twitter/model/drafts/a$a;-><init>()V

    .line 69
    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 70
    invoke-static {p4, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->b(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 71
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {v1, v2, v3}, Lcom/twitter/media/model/MediaFile;->b(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lrx/g;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/periscope/s$1;

    invoke-direct {v2, p0, v0, p5}, Lcom/twitter/android/periscope/s$1;-><init>(Lcom/twitter/android/periscope/s;Lcom/twitter/model/drafts/a$a;Ltv/periscope/android/ui/broadcast/aq$a;)V

    .line 73
    invoke-virtual {v1, v2}, Lrx/g;->a(Lrx/i;)Lrx/j;

    goto :goto_0

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/periscope/s;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    invoke-static {v1, v2, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)Ljava/lang/String;

    .line 90
    invoke-interface {p5}, Ltv/periscope/android/ui/broadcast/aq$a;->a()V

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/session/Session;)Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ltv/periscope/android/session/Session;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/s;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    .line 154
    sget-object v0, Lcom/twitter/android/periscope/s$3;->a:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 156
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    check-cast v0, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;

    .line 159
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 160
    iget-object v0, v0, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;->broadcastId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lcom/twitter/android/periscope/s;->b:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, v1}, Ltv/periscope/android/api/ApiManager;->getBroadcasts(Ljava/util/ArrayList;)Ljava/lang/String;

    goto :goto_0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
