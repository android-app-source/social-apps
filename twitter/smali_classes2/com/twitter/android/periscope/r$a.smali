.class public Lcom/twitter/android/periscope/r$a;
.super Lcom/twitter/android/dialog/g$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/periscope/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private b:Lcom/twitter/android/periscope/q;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/android/dialog/g$b;-><init>(I)V

    .line 39
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/periscope/q;)Lcom/twitter/android/periscope/r$a;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/android/periscope/r$a;->b:Lcom/twitter/android/periscope/q;

    .line 65
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/twitter/android/periscope/r$a;
    .locals 3

    .prologue
    .line 57
    iget-object v1, p0, Lcom/twitter/android/periscope/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "twitter:nsfw_user"

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/periscope/r$a;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/periscope/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "twitter:username"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-object p0
.end method

.method protected b()Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    invoke-direct {v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;-><init>()V

    .line 45
    iget-object v1, p0, Lcom/twitter/android/periscope/r$a;->b:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a(Lcom/twitter/android/periscope/q;)V

    .line 46
    return-object v0
.end method

.method protected synthetic c()Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/android/periscope/r$a;->b()Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Lcom/twitter/android/dialog/TakeoverDialogFragment;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/android/periscope/r$a;->b()Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    move-result-object v0

    return-object v0
.end method
