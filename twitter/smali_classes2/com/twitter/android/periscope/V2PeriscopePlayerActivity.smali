.class public Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;
.implements Lcom/twitter/android/periscope/profile/b;
.implements Lcom/twitter/app/common/dialog/b$c;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcxe;
.implements Ltv/periscope/android/permissions/a;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:J

.field private a:Lcom/twitter/android/periscope/k;

.field private b:Landroid/view/ViewGroup;

.field private c:Ltv/periscope/android/view/PlayerView;

.field private d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field private e:Lcom/twitter/library/av/l;

.field private f:Lcom/twitter/android/periscope/w;

.field private g:Ltv/periscope/android/ui/broadcast/af;

.field private h:Lcom/twitter/model/core/Tweet;

.field private i:Ljava/lang/String;

.field private j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

.field private l:Lyj;

.field private m:Lrx/j;

.field private n:Lcom/twitter/model/core/TwitterUser;

.field private o:Laba;

.field private p:Laey;

.field private q:Laez;

.field private r:Lcom/twitter/android/periscope/v;

.field private s:Laex;

.field private t:Lcom/twitter/android/periscope/profile/a;

.field private u:Laew;

.field private v:Ljava/lang/String;

.field private w:Lcom/twitter/library/client/Session;

.field private x:Lcom/twitter/android/periscope/q;

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->z:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Laba;)Laba;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o:Laba;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Lcom/twitter/android/periscope/w;)Lcom/twitter/android/periscope/w;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f:Lcom/twitter/android/periscope/w;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->n:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/TwitterUser;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->n:Lcom/twitter/model/core/TwitterUser;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/af;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    return-object p1
.end method

.method private a(Lcom/twitter/library/client/Session;)Lyj;
    .locals 6

    .prologue
    .line 172
    new-instance v0, Lyj;

    new-instance v1, Laug;

    new-instance v2, Lwx;

    invoke-direct {v2, p0, p1}, Lwx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-direct {v1, v2}, Laug;-><init>(Lauj;)V

    new-instance v2, Laug;

    new-instance v3, Lww;

    .line 176
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    invoke-direct {v3, v4}, Lww;-><init>(Lcom/twitter/library/provider/t;)V

    invoke-direct {v2, v3}, Laug;-><init>(Lauj;)V

    invoke-direct {v0, v1, v2}, Lyj;-><init>(Lauj;Lauj;)V

    .line 172
    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->l:Lyj;

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->l:Lyj;

    invoke-virtual {v0, p1, p2}, Lyj;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$1;-><init>(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)V

    .line 185
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->m:Lrx/j;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Ljava/lang/String;Ltv/periscope/android/player/PlayMode;)V

    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 256
    new-instance v0, Lcom/twitter/library/av/playback/v;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/v;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    .line 257
    new-instance v1, Lcom/twitter/library/av/playback/r;

    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/library/av/playback/r;-><init>(Lcom/twitter/library/av/playback/q;)V

    .line 258
    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 259
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 260
    invoke-virtual {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Landroid/content/Context;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    sget-object v1, Lbyo;->e:Lbyf;

    .line 261
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lbyf;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/r;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 264
    new-instance v0, Lcom/twitter/library/av/ah;

    invoke-direct {v0}, Lcom/twitter/library/av/ah;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/library/av/ah;->a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;)Lcom/twitter/library/av/VideoViewContainer;

    move-result-object v0

    .line 266
    new-instance v1, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;

    invoke-direct {v1, p0, p2, p1, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;-><init>(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/VideoViewContainer;)V

    iput-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->e:Lcom/twitter/library/av/l;

    .line 342
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->e:Lcom/twitter/library/av/l;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Lcom/twitter/library/av/k;)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 343
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 344
    return-void
.end method

.method private a(Ljava/lang/String;Ltv/periscope/android/player/PlayMode;)V
    .locals 4

    .prologue
    .line 228
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->c:Ltv/periscope/android/view/PlayerView;

    new-instance v1, Ltv/periscope/android/view/PsTextureView;

    invoke-direct {v1, p0}, Ltv/periscope/android/view/PsTextureView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PlayerView;->setTextureView(Landroid/view/TextureView;)V

    .line 232
    new-instance v0, Ltv/periscope/android/ui/broadcast/m;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->c:Ltv/periscope/android/view/PlayerView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v3}, Lcom/twitter/android/periscope/k;->h()Lcyn;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/ui/broadcast/m;-><init>(Ltv/periscope/android/player/c;Ltv/periscope/android/ui/broadcast/ai;Lcyn;)V

    .line 233
    invoke-static {}, Ltv/periscope/android/library/d;->a()Ltv/periscope/android/library/d;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/android/library/d;->d()Ltv/periscope/android/library/a;

    move-result-object v1

    invoke-virtual {v1, p0}, Ltv/periscope/android/library/a;->a(Landroid/app/Activity;)Ltv/periscope/android/library/a;

    move-result-object v1

    .line 234
    invoke-virtual {v1, v0}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/player/d;)Ltv/periscope/android/library/a;

    move-result-object v1

    .line 235
    invoke-virtual {v1, v0}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/player/e;)Ltv/periscope/android/library/a;

    move-result-object v0

    .line 236
    invoke-virtual {v0, p2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/player/PlayMode;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    .line 237
    invoke-virtual {v1}, Laex;->b()Ltv/periscope/android/ui/broadcast/an;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/ui/broadcast/d;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->p:Laey;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->q:Laez;

    .line 238
    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/y;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->r:Lcom/twitter/android/periscope/v;

    .line 239
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/view/o;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    .line 240
    invoke-virtual {v2}, Laex;->a()Ltv/periscope/android/ui/chat/ak;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/ui/chat/aj;Ltv/periscope/android/ui/chat/ak;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/profile/a;

    .line 241
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/view/t;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->u:Laew;

    .line 242
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->a(Lcxq;)Ltv/periscope/android/library/a;

    move-result-object v0

    .line 243
    invoke-virtual {v0, p0}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/permissions/a;)Ltv/periscope/android/library/a;

    move-result-object v0

    .line 244
    invoke-virtual {v0, p0}, Ltv/periscope/android/library/a;->a(Lcxe;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->y:Z

    .line 245
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->b(Z)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->v:Ljava/lang/String;

    .line 246
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->a(Ljava/lang/String;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->C:Z

    .line 247
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->a(Z)Ltv/periscope/android/library/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 248
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->d(Z)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->B:Z

    .line 249
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->c(Z)Ltv/periscope/android/library/a;

    move-result-object v0

    .line 250
    invoke-virtual {v0, p1}, Ltv/periscope/android/library/a;->b(Ljava/lang/String;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->b:Landroid/view/ViewGroup;

    .line 251
    invoke-virtual {v0, v1}, Ltv/periscope/android/library/a;->a(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    .line 252
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0, v1}, Laex;->a(Ltv/periscope/android/ui/broadcast/an$a;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laey;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->p:Laey;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laba;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o:Laba;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laez;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->q:Laez;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/v;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->r:Lcom/twitter/android/periscope/v;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/ui/broadcast/af;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/view/PlayerView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->c:Ltv/periscope/android/view/PlayerView;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/k;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/w;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f:Lcom/twitter/android/periscope/w;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 164
    invoke-static {p0}, Lcom/twitter/android/periscope/k;->a(Landroid/content/Context;)Lcom/twitter/android/periscope/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    .line 165
    invoke-static {}, Ltv/periscope/android/library/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-static {v0, v1}, Ltv/periscope/android/library/d;->a(Landroid/content/Context;Ltv/periscope/android/library/c;)V

    .line 168
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o()V

    .line 202
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->t()Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, p0, v1, p0, v2}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;Lcom/twitter/android/periscope/q;)V

    .line 203
    return-void
.end method

.method static synthetic l(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->B:Z

    return v0
.end method

.method static synthetic m(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laew;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->u:Laew;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/profile/a;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/profile/a;

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->q()V

    .line 216
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->A:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->B:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "video_threesixty_inline_enabled"

    .line 211
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 212
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->i:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->A:Z

    if-eqz v0, :cond_2

    sget-object v0, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    :goto_1
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Ljava/lang/String;Ltv/periscope/android/player/PlayMode;)V

    goto :goto_0

    :cond_2
    sget-object v0, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    goto :goto_1

    .line 214
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->i:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic o(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laex;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 371
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Lcom/twitter/library/av/k;)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 373
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->j()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 374
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 375
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->b(Lcom/twitter/library/av/playback/u;)V

    .line 376
    iput-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 377
    iput-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->e:Lcom/twitter/library/av/l;

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f:Lcom/twitter/android/periscope/w;

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f:Lcom/twitter/android/periscope/w;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/w;->b()V

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    if-eqz v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->B()V

    .line 384
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0, v1}, Laex;->b(Ltv/periscope/android/ui/broadcast/an$a;)V

    .line 386
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->k()Lcom/twitter/android/periscope/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/periscope/g;->a()V

    .line 387
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->l()Lcom/twitter/android/periscope/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/periscope/g;->a()V

    .line 388
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->m()Lcom/twitter/android/periscope/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/periscope/g;->a()V

    .line 389
    return-void
.end method

.method static synthetic p(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->A:Z

    return v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 423
    const-string/jumbo v0, "V2PeriscopePlayer"

    const-string/jumbo v1, "Failed to load broadcast"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->finish()V

    .line 425
    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    .line 451
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->dismiss()V

    .line 456
    :goto_0
    iget v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->z:I

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a(ILcom/twitter/library/client/Session;Lcom/twitter/android/periscope/q;)Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    move-result-object v0

    .line 457
    invoke-virtual {v0, p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a(Lcom/twitter/app/common/dialog/b$c;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 458
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    .line 459
    invoke-virtual {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    const-string/jumbo v2, "PeriscopeTakeoverDialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 460
    return-void

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->f()V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 430
    iget v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->z:I

    if-ne v0, p2, :cond_0

    .line 431
    iput v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->z:I

    .line 432
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    .line 433
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/q;->a(Z)V

    .line 434
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->n()V

    .line 436
    :cond_0
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 3

    .prologue
    .line 441
    iget v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->z:I

    if-ne v0, p2, :cond_0

    .line 442
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->z:I

    .line 443
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    .line 444
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->t()Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 445
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->t()Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, p0, v1, p0, v2}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;Lcom/twitter/android/periscope/q;)V

    .line 446
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/q;->a(Z)V

    .line 448
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->n()V

    .line 496
    return-void
.end method

.method public a(Lcxb;)V
    .locals 2

    .prologue
    .line 413
    const-string/jumbo v0, "V2PeriscopePlayer"

    const-string/jumbo v1, "Loaded broadcast successfully"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->a()V

    .line 415
    return-void
.end method

.method public a(Ltv/periscope/android/api/PsUser;)V
    .locals 0

    .prologue
    .line 481
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->n()V

    .line 482
    return-void
.end method

.method public a(Ltv/periscope/android/library/PeriscopeException;)V
    .locals 0

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->q()V

    .line 420
    return-void
.end method

.method public a(ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x800

    .line 500
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o:Laba;

    if-nez v0, :cond_0

    .line 518
    :goto_0
    return-void

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o:Laba;

    invoke-virtual {v0}, Laba;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 505
    iget v0, v1, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 506
    if-eqz p1, :cond_2

    .line 507
    invoke-static {v0, v3}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 508
    if-eqz p2, :cond_1

    .line 509
    invoke-static {v0, v2}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 515
    :goto_1
    iput v0, v1, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 517
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->q:Laez;

    invoke-virtual {v0}, Laez;->a()V

    goto :goto_0

    .line 510
    :cond_1
    invoke-static {v0, v2}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    goto :goto_1

    .line 512
    :cond_2
    invoke-static {v0, v3}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    .line 513
    invoke-static {v0, v2}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    goto :goto_1
.end method

.method public a([Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 402
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    invoke-virtual {v0, p2, p0, p1}, Lcom/twitter/util/android/f;->a(ILandroid/app/Activity;[Ljava/lang/String;)V

    .line 403
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 474
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->r()V

    .line 475
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x0

    .line 123
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 124
    const v0, 0x7f040320

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->setContentView(I)V

    .line 125
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j()V

    .line 126
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "e_owner_id"

    invoke-virtual {v1, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    .line 129
    invoke-virtual {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 130
    const-string/jumbo v0, "tw"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h:Lcom/twitter/model/core/Tweet;

    .line 131
    const-string/jumbo v0, "broadcast_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->i:Ljava/lang/String;

    .line 132
    const-string/jumbo v0, "association"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 133
    new-instance v2, Lcom/twitter/android/periscope/q;

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 134
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v4, v5, v0}, Lcom/twitter/android/periscope/q;-><init>(JLjava/lang/String;)V

    iput-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    .line 135
    const-string/jumbo v0, "e_file_path"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->v:Ljava/lang/String;

    .line 136
    const-string/jumbo v0, "e_saved_to_gallery"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->y:Z

    .line 137
    const-string/jumbo v0, "is_live"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->A:Z

    .line 138
    const-string/jumbo v0, "is_360"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->B:Z

    .line 139
    const-string/jumbo v0, "e_show_broadcast_info"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->C:Z

    .line 140
    const-string/jumbo v0, "broadcaster_twitter_user_id"

    invoke-virtual {v1, v0, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->D:J

    .line 142
    new-instance v0, Laey;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Laey;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->p:Laey;

    .line 143
    new-instance v0, Laez;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    .line 144
    invoke-virtual {v2}, Lcom/twitter/android/periscope/k;->l()Lcom/twitter/android/periscope/g;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-direct {v0, v1, v2, v3}, Laez;-><init>(Lcom/twitter/library/client/Session;Ldae;Lcom/twitter/android/periscope/q;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->q:Laez;

    .line 145
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->q:Laez;

    invoke-virtual {v0, p0}, Laez;->a(Lcom/twitter/android/periscope/profile/b;)V

    .line 146
    new-instance v0, Lcom/twitter/android/periscope/v;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    .line 147
    invoke-virtual {v2}, Lcom/twitter/android/periscope/k;->l()Lcom/twitter/android/periscope/g;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/periscope/v;-><init>(Lcom/twitter/library/client/Session;Ldae;Lcom/twitter/android/periscope/q;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->r:Lcom/twitter/android/periscope/v;

    .line 148
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->r:Lcom/twitter/android/periscope/v;

    invoke-virtual {v0, p0}, Lcom/twitter/android/periscope/v;->a(Lcom/twitter/android/periscope/profile/b;)V

    .line 149
    new-instance v0, Laex;

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    invoke-direct {v0, p0, v1, v2, v3}, Laex;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/periscope/q;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    .line 151
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    invoke-direct {p0, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Lcom/twitter/library/client/Session;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->l:Lyj;

    .line 152
    new-instance v0, Laew;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 153
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    iget-object v6, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Laew;-><init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/q;Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->u:Laew;

    .line 155
    const v0, 0x7f130750

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PlayerView;

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->c:Ltv/periscope/android/view/PlayerView;

    .line 156
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->b:Landroid/view/ViewGroup;

    .line 157
    new-instance v0, Lcom/twitter/android/periscope/profile/a;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a:Lcom/twitter/android/periscope/k;

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->l:Lyj;

    iget-object v4, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->w:Lcom/twitter/library/client/Session;

    iget-object v5, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->b:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/q;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/periscope/profile/a;-><init>(Landroid/content/Context;Ltv/periscope/android/library/c;Lyj;Lcom/twitter/library/client/Session;Landroid/view/ViewGroup;Lcom/twitter/android/periscope/q;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/profile/a;

    .line 159
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/profile/a;

    invoke-virtual {v0, p0}, Lcom/twitter/android/periscope/profile/a;->a(Lcom/twitter/android/periscope/profile/b;)V

    .line 160
    iget-wide v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->D:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(J)V

    .line 161
    return-void

    .line 134
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public i()V
    .locals 0

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->n()V

    .line 489
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->C()Z

    move-result v0

    if-nez v0, :cond_1

    .line 396
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 398
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 464
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 466
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    if-eqz v0, :cond_0

    .line 467
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->r()V

    .line 469
    :cond_0
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    .line 408
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/af;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/af;->a(II)V

    .line 409
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 348
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStart()V

    .line 349
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->l()V

    .line 350
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 353
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 357
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 358
    invoke-direct {p0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o()V

    .line 359
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/profile/a;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/profile/a;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/profile/a;->c()V

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->m:Lrx/j;

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->m:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 365
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    if-eqz v0, :cond_2

    .line 366
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->s:Laex;

    invoke-virtual {v0}, Laex;->c()V

    .line 368
    :cond_2
    return-void
.end method
