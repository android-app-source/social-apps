.class Lcom/twitter/android/periscope/e;
.super Lcom/twitter/android/periscope/g;
.source "Twttr"


# instance fields
.field private final b:Lbzf;


# direct methods
.method constructor <init>(Lcom/twitter/library/media/manager/g;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/android/periscope/g;-><init>(Lcom/twitter/library/media/manager/g;)V

    .line 22
    new-instance v0, Lbzf;

    invoke-direct {v0}, Lbzf;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/periscope/e;->b:Lbzf;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;IILdae$a;)V
    .locals 2

    .prologue
    .line 35
    invoke-static {p3, p4}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/e;->b:Lbzf;

    .line 36
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lbzi;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/twitter/android/periscope/e$1;

    invoke-direct {v1, p0, p5}, Lcom/twitter/android/periscope/e$1;-><init>(Lcom/twitter/android/periscope/e;Ldae$a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 43
    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/e;->a(Lcom/twitter/media/request/a;)V

    .line 44
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    const/16 v0, 0x100

    .line 27
    invoke-static {v0, v0}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/e;->b:Lbzf;

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lbzi;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 29
    invoke-virtual {p0, v0, p3}, Lcom/twitter/android/periscope/e;->a(Lcom/twitter/media/request/a;Landroid/widget/ImageView;)V

    .line 30
    return-void
.end method
