.class public Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field private c:Lcom/twitter/android/periscope/q;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method public static a(ILcom/twitter/library/client/Session;Lcom/twitter/android/periscope/q;)Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->J:I

    .line 40
    :goto_0
    new-instance v1, Lcom/twitter/android/periscope/r$a;

    invoke-direct {v1, p0}, Lcom/twitter/android/periscope/r$a;-><init>(I)V

    .line 41
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/periscope/r$a;->a(Ljava/lang/String;)Lcom/twitter/android/periscope/r$a;

    move-result-object v1

    .line 42
    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->g(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/periscope/r$a;->a(Ljava/lang/Boolean;)Lcom/twitter/android/periscope/r$a;

    move-result-object v0

    .line 43
    invoke-virtual {v0, p2}, Lcom/twitter/android/periscope/r$a;->a(Lcom/twitter/android/periscope/q;)Lcom/twitter/android/periscope/r$a;

    move-result-object v0

    const v1, 0x7f0a0648

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/r$a;->b(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a0646

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->c(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0a0645

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f0d0195

    .line 47
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->i(I)Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v1, 0x7f020810

    .line 48
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->a(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 49
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    .line 40
    return-object v0

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;)Lcom/twitter/android/periscope/q;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->c:Lcom/twitter/android/periscope/q;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/periscope/r;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/periscope/r;->a(Landroid/os/Bundle;)Lcom/twitter/android/periscope/r;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f13062a

    const/4 v8, 0x1

    .line 64
    invoke-super {p0, p1, p2}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->a(Landroid/app/Dialog;Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a()Lcom/twitter/android/periscope/r;

    move-result-object v2

    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 68
    invoke-virtual {p0, v9}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->b(I)Landroid/view/View;

    move-result-object v4

    .line 69
    const v0, 0x7f13062c

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->b(I)Landroid/view/View;

    move-result-object v5

    .line 70
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 72
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 74
    invoke-static {v3}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e04a8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 76
    invoke-virtual {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 82
    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    const v0, 0x7f13001c

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    new-instance v1, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog$1;

    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f1100c9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v1, p0, v4, v3}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog$1;-><init>(Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;ILandroid/content/Context;)V

    .line 96
    new-array v4, v8, [Ljava/lang/Object;

    aput-object v1, v4, v10

    .line 97
    invoke-virtual {v2}, Lcom/twitter/android/periscope/r;->w()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    const v1, 0x7f0a0647

    .line 99
    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "{{}}"

    invoke-static {v4, v1, v3}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    :goto_1
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 108
    const v0, 0x7f13062b

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 109
    invoke-virtual {v2}, Lcom/twitter/android/periscope/r;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 110
    const v1, 0x7f0a0643

    new-array v3, v8, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "*@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 111
    invoke-virtual {v2}, Lcom/twitter/android/periscope/r;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "*"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v10

    invoke-virtual {p0, v1, v3}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e012c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 118
    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 119
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    neg-float v1, v1

    .line 120
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 121
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x2ee

    .line 122
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 123
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 125
    return-void

    .line 78
    :cond_0
    const/4 v6, -0x1

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 79
    const/4 v6, 0x3

    invoke-virtual {v1, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_0

    .line 102
    :cond_1
    const v1, 0x7f0a0646

    .line 103
    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "{{}}"

    invoke-static {v4, v1, v3}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 114
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public a(Lcom/twitter/android/periscope/q;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->c:Lcom/twitter/android/periscope/q;

    .line 54
    return-void
.end method

.method public synthetic b()Lcom/twitter/android/dialog/g;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a()Lcom/twitter/android/periscope/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/android/dialog/f;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a()Lcom/twitter/android/periscope/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a()Lcom/twitter/android/periscope/r;

    move-result-object v0

    return-object v0
.end method
