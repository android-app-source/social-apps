.class Lcom/twitter/android/periscope/d$1;
.super Ltv/periscope/android/api/DefaultAuthedEventHandler;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/d;-><init>(Landroid/content/Context;Lde/greenrobot/event/c;Lcyw;Lcyn;Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;Lcym;Landroid/os/Handler;Lcom/twitter/library/client/p;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/session/a;

.field final synthetic b:Lcyw;

.field final synthetic c:Landroid/os/Handler;

.field final synthetic d:Lcom/twitter/android/periscope/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/d;Landroid/content/Context;Lcyn;Lcyw;Lde/greenrobot/event/c;Lcym;Ltv/periscope/android/api/AuthedApiManager;Ltv/periscope/android/session/a;Lcyw;Landroid/os/Handler;)V
    .locals 8

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/android/periscope/d$1;->d:Lcom/twitter/android/periscope/d;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/twitter/android/periscope/d$1;->a:Ltv/periscope/android/session/a;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/twitter/android/periscope/d$1;->b:Lcyw;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/twitter/android/periscope/d$1;->c:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v1 .. v7}, Ltv/periscope/android/api/DefaultAuthedEventHandler;-><init>(Landroid/content/Context;Lcyn;Lcyw;Lde/greenrobot/event/c;Lcym;Ltv/periscope/android/api/AuthedApiManager;)V

    return-void
.end method


# virtual methods
.method protected handleEvent(Ltv/periscope/android/event/ApiEvent;)V
    .locals 4

    .prologue
    .line 72
    sget-object v0, Lcom/twitter/android/periscope/d$2;->a:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->handleEvent(Ltv/periscope/android/event/ApiEvent;)V

    .line 87
    return-void

    .line 74
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;

    .line 76
    iget-object v1, p0, Lcom/twitter/android/periscope/d$1;->a:Ltv/periscope/android/session/a;

    iget-object v2, v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;->cookie:Ljava/lang/String;

    sget-object v3, Ltv/periscope/android/session/Session$Type;->b:Ltv/periscope/android/session/Session$Type;

    invoke-interface {v1, v2, v3}, Ltv/periscope/android/session/a;->a(Ljava/lang/String;Ltv/periscope/android/session/Session$Type;)V

    .line 77
    iget-object v1, p0, Lcom/twitter/android/periscope/d$1;->b:Lcyw;

    iget-object v2, v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;->user:Ltv/periscope/android/api/PsUser;

    invoke-interface {v1, v2}, Lcyw;->a(Ltv/periscope/android/api/PsUser;)V

    .line 78
    iget-object v1, p0, Lcom/twitter/android/periscope/d$1;->d:Lcom/twitter/android/periscope/d;

    iget-object v0, v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;->user:Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/android/periscope/d;->getUserStats(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected handleUnauthorizedLogin(Ltv/periscope/android/event/ApiEvent;)V
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ltv/periscope/android/event/AppEvent;

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->b:Ltv/periscope/android/event/AppEvent$Type;

    invoke-direct {v0, v1}, Ltv/periscope/android/event/AppEvent;-><init>(Ltv/periscope/android/event/AppEvent$Type;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/periscope/d$1;->unauthorizedLogout(Ltv/periscope/android/event/AppEvent;Z)V

    .line 68
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/RetryEvent;)V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/periscope/d$1;->c:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/periscope/d$1$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/periscope/d$1$1;-><init>(Lcom/twitter/android/periscope/d$1;Ltv/periscope/android/event/RetryEvent;)V

    iget-object v2, p1, Ltv/periscope/android/event/RetryEvent;->a:Ltv/periscope/android/api/ApiRunnable;

    .line 98
    invoke-virtual {v2}, Ltv/periscope/android/api/ApiRunnable;->currentBackoff()J

    move-result-wide v2

    .line 91
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 99
    return-void
.end method
