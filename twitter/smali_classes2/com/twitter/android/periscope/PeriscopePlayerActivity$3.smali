.class Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/util/collection/k",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v1, v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(Lcom/twitter/android/periscope/PeriscopePlayerActivity;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/TwitterUser;

    .line 189
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->b(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/moments/ui/fullscreen/bo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->b(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/moments/ui/fullscreen/bo;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v1}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->c(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->d(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->d(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->I()V

    .line 194
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->d(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v1}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->c(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->setProfileUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 199
    :cond_1
    :goto_0
    return-void

    .line 196
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->d(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a:Lcom/twitter/android/periscope/PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->d(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->J()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 184
    check-cast p1, Lcom/twitter/util/collection/k;

    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;->a(Lcom/twitter/util/collection/k;)V

    return-void
.end method
