.class public Lcom/twitter/android/periscope/q;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:J

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/twitter/android/periscope/q;->a:J

    .line 18
    invoke-static {p3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    .line 19
    return-void
.end method

.method private static d(ZZ)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 28
    const-string/jumbo v0, "live_follow"

    .line 32
    :goto_0
    return-object v0

    .line 29
    :cond_0
    if-eqz p0, :cond_1

    .line 30
    const-string/jumbo v0, "follow"

    goto :goto_0

    .line 32
    :cond_1
    const-string/jumbo v0, "unfollow"

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "periscope_watch"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 39
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 38
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 40
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/twitter/android/periscope/q;->a:J

    .line 23
    return-void
.end method

.method public a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V
    .locals 5

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 93
    sget-object v1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;->c:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    if-ne p1, v1, :cond_1

    .line 94
    const-string/jumbo v0, "execute"

    .line 98
    :cond_0
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "periscope_authentication"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v0, v2, v3

    .line 99
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 98
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 100
    return-void

    .line 95
    :cond_1
    sget-object v1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    if-ne p1, v1, :cond_0

    .line 96
    const-string/jumbo v0, "deny"

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 117
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v3, "periscope_auth_consent_takeover"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v2, v0

    const/4 v3, 0x4

    if-eqz p1, :cond_0

    const-string/jumbo v0, "consent_granted"

    :goto_0
    aput-object v0, v2, v3

    .line 118
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 117
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 120
    return-void

    .line 117
    :cond_0
    const-string/jumbo v0, "consent_denied"

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 4

    .prologue
    .line 65
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "periscope_watch"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "user_modal"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 66
    invoke-static {p1, p2}, Lcom/twitter/android/periscope/q;->d(ZZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 67
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "periscope_watch"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "share_via"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 44
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 46
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 131
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v3, "settings"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, "privacy"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v3, "toggle"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v3, "connect_to_periscope"

    aput-object v3, v2, v0

    const/4 v3, 0x4

    if-eqz p1, :cond_0

    const-string/jumbo v0, "enable"

    :goto_0
    aput-object v0, v2, v3

    .line 132
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 131
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 133
    return-void

    .line 131
    :cond_0
    const-string/jumbo v0, "disable"

    goto :goto_0
.end method

.method public b(ZZ)V
    .locals 4

    .prologue
    .line 74
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "periscope_watch"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "chat"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "follow_prompt"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 76
    invoke-static {p1, p2}, Lcom/twitter/android/periscope/q;->d(ZZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 74
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 77
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "periscope_watch"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "report_tweet"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 50
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 52
    return-void
.end method

.method public c(ZZ)V
    .locals 4

    .prologue
    .line 84
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "periscope_watch"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "overflow_menu"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "follow_prompt"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 86
    invoke-static {p1, p2}, Lcom/twitter/android/periscope/q;->d(ZZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 85
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 84
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 87
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "periscope_watch"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "retweet"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 57
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 58
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 104
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope_authentication"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "error"

    aput-object v3, v1, v2

    .line 105
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 104
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 106
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 110
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope_auth_consent_takeover"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 111
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 110
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 112
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 124
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope_auth_consent_takeover"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "learn_more_link"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 125
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 124
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 126
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 137
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "start_broadcast"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 138
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 137
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 139
    return-void
.end method

.method public i()V
    .locals 4

    .prologue
    .line 143
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "cancel_broadcast"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 144
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 143
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 145
    return-void
.end method

.method public j()V
    .locals 4

    .prologue
    .line 149
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "end_broadcast"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 150
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 151
    return-void
.end method

.method public k()V
    .locals 4

    .prologue
    .line 155
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "start_broadcast"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "error"

    aput-object v3, v1, v2

    .line 156
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 155
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 157
    return-void
.end method

.method public l()V
    .locals 4

    .prologue
    .line 161
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "chat_carousel"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "block"

    aput-object v3, v1, v2

    .line 162
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 161
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 163
    return-void
.end method

.method public m()V
    .locals 4

    .prologue
    .line 167
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/periscope/q;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/q;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "periscope"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "chat_carousel"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "unblock"

    aput-object v3, v1, v2

    .line 168
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 167
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 169
    return-void
.end method
