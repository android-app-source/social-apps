.class Lcom/twitter/android/periscope/d;
.super Ltv/periscope/android/api/AuthedApiManager;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private final b:Ltv/periscope/android/api/DefaultAuthedEventHandler;


# direct methods
.method constructor <init>(Landroid/content/Context;Lde/greenrobot/event/c;Lcyw;Lcyn;Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;)V
    .locals 13

    .prologue
    .line 48
    new-instance v10, Lcym;

    invoke-direct {v10}, Lcym;-><init>()V

    new-instance v11, Landroid/os/Handler;

    .line 49
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v11, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 50
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v12

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    .line 48
    invoke-direct/range {v0 .. v12}, Lcom/twitter/android/periscope/d;-><init>(Landroid/content/Context;Lde/greenrobot/event/c;Lcyw;Lcyn;Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;Lcym;Landroid/os/Handler;Lcom/twitter/library/client/p;)V

    .line 51
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lde/greenrobot/event/c;Lcyw;Lcyn;Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;Lcym;Landroid/os/Handler;Lcom/twitter/library/client/p;)V
    .locals 12
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 60
    invoke-direct/range {p0 .. p10}, Ltv/periscope/android/api/AuthedApiManager;-><init>(Landroid/content/Context;Lde/greenrobot/event/c;Lcyw;Lcyn;Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;Lcym;)V

    .line 62
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/periscope/d;->a:Lcom/twitter/library/client/p;

    .line 63
    new-instance v1, Lcom/twitter/android/periscope/d$1;

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p4

    move-object v5, p3

    move-object v6, p2

    move-object/from16 v7, p10

    move-object v8, p0

    move-object/from16 v9, p5

    move-object v10, p3

    move-object/from16 v11, p11

    invoke-direct/range {v1 .. v11}, Lcom/twitter/android/periscope/d$1;-><init>(Lcom/twitter/android/periscope/d;Landroid/content/Context;Lcyn;Lcyw;Lde/greenrobot/event/c;Lcym;Ltv/periscope/android/api/AuthedApiManager;Ltv/periscope/android/session/a;Lcyw;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/twitter/android/periscope/d;->b:Ltv/periscope/android/api/DefaultAuthedEventHandler;

    .line 101
    iget-object v1, p0, Lcom/twitter/android/periscope/d;->b:Ltv/periscope/android/api/DefaultAuthedEventHandler;

    invoke-virtual {p0, v1}, Lcom/twitter/android/periscope/d;->registerApiEventHandler(Ltv/periscope/android/api/ApiEventHandler;)V

    .line 102
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/d;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/periscope/d;->a:Lcom/twitter/library/client/p;

    return-object v0
.end method


# virtual methods
.method public addMembersToChannel(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 211
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public bind()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public cancelRequest(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public channelsSearch(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public createChannel(Ljava/lang/String;Ltv/periscope/model/ChannelType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public deleteChannel(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public deleteChannelMember(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/periscope/d;->b:Ltv/periscope/android/api/DefaultAuthedEventHandler;

    return-object v0
.end method

.method public followAll(Ljava/util/Collection;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 142
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getAndHydrateChannelActions(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getAndHydrateChannelMembers(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getBroadcastForTeleport()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getBroadcastMainGlobal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getBroadcastsForChannelId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getChannelInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getChannelsForMember(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getRecentBroadcastGlobalFeed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getSuggestedChannels()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getSuggestedPeople()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getSuggestedPeopleForOnboarding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getSuperfans(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getTrendingPlaces()V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public logout(Ltv/periscope/android/event/AppEvent;Z)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/android/periscope/d;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v0}, Ltv/periscope/android/session/a;->c()V

    .line 163
    return-void
.end method

.method public megaBroadcastCall()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public patchChannel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public patchChannelMember(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/periscope/d;->a:Lcom/twitter/library/client/p;

    new-instance v1, Lcom/twitter/android/periscope/b;

    invoke-direct {v1, p1}, Lcom/twitter/android/periscope/b;-><init>(Ltv/periscope/android/api/ApiRunnable;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 123
    return-void
.end method

.method public unbind()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public updateProfileDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public updateProfileDisplayName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method
