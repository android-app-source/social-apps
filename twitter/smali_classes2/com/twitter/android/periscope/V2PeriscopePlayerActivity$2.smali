.class Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;
.super Lcom/twitter/library/av/l;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/model/core/Tweet;

.field final synthetic c:Lcom/twitter/library/av/VideoViewContainer;

.field final synthetic d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/VideoViewContainer;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    iput-object p2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->b:Lcom/twitter/model/core/Tweet;

    iput-object p4, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->c:Lcom/twitter/library/av/VideoViewContainer;

    invoke-direct {p0}, Lcom/twitter/library/av/l;-><init>()V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->c:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/av/VideoViewContainer;->a(II)V

    .line 327
    return-void
.end method

.method public a(IIZZ)V
    .locals 5

    .prologue
    .line 269
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->g()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/library/av/playback/ai;

    if-eqz v0, :cond_2

    .line 274
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    new-instance v1, Lcom/twitter/android/periscope/w;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 275
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/view/PlayerView;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v3}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->g(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v4}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->i(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/android/periscope/k;->h()Lcyn;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/android/periscope/w;-><init>(Ltv/periscope/android/player/c;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcyn;)V

    .line 274
    invoke-static {v0, v1}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Lcom/twitter/android/periscope/w;)Lcom/twitter/android/periscope/w;

    .line 276
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/periscope/w;->a()V

    .line 277
    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {}, Ltv/periscope/android/library/d;->a()Ltv/periscope/android/library/d;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/library/d;->d()Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 278
    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Landroid/app/Activity;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 279
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/w;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/player/d;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 280
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->j(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/w;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/player/e;)Ltv/periscope/android/library/a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 281
    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->p(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    :goto_1
    invoke-virtual {v2, v0}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/player/PlayMode;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 282
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laex;

    move-result-object v2

    invoke-virtual {v2}, Laex;->b()Ltv/periscope/android/ui/broadcast/an;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/ui/broadcast/d;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 283
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->b(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laey;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v3}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->d(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laez;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/y;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 284
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->e(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/v;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/view/o;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 285
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laex;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v3}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laex;

    move-result-object v3

    invoke-virtual {v3}, Laex;->a()Ltv/periscope/android/ui/chat/ak;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/ui/chat/aj;Ltv/periscope/android/ui/chat/ak;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 286
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->n(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Lcom/twitter/android/periscope/profile/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/view/t;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 287
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->m(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laew;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Lcxq;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 288
    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Ltv/periscope/android/permissions/a;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 289
    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Lcxe;)Ltv/periscope/android/library/a;

    move-result-object v0

    const/4 v2, 0x1

    .line 290
    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->d(Z)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 291
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->l(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->c(Z)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->a:Ljava/lang/String;

    .line 292
    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->b(Ljava/lang/String;)Ltv/periscope/android/library/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    .line 293
    invoke-static {v2}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->k(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/library/a;->a(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v0

    .line 277
    invoke-static {v1, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/af;

    .line 294
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->o(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Laex;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v1}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Laex;->a(Ltv/periscope/android/ui/broadcast/an$a;)V

    goto/16 :goto_0

    .line 281
    :cond_1
    sget-object v0, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    goto/16 :goto_1

    .line 296
    :cond_2
    new-instance v0, Lcpb;

    invoke-direct {v0}, Lcpb;-><init>()V

    const-string/jumbo v1, "periscopeTweet"

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->b:Lcom/twitter/model/core/Tweet;

    .line 297
    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "periscopeBroadcast"

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->a:Ljava/lang/String;

    .line 298
    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v2, "Unexpected non-Periscope AVPlayer from AVPlayerAttachment"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 299
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 296
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/library/av/o;)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->c:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/VideoViewContainer;->a(Lcom/twitter/library/av/o;)V

    .line 332
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 3

    .prologue
    .line 309
    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    iget-object v2, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->p(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->a(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;)V

    .line 310
    return-void

    .line 309
    :cond_0
    sget-object v0, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->c:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->c()V

    .line 315
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/view/PlayerView;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/view/PlayerView;->getPreview()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 316
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->h(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/view/PlayerView;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/view/PlayerView;->getPreview()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->c:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 317
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->c:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->a()Z

    .line 322
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity$2;->d:Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-static {v0}, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;->f(Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->h()V

    .line 339
    :cond_0
    return-void
.end method
