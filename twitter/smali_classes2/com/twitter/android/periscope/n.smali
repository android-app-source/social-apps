.class Lcom/twitter/android/periscope/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/ai;


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field private final b:Lcom/twitter/library/av/playback/ai;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/periscope/n;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 23
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->g()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/ai;

    iput-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    .line 24
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ai;->N()Lczi;

    move-result-object v0

    invoke-virtual {v0}, Lczi;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/ai;->a(F)V

    .line 83
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/av/playback/ai;->b(J)V

    .line 64
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/ai;->a(Landroid/view/Surface;)V

    .line 44
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 68
    if-eqz p1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->a:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->m()V

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ai;->N()Lczi;

    move-result-object v0

    invoke-virtual {v0}, Lczi;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ai;->N()Lczi;

    move-result-object v0

    invoke-virtual {v0}, Lczi;->c()V

    .line 39
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ai;->N()Lczi;

    move-result-object v0

    invoke-virtual {v0}, Lczi;->f()I

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ai;->N()Lczi;

    move-result-object v0

    invoke-virtual {v0}, Lczi;->h()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ai;->d()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/periscope/n;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ai;->c()I

    move-result v0

    return v0
.end method
