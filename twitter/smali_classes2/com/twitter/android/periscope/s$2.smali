.class Lcom/twitter/android/periscope/s$2;
.super Lcom/twitter/android/composer/y;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/android/periscope/s;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/s;Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/twitter/android/periscope/s$2;->b:Lcom/twitter/android/periscope/s;

    iput-object p6, p0, Lcom/twitter/android/periscope/s$2;->a:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/twitter/android/composer/y;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;Z)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/twitter/android/composer/y;->a(Lcom/twitter/async/service/j;)V

    .line 117
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 118
    new-instance v2, Lcom/twitter/android/periscope/s$a;

    iget-object v3, p0, Lcom/twitter/android/periscope/s$2;->b:Lcom/twitter/android/periscope/s;

    .line 119
    invoke-static {v3}, Lcom/twitter/android/periscope/s;->c(Lcom/twitter/android/periscope/s;)Ltv/periscope/android/api/ApiManager;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/periscope/s$2;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/twitter/android/periscope/s$a;-><init>(Ltv/periscope/android/api/ApiManager;Ljava/lang/String;J)V

    .line 120
    invoke-static {v2}, Lcom/twitter/library/provider/o;->b(Lcom/twitter/library/provider/o$a;)V

    .line 121
    iget-object v2, p0, Lcom/twitter/android/periscope/s$2;->b:Lcom/twitter/android/periscope/s;

    invoke-static {v2}, Lcom/twitter/android/periscope/s;->a(Lcom/twitter/android/periscope/s;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/periscope/s$2;->b:Lcom/twitter/android/periscope/s;

    invoke-static {v3}, Lcom/twitter/android/periscope/s;->b(Lcom/twitter/android/periscope/s;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    .line 122
    return-void
.end method
