.class public Lcom/twitter/android/periscope/k;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/library/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/periscope/k$a;
    }
.end annotation


# static fields
.field private static volatile a:Lcom/twitter/android/periscope/k;


# instance fields
.field private final b:Lde/greenrobot/event/c;

.field private final c:Lcyn;

.field private final d:Lcyw;

.field private final e:Ltv/periscope/android/session/a;

.field private final f:Ljava/util/concurrent/ScheduledExecutorService;

.field private final g:Lcom/twitter/android/periscope/i;

.field private final h:Lcom/twitter/android/periscope/g;

.field private final i:Lcom/twitter/android/periscope/g;

.field private final j:Lcom/twitter/android/periscope/g;

.field private final k:Landroid/content/SharedPreferences;

.field private final l:Lretrofit/RestAdapter$LogLevel;

.field private final m:Lcyr;

.field private final n:Lcyt;

.field private final o:Lcom/twitter/android/periscope/s;

.field private final p:Ltv/periscope/android/ui/broadcast/y;

.field private final q:Lafj;

.field private final r:Lcxi;

.field private final s:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 90
    invoke-static {v1}, Ltv/periscope/android/library/d;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->k:Landroid/content/SharedPreferences;

    .line 91
    new-instance v8, Lafb;

    invoke-direct {v8, v1}, Lafb;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-static {}, Lde/greenrobot/event/c;->b()Lde/greenrobot/event/d;

    move-result-object v0

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    invoke-virtual {v2}, Lcof;->a()Z

    move-result v2

    invoke-virtual {v0, v2}, Lde/greenrobot/event/d;->a(Z)Lde/greenrobot/event/d;

    move-result-object v0

    invoke-virtual {v0}, Lde/greenrobot/event/d;->a()Lde/greenrobot/event/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->b:Lde/greenrobot/event/c;

    .line 93
    new-instance v0, Lcyo;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->b:Lde/greenrobot/event/c;

    new-instance v3, Ltv/periscope/android/util/v;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, Ltv/periscope/android/util/v;-><init>(I)V

    invoke-direct {v0, v2, v3}, Lcyo;-><init>(Lde/greenrobot/event/c;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->c:Lcyn;

    .line 94
    new-instance v0, Lafj;

    invoke-direct {v0, v1}, Lafj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->q:Lafj;

    .line 95
    new-instance v0, Lcyx;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->k:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/twitter/android/periscope/k;->c:Lcyn;

    iget-object v4, p0, Lcom/twitter/android/periscope/k;->b:Lde/greenrobot/event/c;

    iget-object v5, p0, Lcom/twitter/android/periscope/k;->q:Lafj;

    invoke-direct {v0, v2, v3, v4, v5}, Lcyx;-><init>(Landroid/content/SharedPreferences;Lcyn;Lde/greenrobot/event/c;Ldbr;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->d:Lcyw;

    .line 96
    new-instance v0, Ltv/periscope/android/session/b;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->k:Landroid/content/SharedPreferences;

    invoke-direct {v0, v2}, Ltv/periscope/android/session/b;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->e:Ltv/periscope/android/session/a;

    .line 97
    invoke-static {v9}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 98
    new-instance v0, Lcom/twitter/android/periscope/h;

    invoke-static {v1}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/periscope/h;-><init>(Landroid/content/Context;Lcom/twitter/library/media/manager/g;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->h:Lcom/twitter/android/periscope/g;

    .line 99
    new-instance v0, Lcom/twitter/android/periscope/g;

    invoke-static {v1}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/periscope/g;-><init>(Lcom/twitter/library/media/manager/g;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->i:Lcom/twitter/android/periscope/g;

    .line 100
    new-instance v0, Lcom/twitter/android/periscope/e;

    invoke-static {v1}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/periscope/e;-><init>(Lcom/twitter/library/media/manager/g;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->j:Lcom/twitter/android/periscope/g;

    .line 101
    sget-object v0, Lretrofit/RestAdapter$LogLevel;->NONE:Lretrofit/RestAdapter$LogLevel;

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->l:Lretrofit/RestAdapter$LogLevel;

    .line 102
    new-instance v0, Lcom/twitter/android/periscope/i;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->e:Ltv/periscope/android/session/a;

    iget-object v3, p0, Lcom/twitter/android/periscope/k;->d:Lcyw;

    iget-object v4, p0, Lcom/twitter/android/periscope/k;->c:Lcyn;

    iget-object v5, p0, Lcom/twitter/android/periscope/k;->b:Lde/greenrobot/event/c;

    iget-object v6, p0, Lcom/twitter/android/periscope/k;->f:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v7, p0, Lcom/twitter/android/periscope/k;->l:Lretrofit/RestAdapter$LogLevel;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/periscope/i;-><init>(Landroid/content/Context;Ltv/periscope/android/session/a;Lcyw;Lcyn;Lde/greenrobot/event/c;Ljava/util/concurrent/Executor;Lretrofit/RestAdapter$LogLevel;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->g:Lcom/twitter/android/periscope/i;

    .line 104
    new-instance v0, Lcyr;

    invoke-direct {v0}, Lcyr;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->m:Lcyr;

    .line 105
    new-instance v0, Lcom/twitter/android/periscope/s;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->g:Lcom/twitter/android/periscope/i;

    iget-object v3, p0, Lcom/twitter/android/periscope/k;->c:Lcyn;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/periscope/s;-><init>(Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyn;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->o:Lcom/twitter/android/periscope/s;

    .line 106
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->b:Lde/greenrobot/event/c;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->o:Lcom/twitter/android/periscope/s;

    invoke-virtual {v0, v2}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 107
    new-instance v0, Lcyt;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->b:Lde/greenrobot/event/c;

    invoke-direct {v0, v2}, Lcyt;-><init>(Lde/greenrobot/event/c;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->n:Lcyt;

    .line 108
    new-instance v0, Ltv/periscope/android/ui/broadcast/y;

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/broadcast/y;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->p:Ltv/periscope/android/ui/broadcast/y;

    .line 109
    new-instance v0, Lafh;

    invoke-direct {v0}, Lafh;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->r:Lcxi;

    .line 110
    new-instance v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-direct {v0, v1, p0, v8}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;-><init>(Landroid/content/Context;Ltv/periscope/android/library/c;Lafb;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/k;->s:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    .line 111
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->s:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/periscope/k;->o:Lcom/twitter/android/periscope/s;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->q:Lafj;

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a([Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;)V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->g:Lcom/twitter/android/periscope/i;

    iget-object v1, p0, Lcom/twitter/android/periscope/k;->s:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/i;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)V

    .line 113
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/k$a;

    iget-object v2, p0, Lcom/twitter/android/periscope/k;->s:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-direct {v1, v2, v8}, Lcom/twitter/android/periscope/k$a;-><init>(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Lafb;)V

    .line 114
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 115
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/periscope/k;
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/twitter/android/periscope/k;->a:Lcom/twitter/android/periscope/k;

    if-nez v0, :cond_1

    .line 73
    const-class v1, Lcom/twitter/android/periscope/k;

    monitor-enter v1

    .line 74
    :try_start_0
    sget-object v0, Lcom/twitter/android/periscope/k;->a:Lcom/twitter/android/periscope/k;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/twitter/android/periscope/k;

    invoke-direct {v0, p0}, Lcom/twitter/android/periscope/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/periscope/k;->a:Lcom/twitter/android/periscope/k;

    .line 76
    const-class v0, Lcom/twitter/android/periscope/k;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 78
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :cond_1
    sget-object v0, Lcom/twitter/android/periscope/k;->a:Lcom/twitter/android/periscope/k;

    return-object v0

    .line 78
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string/jumbo v0, "api.periscope.tv"

    return-object v0
.end method

.method public b()Lretrofit/RestAdapter$LogLevel;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->l:Lretrofit/RestAdapter$LogLevel;

    return-object v0
.end method

.method public c()Lde/greenrobot/event/c;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->b:Lde/greenrobot/event/c;

    return-object v0
.end method

.method public d()Ltv/periscope/android/api/ApiManager;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->g:Lcom/twitter/android/periscope/i;

    return-object v0
.end method

.method public e()Ltv/periscope/android/api/ApiService;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->g:Lcom/twitter/android/periscope/i;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/i;->getApiService()Ltv/periscope/android/api/ApiService;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcyw;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->d:Lcyw;

    return-object v0
.end method

.method public g()Lcyt;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->n:Lcyt;

    return-object v0
.end method

.method public h()Lcyn;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->c:Lcyn;

    return-object v0
.end method

.method public i()Ltv/periscope/android/ui/broadcast/y;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->p:Ltv/periscope/android/ui/broadcast/y;

    return-object v0
.end method

.method public j()Ltv/periscope/android/session/a;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->e:Ltv/periscope/android/session/a;

    return-object v0
.end method

.method public k()Lcom/twitter/android/periscope/g;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->h:Lcom/twitter/android/periscope/g;

    return-object v0
.end method

.method public l()Lcom/twitter/android/periscope/g;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->i:Lcom/twitter/android/periscope/g;

    return-object v0
.end method

.method public m()Lcom/twitter/android/periscope/g;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->j:Lcom/twitter/android/periscope/g;

    return-object v0
.end method

.method public n()Ltv/periscope/android/ui/broadcast/aq;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->o:Lcom/twitter/android/periscope/s;

    return-object v0
.end method

.method public o()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->f:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public p()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->k:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public q()Lcyr;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->m:Lcyr;

    return-object v0
.end method

.method public r()Ldbr;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->q:Lafj;

    return-object v0
.end method

.method public s()Lcxi;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->r:Lcxi;

    return-object v0
.end method

.method public t()Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/twitter/android/periscope/k;->s:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    return-object v0
.end method

.method public synthetic u()Ldae;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/periscope/k;->m()Lcom/twitter/android/periscope/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Ldae;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/periscope/k;->l()Lcom/twitter/android/periscope/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Ldae;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/periscope/k;->k()Lcom/twitter/android/periscope/g;

    move-result-object v0

    return-object v0
.end method
