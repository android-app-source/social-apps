.class public Lcom/twitter/android/periscope/v;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/o;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/periscope/v$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/Session;

.field private final b:Ldae;

.field private final c:Lcom/twitter/android/periscope/q;

.field private d:Laba;

.field private e:Lcom/twitter/android/periscope/profile/b;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/Session;Ldae;Lcom/twitter/android/periscope/q;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/twitter/android/periscope/v;->a:Lcom/twitter/library/client/Session;

    .line 53
    iput-object p2, p0, Lcom/twitter/android/periscope/v;->b:Ldae;

    .line 54
    iput-object p3, p0, Lcom/twitter/android/periscope/v;->c:Lcom/twitter/android/periscope/q;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/v;)Lcom/twitter/android/periscope/profile/b;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/periscope/v;->e:Lcom/twitter/android/periscope/profile/b;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/periscope/v;)Laba;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/periscope/v;)Lcom/twitter/android/periscope/q;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/periscope/v;->c:Lcom/twitter/android/periscope/q;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 3

    .prologue
    .line 103
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 104
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04028d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 105
    new-instance v1, Lcom/twitter/android/periscope/v$a;

    invoke-direct {v1, v0}, Lcom/twitter/android/periscope/v$a;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Laba;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    .line 59
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 7

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    if-nez v0, :cond_0

    .line 180
    :goto_0
    return-void

    .line 117
    :cond_0
    check-cast p1, Lcom/twitter/android/periscope/v$a;

    .line 118
    iget-object v0, p1, Lcom/twitter/android/periscope/v$a;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    invoke-virtual {v1}, Laba;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p1, Lcom/twitter/android/periscope/v$a;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/android/periscope/v$a;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a0b92

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    invoke-virtual {v5}, Laba;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p1, Lcom/twitter/android/periscope/v$a;->a:Ltv/periscope/android/view/MaskImageView;

    invoke-virtual {v0}, Ltv/periscope/android/view/MaskImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/twitter/android/periscope/v;->b:Ldae;

    iget-object v2, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    invoke-virtual {v2}, Laba;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/android/periscope/v$a;->a:Ltv/periscope/android/view/MaskImageView;

    invoke-interface {v1, v0, v2, v3}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 123
    invoke-static {p1}, Lcom/twitter/android/periscope/v$a;->a(Lcom/twitter/android/periscope/v$a;)Laap;

    move-result-object v1

    if-nez v1, :cond_2

    .line 124
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 125
    iget-object v2, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    invoke-virtual {v2}, Laba;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 126
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 127
    iget-object v4, p1, Lcom/twitter/android/periscope/v$a;->d:Landroid/view/ViewGroup;

    const v5, 0x7f0403b4

    .line 128
    invoke-static {v1, v4, v5}, Laay;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Laat;

    move-result-object v4

    .line 130
    invoke-static {v0, v4}, Laax;->a(Landroid/content/Context;Laat;)Laax;

    move-result-object v4

    .line 131
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v5, p1, Lcom/twitter/android/periscope/v$a;->d:Landroid/view/ViewGroup;

    const v6, 0x7f0403b5

    .line 134
    invoke-static {v1, v5, v6}, Lafg;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Laat;

    move-result-object v1

    .line 136
    iget-object v5, p0, Lcom/twitter/android/periscope/v;->a:Lcom/twitter/library/client/Session;

    .line 138
    invoke-static {v0, v1, v2, v5}, Laff;->a(Landroid/content/Context;Laat;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/client/Session;)Laff;

    move-result-object v0

    .line 139
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v1, Lcom/twitter/android/periscope/v$1;

    invoke-direct {v1, p0, v0, v2, p1}, Lcom/twitter/android/periscope/v$1;-><init>(Lcom/twitter/android/periscope/v;Laff;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/android/periscope/v$a;)V

    invoke-virtual {v4, v1}, Laax;->a(Laax$a;)V

    .line 161
    new-instance v1, Lcom/twitter/android/periscope/v$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/periscope/v$2;-><init>(Lcom/twitter/android/periscope/v;)V

    invoke-virtual {v0, v1}, Laff;->a(Laff$a;)V

    .line 172
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    .line 173
    iget-object v2, p1, Lcom/twitter/android/periscope/v$a;->d:Landroid/view/ViewGroup;

    invoke-interface {v0}, Laas;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 176
    :cond_1
    new-instance v0, Laaz;

    iget-object v1, p0, Lcom/twitter/android/periscope/v;->a:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v3, v1}, Laaz;-><init>(Ljava/util/List;Lcom/twitter/library/client/Session;)V

    invoke-static {p1, v0}, Lcom/twitter/android/periscope/v$a;->a(Lcom/twitter/android/periscope/v$a;Laap;)Laap;

    .line 179
    :cond_2
    invoke-static {p1}, Lcom/twitter/android/periscope/v$a;->a(Lcom/twitter/android/periscope/v$a;)Laap;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/v;->d:Laba;

    invoke-interface {v0, v1}, Laap;->a(Laaq;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/android/periscope/profile/b;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/twitter/android/periscope/v;->e:Lcom/twitter/android/periscope/profile/b;

    .line 110
    return-void
.end method

.method public a(Ltv/periscope/android/api/PsUser;Ltv/periscope/android/view/b;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method
