.class public Lcom/twitter/android/periscope/broadcaster/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/twitter/android/client/y;->a(Landroid/content/Context;)Lcom/twitter/android/client/y;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/y;->a(Z)I

    move-result v0

    rsub-int v0, v0, 0x8c

    iput v0, p0, Lcom/twitter/android/periscope/broadcaster/b;->a:I

    .line 21
    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcaster/prebroadcast/d;ZZZJ)V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x8

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x8

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 51
    const/16 v0, 0x8

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 56
    const/16 v0, 0x8

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public h()Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->b:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/twitter/android/periscope/broadcaster/b;->a:I

    return v0
.end method
