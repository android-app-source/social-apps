.class public Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;
.super Lcom/twitter/app/common/base/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/periscope/broadcaster/a$a;
.implements Lcom/twitter/app/common/dialog/b$c;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcxw;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private a:Lcom/twitter/android/periscope/k;

.field private b:Lcom/twitter/library/client/Session;

.field private c:Ltv/periscope/android/view/t;

.field private d:Lcom/twitter/android/periscope/broadcaster/a;

.field private e:Lcom/twitter/android/periscope/q;

.field private f:Lcyc;

.field private g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->i:I

    return-void
.end method

.method private d()Lyj;
    .locals 6

    .prologue
    .line 106
    new-instance v0, Lyj;

    new-instance v1, Laug;

    new-instance v2, Lwx;

    iget-object v3, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v2, p0, v3}, Lwx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-direct {v1, v2}, Laug;-><init>(Lauj;)V

    new-instance v2, Laug;

    new-instance v3, Lww;

    iget-object v4, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    .line 107
    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    invoke-direct {v3, v4}, Lww;-><init>(Lcom/twitter/library/provider/t;)V

    invoke-direct {v2, v3}, Laug;-><init>(Lauj;)V

    invoke-direct {v0, v1, v2}, Lyj;-><init>(Lauj;Lauj;)V

    .line 106
    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->dismiss()V

    .line 156
    :goto_0
    iget v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->i:I

    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a(ILcom/twitter/library/client/Session;Lcom/twitter/android/periscope/q;)Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    move-result-object v0

    .line 157
    invoke-virtual {v0, p0}, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;->a(Lcom/twitter/app/common/dialog/b$c;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 158
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    const-string/jumbo v2, "PeriscopeTakeoverDialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 160
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->f()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->h:Z

    if-eqz v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 115
    :cond_0
    const v0, 0x7f1306ff

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->f:Lcyc;

    invoke-virtual {v0}, Lcyc;->a()V

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->h:Z

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 130
    iget v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->i:I

    if-ne v0, p2, :cond_0

    .line 131
    iput v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->i:I

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    .line 133
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/q;->a(Z)V

    .line 134
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->finish()V

    .line 136
    :cond_0
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 3

    .prologue
    .line 141
    iget v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->i:I

    if-ne v0, p2, :cond_0

    .line 142
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->i:I

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    .line 144
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->t()Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 145
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->d:Lcom/twitter/android/periscope/broadcaster/a;

    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, p0, v1, v2}, Lcom/twitter/android/periscope/broadcaster/a;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/q;)V

    .line 146
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/q;->a(Z)V

    .line 148
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->h()V

    .line 202
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->j()V

    .line 180
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/periscope/V2PeriscopePlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "broadcast_id"

    .line 181
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "is_live"

    const/4 v2, 0x0

    .line 182
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "e_show_broadcast_info"

    const/4 v2, 0x1

    .line 183
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "e_saved_to_gallery"

    .line 184
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "e_file_path"

    .line 185
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "e_owner_id"

    iget-object v2, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    .line 186
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 180
    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->startActivity(Landroid/content/Intent;)V

    .line 188
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->h()Lcyn;

    move-result-object v0

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ltv/periscope/model/p;->D()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {v0}, Ltv/periscope/model/p;->D()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 191
    new-instance v2, Lbga;

    iget-object v3, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v2, p0, v3, v0, v1}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    .line 192
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 195
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->setResult(I)V

    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->finish()V

    .line 197
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->k()V

    .line 123
    const v0, 0x7f0a00b7

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->finish()V

    .line 125
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->i()V

    .line 207
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e()V

    .line 174
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->f:Lcyc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->f:Lcyc;

    invoke-virtual {v0}, Lcyc;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onBackPressed()V

    .line 214
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 166
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->g:Lcom/twitter/android/periscope/PeriscopeTakeoverDialog;

    if-eqz v0, :cond_0

    .line 167
    invoke-direct {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e()V

    .line 169
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const v0, 0x7f040303

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->setContentView(I)V

    .line 70
    invoke-static {}, Ltv/periscope/android/library/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/android/periscope/k;->a(Landroid/content/Context;)Lcom/twitter/android/periscope/k;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/library/d;->a(Landroid/content/Context;Ltv/periscope/android/library/c;)V

    .line 73
    :cond_0
    invoke-static {p0}, Lcom/twitter/android/periscope/k;->a(Landroid/content/Context;)Lcom/twitter/android/periscope/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->a:Lcom/twitter/android/periscope/k;

    .line 74
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 75
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "e_owner_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    .line 76
    new-instance v0, Lcom/twitter/android/periscope/q;

    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    .line 77
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v4, "page"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/android/periscope/q;-><init>(JLjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    .line 78
    invoke-direct {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->d()Lyj;

    move-result-object v3

    .line 79
    new-instance v0, Lcom/twitter/android/periscope/profile/a;

    iget-object v2, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->a:Lcom/twitter/android/periscope/k;

    iget-object v4, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    const v1, 0x1020002

    .line 80
    invoke-virtual {p0, v1}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/periscope/profile/a;-><init>(Landroid/content/Context;Ltv/periscope/android/library/c;Lyj;Lcom/twitter/library/client/Session;Landroid/view/ViewGroup;Lcom/twitter/android/periscope/q;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->c:Ltv/periscope/android/view/t;

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->a:Lcom/twitter/android/periscope/k;

    iget-object v2, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->c:Ltv/periscope/android/view/t;

    new-instance v4, Lcom/twitter/android/periscope/broadcaster/b;

    invoke-direct {v4, p0}, Lcom/twitter/android/periscope/broadcaster/b;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v3, p0

    invoke-static/range {v0 .. v6}, Lcyc;->a(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/view/t;Lcxw;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ljava/lang/String;Z)Lcyc;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->f:Lcyc;

    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 86
    const-string/jumbo v0, "title"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v2, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->f:Lcyc;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcyc;->a(Ljava/lang/String;Z)V

    .line 88
    const-string/jumbo v0, "e_broadcast_location"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    const-string/jumbo v0, "e_broadcast_location"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 90
    const-string/jumbo v2, "e_broadcast_location_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 91
    const-string/jumbo v3, "e_broadcast_location_extras"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 92
    iget-object v3, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->f:Lcyc;

    invoke-virtual {v3, v0, v2, v1}, Lcyc;->a(Landroid/location/Location;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 95
    :cond_1
    const v0, 0x7f1306ff

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ltv/periscope/android/permissions/PermissionsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    new-instance v0, Lcom/twitter/android/periscope/broadcaster/a;

    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->a:Lcom/twitter/android/periscope/k;

    invoke-virtual {v1}, Lcom/twitter/android/periscope/k;->t()Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/periscope/broadcaster/a;-><init>(Lcom/twitter/android/periscope/broadcaster/a$a;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->d:Lcom/twitter/android/periscope/broadcaster/a;

    .line 101
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->d:Lcom/twitter/android/periscope/broadcaster/a;

    iget-object v1, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->e:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, p0, v1, v2}, Lcom/twitter/android/periscope/broadcaster/a;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/q;)V

    .line 102
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    invoke-virtual {p0}, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;->b()V

    goto :goto_0
.end method
