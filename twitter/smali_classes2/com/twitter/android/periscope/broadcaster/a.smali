.class Lcom/twitter/android/periscope/broadcaster/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/periscope/broadcaster/a$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/periscope/broadcaster/a$a;

.field private final b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/broadcaster/a$a;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/android/periscope/broadcaster/a;->a:Lcom/twitter/android/periscope/broadcaster/a$a;

    .line 32
    iput-object p2, p0, Lcom/twitter/android/periscope/broadcaster/a;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    .line 33
    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/q;)V
    .locals 1

    .prologue
    .line 36
    invoke-static {p2}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/a;->a:Lcom/twitter/android/periscope/broadcaster/a$a;

    invoke-interface {v0}, Lcom/twitter/android/periscope/broadcaster/a$a;->b()V

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/a;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-virtual {v0, p1, p2, p0, p3}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;Lcom/twitter/android/periscope/q;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/a;->a:Lcom/twitter/android/periscope/broadcaster/a$a;

    invoke-interface {v0}, Lcom/twitter/android/periscope/broadcaster/a$a;->b()V

    .line 61
    return-void
.end method

.method public a(Ltv/periscope/android/api/PsUser;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/a;->a:Lcom/twitter/android/periscope/broadcaster/a$a;

    invoke-interface {v0}, Lcom/twitter/android/periscope/broadcaster/a$a;->a()V

    .line 51
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/a;->a:Lcom/twitter/android/periscope/broadcaster/a$a;

    invoke-interface {v0}, Lcom/twitter/android/periscope/broadcaster/a$a;->c()V

    .line 46
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/periscope/broadcaster/a;->a:Lcom/twitter/android/periscope/broadcaster/a$a;

    invoke-interface {v0}, Lcom/twitter/android/periscope/broadcaster/a$a;->b()V

    .line 56
    return-void
.end method
