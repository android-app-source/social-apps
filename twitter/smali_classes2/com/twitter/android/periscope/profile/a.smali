.class public Lcom/twitter/android/periscope/profile/a;
.super Ltv/periscope/android/ui/c;
.source "Twttr"


# instance fields
.field private final f:Lyj;

.field private final g:Lcom/twitter/library/client/Session;

.field private final h:Lcom/twitter/android/periscope/q;

.field private i:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

.field private j:Lrx/j;

.field private k:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/periscope/profile/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/library/c;Lyj;Lcom/twitter/library/client/Session;Landroid/view/ViewGroup;Lcom/twitter/android/periscope/q;)V
    .locals 9

    .prologue
    .line 41
    invoke-interface {p2}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v2

    invoke-interface {p2}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcrx;

    invoke-direct {v5}, Lcrx;-><init>()V

    invoke-interface {p2}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v6

    .line 42
    invoke-interface {p2}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v7, p5

    .line 41
    invoke-direct/range {v0 .. v8}, Ltv/periscope/android/ui/c;-><init>(Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyw;Ltv/periscope/android/view/ab$a;Lcsa;Ldae;Landroid/view/ViewGroup;Lde/greenrobot/event/c;)V

    .line 43
    iput-object p3, p0, Lcom/twitter/android/periscope/profile/a;->f:Lyj;

    .line 44
    iput-object p4, p0, Lcom/twitter/android/periscope/profile/a;->g:Lcom/twitter/library/client/Session;

    .line 45
    iput-object p6, p0, Lcom/twitter/android/periscope/profile/a;->h:Lcom/twitter/android/periscope/q;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/profile/a;)Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->i:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/periscope/profile/a;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->g:Lcom/twitter/library/client/Session;

    return-object v0
.end method


# virtual methods
.method a()Lcom/twitter/android/periscope/profile/b;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/profile/b;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/periscope/profile/b;)V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/profile/a;->k:Ljava/lang/ref/WeakReference;

    .line 50
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->f:Lyj;

    if-nez v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/periscope/profile/a;->c()V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->f:Lyj;

    const-wide/16 v2, -0x1

    invoke-static {p1, v2, v3}, Ldcq;->a(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lyj;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/profile/a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/periscope/profile/a$1;-><init>(Lcom/twitter/android/periscope/profile/a;)V

    .line 73
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/profile/a;->j:Lrx/j;

    goto :goto_0
.end method

.method public b()Ltv/periscope/android/view/s;
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->i:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    iget-object v1, p0, Lcom/twitter/android/periscope/profile/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/periscope/profile/a;->h:Lcom/twitter/android/periscope/q;

    invoke-direct {v0, v1, p0, v2}, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;-><init>(Landroid/content/Context;Lcom/twitter/android/periscope/profile/a;Lcom/twitter/android/periscope/q;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/profile/a;->i:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->i:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/profile/a;->a(Ltv/periscope/android/view/BaseProfileSheet;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->i:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->j:Lrx/j;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/a;->j:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 89
    :cond_0
    return-void
.end method
