.class public Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;
.super Ltv/periscope/android/view/SimpleProfileSheet;
.source "Twttr"


# instance fields
.field private final l:Lcom/twitter/android/periscope/profile/a;

.field private final m:Lcom/twitter/android/periscope/q;

.field private n:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/periscope/profile/a;Lcom/twitter/android/periscope/q;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Ltv/periscope/android/view/SimpleProfileSheet;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object p2, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->l:Lcom/twitter/android/periscope/profile/a;

    .line 44
    iput-object p3, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->m:Lcom/twitter/android/periscope/q;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;)Lcom/twitter/android/periscope/q;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->m:Lcom/twitter/android/periscope/q;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 50
    invoke-super {p0, p1}, Ltv/periscope/android/view/SimpleProfileSheet;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    .line 51
    const v0, 0x7f1306aa

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f040290

    const/4 v4, 0x0

    .line 53
    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    .line 54
    iget-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 55
    return-object v2
.end method

.method a(Laba;Lcom/twitter/library/client/Session;)V
    .locals 9

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 73
    new-instance v7, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    const v2, 0x7f040291

    .line 75
    invoke-static {v1, v0, v2}, Laay;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Laat;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Laax;->a(Landroid/content/Context;Laat;)Laax;

    move-result-object v8

    .line 79
    invoke-virtual {v8}, Laax;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 80
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 81
    const/4 v2, 0x0

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 82
    invoke-virtual {v8}, Laax;->b()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    const v2, 0x7f040292

    .line 86
    invoke-static {v1, v0, v2}, Lafg;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Laat;

    move-result-object v0

    .line 88
    invoke-virtual {p1}, Laba;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v3

    .line 90
    invoke-virtual {p0}, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3, p2}, Laff;->a(Landroid/content/Context;Laat;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/client/Session;)Laff;

    move-result-object v4

    .line 91
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->l:Lcom/twitter/android/periscope/profile/a;

    .line 94
    invoke-virtual {v0}, Lcom/twitter/android/periscope/profile/a;->a()Lcom/twitter/android/periscope/profile/b;

    move-result-object v2

    .line 96
    new-instance v5, Laaz;

    invoke-direct {v5, v7, p2}, Laaz;-><init>(Ljava/util/List;Lcom/twitter/library/client/Session;)V

    .line 99
    new-instance v0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;-><init>(Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;Lcom/twitter/android/periscope/profile/b;Lcom/twitter/model/core/TwitterUser;Laff;Laap;Laba;)V

    invoke-virtual {v8, v0}, Laax;->a(Laax$a;)V

    .line 123
    new-instance v0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$2;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$2;-><init>(Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;Lcom/twitter/android/periscope/profile/b;)V

    invoke-virtual {v4, v0}, Laff;->a(Laff$a;)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 135
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    .line 136
    iget-object v2, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    invoke-interface {v0}, Laas;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 139
    :cond_1
    invoke-interface {v5, p1}, Laap;->a(Laaq;)V

    goto/16 :goto_0
.end method

.method public a(Ltv/periscope/android/api/PsUser;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1}, Ltv/periscope/android/view/SimpleProfileSheet;->a(Ltv/periscope/android/api/PsUser;)V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->n:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 62
    if-eqz p1, :cond_0

    iget-object v0, p1, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->l:Lcom/twitter/android/periscope/profile/a;

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/profile/a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
