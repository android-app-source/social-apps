.class Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laax$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->a(Laba;Lcom/twitter/library/client/Session;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/periscope/profile/b;

.field final synthetic b:Lcom/twitter/model/core/TwitterUser;

.field final synthetic c:Laff;

.field final synthetic d:Laap;

.field final synthetic e:Laba;

.field final synthetic f:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;Lcom/twitter/android/periscope/profile/b;Lcom/twitter/model/core/TwitterUser;Laff;Laap;Laba;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->f:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    iput-object p2, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->a:Lcom/twitter/android/periscope/profile/b;

    iput-object p3, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->b:Lcom/twitter/model/core/TwitterUser;

    iput-object p4, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->c:Laff;

    iput-object p5, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->d:Laap;

    iput-object p6, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->e:Laba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 103
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->a:Lcom/twitter/android/periscope/profile/b;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->b:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 105
    iget-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->a:Lcom/twitter/android/periscope/profile/b;

    invoke-static {v0}, Lcom/twitter/model/core/g;->b(I)Z

    move-result v0

    invoke-interface {v1, p1, v0}, Lcom/twitter/android/periscope/profile/b;->a(ZZ)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->b:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 109
    if-eqz p1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->c:Laff;

    invoke-virtual {v1}, Laff;->c()V

    .line 111
    invoke-static {v0, v2}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 117
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->b:Lcom/twitter/model/core/TwitterUser;

    iput v0, v1, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 118
    iget-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->d:Laap;

    iget-object v2, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->e:Laba;

    invoke-interface {v1, v2}, Laap;->a(Laaq;)V

    .line 119
    iget-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->f:Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;

    invoke-static {v1}, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;->a(Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet;)Lcom/twitter/android/periscope/q;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/model/core/g;->b(I)Z

    move-result v0

    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/periscope/q;->a(ZZ)V

    .line 120
    return-void

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/periscope/profile/PeriscopeProfileSheet$1;->c:Laff;

    invoke-virtual {v1}, Laff;->d()V

    .line 114
    invoke-static {v0, v2}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    .line 115
    const/16 v1, 0x800

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    goto :goto_0
.end method
