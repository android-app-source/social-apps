.class Lcom/twitter/android/periscope/s$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/provider/o$a;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/periscope/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/api/ApiManager;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private d:J


# direct methods
.method constructor <init>(Ltv/periscope/android/api/ApiManager;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/twitter/android/periscope/s$a;->a:Ltv/periscope/android/api/ApiManager;

    .line 185
    iput-object p2, p0, Lcom/twitter/android/periscope/s$a;->b:Ljava/lang/String;

    .line 186
    iput-wide p3, p0, Lcom/twitter/android/periscope/s$a;->c:J

    .line 187
    return-void
.end method


# virtual methods
.method public a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z
    .locals 5

    .prologue
    .line 199
    iget-wide v0, p0, Lcom/twitter/android/periscope/s$a;->d:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 200
    invoke-static {p0}, Lcom/twitter/library/provider/o;->a(Lcom/twitter/library/provider/o$a;)V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/periscope/s$a;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v1, p0, Lcom/twitter/android/periscope/s$a;->b:Ljava/lang/String;

    iget-wide v2, p3, Lcom/twitter/model/core/Tweet;->G:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/api/ApiManager;->associateTweetWithBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 203
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public b(Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 191
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/periscope/s$a;->c:J

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 192
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->G:J

    iput-wide v0, p0, Lcom/twitter/android/periscope/s$a;->d:J

    .line 194
    :cond_0
    return-void
.end method

.method public e(J)V
    .locals 3

    .prologue
    .line 208
    iget-wide v0, p0, Lcom/twitter/android/periscope/s$a;->d:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 209
    invoke-static {p0}, Lcom/twitter/library/provider/o;->a(Lcom/twitter/library/provider/o$a;)V

    .line 211
    :cond_0
    return-void
.end method
