.class Lcom/twitter/android/periscope/v$a;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/periscope/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field final a:Ltv/periscope/android/view/MaskImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/widget/TextView;

.field final d:Landroid/view/ViewGroup;

.field final e:Landroid/content/res/Resources;

.field private f:Laap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laap",
            "<",
            "Laba;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 193
    const v0, 0x7f130626

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/MaskImageView;

    iput-object v0, p0, Lcom/twitter/android/periscope/v$a;->a:Ltv/periscope/android/view/MaskImageView;

    .line 194
    const v0, 0x7f13009a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/periscope/v$a;->b:Landroid/widget/TextView;

    .line 195
    const v0, 0x7f1301e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/periscope/v$a;->c:Landroid/widget/TextView;

    .line 196
    const v0, 0x7f130627

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/periscope/v$a;->d:Landroid/view/ViewGroup;

    .line 197
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/v$a;->e:Landroid/content/res/Resources;

    .line 198
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/v$a;)Laap;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/android/periscope/v$a;->f:Laap;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/periscope/v$a;Laap;)Laap;
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/twitter/android/periscope/v$a;->f:Laap;

    return-object p1
.end method
