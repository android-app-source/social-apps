.class Lcom/twitter/android/periscope/m;
.super Ltv/periscope/android/ui/broadcast/m;
.source "Twttr"

# interfaces
.implements Lczi$b;
.implements Lczi$e;


# instance fields
.field private final b:Lcom/twitter/library/av/playback/ai;


# direct methods
.method constructor <init>(Ltv/periscope/android/player/b;Lcom/twitter/library/av/playback/ai;Lcyn;)V
    .locals 3

    .prologue
    .line 24
    invoke-interface {p1}, Ltv/periscope/android/player/b;->a()Ltv/periscope/android/player/c;

    move-result-object v0

    new-instance v1, Ltv/periscope/android/ui/broadcast/aj;

    invoke-virtual {p2}, Lcom/twitter/library/av/playback/ai;->N()Lczi;

    move-result-object v2

    invoke-direct {v1, v2}, Ltv/periscope/android/ui/broadcast/aj;-><init>(Lczi;)V

    invoke-direct {p0, v0, v1, p3}, Ltv/periscope/android/ui/broadcast/m;-><init>(Ltv/periscope/android/player/c;Ltv/periscope/android/ui/broadcast/ai;Lcyn;)V

    .line 26
    iput-object p2, p0, Lcom/twitter/android/periscope/m;->b:Lcom/twitter/library/av/playback/ai;

    .line 27
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/ai;->a(Lczi$e;)V

    .line 31
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/ai;->a(Lczi$b;)V

    .line 32
    return-void
.end method

.method public a(IIIF)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/l;->a(IIIF)V

    .line 59
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/l;->a(Ljava/lang/Exception;)V

    .line 51
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/metadata/id3/Id3Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/l;->a(Ljava/util/List;)V

    .line 73
    :cond_0
    return-void
.end method

.method public a(ZI)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/l;->a(ZI)V

    .line 44
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/ai;->a(Lczi$e;)V

    .line 36
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/ai;->a(Lczi$b;)V

    .line 37
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/twitter/android/periscope/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->c()V

    .line 66
    :cond_0
    return-void
.end method
