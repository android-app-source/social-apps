.class public Lcom/twitter/android/periscope/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/api/ApiManager;


# instance fields
.field private final a:Ltv/periscope/android/api/ApiManager;

.field private final b:Ltv/periscope/android/api/ApiManager;

.field private final c:Ltv/periscope/android/session/a;

.field private d:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/session/a;Lcyw;Lcyn;Lde/greenrobot/event/c;Ljava/util/concurrent/Executor;Lretrofit/RestAdapter$LogLevel;)V
    .locals 13

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p2, p0, Lcom/twitter/android/periscope/i;->c:Ltv/periscope/android/session/a;

    .line 54
    new-instance v0, Ltv/periscope/android/api/RestClient;

    .line 55
    invoke-static {}, Lcom/twitter/android/periscope/a;->a()Ljava/lang/String;

    move-result-object v3

    const-class v4, Ltv/periscope/android/api/ApiService;

    move-object v1, p1

    move-object/from16 v2, p6

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/api/RestClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;)V

    .line 56
    new-instance v1, Ltv/periscope/android/api/RestClient;

    .line 57
    invoke-static {}, Lcom/twitter/android/periscope/a;->a()Ljava/lang/String;

    move-result-object v4

    const-class v5, Ltv/periscope/android/api/PublicApiService;

    move-object v2, p1

    move-object/from16 v3, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/api/RestClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;)V

    .line 58
    new-instance v2, Ltv/periscope/android/api/RestClient;

    .line 59
    invoke-static {}, Lcom/twitter/android/periscope/a;->b()Ljava/lang/String;

    move-result-object v5

    const-class v6, Ltv/periscope/android/signer/SignerService;

    move-object v3, p1

    move-object/from16 v4, p6

    move-object/from16 v7, p7

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/api/RestClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;)V

    .line 60
    new-instance v3, Ltv/periscope/android/api/RestClient;

    .line 61
    invoke-static {}, Lcom/twitter/android/periscope/a;->c()Ljava/lang/String;

    move-result-object v6

    const-class v7, Ltv/periscope/android/api/service/channels/ChannelsService;

    move-object v4, p1

    move-object/from16 v5, p6

    move-object/from16 v8, p7

    invoke-direct/range {v3 .. v8}, Ltv/periscope/android/api/RestClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;)V

    .line 62
    new-instance v4, Ltv/periscope/android/api/RestClient;

    .line 63
    invoke-static {}, Lcom/twitter/android/periscope/a;->d()Ljava/lang/String;

    move-result-object v7

    const-class v8, Ltv/periscope/android/api/service/safety/SafetyService;

    move-object v5, p1

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-direct/range {v4 .. v9}, Ltv/periscope/android/api/RestClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;)V

    .line 65
    new-instance v5, Lcom/twitter/android/periscope/u;

    .line 66
    invoke-virtual {v0}, Ltv/periscope/android/api/RestClient;->getService()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ltv/periscope/android/api/ApiService;

    invoke-virtual {v1}, Ltv/periscope/android/api/RestClient;->getService()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ltv/periscope/android/api/PublicApiService;

    .line 67
    invoke-virtual {v2}, Ltv/periscope/android/api/RestClient;->getService()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ltv/periscope/android/signer/SignerService;

    move-object v6, p1

    move-object/from16 v7, p5

    move-object/from16 v11, p4

    move-object/from16 v12, p3

    invoke-direct/range {v5 .. v12}, Lcom/twitter/android/periscope/u;-><init>(Landroid/content/Context;Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/api/PublicApiService;Ltv/periscope/android/signer/SignerService;Lcyn;Lcyw;)V

    iput-object v5, p0, Lcom/twitter/android/periscope/i;->a:Ltv/periscope/android/api/ApiManager;

    .line 68
    new-instance v10, Lcom/twitter/android/periscope/d;

    .line 70
    invoke-virtual {v0}, Ltv/periscope/android/api/RestClient;->getService()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ltv/periscope/android/api/ApiService;

    invoke-virtual {v2}, Ltv/periscope/android/api/RestClient;->getService()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ltv/periscope/android/signer/SignerService;

    invoke-virtual {v3}, Ltv/periscope/android/api/RestClient;->getService()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ltv/periscope/android/api/service/channels/ChannelsService;

    .line 71
    invoke-virtual {v4}, Ltv/periscope/android/api/RestClient;->getService()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ltv/periscope/android/api/service/safety/SafetyService;

    move-object v0, v10

    move-object v1, p1

    move-object/from16 v2, p5

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/periscope/d;-><init>(Landroid/content/Context;Lde/greenrobot/event/c;Lcyw;Lcyn;Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;)V

    iput-object v10, p0, Lcom/twitter/android/periscope/i;->b:Ltv/periscope/android/api/ApiManager;

    .line 72
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/i;->d:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/i;->c:Ltv/periscope/android/session/a;

    .line 80
    invoke-interface {v0}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Ltv/periscope/android/api/ApiManager;
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/i;->b:Ltv/periscope/android/api/ApiManager;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/i;->a:Ltv/periscope/android/api/ApiManager;

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/android/periscope/i;->d:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    .line 76
    return-void
.end method

.method public activeJuror(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->activeJuror(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public addMembersToChannel(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 520
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->addMembersToChannel(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public associateDigitsAccount(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->associateDigitsAccount(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public associateTweetWithBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 615
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->associateTweetWithBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bind()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->bind()V

    .line 91
    return-void
.end method

.method public block(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->block(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public broadcastMeta(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ltv/periscope/android/api/ChatStats;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Ltv/periscope/android/api/ApiManager;->broadcastMeta(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public broadcastSearch(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->broadcastSearch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cancelRequest(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->cancelRequest(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public channelsSearch(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 566
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->channelsSearch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public clearHistoryBroadcastFeed(Ljava/lang/Long;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->clearHistoryBroadcastFeed(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createBroadcast(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 345
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->createBroadcast(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createChannel(Ljava/lang/String;Ltv/periscope/model/ChannelType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 499
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->createChannel(Ljava/lang/String;Ltv/periscope/model/ChannelType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createExternalEncoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 627
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->createExternalEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deactivateAccount()V
    .locals 1

    .prologue
    .line 438
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->deactivateAccount()V

    .line 439
    return-void
.end method

.method public decreaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->decreaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deleteBroadcast(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->deleteBroadcast(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deleteChannel(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->deleteChannel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deleteChannelMember(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 525
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->deleteChannelMember(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deleteExternalEncoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->deleteExternalEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public endBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->endBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public endWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 9

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/api/ApiManager;->endWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public follow(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->follow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public followAll(Ljava/util/Collection;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->followAll(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccessChat(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getAccessChat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccessChatNoRetry(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getAccessChatNoRetry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccessVideo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->getAccessVideo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAndHydrateChannelActions(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getAndHydrateChannelActions(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAndHydrateChannelMembers(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getAndHydrateChannelMembers(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApiService()Ltv/periscope/android/api/ApiService;
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getApiService()Ltv/periscope/android/api/ApiService;

    move-result-object v0

    return-object v0
.end method

.method public getAuthorizeTokenForBackendService(Ltv/periscope/android/api/BackendServiceName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getAuthorizeTokenForBackendService(Ltv/periscope/android/api/BackendServiceName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBlocked()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getBlocked()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastForExternalEncoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 651
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getBroadcastForExternalEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastForTeleport()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getBroadcastForTeleport()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastIdForShareToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getBroadcastIdForShareToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastMainGlobal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getBroadcastMainGlobal()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastTrailer(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getBroadcastTrailer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastViewers(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->getBroadcastViewers(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcasts(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getBroadcasts(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcasts(Ljava/util/ArrayList;Z)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 360
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->getBroadcasts(Ljava/util/ArrayList;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastsByPolling(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getBroadcastsByPolling(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastsForChannelId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 560
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getBroadcastsForChannelId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChannelInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 572
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getChannelInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChannelsForMember(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 509
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getChannelsForMember(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExternalEncoders()Ljava/lang/String;
    .locals 1

    .prologue
    .line 645
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getExternalEncoders()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getFollowers()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersById(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getFollowersById(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowing()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getFollowing()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowingById(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getFollowingById(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowingIdsOnly()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getFollowingIdsOnly()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGlobalMap(FFFF)Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Ltv/periscope/android/api/ApiManager;->getGlobalMap(FFFF)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMutualFollows()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getMutualFollows()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMyUserBroadcasts()V
    .locals 1

    .prologue
    .line 454
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getMyUserBroadcasts()V

    .line 455
    return-void
.end method

.method public getRecentBroadcastGlobalFeed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getRecentBroadcastGlobalFeed()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRecentlyWatchedBroadcasts()V
    .locals 1

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getRecentlyWatchedBroadcasts()V

    .line 470
    return-void
.end method

.method public getSettings()Ljava/lang/String;
    .locals 1

    .prologue
    .line 399
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getSettings()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestedChannels()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getSuggestedChannels()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestedPeople()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getSuggestedPeople()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestedPeopleForOnboarding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getSuggestedPeopleForOnboarding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuperfans(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 621
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getSuperfans(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrendingPlaces()V
    .locals 1

    .prologue
    .line 535
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getTrendingPlaces()V

    .line 536
    return-void
.end method

.method public getUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getUser()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserBroadcastsByUserId(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 459
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getUserBroadcastsByUserId(Ljava/lang/String;)V

    .line 460
    return-void
.end method

.method public getUserBroadcastsByUsername(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getUserBroadcastsByUsername(Ljava/lang/String;)V

    .line 465
    return-void
.end method

.method public getUserById(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getUserById(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserByUsername(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getUserById(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserStats(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->getUserStats(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hello()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->hello()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public increaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->increaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public livePlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ltv/periscope/android/api/ChatStats;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 410
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->livePlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/session/Session$Type;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Ltv/periscope/android/api/ApiManager;->login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/session/Session$Type;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loginTwitterToken(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/periscope/i;->b:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->loginTwitterToken(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public logout(Ltv/periscope/android/event/AppEvent;Z)V
    .locals 1

    .prologue
    .line 433
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->logout(Ltv/periscope/android/event/AppEvent;Z)V

    .line 434
    return-void
.end method

.method public markBroadcastPersistent(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 589
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->markBroadcastPersistent(Ljava/lang/String;)V

    .line 590
    return-void
.end method

.method public megaBroadcastCall()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->megaBroadcastCall()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public mute(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->mute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public patchChannel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 504
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->patchChannel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public patchChannelMember(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->patchChannelMember(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public performUploadTest()V
    .locals 1

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->performUploadTest()V

    .line 450
    return-void
.end method

.method public pingBroadcast(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->pingBroadcast(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public pingWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 9

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/api/ApiManager;->pingWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public publishBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;ZFFZII)Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;ZFFZII)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-interface/range {v0 .. v10}, Ltv/periscope/android/api/ApiManager;->publishBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;ZFFZII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public replayPlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ltv/periscope/android/api/ChatStats;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 416
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->replayPlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public replayThumbnailPlaylist(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->replayThumbnailPlaylist(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public reportBroadcast(Ljava/lang/String;Ltv/periscope/model/AbuseType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->reportBroadcast(Ljava/lang/String;Ltv/periscope/model/AbuseType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public reportComment(Ltv/periscope/model/chat/Message;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 542
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->reportComment(Ltv/periscope/model/chat/Message;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public retweetBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 609
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->retweetBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setExternalEncoderName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 633
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->setExternalEncoderName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setProfileImage(Ljava/io/File;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 474
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->setProfileImage(Ljava/io/File;Ljava/io/File;)V

    .line 475
    return-void
.end method

.method public setSettings(Ltv/periscope/android/api/PsSettings;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->setSettings(Ltv/periscope/android/api/PsSettings;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public shareBroadcast(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->shareBroadcast(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public startWatching(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->startWatching(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public supportedLanguages()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->supportedLanguages()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tweetBroadcastPublished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 602
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->tweetBroadcastPublished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unban()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->unban()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unbind()V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->unbind()V

    .line 96
    return-void
.end method

.method public unblock(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->unblock(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unfollow(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->unfollow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unmute(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->unmute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateProfileDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->updateProfileDescription(Ljava/lang/String;)V

    .line 485
    return-void
.end method

.method public updateProfileDisplayName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->updateProfileDisplayName(Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public uploadBroadcasterLogs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->uploadBroadcasterLogs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userSearch(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->userSearch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validateUsername(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/api/ApiManager;->validateUsername(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public verifyUsername(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Ltv/periscope/android/api/ApiManager;->verifyUsername(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public vote(Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VoteType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 548
    invoke-direct {p0}, Lcom/twitter/android/periscope/i;->b()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->vote(Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VoteType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
