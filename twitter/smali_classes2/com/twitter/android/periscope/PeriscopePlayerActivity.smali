.class public Lcom/twitter/android/periscope/PeriscopePlayerActivity;
.super Lcom/twitter/android/FullscreenMediaPlayerActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/businessprofiles/c;
.implements Lcom/twitter/android/periscope/c;
.implements Lcxe;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lcom/twitter/model/businessprofiles/d;

.field private D:Lcom/twitter/android/periscope/c$a;

.field private E:Lyj;

.field private K:Lrx/j;

.field private L:Lcom/twitter/model/core/Tweet;

.field private M:Lcom/twitter/model/core/TwitterUser;

.field private N:Lcom/twitter/android/moments/ui/fullscreen/bo;

.field private O:Z

.field private P:J

.field private final l:Lcom/twitter/model/core/TwitterUser;

.field private final m:Lcom/twitter/android/profiles/x;

.field private final n:Lcom/twitter/android/av/aj$a;

.field private final o:Lcom/twitter/android/periscope/f;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Landroid/view/OrientationEventListener;

.field private s:Lcom/twitter/android/periscope/t;

.field private t:Lcom/twitter/android/periscope/k;

.field private u:Lcom/twitter/library/av/playback/ai;

.field private v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

.field private w:Lcom/twitter/android/periscope/m;

.field private x:Lcom/twitter/android/periscope/j;

.field private y:Lcom/twitter/android/av/ag;

.field private z:Landroid/view/Display;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/twitter/android/FullscreenMediaPlayerActivity;-><init>()V

    .line 91
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    const-wide v2, 0x91c81766L

    .line 92
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    const-string/jumbo v1, "periscopeco"

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->l:Lcom/twitter/model/core/TwitterUser;

    .line 93
    new-instance v0, Lcom/twitter/android/profiles/x;

    invoke-direct {v0}, Lcom/twitter/android/profiles/x;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->m:Lcom/twitter/android/profiles/x;

    .line 94
    new-instance v0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity$1;-><init>(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->n:Lcom/twitter/android/av/aj$a;

    .line 100
    new-instance v0, Lcom/twitter/android/periscope/f;

    invoke-direct {v0}, Lcom/twitter/android/periscope/f;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->o:Lcom/twitter/android/periscope/f;

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->A:Z

    return-void
.end method

.method private a(JLcax;)Lcom/twitter/android/card/d;
    .locals 3

    .prologue
    .line 404
    new-instance v0, Lcom/twitter/android/card/f;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    .line 405
    invoke-interface {v0, p1, p2}, Lcom/twitter/android/card/d;->a(J)V

    .line 406
    invoke-virtual {p3}, Lcax;->K()Lcar;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Lcar;)V

    .line 407
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/library/av/playback/ai;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->u:Lcom/twitter/library/av/playback/ai;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/periscope/PeriscopePlayerActivity;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/TwitterUser;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->M:Lcom/twitter/model/core/TwitterUser;

    return-object p1
.end method

.method private a(Lcom/twitter/library/client/Session;)Lyj;
    .locals 6

    .prologue
    .line 171
    new-instance v0, Lyj;

    new-instance v1, Laug;

    new-instance v2, Lwx;

    invoke-direct {v2, p0, p1}, Lwx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-direct {v1, v2}, Laug;-><init>(Lauj;)V

    new-instance v2, Laug;

    new-instance v3, Lww;

    .line 175
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    invoke-direct {v3, v4}, Lww;-><init>(Lcom/twitter/library/provider/t;)V

    invoke-direct {v2, v3}, Laug;-><init>(Lauj;)V

    invoke-direct {v0, v1, v2}, Lyj;-><init>(Lauj;Lauj;)V

    .line 171
    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->E:Lyj;

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->E:Lyj;

    invoke-virtual {v0, p1, p2}, Lyj;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity$3;-><init>(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)V

    .line 184
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->K:Lrx/j;

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V
    .locals 5

    .prologue
    .line 334
    iput-object p1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    .line 335
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->setAppCardViewProvider(Lcom/twitter/android/periscope/c;)V

    .line 336
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->o:Lcom/twitter/android/periscope/f;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->setBroadcastProgress(Lcom/twitter/android/periscope/f;)V

    .line 337
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    iget-boolean v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->q:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->setIsLive(Z)V

    .line 338
    iget-wide v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->P:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->J()V

    .line 343
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s:Lcom/twitter/android/periscope/t;

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/periscope/t;->a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V

    .line 344
    new-instance v0, Lcom/twitter/android/periscope/j;

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    .line 345
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->d:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->c:Lcom/twitter/android/av/GalleryVideoChromeView;

    invoke-virtual {v4}, Lcom/twitter/android/av/GalleryVideoChromeView;->getControls()Landroid/view/View;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/periscope/j;-><init>(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;Lcom/twitter/internal/android/widget/ToolBar;Landroid/view/ViewGroup;Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/j;

    .line 346
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->r()V

    .line 348
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->c()Lcom/twitter/android/av/GalleryVideoChromeView;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/model/av/AVMedia;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/GalleryVideoChromeView;->setShouldShowControls(Z)V

    .line 350
    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s:Lcom/twitter/android/periscope/t;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/GalleryVideoChromeView;->setControlsListener(Lcom/twitter/library/av/control/VideoControlView$a;)V

    .line 351
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->I()V

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 279
    invoke-virtual {p3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 280
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 282
    new-instance v0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity$4;-><init>(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->setProfileClickListener(Lcom/twitter/android/av/PeriscopeFullscreenChromeView$a;)V

    .line 289
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->M:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->M:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p1, v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->setProfileUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 293
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 294
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 295
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/a;

    const/16 v3, 0x4001

    invoke-direct {v1, p0, v3}, Lcom/twitter/android/moments/ui/fullscreen/a;-><init>(Landroid/app/Activity;I)V

    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 299
    new-instance v3, Labt;

    invoke-direct {v3, p2, p3, v0, v4}, Labt;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;I)V

    .line 301
    new-instance v4, Lcom/twitter/android/periscope/o;

    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->L:Lcom/twitter/model/core/Tweet;

    invoke-direct {v4, p0, v1, v0}, Lcom/twitter/android/periscope/o;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/a;Lcom/twitter/model/core/Tweet;)V

    .line 303
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bo;

    move-object v1, p0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/fullscreen/bo;-><init>(Landroid/content/Context;Lcom/twitter/library/client/p;Labt;Lcom/twitter/android/moments/ui/fullscreen/ca;ZZ)V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->N:Lcom/twitter/android/moments/ui/fullscreen/bo;

    .line 305
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->N:Lcom/twitter/android/moments/ui/fullscreen/bo;

    new-instance v1, Lcom/twitter/android/periscope/PeriscopePlayerActivity$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity$5;-><init>(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a(Lcom/twitter/android/moments/ui/fullscreen/bj$a;)V

    .line 312
    iget-wide v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->F:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 313
    new-instance v1, Lais;

    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->G:Lcom/twitter/library/client/p;

    iget-object v3, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->M:Lcom/twitter/model/core/TwitterUser;

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lais;-><init>(Lcom/twitter/library/provider/t;Lcom/twitter/library/client/p;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    .line 315
    new-instance v0, Laio;

    invoke-direct {v0, v1}, Laio;-><init>(Lair;)V

    .line 318
    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->N:Lcom/twitter/android/moments/ui/fullscreen/bo;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a(Lain;)V

    .line 319
    return-void
.end method

.method private a(Lcom/twitter/library/av/playback/ai;Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V
    .locals 1

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->n()V

    .line 269
    iput-object p1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->u:Lcom/twitter/library/av/playback/ai;

    .line 270
    invoke-direct {p0, p2}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V

    .line 272
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->B:Z

    .line 273
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->l()V

    .line 274
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/PeriscopePlayerActivity;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->O:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/moments/ui/fullscreen/bo;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->N:Lcom/twitter/android/moments/ui/fullscreen/bo;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->M:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)Lcom/twitter/android/av/PeriscopeFullscreenChromeView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/periscope/PeriscopePlayerActivity;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->i()V

    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->N:Lcom/twitter/android/moments/ui/fullscreen/bo;

    if-nez v0, :cond_0

    .line 212
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-wide v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->P:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(J)V

    .line 208
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->b(Z)V

    .line 209
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->N:Lcom/twitter/android/moments/ui/fullscreen/bo;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bo;->b()Z

    .line 210
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->N:Lcom/twitter/android/moments/ui/fullscreen/bo;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a()V

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->O:Z

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 215
    invoke-static {p0}, Lcom/twitter/android/periscope/k;->a(Landroid/content/Context;)Lcom/twitter/android/periscope/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/k;

    .line 216
    invoke-static {}, Ltv/periscope/android/library/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/k;

    invoke-static {v0, v1}, Ltv/periscope/android/library/d;->a(Landroid/content/Context;Ltv/periscope/android/library/c;)V

    .line 219
    :cond_0
    return-void
.end method

.method private l()V
    .locals 8

    .prologue
    .line 322
    new-instance v4, Ltv/periscope/android/ui/broadcast/w;

    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    invoke-direct {v4, v0}, Ltv/periscope/android/ui/broadcast/w;-><init>(Ltv/periscope/android/player/c;)V

    .line 323
    iget-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->q:Z

    if-eqz v0, :cond_0

    sget-object v3, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    .line 324
    :goto_0
    new-instance v0, Lcom/twitter/android/periscope/m;

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->u:Lcom/twitter/library/av/playback/ai;

    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/k;

    .line 325
    invoke-virtual {v2}, Lcom/twitter/android/periscope/k;->h()Lcyn;

    move-result-object v2

    invoke-direct {v0, v4, v1, v2}, Lcom/twitter/android/periscope/m;-><init>(Ltv/periscope/android/player/b;Lcom/twitter/library/av/playback/ai;Lcyn;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->w:Lcom/twitter/android/periscope/m;

    .line 326
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->w:Lcom/twitter/android/periscope/m;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/m;->a()V

    .line 327
    invoke-static {}, Ltv/periscope/android/library/d;->a()Ltv/periscope/android/library/d;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Ltv/periscope/android/library/d;->e()Lcxf;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->p:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->w:Lcom/twitter/android/periscope/m;

    iget-object v6, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->w:Lcom/twitter/android/periscope/m;

    move-object v1, p0

    move-object v7, p0

    .line 329
    invoke-interface/range {v0 .. v7}, Lcxf;->a(Landroid/app/Activity;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Ltv/periscope/android/player/b;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Lcxe;)V

    .line 331
    return-void

    .line 323
    :cond_0
    sget-object v3, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 354
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->w:Lcom/twitter/android/periscope/m;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->w:Lcom/twitter/android/periscope/m;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/m;->b()V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s:Lcom/twitter/android/periscope/t;

    invoke-virtual {v0, v2}, Lcom/twitter/android/periscope/t;->a(Lcxb;)V

    .line 359
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s:Lcom/twitter/android/periscope/t;

    invoke-virtual {v0, v2}, Lcom/twitter/android/periscope/t;->a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V

    .line 361
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->k()Lcom/twitter/android/periscope/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/periscope/g;->a()V

    .line 362
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->l()Lcom/twitter/android/periscope/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/periscope/g;->a()V

    .line 363
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->t:Lcom/twitter/android/periscope/k;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/k;->m()Lcom/twitter/android/periscope/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/periscope/g;->a()V

    .line 365
    iput-boolean v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->B:Z

    .line 366
    iput-boolean v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->O:Z

    .line 367
    return-void
.end method

.method private o()Landroid/view/View;
    .locals 4

    .prologue
    .line 372
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->C:Lcom/twitter/model/businessprofiles/d;

    iget-object v0, v0, Lcom/twitter/model/businessprofiles/d;->f:Lcom/twitter/model/businessprofiles/b;

    iget-object v0, v0, Lcom/twitter/model/businessprofiles/b;->e:Lcax;

    .line 373
    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->l:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 374
    invoke-static {p0, v2, v3, v0}, Lcom/twitter/library/card/ac;->a(Landroid/app/Activity;JLcax;)Lcom/twitter/library/card/ac;

    move-result-object v1

    .line 376
    invoke-interface {v1}, Lcom/twitter/library/widget/renderablecontent/d;->bg_()V

    .line 377
    invoke-interface {v1}, Lcom/twitter/library/widget/renderablecontent/d;->c()V

    .line 378
    invoke-interface {v1}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v1

    .line 379
    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->l:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(JLcax;)Lcom/twitter/android/card/d;

    move-result-object v0

    const-string/jumbo v2, "impression"

    const-string/jumbo v3, "platform_card"

    .line 380
    invoke-interface {v0, v2, v3}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    return-object v1
.end method

.method private q()V
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->D:Lcom/twitter/android/periscope/c$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->C:Lcom/twitter/model/businessprofiles/d;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->D:Lcom/twitter/android/periscope/c$a;

    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->o()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/periscope/c$a;->a(Landroid/view/View;)V

    .line 400
    :cond_0
    return-void
.end method

.method private r()V
    .locals 4

    .prologue
    .line 500
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/j;

    if-eqz v0, :cond_1

    .line 501
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 502
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 503
    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->z:Landroid/view/Display;

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 504
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 505
    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->z:Landroid/view/Display;

    invoke-virtual {v2, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 507
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->x:Lcom/twitter/android/periscope/j;

    iget-boolean v3, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->b:Z

    invoke-virtual {v2, v1, v0, v3}, Lcom/twitter/android/periscope/j;->a(Landroid/graphics/Point;Landroid/graphics/Point;Z)V

    .line 509
    :cond_1
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    .line 512
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->y:Lcom/twitter/android/av/ag;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/av/ag;->a(J)V

    .line 513
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/periscope/c$a;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->D:Lcom/twitter/android/periscope/c$a;

    .line 387
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->q()V

    .line 388
    return-void
.end method

.method public a(Lcom/twitter/model/businessprofiles/d;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->C:Lcom/twitter/model/businessprofiles/d;

    .line 393
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->q()V

    .line 394
    return-void
.end method

.method public a(Lcxb;)V
    .locals 2

    .prologue
    .line 467
    const-string/jumbo v0, "PeriscopePlayerActivity"

    const-string/jumbo v1, "Loaded broadcast successfully"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s:Lcom/twitter/android/periscope/t;

    invoke-virtual {v0, p1}, Lcom/twitter/android/periscope/t;->a(Lcxb;)V

    .line 469
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->r()V

    .line 470
    return-void
.end method

.method protected a(Lsl;Z)V
    .locals 8

    .prologue
    .line 223
    invoke-super {p0, p1, p2}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->a(Lsl;Z)V

    .line 224
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    .line 225
    check-cast p1, Lsr;

    .line 226
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->g()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v1

    .line 227
    iget-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->B:Z

    if-nez v0, :cond_0

    instance-of v0, v1, Lcom/twitter/library/av/playback/ai;

    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {p1}, Lsr;->g()Lcom/twitter/library/av/control/e;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    .line 229
    check-cast v1, Lcom/twitter/library/av/playback/ai;

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(Lcom/twitter/library/av/playback/ai;Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V

    .line 231
    invoke-virtual {p1}, Lsr;->h()Lcom/twitter/library/av/VideoPlayerView;

    move-result-object v1

    .line 232
    invoke-virtual {p1}, Lsr;->i()Landroid/view/ViewGroup;

    move-result-object v2

    .line 233
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-wide v4, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->P:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 235
    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 241
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/library/PeriscopeException;)V
    .locals 2

    .prologue
    .line 474
    const-string/jumbo v0, "PeriscopePlayerActivity"

    const-string/jumbo v1, "Failed to load broadcast"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 419
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 420
    const v2, 0x7f1308b9

    if-ne v0, v2, :cond_3

    .line 421
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    if-eqz v0, :cond_0

    .line 423
    iget-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->A:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->A:Z

    .line 425
    iget-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->A:Z

    if-eqz v0, :cond_2

    .line 426
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d()V

    .line 427
    const v0, 0x7f0a063c

    .line 433
    :goto_1
    invoke-interface {p1, v0}, Lcmm;->g(I)Lcmm;

    .line 437
    :cond_0
    :goto_2
    return v1

    .line 423
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 429
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->v:Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    invoke-virtual {v0}, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e()V

    .line 430
    const v0, 0x7f0a0642

    goto :goto_1

    .line 437
    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->a(Lcmm;)Z

    move-result v1

    goto :goto_2
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 412
    invoke-super {p0, p1}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->a(Lcmr;)Z

    .line 413
    const v0, 0x7f140021

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 414
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 130
    invoke-super {p0, p1, p2}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 133
    const-string/jumbo v0, "broadcast_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->p:Ljava/lang/String;

    .line 134
    const-string/jumbo v0, "is_live"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->q:Z

    .line 135
    const-string/jumbo v0, "tw"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->L:Lcom/twitter/model/core/Tweet;

    .line 136
    const-string/jumbo v0, "broadcaster_twitter_user_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->P:J

    .line 139
    new-instance v0, Lcom/twitter/android/periscope/PeriscopePlayerActivity$2;

    invoke-direct {v0, p0, p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity$2;-><init>(Lcom/twitter/android/periscope/PeriscopePlayerActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->r:Landroid/view/OrientationEventListener;

    .line 152
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(Lcom/twitter/library/client/Session;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->E:Lyj;

    .line 154
    new-instance v0, Lcom/twitter/android/av/ag;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->y:Lcom/twitter/android/av/ag;

    .line 155
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->y:Lcom/twitter/android/av/ag;

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->n:Lcom/twitter/android/av/aj$a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/ag;->a(Lcom/twitter/android/av/aj$a;)V

    .line 156
    new-instance v0, Lcom/twitter/android/periscope/t;

    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->y:Lcom/twitter/android/av/ag;

    invoke-direct {v0, v1}, Lcom/twitter/android/periscope/t;-><init>(Lcom/twitter/android/av/ag;)V

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s:Lcom/twitter/android/periscope/t;

    .line 157
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s()V

    .line 159
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->j()V

    .line 160
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->z:Landroid/view/Display;

    .line 162
    new-instance v0, Lcom/twitter/android/businessprofiles/d;

    .line 163
    invoke-virtual {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v5, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->l:Lcom/twitter/model/core/TwitterUser;

    .line 164
    invoke-static {p0}, Lbld;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/businessprofiles/d;-><init>(Landroid/content/Context;Lcom/twitter/android/businessprofiles/c;Landroid/support/v4/app/LoaderManager;ILcom/twitter/model/core/TwitterUser;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->m:Lcom/twitter/android/profiles/x;

    invoke-virtual {v1, v0}, Lcom/twitter/android/profiles/x;->a(Lcom/twitter/android/profiles/w;)V

    .line 166
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->m:Lcom/twitter/android/profiles/x;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/x;->a()V

    .line 167
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->O:Z

    if-eqz v0, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->b(Z)V

    .line 257
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->r()V

    .line 259
    if-eqz p1, :cond_0

    .line 260
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->s()V

    goto :goto_0
.end method

.method public c(I)V
    .locals 8

    .prologue
    .line 448
    invoke-super {p0, p1}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->c(I)V

    .line 449
    iget-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->B:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 450
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->g()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/library/av/playback/ai;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->f:Lsj;

    .line 452
    invoke-virtual {v0, p1}, Lsj;->b(I)Lsl;

    move-result-object v0

    check-cast v0, Lsr;

    .line 453
    invoke-virtual {v0}, Lsr;->g()Lcom/twitter/library/av/control/e;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/av/PeriscopeFullscreenChromeView;

    .line 454
    iget-object v2, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->g()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/av/playback/ai;

    invoke-direct {p0, v2, v1}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(Lcom/twitter/library/av/playback/ai;Lcom/twitter/android/av/PeriscopeFullscreenChromeView;)V

    .line 456
    invoke-virtual {v0}, Lsr;->h()Lcom/twitter/library/av/VideoPlayerView;

    move-result-object v2

    .line 457
    invoke-virtual {v0}, Lsr;->i()Landroid/view/ViewGroup;

    move-result-object v0

    .line 458
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-wide v4, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->P:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 460
    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(Lcom/twitter/android/av/PeriscopeFullscreenChromeView;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 463
    :cond_0
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->n()V

    .line 496
    invoke-super {p0}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->d()V

    .line 497
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 245
    invoke-super {p0, p1}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 246
    invoke-direct {p0}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->r()V

    .line 247
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 442
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->B:Z

    .line 443
    invoke-super {p0}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->onRestart()V

    .line 444
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 479
    invoke-super {p0}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->onStart()V

    .line 480
    iget-wide v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->P:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->a(J)V

    .line 481
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->r:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 482
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->K:Lrx/j;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->K:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/PeriscopePlayerActivity;->r:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 490
    invoke-super {p0}, Lcom/twitter/android/FullscreenMediaPlayerActivity;->onStop()V

    .line 491
    return-void
.end method
