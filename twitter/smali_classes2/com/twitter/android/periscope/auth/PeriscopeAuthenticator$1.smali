.class Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;Lcom/twitter/android/periscope/q;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

.field final synthetic b:Lcom/twitter/android/periscope/q;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Lcom/twitter/library/client/Session;

.field final synthetic e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;Lcom/twitter/android/periscope/q;Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    iput-object p2, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    iput-object p3, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->b:Lcom/twitter/android/periscope/q;

    iput-object p4, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->c:Landroid/content/Context;

    iput-object p5, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->d:Lcom/twitter/library/client/Session;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V
    .locals 5

    .prologue
    .line 179
    sget-object v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$2;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 181
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    invoke-interface {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;->i()V

    .line 182
    invoke-static {}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->b:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0, p1}, Lcom/twitter/android/periscope/q;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V

    goto :goto_0

    .line 190
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    invoke-interface {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;->b()V

    goto :goto_0

    .line 194
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-static {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)Ltv/periscope/android/library/c;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    .line 195
    invoke-static {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)Ltv/periscope/android/library/c;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v0

    invoke-interface {v0}, Lcyw;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-static {v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)Ltv/periscope/android/library/c;

    move-result-object v1

    invoke-interface {v1}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v1

    invoke-interface {v1}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;->a(Ltv/periscope/android/api/PsUser;)V

    goto :goto_0

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-static {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;

    .line 201
    new-instance v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;-><init>(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V

    .line 218
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-static {v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/periscope/a;

    iget-object v3, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->d:Lcom/twitter/library/client/Session;

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/api/periscope/a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 219
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0

    .line 179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 176
    check-cast p1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V

    return-void
.end method
