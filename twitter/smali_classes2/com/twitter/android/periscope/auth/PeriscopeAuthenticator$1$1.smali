.class Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

.field final synthetic b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    iput-object p2, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 201
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 204
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 205
    check-cast v0, Lcom/twitter/library/api/periscope/a;

    .line 206
    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/a;->b()Lcgj;

    move-result-object v0

    .line 207
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcgj;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    iget-object v1, v1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->b:Lcom/twitter/android/periscope/q;

    iget-object v2, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    invoke-virtual {v1, v2}, Lcom/twitter/android/periscope/q;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;)V

    .line 209
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    iget-object v1, v1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    invoke-static {v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)Ltv/periscope/android/library/c;

    move-result-object v1

    invoke-interface {v1}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    iget-object v0, v0, Lcgj;->a:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Ltv/periscope/android/api/ApiManager;->loginTwitterToken(Ljava/lang/String;Z)Ljava/lang/String;

    .line 216
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    iget-object v0, v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    iget-object v1, v1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->b:Lcom/twitter/android/periscope/q;

    invoke-static {v0, p1, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Lcom/twitter/library/service/s;Lcom/twitter/android/periscope/q;)V

    goto :goto_0

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    iget-object v0, v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->e:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;

    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1$1;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    iget-object v1, v1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;->b:Lcom/twitter/android/periscope/q;

    invoke-static {v0, p1, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Lcom/twitter/library/service/s;Lcom/twitter/android/periscope/q;)V

    goto :goto_0
.end method
