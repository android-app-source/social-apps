.class public Lcom/twitter/android/periscope/auth/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/periscope/auth/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/periscope/auth/c$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private final b:Lcom/twitter/android/periscope/auth/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/auth/a;

    invoke-direct {v1, p1}, Lcom/twitter/android/periscope/auth/a;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/periscope/auth/c;-><init>(Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/auth/a;)V

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/auth/a;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/c;->a:Lcom/twitter/library/client/p;

    .line 32
    iput-object p2, p0, Lcom/twitter/android/periscope/auth/c;->b:Lcom/twitter/android/periscope/auth/a;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {p2}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    sget-object v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;->a:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/periscope/auth/c;->b(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/auth/c$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/periscope/auth/c$1;-><init>(Lcom/twitter/android/periscope/auth/c;)V

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/c;->b:Lcom/twitter/android/periscope/auth/a;

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, p3, v2, v3}, Lcom/twitter/android/periscope/auth/a;->a(ZJ)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/c;->a:Lcom/twitter/library/client/p;

    new-instance v1, Lcom/twitter/library/api/periscope/c;

    invoke-direct {v1, p1, p2, p3}, Lcom/twitter/library/api/periscope/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 66
    return-void
.end method

.method public b(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/c;->b:Lcom/twitter/android/periscope/auth/a;

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/periscope/auth/a;->a(J)Ljava/lang/Boolean;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    .line 55
    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/android/periscope/auth/c$a;

    iget-object v1, p0, Lcom/twitter/android/periscope/auth/c;->a:Lcom/twitter/library/client/p;

    iget-object v2, p0, Lcom/twitter/android/periscope/auth/c;->b:Lcom/twitter/android/periscope/auth/a;

    new-instance v3, Lcom/twitter/library/api/periscope/b;

    invoke-direct {v3, p1, p2}, Lcom/twitter/library/api/periscope/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/periscope/auth/c$a;-><init>(Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/auth/a;Lcom/twitter/library/api/periscope/b;Lcom/twitter/android/periscope/auth/c$1;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method
