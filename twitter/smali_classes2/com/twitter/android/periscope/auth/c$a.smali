.class final Lcom/twitter/android/periscope/auth/c$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/periscope/auth/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private final b:Lcom/twitter/library/api/periscope/b;

.field private final c:Lcom/twitter/android/periscope/auth/a;


# direct methods
.method private constructor <init>(Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/auth/a;Lcom/twitter/library/api/periscope/b;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/c$a;->a:Lcom/twitter/library/client/p;

    .line 77
    iput-object p2, p0, Lcom/twitter/android/periscope/auth/c$a;->c:Lcom/twitter/android/periscope/auth/a;

    .line 78
    iput-object p3, p0, Lcom/twitter/android/periscope/auth/c$a;->b:Lcom/twitter/library/api/periscope/b;

    .line 79
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/auth/a;Lcom/twitter/library/api/periscope/b;Lcom/twitter/android/periscope/auth/c$1;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/periscope/auth/c$a;-><init>(Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/auth/a;Lcom/twitter/library/api/periscope/b;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/auth/c$a;)Lcom/twitter/library/api/periscope/b;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/c$a;->b:Lcom/twitter/library/api/periscope/b;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/periscope/auth/c$a;)Lcom/twitter/android/periscope/auth/a;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/c$a;->c:Lcom/twitter/android/periscope/auth/a;

    return-object v0
.end method


# virtual methods
.method public a(Lrx/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/c$a;->a:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/periscope/auth/c$a;->b:Lcom/twitter/library/api/periscope/b;

    new-instance v2, Lcom/twitter/android/periscope/auth/c$a$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/periscope/auth/c$a$1;-><init>(Lcom/twitter/android/periscope/auth/c$a;Lrx/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 94
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/auth/c$a;->a(Lrx/i;)V

    return-void
.end method
