.class Lcom/twitter/android/periscope/auth/c$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/auth/c;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/lang/Boolean;",
        "Lrx/c",
        "<",
        "Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/periscope/auth/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/auth/c;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/c$1;->a:Lcom/twitter/android/periscope/auth/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/auth/c$1;->a(Ljava/lang/Boolean;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Boolean;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;->c:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    :goto_0
    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;->b:Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;

    goto :goto_0
.end method
