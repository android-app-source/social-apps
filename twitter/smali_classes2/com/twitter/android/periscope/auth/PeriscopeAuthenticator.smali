.class public Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;,
        Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;,
        Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$AuthState;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private final b:Ltv/periscope/android/library/c;

.field private final c:Lafb;

.field private final d:Lcom/twitter/android/periscope/auth/b;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/twitter/library/client/Session;

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/library/c;Lafb;)V
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/auth/c;

    invoke-direct {v1, p1}, Lcom/twitter/android/periscope/auth/c;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;-><init>(Lcom/twitter/library/client/p;Ltv/periscope/android/library/c;Lafb;Lcom/twitter/android/periscope/auth/b;)V

    .line 60
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/client/p;Ltv/periscope/android/library/c;Lafb;Lcom/twitter/android/periscope/auth/b;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->e:Ljava/util/Set;

    .line 67
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a:Lcom/twitter/library/client/p;

    .line 68
    iput-object p2, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    .line 69
    iput-object p3, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c:Lafb;

    .line 70
    iput-object p4, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->d:Lcom/twitter/android/periscope/auth/b;

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)Ltv/periscope/android/library/c;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Lcom/twitter/library/service/s;Lcom/twitter/android/periscope/q;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/library/service/s;Lcom/twitter/android/periscope/q;)V

    return-void
.end method

.method private a(Lcom/twitter/library/service/s;Lcom/twitter/android/periscope/q;)V
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f()V

    .line 234
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    invoke-interface {v0, p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;->a(Lcom/twitter/library/service/s;)V

    .line 238
    invoke-virtual {p2}, Lcom/twitter/android/periscope/q;->e()V

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/client/Session;)Z
    .locals 1

    .prologue
    .line 90
    invoke-static {p0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->B:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->e()V

    return-void
.end method

.method public static b(Lcom/twitter/library/client/Session;)Z
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->d()Z

    move-result v0

    return v0
.end method

.method private static d()Z
    .locals 2

    .prologue
    .line 107
    const-string/jumbo v0, "android_periscope_auth_broadcasting_v2_5514"

    const-string/jumbo v1, "enabled"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 115
    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->d:Lcom/twitter/android/periscope/auth/b;

    const/4 v1, 0x1

    invoke-interface {v0, p1, p2, v1}, Lcom/twitter/android/periscope/auth/b;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V

    .line 164
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;Lcom/twitter/android/periscope/q;)V
    .locals 6

    .prologue
    .line 175
    invoke-virtual {p0, p2}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c(Lcom/twitter/library/client/Session;)V

    .line 176
    new-instance v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$1;-><init>(Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;Lcom/twitter/android/periscope/q;Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 228
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->d:Lcom/twitter/android/periscope/auth/b;

    invoke-interface {v1, p1, p2}, Lcom/twitter/android/periscope/auth/b;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;

    move-result-object v1

    .line 229
    invoke-virtual {v1, v0}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 230
    return-void
.end method

.method public varargs a([Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->e:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 297
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/session/a;->c()V

    .line 151
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v0

    invoke-interface {v0}, Lcyw;->h()V

    .line 152
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v0

    invoke-interface {v0}, Lcyn;->a()V

    .line 153
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->p()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 154
    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    if-ne v0, p1, :cond_1

    .line 147
    :cond_0
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b()V

    .line 135
    :cond_2
    iput-object p1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    .line 136
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c:Lafb;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lafb;->a(J)Ltv/periscope/android/api/PsUser;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_3

    .line 138
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v1

    invoke-interface {v1, v0}, Lcyw;->a(Ltv/periscope/android/api/PsUser;)V

    .line 140
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c:Lafb;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lafb;->c(J)Ljava/lang/String;

    move-result-object v0

    .line 141
    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 142
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v1

    sget-object v2, Ltv/periscope/android/session/Session$Type;->b:Ltv/periscope/android/session/Session$Type;

    invoke-interface {v1, v0, v2}, Ltv/periscope/android/session/a;->a(Ljava/lang/String;Ltv/periscope/android/session/Session$Type;)V

    .line 144
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;

    .line 145
    invoke-interface {v0, p1}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;->a(Lcom/twitter/library/client/Session;)V

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 6

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f()V

    .line 244
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    sget-object v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$2;->b:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 249
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 250
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;

    .line 251
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    iget-object v2, v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;->user:Ltv/periscope/android/api/PsUser;

    invoke-interface {v1, v2}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;->a(Ltv/periscope/android/api/PsUser;)V

    .line 252
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c:Lafb;

    iget-object v2, v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;->user:Ltv/periscope/android/api/PsUser;

    iget-object v3, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lafb;->a(Ltv/periscope/android/api/PsUser;J)V

    .line 253
    iget-object v1, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->c:Lafb;

    iget-object v0, v0, Ltv/periscope/android/api/TwitterTokenLoginResponse;->cookie:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->f:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lafb;->a(Ljava/lang/String;J)V

    goto :goto_0

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;

    invoke-interface {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$b;->i()V

    goto :goto_0

    .line 247
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
