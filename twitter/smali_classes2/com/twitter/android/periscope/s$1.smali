.class Lcom/twitter/android/periscope/s$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/periscope/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/ui/broadcast/aq$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/media/model/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/drafts/a$a;

.field final synthetic b:Ltv/periscope/android/ui/broadcast/aq$a;

.field final synthetic c:Lcom/twitter/android/periscope/s;


# direct methods
.method constructor <init>(Lcom/twitter/android/periscope/s;Lcom/twitter/model/drafts/a$a;Ltv/periscope/android/ui/broadcast/aq$a;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/twitter/android/periscope/s$1;->c:Lcom/twitter/android/periscope/s;

    iput-object p2, p0, Lcom/twitter/android/periscope/s$1;->a:Lcom/twitter/model/drafts/a$a;

    iput-object p3, p0, Lcom/twitter/android/periscope/s$1;->b:Ltv/periscope/android/ui/broadcast/aq$a;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/model/MediaFile;)V
    .locals 3

    .prologue
    .line 76
    sget-object v0, Lcom/twitter/model/media/MediaSource;->c:Lcom/twitter/model/media/MediaSource;

    .line 77
    invoke-static {p1, v0}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/twitter/android/periscope/s$1;->a:Lcom/twitter/model/drafts/a$a;

    new-instance v2, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v2, v0}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    invoke-static {v2}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/s$1;->c:Lcom/twitter/android/periscope/s;

    invoke-static {v0}, Lcom/twitter/android/periscope/s;->a(Lcom/twitter/android/periscope/s;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/periscope/s$1;->c:Lcom/twitter/android/periscope/s;

    invoke-static {v0}, Lcom/twitter/android/periscope/s;->b(Lcom/twitter/android/periscope/s;)Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/periscope/s$1;->a:Lcom/twitter/model/drafts/a$a;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    invoke-static {v1, v2, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcom/twitter/android/periscope/s$1;->b:Ltv/periscope/android/ui/broadcast/aq$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/aq$a;->a()V

    .line 81
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 73
    check-cast p1, Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/s$1;->a(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/periscope/s$1;->b:Ltv/periscope/android/ui/broadcast/aq$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/aq$a;->b()V

    .line 86
    return-void
.end method
