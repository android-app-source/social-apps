.class Lcom/twitter/android/periscope/w;
.super Ltv/periscope/android/ui/broadcast/m;
.source "Twttr"

# interfaces
.implements Lczi$b;
.implements Lczi$e;


# instance fields
.field private final b:Lcom/twitter/library/av/playback/ai;


# direct methods
.method constructor <init>(Ltv/periscope/android/player/c;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcyn;)V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/android/periscope/n;

    invoke-direct {v0, p2}, Lcom/twitter/android/periscope/n;-><init>(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    invoke-direct {p0, p1, v0, p3}, Ltv/periscope/android/ui/broadcast/m;-><init>(Ltv/periscope/android/player/c;Ltv/periscope/android/ui/broadcast/ai;Lcyn;)V

    .line 25
    invoke-virtual {p2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->g()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/ai;

    iput-object v0, p0, Lcom/twitter/android/periscope/w;->b:Lcom/twitter/library/av/playback/ai;

    .line 26
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/ai;->a(Lczi$e;)V

    .line 30
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/ai;->a(Lczi$b;)V

    .line 31
    return-void
.end method

.method public a(IIIF)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/l;->a(IIIF)V

    .line 58
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Ltv/periscope/android/ui/broadcast/l;->a(JZ)V

    .line 80
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/l;->a(Ljava/lang/Exception;)V

    .line 50
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/metadata/id3/Id3Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/l;->a(Ljava/util/List;)V

    .line 72
    :cond_0
    return-void
.end method

.method public a(ZI)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/l;->a(ZI)V

    .line 43
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/ai;->a(Lczi$e;)V

    .line 35
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->b:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/ai;->a(Lczi$b;)V

    .line 36
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/twitter/android/periscope/w;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->c()V

    .line 65
    :cond_0
    return-void
.end method
