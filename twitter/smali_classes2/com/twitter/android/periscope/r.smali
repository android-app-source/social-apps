.class public Lcom/twitter/android/periscope/r;
.super Lcom/twitter/android/dialog/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/periscope/r$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/twitter/android/dialog/g;-><init>(Landroid/os/Bundle;)V

    .line 17
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/android/periscope/r;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/android/periscope/r;

    invoke-direct {v0, p0}, Lcom/twitter/android/periscope/r;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/periscope/r;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "twitter:username"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()Z
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/periscope/r;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "twitter:nsfw_user"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
