.class Lcom/twitter/android/periscope/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldae;


# instance fields
.field final a:Ljava/util/List;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/media/manager/g;


# direct methods
.method constructor <init>(Lcom/twitter/library/media/manager/g;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/twitter/android/periscope/g;->b:Lcom/twitter/library/media/manager/g;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/periscope/g;->a:Ljava/util/List;

    .line 52
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 153
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/periscope/g;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 154
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 155
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/periscope/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 158
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 97
    invoke-virtual {p3, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/io/File;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {p2, v0}, Lcom/twitter/media/model/MediaFile;->b(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/periscope/g$3;

    invoke-direct {v1, p0, p3}, Lcom/twitter/android/periscope/g$3;-><init>(Lcom/twitter/android/periscope/g;Landroid/widget/ImageView;)V

    .line 103
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 109
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;IILdae$a;)V
    .locals 2

    .prologue
    .line 140
    invoke-static {p3, p4}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 141
    new-instance v1, Lcom/twitter/android/periscope/g$6;

    invoke-direct {v1, p0, p5}, Lcom/twitter/android/periscope/g$6;-><init>(Lcom/twitter/android/periscope/g;Ldae$a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 147
    invoke-virtual {p0, v0}, Lcom/twitter/android/periscope/g;->a(Lcom/twitter/media/request/a;)V

    .line 148
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    const/16 v0, 0x100

    .line 91
    invoke-static {v0, v0}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 92
    invoke-virtual {p0, v0, p3}, Lcom/twitter/android/periscope/g;->a(Lcom/twitter/media/request/a;Landroid/widget/ImageView;)V

    .line 93
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ldae$b;)V
    .locals 3

    .prologue
    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 126
    const/16 v1, 0x140

    const/16 v2, 0x238

    .line 127
    invoke-static {v1, v2}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v1

    .line 126
    invoke-static {p2, v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 127
    invoke-virtual {v1}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v1

    .line 128
    new-instance v2, Lcom/twitter/android/periscope/g$5;

    invoke-direct {v2, p0, p3, v0}, Lcom/twitter/android/periscope/g$5;-><init>(Lcom/twitter/android/periscope/g;Ldae$b;Landroid/content/res/Resources;)V

    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 134
    invoke-virtual {p0, v1}, Lcom/twitter/android/periscope/g;->a(Lcom/twitter/media/request/a;)V

    .line 135
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;D)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;D)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-static {p4, p5}, Lcom/twitter/util/math/Size;->a(D)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0, p3}, Lcom/twitter/android/periscope/g;->a(Lcom/twitter/media/request/a;Ljava/util/Map;)V

    .line 65
    return-void
.end method

.method protected a(Lcom/twitter/media/request/a;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/periscope/g;->b:Lcom/twitter/library/media/manager/g;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/g;->b(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/twitter/android/periscope/g;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v1, Lcom/twitter/android/periscope/g$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/periscope/g$2;-><init>(Lcom/twitter/android/periscope/g;Lcom/twitter/util/concurrent/g;)V

    invoke-interface {v0, v1}, Lcom/twitter/util/concurrent/g;->a(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 87
    return-void
.end method

.method a(Lcom/twitter/media/request/a;Landroid/widget/ImageView;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 113
    new-instance v0, Lcom/twitter/android/periscope/g$4;

    invoke-direct {v0, p0, p2}, Lcom/twitter/android/periscope/g$4;-><init>(Lcom/twitter/android/periscope/g;Landroid/widget/ImageView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 119
    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/g;->a(Lcom/twitter/media/request/a;)V

    .line 120
    return-void
.end method

.method a(Lcom/twitter/media/request/a;Ljava/util/Map;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/a;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/android/periscope/g$1;

    invoke-direct {v0, p0, p2}, Lcom/twitter/android/periscope/g$1;-><init>(Lcom/twitter/android/periscope/g;Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 75
    invoke-virtual {p0, p1}, Lcom/twitter/android/periscope/g;->a(Lcom/twitter/media/request/a;)V

    .line 76
    return-void
.end method
