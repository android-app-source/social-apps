.class public abstract Lcom/twitter/android/SearchFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/SearchFragment$a;,
        Lcom/twitter/android/SearchFragment$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A:",
        "Lcjr",
        "<TT;>;>",
        "Lcom/twitter/android/TweetListFragment",
        "<TT;TA;>;"
    }
.end annotation


# static fields
.field protected static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected A:Ljava/lang/String;

.field protected B:Ljava/lang/String;

.field protected C:Ljava/lang/String;

.field protected D:Ljava/lang/String;

.field protected E:Lcom/twitter/android/SearchFragment$b;

.field protected F:Lbqn;

.field private final aa:Landroid/os/Handler;

.field private final ab:Ljava/lang/Runnable;

.field private ac:I

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Landroid/content/SharedPreferences;

.field private ah:Lcom/twitter/android/SearchFragment$a;

.field protected b:Z

.field protected c:Z

.field protected d:Z

.field protected e:Z

.field protected f:Z

.field protected g:Z

.field protected h:Z

.field protected i:Z

.field protected j:Z

.field protected k:I

.field protected l:I

.field protected m:I

.field protected n:I

.field protected o:I

.field protected p:I

.field protected q:J

.field protected r:J

.field protected s:Ljava/lang/String;

.field protected t:Ljava/lang/String;

.field protected u:Ljava/lang/String;

.field protected v:Ljava/lang/String;

.field protected w:Ljava/lang/String;

.field protected x:Ljava/lang/String;

.field protected y:Ljava/lang/String;

.field protected z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 97
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    .line 108
    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-wide/16 v2, 0x2710

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 109
    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 110
    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 111
    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-wide/32 v2, 0x1d4c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 112
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    .line 128
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/SearchFragment;->p:I

    .line 149
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->aa:Landroid/os/Handler;

    .line 151
    new-instance v0, Lcom/twitter/android/SearchFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/SearchFragment$1;-><init>(Lcom/twitter/android/SearchFragment;)V

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->ab:Ljava/lang/Runnable;

    return-void
.end method

.method private M()V
    .locals 4

    .prologue
    .line 466
    new-instance v0, Lcom/twitter/library/api/search/e;

    .line 467
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/search/e;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x1

    .line 468
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/e;->d(I)Lcom/twitter/library/service/j;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/e;

    .line 469
    iget-wide v2, p0, Lcom/twitter/android/SearchFragment;->r:J

    iput-wide v2, v0, Lcom/twitter/library/api/search/e;->a:J

    .line 470
    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->S:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 471
    return-void
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 535
    packed-switch p0, :pswitch_data_0

    .line 564
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 538
    :pswitch_1
    const-string/jumbo v0, "everything"

    goto :goto_0

    .line 541
    :pswitch_2
    const-string/jumbo v0, "people"

    goto :goto_0

    .line 544
    :pswitch_3
    const-string/jumbo v0, "photos"

    goto :goto_0

    .line 548
    :pswitch_4
    const-string/jumbo v0, "video"

    goto :goto_0

    .line 552
    :pswitch_5
    const-string/jumbo v0, "periscope"

    goto :goto_0

    .line 555
    :pswitch_6
    const-string/jumbo v0, "news"

    goto :goto_0

    .line 558
    :pswitch_7
    const-string/jumbo v0, "eventsummary"

    goto :goto_0

    .line 561
    :pswitch_8
    const-string/jumbo v0, "geo"

    goto :goto_0

    .line 535
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->c:Z

    return v0
.end method

.method protected B()I
    .locals 1

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->ad:Z

    if-eqz v0, :cond_0

    .line 580
    const/4 v0, 0x6

    .line 582
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/android/SearchFragment;->ac:I

    goto :goto_0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 587
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->ae:Z

    return v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 591
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->af:Z

    return v0
.end method

.method protected abstract E()Ljava/lang/String;
.end method

.method protected abstract F()V
.end method

.method protected abstract G()V
.end method

.method public G_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 441
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 442
    iget v0, p0, Lcom/twitter/android/SearchFragment;->n:I

    if-lez v0, :cond_0

    .line 443
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->s()V

    .line 444
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->A:Ljava/lang/String;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    .line 445
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->E()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "new_tweet_prompt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "click"

    aput-object v2, v0, v1

    .line 444
    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a(Ljava/lang/String;)V

    .line 447
    :cond_0
    return-void
.end method

.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->k()Lcom/twitter/android/bs;

    move-result-object v0

    return-object v0
.end method

.method protected H_()V
    .locals 1

    .prologue
    .line 343
    iget v0, p0, Lcom/twitter/android/SearchFragment;->m:I

    if-lez v0, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->s()V

    .line 348
    :goto_0
    return-void

    .line 346
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a(I)Z

    goto :goto_0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->k()Lcom/twitter/android/bs;

    move-result-object v0

    return-object v0
.end method

.method protected I_()V
    .locals 4

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    iget-wide v0, p0, Lcom/twitter/android/SearchFragment;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, Lcom/twitter/android/SearchFragment;->q:J

    .line 412
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->aa:Landroid/os/Handler;

    iget-object v3, p0, Lcom/twitter/android/SearchFragment;->ab:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 413
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->aa:Landroid/os/Handler;

    iget-object v3, p0, Lcom/twitter/android/SearchFragment;->ab:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 410
    :cond_2
    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    iget v1, p0, Lcom/twitter/android/SearchFragment;->o:I

    const-wide/32 v2, 0x493e0

    .line 411
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_1
.end method

.method protected J_()V
    .locals 4

    .prologue
    .line 420
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->ag:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 421
    const-string/jumbo v1, "refresh_time"

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 422
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 423
    return-void
.end method

.method protected K_()Z
    .locals 1

    .prologue
    .line 499
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    return v0
.end method

.method public a(J)I
    .locals 3

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SearchFragment;->b(J)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 273
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 274
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 275
    return-object v0
.end method

.method protected abstract a(Landroid/content/Context;)V
.end method

.method public a(Lcno;IIIZ)V
    .locals 1

    .prologue
    .line 478
    if-eqz p3, :cond_0

    if-eqz p5, :cond_0

    .line 479
    if-nez p2, :cond_1

    .line 480
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->y()V

    .line 486
    :cond_0
    :goto_0
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/TweetListFragment;->a(Lcno;IIIZ)V

    .line 487
    return-void

    .line 481
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->k()I

    move-result v0

    if-lez v0, :cond_0

    if-lez p2, :cond_0

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    .line 483
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->l()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/android/SearchFragment$a;)V
    .locals 0

    .prologue
    .line 629
    iput-object p1, p0, Lcom/twitter/android/SearchFragment;->ah:Lcom/twitter/android/SearchFragment$a;

    .line 630
    return-void
.end method

.method public a(Lcom/twitter/android/SearchFragment$b;)V
    .locals 1

    .prologue
    .line 595
    iput-object p1, p0, Lcom/twitter/android/SearchFragment;->E:Lcom/twitter/android/SearchFragment$b;

    .line 596
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/android/SearchFragment;->m:I

    if-lez v0, :cond_0

    .line 597
    iget v0, p0, Lcom/twitter/android/SearchFragment;->m:I

    invoke-interface {p1, v0}, Lcom/twitter/android/SearchFragment$b;->a(I)V

    .line 599
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 6

    .prologue
    .line 331
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 332
    invoke-virtual {p1}, Lcom/twitter/app/common/list/l$d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f04012b

    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/common/list/h;

    const/4 v2, 0x1

    const-wide/32 v4, -0x80000000

    .line 334
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/app/common/list/h;-><init>(ZLjava/util/Set;)V

    .line 333
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->a(Lcom/twitter/app/common/list/h;)Lcom/twitter/app/common/list/l$d;

    .line 335
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    const v0, 0x7f0403e0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 337
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 339
    :cond_0
    return-void

    .line 332
    :cond_1
    const v0, 0x7f040124

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/api/search/d;)V
    .locals 1

    .prologue
    .line 602
    invoke-static {p1}, Lcom/twitter/android/events/a;->a(Lcom/twitter/library/api/search/d;)V

    .line 603
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->v:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/library/api/search/d;->b(Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->w:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 607
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/library/api/search/d;->c(Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 609
    :cond_1
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 369
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 370
    instance-of v0, p1, Lcom/twitter/library/api/search/d;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 371
    check-cast v0, Lcom/twitter/library/api/search/d;

    .line 372
    if-ne p2, v1, :cond_1

    .line 373
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 374
    iget v2, p0, Lcom/twitter/android/SearchFragment;->n:I

    invoke-virtual {v0}, Lcom/twitter/library/api/search/d;->h()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/twitter/android/SearchFragment;->n:I

    .line 375
    invoke-virtual {v0}, Lcom/twitter/library/api/search/d;->s()I

    move-result v2

    .line 376
    if-lez v2, :cond_0

    .line 377
    if-ne v2, v1, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/api/search/d;->C()Lcom/twitter/model/topic/TwitterTopic;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 378
    :goto_0
    iget v3, p0, Lcom/twitter/android/SearchFragment;->m:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/twitter/android/SearchFragment;->m:I

    .line 379
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->E:Lcom/twitter/android/SearchFragment$b;

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->E:Lcom/twitter/android/SearchFragment$b;

    iget v2, p0, Lcom/twitter/android/SearchFragment;->m:I

    invoke-interface {v1, v2}, Lcom/twitter/android/SearchFragment$b;->a(I)V

    .line 384
    :cond_0
    iget v1, p0, Lcom/twitter/android/SearchFragment;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/android/SearchFragment;->o:I

    .line 386
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->I_()V

    .line 387
    invoke-virtual {v0}, Lcom/twitter/library/api/search/d;->B()Lcom/twitter/model/topic/TwitterTopic;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchFragment;->a(Lcom/twitter/model/topic/TwitterTopic;)V

    .line 390
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/api/search/d;->t()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/SearchFragment;->q:J

    .line 391
    invoke-virtual {v0}, Lcom/twitter/library/api/search/d;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->D:Ljava/lang/String;

    .line 393
    :cond_2
    return-void

    .line 377
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/model/topic/TwitterTopic;)V
    .locals 3

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->aH()Lcmt;

    move-result-object v1

    .line 397
    invoke-virtual {v1}, Lcmt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 398
    const-class v0, Lcom/twitter/model/topic/d;

    invoke-virtual {p1, v0}, Lcom/twitter/model/topic/TwitterTopic;->a(Ljava/lang/Class;)Lcom/twitter/model/topic/b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/d;

    .line 399
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/twitter/model/topic/d;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 400
    iget-object v0, v0, Lcom/twitter/model/topic/d;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcmt;->b(Ljava/lang/CharSequence;)Z

    .line 403
    :cond_0
    return-void
.end method

.method protected abstract a(Ljava/lang/String;)V
.end method

.method protected abstract a(I)Z
.end method

.method protected aP_()V
    .locals 1

    .prologue
    .line 315
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->aP_()V

    .line 316
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->F()V

    .line 317
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->i:Z

    if-eqz v0, :cond_0

    .line 318
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->ar_()V

    .line 320
    :cond_0
    return-void
.end method

.method protected abstract b(J)I
.end method

.method public b()V
    .locals 0

    .prologue
    .line 309
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->b()V

    .line 310
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->ar_()V

    .line 311
    return-void
.end method

.method public abstract e()I
.end method

.method public k()Lcom/twitter/android/bs;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/bs;->a(Landroid/os/Bundle;)Lcom/twitter/android/bs;

    move-result-object v0

    return-object v0
.end method

.method protected l()V
    .locals 2

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->g:Z

    if-nez v0, :cond_0

    .line 492
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->k()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    .line 493
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a(I)Z

    .line 495
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 352
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 353
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->ah:Lcom/twitter/android/SearchFragment$a;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->ah:Lcom/twitter/android/SearchFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/SearchFragment$a;->a()V

    .line 356
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 176
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 178
    if-eqz p1, :cond_2

    .line 179
    const-string/jumbo v2, "search_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/SearchFragment;->r:J

    .line 180
    const-string/jumbo v2, "is_last"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->g:Z

    .line 181
    const-string/jumbo v2, "q_source"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->u:Ljava/lang/String;

    .line 182
    const-string/jumbo v2, "query_rewrite_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->v:Ljava/lang/String;

    .line 183
    const-string/jumbo v2, "data_lookup_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->w:Ljava/lang/String;

    .line 184
    const-string/jumbo v2, "polled_organic_count"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->m:I

    .line 185
    const-string/jumbo v2, "polled_total_count"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->n:I

    .line 186
    const-string/jumbo v2, "poll_count"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->o:I

    .line 187
    const-string/jumbo v2, "should_poll"

    invoke-virtual {p1, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->h:Z

    .line 188
    const-string/jumbo v2, "should_refresh"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->i:Z

    .line 189
    const-string/jumbo v2, "follows"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->c:Z

    .line 190
    const-string/jumbo v2, "near"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->b:Z

    .line 191
    const-string/jumbo v2, "terminal"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->ae:Z

    .line 192
    const-string/jumbo v2, "search_button"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->af:Z

    .line 193
    const-string/jumbo v2, "seed_hashtag"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->x:Ljava/lang/String;

    .line 194
    const-string/jumbo v2, "query_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->s:Ljava/lang/String;

    .line 195
    const-string/jumbo v2, "timeline_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->y:Ljava/lang/String;

    .line 196
    const-string/jumbo v2, "experiments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->z:Ljava/lang/String;

    .line 197
    const-string/jumbo v2, "scribe_page"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->A:Ljava/lang/String;

    .line 198
    const-string/jumbo v2, "event_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->C:Ljava/lang/String;

    .line 199
    const-string/jumbo v2, "event_type"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchFragment;->p:I

    .line 200
    const-string/jumbo v0, "notification_setting_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->B:Ljava/lang/String;

    .line 201
    const-string/jumbo v0, "is_saved"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->ad:Z

    .line 202
    const-string/jumbo v0, "request_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->D:Ljava/lang/String;

    .line 240
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->B:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->f:Z

    .line 241
    iget v0, p0, Lcom/twitter/android/SearchFragment;->k:I

    if-nez v0, :cond_1

    .line 242
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/SearchFragment;->k:I

    .line 245
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 246
    const-string/jumbo v2, "search"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->ag:Landroid/content/SharedPreferences;

    .line 247
    invoke-static {v0}, Lbqn;->a(Landroid/content/Context;)Lbqn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->F:Lbqn;

    .line 249
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->k()Lcom/twitter/android/bs;

    move-result-object v0

    .line 252
    const-string/jumbo v2, "query"

    invoke-virtual {v0, v2}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 253
    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->t:Ljava/lang/String;

    .line 255
    const-string/jumbo v3, "query_name"

    invoke-virtual {v0, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 256
    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 257
    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->s:Ljava/lang/String;

    .line 262
    :goto_1
    const-string/jumbo v2, "q_type"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->ac:I

    .line 263
    invoke-virtual {v0}, Lcom/twitter/android/bs;->b()I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->l:I

    .line 265
    invoke-virtual {v0}, Lcom/twitter/android/bs;->c()Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->d:Z

    .line 266
    const-string/jumbo v2, "realtime"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->e:Z

    .line 267
    const-string/jumbo v2, "follows"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->c:Z

    .line 268
    const-string/jumbo v2, "near"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->b:Z

    .line 269
    return-void

    .line 204
    :cond_2
    iput-boolean v1, p0, Lcom/twitter/android/SearchFragment;->g:Z

    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->k()Lcom/twitter/android/bs;

    move-result-object v2

    .line 206
    const-string/jumbo v3, "fetch_type"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->b(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/SearchFragment;->k:I

    .line 207
    invoke-virtual {v2}, Lcom/twitter/android/bs;->d()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/SearchFragment;->r:J

    .line 208
    const-string/jumbo v3, "q_source"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->u:Ljava/lang/String;

    .line 209
    const-string/jumbo v3, "query_rewrite_id"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->v:Ljava/lang/String;

    .line 210
    const-string/jumbo v3, "data_lookup_id"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->w:Ljava/lang/String;

    .line 211
    const-string/jumbo v3, "terminal"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->ae:Z

    .line 212
    const-string/jumbo v3, "search_button"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->af:Z

    .line 213
    const-string/jumbo v3, "should_poll"

    invoke-virtual {v2, v3, v6}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->h:Z

    .line 214
    const-string/jumbo v3, "should_refresh"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->i:Z

    .line 215
    const-string/jumbo v3, "seed_hashtag"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->x:Ljava/lang/String;

    .line 216
    const-string/jumbo v3, "query_name"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->s:Ljava/lang/String;

    .line 217
    const-string/jumbo v3, "timeline_type"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->y:Ljava/lang/String;

    .line 218
    const-string/jumbo v3, "experiments"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->z:Ljava/lang/String;

    .line 219
    const-string/jumbo v3, "scribe_page"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->A:Ljava/lang/String;

    .line 220
    const-string/jumbo v3, "follows"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->c:Z

    .line 221
    const-string/jumbo v3, "near"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/bs;->a(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->b:Z

    .line 222
    const-string/jumbo v3, "event_id"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->C:Ljava/lang/String;

    .line 223
    const-string/jumbo v3, "event_type"

    invoke-virtual {v2, v3, v0}, Lcom/twitter/android/bs;->a(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/SearchFragment;->p:I

    .line 224
    const-string/jumbo v3, "notification_setting_key"

    invoke-virtual {v2, v3}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->B:Ljava/lang/String;

    .line 225
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->A:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 226
    const-string/jumbo v2, "search"

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->A:Ljava/lang/String;

    .line 228
    :cond_3
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->u:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->B:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 229
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->B:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_4
    :goto_2
    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 231
    :pswitch_0
    const-string/jumbo v0, "evpa"

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 229
    :pswitch_1
    const-string/jumbo v3, "event_parrot"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_2

    .line 259
    :cond_5
    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->s:Ljava/lang/String;

    goto/16 :goto_1

    .line 229
    :pswitch_data_0
    .packed-switch 0x390582db
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 280
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 281
    const-string/jumbo v0, "should_poll"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 282
    const-string/jumbo v0, "should_refresh"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 283
    const-string/jumbo v0, "follows"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 284
    const-string/jumbo v0, "near"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 285
    const-string/jumbo v0, "terminal"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->ae:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 286
    const-string/jumbo v0, "search_button"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->af:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 287
    const-string/jumbo v0, "is_last"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 288
    const-string/jumbo v0, "is_saved"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->ad:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 289
    const-string/jumbo v0, "polled_organic_count"

    iget v1, p0, Lcom/twitter/android/SearchFragment;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 290
    const-string/jumbo v0, "polled_total_count"

    iget v1, p0, Lcom/twitter/android/SearchFragment;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 291
    const-string/jumbo v0, "poll_count"

    iget v1, p0, Lcom/twitter/android/SearchFragment;->o:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 292
    const-string/jumbo v0, "event_type"

    iget v1, p0, Lcom/twitter/android/SearchFragment;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 293
    const-string/jumbo v0, "search_id"

    iget-wide v2, p0, Lcom/twitter/android/SearchFragment;->r:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 294
    const-string/jumbo v0, "q_source"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string/jumbo v0, "query_rewrite_id"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-string/jumbo v0, "data_lookup_id"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string/jumbo v0, "query_name"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const-string/jumbo v0, "seed_hashtag"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string/jumbo v0, "timeline_type"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const-string/jumbo v0, "experiments"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-string/jumbo v0, "scribe_page"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const-string/jumbo v0, "notification_setting_key"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string/jumbo v0, "event_id"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string/jumbo v0, "request_url"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->aa:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->ab:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 325
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->G()V

    .line 326
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onStop()V

    .line 327
    return-void
.end method

.method protected q()Z
    .locals 6

    .prologue
    .line 429
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 430
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->ag:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "refresh_time"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 431
    iget-boolean v4, p0, Lcom/twitter/android/SearchFragment;->i:Z

    if-eqz v4, :cond_0

    const-wide/32 v4, 0xdbba0

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract r()Z
.end method

.method protected s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 452
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->E:Lcom/twitter/android/SearchFragment$b;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->E:Lcom/twitter/android/SearchFragment$b;

    invoke-interface {v0}, Lcom/twitter/android/SearchFragment$b;->a()V

    .line 455
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/SearchFragment;->k:I

    .line 456
    invoke-direct {p0}, Lcom/twitter/android/SearchFragment;->M()V

    .line 457
    iput v1, p0, Lcom/twitter/android/SearchFragment;->o:I

    .line 458
    iput v1, p0, Lcom/twitter/android/SearchFragment;->m:I

    .line 459
    iput v1, p0, Lcom/twitter/android/SearchFragment;->n:I

    .line 460
    return-void
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->s:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->s:Ljava/lang/String;

    .line 506
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->t:Ljava/lang/String;

    goto :goto_0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->t:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->j:Z

    return v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->D:Ljava/lang/String;

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->x:Ljava/lang/String;

    .line 531
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->t:Ljava/lang/String;

    goto :goto_0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 570
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->b:Z

    return v0
.end method
