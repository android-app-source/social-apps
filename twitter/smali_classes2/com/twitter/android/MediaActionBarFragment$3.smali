.class final Lcom/twitter/android/MediaActionBarFragment$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/core/Tweet;I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

.field final synthetic b:Lcom/twitter/model/core/Tweet;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/twitter/android/MediaActionBarFragment$3;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    iput-object p2, p0, Lcom/twitter/android/MediaActionBarFragment$3;->b:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 421
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment$3;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 423
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/MediaActionBarFragment$3;->b:Lcom/twitter/model/core/Tweet;

    .line 424
    invoke-static {v1, v0, v3}, Lbhb;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lbhb;

    move-result-object v0

    new-instance v3, Lcom/twitter/android/MediaActionBarFragment$3$1;

    invoke-direct {v3, p0, v1}, Lcom/twitter/android/MediaActionBarFragment$3$1;-><init>(Lcom/twitter/android/MediaActionBarFragment$3;Landroid/content/Context;)V

    .line 423
    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 440
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 441
    const-string/jumbo v1, "deleted"

    iget-object v2, p0, Lcom/twitter/android/MediaActionBarFragment$3;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->u:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 444
    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment$3;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 446
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment$3;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 447
    return-void
.end method
