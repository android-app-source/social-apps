.class public Lcom/twitter/android/ci;
.super Lcom/twitter/android/ah;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/ah",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/library/client/p;

.field private final e:Landroid/content/Context;

.field private final f:I

.field private final g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/content/Context;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/twitter/android/ah;-><init>(Lcom/twitter/library/client/v;)V

    .line 33
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ci;->b:Ljava/util/Set;

    .line 35
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ci;->c:Ljava/util/List;

    .line 49
    iput-object p2, p0, Lcom/twitter/android/ci;->d:Lcom/twitter/library/client/p;

    .line 50
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ci;->e:Landroid/content/Context;

    .line 51
    iput p4, p0, Lcom/twitter/android/ci;->f:I

    .line 52
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    invoke-static {p5, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/ci;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 53
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 7

    .prologue
    .line 65
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ci;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/twitter/android/ci;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/twitter/android/ci;->d:Lcom/twitter/library/client/p;

    new-instance v2, Lbft;

    iget-object v3, p0, Lcom/twitter/android/ci;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/ci;->b:Ljava/util/Set;

    .line 71
    invoke-static {v4}, Lcom/twitter/util/collection/o;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    iget v5, p0, Lcom/twitter/android/ci;->f:I

    invoke-direct {v2, v3, v0, v4, v5}, Lbft;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/Collection;I)V

    .line 70
    invoke-virtual {v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/ci;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/timeline/az;I)V
    .locals 8

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ci;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 106
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    iget-object v5, p1, Lcom/twitter/android/timeline/az;->e:Lcom/twitter/model/timeline/r;

    iget-object v5, v5, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    const-string/jumbo v6, ""

    const-string/jumbo v7, "impression"

    invoke-static {v3, v4, v5, v6, v7}, Lcom/twitter/analytics/model/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v3

    .line 107
    invoke-virtual {v3}, Lcom/twitter/analytics/model/a;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 106
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 108
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/bk;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/android/timeline/bg;->t:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ci;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p1, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    .line 90
    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 91
    iput-object v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 92
    iput p2, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 93
    iget-object v3, p0, Lcom/twitter/android/ci;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/ci;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 95
    invoke-virtual {v6}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/twitter/android/ci;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v6}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x3

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "impression"

    aput-object v1, v4, v0

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 97
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ci;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 99
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 101
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    .line 95
    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 31
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ci;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/twitter/android/ah;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    iget-object v1, p0, Lcom/twitter/android/ci;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_0
    return v0
.end method

.method public b(J)V
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/ci;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ci;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 80
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/ci;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "stream"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "results"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/ci;->c:Ljava/util/List;

    .line 81
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/ci;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 84
    :cond_0
    return-void
.end method
