.class public Lcom/twitter/android/UrlInterpreterActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Ljava/util/regex/Pattern;

.field public static final c:Ljava/util/regex/Pattern;

.field private static final d:Landroid/content/UriMatcher;

.field private static final e:Landroid/content/UriMatcher;

.field private static final f:Landroid/content/UriMatcher;


# instance fields
.field private g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v1, -0x1

    const/16 v7, 0x9f

    const/16 v6, 0x9e

    const/16 v5, 0x55

    const/4 v4, 0x0

    .line 115
    const-string/jumbo v0, "(www\\.)?twitter.com"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/UrlInterpreterActivity;->a:Ljava/util/regex/Pattern;

    .line 116
    const-string/jumbo v0, "https?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/UrlInterpreterActivity;->b:Ljava/util/regex/Pattern;

    .line 117
    const-string/jumbo v0, "twitter"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/UrlInterpreterActivity;->c:Ljava/util/regex/Pattern;

    .line 272
    new-instance v0, Landroid/content/UriMatcher;

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    .line 274
    new-instance v0, Landroid/content/UriMatcher;

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    .line 277
    new-instance v0, Landroid/content/UriMatcher;

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/twitter/android/UrlInterpreterActivity;->f:Landroid/content/UriMatcher;

    .line 296
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "account"

    const/16 v2, 0x6e

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 297
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "bouncer"

    const/16 v2, 0x8b

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 298
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "custom_timeline"

    const/16 v2, 0x77

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 299
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "dm_conversation"

    const/16 v2, 0x83

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 300
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "explore"

    const/16 v2, 0x78

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 301
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "flow"

    const-string/jumbo v2, "ad_hoc"

    const/16 v3, 0x73

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 302
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "flow"

    const-string/jumbo v2, "add_phone"

    const/16 v3, 0x88

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 303
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "flow"

    const-string/jumbo v2, "setup_profile"

    const/16 v3, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 304
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "follow"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 305
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "followers"

    const-string/jumbo v2, "verified"

    const/16 v3, 0x6c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 306
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "front"

    const/16 v2, 0x71

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 307
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "hashtag"

    const-string/jumbo v2, "*"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 308
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "intent"

    const-string/jumbo v2, "favorite"

    const/16 v3, 0x7d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 309
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "intent"

    const-string/jumbo v2, "follow"

    const/16 v3, 0x82

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 310
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "intent"

    const-string/jumbo v2, "like"

    const/16 v3, 0x7d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 311
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "intent"

    const-string/jumbo v2, "retweet"

    const/16 v3, 0x7d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 312
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "internal"

    const-string/jumbo v2, "who_to_follow"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 313
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "list"

    const/16 v2, 0x75

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 314
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "login"

    const/16 v2, 0x7a

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 315
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "login-token"

    const/16 v2, 0x7a

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 316
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "mentions"

    const/16 v2, 0x74

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 317
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "moment"

    const/16 v2, 0x8a

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 318
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "moments"

    const/16 v2, 0x8e

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 319
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "moments"

    const-string/jumbo v2, "maker"

    const/16 v3, 0xa7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 320
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "moments"

    const-string/jumbo v2, "guide"

    const/16 v3, 0x8e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 321
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "moments"

    const-string/jumbo v2, "list"

    const/16 v3, 0xa6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 322
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "news"

    const/16 v2, 0x89

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 323
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "news"

    const-string/jumbo v2, "*"

    const/16 v3, 0x92

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 324
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "photo"

    const/16 v2, 0x7f

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 325
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "post"

    const/16 v2, 0x7e

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 326
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "profiles"

    const-string/jumbo v2, "edit"

    const/16 v3, 0x90

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 327
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "profiles"

    const-string/jumbo v2, "edit/birthday"

    const/16 v3, 0x91

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 328
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "qr_follow"

    const/16 v2, 0xa8

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 329
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "quote"

    const/16 v2, 0x7e

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 330
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "search"

    const/16 v2, 0x79

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 331
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "trends"

    const/16 v2, 0xa3

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 332
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "session"

    const-string/jumbo v2, "new"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 333
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "settings"

    const/16 v2, 0x6f

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 334
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "settings"

    const-string/jumbo v2, "notifications_tab"

    const/16 v3, 0x84

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 335
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "share_via_dm"

    const/16 v2, 0x8c

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 336
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "signup"

    const/16 v2, 0x70

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 337
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "status"

    const/16 v2, 0x7c

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 338
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "storystream"

    const/16 v2, 0x81

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 339
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "timeline"

    const/16 v2, 0x7b

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 340
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "tweet"

    const/16 v2, 0x7c

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 341
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "unfollow"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 342
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "user"

    const/16 v2, 0x76

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 343
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "user"

    const-string/jumbo v2, "media"

    const/16 v3, 0x93

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 344
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "user"

    const-string/jumbo v2, "tweets"

    const/16 v3, 0x9d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 345
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "who_to_follow"

    const-string/jumbo v2, "interests/*"

    const/16 v3, 0x85

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 346
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "settings"

    const-string/jumbo v2, "timeline"

    const/16 v3, 0x2d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 347
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "settings"

    const-string/jumbo v2, "accessibility"

    const/16 v3, 0x4a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 348
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "people_discovery_modules"

    const/16 v2, 0xa2

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 349
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "connect_people"

    const/16 v2, 0x9b

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 350
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "settings"

    const-string/jumbo v2, "backup_code"

    const/16 v3, 0x9c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 351
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "stickers"

    const-string/jumbo v2, "*"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 352
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "stickers"

    const-string/jumbo v2, "*/top"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 353
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "stickers"

    const-string/jumbo v2, "*/all"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 354
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "stickers"

    const-string/jumbo v2, "*/live"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 355
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "messages"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 356
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "messages"

    const-string/jumbo v2, "compose"

    const/16 v3, 0x56

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 357
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "people_address_book"

    const/16 v2, 0xa4

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 358
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const-string/jumbo v1, "golive"

    const/16 v2, 0xa5

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 359
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    const/16 v1, 0xa1

    invoke-static {v0, v1}, Lcom/twitter/android/livevideo/c;->a(Landroid/content/UriMatcher;I)V

    .line 363
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "analytics.twitter.com"

    const-string/jumbo v2, "user/*/tweet/*"

    const/16 v3, 0x51

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 364
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "ads.twitter.com"

    const-string/jumbo v2, "mobile/*/accounts"

    const/16 v3, 0x54

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 365
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "ads.twitter.com"

    const-string/jumbo v2, "mobile/*/accounts/*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 366
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "ads.twitter.com"

    const-string/jumbo v2, "mobile/*/accounts/*/*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 367
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "ads.twitter.com"

    const-string/jumbo v2, "mobile/*/accounts/*/*/*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 368
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "ads.twitter.com"

    const-string/jumbo v2, "mobile/*/accounts/*/*/*/*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 369
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "ads.twitter.com"

    const-string/jumbo v2, "mobile/*/accounts/*/*/*/*/*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 370
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/app_link"

    const/16 v3, 0x4b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 371
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/redirect"

    const/16 v3, 0x35

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 372
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/cricket"

    const/16 v3, 0x4f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 373
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/cricket/*"

    const/16 v3, 0x50

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 374
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/highlights"

    const/16 v3, 0x81

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 375
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/notifications"

    const/16 v3, 0x52

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 376
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/connect"

    const/16 v3, 0x53

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 377
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/verified_followers"

    const/16 v3, 0x4d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 378
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/soccer/*"

    const/16 v3, 0x86

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 379
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/soccer/*/*"

    const/16 v3, 0x87

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 380
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/moments"

    const/16 v3, 0x94

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 381
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/moments/*"

    const/16 v3, 0x8d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 382
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "hashtag/*"

    const/16 v3, 0x3a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 383
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "search"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 384
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "search/users/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 385
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "search/realtime/*"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 386
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "search/links/*"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 387
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "search/*/grid/*"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 388
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "search/*"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 389
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "compose/tweet"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 390
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "compose/dm"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 391
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "compose/dm/*"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 392
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "compose/gifs"

    const/16 v3, 0x99

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 393
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "direct_messages/create/*"

    const/16 v3, 0x39

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 394
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "open_play_store"

    const/16 v3, 0x2e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 395
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "enable_device_follow"

    const/16 v3, 0x2f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 396
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "follow_user/#"

    const/16 v3, 0x30

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 397
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "mentions"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 398
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "messages"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 399
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "messages/media/*"

    const/16 v3, 0x9a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 400
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "direct_messages"

    const/16 v3, 0x38

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 401
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "messages/compose"

    const/16 v3, 0x56

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 402
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "messages/*/#"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 403
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "messages/*"

    const/16 v3, 0x33

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 404
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "who_to_follow"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 405
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "who_to_follow/suggestions"

    const/16 v3, 0xe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 406
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "who_to_follow/interests"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 407
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "who_to_follow/interests/*"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 408
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "who_to_follow/import"

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 409
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "who_to_follow/search/*"

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 410
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "lists"

    const/16 v3, 0x13

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 411
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "favorites"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 412
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "likes"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 413
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "find_friends"

    const/16 v3, 0x29

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 414
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "turn_on_push"

    const/16 v3, 0x2c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 415
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "settings/profile"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 416
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "settings/accessibility"

    const/16 v3, 0x4a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 417
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "similar_to/*"

    const/16 v3, 0x16

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 418
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "share"

    const/16 v3, 0x24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 419
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "intent/tweet"

    const/16 v3, 0x25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 420
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "intent/user"

    const/16 v3, 0x26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 421
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "intent/retweet"

    const/16 v3, 0x3e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 422
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "intent/favorite"

    const/16 v3, 0x3f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 423
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "intent/like"

    const/16 v3, 0x3f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 424
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "intent/follow"

    const/16 v3, 0x4c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 425
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "intent/session"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 426
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "session/new"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 427
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "login"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 428
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "signup"

    const/16 v3, 0x27

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 429
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "people_timeline"

    const/16 v3, 0x2b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 430
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "start_phone_ownership_verification"

    const/16 v3, 0x47

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 431
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "account/confirm_email_reset"

    const/16 v3, 0x4e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 432
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "download"

    const/16 v3, 0x37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 433
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "home"

    const/16 v3, 0x44

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 434
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/stickers/*"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 435
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/stickers/*/top"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 436
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/stickers/*/live"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 437
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "i/stickers/*/all"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 438
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    invoke-static {v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    const/16 v2, 0xa1

    invoke-static {v0, v1, v2}, Lcom/twitter/android/livevideo/c;->a(Landroid/content/UriMatcher;Ljava/util/Set;I)V

    .line 441
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/status/#/photo/#/large"

    const/16 v3, 0x3b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 442
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/status/#/photo/#"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 443
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/status/#/video/#"

    const/16 v3, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 444
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/status/#"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 445
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/statuses/#"

    const/16 v3, 0x36

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 446
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/lists"

    const/16 v3, 0x18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 447
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/lists/*"

    const/16 v3, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 448
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/following"

    const/16 v3, 0x1a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 449
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/following/*"

    const/16 v3, 0x1b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 450
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/followers"

    const/16 v3, 0x1c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 451
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/followers_you_follow"

    const/16 v3, 0x1d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 452
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/favorites"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 453
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/likes"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 454
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/activity"

    const/16 v3, 0x1f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 455
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/alerts"

    const/16 v3, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 456
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/with_replies"

    const/16 v3, 0x48

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 457
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/media"

    const/16 v3, 0x49

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 458
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/tweets"

    const/16 v3, 0x57

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 459
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/timelines/*"

    const/16 v3, 0x34

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 460
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/moments/*"

    const/16 v3, 0x8d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 461
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/*/members"

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 462
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*/*/subscribers"

    const/16 v3, 0x22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 463
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const-string/jumbo v2, "*"

    const/16 v3, 0x23

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 464
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    const-string/jumbo v1, "*"

    const/16 v2, 0x58

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 466
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->f:Landroid/content/UriMatcher;

    const-string/jumbo v1, "profiles"

    const-string/jumbo v2, "vine/enable_display"

    const/16 v3, 0x95

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 467
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->f:Landroid/content/UriMatcher;

    const-string/jumbo v1, "settings"

    const-string/jumbo v2, "auto_mute"

    const/16 v3, 0x98

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 468
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 2

    .prologue
    .line 534
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    if-eqz p3, :cond_0

    .line 536
    sget-object v1, Lcom/twitter/android/UrlInterpreterActivity;->a:Ljava/util/regex/Pattern;

    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 537
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 544
    :goto_0
    return v0

    .line 541
    :cond_0
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    goto :goto_0

    .line 544
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;Z)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 548
    .line 549
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 550
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->e:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 551
    if-ne v0, v1, :cond_0

    if-eqz p2, :cond_0

    .line 552
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->f:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 555
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;ZZ)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 501
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 502
    :goto_0
    if-nez v2, :cond_2

    move v0, v1

    .line 516
    :cond_0
    :goto_1
    return v0

    :cond_1
    move-object v2, v0

    .line 501
    goto :goto_0

    .line 505
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 507
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 512
    :goto_2
    invoke-static {p0, v2, v0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 513
    if-ne v0, v1, :cond_0

    .line 514
    invoke-static {p0, v2, p2}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;Ljava/lang/String;Z)I

    move-result v0

    goto :goto_1

    .line 510
    :cond_3
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1740
    const/4 v0, 0x0

    .line 1741
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1742
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "guide_category_id"

    .line 1743
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1747
    :goto_0
    return-object v0

    .line 1745
    :cond_0
    invoke-static {p0}, Lcom/twitter/android/moments/ui/guide/ac;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1947
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 1948
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    .line 1949
    if-eqz p2, :cond_0

    if-nez v0, :cond_0

    .line 1950
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    .line 1951
    const/4 p1, 0x0

    .line 1954
    :goto_0
    return-object p1

    .line 1953
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1699
    const-string/jumbo v0, "tweet_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1700
    if-nez v1, :cond_0

    .line 1701
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1702
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1732
    :goto_0
    return-object v0

    .line 1705
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1715
    const/4 v0, 0x0

    .line 1719
    :goto_1
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v3, "twitter"

    .line 1720
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "tweet"

    .line 1721
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "status_id"

    .line 1722
    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 1723
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1732
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x1

    .line 1733
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1734
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1707
    :pswitch_0
    const-string/jumbo v0, "email_redirect_retweet"

    goto :goto_1

    .line 1711
    :pswitch_1
    const-string/jumbo v0, "email_redirect_favorite"

    goto :goto_1

    .line 1705
    :pswitch_data_0
    .packed-switch 0x3e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 1861
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1862
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "post"

    .line 1863
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 1864
    const-string/jumbo v0, "text"

    invoke-static {v2, v0, p2}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 1865
    const-string/jumbo v0, "url"

    const-string/jumbo v1, "url"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 1866
    const-string/jumbo v0, "hashtags"

    const-string/jumbo v1, "hashtags"

    .line 1867
    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "UTF8"

    invoke-static {v1, v3}, Lcom/twitter/util/ac;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1866
    invoke-static {v2, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 1868
    const-string/jumbo v0, "via"

    const-string/jumbo v1, "via"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    const-string/jumbo v0, "in_reply_to"

    .line 1871
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v7}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1876
    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 1877
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 1900
    :goto_0
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Landroid/net/Uri;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 1901
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1899
    return-object v0

    .line 1880
    :cond_0
    const-string/jumbo v0, "in_reply_to_usernames"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1881
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, ","

    .line 1882
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1885
    :goto_1
    const-string/jumbo v1, "in_reply_to_status_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 1886
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v1

    .line 1891
    if-eqz v0, :cond_1

    array-length v3, v0

    if-lez v3, :cond_1

    .line 1894
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Ljava/lang/Long;[Ljava/lang/String;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 1895
    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 1882
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1665
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1666
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "user"

    .line 1667
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1668
    const-string/jumbo v1, "user_id"

    invoke-static {v0, v1, p1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 1669
    const-string/jumbo v1, "screen_name"

    invoke-static {v0, v1, p2}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    invoke-direct {p0}, Lcom/twitter/android/UrlInterpreterActivity;->j()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Long;[Ljava/lang/String;)Lcom/twitter/model/core/Tweet;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1788
    new-instance v1, Lcom/twitter/model/core/Tweet$a;

    invoke-direct {v1}, Lcom/twitter/model/core/Tweet$a;-><init>()V

    .line 1789
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/core/Tweet$a;->e(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    .line 1790
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/core/Tweet$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v2

    .line 1791
    array-length v1, p1

    if-le v1, v0, :cond_1

    .line 1792
    new-instance v3, Lcom/twitter/model/core/f$b;

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v3, v1}, Lcom/twitter/model/core/f$b;-><init>(I)V

    move v1, v0

    .line 1794
    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_0

    .line 1795
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0}, Lcom/twitter/model/core/q$a;-><init>()V

    aget-object v4, p1, v1

    .line 1796
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    .line 1797
    invoke-virtual {v0}, Lcom/twitter/model/core/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 1795
    invoke-virtual {v3, v0}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/d;)Lcom/twitter/model/core/f$a;

    .line 1794
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1800
    :cond_0
    new-instance v1, Lcom/twitter/model/core/v$a;

    invoke-direct {v1}, Lcom/twitter/model/core/v$a;-><init>()V

    .line 1801
    invoke-virtual {v3}, Lcom/twitter/model/core/f$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/v$a;->b(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    move-result-object v0

    .line 1802
    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    .line 1800
    invoke-virtual {v2, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/Tweet$a;

    .line 1805
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet$a;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Intent;Lcom/twitter/android/client/notifications/StatusBarNotif;)V
    .locals 2

    .prologue
    .line 1994
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1995
    const-string/jumbo v0, "tw_is_negative_feedback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1996
    const-string/jumbo v0, "tw_negative_feedback_type"

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1997
    const-string/jumbo v0, "tw_negative_feedback_impression_id"

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1999
    :cond_0
    return-void
.end method

.method private static a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1813
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1814
    invoke-virtual {p0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1816
    :cond_0
    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1823
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    .line 1824
    new-instance v1, Lcom/twitter/android/bk;

    .line 1825
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/twitter/android/bk;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Landroid/net/Uri;)V

    .line 1826
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 1828
    invoke-virtual {p0, p2}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/net/Uri;)V

    .line 1829
    return-void
.end method

.method public static a(Landroid/net/Uri;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 491
    invoke-static {p0, v0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;ZZ)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-static {p0}, Lcom/twitter/android/UrlInterpreterActivity;->f(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/UrlInterpreterActivity;)Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/twitter/android/UrlInterpreterActivity;->i:Z

    return v0
.end method

.method private a(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1973
    if-eqz p2, :cond_1

    const-string/jumbo v0, "smart_follow"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1975
    const-string/jumbo v0, "interest_persistence_source_location_rux_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977
    sget-object v0, Lcom/twitter/model/stratostore/SourceLocation;->c:Lcom/twitter/model/stratostore/SourceLocation;

    .line 1981
    :goto_0
    new-instance v2, Lcom/twitter/android/smartfollow/d;

    invoke-direct {v2}, Lcom/twitter/android/smartfollow/d;-><init>()V

    const-string/jumbo v3, "url_interpreter"

    .line 1982
    invoke-virtual {v2, v3}, Lcom/twitter/android/smartfollow/d;->a(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;

    move-result-object v2

    .line 1983
    invoke-virtual {v2, v1}, Lcom/twitter/android/smartfollow/d;->a(I)Lcom/twitter/android/smartfollow/d;

    move-result-object v2

    .line 1984
    invoke-virtual {v2, v1}, Lcom/twitter/android/smartfollow/d;->a(Z)Lcom/twitter/android/smartfollow/d;

    move-result-object v2

    .line 1985
    invoke-virtual {v2, v0}, Lcom/twitter/android/smartfollow/d;->a(Lcom/twitter/model/stratostore/SourceLocation;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    const-string/jumbo v2, "resurrection"

    .line 1986
    invoke-virtual {v0, v2}, Lcom/twitter/android/smartfollow/d;->b(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 1987
    invoke-virtual {v0, p0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1981
    invoke-virtual {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 1990
    :goto_1
    return v0

    .line 1979
    :cond_0
    sget-object v0, Lcom/twitter/model/stratostore/SourceLocation;->a:Lcom/twitter/model/stratostore/SourceLocation;

    goto :goto_0

    .line 1990
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Landroid/content/Intent;Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1964
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1965
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1968
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/net/Uri;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1648
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/people/PeopleDiscoveryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1649
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    .line 1648
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1650
    if-eqz v0, :cond_0

    .line 1651
    const-string/jumbo v1, "is_internal"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1653
    :cond_0
    return-object v0
.end method

.method private b(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1677
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    .line 1678
    invoke-virtual {v0, p1}, Lcom/twitter/app/users/f;->a(Ljava/lang/String;)Lcom/twitter/app/users/f;

    move-result-object v0

    const/4 v1, 0x6

    .line 1679
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    const-string/jumbo v1, "url_interpreter"

    .line 1680
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->e(Ljava/lang/String;)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1681
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->a(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1682
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->b(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1683
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->e(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1684
    invoke-virtual {v0, p0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1677
    return-object v0
.end method

.method private c(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1689
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1690
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "user"

    .line 1691
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    .line 1692
    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1693
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1694
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 1693
    return-object v0
.end method

.method public static c_(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 481
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method

.method private static f(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 495
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->b:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->a:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "compose"

    const-string/jumbo v1, "action"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 495
    :goto_0
    return v0

    .line 497
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1638
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/net/Uri;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private h(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1643
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/net/Uri;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 6

    .prologue
    .line 630
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "shortcut"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 631
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 632
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "app:url_interpreter:shortcut:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ":open"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 634
    :cond_0
    return-void
.end method

.method private j()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1658
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "start_page"

    sget-object v2, Lcom/twitter/android/ProfileActivity;->j:Landroid/net/Uri;

    .line 1659
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1660
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 1658
    return-object v0
.end method

.method private l()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2006
    invoke-static {p0}, Lcom/twitter/util/u;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2007
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/twitter/util/u;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x40000000    # 2.0f

    .line 2008
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 2012
    :goto_0
    return-object v0

    .line 2010
    :cond_0
    const v0, 0x7f0a06a3

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2012
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/net/Uri;J)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1847
    const-string/jumbo v0, "action"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    .line 1849
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1851
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/twitter/database/schema/a$z;->b:Landroid/net/Uri;

    .line 1852
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 1853
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    .line 1854
    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1855
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1852
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1856
    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 577
    const/16 v0, 0x1a

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 579
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 580
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 581
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 582
    return-object p2
.end method

.method public a(J)Lcom/twitter/library/client/Session;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1938
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/v;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method b()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1833
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method protected b(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 589
    invoke-direct {p0}, Lcom/twitter/android/UrlInterpreterActivity;->i()V

    .line 590
    invoke-virtual {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 591
    if-eqz v0, :cond_0

    .line 592
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 624
    :goto_0
    return-void

    .line 594
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->c(Landroid/net/Uri;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/UrlInterpreterActivity$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/UrlInterpreterActivity$3;-><init>(Lcom/twitter/android/UrlInterpreterActivity;)V

    .line 595
    invoke-interface {v0, v1}, Lcom/twitter/util/concurrent/g;->b(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/UrlInterpreterActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/UrlInterpreterActivity$2;-><init>(Lcom/twitter/android/UrlInterpreterActivity;)V

    .line 608
    invoke-interface {v0, v1}, Lcom/twitter/util/concurrent/g;->d(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/UrlInterpreterActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/UrlInterpreterActivity$1;-><init>(Lcom/twitter/android/UrlInterpreterActivity;)V

    .line 614
    invoke-interface {v0, v1}, Lcom/twitter/util/concurrent/g;->c(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 561
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v1, "permalink"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 562
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 564
    const-string/jumbo v2, "is_from_umf_prompt"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UrlInterpreterActivity;->h:Z

    .line 566
    if-eqz v1, :cond_0

    .line 567
    invoke-static {v1}, Lcom/twitter/util/ac;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/net/Uri;)V

    .line 569
    :cond_0
    return-void
.end method

.method protected c(Landroid/net/Uri;)Lcom/twitter/util/concurrent/g;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/twitter/util/concurrent/g",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 638
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 639
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v5

    .line 640
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v6

    .line 641
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 642
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    .line 643
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v7

    .line 644
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->d()Z

    move-result v8

    .line 646
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    .line 647
    const/4 v1, 0x0

    iget-boolean v10, p0, Lcom/twitter/android/UrlInterpreterActivity;->h:Z

    invoke-static {p1, v1, v10}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;ZZ)I

    move-result v10

    .line 648
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 649
    if-eqz v1, :cond_0

    .line 650
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v1}, Lcom/twitter/util/ac;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v11, v1}, Lcom/twitter/android/util/AppEventTrack;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 653
    :cond_0
    const/4 v1, 0x0

    .line 654
    packed-switch v10, :pswitch_data_0

    .line 1607
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "twitter.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    .line 1608
    if-eqz v0, :cond_37

    if-eqz v9, :cond_1

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 1609
    :cond_1
    if-eqz v8, :cond_36

    .line 1611
    const-string/jumbo v0, "action"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1612
    const-string/jumbo v1, "mode"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1613
    const-string/jumbo v2, "compose"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    const-string/jumbo v0, "poll"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 1614
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    const/4 v1, 0x5

    .line 1615
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(I)Lcom/twitter/android/composer/a;

    move-result-object v0

    const/high16 v1, 0x4000000

    .line 1616
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->d(I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 1617
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 1633
    :cond_2
    :goto_1
    invoke-static {v1}, Lcom/twitter/util/concurrent/ObservablePromise;->a(Ljava/lang/Object;)Lcom/twitter/util/concurrent/ObservablePromise;

    move-result-object v0

    return-object v0

    .line 656
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/PhoneOwnershipActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 660
    :pswitch_2
    if-eqz v9, :cond_2

    .line 661
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "statusId"

    const/4 v0, 0x2

    .line 662
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    goto :goto_1

    .line 670
    :pswitch_3
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 671
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    .line 672
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "status_id"

    const/4 v0, 0x2

    .line 673
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 674
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 675
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_1

    .line 679
    :pswitch_4
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/trends/d;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 683
    :pswitch_5
    const-string/jumbo v0, "query"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 684
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/search/SearchFieldActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 688
    :cond_3
    const-string/jumbo v0, "query"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 689
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    .line 690
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 691
    const-string/jumbo v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 692
    if-eqz v0, :cond_2

    .line 693
    const-string/jumbo v2, "event_id"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "terminal"

    const/4 v3, 0x1

    .line 694
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_1

    .line 700
    :pswitch_6
    const-string/jumbo v0, "q"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 701
    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 702
    const-string/jumbo v0, "src"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 704
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 705
    const-string/jumbo v0, "api_call"

    .line 709
    :cond_4
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    .line 710
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "q_source"

    .line 711
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 712
    const-string/jumbo v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 713
    if-eqz v0, :cond_2

    .line 714
    const-string/jumbo v2, "event_id"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "terminal"

    const/4 v3, 0x1

    .line 715
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_1

    .line 723
    :pswitch_7
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    const/4 v0, 0x1

    .line 724
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "api_call"

    .line 725
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 730
    :pswitch_8
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    const/4 v0, 0x2

    .line 731
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "realtime"

    const/4 v2, 0x1

    .line 732
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "api_call"

    .line 733
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 738
    :pswitch_9
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    const/4 v0, 0x2

    .line 739
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "api_call"

    .line 740
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 745
    :pswitch_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 746
    const-string/jumbo v1, "src"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 747
    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    .line 748
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "q_source"

    .line 749
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 754
    :pswitch_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 755
    const-string/jumbo v1, "src"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 756
    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    .line 757
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "q_source"

    .line 758
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 762
    :pswitch_c
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->b(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 767
    :pswitch_d
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_account_id"

    .line 768
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 772
    :pswitch_e
    if-eqz v8, :cond_5

    .line 773
    const/4 v0, 0x1

    invoke-static {p0, v2, v3, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Landroid/content/Context;JZ)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 775
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 781
    :pswitch_f
    sget-object v0, Lcom/twitter/android/TabbedVitFollowersActivity;->d:Landroid/net/Uri;

    invoke-static {p0, v2, v3, v7, v0}, Lcom/twitter/android/util/i;->a(Landroid/content/Context;JLcom/twitter/model/core/TwitterUser;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 786
    :pswitch_10
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 787
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "favorites"

    .line 788
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "screen_name"

    const/4 v0, 0x0

    .line 789
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 790
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "start_page"

    sget-object v3, Lcom/twitter/android/ProfileActivity;->d:Landroid/net/Uri;

    .line 791
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 792
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 793
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 797
    :pswitch_11
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/app/lists/ListTabActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    const/4 v0, 0x0

    .line 798
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "slug"

    const/4 v0, 0x2

    .line 799
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 803
    :pswitch_12
    const-string/jumbo v0, "screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 804
    const-string/jumbo v1, "slug"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 805
    const-string/jumbo v2, "members"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v2

    .line 806
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/app/lists/ListTabActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "screen_name"

    .line 807
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "slug"

    .line 808
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 809
    if-eqz v2, :cond_2

    .line 810
    const-string/jumbo v0, "tab"

    const-string/jumbo v2, "list_members"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 815
    :pswitch_13
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 816
    invoke-static {p0}, Lcom/twitter/android/client/y;->a(Landroid/content/Context;)Lcom/twitter/android/client/y;

    move-result-object v1

    .line 817
    invoke-virtual {v1}, Lcom/twitter/android/client/y;->b()Ljava/util/List;

    move-result-object v1

    .line 818
    if-eqz v1, :cond_6

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 819
    :cond_6
    invoke-direct {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 821
    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 837
    :pswitch_14
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 838
    invoke-direct {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 840
    sparse-switch v10, :sswitch_data_0

    .line 858
    const/4 v0, 0x0

    .line 862
    :goto_2
    if-eqz v0, :cond_2

    .line 863
    const-string/jumbo v2, "start_page"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 842
    :sswitch_0
    sget-object v0, Lcom/twitter/android/ProfileActivity;->b:Landroid/net/Uri;

    goto :goto_2

    .line 846
    :sswitch_1
    sget-object v0, Lcom/twitter/android/ProfileActivity;->c:Landroid/net/Uri;

    goto :goto_2

    .line 850
    :sswitch_2
    sget-object v0, Lcom/twitter/android/ProfileActivity;->h:Landroid/net/Uri;

    goto :goto_2

    .line 854
    :sswitch_3
    sget-object v0, Lcom/twitter/android/ProfileActivity;->i:Landroid/net/Uri;

    goto :goto_2

    .line 869
    :pswitch_15
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/app/lists/ListTabActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    const/4 v0, 0x0

    .line 870
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "slug"

    const/4 v0, 0x1

    .line 871
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 876
    :pswitch_16
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/app/lists/ListTabActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    const/4 v0, 0x0

    .line 877
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "slug"

    const/4 v0, 0x1

    .line 878
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "tab"

    const-string/jumbo v2, "list_members"

    .line 879
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 884
    :pswitch_17
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 885
    const-string/jumbo v1, "status"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 886
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 887
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    .line 889
    :cond_8
    const-string/jumbo v1, "cursor"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 890
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 891
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 892
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    aput v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/a;->a([I)Lcom/twitter/android/composer/a;

    .line 894
    :cond_9
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->d(I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 895
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 900
    :pswitch_18
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    const/high16 v1, 0x4000000

    .line 901
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->d(I)Lcom/twitter/android/composer/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 902
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 903
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 907
    :pswitch_19
    invoke-direct {p0}, Lcom/twitter/android/UrlInterpreterActivity;->l()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 911
    :pswitch_1a
    const-string/jumbo v0, "screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "UTF8"

    invoke-static {v0, v2}, Lcom/twitter/util/ac;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 913
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 916
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "start_page"

    sget-object v3, Lcom/twitter/android/ProfileActivity;->k:Landroid/net/Uri;

    .line 917
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 918
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "screen_name"

    .line 919
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 923
    :pswitch_1b
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 927
    :pswitch_1c
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 933
    :pswitch_1d
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    const/4 v0, 0x2

    .line 934
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "search_type"

    const/4 v2, 0x2

    .line 935
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "api_call"

    .line 936
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 941
    :pswitch_1e
    invoke-static {}, Lcom/twitter/app/lists/a;->a()Lcom/twitter/app/lists/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 942
    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/lists/a;->b(J)Lcom/twitter/app/lists/a;

    move-result-object v1

    const/4 v0, 0x0

    .line 943
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/app/lists/a;->a(Ljava/lang/String;)Lcom/twitter/app/lists/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 944
    invoke-virtual {v0, v1}, Lcom/twitter/app/lists/a;->a(Z)Lcom/twitter/app/lists/a;

    move-result-object v0

    .line 945
    invoke-virtual {v0, p0}, Lcom/twitter/app/lists/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 949
    :pswitch_1f
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 953
    :pswitch_20
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 955
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/UrlInterpreterActivity;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 956
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 957
    invoke-virtual {v5, v0}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 958
    const-string/jumbo v0, "welcome_message_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 959
    const-string/jumbo v0, "text"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 960
    const-string/jumbo v0, "recipient_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 961
    const-wide/16 v4, -0x1

    invoke-static {v0, v4, v5}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 962
    const-string/jumbo v0, "recipient_screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 964
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 965
    new-instance v3, Lcom/twitter/app/dm/j$a;

    invoke-direct {v3}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 966
    invoke-virtual {v3, v0}, Lcom/twitter/app/dm/j$a;->b(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    const/4 v3, 0x1

    .line 967
    invoke-virtual {v0, v3}, Lcom/twitter/app/dm/j$a;->b(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 968
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/j$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 969
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->f(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 970
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 965
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 984
    goto/16 :goto_1

    .line 971
    :cond_a
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_b

    .line 972
    new-instance v0, Lcom/twitter/app/dm/j$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 973
    invoke-virtual {v0, v4, v5}, Lcom/twitter/app/dm/j$a;->a(J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    const/4 v3, 0x1

    .line 974
    invoke-virtual {v0, v3}, Lcom/twitter/app/dm/j$a;->b(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 975
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/j$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 976
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->f(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 977
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 972
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_3

    .line 979
    :cond_b
    new-instance v0, Lcom/twitter/app/dm/h$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/h$a;-><init>()V

    const/4 v1, 0x1

    .line 980
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/h$a;->b(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 981
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/h$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 982
    invoke-virtual {v0}, Lcom/twitter/app/dm/h$a;->e()Lcom/twitter/app/dm/h;

    move-result-object v0

    .line 979
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/h;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_3

    .line 984
    :cond_c
    if-eqz v8, :cond_d

    .line 985
    new-instance v0, Lcom/twitter/app/dm/b$b;

    invoke-direct {v0}, Lcom/twitter/app/dm/b$b;-><init>()V

    const/4 v1, 0x1

    .line 986
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/b$b;->b(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/b$b;

    invoke-virtual {v0}, Lcom/twitter/app/dm/b$b;->a()Lcom/twitter/app/dm/b;

    move-result-object v0

    .line 985
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 988
    :cond_d
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 994
    :pswitch_21
    invoke-static {p0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1000
    :pswitch_22
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1004
    const/16 v2, 0x39

    if-ne v10, v2, :cond_e

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_e

    .line 1005
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1007
    :cond_e
    new-instance v2, Lcom/twitter/app/dm/j$a;

    invoke-direct {v2}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 1008
    invoke-virtual {v2, v0, v1}, Lcom/twitter/app/dm/j$a;->a(J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 1009
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 1007
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1015
    :pswitch_23
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1016
    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-eqz v0, :cond_f

    .line 1017
    invoke-virtual {v5, v2, v3}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1018
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1019
    invoke-virtual {v5, v0}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 1020
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1021
    new-instance v1, Lcom/twitter/app/dm/j$a;

    invoke-direct {v1}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 1022
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/j$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 1023
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->b(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 1024
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 1021
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1032
    :cond_f
    if-eqz v8, :cond_10

    .line 1033
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1035
    new-instance v1, Lcom/twitter/app/dm/j$a;

    invoke-direct {v1}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 1036
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/j$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 1037
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->b(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 1038
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 1035
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1040
    :cond_10
    invoke-static {p0}, Lcom/twitter/android/al;->c(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 1046
    :pswitch_24
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1047
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_11

    .line 1048
    new-instance v2, Lcom/twitter/app/dm/j$a;

    invoke-direct {v2}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 1049
    invoke-virtual {v2, v0, v1}, Lcom/twitter/app/dm/j$a;->a(J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 1050
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 1048
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1052
    :cond_11
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1059
    :pswitch_25
    invoke-static {p0}, Lcom/twitter/android/util/n;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1065
    :pswitch_26
    invoke-static {p0}, Lcom/twitter/android/util/n;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1071
    :pswitch_27
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1072
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "messages"

    .line 1073
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1074
    invoke-static {p0}, Lcom/twitter/app/dm/l;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1080
    :pswitch_28
    invoke-direct {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->h(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1085
    :pswitch_29
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    const/4 v1, 0x7

    .line 1086
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    const-string/jumbo v1, "url_interpreter"

    .line 1087
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->e(Ljava/lang/String;)Lcom/twitter/app/users/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 1088
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1089
    invoke-virtual {v0, p0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1094
    :pswitch_2a
    const-string/jumbo v0, "scribe_page"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "people"

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1096
    new-instance v1, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    invoke-direct {v1}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;-><init>()V

    .line 1097
    invoke-virtual {v1, v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    move-result-object v0

    .line 1098
    invoke-virtual {v0, p0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x14000000

    .line 1099
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1103
    :pswitch_2b
    if-eqz v8, :cond_2

    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1104
    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Ljava/lang/String;)V

    .line 1105
    invoke-static {p0}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1110
    :pswitch_2c
    invoke-direct {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->h(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1115
    :pswitch_2d
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1116
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "list"

    .line 1117
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1118
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1119
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1120
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1125
    :pswitch_2e
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    .line 1126
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "start_page"

    sget-object v2, Lcom/twitter/android/ProfileActivity;->d:Landroid/net/Uri;

    .line 1127
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1128
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1133
    :pswitch_2f
    invoke-virtual {p0, p1, v2, v3}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;J)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1137
    :pswitch_30
    if-eqz v8, :cond_2

    .line 1138
    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "start_page"

    sget-object v2, Lcom/twitter/android/ProfileActivity;->l:Landroid/net/Uri;

    .line 1140
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1139
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1145
    :pswitch_31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/PrivacyAndContentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "scroll_to_row"

    const-string/jumbo v4, "smart_mute"

    .line 1146
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_account_id"

    .line 1148
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1153
    :pswitch_32
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 1154
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "user"

    .line 1155
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "screen_name"

    const/4 v0, 0x1

    .line 1156
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1157
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1158
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1159
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1163
    :pswitch_33
    const-string/jumbo v0, "status"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1164
    if-eqz v0, :cond_12

    .line 1165
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1167
    :cond_12
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1173
    :pswitch_34
    const-string/jumbo v0, "text"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1174
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1179
    :pswitch_35
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "id"

    .line 1180
    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1179
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1181
    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1182
    const-string/jumbo v0, "screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1183
    const-string/jumbo v0, "1"

    const-string/jumbo v1, "df"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    if-eqz v4, :cond_13

    const/4 v0, 0x1

    .line 1185
    :goto_4
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/UrlInterpreterActivity;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v1

    .line 1187
    if-eqz v0, :cond_2

    .line 1188
    const-string/jumbo v0, "start_page"

    sget-object v2, Lcom/twitter/android/ProfileActivity;->k:Landroid/net/Uri;

    .line 1189
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1188
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1183
    :cond_13
    const/4 v0, 0x0

    goto :goto_4

    .line 1195
    :pswitch_36
    if-eqz v8, :cond_14

    .line 1196
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1198
    :cond_14
    new-instance v0, Lcom/twitter/android/v$a;

    invoke-direct {v0}, Lcom/twitter/android/v$a;-><init>()V

    const/4 v2, 0x0

    .line 1199
    invoke-virtual {v0, v2}, Lcom/twitter/android/v$a;->a(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    const/4 v2, 0x0

    .line 1200
    invoke-virtual {v0, v2}, Lcom/twitter/android/v$a;->b(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    const/4 v2, 0x1

    .line 1201
    invoke-virtual {v0, v2}, Lcom/twitter/android/v$a;->c(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    .line 1202
    new-instance v2, Lcom/twitter/android/FlowActivity$a;

    invoke-direct {v2, p0}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lako;

    invoke-virtual {v2, v0}, Lcom/twitter/android/FlowActivity$a;->a(Lako;)V

    goto/16 :goto_1

    .line 1208
    :pswitch_37
    invoke-static {v0, p1}, Lcom/twitter/android/util/o;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1209
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1210
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_15

    .line 1211
    invoke-virtual {v5, v0, v1}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 1213
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v0, v6, v0

    if-nez v0, :cond_15

    .line 1216
    invoke-virtual {v5, v2}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 1217
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    .line 1218
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1223
    :cond_15
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/LoginActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1224
    const-string/jumbo v0, "screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1225
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1227
    const-string/jumbo v2, "screen_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1229
    :cond_16
    if-eqz v8, :cond_17

    .line 1232
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1233
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1235
    :cond_17
    const-string/jumbo v0, "android.intent.extra.INTENT"

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1240
    :pswitch_38
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1244
    :pswitch_39
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    .line 1245
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/StatusBarNotif;

    move-result-object v0

    .line 1246
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1247
    invoke-static {v1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto/16 :goto_1

    .line 1252
    :pswitch_3a
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1256
    :pswitch_3b
    invoke-direct {p0}, Lcom/twitter/android/UrlInterpreterActivity;->j()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1260
    :pswitch_3c
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/composer/ComposerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1264
    :pswitch_3d
    new-instance v0, Lcom/twitter/app/dm/b$b;

    invoke-direct {v0}, Lcom/twitter/app/dm/b$b;-><init>()V

    .line 1265
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/b$b;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/b$b;

    .line 1266
    invoke-virtual {v0}, Lcom/twitter/app/dm/b$b;->a()Lcom/twitter/app/dm/b;

    move-result-object v0

    .line 1264
    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->b(Landroid/content/Context;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v0

    .line 1267
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1271
    :pswitch_3e
    invoke-static {p0}, Lcom/twitter/app/dm/l;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1275
    :pswitch_3f
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1279
    :pswitch_40
    invoke-direct {p0}, Lcom/twitter/android/UrlInterpreterActivity;->j()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1283
    :pswitch_41
    if-nez v8, :cond_18

    .line 1284
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 1286
    :cond_18
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1287
    if-eqz v0, :cond_19

    const-class v2, Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "source"

    .line 1288
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1287
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 1290
    invoke-static {p0}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1291
    invoke-virtual {v0, v2}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1293
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 1292
    invoke-virtual {v0, v2}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    .line 1294
    invoke-virtual {v0}, Landroid/support/v4/app/TaskStackBuilder;->startActivities()V

    move-object v0, v1

    :goto_5
    move-object v1, v0

    .line 1300
    goto/16 :goto_1

    .line 1296
    :cond_19
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1297
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_5

    .line 1304
    :pswitch_42
    invoke-direct {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->h(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1309
    :pswitch_43
    const-string/jumbo v0, "timeline_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1310
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1311
    :goto_6
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1312
    new-instance v1, Lcom/twitter/app/collections/a$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/app/collections/a$a;-><init>(Landroid/os/Bundle;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "custom-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1314
    invoke-virtual {v1, v0}, Lcom/twitter/app/collections/a$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/collections/a$a;

    .line 1315
    invoke-virtual {v0}, Lcom/twitter/app/collections/a$a;->a()Lcom/twitter/app/collections/a;

    move-result-object v0

    .line 1316
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/CollectionPermalinkActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1317
    invoke-virtual {v0}, Lcom/twitter/app/collections/a;->n()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1310
    :cond_1a
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 1322
    :pswitch_44
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1323
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1324
    invoke-virtual {v5, v0}, Lcom/twitter/library/client/v;->d(Ljava/lang/String;)V

    .line 1326
    :cond_1b
    const/4 v0, 0x3

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1327
    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 1328
    invoke-static {p0, v2, v3}, Lcom/twitter/android/analytics/TweetAnalyticsWebViewActivity;->a(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1333
    :pswitch_45
    invoke-static {p0}, Lcom/twitter/android/ads/AdsCompanionWebViewActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1337
    :pswitch_46
    const-string/jumbo v2, "user"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1338
    if-eqz v2, :cond_2

    .line 1339
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 1340
    invoke-virtual {v5, v2}, Lcom/twitter/library/client/v;->d(Ljava/lang/String;)V

    .line 1342
    :cond_1c
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ads/AdsCompanionWebViewActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1349
    :pswitch_47
    invoke-static {p0}, Lcom/twitter/util/u;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1350
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/util/u;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1352
    :goto_7
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 1351
    :cond_1d
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/util/u;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 1356
    :pswitch_48
    const/16 v0, 0x3f

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1360
    :pswitch_49
    const/16 v0, 0x3e

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1364
    :pswitch_4a
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1368
    :pswitch_4b
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    .line 1369
    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1368
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1374
    :pswitch_4c
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1378
    :pswitch_4d
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/TimelineSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_account_id"

    .line 1379
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "source"

    const-string/jumbo v2, "source"

    .line 1380
    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1384
    :pswitch_4e
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/AccessibilityActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_account_id"

    .line 1385
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1389
    :pswitch_4f
    const-string/jumbo v0, "steps"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v8}, Lcom/twitter/android/UrlInterpreterActivity;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1390
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1395
    :pswitch_50
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PasswordResetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "init_url"

    .line 1396
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1400
    :pswitch_51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PhoneEntrySettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "account_name"

    .line 1401
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "umf_flow"

    const/4 v2, 0x1

    .line 1402
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1406
    :pswitch_52
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbsi;->g(J)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1407
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/news/NewsActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 1409
    :cond_1e
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1414
    :pswitch_53
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbsi;->g(J)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1415
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/news/NewsDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "news_id"

    .line 1416
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1418
    :cond_1f
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1424
    :pswitch_54
    if-eqz v8, :cond_22

    .line 1425
    const/16 v0, 0x8d

    if-ne v10, v0, :cond_20

    .line 1426
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1427
    :goto_8
    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1428
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_21

    .line 1429
    invoke-static {p0, v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->c(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    :goto_9
    move-object v1, v0

    .line 1434
    goto/16 :goto_1

    .line 1426
    :cond_20
    const-string/jumbo v0, "moment_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 1432
    :cond_21
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_9

    .line 1435
    :cond_22
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1440
    :pswitch_55
    if-eqz v8, :cond_23

    .line 1441
    const-string/jumbo v1, "categoryId"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1442
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1444
    :cond_23
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1449
    :pswitch_56
    if-eqz v8, :cond_24

    .line 1450
    const-string/jumbo v1, "category_id"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1451
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1453
    :cond_24
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1458
    :pswitch_57
    if-eqz v8, :cond_25

    .line 1459
    new-instance v0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$f;

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$f;-><init>(Landroid/app/Activity;J)V

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$f;->a()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1461
    :cond_25
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1466
    :pswitch_58
    if-eqz v8, :cond_26

    .line 1467
    new-instance v0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;->a()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1469
    :cond_26
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1474
    :pswitch_59
    const-string/jumbo v0, "bounce_location"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1475
    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Lcom/twitter/android/BouncerWebViewActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1479
    :pswitch_5a
    const-string/jumbo v0, "profile_birthday_collection_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1480
    invoke-virtual {p0, p1, v2, v3}, Lcom/twitter/android/UrlInterpreterActivity;->a(Landroid/net/Uri;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "edit_birthdate"

    const/4 v2, 0x1

    .line 1481
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1483
    :cond_27
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1489
    :pswitch_5b
    const-string/jumbo v0, "screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1490
    invoke-direct {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1492
    sparse-switch v10, :sswitch_data_1

    .line 1502
    const/4 v0, 0x0

    .line 1506
    :goto_a
    if-eqz v0, :cond_2

    .line 1507
    const-string/jumbo v2, "start_page"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1494
    :sswitch_4
    sget-object v0, Lcom/twitter/android/ProfileActivity;->b:Landroid/net/Uri;

    goto :goto_a

    .line 1498
    :sswitch_5
    sget-object v0, Lcom/twitter/android/ProfileActivity;->c:Landroid/net/Uri;

    goto :goto_a

    .line 1512
    :pswitch_5c
    if-eqz v8, :cond_28

    .line 1513
    const-string/jumbo v0, "deep_link"

    invoke-static {p0, v0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1515
    :cond_28
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1520
    :pswitch_5d
    invoke-direct {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->h(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1524
    :pswitch_5e
    invoke-direct {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->g(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1528
    :pswitch_5f
    const-string/jumbo v0, "native_mobile_sms_2fa_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1529
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/BackupCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "bc_account_id"

    .line 1530
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1532
    :cond_29
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1537
    :pswitch_60
    invoke-static {}, Lbpt;->a()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1538
    const/4 v0, 0x0

    .line 1539
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1540
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2a

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1538
    :goto_b
    invoke-static {p0, v2, v3, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1540
    :cond_2a
    const/4 v0, 0x0

    goto :goto_b

    .line 1542
    :cond_2b
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1547
    :pswitch_61
    invoke-static {}, Lbpt;->a()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 1548
    const/4 v0, 0x2

    .line 1549
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1550
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_2c

    const/4 v0, 0x3

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1548
    :goto_c
    invoke-static {p0, v2, v3, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1550
    :cond_2c
    const-string/jumbo v0, "top"

    goto :goto_c

    .line 1552
    :cond_2d
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1557
    :pswitch_62
    if-nez v8, :cond_2f

    .line 1558
    invoke-static {p0}, Lcom/twitter/android/al;->c(Landroid/app/Activity;)V

    .line 1566
    :cond_2e
    :goto_d
    if-nez v1, :cond_2

    .line 1567
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/livevideo/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 1568
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1569
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1559
    :cond_2f
    const-string/jumbo v0, "live_video_timeline_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 1560
    invoke-static {p1}, Lcom/twitter/android/livevideo/c;->a(Landroid/net/Uri;)J

    move-result-wide v2

    .line 1561
    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2e

    .line 1562
    new-instance v0, Lcom/twitter/android/livevideo/landing/b;

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/livevideo/landing/b;-><init>(J)V

    .line 1563
    invoke-static {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->a(Landroid/content/Context;Lcom/twitter/android/livevideo/landing/b;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_d

    .line 1571
    :cond_30
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1577
    :pswitch_63
    if-eqz v8, :cond_31

    .line 1578
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1580
    :cond_31
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/LoginActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 1585
    :pswitch_64
    if-eqz v8, :cond_33

    invoke-static {v6}, Lbpq;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1586
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 1587
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 1589
    :cond_32
    const/4 v0, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1590
    invoke-static {p0, v0, v2, v3}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x3

    .line 1589
    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/UrlInterpreterActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 1594
    :cond_33
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1599
    :pswitch_65
    if-eqz v8, :cond_34

    invoke-static {}, Lcom/twitter/android/qrcodes/a;->a()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 1600
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/qrcodes/a;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1602
    :cond_34
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1619
    :cond_35
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 1622
    :cond_36
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/twitter/android/LoginActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 1624
    :cond_37
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->c:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 1625
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->b()Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 1627
    :cond_38
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1628
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 654
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_1d
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_17
        :pswitch_21
        :pswitch_22
        :pswitch_25
        :pswitch_27
        :pswitch_24
        :pswitch_28
        :pswitch_28
        :pswitch_2c
        :pswitch_1c
        :pswitch_29
        :pswitch_1d
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_32
        :pswitch_3
        :pswitch_1e
        :pswitch_11
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_10
        :pswitch_14
        :pswitch_0
        :pswitch_16
        :pswitch_15
        :pswitch_13
        :pswitch_34
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_2a
        :pswitch_0
        :pswitch_42
        :pswitch_2b
        :pswitch_4d
        :pswitch_19
        :pswitch_1a
        :pswitch_4a
        :pswitch_0
        :pswitch_14
        :pswitch_23
        :pswitch_43
        :pswitch_0
        :pswitch_3
        :pswitch_47
        :pswitch_27
        :pswitch_22
        :pswitch_a
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_49
        :pswitch_48
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_33
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_14
        :pswitch_14
        :pswitch_4e
        :pswitch_0
        :pswitch_4b
        :pswitch_f
        :pswitch_50
        :pswitch_0
        :pswitch_0
        :pswitch_44
        :pswitch_26
        :pswitch_26
        :pswitch_45
        :pswitch_46
        :pswitch_20
        :pswitch_14
        :pswitch_63
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_37
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_36
        :pswitch_4c
        :pswitch_0
        :pswitch_4f
        :pswitch_25
        :pswitch_12
        :pswitch_35
        :pswitch_43
        :pswitch_0
        :pswitch_5
        :pswitch_38
        :pswitch_4c
        :pswitch_39
        :pswitch_3a
        :pswitch_3c
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_3b
        :pswitch_3d
        :pswitch_e
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_51
        :pswitch_52
        :pswitch_54
        :pswitch_59
        :pswitch_3e
        :pswitch_54
        :pswitch_56
        :pswitch_0
        :pswitch_2f
        :pswitch_5a
        :pswitch_53
        :pswitch_5b
        :pswitch_55
        :pswitch_30
        :pswitch_5c
        :pswitch_0
        :pswitch_31
        :pswitch_18
        :pswitch_1f
        :pswitch_5d
        :pswitch_5f
        :pswitch_5b
        :pswitch_60
        :pswitch_61
        :pswitch_0
        :pswitch_62
        :pswitch_5e
        :pswitch_4
        :pswitch_2a
        :pswitch_64
        :pswitch_57
        :pswitch_58
        :pswitch_65
    .end packed-switch

    .line 840
    :sswitch_data_0
    .sparse-switch
        0x1a -> :sswitch_3
        0x1c -> :sswitch_2
        0x49 -> :sswitch_1
        0x57 -> :sswitch_0
    .end sparse-switch

    .line 1492
    :sswitch_data_1
    .sparse-switch
        0x93 -> :sswitch_5
        0x9d -> :sswitch_4
    .end sparse-switch
.end method

.method d(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 1916
    sget-object v0, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/16 v1, 0x35

    if-ne v0, v1, :cond_0

    .line 1917
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 1918
    const-string/jumbo v0, "impression"

    invoke-static {v2, v3, v0, p1}, Lcom/twitter/android/bk;->a(JLjava/lang/String;Landroid/net/Uri;)V

    .line 1919
    const-string/jumbo v0, "url"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1920
    if-eqz v0, :cond_0

    .line 1921
    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1924
    sget-object v1, Lcom/twitter/android/UrlInterpreterActivity;->d:Landroid/content/UriMatcher;

    invoke-virtual {v1, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 1925
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 1926
    const-string/jumbo v1, "resolvable"

    invoke-static {v2, v3, v1, v0}, Lcom/twitter/android/bk;->a(JLjava/lang/String;Landroid/net/Uri;)V

    .line 1932
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1758
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1759
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/UrlInterpreterActivity;->i:Z

    .line 1760
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_1

    .line 1761
    const-string/jumbo v0, "AbsFragmentActivity_account_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1762
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1763
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "AbsFragmentActivity_account_name"

    .line 1764
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1763
    invoke-virtual {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->startActivity(Landroid/content/Intent;)V

    .line 1771
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/UrlInterpreterActivity;->finish()V

    .line 1772
    return-void

    .line 1766
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 1767
    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1768
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/periscope/broadcaster/PeriscopeBroadcasterActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/UrlInterpreterActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1841
    const/4 v0, 0x1

    invoke-static {v0, p1}, Lcom/twitter/android/al;->a(ZLandroid/content/Intent;)V

    .line 1842
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 1843
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 1752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UrlInterpreterActivity;->i:Z

    .line 1753
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1754
    return-void
.end method
