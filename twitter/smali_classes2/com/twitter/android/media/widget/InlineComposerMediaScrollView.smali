.class public Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;
.super Landroid/widget/ScrollView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;
    }
.end annotation


# instance fields
.field a:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;

.field private final b:Landroid/view/ViewConfiguration;

.field private c:F

.field private d:I

.field private e:Z

.field private f:Lcom/twitter/android/media/widget/AttachmentMediaView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->b:Landroid/view/ViewConfiguration;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)Lcom/twitter/android/media/widget/AttachmentMediaView;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 68
    if-nez p1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->f:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a(Lcom/twitter/model/media/EditableMedia;)Z

    .line 70
    iget-object v1, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->f:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :goto_0
    return-object v0

    .line 74
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_1

    .line 76
    iget-object v1, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->f:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->a()F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setAspectRatio(F)V

    .line 77
    iget-object v0, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->f:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)V

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->f:Lcom/twitter/android/media/widget/AttachmentMediaView;

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    .line 39
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->c:F

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->e:Z

    .line 42
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    iput-object v0, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->f:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 86
    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 87
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 92
    iput p2, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->d:I

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->e:Z

    .line 94
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 98
    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->d:I

    add-int/2addr v1, p4

    sub-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->scrollTo(II)V

    .line 99
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onSizeChanged(IIII)V

    .line 100
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 50
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 51
    iget-boolean v2, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->e:Z

    if-nez v2, :cond_0

    iget v2, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->c:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->b:Landroid/view/ViewConfiguration;

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->a:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->a:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;

    invoke-interface {v1, p0}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;->a(Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;)V

    .line 58
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setActionListener(Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->a:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;

    .line 63
    return-void
.end method
