.class public Lcom/twitter/android/media/widget/MediaAttachmentsView;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/widget/MediaAttachmentsView$b;,
        Lcom/twitter/android/media/widget/MediaAttachmentsView$SavedState;,
        Lcom/twitter/android/media/widget/MediaAttachmentsView$a;,
        Lcom/twitter/android/media/widget/MediaAttachmentsView$c;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

.field private final c:Landroid/view/ViewGroup;

.field private d:Landroid/view/ViewGroup;

.field private final e:Landroid/animation/LayoutTransition;

.field private final f:Landroid/view/animation/Animation;

.field private final g:Landroid/view/animation/Animation;

.field private h:Lcom/twitter/android/media/selection/b;

.field private final i:I

.field private j:Lcom/twitter/model/media/EditableMedia;

.field private k:Z

.field private final l:I

.field private final m:I

.field private n:Lcom/twitter/android/media/widget/AttachmentMediaView;

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

.field private s:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f01004d

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Lcom/twitter/android/media/selection/b;

    invoke-direct {v0, v3}, Lcom/twitter/android/media/selection/b;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->h:Lcom/twitter/android/media/selection/b;

    .line 70
    sget-object v0, Lcom/twitter/android/bi$a;->MediaAttachmentsView:[I

    .line 71
    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 73
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->l:I

    .line 74
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->m:I

    .line 75
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 77
    new-instance v1, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    invoke-direct {v1, p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;-><init>(Landroid/content/Context;)V

    .line 78
    invoke-virtual {v1, v3}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->setHorizontalScrollBarEnabled(Z)V

    .line 79
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->addView(Landroid/view/View;)V

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 82
    const v2, 0x7f0401a1

    .line 83
    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 84
    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->addView(Landroid/view/View;)V

    .line 86
    iput-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    .line 87
    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0179

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->i:I

    .line 91
    const v0, 0x7f050023

    .line 92
    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f:Landroid/view/animation/Animation;

    .line 93
    const v0, 0x7f050024

    .line 94
    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->g:Landroid/view/animation/Animation;

    .line 96
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 97
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 98
    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 99
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 100
    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->e:Landroid/animation/LayoutTransition;

    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->d()V

    .line 102
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/widget/MediaAttachmentsView;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->s:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/widget/MediaAttachmentsView;Lcom/twitter/android/media/widget/AttachmentMediaView;)Lcom/twitter/android/media/widget/AttachmentMediaView;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    return-object p1
.end method

.method private a(Landroid/net/Uri;Z)V
    .locals 3

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/net/Uri;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    .line 236
    if-nez v0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 240
    :cond_0
    iget v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    .line 242
    if-eqz p2, :cond_1

    .line 243
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->g:Landroid/view/animation/Animation;

    new-instance v2, Lcom/twitter/android/media/widget/MediaAttachmentsView$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$2;-><init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 265
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->p:Z

    goto :goto_0

    .line 268
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-ne v0, v1, :cond_2

    .line 269
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 271
    :cond_2
    invoke-direct {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 420
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-ne p1, v0, :cond_0

    .line 421
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->removeView(Landroid/view/View;)V

    .line 422
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 477
    :goto_0
    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 425
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_2

    .line 427
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getMediaCount()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 428
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    .line 431
    new-instance v2, Lcom/twitter/android/media/widget/MediaAttachmentsView$4;

    invoke-direct {v2, p0, v1, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$4;-><init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;Landroid/view/ViewGroup;Landroid/animation/LayoutTransition;)V

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    goto :goto_0

    .line 448
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    .line 452
    new-instance v2, Lcom/twitter/android/media/widget/MediaAttachmentsView$5;

    invoke-direct {v2, p0, v1, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$5;-><init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;Lcom/twitter/android/media/widget/MediaAttachmentsView$c;Landroid/animation/LayoutTransition;)V

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    goto :goto_0

    .line 464
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getMediaCount()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 465
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 466
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 467
    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->addView(Landroid/view/View;I)V

    move-object v0, v1

    .line 468
    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 469
    check-cast v1, Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/widget/AttachmentMediaView;Z)V

    goto :goto_0

    .line 473
    :cond_3
    iput-boolean v3, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->k:Z

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/media/selection/MediaAttachment;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 161
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    .line 162
    if-nez v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/net/Uri;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    .line 167
    if-nez v0, :cond_2

    .line 168
    new-instance v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v4}, Lcom/twitter/android/media/widget/AttachmentMediaView;-><init>(Landroid/content/Context;Z)V

    .line 169
    const v2, 0x7f13004f

    invoke-virtual {v1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setTag(ILjava/lang/Object;)V

    .line 170
    new-instance v2, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;-><init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;Lcom/twitter/android/media/widget/MediaAttachmentsView$1;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setOnAttachmentActionListener(Lcom/twitter/android/media/widget/AttachmentMediaView$a;)V

    .line 171
    invoke-direct {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/widget/AttachmentMediaView;)V

    .line 177
    :goto_1
    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/widget/AttachmentMediaView;Z)V

    .line 178
    invoke-virtual {v1}, Lcom/twitter/model/media/EditableMedia;->a()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setAspectRatio(F)V

    .line 180
    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setMediaAttachment(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 181
    iget v2, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-nez v2, :cond_0

    .line 182
    iput-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->j:Lcom/twitter/model/media/EditableMedia;

    .line 183
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-ne v0, v1, :cond_0

    .line 184
    if-eqz p2, :cond_3

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->g()Z

    move-result v1

    if-nez v1, :cond_3

    .line 185
    new-instance v1, Lcom/twitter/android/media/widget/MediaAttachmentsView$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$1;-><init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    goto :goto_0

    .line 174
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->k()V

    goto :goto_1

    .line 195
    :cond_3
    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->clearAnimation()V

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 345
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->d:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setVisibleAreaContainer(Landroid/view/ViewGroup;)V

    .line 346
    iget v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    if-nez v0, :cond_0

    .line 347
    const v0, 0x7f130076

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setId(I)V

    .line 348
    invoke-virtual {p0, p1, v2}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->addView(Landroid/view/View;I)V

    .line 349
    iput-object p1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 365
    :goto_0
    iget v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    .line 366
    return-void

    .line 351
    :cond_0
    iget v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    if-ne v0, v3, :cond_2

    .line 352
    sget-boolean v0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 356
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 357
    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/widget/AttachmentMediaView;Z)V

    .line 358
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->removeView(Landroid/view/View;)V

    .line 359
    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setId(I)V

    .line 360
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 362
    :cond_2
    invoke-virtual {p1, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setId(I)V

    .line 363
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/media/widget/MediaAttachmentsView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/widget/MediaAttachmentsView;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/AttachmentMediaView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->g:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->r:Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 202
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->h:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v1}, Lcom/twitter/android/media/selection/b;->e()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 203
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->h:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v1}, Lcom/twitter/android/media/selection/b;->b()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 204
    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/net/Uri;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_2

    .line 206
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setPhotoNumber(I)V

    .line 207
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 209
    goto :goto_1

    .line 202
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 210
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method static synthetic g(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->p:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Lcom/twitter/android/media/widget/AttachmentMediaView;
    .locals 4

    .prologue
    const v3, 0x7f13004f

    .line 214
    if-eqz p1, :cond_2

    .line 215
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 217
    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 231
    :goto_0
    return-object v0

    .line 221
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 222
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 223
    instance-of v0, v1, Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_1

    .line 224
    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 225
    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 226
    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    goto :goto_0

    .line 221
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 231
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a()V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 152
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 153
    instance-of v2, v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v2, :cond_1

    .line 154
    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a()V

    .line 151
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 158
    :cond_2
    return-void
.end method

.method a(Lcom/twitter/android/media/widget/AttachmentMediaView;Z)V
    .locals 5

    .prologue
    const/4 v1, -0x2

    const v3, 0x7f13000e

    .line 371
    if-eqz p2, :cond_2

    .line 373
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-ne p1, v0, :cond_0

    .line 374
    const/4 v2, -0x1

    .line 376
    invoke-virtual {p1, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 377
    invoke-virtual {p1, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setMaxAspectRatio(F)V

    move v0, v1

    move v1, v2

    .line 403
    :goto_0
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object v0, v2

    .line 408
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-ne p1, v1, :cond_3

    .line 409
    iget v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->m:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 410
    iget v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->m:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 416
    :goto_2
    invoke-virtual {p1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 417
    return-void

    .line 381
    :cond_0
    iget v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->i:I

    .line 382
    invoke-virtual {p1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getMaxAspectRatio()F

    move-result v2

    .line 383
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {p1, v3, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setTag(ILjava/lang/Object;)V

    .line 384
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getMeasuredWidth()I

    move-result v2

    if-lez v2, :cond_1

    .line 385
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->m:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 386
    int-to-float v2, v2

    int-to-float v3, v0

    div-float/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setMaxAspectRatio(F)V

    goto :goto_0

    .line 388
    :cond_1
    iget v2, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->m:I

    .line 391
    invoke-virtual {p1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/media/widget/MediaAttachmentsView$3;

    invoke-direct {v4, p0, v2, p1, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$3;-><init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;ILcom/twitter/android/media/widget/AttachmentMediaView;I)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 405
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    goto :goto_1

    .line 412
    :cond_3
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 413
    iget v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->l:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_2

    :cond_4
    move v0, v1

    move v1, v2

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 480
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 481
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 482
    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getEditableMedia()Lcom/twitter/model/media/EditableMedia;

    move-result-object v3

    .line 483
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 484
    iget-object v3, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getLeft()I

    move-result v0

    invoke-virtual {v3, v0, v2}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->scrollTo(II)V

    .line 485
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq v1, v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a()V

    .line 480
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 490
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/b;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 105
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->h:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/selection/b;->a(Lcom/twitter/android/media/selection/b;)Lcom/twitter/android/media/selection/b$a;

    move-result-object v3

    .line 106
    if-eqz p2, :cond_0

    iget-object v0, v3, Lcom/twitter/android/media/selection/b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, v3, Lcom/twitter/android/media/selection/b$a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-gt v0, v2, :cond_0

    move v1, v2

    .line 107
    :goto_0
    iget-object v0, v3, Lcom/twitter/android/media/selection/b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 108
    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/net/Uri;Z)V

    goto :goto_1

    .line 106
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, v3, Lcom/twitter/android/media/selection/b$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 111
    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/net/Uri;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_2

    .line 113
    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->c()V

    goto :goto_2

    .line 116
    :cond_3
    iget-object v0, v3, Lcom/twitter/android/media/selection/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 117
    iget v5, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-eqz v5, :cond_5

    iget v5, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-ne v5, v2, :cond_4

    .line 118
    :cond_5
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/selection/MediaAttachment;Z)V

    goto :goto_3

    .line 121
    :cond_6
    iget-object v0, v3, Lcom/twitter/android/media/selection/b$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 122
    iget v4, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-eqz v4, :cond_8

    iget v4, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-ne v4, v2, :cond_7

    .line 123
    :cond_8
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/selection/MediaAttachment;Z)V

    goto :goto_4

    .line 126
    :cond_9
    new-instance v0, Lcom/twitter/android/media/selection/b;

    invoke-direct {v0, p1}, Lcom/twitter/android/media/selection/b;-><init>(Lcom/twitter/android/media/selection/b;)V

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->h:Lcom/twitter/android/media/selection/b;

    .line 127
    invoke-direct {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->g()V

    .line 128
    return v2
.end method

.method public b()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 278
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->removeView(Landroid/view/View;)V

    .line 280
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 282
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    .line 283
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->p:Z

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->q:Z

    .line 307
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-direct {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/view/View;)V

    .line 304
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->setVisibility(I)V

    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->q:Z

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->e:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 323
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 327
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->e()V

    .line 342
    :cond_0
    return-void

    .line 337
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 339
    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->c()V

    .line 337
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->h:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/b;->e()I

    move-result v0

    return v0
.end method

.method public getMediaCount()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->o:I

    return v0
.end method

.method public getSelectedImage()Lcom/twitter/model/media/EditableImage;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 669
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->h:Lcom/twitter/android/media/selection/b;

    iget-object v2, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->s:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/selection/b;->a(Landroid/net/Uri;)Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 670
    if-nez v0, :cond_0

    .line 674
    :goto_0
    return-object v1

    .line 673
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 674
    instance-of v2, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 311
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 312
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->j:Lcom/twitter/model/media/EditableMedia;

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->j:Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 314
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->j:Lcom/twitter/model/media/EditableMedia;

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->k:Z

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a()V

    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->k:Z

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 652
    check-cast p1, Lcom/twitter/android/media/widget/MediaAttachmentsView$SavedState;

    .line 653
    invoke-virtual {p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 655
    iget-object v0, p1, Lcom/twitter/android/media/widget/MediaAttachmentsView$SavedState;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->s:Landroid/net/Uri;

    .line 656
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->r:Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->r:Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 659
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 644
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 645
    new-instance v1, Lcom/twitter/android/media/widget/MediaAttachmentsView$SavedState;

    invoke-direct {v1, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 646
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->s:Landroid/net/Uri;

    iput-object v0, v1, Lcom/twitter/android/media/widget/MediaAttachmentsView$SavedState;->a:Landroid/net/Uri;

    .line 647
    return-object v1
.end method

.method public setActionListener(Lcom/twitter/android/media/widget/MediaAttachmentsView$b;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->r:Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    .line 331
    return-void
.end method

.method public setError(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 286
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Landroid/net/Uri;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    .line 287
    if-nez v0, :cond_0

    .line 291
    :goto_0
    return-void

    .line 290
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->d()Z

    goto :goto_0
.end method

.method public setSelectedImage(Lcom/twitter/model/media/EditableImage;)V
    .locals 2

    .prologue
    .line 662
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->s:Landroid/net/Uri;

    .line 663
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->r:Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->r:Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 666
    :cond_0
    return-void

    .line 662
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableImage;->c()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public setVisibleAreaContainer(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 132
    iput-object p1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->d:Landroid/view/ViewGroup;

    .line 133
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->n:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setVisibleAreaContainer(Landroid/view/ViewGroup;)V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->b:Lcom/twitter/android/media/widget/MediaAttachmentsView$c;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 138
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 139
    instance-of v2, v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v2, :cond_1

    .line 140
    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setVisibleAreaContainer(Landroid/view/ViewGroup;)V

    .line 137
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 144
    :cond_2
    return-void
.end method
