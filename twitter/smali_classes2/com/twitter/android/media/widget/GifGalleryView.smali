.class public Lcom/twitter/android/media/widget/GifGalleryView;
.super Lcom/twitter/refresh/widget/RefreshableListView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/widget/GifGalleryView$b;,
        Lcom/twitter/android/media/widget/GifGalleryView$d;,
        Lcom/twitter/android/media/widget/GifGalleryView$c;,
        Lcom/twitter/android/media/widget/GifGalleryView$a;
    }
.end annotation


# instance fields
.field final a:Landroid/view/View$OnClickListener;

.field final b:Landroid/view/View$OnLongClickListener;

.field c:I

.field d:I

.field e:Z

.field f:Lcom/twitter/android/media/widget/GifGalleryView$b;

.field g:Lcom/twitter/android/media/widget/GifGalleryView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 67
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/media/widget/GifGalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/media/widget/GifGalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/refresh/widget/RefreshableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance v0, Lcom/twitter/android/media/widget/GifGalleryView$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/widget/GifGalleryView$1;-><init>(Lcom/twitter/android/media/widget/GifGalleryView;)V

    iput-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->a:Landroid/view/View$OnClickListener;

    .line 48
    new-instance v0, Lcom/twitter/android/media/widget/GifGalleryView$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/widget/GifGalleryView$2;-><init>(Lcom/twitter/android/media/widget/GifGalleryView;)V

    iput-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->b:Landroid/view/View$OnLongClickListener;

    .line 76
    sget-object v0, Lcom/twitter/android/bi$a;->GifGalleryView:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 78
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 79
    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->c:I

    .line 80
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 84
    return-void

    .line 82
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public a(II)V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    if-nez v0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/GifGalleryView$a;->b(I)I

    move-result v0

    .line 166
    if-ltz v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 171
    add-int/lit8 v0, v0, 0x1

    .line 173
    :cond_2
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/media/widget/GifGalleryView;->setSelectionFromTop(II)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Iterable;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lcom/twitter/android/media/widget/GifGalleryView$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/media/widget/GifGalleryView$a;-><init>(Lcom/twitter/android/media/widget/GifGalleryView;Ljava/lang/Iterable;Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/GifGalleryView;->setAdapter(Lcom/twitter/android/media/widget/GifGalleryView$a;)V

    .line 108
    return-void
.end method

.method public b(Ljava/lang/Iterable;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/widget/GifGalleryView$a;->a(Ljava/lang/Iterable;Z)V

    .line 114
    :cond_0
    return-void
.end method

.method public getFirstVisibleItemIndex()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    if-nez v0, :cond_0

    move v0, v1

    .line 144
    :goto_0
    return v0

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView;->getChildCount()I

    move-result v3

    move v2, v1

    .line 135
    :goto_1
    if-ge v2, v3, :cond_2

    .line 136
    invoke-virtual {p0, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 137
    instance-of v4, v0, Lcom/twitter/android/media/widget/GifGalleryView$c;

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v4

    if-ltz v4, :cond_1

    .line 138
    check-cast v0, Lcom/twitter/android/media/widget/GifGalleryView$c;

    iget-object v0, v0, Lcom/twitter/android/media/widget/GifGalleryView$c;->a:Lcom/twitter/android/media/widget/GifGalleryView$d;

    .line 139
    if-eqz v0, :cond_1

    .line 140
    iget v0, v0, Lcom/twitter/android/media/widget/GifGalleryView$d;->b:I

    goto :goto_0

    .line 135
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 144
    goto :goto_0
.end method

.method public getFirstVisibleItemOffsetPixels()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 148
    iget-object v1, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    if-nez v1, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView;->getChildCount()I

    move-result v2

    move v1, v0

    .line 152
    :goto_1
    if-ge v1, v2, :cond_0

    .line 153
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/widget/GifGalleryView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 154
    instance-of v4, v3, Lcom/twitter/android/media/widget/GifGalleryView$c;

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    if-ltz v4, :cond_2

    .line 155
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0

    .line 152
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    sub-int v1, p4, p2

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/GifGalleryView$a;->a(I)V

    .line 181
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/twitter/refresh/widget/RefreshableListView;->onLayout(ZIIII)V

    .line 182
    return-void
.end method

.method public setAdapter(Lcom/twitter/android/media/widget/GifGalleryView$a;)V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    iput-object p1, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    .line 119
    return-void
.end method

.method public setItemClickListener(Lcom/twitter/android/media/widget/GifGalleryView$b;)V
    .locals 1

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/android/media/widget/GifGalleryView;->f:Lcom/twitter/android/media/widget/GifGalleryView$b;

    .line 88
    if-nez p1, :cond_0

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/GifGalleryView;->setRefreshListener(Lcom/twitter/refresh/widget/RefreshableListView$b;)V

    .line 104
    :goto_0
    return-void

    .line 91
    :cond_0
    new-instance v0, Lcom/twitter/android/media/widget/GifGalleryView$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/media/widget/GifGalleryView$3;-><init>(Lcom/twitter/android/media/widget/GifGalleryView;Lcom/twitter/android/media/widget/GifGalleryView$b;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/GifGalleryView;->setRefreshListener(Lcom/twitter/refresh/widget/RefreshableListView$b;)V

    goto :goto_0
.end method

.method public setPlayAnimation(Z)V
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->e:Z

    if-eq v0, p1, :cond_0

    .line 123
    iput-boolean p1, p0, Lcom/twitter/android/media/widget/GifGalleryView;->e:Z

    .line 124
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/GifGalleryView$a;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView;->g:Lcom/twitter/android/media/widget/GifGalleryView$a;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/GifGalleryView$a;->notifyDataSetChanged()V

    .line 128
    :cond_0
    return-void
.end method
