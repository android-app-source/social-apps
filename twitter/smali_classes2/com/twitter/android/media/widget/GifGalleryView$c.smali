.class Lcom/twitter/android/media/widget/GifGalleryView$c;
.super Landroid/view/ViewGroup;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/widget/GifGalleryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field a:Lcom/twitter/android/media/widget/GifGalleryView$d;

.field final synthetic b:Lcom/twitter/android/media/widget/GifGalleryView;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/widget/GifGalleryView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->b:Lcom/twitter/android/media/widget/GifGalleryView;

    .line 301
    invoke-direct {p0, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 302
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/android/media/widget/GifGalleryView$d;)V
    .locals 11

    .prologue
    const v10, 0x7f1303bc

    const/4 v3, 0x0

    .line 344
    iput-object p1, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->a:Lcom/twitter/android/media/widget/GifGalleryView$d;

    .line 345
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView$c;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 346
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView$c;->getChildCount()I

    move-result v6

    .line 348
    iget-object v0, p1, Lcom/twitter/android/media/widget/GifGalleryView$d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/d;

    .line 351
    if-ge v2, v6, :cond_0

    .line 352
    invoke-virtual {p0, v2}, Lcom/twitter/android/media/widget/GifGalleryView$c;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 353
    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/media/widget/AnimatedGifView;

    .line 354
    invoke-virtual {v1, v3}, Lcom/twitter/library/media/widget/AnimatedGifView;->setVisibility(I)V

    .line 364
    :goto_1
    const v8, 0x7f13003a

    invoke-virtual {v4, v8, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 365
    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/d;->g:Landroid/util/SparseArray;

    .line 366
    invoke-static {v1}, Lcom/twitter/util/math/Size;->a(Landroid/view/View;)Lcom/twitter/util/math/Size;

    move-result-object v4

    iget-object v8, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->b:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-boolean v8, v8, Lcom/twitter/android/media/widget/GifGalleryView;->e:Z

    .line 365
    invoke-static {v0, v4, v8}, Lcom/twitter/android/util/j;->a(Landroid/util/SparseArray;Lcom/twitter/util/math/Size;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/AnimatedGifView;->setResourceUri(Ljava/lang/String;)V

    .line 367
    sget-object v0, Lcom/twitter/library/media/widget/AnimatedGifView;->a:Lcom/twitter/library/media/widget/AnimatedGifView$a;

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/AnimatedGifView;->setListener(Lcom/twitter/library/media/widget/AnimatedGifView$a;)V

    .line 369
    sget-object v0, Lcom/twitter/android/util/j;->a:[I

    .line 370
    iget-object v4, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->a:Lcom/twitter/android/media/widget/GifGalleryView$d;

    invoke-virtual {v4, v2}, Lcom/twitter/android/media/widget/GifGalleryView$d;->a(I)I

    move-result v4

    array-length v8, v0

    rem-int/2addr v4, v8

    aget v0, v0, v4

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/AnimatedGifView;->setBackgroundResource(I)V

    .line 372
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 373
    goto :goto_0

    .line 356
    :cond_0
    const v1, 0x7f040118

    invoke-virtual {v5, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 357
    invoke-virtual {p0, v4}, Lcom/twitter/android/media/widget/GifGalleryView$c;->addView(Landroid/view/View;)V

    .line 358
    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/media/widget/AnimatedGifView;

    .line 359
    iget-object v8, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->b:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v8, v8, Lcom/twitter/android/media/widget/GifGalleryView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    iget-object v8, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->b:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v8, v8, Lcom/twitter/android/media/widget/GifGalleryView;->b:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v4, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 361
    const v8, 0x7f130093

    const-string/jumbo v9, "found_media"

    invoke-virtual {v4, v8, v9}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1

    .line 375
    :cond_1
    add-int/lit8 v0, v6, -0x1

    :goto_2
    if-lt v0, v2, :cond_2

    .line 376
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/GifGalleryView$c;->removeViewAt(I)V

    .line 375
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 378
    :cond_2
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 332
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView$c;->getChildCount()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .line 333
    sub-int v5, p5, p3

    move v1, v2

    move v3, v2

    .line 335
    :goto_0
    if-gt v1, v4, :cond_1

    .line 336
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/widget/GifGalleryView$c;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 337
    if-ne v1, v4, :cond_0

    move v0, p4

    .line 338
    :goto_1
    invoke-virtual {v6, v3, v2, v0, v5}, Landroid/view/View;->layout(IIII)V

    .line 339
    iget-object v3, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->b:Lcom/twitter/android/media/widget/GifGalleryView;

    iget v3, v3, Lcom/twitter/android/media/widget/GifGalleryView;->c:I

    add-int/2addr v0, v3

    add-int/lit8 v3, v0, 0x1

    .line 335
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 337
    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v3

    goto :goto_1

    .line 341
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 306
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->a:Lcom/twitter/android/media/widget/GifGalleryView$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->a:Lcom/twitter/android/media/widget/GifGalleryView$d;

    iget-object v0, v0, Lcom/twitter/android/media/widget/GifGalleryView$d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 328
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->a:Lcom/twitter/android/media/widget/GifGalleryView$d;

    iget-object v2, v0, Lcom/twitter/android/media/widget/GifGalleryView$d;->a:Ljava/util/List;

    .line 312
    const/4 v0, 0x0

    .line 313
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/d;

    .line 314
    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/d;->h:Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    add-float/2addr v0, v1

    move v1, v0

    .line 315
    goto :goto_1

    .line 316
    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 317
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView$c;->getChildCount()I

    move-result v4

    .line 318
    add-int/lit8 v0, v4, -0x1

    iget-object v5, p0, Lcom/twitter/android/media/widget/GifGalleryView$c;->b:Lcom/twitter/android/media/widget/GifGalleryView;

    iget v5, v5, Lcom/twitter/android/media/widget/GifGalleryView;->c:I

    mul-int/2addr v0, v5

    sub-int v0, v3, v0

    .line 319
    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-int v5, v0

    .line 320
    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 321
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_3

    .line 322
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/widget/GifGalleryView$c;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 323
    int-to-float v8, v5

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/d;

    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/d;->h:Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    mul-float/2addr v0, v8

    float-to-int v0, v0

    .line 324
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 325
    invoke-virtual {v7, v0, v6}, Landroid/view/View;->measure(II)V

    .line 321
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 327
    :cond_3
    invoke-virtual {p0, v3, v5}, Lcom/twitter/android/media/widget/GifGalleryView$c;->setMeasuredDimension(II)V

    goto :goto_0
.end method
