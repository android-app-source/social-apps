.class public Lcom/twitter/android/media/widget/AttachmentMediaView;
.super Lcom/twitter/library/media/widget/EditableMediaView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/widget/AttachmentMediaView$a;
    }
.end annotation


# instance fields
.field private k:Landroid/net/Uri;

.field private l:Lcom/twitter/util/math/c;

.field private m:Z

.field private n:Ljava/io/File;

.field private o:Z

.field private p:Lcom/twitter/android/media/widget/AttachmentMediaView$a;

.field private final q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

.field private final r:Z

.field private s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 64
    const v0, 0x7f010029

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/media/widget/EditableMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    invoke-direct {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->t()Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    .line 70
    sget-object v0, Lcom/twitter/android/bi$a;->AttachmentMediaView:[I

    .line 71
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 72
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->r:Z

    .line 73
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 74
    const v1, 0x7f0207d9

    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 76
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/twitter/library/media/widget/EditableMediaView;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-direct {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->t()Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    .line 59
    iput-boolean p2, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->r:Z

    .line 60
    const v0, 0x7f0207d9

    invoke-static {p1, v0}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/widget/AttachmentMediaView;)Lcom/twitter/android/media/widget/AttachmentMediaView$a;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->p:Lcom/twitter/android/media/widget/AttachmentMediaView$a;

    return-object v0
.end method

.method private b(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 93
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v3

    .line 94
    if-nez v3, :cond_0

    .line 178
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v0, Lcom/twitter/android/media/widget/AttachmentMediaView$1;

    invoke-direct {v0, p0, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView$1;-><init>(Lcom/twitter/android/media/widget/AttachmentMediaView;Lcom/twitter/model/media/EditableMedia;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getDismissView()Landroid/view/View;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_1

    .line 109
    new-instance v4, Lcom/twitter/android/media/widget/AttachmentMediaView$2;

    invoke-direct {v4, p0}, Lcom/twitter/android/media/widget/AttachmentMediaView$2;-><init>(Lcom/twitter/android/media/widget/AttachmentMediaView;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    const v4, 0x7f13028b

    invoke-virtual {v0, v4}, Lcom/twitter/android/media/widget/MediaEditButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 122
    iget-object v4, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    const v5, 0x7f13028a

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/widget/MediaEditButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 123
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-boolean v5, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->r:Z

    if-eqz v5, :cond_3

    iget v5, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-nez v5, :cond_3

    .line 126
    invoke-virtual {v3}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v5

    .line 127
    sget-object v6, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-eq v5, v6, :cond_2

    .line 128
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 129
    new-instance v6, Lcom/twitter/android/media/widget/AttachmentMediaView$3;

    invoke-direct {v6, p0, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView$3;-><init>(Lcom/twitter/android/media/widget/AttachmentMediaView;Lcom/twitter/model/media/EditableMedia;)V

    invoke-virtual {v0, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    :cond_2
    sget-object v0, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne v5, v0, :cond_3

    sget-object v0, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    if-ne p2, v0, :cond_6

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbpt;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 140
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 141
    new-instance v0, Lcom/twitter/android/media/widget/AttachmentMediaView$4;

    invoke-direct {v0, p0, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView$4;-><init>(Lcom/twitter/android/media/widget/AttachmentMediaView;Lcom/twitter/model/media/EditableMedia;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->r:Z

    if-eqz v0, :cond_9

    .line 154
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 156
    invoke-static {}, Lcom/twitter/library/dm/d;->v()Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    if-eq p2, v4, :cond_8

    :cond_4
    if-nez v0, :cond_7

    .line 158
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "compose_alt_text"

    .line 159
    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 164
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    const v4, 0x7f130288

    invoke-virtual {v0, v4}, Lcom/twitter/android/media/widget/MediaEditButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 165
    if-eqz v1, :cond_a

    iget v1, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-nez v1, :cond_a

    invoke-virtual {v3}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    sget-object v4, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne v1, v4, :cond_a

    .line 166
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 167
    new-instance v1, Lcom/twitter/android/media/widget/AttachmentMediaView$5;

    invoke-direct {v1, p0, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView$5;-><init>(Lcom/twitter/android/media/widget/AttachmentMediaView;Lcom/twitter/model/media/EditableMedia;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 138
    goto :goto_1

    .line 159
    :cond_7
    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->r:Z

    if-nez v0, :cond_5

    :cond_8
    move v1, v2

    goto :goto_2

    :cond_9
    move v1, v2

    .line 162
    goto :goto_2

    .line 176
    :cond_a
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private b(Lcom/twitter/model/media/EditableMedia;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 327
    iget-object v0, p1, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    iput-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->n:Ljava/io/File;

    .line 328
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 329
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getDismissView()Landroid/view/View;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 333
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lbru;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->b(Lcom/twitter/media/request/a$a;Z)Z

    .line 334
    return-void
.end method

.method private t()Lcom/twitter/android/media/widget/MediaEditButtonContainer;
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040086

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 89
    const v0, 0x7f130287

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    return-object v0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 315
    const v0, 0x7f130288

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 316
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getEditableMedia()Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    .line 317
    if-eqz v0, :cond_1

    instance-of v2, v1, Lcom/twitter/model/core/a;

    if-eqz v2, :cond_1

    .line 318
    check-cast v1, Lcom/twitter/model/core/a;

    .line 319
    invoke-interface {v1}, Lcom/twitter/model/core/a;->bv_()Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a01ec

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    .line 320
    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 324
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/MediaEditButtonContainer;->a()V

    .line 85
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 189
    if-nez p1, :cond_1

    .line 190
    invoke-virtual {p0, v4, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a(Lcom/twitter/model/media/EditableMedia;Z)Z

    .line 191
    iput-object v4, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->n:Ljava/io/File;

    .line 192
    iput-boolean v2, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->o:Z

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->b(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)V

    .line 198
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->k:Landroid/net/Uri;

    .line 199
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->k:Landroid/net/Uri;

    .line 200
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->k:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    move v1, v3

    .line 201
    :goto_1
    if-eqz v1, :cond_5

    .line 202
    iput-boolean v2, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->m:Z

    .line 203
    iput-object v4, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->n:Ljava/io/File;

    .line 216
    :cond_3
    :goto_2
    iget v0, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 218
    :pswitch_0
    const/4 v0, 0x2

    .line 219
    invoke-virtual {p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    .line 218
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a(Lcom/twitter/model/media/EditableMedia;Z)Z

    goto :goto_0

    :cond_4
    move v1, v2

    .line 200
    goto :goto_1

    .line 205
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 206
    instance-of v4, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v4, :cond_3

    .line 207
    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    sget-object v4, Lcom/twitter/util/math/c;->c:Lcom/twitter/util/math/c;

    .line 208
    invoke-static {v0, v4}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/c;

    .line 209
    iget-object v4, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->l:Lcom/twitter/util/math/c;

    invoke-virtual {v0, v4}, Lcom/twitter/util/math/c;->b(Lcom/twitter/util/math/c;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 210
    iput-boolean v2, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->m:Z

    .line 211
    iput-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->l:Lcom/twitter/util/math/c;

    goto :goto_2

    .line 224
    :pswitch_1
    invoke-virtual {p1, v3}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_0

    .line 226
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->b(Lcom/twitter/model/media/EditableMedia;Z)V

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 238
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    invoke-virtual {v0}, Lcom/twitter/media/request/a;->d()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    iget-object v1, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->n:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 241
    if-eqz p2, :cond_0

    .line 242
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->o:Z

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/twitter/library/media/widget/EditableMediaView;->a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/request/a;)Z
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->d()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 274
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    iget-object v1, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->n:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->o:Z

    .line 277
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/media/widget/EditableMediaView;->a(Lcom/twitter/media/request/a;)Z

    move-result v0

    goto :goto_0
.end method

.method protected b()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 307
    iget v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->s:I

    if-lt v0, v2, :cond_0

    .line 308
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a01f3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->s:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 312
    :goto_0
    return-void

    .line 311
    :cond_0
    invoke-super {p0}, Lcom/twitter/library/media/widget/EditableMediaView;->b()V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->m:Z

    if-eqz v0, :cond_0

    .line 253
    invoke-super {p0}, Lcom/twitter/library/media/widget/EditableMediaView;->e()V

    .line 257
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->u()V

    .line 258
    return-void

    .line 255
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->requestLayout()V

    goto :goto_0
.end method

.method public getAttachmentMediaKey()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->k:Landroid/net/Uri;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->m:Z

    .line 268
    invoke-super/range {p0 .. p5}, Lcom/twitter/library/media/widget/EditableMediaView;->onLayout(ZIIII)V

    .line 269
    return-void
.end method

.method public setMediaAttachment(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)V

    .line 186
    return-void
.end method

.method public setOnAttachmentActionListener(Lcom/twitter/android/media/widget/AttachmentMediaView$a;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->p:Lcom/twitter/android/media/widget/AttachmentMediaView$a;

    .line 182
    return-void
.end method

.method public setPhotoNumber(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 281
    iput p1, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->s:I

    .line 282
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->b()V

    .line 284
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 285
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getDismissView()Landroid/view/View;

    move-result-object v2

    .line 286
    if-eqz v2, :cond_0

    .line 287
    if-lt p1, v5, :cond_1

    const v0, 0x7f0a01ed

    new-array v3, v5, [Ljava/lang/Object;

    .line 288
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 287
    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    const v2, 0x7f13028a

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/MediaEditButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 293
    if-lt p1, v5, :cond_2

    const v0, 0x7f0a01fa

    new-array v3, v5, [Ljava/lang/Object;

    .line 294
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 293
    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    const v2, 0x7f13028b

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/MediaEditButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 298
    if-lt p1, v5, :cond_3

    const v0, 0x7f0a01f0

    new-array v3, v5, [Ljava/lang/Object;

    .line 299
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 298
    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 302
    invoke-direct {p0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->u()V

    .line 303
    return-void

    .line 288
    :cond_1
    const v0, 0x7f0a00be

    .line 289
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 294
    :cond_2
    const v0, 0x7f0a01f9

    .line 295
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 299
    :cond_3
    const v0, 0x7f0a01ef

    .line 300
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public setVisibleAreaContainer(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/media/widget/AttachmentMediaView;->q:Lcom/twitter/android/media/widget/MediaEditButtonContainer;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/MediaEditButtonContainer;->setVisibleAreaContainer(Landroid/view/ViewGroup;)V

    .line 81
    return-void
.end method
