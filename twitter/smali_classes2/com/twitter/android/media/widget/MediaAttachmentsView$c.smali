.class Lcom/twitter/android/media/widget/MediaAttachmentsView$c;
.super Landroid/widget/HorizontalScrollView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/widget/MediaAttachmentsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 509
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 510
    return-void
.end method

.method private a(Z)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 601
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getScrollX()I

    move-result v5

    .line 602
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getMeasuredWidth()I

    move-result v0

    add-int v6, v5, v0

    .line 603
    invoke-virtual {p0, v4}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 604
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    .line 606
    const/4 v2, 0x0

    .line 607
    if-eqz p1, :cond_2

    move v3, v4

    .line 608
    :goto_0
    if-ge v3, v7, :cond_4

    .line 609
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 611
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 612
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v6

    if-le v6, v5, :cond_1

    move-object v0, v1

    .line 629
    :goto_1
    if-eqz v0, :cond_0

    .line 632
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 633
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 634
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    .line 635
    sub-int/2addr v0, v1

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->smoothScrollBy(II)V

    .line 637
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a:Z

    .line 639
    :cond_0
    return-void

    .line 608
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 618
    :cond_2
    add-int/lit8 v1, v7, -0x1

    move v3, v1

    :goto_2
    if-ltz v3, :cond_4

    .line 619
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 621
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_3

    .line 622
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v5

    if-ge v5, v6, :cond_3

    move-object v0, v1

    .line 624
    goto :goto_1

    .line 618
    :cond_3
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    goto :goto_2

    :cond_4
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 520
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v6, v0, v1

    .line 523
    invoke-virtual {p0, v5}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 524
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    .line 526
    const/4 v3, 0x0

    .line 527
    const v1, 0x7fffffff

    move v4, v5

    .line 528
    :goto_0
    if-ge v4, v7, :cond_4

    .line 529
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 530
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v8

    if-gt v8, v6, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v8

    if-lt v8, v6, :cond_1

    .line 546
    :goto_1
    if-eqz v2, :cond_0

    .line 549
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sub-int/2addr v0, v6

    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->smoothScrollBy(II)V

    .line 552
    :cond_0
    return-void

    .line 534
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v8

    if-ge v8, v6, :cond_2

    .line 535
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v8

    sub-int v8, v6, v8

    if-ge v8, v1, :cond_2

    .line 538
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int v1, v6, v1

    .line 528
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    goto :goto_0

    .line 539
    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v8

    if-le v8, v6, :cond_3

    .line 540
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v8, v6

    if-ge v8, v1, :cond_3

    .line 543
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, v6

    goto :goto_2

    :cond_3
    move-object v2, v3

    goto :goto_2

    :cond_4
    move-object v2, v3

    goto :goto_1
.end method

.method public fling(I)V
    .locals 1

    .prologue
    .line 584
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 587
    if-gez p1, :cond_1

    const/4 v0, 0x1

    .line 588
    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a(Z)V

    .line 590
    :cond_0
    return-void

    .line 587
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 1

    .prologue
    .line 595
    iput-boolean p3, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->b:Z

    .line 596
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->c:Z

    .line 597
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onOverScrolled(IIZZ)V

    .line 598
    return-void

    .line 596
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 556
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 557
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 560
    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 561
    iget-boolean v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a:Z

    if-nez v1, :cond_0

    .line 562
    iget-boolean v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->b:Z

    if-eqz v1, :cond_2

    .line 566
    iget-boolean v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->c:Z

    invoke-direct {p0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a(Z)V

    .line 571
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a:Z

    .line 572
    iput-boolean v3, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->b:Z

    .line 574
    :cond_1
    return v0

    .line 568
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/MediaAttachmentsView$c;->a()V

    goto :goto_0
.end method
