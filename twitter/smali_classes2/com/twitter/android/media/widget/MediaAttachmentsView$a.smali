.class Lcom/twitter/android/media/widget/MediaAttachmentsView$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/AttachmentMediaView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/widget/MediaAttachmentsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/widget/MediaAttachmentsView;


# direct methods
.method private constructor <init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;)V
    .locals 0

    .prologue
    .line 677
    iput-object p1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;Lcom/twitter/android/media/widget/MediaAttachmentsView$1;)V
    .locals 0

    .prologue
    .line 677
    invoke-direct {p0, p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;-><init>(Lcom/twitter/android/media/widget/MediaAttachmentsView;)V

    return-void
.end method

.method private e(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 720
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/widget/MediaAttachmentsView;Landroid/net/Uri;)Landroid/net/Uri;

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    .line 724
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 726
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 681
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->e(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 682
    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->g(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    .line 683
    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 684
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->a(Landroid/net/Uri;)V

    .line 686
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 0

    .prologue
    .line 690
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->e(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V

    .line 691
    return-void
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 2

    .prologue
    .line 695
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/widget/MediaAttachmentsView;Landroid/net/Uri;)Landroid/net/Uri;

    .line 697
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->b(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    .line 698
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 700
    :cond_0
    return-void
.end method

.method public c(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 0

    .prologue
    .line 704
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->e(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V

    .line 705
    return-void
.end method

.method public d(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 2

    .prologue
    .line 709
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 710
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->a(Lcom/twitter/android/media/widget/MediaAttachmentsView;Landroid/net/Uri;)Landroid/net/Uri;

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->c(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V

    .line 714
    iget-object v0, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-static {v0}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->f(Lcom/twitter/android/media/widget/MediaAttachmentsView;)Lcom/twitter/android/media/widget/MediaAttachmentsView$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/widget/MediaAttachmentsView$a;->a:Lcom/twitter/android/media/widget/MediaAttachmentsView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView;->getSelectedImage()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/media/widget/MediaAttachmentsView$b;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 716
    :cond_1
    return-void
.end method
