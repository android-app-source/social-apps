.class Lcom/twitter/android/media/widget/GifGalleryView$a;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/widget/GifGalleryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/widget/GifGalleryView;

.field private b:Z

.field private c:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/media/widget/GifGalleryView$d;",
            ">;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method constructor <init>(Lcom/twitter/android/media/widget/GifGalleryView;Ljava/lang/Iterable;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 191
    iput-object p1, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 187
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->d:Ljava/util/List;

    .line 192
    iput-boolean p3, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->b:Z

    .line 193
    iput-object p2, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->c:Ljava/lang/Iterable;

    .line 194
    return-void
.end method

.method private c(I)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 259
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 262
    const/4 v0, 0x0

    .line 263
    iget-object v2, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->c:Ljava/lang/Iterable;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v1

    move v3, v4

    move v1, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/d;

    .line 264
    iget-object v5, v0, Lcom/twitter/model/media/foundmedia/d;->h:Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    iget-object v5, v5, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v5}, Lcom/twitter/util/math/Size;->g()F

    move-result v5

    .line 266
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget-object v9, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    iget v9, v9, Lcom/twitter/android/media/widget/GifGalleryView;->c:I

    mul-int/2addr v8, v9

    sub-int v8, p1, v8

    int-to-float v8, v8

    add-float v9, v3, v5

    div-float/2addr v8, v9

    .line 267
    iget-object v9, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    iget v9, v9, Lcom/twitter/android/media/widget/GifGalleryView;->d:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_0

    .line 268
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    add-float v0, v3, v5

    move v10, v1

    move-object v1, v2

    move v2, v0

    move v0, v10

    :goto_1
    move v3, v2

    move-object v2, v1

    move v1, v0

    .line 282
    goto :goto_0

    .line 270
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 271
    new-instance v3, Lcom/twitter/android/media/widget/GifGalleryView$d;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v1, v0}, Lcom/twitter/android/media/widget/GifGalleryView$d;-><init>(ILjava/util/List;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    add-int/lit8 v0, v1, 0x1

    move-object v1, v2

    move v2, v4

    .line 273
    goto :goto_1

    .line 275
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->trimToSize()V

    .line 276
    new-instance v3, Lcom/twitter/android/media/widget/GifGalleryView$d;

    invoke-direct {v3, v1, v2}, Lcom/twitter/android/media/widget/GifGalleryView$d;-><init>(ILjava/util/List;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 278
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 279
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    move-object v1, v2

    move v2, v5

    .line 280
    goto :goto_1

    .line 283
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 284
    new-instance v0, Lcom/twitter/android/media/widget/GifGalleryView$d;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/media/widget/GifGalleryView$d;-><init>(ILjava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    :cond_3
    iput-object v6, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->d:Ljava/util/List;

    .line 288
    invoke-virtual {p0}, Lcom/twitter/android/media/widget/GifGalleryView$a;->notifyDataSetChanged()V

    .line 290
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v0, v0, Lcom/twitter/android/media/widget/GifGalleryView;->f:Lcom/twitter/android/media/widget/GifGalleryView$b;

    if-eqz v0, :cond_4

    .line 291
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v0, v0, Lcom/twitter/android/media/widget/GifGalleryView;->f:Lcom/twitter/android/media/widget/GifGalleryView$b;

    invoke-interface {v0}, Lcom/twitter/android/media/widget/GifGalleryView$b;->c()V

    .line 293
    :cond_4
    return-void
.end method


# virtual methods
.method a(I)V
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->e:I

    if-eq v0, p1, :cond_0

    .line 241
    iput p1, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->e:I

    .line 242
    invoke-direct {p0, p1}, Lcom/twitter/android/media/widget/GifGalleryView$a;->c(I)V

    .line 244
    :cond_0
    return-void
.end method

.method a(Ljava/lang/Iterable;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 234
    iput-boolean p2, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->b:Z

    .line 235
    iput-object p1, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->c:Ljava/lang/Iterable;

    .line 236
    iget v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->e:I

    invoke-direct {p0, v0}, Lcom/twitter/android/media/widget/GifGalleryView$a;->c(I)V

    .line 237
    return-void
.end method

.method b(I)I
    .locals 4

    .prologue
    .line 247
    const/4 v0, 0x0

    .line 248
    iget-object v1, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/GifGalleryView$d;

    .line 249
    iget v3, v0, Lcom/twitter/android/media/widget/GifGalleryView$d;->b:I

    sub-int v3, p1, v3

    .line 250
    if-ltz v3, :cond_0

    iget-object v0, v0, Lcom/twitter/android/media/widget/GifGalleryView$d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 255
    :goto_1
    return v1

    .line 253
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 254
    goto :goto_0

    .line 255
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 199
    iget-boolean v1, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->b:Z

    if-eqz v1, :cond_0

    .line 201
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 204
    add-int/lit8 v0, v0, -0x1

    .line 207
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 217
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v0, v0, Lcom/twitter/android/media/widget/GifGalleryView;->f:Lcom/twitter/android/media/widget/GifGalleryView$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-lt p1, v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v0, v0, Lcom/twitter/android/media/widget/GifGalleryView;->f:Lcom/twitter/android/media/widget/GifGalleryView$b;

    invoke-interface {v0}, Lcom/twitter/android/media/widget/GifGalleryView$b;->a()V

    .line 226
    :cond_0
    instance-of v0, p2, Lcom/twitter/android/media/widget/GifGalleryView$c;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/twitter/android/media/widget/GifGalleryView$c;

    .line 229
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/GifGalleryView$d;

    invoke-virtual {p2, v0}, Lcom/twitter/android/media/widget/GifGalleryView$c;->a(Lcom/twitter/android/media/widget/GifGalleryView$d;)V

    .line 230
    return-object p2

    .line 226
    :cond_1
    new-instance p2, Lcom/twitter/android/media/widget/GifGalleryView$c;

    iget-object v0, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v1, p0, Lcom/twitter/android/media/widget/GifGalleryView$a;->a:Lcom/twitter/android/media/widget/GifGalleryView;

    .line 228
    invoke-virtual {v1}, Lcom/twitter/android/media/widget/GifGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v0, v1}, Lcom/twitter/android/media/widget/GifGalleryView$c;-><init>(Lcom/twitter/android/media/widget/GifGalleryView;Landroid/content/Context;)V

    goto :goto_0
.end method
