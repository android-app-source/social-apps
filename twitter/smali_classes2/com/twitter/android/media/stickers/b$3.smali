.class final Lcom/twitter/android/media/stickers/b$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/stickers/b;->a(Ljava/util/List;J)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcec;",
        "Lcec;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcpv;


# direct methods
.method constructor <init>(JLcpv;)V
    .locals 1

    .prologue
    .line 67
    iput-wide p1, p0, Lcom/twitter/android/media/stickers/b$3;->a:J

    iput-object p3, p0, Lcom/twitter/android/media/stickers/b$3;->b:Lcpv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcec;)Lcec;
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    .line 72
    iget-object v2, v0, Lcec;->i:Lcef;

    iget-wide v4, p0, Lcom/twitter/android/media/stickers/b$3;->a:J

    invoke-virtual {v2, v4, v5}, Lcef;->a(J)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 83
    :cond_0
    :goto_0
    return-object v0

    .line 75
    :cond_1
    iget-object v2, v0, Lcec;->f:Ljava/util/List;

    iget-object v3, p0, Lcom/twitter/android/media/stickers/b$3;->b:Lcpv;

    invoke-static {v2, v3}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v6

    .line 76
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 77
    goto :goto_0

    .line 79
    :cond_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, v0, Lcec;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 83
    new-instance v1, Lcec;

    iget-wide v2, v0, Lcec;->a:J

    iget-wide v4, v0, Lcec;->b:J

    iget-object v7, v0, Lcec;->e:Ljava/lang/String;

    iget-object v8, v0, Lcec;->d:Lcdw;

    iget v9, v0, Lcec;->c:I

    iget-boolean v10, v0, Lcec;->g:Z

    iget-object v11, v0, Lcec;->h:Ljava/lang/String;

    iget-object v12, v0, Lcec;->i:Lcef;

    invoke-direct/range {v1 .. v12}, Lcec;-><init>(JJLjava/util/List;Ljava/lang/String;Lcdw;IZLjava/lang/String;Lcef;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    check-cast p1, Lcec;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/b$3;->a(Lcec;)Lcec;

    move-result-object v0

    return-object v0
.end method
