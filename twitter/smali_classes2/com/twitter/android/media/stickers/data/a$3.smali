.class Lcom/twitter/android/media/stickers/data/a$3;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/stickers/data/a;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/p;

.field final synthetic b:Lcom/twitter/android/media/stickers/data/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/stickers/data/a;Lcom/twitter/library/client/p;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/twitter/android/media/stickers/data/a$3;->b:Lcom/twitter/android/media/stickers/data/a;

    iput-object p2, p0, Lcom/twitter/android/media/stickers/data/a$3;->a:Lcom/twitter/library/client/p;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 181
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/data/a$3;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 184
    check-cast p1, Lcom/twitter/android/media/stickers/data/c;

    invoke-virtual {p1}, Lcom/twitter/android/media/stickers/data/c;->g()Lcdv;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    .line 186
    iget-object v1, p0, Lcom/twitter/android/media/stickers/data/a$3;->b:Lcom/twitter/android/media/stickers/data/a;

    invoke-static {v1}, Lcom/twitter/android/media/stickers/data/a;->h(Lcom/twitter/android/media/stickers/data/a;)Lcom/twitter/android/media/stickers/data/a$b;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/media/stickers/data/a$3;->b:Lcom/twitter/android/media/stickers/data/a;

    .line 187
    invoke-static {v2}, Lcom/twitter/android/media/stickers/data/a;->f(Lcom/twitter/android/media/stickers/data/a;)Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/media/stickers/data/a$3;->b:Lcom/twitter/android/media/stickers/data/a;

    invoke-static {v3}, Lcom/twitter/android/media/stickers/data/a;->g(Lcom/twitter/android/media/stickers/data/a;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/twitter/android/media/stickers/data/a;->a(Lcdv;Landroid/content/SharedPreferences;Ljava/lang/String;)Lcdv;

    move-result-object v2

    iget-object v2, v2, Lcdv;->b:Ljava/util/List;

    .line 186
    invoke-interface {v1, v2}, Lcom/twitter/android/media/stickers/data/a$b;->a(Ljava/util/List;)V

    .line 188
    new-instance v1, Lcom/twitter/android/media/stickers/data/d;

    iget-object v2, p0, Lcom/twitter/android/media/stickers/data/a$3;->b:Lcom/twitter/android/media/stickers/data/a;

    .line 189
    invoke-static {v2}, Lcom/twitter/android/media/stickers/data/a;->c(Lcom/twitter/android/media/stickers/data/a;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/media/stickers/data/d;-><init>(Lcom/twitter/library/provider/t;Lcdv;)V

    .line 190
    iget-object v0, p0, Lcom/twitter/android/media/stickers/data/a$3;->a:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 192
    :cond_0
    return-void
.end method
