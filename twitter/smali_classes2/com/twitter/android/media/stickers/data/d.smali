.class public Lcom/twitter/android/media/stickers/data/d;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Lcdv;


# direct methods
.method public constructor <init>(Lcom/twitter/library/provider/t;Lcdv;)V
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "write_stickers_to_db"

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/twitter/android/media/stickers/data/d;->a:Lcom/twitter/library/provider/t;

    .line 37
    iput-object p2, p0, Lcom/twitter/android/media/stickers/data/d;->b:Lcdv;

    .line 38
    return-void
.end method

.method private static a(Lcom/twitter/database/model/h;Lcom/twitter/database/model/i;Ljava/util/List;Z)Z
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/h",
            "<",
            "Lazi$a;",
            ">;",
            "Lcom/twitter/database/model/i;",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 90
    invoke-static/range {p1 .. p1}, Lcom/twitter/database/hydrator/d;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/d;

    move-result-object v5

    .line 92
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcec;

    .line 93
    new-instance v3, Lcom/twitter/android/media/stickers/data/d$1;

    move/from16 v0, p3

    invoke-direct {v3, v0}, Lcom/twitter/android/media/stickers/data/d$1;-><init>(Z)V

    .line 94
    invoke-virtual {v5, v2, v3}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Object;Lcom/twitter/database/hydrator/d$a;)J

    move-result-wide v8

    .line 101
    const-wide/16 v10, -0x1

    cmp-long v3, v8, v10

    if-nez v3, :cond_0

    .line 102
    const/4 v2, 0x0

    .line 162
    :goto_1
    return v2

    .line 105
    :cond_0
    invoke-interface/range {p1 .. p1}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v7

    .line 107
    :try_start_0
    iget-object v3, v2, Lcec;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcee;

    .line 108
    invoke-virtual {v3}, Lcee;->a()Lcdu;

    move-result-object v9

    .line 109
    new-instance v4, Lcom/twitter/android/media/stickers/data/d$2;

    invoke-direct {v4}, Lcom/twitter/android/media/stickers/data/d$2;-><init>()V

    .line 110
    invoke-virtual {v5, v9, v4}, Lcom/twitter/database/hydrator/d;->b(Ljava/lang/Object;Lcom/twitter/database/hydrator/d$a;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v10

    .line 119
    const-wide/16 v12, -0x1

    cmp-long v4, v10, v12

    if-nez v4, :cond_2

    .line 120
    const/4 v2, 0x0

    .line 159
    invoke-interface {v7}, Lcom/twitter/database/model/o;->close()V

    goto :goto_1

    .line 123
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v4, Lazi$a;

    iget-wide v10, v9, Lcdu;->h:J

    .line 124
    invoke-interface {v4, v10, v11}, Lazi$a;->b(J)Lcom/twitter/database/model/n$c;

    move-result-object v4

    check-cast v4, Lazi$a;

    iget-wide v10, v2, Lcec;->a:J

    .line 125
    invoke-interface {v4, v10, v11}, Lazi$a;->c(J)Lazi$a;

    .line 126
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/database/model/h;->c()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v10

    .line 127
    const-wide/16 v12, -0x1

    cmp-long v4, v10, v12

    if-nez v4, :cond_3

    .line 128
    const/4 v2, 0x0

    .line 159
    invoke-interface {v7}, Lcom/twitter/database/model/o;->close()V

    goto :goto_1

    .line 131
    :cond_3
    :try_start_2
    iget-object v3, v3, Lcee;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcdu;

    .line 132
    invoke-static/range {p1 .. p1}, Lcom/twitter/database/hydrator/d;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/d;

    move-result-object v4

    new-instance v11, Lcom/twitter/android/media/stickers/data/d$3;

    invoke-direct {v11, v9}, Lcom/twitter/android/media/stickers/data/d$3;-><init>(Lcdu;)V

    .line 133
    invoke-virtual {v4, v3, v11}, Lcom/twitter/database/hydrator/d;->b(Ljava/lang/Object;Lcom/twitter/database/hydrator/d$a;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v12

    .line 144
    const-wide/16 v14, -0x1

    cmp-long v4, v12, v14

    if-nez v4, :cond_5

    .line 145
    const/4 v2, 0x0

    .line 159
    invoke-interface {v7}, Lcom/twitter/database/model/o;->close()V

    goto :goto_1

    .line 148
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v4, Lazi$a;

    iget-wide v12, v3, Lcdu;->h:J

    .line 149
    invoke-interface {v4, v12, v13}, Lazi$a;->b(J)Lcom/twitter/database/model/n$c;

    move-result-object v3

    check-cast v3, Lazi$a;

    iget-wide v12, v2, Lcec;->a:J

    .line 150
    invoke-interface {v3, v12, v13}, Lazi$a;->c(J)Lazi$a;

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/database/model/h;->c()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v12

    .line 152
    const-wide/16 v14, -0x1

    cmp-long v3, v12, v14

    if-nez v3, :cond_4

    .line 153
    const/4 v2, 0x0

    .line 159
    invoke-interface {v7}, Lcom/twitter/database/model/o;->close()V

    goto/16 :goto_1

    .line 157
    :cond_6
    :try_start_4
    invoke-interface {v7}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 159
    invoke-interface {v7}, Lcom/twitter/database/model/o;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v7}, Lcom/twitter/database/model/o;->close()V

    throw v2

    .line 162
    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method protected a()Ljava/lang/Boolean;
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 42
    iget-object v0, p0, Lcom/twitter/android/media/stickers/data/d;->a:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v5

    .line 43
    invoke-static {v5}, Lcom/twitter/database/hydrator/d;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/d;

    move-result-object v6

    .line 45
    iget-object v0, p0, Lcom/twitter/android/media/stickers/data/d;->b:Lcdv;

    iget-object v0, v0, Lcdv;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 47
    const-class v0, Lazl;

    invoke-virtual {v6, v0}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;)I

    .line 48
    const-class v0, Lazi;

    invoke-virtual {v6, v0}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;)I

    .line 67
    :cond_0
    :goto_0
    const-class v0, Lazi;

    .line 68
    invoke-interface {v5, v0}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/twitter/android/media/stickers/data/d;->b:Lcdv;

    iget-object v1, v1, Lcdv;->a:Ljava/util/List;

    invoke-static {v0, v5, v1, v2}, Lcom/twitter/android/media/stickers/data/d;->a(Lcom/twitter/database/model/h;Lcom/twitter/database/model/i;Ljava/util/List;Z)Z

    move-result v1

    .line 72
    iget-object v3, p0, Lcom/twitter/android/media/stickers/data/d;->b:Lcdv;

    iget-object v3, v3, Lcdv;->b:Ljava/util/List;

    invoke-static {v0, v5, v3, v4}, Lcom/twitter/android/media/stickers/data/d;->a(Lcom/twitter/database/model/h;Lcom/twitter/database/model/i;Ljava/util/List;Z)Z

    move-result v0

    .line 76
    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 77
    :cond_1
    const-class v3, Lazl;

    invoke-virtual {v6, v3}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;)I

    .line 78
    const-class v3, Lazi;

    invoke-virtual {v6, v3}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;)I

    .line 80
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    move v2, v4

    :cond_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 49
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/media/stickers/data/d;->b:Lcdv;

    iget-object v0, v0, Lcdv;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const-class v0, Lazl;

    new-instance v1, Lcom/twitter/database/model/f$a;

    invoke-direct {v1}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v3, "is_featured"

    .line 53
    invoke-static {v3}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v1, v3, v7}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    .line 54
    invoke-virtual {v1}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v1

    .line 51
    invoke-virtual {v6, v0, v1}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;Lcom/twitter/database/model/f;)I

    .line 56
    iget-object v0, p0, Lcom/twitter/android/media/stickers/data/d;->b:Lcdv;

    iget-object v0, v0, Lcdv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v7, v0, [Ljava/lang/Long;

    .line 58
    iget-object v0, p0, Lcom/twitter/android/media/stickers/data/d;->b:Lcdv;

    iget-object v0, v0, Lcdv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    .line 59
    add-int/lit8 v3, v1, 0x1

    iget-wide v10, v0, Lcec;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v1

    move v1, v3

    .line 60
    goto :goto_1

    .line 61
    :cond_5
    const-class v0, Lazi;

    new-instance v1, Lcom/twitter/database/model/f$a;

    invoke-direct {v1}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v3, "category_id"

    .line 63
    invoke-static {v3, v7}, Laux;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v1

    .line 61
    invoke-virtual {v6, v0, v1}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;Lcom/twitter/database/model/f;)I

    goto/16 :goto_0
.end method

.method protected b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/data/d;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/data/d;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
