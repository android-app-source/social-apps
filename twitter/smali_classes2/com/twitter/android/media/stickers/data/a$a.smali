.class Lcom/twitter/android/media/stickers/data/a$a;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/stickers/data/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Lcdv;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final g:Landroid/content/SharedPreferences;


# direct methods
.method protected constructor <init>(Lcom/twitter/library/provider/t;Ljava/util/Set;Ljava/lang/String;Landroid/content/SharedPreferences;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/provider/t;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/SharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    .line 204
    const-string/jumbo v0, "load_stickers"

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 205
    iput-object p1, p0, Lcom/twitter/android/media/stickers/data/a$a;->a:Lcom/twitter/library/provider/t;

    .line 206
    iput-object p2, p0, Lcom/twitter/android/media/stickers/data/a$a;->b:Ljava/util/Set;

    .line 207
    iput-object p3, p0, Lcom/twitter/android/media/stickers/data/a$a;->c:Ljava/lang/String;

    .line 208
    iput-object p4, p0, Lcom/twitter/android/media/stickers/data/a$a;->g:Landroid/content/SharedPreferences;

    .line 209
    return-void
.end method

.method private static a(JLcom/twitter/database/model/g;)Lcdu$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/twitter/database/model/g",
            "<",
            "Lazm$a;",
            ">;)",
            "Lcdu$a;"
        }
    .end annotation

    .prologue
    .line 330
    new-instance v0, Lcdu$a;

    invoke-direct {v0}, Lcdu$a;-><init>()V

    .line 331
    invoke-virtual {v0, p0, p1}, Lcdu$a;->c(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 332
    invoke-interface {v0}, Lazm$a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcdu$a;->a(Ljava/lang/String;)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 333
    invoke-interface {v0}, Lazm$a;->l()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdu$a;->f(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 334
    invoke-interface {v0}, Lazm$a;->m()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdu$a;->g(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 335
    invoke-interface {v0}, Lazm$a;->n()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdu$a;->h(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 336
    invoke-interface {v0}, Lazm$a;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdu$a;->b(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 337
    invoke-interface {v0}, Lazm$a;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdu$a;->a(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 338
    invoke-interface {v0}, Lazm$a;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdu$a;->e(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 339
    invoke-interface {v0}, Lazm$a;->h()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdu$a;->d(J)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 340
    invoke-interface {v0}, Lazm$a;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcdu$a;->c(Ljava/lang/String;)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 341
    invoke-interface {v0}, Lazm$a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcdu$a;->b(Ljava/lang/String;)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 342
    invoke-interface {v0}, Lazm$a;->p()Lcea;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcdu$a;->a(Lcea;)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 343
    invoke-interface {v0}, Lazm$a;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcdu$a;->d(Ljava/lang/String;)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 344
    invoke-interface {v0}, Lazm$a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcdu$a;->e(Ljava/lang/String;)Lcdu$a;

    move-result-object v1

    iget-object v0, p2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazm$a;

    .line 345
    invoke-interface {v0}, Lazm$a;->o()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcdu$a;->a(Z)Lcdu$a;

    move-result-object v0

    .line 330
    return-object v0
.end method


# virtual methods
.method protected a()Lcdv;
    .locals 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/media/stickers/data/a$a;->a:Lcom/twitter/library/provider/t;

    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v2

    .line 214
    const-class v3, Lazg;

    invoke-interface {v2, v3}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v3

    .line 215
    invoke-interface {v3}, Lcom/twitter/database/model/k;->f()Lcom/twitter/database/model/l;

    move-result-object v3

    .line 217
    const-class v4, Lazm;

    invoke-interface {v2, v4}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v2

    .line 218
    invoke-interface {v2}, Lcom/twitter/database/model/k;->f()Lcom/twitter/database/model/l;

    move-result-object v15

    .line 220
    invoke-interface {v3}, Lcom/twitter/database/model/l;->c()Lcom/twitter/database/model/g;

    move-result-object v16

    .line 221
    invoke-virtual/range {v16 .. v16}, Lcom/twitter/database/model/g;->a()I

    move-result v2

    if-nez v2, :cond_0

    .line 222
    invoke-virtual/range {v16 .. v16}, Lcom/twitter/database/model/g;->close()V

    .line 223
    const/4 v2, 0x0

    .line 323
    :goto_0
    return-object v2

    .line 226
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v17

    .line 227
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v18

    .line 228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/media/stickers/data/a$a;->b:Ljava/util/Set;

    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 231
    new-instance v2, Lcom/twitter/database/model/f$a;

    invoke-direct {v2}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v3, "stickers__id"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/media/stickers/data/a$a;->b:Ljava/util/Set;

    .line 232
    invoke-static {v3, v4}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    const-string/jumbo v3, "stickers__id"

    .line 233
    invoke-virtual {v2, v3}, Lcom/twitter/database/model/f$a;->c(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    .line 234
    invoke-virtual {v2}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v2

    .line 235
    invoke-interface {v15, v2}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v3

    .line 237
    :try_start_0
    new-instance v4, Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3}, Lcom/twitter/database/model/g;->a()I

    move-result v2

    invoke-direct {v4, v2}, Landroid/support/v4/util/LongSparseArray;-><init>(I)V

    .line 238
    :cond_1
    :goto_1
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 239
    iget-object v2, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazm$a;

    invoke-interface {v2}, Lazm$a;->o()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 240
    iget-object v2, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazm$a;

    invoke-interface {v2}, Lazm$a;->a()J

    move-result-wide v6

    .line 241
    invoke-static {v6, v7, v3}, Lcom/twitter/android/media/stickers/data/a$a;->a(JLcom/twitter/database/model/g;)Lcdu$a;

    move-result-object v2

    invoke-virtual {v2}, Lcdu$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcdu;

    .line 242
    invoke-virtual {v4, v6, v7, v2}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 247
    :catchall_0
    move-exception v2

    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    throw v2

    .line 245
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/media/stickers/data/a$a;->b:Ljava/util/Set;

    invoke-static {v4, v2}, Lcom/twitter/android/media/stickers/data/a;->a(Landroid/support/v4/util/LongSparseArray;Ljava/util/Set;)Lcec;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    .line 252
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/media/stickers/data/a$a;->g:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/media/stickers/data/a$a;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/android/media/stickers/data/a;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v19

    .line 254
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/twitter/database/model/g;->d()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 255
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    invoke-interface {v2}, Lazg$a;->b()J

    move-result-wide v4

    .line 257
    const-string/jumbo v2, "category_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 258
    new-instance v2, Lcom/twitter/database/model/f$a;

    invoke-direct {v2}, Lcom/twitter/database/model/f$a;-><init>()V

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "stickers_variant_item_id"

    .line 259
    invoke-static {v8}, Laux;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    .line 260
    invoke-virtual {v2}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v2

    .line 261
    invoke-interface {v15, v2}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v6

    .line 263
    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 265
    :cond_4
    :goto_3
    :try_start_3
    invoke-virtual {v6}, Lcom/twitter/database/model/g;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 266
    iget-object v2, v6, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazm$a;

    invoke-interface {v2}, Lazm$a;->o()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 267
    iget-object v2, v6, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazm$a;

    invoke-interface {v2}, Lazm$a;->a()J

    move-result-wide v8

    .line 268
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 269
    invoke-static {v8, v9, v6}, Lcom/twitter/android/media/stickers/data/a$a;->a(JLcom/twitter/database/model/g;)Lcdu$a;

    move-result-object v8

    invoke-virtual {v8}, Lcdu$a;->q()Ljava/lang/Object;

    move-result-object v8

    .line 268
    invoke-virtual {v7, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    .line 273
    :catchall_1
    move-exception v2

    :try_start_4
    invoke-virtual {v6}, Lcom/twitter/database/model/g;->close()V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 321
    :catchall_2
    move-exception v2

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/database/model/g;->close()V

    throw v2

    .line 273
    :cond_5
    :try_start_5
    invoke-virtual {v6}, Lcom/twitter/database/model/g;->close()V

    .line 276
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v2, v6

    const/4 v3, 0x1

    const-string/jumbo v6, "stickers_variant_item_id"

    .line 277
    invoke-static {v6}, Laux;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-static {v2}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    .line 276
    invoke-interface {v15, v2, v3}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/g;

    move-result-object v6

    .line 279
    new-instance v8, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v8}, Landroid/support/v4/util/LongSparseArray;-><init>()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 281
    :cond_6
    :goto_4
    :try_start_6
    invoke-virtual {v6}, Lcom/twitter/database/model/g;->d()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 282
    iget-object v2, v6, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazm$a;

    invoke-interface {v2}, Lazm$a;->f()J

    move-result-wide v10

    .line 283
    invoke-virtual {v8, v10, v11}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 285
    if-nez v2, :cond_b

    .line 286
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 287
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v2, v3, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    invoke-virtual {v8, v10, v11, v2}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    move-object v3, v2

    .line 290
    :goto_5
    iget-object v2, v6, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazm$a;

    invoke-interface {v2}, Lazm$a;->o()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 291
    iget-object v2, v6, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazm$a;

    invoke-interface {v2}, Lazm$a;->a()J

    move-result-wide v10

    .line 292
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 293
    invoke-static {v10, v11, v6}, Lcom/twitter/android/media/stickers/data/a$a;->a(JLcom/twitter/database/model/g;)Lcdu$a;

    move-result-object v9

    invoke-virtual {v9}, Lcdu$a;->q()Ljava/lang/Object;

    move-result-object v9

    .line 292
    invoke-virtual {v3, v2, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_4

    .line 297
    :catchall_3
    move-exception v2

    :try_start_7
    invoke-virtual {v6}, Lcom/twitter/database/model/g;->close()V

    throw v2

    :cond_7
    invoke-virtual {v6}, Lcom/twitter/database/model/g;->close()V

    .line 300
    move-object/from16 v0, v19

    invoke-static {v7, v8, v0}, Lcom/twitter/android/media/stickers/data/a;->a(Ljava/util/Map;Landroid/support/v4/util/LongSparseArray;Landroid/support/v4/util/LongSparseArray;)Ljava/util/List;

    move-result-object v8

    .line 303
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    invoke-interface {v2}, Lazg$a;->h()Z

    move-result v20

    .line 304
    new-instance v3, Lcec;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    .line 306
    invoke-interface {v2}, Lazg$a;->e()J

    move-result-wide v6

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    .line 308
    invoke-interface {v2}, Lazg$a;->d()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    .line 309
    invoke-interface {v2}, Lazg$a;->c()Lcdw;

    move-result-object v10

    if-eqz v20, :cond_8

    const/4 v11, 0x2

    :goto_6
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    .line 311
    invoke-interface {v2}, Lazg$a;->i()Z

    move-result v12

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    .line 312
    invoke-interface {v2}, Lazg$a;->j()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    .line 313
    invoke-interface {v2}, Lazg$a;->f()J

    move-result-wide v22

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v2, Lazg$a;

    invoke-interface {v2}, Lazg$a;->g()J

    move-result-wide v24

    invoke-static/range {v22 .. v25}, Lcef;->a(JJ)Lcef;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, Lcec;-><init>(JJLjava/util/List;Ljava/lang/String;Lcdw;IZLjava/lang/String;Lcef;)V

    .line 314
    if-eqz v20, :cond_9

    .line 315
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_2

    .line 309
    :cond_8
    const/4 v11, 0x0

    goto :goto_6

    .line 317
    :cond_9
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto/16 :goto_2

    .line 321
    :cond_a
    invoke-virtual/range {v16 .. v16}, Lcom/twitter/database/model/g;->close()V

    .line 323
    new-instance v4, Lcdv;

    invoke-virtual/range {v17 .. v17}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual/range {v18 .. v18}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-direct {v4, v2, v3}, Lcdv;-><init>(Ljava/util/List;Ljava/util/List;)V

    move-object v2, v4

    goto/16 :goto_0

    :cond_b
    move-object v3, v2

    goto/16 :goto_5
.end method

.method protected b()Lcdv;
    .locals 3

    .prologue
    .line 351
    new-instance v0, Lcdv;

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 352
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcdv;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 351
    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/data/a$a;->b()Lcdv;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/data/a$a;->a()Lcdv;

    move-result-object v0

    return-object v0
.end method
