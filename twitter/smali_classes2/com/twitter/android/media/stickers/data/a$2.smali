.class Lcom/twitter/android/media/stickers/data/a$2;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/stickers/data/a;->f()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/p;

.field final synthetic b:Lcom/twitter/android/media/stickers/data/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/stickers/data/a;Lcom/twitter/library/client/p;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/twitter/android/media/stickers/data/a$2;->b:Lcom/twitter/android/media/stickers/data/a;

    iput-object p2, p0, Lcom/twitter/android/media/stickers/data/a$2;->a:Lcom/twitter/library/client/p;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 141
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/data/a$2;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 144
    check-cast p1, Lcom/twitter/android/media/stickers/data/b;

    .line 145
    invoke-virtual {p1}, Lcom/twitter/android/media/stickers/data/b;->g()Lcdv;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_1

    .line 147
    new-instance v1, Lcom/twitter/android/media/stickers/data/d;

    iget-object v2, p0, Lcom/twitter/android/media/stickers/data/a$2;->b:Lcom/twitter/android/media/stickers/data/a;

    .line 148
    invoke-static {v2}, Lcom/twitter/android/media/stickers/data/a;->c(Lcom/twitter/android/media/stickers/data/a;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/media/stickers/data/d;-><init>(Lcom/twitter/library/provider/t;Lcdv;)V

    .line 149
    new-instance v2, Lcom/twitter/android/media/stickers/data/a$2$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/stickers/data/a$2$1;-><init>(Lcom/twitter/android/media/stickers/data/a$2;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/stickers/data/d;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 158
    iget-object v2, p0, Lcom/twitter/android/media/stickers/data/a$2;->a:Lcom/twitter/library/client/p;

    invoke-virtual {v2, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 160
    iget-object v1, p0, Lcom/twitter/android/media/stickers/data/a$2;->b:Lcom/twitter/android/media/stickers/data/a;

    invoke-static {v1}, Lcom/twitter/android/media/stickers/data/a;->e(Lcom/twitter/android/media/stickers/data/a;)Lcom/twitter/android/media/stickers/data/StickerLruCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/media/stickers/data/StickerLruCache;->a()Ljava/util/Set;

    move-result-object v1

    .line 162
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/media/stickers/data/a$2;->b:Lcom/twitter/android/media/stickers/data/a;

    iget-object v2, p0, Lcom/twitter/android/media/stickers/data/a$2;->b:Lcom/twitter/android/media/stickers/data/a;

    invoke-static {v2}, Lcom/twitter/android/media/stickers/data/a;->f(Lcom/twitter/android/media/stickers/data/a;)Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/media/stickers/data/a$2;->b:Lcom/twitter/android/media/stickers/data/a;

    invoke-static {v3}, Lcom/twitter/android/media/stickers/data/a;->g(Lcom/twitter/android/media/stickers/data/a;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/twitter/android/media/stickers/data/a;->a(Lcdv;Landroid/content/SharedPreferences;Ljava/lang/String;)Lcdv;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/android/media/stickers/data/a;->b(Lcom/twitter/android/media/stickers/data/a;Ljava/lang/Object;)V

    .line 170
    :goto_1
    return-void

    .line 163
    :cond_0
    invoke-static {v0, v1}, Lcom/twitter/android/media/stickers/data/a;->a(Lcdv;Ljava/util/Set;)Lcdv;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/stickers/data/a$2;->b:Lcom/twitter/android/media/stickers/data/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/media/stickers/data/a;->c(Lcom/twitter/android/media/stickers/data/a;Ljava/lang/Object;)V

    goto :goto_1
.end method
