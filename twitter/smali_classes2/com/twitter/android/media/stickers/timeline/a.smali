.class public Lcom/twitter/android/media/stickers/timeline/a;
.super Lcom/twitter/app/common/list/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/stickers/timeline/a$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/android/media/stickers/timeline/a;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/media/stickers/timeline/a;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/stickers/timeline/a;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/media/stickers/timeline/a$a;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/media/stickers/timeline/a$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/stickers/timeline/a$a;-><init>(Lcom/twitter/android/media/stickers/timeline/a;)V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "sticker_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/a;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/android/media/stickers/b;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "recent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public synthetic h()Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/a;->a()Lcom/twitter/android/media/stickers/timeline/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/twitter/app/common/base/b$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/a;->a()Lcom/twitter/android/media/stickers/timeline/a$a;

    move-result-object v0

    return-object v0
.end method
