.class Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:J


# direct methods
.method constructor <init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;JLcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 548
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 549
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->a:Ljava/lang/ref/WeakReference;

    .line 550
    iput-wide p2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->c:J

    .line 551
    iput-object p4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->b:Lcom/twitter/library/client/Session;

    .line 552
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/model/core/TwitterUser;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 557
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    .line 558
    if-eqz v2, :cond_0

    .line 559
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->c:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/t;->b(J)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 560
    if-nez v0, :cond_1

    .line 561
    new-instance v1, Lbio;

    iget-object v3, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->b:Lcom/twitter/library/client/Session;

    iget-wide v4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->c:J

    invoke-direct/range {v1 .. v6}, Lbio;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    .line 562
    const/4 v0, 0x1

    invoke-static {v2, v1, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Lcom/twitter/library/service/s;I)Z

    .line 567
    :cond_0
    :goto_0
    return-object v6

    :cond_1
    move-object v6, v0

    .line 564
    goto :goto_0
.end method

.method protected a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    .line 573
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 574
    invoke-static {v0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/TwitterUser;

    .line 575
    invoke-static {v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->b(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;)V

    .line 577
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 543
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->a([Ljava/lang/Void;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 543
    check-cast p1, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->a(Lcom/twitter/model/core/TwitterUser;)V

    return-void
.end method
