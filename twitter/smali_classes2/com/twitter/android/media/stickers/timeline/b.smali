.class public Lcom/twitter/android/media/stickers/timeline/b;
.super Laou;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;JJ)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Laou;-><init>(Landroid/content/Context;)V

    .line 19
    iput-wide p2, p0, Lcom/twitter/android/media/stickers/timeline/b;->a:J

    .line 20
    iput-wide p4, p0, Lcom/twitter/android/media/stickers/timeline/b;->b:J

    .line 21
    return-void
.end method


# virtual methods
.method public a()Lapb;
    .locals 6

    .prologue
    .line 26
    new-instance v0, Lapb$a;

    invoke-direct {v0}, Lapb$a;-><init>()V

    sget-object v1, Lcom/twitter/database/schema/a$t;->a:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/android/media/stickers/timeline/b;->a:J

    .line 27
    invoke-static {v1, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    const-string/jumbo v1, "search_id=?"

    .line 28
    invoke-virtual {v0, v1}, Lapb$a;->a(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/twitter/android/media/stickers/timeline/b;->b:J

    .line 29
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lapb$a;->a([Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    sget-object v1, Lbtv;->a:[Ljava/lang/String;

    .line 30
    invoke-virtual {v0, v1}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 26
    return-object v0
.end method
