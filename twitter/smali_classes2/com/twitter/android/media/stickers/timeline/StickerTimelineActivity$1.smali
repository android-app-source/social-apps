.class Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$1;->a:Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 232
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 233
    if-nez v1, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    const-string/jumbo v0, "android.intent.extra.CHOSEN_COMPONENT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 237
    if-eqz v0, :cond_0

    .line 240
    const-string/jumbo v2, "sticker_scribe_shared_page"

    .line 241
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 242
    invoke-static {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(Landroid/content/ComponentName;Ljava/lang/String;)V

    goto :goto_0
.end method
