.class public Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;
.super Lcom/twitter/android/ScrollingHeaderActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Laow;
.implements Lcom/twitter/media/ui/image/MediaImageView$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;,
        Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/ScrollingHeaderActivity;",
        "Landroid/view/View$OnClickListener;",
        "Laow",
        "<",
        "Ljava/util/List",
        "<",
        "Lcdu;",
        ">;>;",
        "Lcom/twitter/media/ui/image/MediaImageView$b;"
    }
.end annotation


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;

.field private static final c:[Landroid/net/Uri;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private e:Lcom/twitter/android/at;

.field private f:Lcom/twitter/media/ui/image/MediaImageView;

.field private g:J

.field private h:Landroid/view/ViewGroup;

.field private i:Lcom/twitter/model/core/TwitterUser;

.field private j:Lcdu;

.field private k:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 88
    const-string/jumbo v0, "twitter://stickers/top_tweets"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a:Landroid/net/Uri;

    .line 89
    const-string/jumbo v0, "twitter://stickers/all_tweets"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->b:Landroid/net/Uri;

    .line 90
    new-array v0, v2, [Landroid/net/Uri;

    sget-object v1, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->b:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->c:[Landroid/net/Uri;

    .line 96
    const-string/jumbo v0, "top"

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v2, "all"

    aput-object v2, v1, v3

    const-string/jumbo v2, "live"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->d:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/twitter/android/ScrollingHeaderActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 119
    sget-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 120
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "sticker_id"

    .line 121
    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "sticker_tab"

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 122
    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 120
    return-object v0

    .line 121
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;I)Lcom/twitter/library/client/m;
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 300
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->l()Ljava/lang/String;

    move-result-object v5

    .line 303
    new-instance v0, Lcom/twitter/app/common/list/i$b;

    invoke-direct {v0}, Lcom/twitter/app/common/list/i$b;-><init>()V

    const v1, 0x7f0a07db

    .line 304
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i$b;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    const v1, 0x7f0a07dc

    .line 305
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i$b;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    .line 306
    invoke-virtual {v0, v6}, Lcom/twitter/app/common/list/i$b;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    .line 308
    sget-object v1, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 309
    const-class v2, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;

    .line 310
    const v1, 0x7f0a07e2

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 311
    const-string/jumbo v3, "sticker_timeline_top"

    .line 312
    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, v5}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v5, "sticker_id"

    iget-wide v6, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->g:J

    .line 313
    invoke-virtual {v1, v5, v6, v7}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;J)Lcom/twitter/app/common/base/b$a;

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    .line 325
    :goto_0
    const-string/jumbo v4, "fragment_page_number"

    .line 326
    invoke-virtual {v0, v4, p2}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/i$b;->b()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 327
    new-instance v4, Lcom/twitter/library/client/m$a;

    invoke-direct {v4, p1, v1}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 328
    invoke-virtual {v4, v3}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v1

    .line 329
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 330
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 331
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 327
    return-object v0

    .line 314
    :cond_0
    sget-object v1, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->b:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 315
    const-class v2, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;

    .line 316
    const v1, 0x7f0a07de

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 317
    const-string/jumbo v3, "sticker_timeline_all"

    .line 318
    const-string/jumbo v1, "recent"

    invoke-virtual {v0, v1, v6}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v6, "query"

    .line 319
    invoke-virtual {v1, v6, v5}, Lcom/twitter/app/common/list/i$b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/list/i$b;

    const-string/jumbo v5, "sticker_id"

    iget-wide v6, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->g:J

    .line 320
    invoke-virtual {v1, v5, v6, v7}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;J)Lcom/twitter/app/common/base/b$a;

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto :goto_0

    .line 322
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown Uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/TwitterUser;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Lcom/twitter/library/service/s;I)Z
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->b(Lcom/twitter/library/service/s;I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->n()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 156
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 158
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v2, "https"

    .line 159
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "twitter.com"

    .line 160
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "i"

    .line 161
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "stickers"

    .line 162
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->g:J

    .line 163
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 164
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 166
    iget-object v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    invoke-virtual {v2}, Lcom/twitter/android/at;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 167
    iget-object v0, v0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    .line 168
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.twitter.timeline_url_shared"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "sticker_scribe_shared_page"

    .line 169
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 170
    const/high16 v3, 0x8000000

    .line 171
    invoke-static {p0, v4, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 172
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    invoke-static {p0, v1, v4, v2}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;ZLandroid/content/IntentSender;)V

    .line 174
    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/b;->b(Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method private j()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 275
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 336
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    iget-wide v0, v0, Lcdu;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "entity_id:9.41."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    iget-wide v2, v1, Lcdu;->k:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private n()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 420
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    if-nez v0, :cond_0

    .line 441
    :goto_0
    return-void

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    const v1, 0x7f130058

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 424
    new-array v1, v3, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    new-instance v2, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v2, p0, v3}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v2, v1, v5

    .line 425
    const v2, 0x7f0a0920

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    .line 426
    invoke-virtual {v4}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "{{}}"

    .line 425
    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    const v1, 0x7f13080c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 428
    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v1, v1, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v1, :cond_1

    .line 429
    const v1, 0x7f0a0416

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 430
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 434
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    const v1, 0x7f13080b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$2;-><init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 432
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private o()V
    .locals 4

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110190

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 474
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    const v2, 0x7f13080a

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 476
    iget-object v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    if-eqz v2, :cond_1

    .line 477
    iget-object v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    iget-object v2, v2, Lcdu;->e:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/twitter/util/ui/g;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 478
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    const v1, 0x7f130321

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->f:Lcom/twitter/media/ui/image/MediaImageView;

    .line 479
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->f:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, p0}, Lcom/twitter/media/ui/image/MediaImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 480
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->f:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    iget-object v1, v1, Lcdu;->j:Lcea;

    iget-object v1, v1, Lcea;->c:Lcdw;

    iget-object v1, v1, Lcdw;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    new-instance v2, Lcom/twitter/media/util/r;

    iget-object v3, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    iget-object v3, v3, Lcdu;->j:Lcea;

    invoke-direct {v2, v3}, Lcom/twitter/media/util/r;-><init>(Lcea;)V

    .line 481
    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/a$c;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 480
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 483
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    if-nez v0, :cond_0

    .line 484
    new-instance v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;

    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    iget-wide v2, v1, Lcdu;->i:J

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v2, v3, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;-><init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;JLcom/twitter/library/client/Session;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 491
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->notifyDataSetChanged()V

    .line 492
    return-void

    .line 486
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->n()V

    goto :goto_0

    .line 489
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 279
    const v0, 0x7f0e0505

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected a(Ljava/util/List;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/PagerAdapter;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            ")",
            "Landroid/support/v4/view/PagerAdapter;"
        }
    .end annotation

    .prologue
    .line 350
    new-instance v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;

    iget-object v4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->p:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v5, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;-><init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    return-object v0
.end method

.method protected a(Ljava/util/List;)Landroid/widget/BaseAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)",
            "Landroid/widget/BaseAdapter;"
        }
    .end annotation

    .prologue
    .line 343
    new-instance v0, Lcom/twitter/android/at;

    invoke-direct {v0, p1}, Lcom/twitter/android/at;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    .line 344
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    return-object v0
.end method

.method protected a(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)Lcom/twitter/android/bp;
    .locals 2

    .prologue
    .line 182
    new-instance v0, Lcom/twitter/android/bp;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p3}, Lcom/twitter/android/bp;-><init>(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    return-object v0
.end method

.method protected a(I)V
    .locals 1

    .prologue
    .line 355
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->a(I)V

    .line 356
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->a()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 358
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    invoke-virtual {v0, p1}, Lcom/twitter/android/at;->a(I)V

    .line 360
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 369
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->f:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->f:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    .line 371
    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->f:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1}, Lcom/twitter/media/ui/image/MediaImageView;->getHeight()I

    move-result v1

    sub-int/2addr v1, v0

    .line 372
    iget v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->r:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 373
    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 374
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p1

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 376
    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->f:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setTranslationY(F)V

    .line 378
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    const v1, 0x7f13080b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 379
    const v1, 0x7f130058

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 380
    if-eqz v1, :cond_0

    .line 381
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    int-to-float v1, v1

    div-float v1, v3, v1

    sub-float v1, v2, v1

    .line 382
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 385
    :cond_0
    return-void
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 409
    return-void
.end method

.method public a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;)V
    .locals 1

    .prologue
    .line 467
    invoke-virtual {p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    if-eqz v0, :cond_0

    .line 468
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->a(Ljava/lang/String;)V

    .line 470
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;I)V
    .locals 1

    .prologue
    .line 537
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 538
    check-cast p1, Lbio;

    .line 539
    iget-object v0, p1, Lbio;->a:Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    .line 540
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->n()V

    .line 541
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 0

    .prologue
    .line 79
    check-cast p1, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 1

    .prologue
    .line 413
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 414
    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Landroid/graphics/Bitmap;)V

    .line 417
    :cond_0
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 1

    .prologue
    .line 144
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 150
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->a(Lcmm;)Z

    move-result v0

    :goto_0
    return v0

    .line 146
    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i()V

    .line 147
    const/4 v0, 0x1

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x7f1308bc
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->a(Lcmr;)Z

    .line 128
    const v0, 0x7f14002f

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 3

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Lcmr;)I

    .line 136
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 137
    const v1, 0x7f13088d

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lazv;->b(Z)Lazv;

    .line 138
    const v1, 0x7f1308bc

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 139
    const/4 v0, 0x2

    return v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "sticker_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->g:J

    .line 188
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "sticker_tab"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 191
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0403d9

    iget-object v4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    invoke-virtual {v0, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    .line 193
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    const v3, 0x7f13080a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 194
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110190

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 196
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 198
    invoke-static {}, Lbpt;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 199
    const v0, 0x7f0a0922

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 200
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->finish()V

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    if-eq v1, v6, :cond_2

    .line 206
    sget-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v3, "live"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->d:Ljava/util/List;

    const-string/jumbo v1, "all"

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 208
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 209
    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    invoke-virtual {v1, v0}, Lcom/twitter/android/at;->a(I)V

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->h:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->setHeaderView(Landroid/view/View;)V

    .line 214
    if-nez p1, :cond_5

    .line 215
    new-instance v6, Lbrm;

    new-instance v0, Lbrn;

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->g:J

    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lbrn;-><init>(IJJ)V

    invoke-direct {v6, p0, v0}, Lbrm;-><init>(Landroid/content/Context;Lbrn;)V

    .line 218
    invoke-virtual {v6, p0}, Lbrm;->a(Laow;)V

    .line 224
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    .line 229
    new-instance v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$1;-><init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;)V

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->k:Landroid/content/BroadcastReceiver;

    .line 245
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->k:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.twitter.timeline_url_shared"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 206
    :cond_3
    sget-object v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->c:[Landroid/net/Uri;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    .line 220
    :cond_5
    const-string/jumbo v0, "sticker_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    .line 221
    const-string/jumbo v0, "sticker_data"

    sget-object v1, Lcdu;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    .line 222
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->o()V

    goto :goto_2
.end method

.method public synthetic b_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 79
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->c(Ljava/util/List;)V

    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcdu;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 445
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 446
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    .line 447
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->o()V

    .line 449
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->b()Ljava/util/List;

    move-result-object v1

    .line 450
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 451
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 452
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    .line 453
    instance-of v4, v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;

    if-eqz v4, :cond_0

    .line 454
    check-cast v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;

    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->e:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->a()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 458
    iget-object v0, v0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(Ljava/lang/String;)V

    .line 463
    :goto_1
    return-void

    .line 460
    :cond_2
    const v0, 0x7f0a0922

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 461
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->finish()V

    goto :goto_1
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->d()V

    .line 267
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->k:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 270
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 258
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onResume()V

    .line 259
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 260
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j()V

    .line 262
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 251
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 252
    const-string/jumbo v0, "sticker_user"

    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->i:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 253
    const-string/jumbo v0, "sticker_data"

    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->j:Lcdu;

    sget-object v2, Lcdu;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 254
    return-void
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    return-object v0
.end method

.method protected s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    return-object v0
.end method

.method protected t()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 290
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->c:[Landroid/net/Uri;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 291
    sget-object v2, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->c:[Landroid/net/Uri;

    aget-object v2, v2, v0

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Landroid/net/Uri;I)Lcom/twitter/library/client/m;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 293
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public u()F
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x0

    return v0
.end method
