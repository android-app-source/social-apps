.class public Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"

# interfaces
.implements Laow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/TweetListFragment",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        "Lcom/twitter/android/cv;",
        ">;",
        "Laow",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:J

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    return-void
.end method

.method private q()Lbxc;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->i()Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->k()Ljava/lang/String;

    move-result-object v2

    .line 276
    const-string/jumbo v3, "tweet"

    .line 278
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    const-string/jumbo v4, "tweet"

    const-string/jumbo v5, "avatar"

    const-string/jumbo v6, "profile_click"

    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 280
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    aput-object v1, v4, v7

    aput-object v2, v4, v8

    const-string/jumbo v5, "tweet"

    aput-object v5, v4, v9

    const-string/jumbo v5, "link"

    aput-object v5, v4, v10

    const-string/jumbo v5, "open_link"

    aput-object v5, v4, v11

    invoke-static {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 282
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v7

    aput-object v2, v5, v8

    const-string/jumbo v6, "tweet"

    aput-object v6, v5, v9

    const-string/jumbo v6, "platform_photo_card"

    aput-object v6, v5, v10

    const-string/jumbo v6, "click"

    aput-object v6, v5, v11

    invoke-static {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 284
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    aput-object v1, v6, v7

    aput-object v2, v6, v8

    const-string/jumbo v1, "tweet"

    aput-object v1, v6, v9

    const-string/jumbo v1, "platform_player_card"

    aput-object v1, v6, v10

    const-string/jumbo v1, "click"

    aput-object v1, v6, v11

    invoke-static {v0, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 287
    new-instance v1, Lbxc$a;

    invoke-direct {v1}, Lbxc$a;-><init>()V

    .line 288
    invoke-virtual {v1, v3}, Lbxc$a;->a(Ljava/lang/String;)Lbxc$a;

    move-result-object v1

    .line 289
    invoke-virtual {v1, v4}, Lbxc$a;->b(Ljava/lang/String;)Lbxc$a;

    move-result-object v1

    .line 290
    invoke-virtual {v1, v5}, Lbxc$a;->c(Ljava/lang/String;)Lbxc$a;

    move-result-object v1

    .line 291
    invoke-virtual {v1, v0}, Lbxc$a;->d(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Lbxc$a;->a()Lbxc;

    move-result-object v0

    .line 287
    return-object v0
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->f()Lcom/twitter/android/media/stickers/timeline/a;

    move-result-object v0

    return-object v0
.end method

.method public H_()V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c(I)Z

    .line 205
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->f()Lcom/twitter/android/media/stickers/timeline/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0, p1}, Lcom/twitter/android/cv;->b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bl;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/twitter/android/TweetListFragment;->a(Lcbi;)V

    .line 196
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 197
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c(I)Z

    .line 200
    :cond_0
    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cu;

    .line 136
    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    .line 138
    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getReason()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getReasonIconResId()I

    move-result v2

    const/4 v3, 0x0

    .line 137
    invoke-static {v1, v2, v3}, Laji;->a(Ljava/lang/String;IZ)Laji;

    move-result-object v1

    .line 140
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/TweetActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "tw"

    .line 141
    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "association"

    .line 142
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 143
    const-string/jumbo v2, "social_proof_override"

    sget-object v3, Laji;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v2, v1, v3}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 145
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 69
    const v0, 0x7f0a07db

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->a(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0a07dc

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->b(I)Lcom/twitter/app/common/list/l$d;

    .line 71
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 259
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 260
    if-ne p2, v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    check-cast p1, Lcom/twitter/library/api/search/d;

    .line 262
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->h()I

    move-result v0

    if-nez v0, :cond_0

    if-ne p3, v1, :cond_0

    .line 263
    iput-boolean v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->e:Z

    .line 266
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 267
    instance-of v1, v0, Lcom/twitter/android/ScrollingHeaderActivity;

    if-eqz v1, :cond_1

    .line 268
    check-cast v0, Lcom/twitter/android/ScrollingHeaderActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Z)V

    .line 270
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->g:Z

    .line 166
    iput-object p1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->b:Ljava/lang/String;

    .line 167
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    new-instance v6, Laot;

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v0, Lcom/twitter/android/media/stickers/timeline/b;

    .line 170
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/media/stickers/timeline/b;-><init>(Landroid/content/Context;JJ)V

    invoke-direct {v6, v7, v8, v0}, Laot;-><init>(Landroid/support/v4/app/LoaderManager;ILcom/twitter/util/object/j;)V

    .line 171
    invoke-virtual {v6, p0}, Laot;->a(Laow;)V

    .line 173
    :cond_0
    return-void
.end method

.method protected ap_()Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic b_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method protected c(I)Z
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v7, 0x0

    .line 218
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v13, v7

    .line 254
    :goto_0
    return v13

    .line 222
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v13, [Ljava/lang/String;

    .line 223
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->b:Ljava/lang/String;

    const-string/jumbo v2, "everything"

    .line 224
    invoke-virtual {v0, v1, v2, v7, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 226
    packed-switch p1, :pswitch_data_0

    .line 241
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move v10, v7

    .line 244
    :goto_1
    new-instance v1, Lcom/twitter/library/api/search/d;

    .line 245
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c:J

    iget-object v6, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->b:Ljava/lang/String;

    const-string/jumbo v8, "stickers"

    iget-object v9, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->b:Ljava/lang/String;

    const/4 v11, 0x0

    move v12, v7

    invoke-direct/range {v1 .. v12}, Lcom/twitter/library/api/search/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    const/16 v2, 0xe

    iget-boolean v3, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->f:Z

    .line 247
    invoke-virtual {v1, v2, v7, v3, v7}, Lcom/twitter/library/api/search/d;->a(IZZZ)Lcom/twitter/library/api/search/d;

    move-result-object v1

    const-string/jumbo v2, "scribe_log"

    .line 248
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/d;

    .line 249
    iget-boolean v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->f:Z

    if-nez v1, :cond_2

    .line 250
    const-string/jumbo v1, "top"

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/d;->e(Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 251
    const-string/jumbo v1, "stickers_timeline"

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/d;->d(Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 253
    :cond_2
    invoke-virtual {p0, v0, v13, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    :pswitch_1
    move v10, v13

    .line 234
    goto :goto_1

    .line 237
    :pswitch_2
    const/4 v10, 0x2

    .line 238
    goto :goto_1

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public f()Lcom/twitter/android/media/stickers/timeline/a;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/media/stickers/timeline/a;->a(Landroid/os/Bundle;)Lcom/twitter/android/media/stickers/timeline/a;

    move-result-object v0

    return-object v0
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const-string/jumbo v0, "sticker_timeline"

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    const-string/jumbo v0, "tweets"

    return-object v0
.end method

.method protected l()V
    .locals 2

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->e:Z

    if-nez v0, :cond_0

    .line 210
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    .line 211
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c(I)Z

    .line 213
    :cond_0
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->g:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 150
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 151
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    .line 152
    if-eqz v1, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v6

    new-instance v0, Lcom/twitter/android/cv;

    const/4 v2, 0x1

    new-instance v3, Lcom/twitter/android/ct;

    iget-object v5, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 154
    invoke-direct {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->q()Lbxc;

    move-result-object v7

    invoke-direct {v3, p0, v5, v4, v7}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;)V

    iget-object v5, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cv;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 153
    invoke-virtual {v6, v0}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 157
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->f()Lcom/twitter/android/media/stickers/timeline/a;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/timeline/a;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->a:J

    .line 115
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/timeline/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->b:Ljava/lang/String;

    .line 116
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/timeline/a;->e()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->f:Z

    .line 117
    if-eqz p1, :cond_0

    .line 118
    const-string/jumbo v0, "is_last"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->e:Z

    .line 119
    const-string/jumbo v0, "search_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c:J

    .line 124
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x5

    .line 125
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 126
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 128
    return-void

    .line 121
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/timeline/a;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c:J

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 76
    const-string/jumbo v0, "is_last"

    iget-boolean v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 77
    const-string/jumbo v0, "search_id"

    iget-wide v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->c:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 78
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    .line 96
    instance-of v1, v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    if-eqz v1, :cond_0

    .line 97
    check-cast v0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    invoke-virtual {v0, p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;)V

    .line 99
    :cond_0
    const v0, 0x7f13002f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_1

    .line 101
    new-instance v1, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment$1;-><init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    :cond_1
    return-void
.end method

.method protected p()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method
