.class Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;
.super Lcom/twitter/android/AbsPagesAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic g:Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/internal/android/widget/HorizontalListView;",
            "Lcom/twitter/android/at;",
            ")V"
        }
    .end annotation

    .prologue
    .line 496
    iput-object p1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->g:Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    .line 497
    invoke-virtual {p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/AbsPagesAdapter;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    .line 498
    invoke-virtual {p3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->f:I

    .line 499
    return-void
.end method


# virtual methods
.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 503
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsPagesAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetListFragment;

    .line 504
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->a(Lcom/twitter/app/common/base/BaseFragment;I)V

    .line 506
    iget-object v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->g:Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->a(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Landroid/support/v4/app/Fragment;)V

    .line 507
    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 3

    .prologue
    .line 512
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageScrollStateChanged(I)V

    .line 513
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->e:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 515
    iget-object v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->g:Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->c(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->b(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 518
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3

    .prologue
    .line 522
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageSelected(I)V

    .line 524
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    .line 525
    iget v1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->f:I

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->c(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->a(Lcom/twitter/library/client/m;)Z

    .line 526
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->c(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v1

    .line 527
    iget-object v2, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->g:Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    invoke-static {v2, v1}, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;->c(Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;Landroid/support/v4/app/Fragment;)V

    .line 529
    iput p1, p0, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity$b;->f:I

    .line 531
    iget-object v0, v0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(Ljava/lang/String;)V

    .line 532
    return-void
.end method
