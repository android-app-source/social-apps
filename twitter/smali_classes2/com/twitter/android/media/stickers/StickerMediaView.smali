.class public Lcom/twitter/android/media/stickers/StickerMediaView;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/BaseMediaImageView$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lcom/twitter/media/ui/image/BaseMediaImageView$b",
        "<",
        "Lcom/twitter/media/ui/image/MediaImageView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcds;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/media/stickers/StickerMediaImageView;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/media/stickers/StickerSheenView;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcdt;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/twitter/android/media/stickers/a;

.field private g:Lcom/twitter/ui/widget/MultiTouchImageView;

.field private h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:J

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    iput-object p1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->a:Landroid/content/Context;

    .line 85
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->b:Ljava/util/List;

    .line 86
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    .line 87
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->d:Ljava/util/List;

    .line 88
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->e:Ljava/util/List;

    .line 89
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->h:Ljava/util/Set;

    .line 90
    iput-boolean v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->j:Z

    .line 91
    iput-boolean v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->k:Z

    .line 92
    return-void
.end method

.method private a(Landroid/view/View;II)I
    .locals 4

    .prologue
    .line 257
    move-object v0, p1

    check-cast v0, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 260
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 264
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int v2, p2, v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    .line 265
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v3, p3, v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    .line 267
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 268
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 269
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 271
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/media/stickers/StickerMediaImageView;)Lcom/twitter/android/media/stickers/StickerSheenView;
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 169
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/StickerSheenView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/media/stickers/StickerMediaView;Lcom/twitter/android/media/stickers/StickerMediaImageView;)Lcom/twitter/android/media/stickers/StickerSheenView;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Lcom/twitter/android/media/stickers/StickerMediaImageView;)Lcom/twitter/android/media/stickers/StickerSheenView;

    move-result-object v0

    return-object v0
.end method

.method private a(Lbrm;)V
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->j:Z

    .line 305
    new-instance v0, Lcom/twitter/android/media/stickers/StickerMediaView$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/stickers/StickerMediaView$2;-><init>(Lcom/twitter/android/media/stickers/StickerMediaView;)V

    invoke-virtual {p1, v0}, Lbrm;->a(Laow;)V

    .line 318
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcdt;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 344
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->h:Ljava/util/Set;

    .line 345
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 346
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 347
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 348
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdt;

    .line 349
    new-instance v4, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    iget-object v5, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->a:Landroid/content/Context;

    invoke-direct {v4, v5, v0}, Lcom/twitter/android/media/stickers/StickerMediaImageView;-><init>(Landroid/content/Context;Lcdt;)V

    .line 350
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x3a

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lcdt;->a:J

    .line 351
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 352
    invoke-virtual {v4, v5}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->setTag(Ljava/lang/Object;)V

    .line 353
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->setScaleMode(I)V

    .line 354
    iget-object v5, v0, Lcdt;->c:Lcea;

    iget-object v5, v5, Lcea;->c:Lcdw;

    iget-object v5, v5, Lcdw;->c:Ljava/lang/String;

    invoke-static {v5}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 355
    sget-object v5, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->a:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 356
    iget v5, v0, Lcdt;->d:F

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->setRotation(F)V

    .line 357
    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->setVisibility(I)V

    .line 358
    invoke-virtual {v4, p0}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 359
    invoke-virtual {p0, v4}, Lcom/twitter/android/media/stickers/StickerMediaView;->addView(Landroid/view/View;)V

    .line 361
    new-instance v5, Lcom/twitter/android/media/stickers/StickerSheenView;

    iget-object v6, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->a:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/twitter/android/media/stickers/StickerSheenView;-><init>(Landroid/content/Context;)V

    .line 362
    sget-object v6, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Lcom/twitter/android/media/stickers/StickerSheenView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 363
    iget v0, v0, Lcdt;->d:F

    invoke-virtual {v5, v0}, Lcom/twitter/android/media/stickers/StickerSheenView;->setRotation(F)V

    .line 364
    invoke-virtual {p0, v5}, Lcom/twitter/android/media/stickers/StickerMediaView;->addView(Landroid/view/View;)V

    .line 366
    invoke-virtual {v4}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 367
    iput v8, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 368
    iput v8, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 369
    invoke-virtual {v1, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 371
    invoke-virtual {v5}, Lcom/twitter/android/media/stickers/StickerSheenView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 372
    iput v8, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 373
    iput v8, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 374
    invoke-virtual {v2, v5}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_0

    .line 376
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    .line 377
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->d:Ljava/util/List;

    .line 379
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->a:Landroid/content/Context;

    instance-of v0, v0, Lcom/twitter/android/GalleryActivity;

    if-eqz v0, :cond_1

    .line 380
    new-instance v1, Lcom/twitter/android/media/stickers/a;

    iget-object v2, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    iget-object v3, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    .line 381
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/media/stickers/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->f:Lcom/twitter/android/media/stickers/a;

    .line 383
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/stickers/StickerMediaView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->l:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/media/stickers/StickerMediaView;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/media/stickers/StickerMediaView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    return-object v0
.end method

.method private b(Lcom/twitter/android/media/stickers/StickerMediaImageView;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 275
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getPaddingLeft()I

    move-result v0

    .line 276
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 277
    invoke-virtual {p1}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->getStickerTag()Lcdt;

    move-result-object v2

    .line 278
    invoke-virtual {v2, v1}, Lcdt;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v3

    .line 279
    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->a()I

    move-result v4

    .line 280
    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->b()I

    move-result v3

    .line 282
    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 283
    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 281
    invoke-virtual {p1, v5, v6}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->measure(II)V

    .line 289
    invoke-virtual {v2, v1}, Lcdt;->b(I)F

    move-result v5

    div-int/lit8 v6, v4, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    add-int/2addr v0, v5

    .line 291
    invoke-virtual {v2, v1}, Lcdt;->c(I)F

    move-result v1

    div-int/lit8 v2, v3, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 292
    add-int v2, v0, v4

    add-int v5, v1, v3

    invoke-virtual {p1, v0, v1, v2, v5}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->layout(IIII)V

    .line 294
    invoke-direct {p0, p1}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Lcom/twitter/android/media/stickers/StickerMediaImageView;)Lcom/twitter/android/media/stickers/StickerSheenView;

    move-result-object v2

    .line 295
    if-eqz v2, :cond_0

    .line 297
    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 298
    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 296
    invoke-virtual {v2, v5, v6}, Lcom/twitter/android/media/stickers/StickerSheenView;->measure(II)V

    .line 299
    add-int/2addr v4, v0

    add-int/2addr v3, v1

    invoke-virtual {v2, v0, v1, v4, v3}, Lcom/twitter/android/media/stickers/StickerSheenView;->layout(IIII)V

    .line 301
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/media/stickers/StickerMediaView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->m:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/media/stickers/StickerMediaView;)Lcom/twitter/android/media/stickers/a;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->f:Lcom/twitter/android/media/stickers/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 122
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->i:J

    .line 123
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 0

    .prologue
    .line 48
    check-cast p1, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 2

    .prologue
    .line 387
    instance-of v0, p1, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    invoke-virtual {p1}, Lcom/twitter/media/ui/image/MediaImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 389
    iget-object v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->h:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 390
    check-cast p1, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    invoke-direct {p0, p1}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Lcom/twitter/android/media/stickers/StickerMediaImageView;)Lcom/twitter/android/media/stickers/StickerSheenView;

    move-result-object v1

    .line 391
    if-eqz v1, :cond_0

    .line 392
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/stickers/StickerSheenView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 395
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Z)V

    .line 398
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;Lbrm;Lcom/twitter/ui/widget/MultiTouchImageView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcds;",
            ">;",
            "Lbrm;",
            "Lcom/twitter/ui/widget/MultiTouchImageView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a()V

    .line 101
    iput-object p3, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->g:Lcom/twitter/ui/widget/MultiTouchImageView;

    .line 103
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->j:Z

    if-nez v0, :cond_0

    .line 104
    iput-object p1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->b:Ljava/util/List;

    .line 105
    invoke-direct {p0, p2}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Lbrm;)V

    .line 107
    :cond_0
    return-void
.end method

.method a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcdu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 321
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->b:Ljava/util/List;

    new-instance v1, Lcom/twitter/android/media/stickers/StickerMediaView$3;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/media/stickers/StickerMediaView$3;-><init>(Lcom/twitter/android/media/stickers/StickerMediaView;Ljava/util/Map;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->e:Ljava/util/List;

    .line 336
    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->k:Z

    if-eqz v0, :cond_0

    .line 337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->k:Z

    .line 338
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Ljava/util/List;)V

    .line 341
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 126
    iput-boolean v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->l:Z

    .line 128
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->g:Lcom/twitter/ui/widget/MultiTouchImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->g:Lcom/twitter/ui/widget/MultiTouchImageView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/MultiTouchImageView;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->i:J

    sub-long/2addr v0, v2

    .line 134
    const-wide/16 v2, 0x0

    if-eqz p1, :cond_1

    const-wide/16 v4, 0x1f4

    sub-long v0, v4, v0

    :goto_1
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 136
    new-instance v2, Lcom/twitter/android/media/stickers/StickerMediaView$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/stickers/StickerMediaView$1;-><init>(Lcom/twitter/android/media/stickers/StickerMediaView;)V

    invoke-virtual {p0, v2, v0, v1}, Lcom/twitter/android/media/stickers/StickerMediaView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 134
    :cond_1
    const-wide/16 v4, 0x12c

    sub-long v0, v4, v0

    goto :goto_1

    .line 158
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->j:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 159
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Ljava/util/List;)V

    goto :goto_0

    .line 161
    :cond_3
    iput-boolean v1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->k:Z

    goto :goto_0
.end method

.method public a(II)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 211
    iget-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->m:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lbpt;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v3

    .line 253
    :goto_0
    return v0

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getPaddingLeft()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 216
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getPaddingTop()I

    move-result v0

    if-lt p2, v0, :cond_2

    .line 218
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    if-le p2, v0, :cond_3

    :cond_2
    move v0, v3

    .line 219
    goto :goto_0

    .line 221
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_1
    if-ltz v4, :cond_5

    .line 222
    invoke-virtual {p0, v4}, Lcom/twitter/android/media/stickers/StickerMediaView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 223
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    instance-of v0, v1, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    if-eqz v0, :cond_4

    .line 226
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    move-object v0, v1

    .line 227
    check-cast v0, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->getStickerTag()Lcdt;

    move-result-object v0

    iget v0, v0, Lcdt;->d:F

    neg-float v0, v0

    .line 228
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v6

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    int-to-float v7, v7

    .line 227
    invoke-virtual {v5, v0, v6, v7}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 229
    const/4 v0, 0x2

    new-array v0, v0, [F

    int-to-float v6, p1

    aput v6, v0, v3

    int-to-float v6, p2

    aput v6, v0, v2

    .line 230
    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 232
    aget v5, v0, v3

    float-to-int v5, v5

    .line 233
    aget v0, v0, v2

    float-to-int v0, v0

    .line 237
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v6

    if-lt v5, v6, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v6

    if-gt v5, v6, :cond_4

    .line 238
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    if-lt v0, v6, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v6

    if-gt v0, v6, :cond_4

    .line 239
    invoke-direct {p0, v1, v5, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Landroid/view/View;II)I

    move-result v0

    if-lez v0, :cond_4

    .line 240
    check-cast v1, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    invoke-virtual {v1}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->getStickerTag()Lcdt;

    move-result-object v0

    iget-wide v0, v0, Lcdt;->a:J

    .line 241
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->c()V

    .line 242
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->a:Landroid/content/Context;

    const-class v5, Lcom/twitter/android/media/stickers/timeline/StickerTimelineActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    const-string/jumbo v4, "sticker_id"

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 244
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 246
    invoke-static {}, Lcom/twitter/android/media/imageeditor/stickers/b;->a()V

    move v0, v2

    .line 248
    goto/16 :goto_0

    .line 221
    :cond_4
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto/16 :goto_1

    :cond_5
    move v0, v3

    .line 253
    goto/16 :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 179
    iput-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->l:Z

    .line 180
    iput-object v3, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->f:Lcom/twitter/android/media/stickers/a;

    .line 181
    iput-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->j:Z

    .line 182
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    .line 183
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->d:Ljava/util/List;

    .line 184
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getChildCount()I

    move-result v0

    .line 185
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    .line 186
    invoke-virtual {p0, v2}, Lcom/twitter/android/media/stickers/StickerMediaView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 187
    instance-of v0, v1, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 188
    check-cast v0, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 190
    :cond_0
    instance-of v0, v1, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/twitter/android/media/stickers/StickerSheenView;

    if-eqz v0, :cond_2

    .line 191
    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/stickers/StickerMediaView;->removeView(Landroid/view/View;)V

    .line 185
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 194
    :cond_3
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->l:Z

    .line 198
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    .line 199
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaImageView;->a()V

    .line 200
    invoke-direct {p0, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Lcom/twitter/android/media/stickers/StickerMediaImageView;)Lcom/twitter/android/media/stickers/StickerSheenView;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerSheenView;->c()V

    goto :goto_0

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->f:Lcom/twitter/android/media/stickers/a;

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->f:Lcom/twitter/android/media/stickers/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/a;->b()V

    .line 208
    :cond_2
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 174
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 175
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->b()V

    .line 176
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 111
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getChildCount()I

    move-result v2

    .line 113
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 114
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/stickers/StickerMediaView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 115
    instance-of v3, v0, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    if-eqz v3, :cond_0

    .line 116
    check-cast v0, Lcom/twitter/android/media/stickers/StickerMediaImageView;

    invoke-direct {p0, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->b(Lcom/twitter/android/media/stickers/StickerMediaImageView;)V

    .line 113
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 119
    :cond_1
    return-void
.end method

.method public setShouldShowStickerVisualHashtags(Z)V
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/twitter/android/media/stickers/StickerMediaView;->m:Z

    .line 96
    return-void
.end method
