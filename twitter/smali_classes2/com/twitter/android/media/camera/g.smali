.class public Lcom/twitter/android/media/camera/g;
.super Lcom/twitter/android/media/camera/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/camera/g$b;,
        Lcom/twitter/android/media/camera/g$a;
    }
.end annotation


# static fields
.field private static final e:[[Lcom/twitter/android/media/camera/f;


# instance fields
.field private final f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

.field private final g:Landroid/view/View;

.field private final h:Lcom/twitter/android/media/camera/g$b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f1301cb

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [[Lcom/twitter/android/media/camera/f;

    new-array v1, v6, [Lcom/twitter/android/media/camera/f;

    new-instance v2, Lcom/twitter/android/media/camera/f;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v2, v1, v4

    new-instance v2, Lcom/twitter/android/media/camera/f;

    const/16 v3, 0xb

    invoke-direct {v2, v3}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v5, [Lcom/twitter/android/media/camera/f;

    new-instance v2, Lcom/twitter/android/media/camera/f;

    invoke-direct {v2, v4, v7}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v2, v1, v4

    aput-object v1, v0, v5

    new-array v1, v5, [Lcom/twitter/android/media/camera/f;

    new-instance v2, Lcom/twitter/android/media/camera/f;

    invoke-direct {v2, v5, v7}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v2, v1, v4

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/android/media/camera/g;->e:[[Lcom/twitter/android/media/camera/f;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/b;)V
    .locals 2

    .prologue
    .line 70
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/media/camera/a;-><init>(Landroid/content/Context;Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/b;I)V

    .line 71
    invoke-interface {p3}, Lcom/twitter/android/media/camera/b;->a()Landroid/view/View;

    move-result-object v1

    .line 72
    const v0, 0x7f1301bf

    .line 73
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iput-object v0, p0, Lcom/twitter/android/media/camera/g;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 74
    const v0, 0x7f1301c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/g;->g:Landroid/view/View;

    .line 75
    new-instance v0, Lcom/twitter/android/media/camera/g$b;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/media/camera/g$b;-><init>(Lcom/twitter/android/media/camera/g;Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    .line 76
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/g;)Lcom/twitter/android/media/widget/CameraPreviewContainer;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/g;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 219
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "twitter_camera"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "speed_bump"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 220
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 221
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 222
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/media/camera/g;)Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->g:Landroid/view/View;

    return-object v0
.end method

.method private c(I)F
    .locals 3

    .prologue
    const v1, 0x3faaaaab

    const/high16 v0, 0x3f400000    # 0.75f

    .line 201
    invoke-static {}, Lcom/twitter/android/util/b;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 202
    if-nez p1, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 202
    goto :goto_0

    .line 206
    :cond_2
    iget-object v2, p0, Lcom/twitter/android/media/camera/g;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v2}, Lcom/twitter/android/media/camera/c;->a()Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 207
    if-nez v2, :cond_3

    .line 208
    if-eqz p1, :cond_0

    move v0, v1

    goto :goto_0

    .line 211
    :cond_3
    iget v0, v2, Landroid/hardware/Camera$Size;->width:I

    iget v1, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    .line 212
    iget v1, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    .line 213
    if-nez p1, :cond_4

    div-float v0, v1, v0

    goto :goto_0

    :cond_4
    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 191
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "twitter_camera::photo:shutter:click"

    aput-object v2, v1, v3

    .line 192
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 193
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 195
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0, v3}, Lcom/twitter/android/media/camera/b;->a(Z)V

    .line 198
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->b(Z)V

    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->b()V

    .line 98
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->k()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_0

    .line 101
    invoke-static {}, Lcom/twitter/android/util/b;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    .line 100
    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 105
    :cond_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 101
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    sget-object v1, Lcom/twitter/android/media/camera/g;->e:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    .line 185
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/g;->c(I)F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a(FZ)V

    .line 186
    iget-object v1, p0, Lcom/twitter/android/media/camera/g;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    if-nez p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->setScaleMode(I)V

    .line 188
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/g$b;->a(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public a(Lcom/twitter/app/common/base/b;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/camera/g$b;->a(Lcom/twitter/app/common/base/b;Landroid/os/Bundle;)V

    .line 86
    return-void
.end method

.method public a(Lcom/twitter/media/model/ImageFile;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/g$b;->a(Lcom/twitter/media/model/ImageFile;)V

    .line 160
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->b()V

    .line 161
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x1b

    if-ne v0, v1, :cond_1

    .line 143
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 144
    invoke-direct {p0}, Lcom/twitter/android/media/camera/g;->g()V

    .line 146
    :cond_0
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 123
    invoke-static {p2}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 135
    :goto_0
    :pswitch_0
    return v0

    .line 128
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-static {p1, v2, v3}, Lcom/twitter/android/util/b;->a(Landroid/view/View;II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    invoke-direct {p0}, Lcom/twitter/android/media/camera/g;->g()V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 132
    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 111
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/g$b;->b(I)V

    .line 156
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->g()V

    .line 118
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->c()V

    .line 167
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->e()V

    .line 175
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/android/media/camera/g;->h:Lcom/twitter/android/media/camera/g$b;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/g$b;->f()V

    .line 180
    return-void
.end method
