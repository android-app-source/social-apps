.class Lcom/twitter/android/media/camera/i$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/media/model/VideoFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/i;


# direct methods
.method private constructor <init>(Lcom/twitter/android/media/camera/i;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/twitter/android/media/camera/i$a;->a:Lcom/twitter/android/media/camera/i;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/media/camera/i;Lcom/twitter/android/media/camera/i$1;)V
    .locals 0

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/i$a;-><init>(Lcom/twitter/android/media/camera/i;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/media/model/VideoFile;
    .locals 1

    .prologue
    .line 363
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 364
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$a;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->b(Lcom/twitter/android/media/camera/i;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/model/VideoFile;->a(Ljava/io/File;)Lcom/twitter/media/model/VideoFile;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/media/model/VideoFile;)V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$a;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    if-nez v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 372
    :cond_0
    if-nez p1, :cond_1

    .line 373
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$a;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/media/camera/i$d;->j()V

    .line 377
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$a;->a:Lcom/twitter/android/media/camera/i;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;Z)Z

    goto :goto_0

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$a;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/media/camera/i$d;->a(Lcom/twitter/media/model/VideoFile;)V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 360
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/i$a;->a([Ljava/lang/Void;)Lcom/twitter/media/model/VideoFile;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 360
    check-cast p1, Lcom/twitter/media/model/VideoFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/i$a;->a(Lcom/twitter/media/model/VideoFile;)V

    return-void
.end method
