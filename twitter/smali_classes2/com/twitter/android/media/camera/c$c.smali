.class Lcom/twitter/android/media/camera/c$c;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/hardware/Camera;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/c;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/c;I)V
    .locals 0

    .prologue
    .line 861
    iput-object p1, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 862
    iput p2, p0, Lcom/twitter/android/media/camera/c$c;->b:I

    .line 863
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/hardware/Camera;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 868
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c$c;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 893
    :goto_0
    return-object v0

    .line 872
    :cond_0
    :try_start_0
    iget v1, p0, Lcom/twitter/android/media/camera/c$c;->b:I

    invoke-static {v1}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v1

    .line 878
    iget-object v2, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    iget-object v3, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    .line 879
    invoke-static {v3}, Lcom/twitter/android/media/camera/c;->b(Lcom/twitter/android/media/camera/c;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/n;->a(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/camera/c;->e(I)I

    move-result v2

    .line 878
    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 880
    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 882
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v3

    .line 881
    invoke-static {v3}, Lcom/twitter/android/util/b;->a(Ljava/util/List;)[I

    move-result-object v3

    .line 883
    if-eqz v3, :cond_1

    .line 884
    const/4 v4, 0x0

    aget v4, v3, v4

    const/4 v5, 0x1

    aget v3, v3, v5

    invoke-virtual {v2, v4, v3}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 887
    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move-object v0, v1

    .line 890
    goto :goto_0

    .line 891
    :catch_0
    move-exception v1

    .line 892
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected a(Landroid/hardware/Camera;)V
    .locals 2

    .prologue
    .line 900
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->g(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$c;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 901
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/c$c;)Lcom/twitter/android/media/camera/c$c;

    .line 903
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera;)V

    .line 904
    return-void
.end method

.method protected b(Landroid/hardware/Camera;)V
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->g(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$c;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 910
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$c;->a:Lcom/twitter/android/media/camera/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/c$c;)Lcom/twitter/android/media/camera/c$c;

    .line 912
    :cond_0
    if-eqz p1, :cond_1

    .line 913
    invoke-static {p1}, Lcom/twitter/android/media/camera/c;->b(Landroid/hardware/Camera;)V

    .line 915
    :cond_1
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 857
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/c$c;->a([Ljava/lang/Void;)Landroid/hardware/Camera;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 857
    check-cast p1, Landroid/hardware/Camera;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/c$c;->b(Landroid/hardware/Camera;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 857
    check-cast p1, Landroid/hardware/Camera;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/c$c;->a(Landroid/hardware/Camera;)V

    return-void
.end method
