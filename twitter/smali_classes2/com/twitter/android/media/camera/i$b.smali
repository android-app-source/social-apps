.class Lcom/twitter/android/media/camera/i$b;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/i;

.field private final b:I

.field private final c:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/i;I)V
    .locals 2

    .prologue
    .line 294
    iput-object p1, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 295
    iput p2, p0, Lcom/twitter/android/media/camera/i$b;->b:I

    .line 296
    invoke-virtual {p1}, Lcom/twitter/android/media/camera/i;->d()I

    move-result v0

    const/16 v1, 0x7d0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/media/camera/i$b;->c:Z

    .line 297
    return-void

    .line 296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 301
    iget-object v1, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    iget v2, p0, Lcom/twitter/android/media/camera/i$b;->b:I

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/camera/i;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 304
    :goto_0
    return-object v0

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/media/camera/i$b;->c:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    iget v2, p0, Lcom/twitter/android/media/camera/i$b;->b:I

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/camera/i;->c(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 309
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/media/camera/i$d;->i()V

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;Lcom/twitter/android/media/camera/i$b;)Lcom/twitter/android/media/camera/i$b;

    .line 317
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/twitter/android/media/camera/i$d;->a(Z)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 289
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/i$b;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 3

    .prologue
    .line 324
    new-instance v0, Lcom/twitter/android/media/camera/i$c;

    iget-object v1, p0, Lcom/twitter/android/media/camera/i$b;->a:Lcom/twitter/android/media/camera/i;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/media/camera/i$c;-><init>(Lcom/twitter/android/media/camera/i;Z)V

    sget-object v1, Lcom/twitter/android/media/camera/i$b;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/camera/i$c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 325
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 289
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/i$b;->a(Ljava/lang/Boolean;)V

    return-void
.end method
