.class public Lcom/twitter/android/media/camera/h;
.super Lcom/twitter/android/media/camera/a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/camera/VideoTextureView$a;
.implements Lcom/twitter/android/media/camera/i$d;
.implements Lcom/twitter/android/media/widget/VideoSegmentEditView$f;
.implements Lcom/twitter/android/media/widget/VideoSegmentEditView$h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/camera/h$a;
    }
.end annotation


# static fields
.field static final synthetic e:Z

.field private static final f:[I

.field private static final g:[[Lcom/twitter/android/media/camera/f;

.field private static final h:[[Lcom/twitter/android/media/camera/f;

.field private static final i:[[Lcom/twitter/android/media/camera/f;

.field private static final j:[[Lcom/twitter/android/media/camera/f;

.field private static final k:[[Lcom/twitter/android/media/camera/f;

.field private static l:I

.field private static m:I


# instance fields
.field private A:Lcom/twitter/android/media/camera/VideoTextureView;

.field private B:I

.field private final C:Ljava/lang/Runnable;

.field private final D:Ljava/lang/Runnable;

.field private final E:Landroid/view/View$OnClickListener;

.field private final F:Landroid/os/Handler;

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:Z

.field private L:Z

.field private M:I

.field private N:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/media/model/VideoFile;",
            ">;"
        }
    .end annotation
.end field

.field private O:Z

.field private P:Lcom/twitter/media/model/SegmentedVideoFile$a;

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:I

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:I

.field private af:Z

.field private ag:I

.field private final n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

.field private final o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

.field private final p:Landroid/view/View;

.field private final q:Lcom/twitter/android/media/widget/HoverGarbageCanView;

.field private final r:Lcom/twitter/android/media/widget/CameraShutterBar;

.field private final s:Lcom/twitter/library/media/widget/VideoDurationView;

.field private final t:Lcom/twitter/android/media/widget/VideoSegmentListView;

.field private final u:Landroid/view/View;

.field private final v:Lcom/twitter/android/media/camera/h$a;

.field private final w:Landroid/widget/ProgressBar;

.field private final x:Landroid/view/animation/Animation;

.field private final y:Landroid/view/animation/Animation;

.field private z:Lcom/twitter/android/media/camera/VideoTextureView;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x9

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 90
    const-class v0, Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/media/camera/h;->e:Z

    .line 121
    new-array v0, v8, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/media/camera/h;->f:[I

    .line 130
    new-array v0, v8, [[Lcom/twitter/android/media/camera/f;

    new-array v3, v7, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    aput-object v3, v0, v2

    new-array v3, v1, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    aput-object v3, v0, v1

    new-array v3, v1, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    aput-object v3, v0, v7

    sput-object v0, Lcom/twitter/android/media/camera/h;->g:[[Lcom/twitter/android/media/camera/f;

    .line 146
    new-array v0, v8, [[Lcom/twitter/android/media/camera/f;

    new-array v3, v7, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    aput-object v3, v0, v2

    new-array v3, v7, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v2, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v1

    aput-object v3, v0, v1

    new-array v3, v7, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v1, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v1

    aput-object v3, v0, v7

    sput-object v0, Lcom/twitter/android/media/camera/h;->h:[[Lcom/twitter/android/media/camera/f;

    .line 164
    new-array v0, v8, [[Lcom/twitter/android/media/camera/f;

    new-array v3, v8, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301ca

    invoke-direct {v4, v8, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v7

    aput-object v3, v0, v2

    new-array v3, v8, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301ca

    invoke-direct {v4, v7, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v2, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v7

    aput-object v3, v0, v1

    new-array v3, v8, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301ca

    invoke-direct {v4, v7, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v1, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v7

    aput-object v3, v0, v7

    sput-object v0, Lcom/twitter/android/media/camera/h;->i:[[Lcom/twitter/android/media/camera/f;

    .line 185
    new-array v0, v8, [[Lcom/twitter/android/media/camera/f;

    new-array v3, v1, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/4 v5, 0x5

    const v6, 0x7f1301bf

    invoke-direct {v4, v5, v6}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    aput-object v3, v0, v2

    new-array v3, v1, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/4 v5, 0x5

    const v6, 0x7f1301bf

    invoke-direct {v4, v5, v6}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    aput-object v3, v0, v1

    new-array v3, v1, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v1, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    aput-object v3, v0, v7

    sput-object v0, Lcom/twitter/android/media/camera/h;->j:[[Lcom/twitter/android/media/camera/f;

    .line 200
    new-array v0, v8, [[Lcom/twitter/android/media/camera/f;

    new-array v3, v8, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301bf

    invoke-direct {v4, v8, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v7

    aput-object v3, v0, v2

    new-array v3, v8, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    invoke-direct {v4, v9}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v2, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xc

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v7

    aput-object v3, v0, v1

    new-array v3, v8, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v2, Lcom/twitter/android/media/camera/f;

    const v4, 0x7f1301cb

    invoke-direct {v2, v1, v4}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v2, v3, v1

    new-instance v1, Lcom/twitter/android/media/camera/f;

    const/16 v2, 0xc

    invoke-direct {v1, v2}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v1, v3, v7

    aput-object v3, v0, v7

    sput-object v0, Lcom/twitter/android/media/camera/h;->k:[[Lcom/twitter/android/media/camera/f;

    return-void

    :cond_0
    move v0, v2

    .line 90
    goto/16 :goto_0

    .line 121
    :array_0
    .array-data 4
        0x7f0e057a
        0x7f0e0579
        0x7f0e0578
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/b;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 311
    invoke-direct {p0, p1, p2, p3, v2}, Lcom/twitter/android/media/camera/a;-><init>(Landroid/content/Context;Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/b;I)V

    .line 242
    new-instance v0, Lcom/twitter/android/media/camera/h$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/camera/h$1;-><init>(Lcom/twitter/android/media/camera/h;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->C:Ljava/lang/Runnable;

    .line 249
    new-instance v0, Lcom/twitter/android/media/camera/h$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/camera/h$4;-><init>(Lcom/twitter/android/media/camera/h;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->D:Ljava/lang/Runnable;

    .line 259
    new-instance v0, Lcom/twitter/android/media/camera/h$5;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/camera/h$5;-><init>(Lcom/twitter/android/media/camera/h;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->E:Landroid/view/View$OnClickListener;

    .line 279
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->F:Landroid/os/Handler;

    .line 288
    iput v4, p0, Lcom/twitter/android/media/camera/h;->M:I

    .line 292
    iput v1, p0, Lcom/twitter/android/media/camera/h;->Q:I

    .line 293
    iput v1, p0, Lcom/twitter/android/media/camera/h;->R:I

    .line 294
    iput v1, p0, Lcom/twitter/android/media/camera/h;->S:I

    .line 302
    iput v2, p0, Lcom/twitter/android/media/camera/h;->aa:I

    .line 313
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v1

    .line 315
    new-instance v0, Lcom/twitter/android/media/camera/h$a;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/media/camera/h$a;-><init>(Lcom/twitter/android/media/camera/h;Lcom/twitter/android/media/camera/h$1;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    .line 317
    invoke-interface {p3}, Lcom/twitter/android/media/camera/b;->a()Landroid/view/View;

    move-result-object v2

    .line 318
    const v0, 0x7f1301be

    .line 319
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/VideoSegmentEditView;

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    .line 321
    const v0, 0x7f1301bf

    .line 322
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 324
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    const v3, 0x7f1301c5

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    .line 326
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    const v3, 0x7f1301c2

    .line 327
    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/HoverGarbageCanView;

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->q:Lcom/twitter/android/media/widget/HoverGarbageCanView;

    .line 329
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/VideoSegmentListView;

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    .line 330
    const v0, 0x7f1301c9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    .line 332
    const v0, 0x7f1301cb

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraShutterBar;

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    .line 334
    const v0, 0x7f1301c6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/widget/VideoDurationView;

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    .line 335
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, v4}, Lcom/twitter/library/media/widget/VideoDurationView;->setDuration(I)V

    .line 337
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    const v3, 0x7f050013

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->x:Landroid/view/animation/Animation;

    .line 339
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    const v3, 0x7f050014

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->y:Landroid/view/animation/Animation;

    .line 341
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->x:Landroid/view/animation/Animation;

    new-instance v3, Lcom/twitter/android/media/camera/h$6;

    invoke-direct {v3, p0}, Lcom/twitter/android/media/camera/h$6;-><init>(Lcom/twitter/android/media/camera/h;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 347
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->y:Landroid/view/animation/Animation;

    new-instance v3, Lcom/twitter/android/media/camera/h$7;

    invoke-direct {v3, p0}, Lcom/twitter/android/media/camera/h$7;-><init>(Lcom/twitter/android/media/camera/h;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 354
    const v0, 0x7f1301ca

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    .line 356
    iget-object v0, v1, Lcom/twitter/android/media/widget/CameraToolbar;->a:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/android/media/camera/h$8;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/camera/h$8;-><init>(Lcom/twitter/android/media/camera/h;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    return-void
.end method

.method private A()Lcom/twitter/android/media/camera/VideoTextureView;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1229
    new-instance v0, Lcom/twitter/android/media/camera/VideoTextureView;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/android/media/camera/VideoTextureView;-><init>(Landroid/content/Context;)V

    .line 1230
    invoke-virtual {v0, p0}, Lcom/twitter/android/media/camera/VideoTextureView;->setPlaybackListener(Lcom/twitter/android/media/camera/VideoTextureView$a;)V

    .line 1231
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTextureView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1232
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTextureView;->setVisibility(I)V

    .line 1233
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->addView(Landroid/view/View;I)V

    .line 1234
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1235
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1236
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1237
    return-object v0
.end method

.method private B()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1241
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    const-string/jumbo v1, "off"

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Ljava/lang/CharSequence;)V

    .line 1242
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->h()V

    .line 1243
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->setVisibility(I)V

    .line 1244
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/CameraShutterBar;->setShutterButtonMode(I)V

    .line 1245
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/twitter/android/media/camera/h;->J:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1246
    invoke-virtual {p0, v3}, Lcom/twitter/android/media/camera/h;->b(Z)V

    .line 1247
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/VideoSegmentListView;->a(Z)V

    .line 1248
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->D()V

    .line 1249
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1250
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/h$a;->a(Z)V

    .line 1252
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1253
    const-string/jumbo v1, "command"

    const-string/jumbo v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1254
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1255
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    .line 1256
    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlashEnabled(Z)V

    .line 1257
    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlipCameraButtonEnabled(Z)V

    .line 1258
    const-string/jumbo v1, "off"

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlashToggleMode(Ljava/lang/CharSequence;)V

    .line 1259
    iput-boolean v3, p0, Lcom/twitter/android/media/camera/h;->V:Z

    .line 1260
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/camera/VideoTextureView;->setVisibility(I)V

    .line 1261
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->e()V

    .line 1262
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/camera/VideoTextureView;->setVisibility(I)V

    .line 1263
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->f()V

    .line 1264
    return-void
.end method

.method private C()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1267
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->d()V

    .line 1268
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/camera/VideoTextureView;->setVisibility(I)V

    .line 1269
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->d()V

    .line 1270
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/camera/VideoTextureView;->setVisibility(I)V

    .line 1271
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1272
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/media/camera/h;->R:I

    .line 1273
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->setVisibility(I)V

    .line 1274
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1275
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->g()V

    .line 1276
    iget v0, p0, Lcom/twitter/android/media/camera/h;->I:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/h;->b(Z)V

    .line 1277
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/VideoSegmentListView;->a(Z)V

    .line 1278
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->ad:Z

    if-nez v0, :cond_0

    .line 1279
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->E()V

    .line 1281
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    .line 1282
    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v3}, Lcom/twitter/android/media/camera/c;->m()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlashEnabled(Z)V

    .line 1283
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlipCameraButtonEnabled(Z)V

    .line 1284
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/h$a;->a(Z)V

    .line 1285
    return-void

    :cond_1
    move v0, v2

    .line 1276
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1282
    goto :goto_1
.end method

.method private D()V
    .locals 2

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1289
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1290
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    const/16 v1, 0xc8

    .line 1291
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1293
    :cond_0
    return-void
.end method

.method private E()V
    .locals 2

    .prologue
    .line 1296
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1297
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    const/16 v1, 0xc8

    .line 1298
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    .line 1300
    :cond_0
    return-void
.end method

.method private static F()Lcom/twitter/media/model/SegmentedVideoFile$a;
    .locals 1

    .prologue
    .line 1378
    invoke-static {}, Lcom/twitter/media/util/c;->b()Ljava/io/File;

    move-result-object v0

    .line 1379
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/media/model/SegmentedVideoFile$a;->a(Ljava/io/File;)Lcom/twitter/media/model/SegmentedVideoFile$a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/h;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/h;I)I
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lcom/twitter/android/media/camera/h;->M:I

    return p1
.end method

.method private a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 2

    .prologue
    .line 1372
    new-instance v0, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 1373
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(IIJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    .line 1372
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/h;IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/h;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V
    .locals 4

    .prologue
    .line 1360
    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;)V

    .line 1361
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v1}, Lcom/twitter/android/media/camera/b;->h()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 1362
    invoke-virtual {p2, p1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeSection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1363
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1364
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/h;Z)Z
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/twitter/android/media/camera/h;->Z:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/media/camera/h;I)I
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lcom/twitter/android/media/camera/h;->J:I

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/VideoSegmentEditView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/media/camera/h;Z)Z
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/twitter/android/media/camera/h;->ac:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/media/camera/h;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/twitter/android/media/camera/h;->R:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/media/camera/h;I)I
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lcom/twitter/android/media/camera/h;->Q:I

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/media/camera/h;Z)Z
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/twitter/android/media/camera/h;->K:Z

    return p1
.end method

.method static synthetic d(Lcom/twitter/android/media/camera/h;I)I
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lcom/twitter/android/media/camera/h;->U:I

    return p1
.end method

.method static synthetic d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/media/camera/h;Z)Z
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/twitter/android/media/camera/h;->X:Z

    return p1
.end method

.method static synthetic e(Lcom/twitter/android/media/camera/h;I)I
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lcom/twitter/android/media/camera/h;->R:I

    return p1
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 1157
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->x()I

    move-result v0

    if-le p1, v0, :cond_2

    const v0, 0x7f020099

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/VideoDurationView;->setBackgroundResource(I)V

    .line 1159
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/media/camera/h;->G:I

    if-le p1, v0, :cond_1

    :cond_0
    iget p1, p0, Lcom/twitter/android/media/camera/h;->G:I

    .line 1162
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/widget/VideoDurationView;->setDuration(I)V

    .line 1163
    return-void

    .line 1157
    :cond_2
    const v0, 0x7f020075

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/media/camera/h;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->V:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/media/camera/h;)Lcom/twitter/library/media/widget/VideoDurationView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/media/camera/h;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->O:Z

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/media/camera/h;)Lcom/twitter/media/model/SegmentedVideoFile$a;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/camera/h$a;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/media/camera/h;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/twitter/android/media/camera/h;->aa:I

    return v0
.end method

.method static synthetic k(Lcom/twitter/android/media/camera/h;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->ab:Z

    return v0
.end method

.method static synthetic l(Lcom/twitter/android/media/camera/h;)Landroid/view/View;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/HoverGarbageCanView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->q:Lcom/twitter/android/media/widget/HoverGarbageCanView;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/media/camera/h;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/twitter/android/media/camera/h;->J:I

    return v0
.end method

.method static synthetic o(Lcom/twitter/android/media/camera/h;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/media/camera/h;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->E()V

    return-void
.end method

.method static synthetic q(Lcom/twitter/android/media/camera/h;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->C()V

    return-void
.end method

.method static synthetic r(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/CameraShutterBar;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/media/camera/h;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/twitter/android/media/camera/h;->Q:I

    return v0
.end method

.method static synthetic t(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/VideoSegmentListView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/android/media/camera/h;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    return-void
.end method

.method private u()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 826
    iput-boolean v1, p0, Lcom/twitter/android/media/camera/h;->L:Z

    .line 827
    iget-boolean v2, p0, Lcom/twitter/android/media/camera/h;->K:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    .line 828
    invoke-virtual {v2}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    .line 829
    invoke-virtual {v2}, Lcom/twitter/android/media/camera/c;->p()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    .line 830
    invoke-virtual {v2}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->a()Z

    move-result v2

    if-nez v2, :cond_4

    .line 831
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    if-nez v2, :cond_0

    .line 832
    invoke-static {}, Lcom/twitter/android/media/camera/h;->F()Lcom/twitter/media/model/SegmentedVideoFile$a;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    .line 833
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    if-nez v2, :cond_0

    .line 863
    :goto_0
    return v0

    .line 838
    :cond_0
    iget v2, p0, Lcom/twitter/android/media/camera/h;->G:I

    iget v3, p0, Lcom/twitter/android/media/camera/h;->J:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/twitter/android/media/camera/h;->B:I

    .line 840
    iget v2, p0, Lcom/twitter/android/media/camera/h;->B:I

    if-gtz v2, :cond_1

    .line 841
    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    .line 842
    iget v1, p0, Lcom/twitter/android/media/camera/h;->G:I

    invoke-direct {p0, v1}, Lcom/twitter/android/media/camera/h;->e(I)V

    goto :goto_0

    .line 846
    :cond_1
    const-string/jumbo v2, "twitter_camera::video:shutter:click"

    invoke-virtual {p0, v2}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;)V

    .line 848
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v2

    .line 849
    if-eqz v2, :cond_2

    .line 850
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->C()V

    .line 852
    :cond_2
    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v3, v0}, Lcom/twitter/android/media/widget/CameraShutterBar;->setShutterButtonMode(I)V

    .line 853
    if-eqz v2, :cond_5

    .line 854
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->D:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/android/media/widget/CameraShutterBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 861
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    :cond_4
    move v0, v1

    .line 863
    goto :goto_0

    .line 856
    :cond_5
    iget v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    if-nez v0, :cond_3

    .line 859
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->k()V

    goto :goto_1
.end method

.method static synthetic v(Lcom/twitter/android/media/camera/h;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->z()V

    return-void
.end method

.method private v()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 867
    iput-boolean v2, p0, Lcom/twitter/android/media/camera/h;->L:Z

    .line 868
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraShutterBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 869
    iget v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    if-lez v0, :cond_0

    .line 870
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    .line 871
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->r()V

    .line 872
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/CameraShutterBar;->setShutterButtonMode(I)V

    .line 873
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0, v2}, Lcom/twitter/android/media/camera/b;->a(Z)V

    .line 874
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/CameraToolbar;->a(Z)V

    .line 875
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h$a;->notifyDataSetChanged()V

    .line 877
    :cond_0
    return v3
.end method

.method static synthetic w(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/camera/VideoTextureView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    return-object v0
.end method

.method private w()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 881
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v4

    .line 882
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/CameraToolbar;->a(Z)V

    .line 883
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0, v1}, Lcom/twitter/android/media/camera/b;->a(Z)V

    .line 884
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/VideoSegmentListView;->setEnabled(Z)V

    .line 885
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v3, v1

    .line 886
    :goto_0
    if-nez v3, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlipCameraButtonEnabled(Z)V

    .line 887
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->m()Ljava/lang/String;

    move-result-object v5

    .line 888
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v0

    if-nez v0, :cond_6

    if-eqz v5, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlashEnabled(Z)V

    .line 889
    if-eqz v5, :cond_0

    .line 890
    invoke-virtual {v4, v5}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlashToggleMode(Ljava/lang/CharSequence;)V

    .line 892
    :cond_0
    invoke-virtual {v4, v3}, Lcom/twitter/android/media/widget/CameraToolbar;->setDoneButtonEnabled(Z)V

    .line 893
    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    if-nez v3, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Lcom/twitter/android/media/widget/CameraShutterBar;->a(Z)V

    .line 894
    iget v0, p0, Lcom/twitter/android/media/camera/h;->J:I

    invoke-direct {p0, v0}, Lcom/twitter/android/media/camera/h;->e(I)V

    .line 895
    iget v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    const/4 v4, 0x3

    if-eq v0, v4, :cond_8

    move v0, v1

    .line 897
    :goto_4
    if-nez v0, :cond_1

    if-eqz v3, :cond_9

    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_1
    :goto_5
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/camera/h;->b(Z)V

    .line 899
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    if-nez v0, :cond_3

    .line 900
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->B()V

    .line 901
    invoke-virtual {p0, v2, v2}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    .line 902
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 903
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->d:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 905
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->s()V

    .line 906
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    .line 908
    :cond_3
    return-void

    :cond_4
    move v3, v2

    .line 885
    goto :goto_0

    :cond_5
    move v0, v2

    .line 886
    goto :goto_1

    :cond_6
    move v0, v2

    .line 888
    goto :goto_2

    :cond_7
    move v0, v2

    .line 893
    goto :goto_3

    :cond_8
    move v0, v2

    .line 895
    goto :goto_4

    :cond_9
    move v1, v2

    .line 897
    goto :goto_5
.end method

.method private x()I
    .locals 1

    .prologue
    .line 1153
    iget v0, p0, Lcom/twitter/android/media/camera/h;->G:I

    add-int/lit16 v0, v0, -0x2710

    return v0
.end method

.method static synthetic x(Lcom/twitter/android/media/camera/h;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/twitter/android/media/camera/h;->U:I

    return v0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 1166
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    .line 1167
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h$a;->notifyDataSetChanged()V

    .line 1168
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->a(I)V

    .line 1169
    return-void
.end method

.method private z()V
    .locals 3

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->k()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1207
    if-nez v1, :cond_1

    .line 1226
    :cond_0
    :goto_0
    return-void

    .line 1210
    :cond_1
    invoke-static {}, Lcom/twitter/android/util/b;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1211
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1212
    iget v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    if-nez v0, :cond_3

    const/4 v0, 0x4

    .line 1214
    :goto_1
    iput v0, p0, Lcom/twitter/android/media/camera/h;->ae:I

    .line 1221
    :cond_2
    :goto_2
    iget v0, p0, Lcom/twitter/android/media/camera/h;->ae:I

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1223
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    if-eqz v0, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    iget v1, p0, Lcom/twitter/android/media/camera/h;->ae:I

    invoke-virtual {v0, v1}, Lcom/twitter/media/model/SegmentedVideoFile$a;->a(I)Lcom/twitter/media/model/SegmentedVideoFile$a;

    goto :goto_0

    .line 1212
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    .line 1214
    invoke-static {v0}, Lcom/twitter/util/n;->c(Landroid/content/Context;)I

    move-result v0

    goto :goto_1

    .line 1217
    :cond_4
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/media/camera/h;->ae:I

    .line 1218
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/twitter/android/media/camera/b;->b(I)V

    goto :goto_2
.end method


# virtual methods
.method a(II)Lcom/twitter/android/media/camera/VideoTextureView;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1042
    iget v1, p0, Lcom/twitter/android/media/camera/h;->R:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/twitter/android/media/camera/h;->R:I

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/android/media/camera/h;->R:I

    if-eq v1, p1, :cond_0

    .line 1044
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    iget v2, p0, Lcom/twitter/android/media/camera/h;->R:I

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/media/camera/h$a;->b(II)V

    .line 1047
    :cond_0
    iput p1, p0, Lcom/twitter/android/media/camera/h;->R:I

    .line 1048
    iput p2, p0, Lcom/twitter/android/media/camera/h;->U:I

    move v1, v0

    move v2, v0

    .line 1051
    :goto_0
    if-ge v1, p1, :cond_1

    .line 1052
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    iget v0, v0, Lcom/twitter/media/model/VideoFile;->h:I

    add-int/2addr v2, v0

    .line 1051
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1054
    :cond_1
    iput v2, p0, Lcom/twitter/android/media/camera/h;->T:I

    .line 1055
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    add-int v1, v2, p2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1057
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/camera/h$a;->b(II)V

    .line 1058
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/h$a;->i(I)V

    .line 1062
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    .line 1063
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v1}, Lcom/twitter/android/media/camera/VideoTextureView;->getVideoFile()Lcom/twitter/media/model/VideoFile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/model/VideoFile;->a(Lcom/twitter/media/model/VideoFile;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1064
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 1065
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 1066
    iput-object v1, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 1067
    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    move-object v2, v1

    move-object v1, v0

    .line 1074
    :goto_1
    invoke-virtual {v1}, Lcom/twitter/android/media/camera/VideoTextureView;->d()V

    .line 1076
    add-int/lit8 v0, p1, 0x1

    .line 1077
    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 1078
    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/media/camera/VideoTextureView;->a(Lcom/twitter/media/model/VideoFile;Lcom/twitter/android/media/camera/VideoTextureView;)V

    .line 1083
    :goto_2
    invoke-virtual {v2, v1}, Lcom/twitter/android/media/camera/VideoTextureView;->a(Lcom/twitter/android/media/camera/VideoTextureView;)V

    .line 1085
    return-object v2

    .line 1069
    :cond_2
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 1070
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 1071
    invoke-virtual {v2, v0, p2}, Lcom/twitter/android/media/camera/VideoTextureView;->a(Lcom/twitter/media/model/VideoFile;I)V

    goto :goto_1

    .line 1080
    :cond_3
    invoke-virtual {v2, v4, v4}, Lcom/twitter/android/media/camera/VideoTextureView;->a(Lcom/twitter/media/model/VideoFile;Lcom/twitter/android/media/camera/VideoTextureView;)V

    goto :goto_2
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 430
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v2, v0}, Lcom/twitter/android/media/camera/c;->b(Z)V

    .line 432
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->z()V

    .line 434
    iput-boolean v1, p0, Lcom/twitter/android/media/camera/h;->ad:Z

    .line 435
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->A()Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 436
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->A()Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 439
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v2}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 440
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v2}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->b:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 443
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->q:Lcom/twitter/android/media/widget/HoverGarbageCanView;

    invoke-virtual {v2, v1}, Lcom/twitter/android/media/widget/HoverGarbageCanView;->setVisibility(I)V

    .line 444
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->q:Lcom/twitter/android/media/widget/HoverGarbageCanView;

    invoke-virtual {v2}, Lcom/twitter/android/media/widget/HoverGarbageCanView;->b()V

    .line 446
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v2, v1}, Lcom/twitter/android/media/widget/VideoSegmentListView;->setVisibility(I)V

    .line 447
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 448
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->setAdapter(Lcom/twitter/android/media/widget/VideoSegmentEditView$l;)V

    .line 449
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    invoke-virtual {v2, p0}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->setListItemClickListener(Lcom/twitter/android/media/widget/VideoSegmentEditView$f;)V

    .line 450
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    iget v3, p0, Lcom/twitter/android/media/camera/h;->S:I

    if-ltz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/android/media/widget/CameraShutterBar;->setShutterButtonMode(I)V

    .line 453
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    .line 454
    return-void

    :cond_1
    move v0, v1

    .line 450
    goto :goto_0
.end method

.method public a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1173
    iput-boolean v5, p0, Lcom/twitter/android/media/camera/h;->af:Z

    .line 1174
    iput p1, p0, Lcom/twitter/android/media/camera/h;->ag:I

    .line 1176
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    sget-object v1, Lcom/twitter/android/media/camera/h;->g:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    .line 1177
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    sget-object v1, Lcom/twitter/android/media/camera/h;->h:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    .line 1178
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    sget-object v1, Lcom/twitter/android/media/camera/h;->k:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    .line 1180
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    sget-object v1, Lcom/twitter/android/media/camera/h;->i:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    .line 1182
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1183
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    sget-object v2, Lcom/twitter/android/media/camera/h;->j:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v1, v2, p1}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v1

    const v2, 0x7f0e0024

    .line 1185
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0e0023

    .line 1188
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1184
    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1190
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    const v2, 0x7f0e0021

    .line 1191
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1190
    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/VideoSegmentListView;->setDividerWidth(I)V

    .line 1193
    if-nez p1, :cond_0

    .line 1194
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v4}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a(FZ)V

    .line 1195
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->setScaleMode(I)V

    .line 1196
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1197
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->setPostLayoutListener(Lcom/twitter/android/media/widget/VideoSegmentEditView$h;)V

    .line 1203
    :goto_0
    return-void

    .line 1199
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1200
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->setScaleMode(I)V

    .line 1201
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->setPostLayoutListener(Lcom/twitter/android/media/widget/VideoSegmentEditView$h;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 366
    const-string/jumbo v0, "video_segments"

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 367
    const-string/jumbo v0, "editing_prior_session"

    iget-boolean v1, p0, Lcom/twitter/android/media/camera/h;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 368
    const-string/jumbo v0, "editing_session_dirty"

    iget-boolean v1, p0, Lcom/twitter/android/media/camera/h;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 369
    const-string/jumbo v0, "active_segment"

    iget v1, p0, Lcom/twitter/android/media/camera/h;->S:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 370
    const-string/jumbo v0, "paused"

    iget-boolean v1, p0, Lcom/twitter/android/media/camera/h;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 371
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    invoke-virtual {v0}, Lcom/twitter/media/model/SegmentedVideoFile$a;->b()Ljava/io/File;

    move-result-object v0

    .line 373
    if-eqz v0, :cond_0

    .line 374
    const-string/jumbo v1, "session_directory"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :cond_0
    const-string/jumbo v0, "can_add_segment"

    iget-boolean v1, p0, Lcom/twitter/android/media/camera/h;->K:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 378
    const-string/jumbo v0, "orientation"

    iget v1, p0, Lcom/twitter/android/media/camera/h;->ae:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 379
    const-string/jumbo v0, "max_clip_length"

    iget v1, p0, Lcom/twitter/android/media/camera/h;->G:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 380
    const-string/jumbo v0, "min_clip_length"

    iget v1, p0, Lcom/twitter/android/media/camera/h;->H:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 381
    return-void
.end method

.method public a(Lcom/twitter/app/common/base/b;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 385
    .line 386
    if-eqz p2, :cond_3

    .line 387
    const-string/jumbo v0, "session_directory"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 388
    if-eqz v0, :cond_1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 389
    invoke-static {v2}, Lcom/twitter/media/model/SegmentedVideoFile$a;->a(Ljava/io/File;)Lcom/twitter/media/model/SegmentedVideoFile$a;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    .line 390
    const-string/jumbo v0, "video_segments"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 391
    const-string/jumbo v0, "editing_prior_session"

    .line 392
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->O:Z

    .line 393
    const-string/jumbo v0, "editing_session_dirty"

    .line 394
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->X:Z

    .line 395
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 396
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 402
    :cond_0
    const-string/jumbo v0, "can_add_segment"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    .line 403
    const-string/jumbo v0, "orientation"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/h;->ae:I

    .line 404
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    iget v2, p0, Lcom/twitter/android/media/camera/h;->ae:I

    invoke-virtual {v0, v2}, Lcom/twitter/media/model/SegmentedVideoFile$a;->a(I)Lcom/twitter/media/model/SegmentedVideoFile$a;

    .line 405
    const-string/jumbo v0, "active_segment"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/h;->S:I

    .line 406
    const-string/jumbo v0, "paused"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->W:Z

    .line 407
    const-string/jumbo v0, "max_clip_length"

    .line 408
    invoke-static {}, Lcom/twitter/media/util/d;->b()I

    move-result v2

    .line 407
    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/h;->G:I

    .line 409
    const-string/jumbo v0, "min_clip_length"

    .line 410
    invoke-static {}, Lcom/twitter/media/util/d;->c()I

    move-result v2

    .line 409
    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/h;->H:I

    .line 425
    :goto_1
    iput v1, p0, Lcom/twitter/android/media/camera/h;->J:I

    .line 426
    return-void

    .line 389
    :cond_1
    invoke-static {}, Lcom/twitter/android/media/camera/h;->F()Lcom/twitter/media/model/SegmentedVideoFile$a;

    move-result-object v0

    goto :goto_0

    .line 398
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    .line 399
    iget v0, v0, Lcom/twitter/media/model/VideoFile;->h:I

    add-int/2addr v0, v1

    move v1, v0

    .line 400
    goto :goto_2

    .line 412
    :cond_3
    invoke-static {}, Lcom/twitter/android/media/camera/h;->F()Lcom/twitter/media/model/SegmentedVideoFile$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    .line 413
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    .line 416
    const-string/jumbo v0, "seg_video_uri"

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    instance-of v0, v0, Landroid/net/Uri;

    if-eqz v0, :cond_4

    .line 419
    iput v1, p0, Lcom/twitter/android/media/camera/h;->S:I

    .line 422
    :cond_4
    invoke-static {}, Lcom/twitter/media/util/d;->b()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/h;->G:I

    .line 423
    invoke-static {}, Lcom/twitter/media/util/d;->c()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/h;->H:I

    goto :goto_1
.end method

.method public a(Lcom/twitter/media/model/SegmentedVideoFile;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 671
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 673
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->O:Z

    .line 674
    iput-boolean v1, p0, Lcom/twitter/android/media/camera/h;->X:Z

    .line 675
    iget v0, p1, Lcom/twitter/media/model/SegmentedVideoFile;->k:I

    iput v0, p0, Lcom/twitter/android/media/camera/h;->ae:I

    .line 677
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 678
    iput v1, p0, Lcom/twitter/android/media/camera/h;->J:I

    .line 679
    invoke-static {p1}, Lcom/twitter/media/model/SegmentedVideoFile$a;->a(Lcom/twitter/media/model/SegmentedVideoFile;)Lcom/twitter/media/model/SegmentedVideoFile$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    .line 680
    iget-object v0, p1, Lcom/twitter/media/model/SegmentedVideoFile;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    .line 681
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 682
    iget v2, p0, Lcom/twitter/android/media/camera/h;->J:I

    iget v0, v0, Lcom/twitter/media/model/VideoFile;->h:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/twitter/android/media/camera/h;->J:I

    goto :goto_0

    .line 685
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    .line 686
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->z()V

    .line 687
    return-void
.end method

.method public a(Lcom/twitter/media/model/VideoFile;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 712
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->m()V

    .line 715
    iget v2, p1, Lcom/twitter/media/model/VideoFile;->h:I

    iget v3, p0, Lcom/twitter/android/media/camera/h;->H:I

    if-ge v2, v3, :cond_2

    iget v2, p0, Lcom/twitter/android/media/camera/h;->B:I

    iget v3, p0, Lcom/twitter/android/media/camera/h;->H:I

    if-lt v2, v3, :cond_2

    .line 717
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->a:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 720
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/media/model/VideoFile;->c()Lrx/g;

    .line 721
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    .line 722
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->y()V

    .line 723
    const-string/jumbo v0, "twitter_camera::video:segment:deny"

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 724
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v2, p1, Lcom/twitter/media/model/VideoFile;->h:I

    int-to-long v2, v2

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v1

    .line 723
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    .line 781
    :cond_1
    :goto_0
    return-void

    .line 728
    :cond_2
    iput v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    .line 729
    iget v2, p0, Lcom/twitter/android/media/camera/h;->J:I

    iget v3, p1, Lcom/twitter/media/model/VideoFile;->h:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/twitter/android/media/camera/h;->J:I

    .line 730
    iget v2, p0, Lcom/twitter/android/media/camera/h;->G:I

    iget v3, p0, Lcom/twitter/android/media/camera/h;->J:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/twitter/android/media/camera/h;->B:I

    .line 732
    iget v2, p0, Lcom/twitter/android/media/camera/h;->B:I

    const/16 v3, 0x190

    if-lt v2, v3, :cond_6

    .line 733
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v2}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 734
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v2}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->c:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 740
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 741
    iput-boolean v1, p0, Lcom/twitter/android/media/camera/h;->X:Z

    .line 743
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 744
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/camera/h$a;->h(I)Lcom/twitter/android/media/widget/VideoSegmentListItemView;

    move-result-object v2

    .line 745
    if-eqz v2, :cond_7

    .line 746
    invoke-virtual {v2, p1}, Lcom/twitter/android/media/widget/VideoSegmentListItemView;->setVideoFile(Lcom/twitter/media/model/VideoFile;)V

    .line 747
    invoke-virtual {v2, v0}, Lcom/twitter/android/media/widget/VideoSegmentListItemView;->setStatus(I)V

    .line 748
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v2}, Lcom/twitter/android/media/widget/VideoSegmentListView;->a()V

    .line 753
    :goto_2
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    .line 757
    const-string/jumbo v2, "twitter_camera::video:segment:create"

    invoke-virtual {p0, v2}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;)V

    .line 758
    const-string/jumbo v2, "twitter_camera::video:segment:create"

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 759
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iget v4, p1, Lcom/twitter/media/model/VideoFile;->h:I

    int-to-long v4, v4

    invoke-direct {p0, v3, v4, v5}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v3

    iget v4, p0, Lcom/twitter/android/media/camera/h;->ag:I

    if-nez v4, :cond_8

    .line 760
    :goto_3
    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->c(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    const-string/jumbo v1, "torch"

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    .line 763
    invoke-virtual {v3}, Lcom/twitter/android/media/camera/c;->m()Ljava/lang/String;

    move-result-object v3

    .line 762
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(Z)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    .line 758
    invoke-direct {p0, v2, v0}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    .line 765
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    if-nez v0, :cond_4

    .line 766
    const-string/jumbo v0, "twitter_camera::video:segment:limit_exceed"

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 767
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v2, p1, Lcom/twitter/media/model/VideoFile;->h:I

    int-to-long v2, v2

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v1

    .line 766
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    .line 771
    :cond_4
    invoke-static {}, Lcom/twitter/android/util/b;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 772
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->s()V

    .line 775
    :cond_5
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->L:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->k()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 779
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->k()V

    goto/16 :goto_0

    .line 737
    :cond_6
    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->K:Z

    goto/16 :goto_1

    .line 750
    :cond_7
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v2}, Lcom/twitter/android/media/camera/h$a;->notifyDataSetChanged()V

    goto :goto_2

    :cond_8
    move v0, v1

    .line 759
    goto :goto_3
.end method

.method a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1349
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v1}, Lcom/twitter/android/media/camera/b;->h()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 1350
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1351
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1352
    return-void
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 787
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->z()V

    .line 788
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->m()V

    .line 789
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->q()I

    move-result v0

    int-to-long v0, v0

    .line 790
    if-nez p1, :cond_1

    const-wide/16 v2, 0x64

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget v2, p0, Lcom/twitter/android/media/camera/h;->H:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 794
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->a:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 797
    :cond_0
    const-string/jumbo v0, "twitter_camera::video:segment:deny"

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 798
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v4, v5}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v1

    .line 797
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    .line 808
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    .line 809
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->y()V

    .line 810
    return-void

    .line 800
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->k()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 801
    if-eqz v0, :cond_2

    .line 802
    const v1, 0x7f0a0a1c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 803
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 805
    :cond_2
    const-string/jumbo v0, "twitter_camera::video:segment:failure"

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 806
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v4, v5}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v1

    .line 805
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    goto :goto_0
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 579
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x82

    if-ne v0, v1, :cond_1

    .line 580
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 581
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->u()Z

    move-result v0

    .line 586
    :goto_0
    return v0

    .line 582
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->L:Z

    if-eqz v0, :cond_1

    .line 583
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->v()Z

    move-result v0

    goto :goto_0

    .line 586
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 563
    invoke-static {p2}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 572
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 565
    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->u()Z

    move-result v0

    goto :goto_0

    .line 569
    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->v()Z

    move-result v0

    goto :goto_0

    .line 563
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 458
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->setPostLayoutListener(Lcom/twitter/android/media/widget/VideoSegmentEditView$h;)V

    .line 459
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->setAdapter(Lcom/twitter/android/media/widget/VideoSegmentEditView$l;)V

    .line 461
    iput v4, p0, Lcom/twitter/android/media/camera/h;->M:I

    .line 462
    iput-boolean v4, p0, Lcom/twitter/android/media/camera/h;->L:Z

    .line 463
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->ad:Z

    .line 464
    iget v0, p0, Lcom/twitter/android/media/camera/h;->R:I

    iput v0, p0, Lcom/twitter/android/media/camera/h;->S:I

    .line 465
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->V:Z

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->W:Z

    .line 467
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->s()V

    .line 469
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->C()V

    .line 471
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->removeView(Landroid/view/View;)V

    .line 472
    iput-object v3, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 473
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->removeView(Landroid/view/View;)V

    .line 474
    iput-object v3, p0, Lcom/twitter/android/media/camera/h;->A:Lcom/twitter/android/media/camera/VideoTextureView;

    .line 476
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->b()V

    .line 477
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->q:Lcom/twitter/android/media/widget/HoverGarbageCanView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/HoverGarbageCanView;->setVisibility(I)V

    .line 478
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0}, Lcom/twitter/library/media/widget/VideoDurationView;->clearAnimation()V

    .line 479
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/widget/VideoDurationView;->setVisibility(I)V

    .line 480
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->C:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/VideoDurationView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 481
    iput-boolean v4, p0, Lcom/twitter/android/media/camera/h;->Y:Z

    .line 482
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/VideoSegmentListView;->setVisibility(I)V

    .line 483
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 484
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 485
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->g()V

    .line 486
    return-void
.end method

.method b(Z)V
    .locals 2

    .prologue
    .line 1338
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->Y:Z

    if-eq v0, p1, :cond_0

    .line 1339
    iput-boolean p1, p0, Lcom/twitter/android/media/camera/h;->Y:Z

    .line 1340
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->x:Landroid/view/animation/Animation;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/VideoDurationView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1343
    :cond_0
    return-void

    .line 1340
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->y:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 543
    iget v0, p0, Lcom/twitter/android/media/camera/h;->S:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/media/camera/h;->S:I

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 544
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlashEnabled(Z)V

    .line 545
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->B()V

    .line 546
    iget v0, p0, Lcom/twitter/android/media/camera/h;->S:I

    .line 547
    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    .line 548
    iget-boolean v1, p0, Lcom/twitter/android/media/camera/h;->W:Z

    if-eqz v1, :cond_0

    .line 549
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->b()V

    .line 550
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 558
    :goto_0
    return-void

    .line 552
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    .line 553
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 556
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->C()V

    goto :goto_0
.end method

.method public c(I)V
    .locals 6

    .prologue
    .line 593
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 596
    :cond_1
    iget v0, p0, Lcom/twitter/android/media/camera/h;->R:I

    if-ne v0, p1, :cond_3

    .line 597
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->t()V

    .line 598
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "twitter_camera::video:segment:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->V:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "pause"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/media/camera/h;->R:I

    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    iget v3, p0, Lcom/twitter/android/media/camera/h;->R:I

    .line 601
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    iget v0, v0, Lcom/twitter/media/model/VideoFile;->h:I

    int-to-long v4, v0

    .line 600
    invoke-direct {p0, v2, v4, v5}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    .line 598
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "play"

    goto :goto_1

    .line 603
    :cond_3
    const-string/jumbo v0, "twitter_camera::video:segment:replay"

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;)V

    .line 604
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->B()V

    .line 605
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    .line 606
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 607
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->e:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 609
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->s()V

    goto :goto_0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 663
    iget v0, p0, Lcom/twitter/android/media/camera/h;->U:I

    if-lt p1, v0, :cond_0

    .line 664
    iput p1, p0, Lcom/twitter/android/media/camera/h;->U:I

    .line 665
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/twitter/android/media/camera/h;->T:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 667
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 491
    iget-boolean v2, p0, Lcom/twitter/android/media/camera/h;->Z:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 538
    :cond_0
    :goto_0
    return v0

    .line 496
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/twitter/android/media/camera/h;->O:Z

    if-eqz v2, :cond_0

    :cond_2
    iget-boolean v2, p0, Lcom/twitter/android/media/camera/h;->O:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/twitter/android/media/camera/h;->X:Z

    if-eqz v2, :cond_0

    .line 500
    :cond_3
    new-instance v2, Lcom/twitter/android/media/camera/h$9;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/camera/h$9;-><init>(Lcom/twitter/android/media/camera/h;)V

    .line 517
    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v3}, Lcom/twitter/android/media/camera/b;->k()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 518
    if-eqz v3, :cond_0

    .line 522
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->O:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0a002b

    .line 523
    :goto_1
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->O:Z

    if-eqz v0, :cond_5

    const v0, 0x7f0a0279

    .line 525
    :goto_2
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0a0278

    .line 527
    invoke-virtual {v0, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a00f6

    const/4 v3, 0x0

    .line 528
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 530
    new-instance v2, Lcom/twitter/android/media/camera/h$10;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/camera/h$10;-><init>(Lcom/twitter/android/media/camera/h;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 536
    iput-boolean v1, p0, Lcom/twitter/android/media/camera/h;->Z:Z

    .line 537
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    move v0, v1

    .line 538
    goto :goto_0

    .line 522
    :cond_4
    const v0, 0x7f0a027b

    goto :goto_1

    .line 523
    :cond_5
    const v0, 0x7f0a027a

    goto :goto_2
.end method

.method public g()V
    .locals 6

    .prologue
    .line 615
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/CameraShutterBar;->getTop()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 644
    :goto_0
    return-void

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/CameraShutterBar;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 620
    div-int/lit8 v1, v0, 0x2

    .line 621
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 622
    invoke-virtual {v2}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->getHeight()I

    move-result v2

    sub-int/2addr v2, v0

    const/high16 v3, 0x40000000    # 2.0f

    .line 621
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 623
    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    invoke-virtual {v3, v2, v2}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->measure(II)V

    .line 624
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 625
    invoke-virtual {v3}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->getLeft()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 626
    invoke-virtual {v4}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->getTop()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 627
    invoke-virtual {v5}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->getRight()I

    move-result v5

    sub-int v1, v5, v1

    iget-object v5, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 628
    invoke-virtual {v5}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->getBottom()I

    move-result v5

    sub-int/2addr v5, v0

    .line 624
    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->layout(IIII)V

    .line 629
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    .line 630
    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    .line 631
    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getTop()I

    move-result v3

    sub-int/2addr v3, v0

    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    .line 632
    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    .line 633
    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getBottom()I

    move-result v5

    sub-int/2addr v5, v0

    .line 629
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ProgressBar;->layout(IIII)V

    .line 634
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    .line 635
    invoke-virtual {v2}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    .line 636
    invoke-virtual {v3}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getTop()I

    move-result v3

    sub-int/2addr v3, v0

    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    .line 637
    invoke-virtual {v4}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    .line 638
    invoke-virtual {v5}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getBottom()I

    move-result v5

    sub-int/2addr v5, v0

    .line 634
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/media/widget/VideoSegmentListView;->layout(IIII)V

    .line 639
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    .line 640
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    .line 641
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int/2addr v3, v0

    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    .line 642
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/media/camera/h;->u:Landroid/view/View;

    .line 643
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    sub-int v0, v5, v0

    .line 639
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 648
    iget v0, p0, Lcom/twitter/android/media/camera/h;->R:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/media/camera/h;->R:I

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 649
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 650
    iget v1, p0, Lcom/twitter/android/media/camera/h;->R:I

    add-int/lit8 v1, v1, 0x1

    .line 651
    if-ge v1, v0, :cond_1

    .line 652
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    .line 659
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/media/camera/h;->R:I

    .line 655
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/twitter/android/media/camera/h;->J:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 656
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->r()V

    goto :goto_0
.end method

.method public i()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 692
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    .line 693
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->z()V

    .line 694
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 695
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/h$a;->h(I)Lcom/twitter/android/media/widget/VideoSegmentListItemView;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_0

    .line 697
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/VideoSegmentListItemView;->setStatus(I)V

    .line 701
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0, v2}, Lcom/twitter/android/media/camera/b;->a(Z)V

    .line 702
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraToolbar;->setControlsEnabled(Z)V

    .line 703
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/CameraToolbar;->b(Z)V

    .line 704
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->C:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 705
    return-void

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h$a;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 815
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->z()V

    .line 816
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->m()V

    .line 817
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 818
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->a:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->C:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/VideoDurationView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 821
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->w()V

    .line 822
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->y()V

    .line 823
    return-void
.end method

.method k()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 911
    iget v0, p0, Lcom/twitter/android/media/camera/h;->M:I

    if-eqz v0, :cond_0

    .line 948
    :goto_0
    return-void

    .line 915
    :cond_0
    iput v2, p0, Lcom/twitter/android/media/camera/h;->M:I

    .line 916
    invoke-direct {p0}, Lcom/twitter/android/media/camera/h;->z()V

    .line 917
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->v:Lcom/twitter/android/media/camera/h$a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h$a;->notifyDataSetChanged()V

    .line 918
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->l()V

    .line 920
    sget-boolean v0, Lcom/twitter/android/media/camera/h;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 921
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    invoke-virtual {v3}, Lcom/twitter/media/model/SegmentedVideoFile$a;->c()Ljava/io/File;

    move-result-object v3

    iget v4, p0, Lcom/twitter/android/media/camera/h;->B:I

    invoke-virtual {v0, v3, v4, p0}, Lcom/twitter/android/media/camera/c;->a(Ljava/io/File;ILcom/twitter/android/media/camera/i$d;)V

    .line 923
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 924
    new-instance v3, Lcom/twitter/android/media/camera/h$11;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/media/camera/h$11;-><init>(Lcom/twitter/android/media/camera/h;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 933
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraShutterBar;->a(Z)V

    .line 934
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/VideoSegmentListView;->setEnabled(Z)V

    .line 935
    invoke-virtual {p0, v2}, Lcom/twitter/android/media/camera/h;->b(Z)V

    .line 936
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/CameraToolbar;->b(Z)V

    .line 940
    const-string/jumbo v3, "twitter_camera::video:segment:attempt"

    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    .line 941
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-wide/16 v4, 0x0

    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/android/media/camera/h;->a(IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v4

    iget v0, p0, Lcom/twitter/android/media/camera/h;->ag:I

    if-nez v0, :cond_2

    move v0, v1

    .line 942
    :goto_1
    invoke-virtual {v4, v0}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->c(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    const-string/jumbo v4, "torch"

    iget-object v5, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    .line 945
    invoke-virtual {v5}, Lcom/twitter/android/media/camera/c;->m()Ljava/lang/String;

    move-result-object v5

    .line 944
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(Z)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    .line 946
    invoke-virtual {v4}, Lcom/twitter/android/media/camera/c;->j()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->d(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    .line 940
    invoke-direct {p0, v3, v0}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 941
    goto :goto_1

    :cond_3
    move v2, v1

    .line 946
    goto :goto_2
.end method

.method l()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 952
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->F:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 953
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->ac:Z

    if-eqz v0, :cond_1

    .line 981
    :cond_0
    :goto_0
    return-void

    .line 957
    :cond_1
    iput-boolean v3, p0, Lcom/twitter/android/media/camera/h;->ac:Z

    .line 958
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 959
    invoke-virtual {v0, v2, v3, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 961
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/media/camera/h;->aa:I

    .line 962
    iget v1, p0, Lcom/twitter/android/media/camera/h;->aa:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 964
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 968
    :cond_2
    :goto_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_4

    .line 969
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/media/camera/h;->ab:Z

    .line 970
    iget-boolean v1, p0, Lcom/twitter/android/media/camera/h;->ab:Z

    if-nez v1, :cond_3

    .line 971
    const/16 v1, -0x64

    invoke-virtual {v0, v3, v1, v4}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 977
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->d()Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    .line 978
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    iget-boolean v0, v0, Landroid/hardware/Camera$CameraInfo;->canDisableShutterSound:Z

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, v4}, Lcom/twitter/android/media/camera/c;->c(Z)V

    goto :goto_0

    .line 974
    :cond_4
    invoke-virtual {v0, v3, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    goto :goto_2

    .line 965
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method m()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 985
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->ac:Z

    if-nez v0, :cond_0

    .line 1012
    :goto_0
    return-void

    .line 988
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->F:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 990
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 993
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->F:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/android/media/camera/h$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/media/camera/h$2;-><init>(Lcom/twitter/android/media/camera/h;Landroid/media/AudioManager;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method n()V
    .locals 3

    .prologue
    .line 1016
    iget v0, p0, Lcom/twitter/android/media/camera/h;->J:I

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v1}, Lcom/twitter/android/media/camera/c;->q()I

    move-result v1

    add-int/2addr v0, v1

    .line 1017
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v1}, Lcom/twitter/android/media/camera/c;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1018
    div-int/lit16 v1, v0, 0x3e8

    .line 1019
    iget v2, p0, Lcom/twitter/android/media/camera/h;->I:I

    if-eq v1, v2, :cond_0

    .line 1020
    iput v1, p0, Lcom/twitter/android/media/camera/h;->I:I

    .line 1021
    invoke-direct {p0, v0}, Lcom/twitter/android/media/camera/h;->e(I)V

    .line 1023
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->s:Lcom/twitter/library/media/widget/VideoDurationView;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->C:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1025
    :cond_1
    return-void
.end method

.method o()V
    .locals 3

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1039
    :goto_0
    return-void

    .line 1031
    :cond_0
    sget-boolean v0, Lcom/twitter/android/media/camera/h;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1032
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    .line 1033
    iget-object v2, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    invoke-virtual {v2, v0}, Lcom/twitter/media/model/SegmentedVideoFile$a;->a(Lcom/twitter/media/model/VideoFile;)Lcom/twitter/media/model/SegmentedVideoFile$a;

    goto :goto_1

    .line 1035
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->P:Lcom/twitter/media/model/SegmentedVideoFile$a;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->c:Lcom/twitter/android/media/camera/c;

    .line 1036
    invoke-virtual {v1}, Lcom/twitter/android/media/camera/c;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/model/SegmentedVideoFile$a;->b(I)Lcom/twitter/media/model/SegmentedVideoFile$a;

    move-result-object v0

    .line 1037
    invoke-virtual {v0}, Lcom/twitter/media/model/SegmentedVideoFile$a;->a()Lcom/twitter/media/model/SegmentedVideoFile;

    move-result-object v0

    .line 1038
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    sget-object v2, Lcom/twitter/media/model/MediaType;->f:Lcom/twitter/media/model/MediaType;

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/media/camera/b;->a(Lcom/twitter/media/model/MediaType;Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0
.end method

.method p()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1089
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->af:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 1139
    :goto_0
    return v0

    .line 1093
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1095
    iget v0, p0, Lcom/twitter/android/media/camera/h;->ag:I

    if-nez v0, :cond_5

    .line 1096
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    move v0, v1

    .line 1099
    goto :goto_0

    .line 1101
    :cond_1
    sget v0, Lcom/twitter/android/media/camera/h;->m:I

    if-lez v0, :cond_2

    .line 1102
    sget v0, Lcom/twitter/android/media/camera/h;->m:I

    goto :goto_0

    .line 1105
    :cond_2
    const v0, 0x7f0e0022

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1107
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    .line 1108
    invoke-virtual {v0}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1109
    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->r:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v4}, Lcom/twitter/android/media/widget/CameraShutterBar;->getTop()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/media/camera/h;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getBottom()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v4, v5

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v4, v0

    sub-int/2addr v0, v3

    .line 1112
    iget-object v3, p0, Lcom/twitter/android/media/camera/h;->n:Lcom/twitter/android/media/widget/VideoSegmentEditView;

    invoke-virtual {v3}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/media/camera/h;->t:Lcom/twitter/android/media/widget/VideoSegmentListView;

    .line 1113
    invoke-virtual {v4}, Lcom/twitter/android/media/widget/VideoSegmentListView;->getDividerWidth()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x3

    .line 1114
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1116
    sget-object v4, Lcom/twitter/android/media/camera/h;->f:[I

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v1, v5, :cond_3

    aget v0, v4, v1

    .line 1117
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1118
    if-gt v0, v3, :cond_4

    .line 1123
    :cond_3
    sput v0, Lcom/twitter/android/media/camera/h;->m:I

    .line 1124
    const-class v1, Lcom/twitter/android/media/camera/h;

    invoke-static {v1}, Lcru;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 1116
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1126
    :cond_5
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_6

    move v0, v1

    .line 1129
    goto :goto_0

    .line 1131
    :cond_6
    sget v0, Lcom/twitter/android/media/camera/h;->l:I

    if-gtz v0, :cond_7

    .line 1133
    const v0, 0x7f0e0578

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/twitter/android/media/camera/h;->l:I

    .line 1134
    const-class v0, Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 1136
    :cond_7
    sget v0, Lcom/twitter/android/media/camera/h;->l:I

    goto/16 :goto_0
.end method

.method q()Z
    .locals 1

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->o:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method r()V
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1304
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    const v1, 0x7f05001e

    .line 1305
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1306
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1307
    return-void
.end method

.method s()V
    .locals 2

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1323
    :goto_0
    return-void

    .line 1314
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->b:Landroid/content/Context;

    const v1, 0x7f05001f

    .line 1315
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1316
    new-instance v1, Lcom/twitter/android/media/camera/h$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/camera/h$3;-><init>(Lcom/twitter/android/media/camera/h;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1322
    iget-object v1, p0, Lcom/twitter/android/media/camera/h;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method t()V
    .locals 1

    .prologue
    .line 1326
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/h;->V:Z

    if-eqz v0, :cond_0

    .line 1327
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->V:Z

    .line 1328
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    .line 1329
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->s()V

    .line 1335
    :goto_0
    return-void

    .line 1331
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/h;->V:Z

    .line 1332
    iget-object v0, p0, Lcom/twitter/android/media/camera/h;->z:Lcom/twitter/android/media/camera/VideoTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->c()V

    .line 1333
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/h;->r()V

    goto :goto_0
.end method
