.class Lcom/twitter/android/media/camera/g$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/CameraSpeedBumpBar$a;
.implements Lcom/twitter/media/ui/image/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final a:Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

.field final synthetic b:Lcom/twitter/android/media/camera/g;

.field private final c:Lcom/twitter/media/ui/image/MediaImageView;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private f:Lcom/twitter/media/model/ImageFile;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/g;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 302
    iput-object p1, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    const v0, 0x7f1301c4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    .line 304
    const v0, 0x7f1301c3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/g$b;->d:Landroid/view/View;

    .line 305
    const v0, 0x7f1301cb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/g$b;->e:Landroid/view/View;

    .line 306
    const v0, 0x7f1301cc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

    iput-object v0, p0, Lcom/twitter/android/media/camera/g$b;->a:Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

    .line 307
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->a:Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

    invoke-virtual {v0, p0}, Lcom/twitter/android/media/widget/CameraSpeedBumpBar;->setListener(Lcom/twitter/android/media/widget/CameraSpeedBumpBar$a;)V

    .line 308
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 426
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/g$b;->f()V

    .line 427
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    invoke-virtual {v0}, Lcom/twitter/media/model/ImageFile;->c()Lrx/g;

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    .line 431
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 312
    packed-switch p1, :pswitch_data_0

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 314
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    const-string/jumbo v1, "retake"

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/g;->a(Lcom/twitter/android/media/camera/g;Ljava/lang/String;)V

    .line 315
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/g$b;->c()V

    goto :goto_0

    .line 319
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    const-string/jumbo v1, "use"

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/g;->a(Lcom/twitter/android/media/camera/g;Ljava/lang/String;)V

    .line 321
    new-instance v0, Lcom/twitter/android/media/camera/g$a;

    iget-object v1, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v1, v1, Lcom/twitter/android/media/camera/g;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    invoke-direct {v0, v1, v2, p0}, Lcom/twitter/android/media/camera/g$a;-><init>(Landroid/content/Context;Lcom/twitter/media/model/ImageFile;Lcom/twitter/android/media/camera/g$b;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/g$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 323
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    goto :goto_0

    .line 328
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    const-string/jumbo v1, "close"

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/g;->a(Lcom/twitter/android/media/camera/g;Ljava/lang/String;)V

    .line 329
    invoke-direct {p0}, Lcom/twitter/android/media/camera/g$b;->g()V

    .line 330
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->j()V

    goto :goto_0

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_0

    .line 387
    const-string/jumbo v0, "preview_image"

    iget-object v1, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 389
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/app/common/base/b;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 392
    if-eqz p2, :cond_0

    const-string/jumbo v0, "preview_image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    const-string/jumbo v0, "preview_image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    iput-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    .line 395
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/media/model/ImageFile;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    .line 345
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 348
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->e:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    invoke-static {v0}, Lcom/twitter/android/media/camera/g;->a(Lcom/twitter/android/media/camera/g;)Lcom/twitter/android/media/widget/CameraPreviewContainer;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    invoke-static {v0}, Lcom/twitter/android/media/camera/g;->b(Lcom/twitter/android/media/camera/g;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraToolbar;->b(Z)V

    .line 355
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v4}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->a:Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

    invoke-virtual {v0, v4}, Lcom/twitter/android/media/widget/CameraSpeedBumpBar;->setVisibility(I)V

    .line 357
    invoke-static {}, Lcom/twitter/android/util/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    .line 359
    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 358
    invoke-virtual {v0, v1, v4}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/request/a$a;Z)Z

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->i()I

    move-result v0

    .line 362
    iget-object v1, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v2, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    .line 364
    invoke-static {v2}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/request/a$a;

    move-result-object v2

    .line 365
    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/media/request/a$a;->a(I)Lcom/twitter/media/request/a$a;

    move-result-object v2

    .line 362
    invoke-virtual {v1, v2, v4}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/request/a$a;Z)Z

    .line 367
    iget-object v1, p0, Lcom/twitter/android/media/camera/g$b;->a:Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/CameraSpeedBumpBar;->a(I)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 400
    invoke-static {}, Lcom/twitter/android/util/b;->d()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 402
    const-string/jumbo v0, "speed_bump"

    invoke-static {p1, v0}, Lcom/twitter/android/util/b;->a(ILjava/lang/String;)V

    .line 407
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v2, p0, Lcom/twitter/android/media/camera/g$b;->f:Lcom/twitter/media/model/ImageFile;

    .line 409
    invoke-static {v2}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/request/a$a;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/twitter/media/request/a$a;->a(I)Lcom/twitter/media/request/a$a;

    move-result-object v2

    .line 408
    invoke-virtual {v0, v2, v1}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/request/a$a;Z)Z

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->a:Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/CameraSpeedBumpBar;->a(I)V

    .line 413
    return-void

    :cond_1
    move v0, v1

    .line 400
    goto :goto_0
.end method

.method b(Lcom/twitter/media/model/ImageFile;)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-interface {v0, v1, p1}, Lcom/twitter/android/media/camera/b;->a(Lcom/twitter/media/model/MediaType;Lcom/twitter/media/model/MediaFile;)V

    .line 444
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 373
    invoke-direct {p0}, Lcom/twitter/android/media/camera/g$b;->g()V

    .line 374
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 375
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->a:Lcom/twitter/android/media/widget/CameraSpeedBumpBar;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/CameraSpeedBumpBar;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 378
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    invoke-static {v0}, Lcom/twitter/android/media/camera/g;->a(Lcom/twitter/android/media/camera/g;)Lcom/twitter/android/media/widget/CameraPreviewContainer;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->setVisibility(I)V

    .line 379
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/CameraToolbar;->a(Z)V

    .line 380
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    invoke-static {v0}, Lcom/twitter/android/media/camera/g;->b(Lcom/twitter/android/media/camera/g;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0, v3}, Lcom/twitter/android/media/camera/b;->a(Z)V

    .line 382
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->c:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->g()V

    .line 383
    return-void
.end method

.method d()V
    .locals 3

    .prologue
    .line 434
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->b:Landroid/content/Context;

    .line 435
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v1, v1, Lcom/twitter/android/media/camera/g;->b:Landroid/content/Context;

    const v2, 0x7f0a07b5

    .line 436
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 434
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 438
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 439
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->b:Lcom/twitter/android/media/camera/g;

    iget-object v0, v0, Lcom/twitter/android/media/camera/g;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->j()V

    .line 440
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->e()V

    .line 418
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/twitter/android/media/camera/g$b;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->f()V

    .line 423
    return-void
.end method
