.class public Lcom/twitter/android/media/camera/CameraFragment;
.super Lcom/twitter/app/common/base/BaseFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/camera/b;
.implements Lcom/twitter/media/ui/image/a;
.implements Lcom/twitter/util/n$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/camera/CameraFragment$a;,
        Lcom/twitter/android/media/camera/CameraFragment$d;,
        Lcom/twitter/android/media/camera/CameraFragment$c;,
        Lcom/twitter/android/media/camera/CameraFragment$b;
    }
.end annotation


# static fields
.field static final synthetic i:Z

.field private static final j:[[Lcom/twitter/android/media/camera/f;

.field private static final k:[[Lcom/twitter/android/media/camera/f;


# instance fields
.field private A:I

.field private B:Landroid/view/Display;

.field private C:Lcom/twitter/util/n;

.field a:Lcom/twitter/android/media/camera/c;

.field b:Lcom/twitter/android/media/camera/CameraFragment$a;

.field c:Landroid/view/ViewGroup;

.field d:Lcom/twitter/android/media/widget/CameraShutterBar;

.field e:Landroid/view/View;

.field f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

.field g:Landroid/view/View;

.field h:Landroid/widget/ProgressBar;

.field private final l:Lcom/twitter/android/media/camera/c$a;

.field private m:I

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Landroid/view/animation/Animation;

.field private t:Landroid/view/animation/Animation;

.field private u:Lcom/twitter/android/media/widget/CameraToolbar;

.field private v:I

.field private w:Lcom/twitter/android/media/camera/g;

.field private x:Lcom/twitter/android/media/camera/h;

.field private y:Lcom/twitter/android/media/camera/a;

.field private z:Lcom/twitter/android/media/camera/VideoTooltipManager;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const v7, 0x7f1301bf

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    const-class v0, Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/media/camera/CameraFragment;->i:Z

    .line 74
    new-array v0, v8, [[Lcom/twitter/android/media/camera/f;

    new-array v3, v6, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/4 v5, 0x5

    invoke-direct {v4, v5, v7}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/4 v5, 0x7

    invoke-direct {v4, v5, v7}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v1

    aput-object v3, v0, v2

    new-array v3, v6, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v2, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/4 v5, 0x5

    invoke-direct {v4, v5, v7}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v1

    aput-object v3, v0, v1

    new-array v3, v6, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const v5, 0x7f1301cb

    invoke-direct {v4, v1, v5}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/4 v5, 0x7

    invoke-direct {v4, v5, v7}, Lcom/twitter/android/media/camera/f;-><init>(II)V

    aput-object v4, v3, v1

    aput-object v3, v0, v6

    sput-object v0, Lcom/twitter/android/media/camera/CameraFragment;->j:[[Lcom/twitter/android/media/camera/f;

    .line 92
    new-array v0, v8, [[Lcom/twitter/android/media/camera/f;

    new-array v3, v6, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0x9

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    aput-object v3, v0, v2

    new-array v3, v6, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0xa

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v1

    aput-object v3, v0, v1

    new-array v3, v6, [Lcom/twitter/android/media/camera/f;

    new-instance v4, Lcom/twitter/android/media/camera/f;

    const/16 v5, 0x9

    invoke-direct {v4, v5}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v4, v3, v2

    new-instance v2, Lcom/twitter/android/media/camera/f;

    const/16 v4, 0xa

    invoke-direct {v2, v4}, Lcom/twitter/android/media/camera/f;-><init>(I)V

    aput-object v2, v3, v1

    aput-object v3, v0, v6

    sput-object v0, Lcom/twitter/android/media/camera/CameraFragment;->k:[[Lcom/twitter/android/media/camera/f;

    return-void

    :cond_0
    move v0, v2

    .line 59
    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragment;-><init>()V

    .line 121
    new-instance v0, Lcom/twitter/android/media/camera/CameraFragment$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/media/camera/CameraFragment$b;-><init>(Lcom/twitter/android/media/camera/CameraFragment;Lcom/twitter/android/media/camera/CameraFragment$1;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->l:Lcom/twitter/android/media/camera/c$a;

    .line 132
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->v:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/CameraFragment;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/twitter/android/media/camera/CameraFragment;->A:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/a;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    return-object v0
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 481
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/widget/CameraShutterBar;->a(IZ)V

    .line 482
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_1

    .line 483
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    iget v0, v0, Lcom/twitter/android/media/camera/a;->a:I

    if-ne v0, p1, :cond_0

    .line 503
    :goto_0
    return-void

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->b()V

    .line 488
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 489
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->w:Lcom/twitter/android/media/camera/g;

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    .line 495
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->a()V

    .line 496
    iput p1, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    .line 497
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->o()V

    .line 498
    if-eqz p2, :cond_4

    .line 499
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->c:Landroid/view/ViewGroup;

    new-instance v1, Lcom/twitter/android/media/camera/d;

    iget-object v2, p0, Lcom/twitter/android/media/camera/CameraFragment;->c:Landroid/view/ViewGroup;

    invoke-direct {v1, v2}, Lcom/twitter/android/media/camera/d;-><init>(Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 490
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 491
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->x:Lcom/twitter/android/media/camera/h;

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    goto :goto_1

    .line 493
    :cond_3
    const-string/jumbo v0, "Invalid camera mode"

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 501
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/CameraFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/CameraFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private static a(ILandroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 461
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/media/camera/CameraFragment;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->A:I

    return v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 631
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "twitter_camera::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 632
    invoke-direct {p0}, Lcom/twitter/android/media/camera/CameraFragment;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":click"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 633
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 634
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/media/camera/CameraFragment;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/media/camera/CameraFragment;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    return v0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->f(I)V

    .line 447
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 448
    invoke-static {v0, p1}, Lcom/twitter/util/n;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->o:I

    .line 449
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    iget v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->o:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraShutterBar;->a(I)V

    .line 450
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    iget v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->o:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/CameraToolbar;->a(I)V

    .line 451
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    iget v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->o:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/a;->b(I)V

    .line 454
    :cond_1
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->o:I

    .line 456
    invoke-direct {p0}, Lcom/twitter/android/media/camera/CameraFragment;->p()Ljava/lang/String;

    move-result-object v1

    .line 454
    invoke-static {v0, v1}, Lcom/twitter/android/util/b;->a(ILjava/lang/String;)V

    .line 458
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/widget/CameraToolbar;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/media/camera/CameraFragment;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->t:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/g;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->w:Lcom/twitter/android/media/camera/g;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/media/camera/CameraFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/android/media/camera/CameraFragment;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/h;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->x:Lcom/twitter/android/media/camera/h;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 598
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-nez v0, :cond_0

    .line 599
    const-string/jumbo v0, ""

    .line 601
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->w:Lcom/twitter/android/media/camera/g;

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "photo"

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "video"

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 171
    const v0, 0x7f040057

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->c:Landroid/view/ViewGroup;

    .line 172
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method a(II)V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    .line 508
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 509
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 510
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p2, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 511
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 512
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 513
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 514
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->s:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 516
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/media/camera/CameraFragment$a;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lcom/twitter/android/media/camera/CameraFragment;->b:Lcom/twitter/android/media/camera/CameraFragment$a;

    .line 406
    return-void
.end method

.method public a(Lcom/twitter/media/model/MediaType;Lcom/twitter/media/model/MediaFile;)V
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->b:Lcom/twitter/android/media/camera/CameraFragment$a;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->b:Lcom/twitter/android/media/camera/CameraFragment$a;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/media/camera/CameraFragment$a;->a(Lcom/twitter/media/model/MediaType;Lcom/twitter/media/model/MediaFile;)V

    .line 546
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/CameraToolbar;->setControlsEnabled(Z)V

    .line 538
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/CameraShutterBar;->setEnabled(Z)V

    .line 539
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 417
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 418
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 438
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v1, p1}, Lcom/twitter/android/media/camera/a;->a(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return v0

    .line 420
    :sswitch_0
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    iget-object v2, p0, Lcom/twitter/android/media/camera/CameraFragment;->x:Lcom/twitter/android/media/camera/h;

    if-eq v1, v2, :cond_0

    .line 421
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/camera/CameraFragment;->c(I)V

    goto :goto_0

    .line 427
    :sswitch_1
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    iget-object v2, p0, Lcom/twitter/android/media/camera/CameraFragment;->w:Lcom/twitter/android/media/camera/g;

    if-eq v1, v2, :cond_0

    .line 428
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/CameraFragment;->c(I)V

    goto :goto_0

    .line 438
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 418
    :sswitch_data_0
    .sparse-switch
        0x1b -> :sswitch_1
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 326
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->C:Lcom/twitter/util/n;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->C:Lcom/twitter/util/n;

    invoke-virtual {v0}, Lcom/twitter/util/n;->a()V

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 331
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    invoke-static {v1, v0}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 332
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/camera/CameraFragment;->a(IZ)V

    .line 333
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->v:I

    if-ltz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    iget v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->v:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->c(I)V

    .line 343
    :cond_1
    :goto_0
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->b()V

    .line 344
    return-void

    .line 337
    :cond_2
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    if-ne v0, v5, :cond_3

    const-string/jumbo v0, "photo"

    .line 338
    :goto_1
    iget v2, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "twitter_camera:::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/media/camera/CameraFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 337
    :cond_3
    const-string/jumbo v0, "video"

    goto :goto_1
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 550
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/CameraFragment;->d(I)V

    .line 551
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->p:Z

    .line 552
    return-void
.end method

.method c(I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 465
    if-ne p1, v4, :cond_0

    .line 466
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 467
    invoke-static {v0, v4}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 468
    new-instance v1, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    const v2, 0x7f0a0a0f

    .line 469
    invoke-virtual {p0, v2}, Lcom/twitter/android/media/camera/CameraFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 471
    invoke-static {v4}, Lcom/twitter/android/media/camera/e;->a(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v0, "twitter_camera::video:"

    .line 472
    invoke-virtual {v1, v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 473
    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/media/camera/CameraFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 478
    :goto_0
    return-void

    .line 477
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/media/camera/CameraFragment;->a(IZ)V

    goto :goto_0
.end method

.method public d()Lcom/twitter/android/media/widget/CameraToolbar;
    .locals 1

    .prologue
    .line 531
    sget-boolean v0, Lcom/twitter/android/media/camera/CameraFragment;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->e()V

    .line 395
    :cond_0
    return-void
.end method

.method public e_(I)V
    .locals 1

    .prologue
    .line 410
    iput p1, p0, Lcom/twitter/android/media/camera/CameraFragment;->n:I

    .line 411
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->p:Z

    if-nez v0, :cond_0

    .line 412
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/CameraFragment;->d(I)V

    .line 414
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->f()V

    .line 402
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 556
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->p:Z

    if-eqz v0, :cond_0

    .line 557
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->n:I

    invoke-direct {p0, v0}, Lcom/twitter/android/media/camera/CameraFragment;->d(I)V

    .line 559
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->p:Z

    .line 560
    return-void
.end method

.method public h()Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 565
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 570
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->o:I

    return v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->b()V

    .line 577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->b:Lcom/twitter/android/media/camera/CameraFragment$a;

    if-eqz v0, :cond_1

    .line 580
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->b:Lcom/twitter/android/media/camera/CameraFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/CameraFragment$a;->a()V

    .line 582
    :cond_1
    return-void
.end method

.method public k()Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 587
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    goto :goto_0
.end method

.method public l()Lcom/twitter/android/media/camera/VideoTooltipManager;
    .locals 1

    .prologue
    .line 593
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->z:Lcom/twitter/android/media/camera/VideoTooltipManager;

    goto :goto_0
.end method

.method m()I
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->B:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5a

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 611
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 612
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_2

    .line 613
    const/4 v0, 0x0

    .line 619
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    sget-object v2, Lcom/twitter/android/media/camera/CameraFragment;->k:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v1, v2, v0}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    .line 620
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    sget-object v2, Lcom/twitter/android/media/camera/CameraFragment;->j:[[Lcom/twitter/android/media/camera/f;

    invoke-static {v1, v2, v0}, Lcom/twitter/android/media/camera/f;->a(Landroid/view/View;[[Lcom/twitter/android/media/camera/f;I)Landroid/widget/RelativeLayout$LayoutParams;

    .line 622
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v1, :cond_1

    .line 623
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/camera/a;->a(I)V

    .line 625
    :cond_1
    return-void

    .line 615
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->m()I

    move-result v1

    const/16 v2, 0xb4

    if-lt v1, v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 279
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 281
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    .line 282
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 283
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 284
    new-instance v3, Lcom/twitter/android/media/camera/g;

    iget-object v4, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-direct {v3, v2, v4, p0}, Lcom/twitter/android/media/camera/g;-><init>(Landroid/content/Context;Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/b;)V

    iput-object v3, p0, Lcom/twitter/android/media/camera/CameraFragment;->w:Lcom/twitter/android/media/camera/g;

    .line 285
    iget-object v3, p0, Lcom/twitter/android/media/camera/CameraFragment;->w:Lcom/twitter/android/media/camera/g;

    invoke-virtual {v3, v0, p1}, Lcom/twitter/android/media/camera/g;->a(Lcom/twitter/app/common/base/b;Landroid/os/Bundle;)V

    .line 286
    new-instance v3, Lcom/twitter/android/media/camera/h;

    iget-object v4, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-direct {v3, v2, v4, p0}, Lcom/twitter/android/media/camera/h;-><init>(Landroid/content/Context;Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/b;)V

    iput-object v3, p0, Lcom/twitter/android/media/camera/CameraFragment;->x:Lcom/twitter/android/media/camera/h;

    .line 287
    iget-object v3, p0, Lcom/twitter/android/media/camera/CameraFragment;->x:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v3, v0, p1}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/app/common/base/b;Landroid/os/Bundle;)V

    .line 289
    const-string/jumbo v3, "initiator"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;I)I

    move-result v0

    .line 290
    new-instance v3, Lcom/twitter/android/media/camera/VideoTooltipManager;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 291
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0}, Lcom/twitter/android/media/camera/VideoTooltipManager;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;I)V

    iput-object v3, p0, Lcom/twitter/android/media/camera/CameraFragment;->z:Lcom/twitter/android/media/camera/VideoTooltipManager;

    .line 292
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->B:Landroid/view/Display;

    .line 293
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->m()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->A:I

    .line 294
    invoke-static {}, Lcom/twitter/android/util/b;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    new-instance v0, Lcom/twitter/util/n;

    invoke-direct {v0, v2}, Lcom/twitter/util/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->C:Lcom/twitter/util/n;

    .line 296
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->C:Lcom/twitter/util/n;

    invoke-virtual {v0, p0}, Lcom/twitter/util/n;->a(Lcom/twitter/util/n$a;)V

    .line 297
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->A:I

    invoke-direct {p0, v0}, Lcom/twitter/android/media/camera/CameraFragment;->d(I)V

    .line 299
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 371
    packed-switch p1, :pswitch_data_0

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 373
    :pswitch_0
    invoke-static {p2, p3}, Lcom/twitter/android/media/camera/CameraFragment;->a(ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    goto :goto_0

    .line 379
    :pswitch_1
    invoke-static {p2, p3}, Lcom/twitter/android/media/camera/CameraFragment;->a(ILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->j()V

    goto :goto_0

    .line 371
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 317
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 318
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->m()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->A:I

    .line 319
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    iget v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->A:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->b(I)V

    .line 320
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->o()V

    .line 321
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 322
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 144
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 146
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/CameraFragment;->setRetainInstance(Z)V

    .line 148
    if-eqz p1, :cond_0

    .line 149
    const-string/jumbo v0, "camera_video_mode_available"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->q:Z

    .line 151
    const-string/jumbo v0, "camera_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    .line 157
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 159
    const v1, 0x7f050015

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->s:Landroid/view/animation/Animation;

    .line 160
    const v1, 0x7f050016

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->t:Landroid/view/animation/Animation;

    .line 161
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->t:Landroid/view/animation/Animation;

    new-instance v1, Lcom/twitter/android/media/camera/CameraFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/camera/CameraFragment$1;-><init>(Lcom/twitter/android/media/camera/CameraFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 167
    return-void

    .line 153
    :cond_0
    invoke-static {}, Lcom/twitter/android/util/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 154
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v2

    const-string/jumbo v3, "allow_video"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->q:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->r:Z

    .line 364
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->b:Lcom/twitter/android/media/camera/CameraFragment$a;

    .line 365
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->l:Lcom/twitter/android/media/camera/c$a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->b(Lcom/twitter/android/media/camera/c$a;)V

    .line 366
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->onDestroy()V

    .line 367
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 303
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 305
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    iget v0, v0, Lcom/twitter/android/media/camera/a;->a:I

    .line 307
    :goto_0
    const-string/jumbo v1, "camera_mode"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 308
    const-string/jumbo v0, "camera_video_mode_available"

    iget-boolean v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 310
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->w:Lcom/twitter/android/media/camera/g;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/g;->a(Landroid/os/Bundle;)V

    .line 311
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->x:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/h;->a(Landroid/os/Bundle;)V

    .line 312
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->a(Landroid/os/Bundle;)V

    .line 313
    return-void

    .line 305
    :cond_0
    iget v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 177
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 179
    const v0, 0x7f1301c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->e:Landroid/view/View;

    .line 181
    const v0, 0x7f1301c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraToolbar;

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    .line 182
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->u:Lcom/twitter/android/media/widget/CameraToolbar;

    new-instance v5, Lcom/twitter/android/media/camera/CameraFragment$c;

    invoke-direct {v5, p0, v1}, Lcom/twitter/android/media/camera/CameraFragment$c;-><init>(Lcom/twitter/android/media/camera/CameraFragment;Lcom/twitter/android/media/camera/CameraFragment$1;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraToolbar;->setOnCameraToolbarClickListener(Lcom/twitter/android/media/widget/CameraToolbar$a;)V

    .line 184
    const v0, 0x7f1301cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraShutterBar;

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    .line 185
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    new-instance v5, Lcom/twitter/android/media/camera/CameraFragment$2;

    invoke-direct {v5, p0}, Lcom/twitter/android/media/camera/CameraFragment$2;-><init>(Lcom/twitter/android/media/camera/CameraFragment;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraShutterBar;->setCameraShutterBarListener(Lcom/twitter/android/media/widget/CameraShutterBar$a;)V

    .line 192
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraShutterBar;->a:Lcom/twitter/android/media/widget/CameraModeButton;

    new-instance v5, Lcom/twitter/android/media/camera/CameraFragment$3;

    invoke-direct {v5, p0}, Lcom/twitter/android/media/camera/CameraFragment$3;-><init>(Lcom/twitter/android/media/camera/CameraFragment;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraModeButton;->setListener(Lcom/twitter/android/media/widget/CameraModeButton$a;)V

    .line 200
    const v0, 0x7f130064

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->h:Landroid/widget/ProgressBar;

    .line 202
    const v0, 0x7f1301bf

    .line 203
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    .line 204
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    const v5, 0x7f1301c1

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    .line 205
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    new-instance v5, Lcom/twitter/android/media/camera/CameraFragment$4;

    invoke-direct {v5, p0}, Lcom/twitter/android/media/camera/CameraFragment$4;-><init>(Lcom/twitter/android/media/camera/CameraFragment;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraPreviewContainer;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 215
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v5

    .line 218
    if-nez p2, :cond_6

    .line 219
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->q:Z

    if-eqz v0, :cond_2

    .line 220
    const-string/jumbo v0, "seg_video_uri"

    invoke-virtual {v5, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 221
    if-eqz v0, :cond_0

    move v1, v2

    .line 223
    :goto_0
    iput v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    .line 227
    :goto_1
    const-string/jumbo v1, "front_facing"

    invoke-virtual {v5, v1, v4}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    move v7, v1

    move-object v1, v0

    move v0, v7

    .line 230
    :goto_2
    iget-object v5, p0, Lcom/twitter/android/media/camera/CameraFragment;->d:Lcom/twitter/android/media/widget/CameraShutterBar;

    iget-boolean v6, p0, Lcom/twitter/android/media/camera/CameraFragment;->q:Z

    invoke-virtual {v5, v6}, Lcom/twitter/android/media/widget/CameraShutterBar;->setVideoModeAvailable(Z)V

    .line 232
    invoke-virtual {p0, v4}, Lcom/twitter/android/media/camera/CameraFragment;->a(Z)V

    .line 234
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/android/media/camera/c;->a(Landroid/content/Context;)Lcom/twitter/android/media/camera/c;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    .line 235
    iget-object v5, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    iget-object v6, p0, Lcom/twitter/android/media/camera/CameraFragment;->l:Lcom/twitter/android/media/camera/c$a;

    invoke-virtual {v5, v6}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c$a;)V

    .line 236
    iget-object v5, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v5, p2}, Lcom/twitter/android/media/camera/c;->b(Landroid/os/Bundle;)V

    .line 237
    iget-object v5, p0, Lcom/twitter/android/media/camera/CameraFragment;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 239
    iget-object v5, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    iget v6, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    if-ne v6, v2, :cond_3

    move v2, v3

    :goto_3
    invoke-virtual {v5, v2}, Lcom/twitter/android/media/camera/c;->b(Z)V

    .line 242
    const/4 v2, -0x1

    iput v2, p0, Lcom/twitter/android/media/camera/CameraFragment;->v:I

    .line 244
    if-eqz v1, :cond_4

    .line 245
    new-instance v0, Lcom/twitter/android/media/camera/CameraFragment$d;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/media/camera/CameraFragment$d;-><init>(Lcom/twitter/android/media/camera/CameraFragment;Landroid/net/Uri;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/CameraFragment$d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 250
    :goto_4
    invoke-static {}, Lcom/twitter/android/util/b;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/camera/CameraFragment$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/camera/CameraFragment$5;-><init>(Lcom/twitter/android/media/camera/CameraFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 275
    :goto_5
    return-void

    .line 221
    :cond_0
    const-string/jumbo v1, "start_mode"

    .line 223
    invoke-virtual {v5, v1}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "start_mode"

    invoke-virtual {v5, v1}, Lcom/twitter/app/common/base/b;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_0

    .line 225
    :cond_2
    iput v3, p0, Lcom/twitter/android/media/camera/CameraFragment;->m:I

    move-object v0, v1

    goto :goto_1

    :cond_3
    move v2, v4

    .line 239
    goto :goto_3

    .line 247
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/camera/c;->a(Z)V

    goto :goto_4

    .line 273
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto :goto_5

    :cond_6
    move v0, v4

    goto :goto_2
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->C:Lcom/twitter/util/n;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->C:Lcom/twitter/util/n;

    invoke-virtual {v0}, Lcom/twitter/util/n;->b()V

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->b()V

    .line 353
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->y:Lcom/twitter/android/media/camera/a;

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->e()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/CameraFragment;->v:I

    .line 358
    :cond_2
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->q_()V

    .line 359
    return-void
.end method
