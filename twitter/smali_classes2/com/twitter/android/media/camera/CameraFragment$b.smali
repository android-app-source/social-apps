.class Lcom/twitter/android/media/camera/CameraFragment$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/camera/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/CameraFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/CameraFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/media/camera/CameraFragment;)V
    .locals 0

    .prologue
    .line 636
    iput-object p1, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/media/camera/CameraFragment;Lcom/twitter/android/media/camera/CameraFragment$1;)V
    .locals 0

    .prologue
    .line 636
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/CameraFragment$b;-><init>(Lcom/twitter/android/media/camera/CameraFragment;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 658
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 660
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    const v3, 0x7f0a0619

    .line 661
    invoke-virtual {v2, v3}, Lcom/twitter/android/media/camera/CameraFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 660
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 661
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 662
    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 663
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 665
    :cond_0
    return-void
.end method

.method public a(Landroid/hardware/Camera;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 641
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->h:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 642
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/CameraFragment;->a(Z)V

    .line 643
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->f:Lcom/twitter/android/media/widget/CameraPreviewContainer;

    iget-object v0, v0, Lcom/twitter/android/media/widget/CameraPreviewContainer;->a:Lcom/twitter/android/media/camera/CameraPreviewTextureView;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a()V

    .line 644
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->a(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->a(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->c()V

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->c(Lcom/twitter/android/media/camera/CameraFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->d(Lcom/twitter/android/media/camera/CameraFragment;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 649
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraFragment;->l()Lcom/twitter/android/media/camera/VideoTooltipManager;

    move-result-object v0

    .line 650
    if-eqz v0, :cond_1

    .line 651
    sget-object v1, Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;->f:Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/VideoTooltipManager;->a(Lcom/twitter/android/media/camera/VideoTooltipManager$CameraTooltip;)V

    .line 654
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/media/model/ImageFile;)V
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->g(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/g;->a(Lcom/twitter/media/model/ImageFile;)V

    .line 699
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 675
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    const v2, 0x7f0a0961

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/camera/CameraFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 676
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 677
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->e(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/widget/CameraToolbar;->a(Ljava/lang/CharSequence;)V

    .line 678
    return-void
.end method

.method public a(Ljava/util/Set;Ljava/lang/CharSequence;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 670
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->e(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/widget/CameraToolbar;->a(Ljava/util/Set;Ljava/lang/CharSequence;)V

    .line 671
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 683
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v1}, Lcom/twitter/android/media/camera/CameraFragment;->f(Lcom/twitter/android/media/camera/CameraFragment;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 684
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 688
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    const v2, 0x7f0a0956

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/camera/CameraFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 689
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 690
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$b;->a:Lcom/twitter/android/media/camera/CameraFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/CameraFragment;->a(Z)V

    .line 692
    return-void
.end method
