.class Lcom/twitter/android/media/camera/c$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/camera/c;->b(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/android/media/camera/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/c;I)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/twitter/android/media/camera/c$4;->b:Lcom/twitter/android/media/camera/c;

    iput p2, p0, Lcom/twitter/android/media/camera/c$4;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$4;->b:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->c(Lcom/twitter/android/media/camera/c;)Landroid/hardware/Camera;

    move-result-object v1

    .line 211
    if-eqz v1, :cond_0

    .line 212
    monitor-enter v1

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$4;->b:Lcom/twitter/android/media/camera/c;

    iget v2, p0, Lcom/twitter/android/media/camera/c$4;->a:I

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/c;->e(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 220
    :cond_0
    return-void

    .line 215
    :catch_0
    move-exception v0

    .line 216
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
