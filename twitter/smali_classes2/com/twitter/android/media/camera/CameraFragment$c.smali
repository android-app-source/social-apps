.class Lcom/twitter/android/media/camera/CameraFragment$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/CameraToolbar$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/CameraFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/CameraFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/media/camera/CameraFragment;)V
    .locals 0

    .prologue
    .line 702
    iput-object p1, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/media/camera/CameraFragment;Lcom/twitter/android/media/camera/CameraFragment$1;)V
    .locals 0

    .prologue
    .line 702
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/CameraFragment$c;-><init>(Lcom/twitter/android/media/camera/CameraFragment;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 705
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    const-string/jumbo v1, "cancel"

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/CameraFragment;->a(Lcom/twitter/android/media/camera/CameraFragment;Ljava/lang/String;)V

    .line 706
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->a(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-static {v0}, Lcom/twitter/android/media/camera/CameraFragment;->a(Lcom/twitter/android/media/camera/CameraFragment;)Lcom/twitter/android/media/camera/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/a;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 707
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraFragment;->j()V

    .line 709
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    .line 724
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "twitter_camera::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    .line 725
    invoke-static {v4}, Lcom/twitter/android/media/camera/CameraFragment;->h(Lcom/twitter/android/media/camera/CameraFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":flash:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 726
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 728
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->a(Ljava/lang/CharSequence;)V

    .line 729
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 713
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    const-string/jumbo v1, "flip_camera"

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/CameraFragment;->a(Lcom/twitter/android/media/camera/CameraFragment;Ljava/lang/String;)V

    .line 714
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/CameraFragment;->a(Z)V

    .line 715
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 716
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 717
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraFragment$c;->a:Lcom/twitter/android/media/camera/CameraFragment;

    iget-object v0, v0, Lcom/twitter/android/media/camera/CameraFragment;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->b()V

    .line 718
    return-void
.end method
