.class public Lcom/twitter/android/media/camera/CameraPreviewTextureView;
.super Landroid/view/TextureView;
.source "Twttr"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private final a:Lcom/twitter/android/media/camera/c;

.field private b:Landroid/hardware/Camera$Size;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/media/camera/c;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {p0, p0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 24
    iput-object p2, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a:Lcom/twitter/android/media/camera/c;

    .line 25
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 53
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->a()Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 54
    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->b:Landroid/hardware/Camera$Size;

    .line 56
    if-nez v0, :cond_1

    .line 91
    :goto_0
    return-void

    .line 60
    :cond_0
    iput-object v0, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->b:Landroid/hardware/Camera$Size;

    .line 65
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a:Lcom/twitter/android/media/camera/c;

    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/n;->a(Landroid/content/Context;)I

    move-result v3

    .line 65
    invoke-virtual {v1, v3}, Lcom/twitter/android/media/camera/c;->e(I)I

    move-result v1

    .line 67
    if-eqz v1, :cond_2

    const/16 v3, 0xb4

    if-ne v1, v3, :cond_3

    .line 68
    :cond_2
    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-float v1, v1

    .line 69
    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-float v0, v0

    .line 75
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    .line 76
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 78
    div-float/2addr v1, v3

    .line 79
    div-float/2addr v0, v4

    .line 80
    cmpl-float v5, v1, v0

    if-ltz v5, :cond_4

    .line 81
    div-float v0, v1, v0

    move v7, v2

    move v2, v0

    move v0, v7

    .line 88
    :goto_2
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 89
    div-float/2addr v3, v6

    div-float/2addr v4, v6

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 90
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->setTransform(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 71
    :cond_3
    iget v1, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-float v1, v1

    .line 72
    iget v0, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-float v0, v0

    goto :goto_1

    .line 84
    :cond_4
    div-float/2addr v0, v1

    .line 85
    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->b()V

    .line 50
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c;->d(I)V

    .line 30
    invoke-direct {p0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->b()V

    .line 31
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->a(Landroid/graphics/SurfaceTexture;)V

    .line 32
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/camera/c;->b(Landroid/graphics/SurfaceTexture;)V

    .line 42
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/android/media/camera/CameraPreviewTextureView;->b()V

    .line 37
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method
