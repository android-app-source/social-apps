.class Lcom/twitter/android/media/camera/c$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/camera/c;->n()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/c;)V
    .locals 0

    .prologue
    .line 666
    iput-object p1, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 669
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->d(Lcom/twitter/android/media/camera/c;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 693
    :cond_0
    :goto_0
    return-void

    .line 673
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->e(Lcom/twitter/android/media/camera/c;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v0

    .line 674
    const-string/jumbo v1, "continuous-picture"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "continuous-video"

    .line 675
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 676
    :cond_2
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    const-string/jumbo v1, "auto"

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 677
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    iput v2, v0, Lcom/twitter/android/media/camera/c;->a:I

    .line 678
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/c;->n()Z

    .line 692
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    iput v2, v0, Lcom/twitter/android/media/camera/c;->a:I

    goto :goto_0

    .line 681
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->c(Lcom/twitter/android/media/camera/c;)Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 682
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 683
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/media/camera/c$a;->b()V

    goto :goto_1

    .line 687
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$a;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 688
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->a(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/media/camera/c$a;->b()V

    .line 690
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$5;->a:Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->f(Lcom/twitter/android/media/camera/c;)V

    goto :goto_1
.end method
