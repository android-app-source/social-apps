.class Lcom/twitter/android/media/camera/h$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/h;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/h;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 264
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->b(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/VideoSegmentEditView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/VideoSegmentEditView;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->c(Lcom/twitter/android/media/camera/h;)I

    move-result v0

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->c(Lcom/twitter/android/media/camera/h;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v1}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 268
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->t()V

    .line 269
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "twitter_camera::video:preview:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    .line 270
    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->e(Lcom/twitter/android/media/camera/h;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "pause"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 269
    invoke-virtual {v1, v0}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_2
    const-string/jumbo v0, "play"

    goto :goto_1

    .line 271
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    const-string/jumbo v1, "twitter_camera::video:preview:replay"

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/h;->a(Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0, v2, v2}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    .line 274
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$5;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->s()V

    goto :goto_0
.end method
