.class Lcom/twitter/android/media/camera/CameraActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/camera/CameraFragment$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/camera/CameraActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/CameraActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/CameraActivity;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/twitter/android/media/camera/CameraActivity$1;->a:Lcom/twitter/android/media/camera/CameraActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraActivity$1;->a:Lcom/twitter/android/media/camera/CameraActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/CameraActivity;->setResult(I)V

    .line 120
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraActivity$1;->a:Lcom/twitter/android/media/camera/CameraActivity;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraActivity;->finish()V

    .line 121
    return-void
.end method

.method public a(Lcom/twitter/media/model/MediaType;Lcom/twitter/media/model/MediaFile;)V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraActivity$1;->a:Lcom/twitter/android/media/camera/CameraActivity;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/twitter/android/media/camera/CameraActivity$1;->a:Lcom/twitter/android/media/camera/CameraActivity;

    invoke-virtual {v2}, Lcom/twitter/android/media/camera/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "media_type"

    .line 112
    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "media_file"

    .line 113
    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    .line 111
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/camera/CameraActivity;->setResult(ILandroid/content/Intent;)V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/media/camera/CameraActivity$1;->a:Lcom/twitter/android/media/camera/CameraActivity;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/CameraActivity;->finish()V

    .line 115
    return-void
.end method
