.class Lcom/twitter/android/media/camera/i$c;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/media/model/VideoFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/i;

.field private final b:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/media/camera/i;Z)V
    .locals 2

    .prologue
    .line 332
    iput-object p1, p0, Lcom/twitter/android/media/camera/i$c;->a:Lcom/twitter/android/media/camera/i;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 333
    if-nez p2, :cond_0

    .line 334
    invoke-virtual {p1}, Lcom/twitter/android/media/camera/i;->d()I

    move-result v0

    const/16 v1, 0x7d0

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/media/camera/i$c;->b:Z

    .line 335
    return-void

    .line 334
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/media/model/VideoFile;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$c;->a:Lcom/twitter/android/media/camera/i;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/i;->e()Lcom/twitter/media/model/VideoFile;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/media/model/VideoFile;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 344
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$c;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    if-nez v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 347
    :cond_0
    if-nez p1, :cond_2

    .line 348
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/i$c;->b:Z

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$c;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/media/camera/i$d;->j()V

    .line 356
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$c;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;Z)Z

    goto :goto_0

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$c;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/twitter/android/media/camera/i$d;->a(Z)V

    goto :goto_1

    .line 354
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/i$c;->a:Lcom/twitter/android/media/camera/i;

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i;)Lcom/twitter/android/media/camera/i$d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/media/camera/i$d;->a(Lcom/twitter/media/model/VideoFile;)V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 328
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/i$c;->a([Ljava/lang/Void;)Lcom/twitter/media/model/VideoFile;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 328
    check-cast p1, Lcom/twitter/media/model/VideoFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/i$c;->a(Lcom/twitter/media/model/VideoFile;)V

    return-void
.end method
