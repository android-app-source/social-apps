.class public Lcom/twitter/android/media/camera/e;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string/jumbo v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/android/media/camera/e;->a:[Ljava/lang/String;

    .line 23
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string/jumbo v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    const-string/jumbo v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/android/media/camera/e;->b:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 74
    const-string/jumbo v0, "extra_permissions"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_1

    const-string/jumbo v1, "android.permission.CAMERA"

    .line 76
    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    const-string/jumbo v1, "android.permission.RECORD_AUDIO"

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x2

    .line 85
    :goto_0
    return v0

    .line 82
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;ILjava/lang/String;Z)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 57
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 58
    :cond_0
    const v1, 0x7f0a0a11

    .line 59
    const v0, 0x7f0a0a10

    .line 64
    :goto_0
    new-instance v2, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 65
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/twitter/android/media/camera/e;->a(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, p0, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->e(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 67
    invoke-virtual {v0, p2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0, p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a(Z)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 64
    return-object v0

    .line 61
    :cond_1
    const v1, 0x7f0a0690

    .line 62
    const v0, 0x7f0a068f

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;I)Z
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    .line 28
    invoke-static {p1}, Lcom/twitter/android/media/camera/e;->a(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(I)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    packed-switch p0, :pswitch_data_0

    .line 41
    sget-object v0, Lcom/twitter/android/media/camera/e;->a:[Ljava/lang/String;

    :goto_0
    return-object v0

    .line 36
    :pswitch_0
    sget-object v0, Lcom/twitter/android/media/camera/e;->b:[Ljava/lang/String;

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
