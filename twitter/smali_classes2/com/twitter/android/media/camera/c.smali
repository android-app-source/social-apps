.class public Lcom/twitter/android/media/camera/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/camera/c$a;,
        Lcom/twitter/android/media/camera/c$b;,
        Lcom/twitter/android/media/camera/c$c;
    }
.end annotation


# static fields
.field private static final b:Landroid/hardware/Camera$ShutterCallback;

.field private static c:Lcom/twitter/android/media/camera/c;


# instance fields
.field a:I

.field private final d:Landroid/content/Context;

.field private final e:I

.field private f:Lcom/twitter/android/media/camera/c$a;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Landroid/hardware/Camera;

.field private l:Landroid/graphics/SurfaceTexture;

.field private m:Z

.field private n:I

.field private o:Landroid/media/MediaRecorder;

.field private p:Z

.field private q:Z

.field private r:Landroid/hardware/Camera$Parameters;

.field private s:I

.field private t:I

.field private u:Lcom/twitter/android/media/camera/c$c;

.field private v:Lcom/twitter/android/media/camera/i;

.field private final w:Landroid/hardware/Camera$PreviewCallback;

.field private final x:Landroid/hardware/Camera$PictureCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/android/media/camera/c$1;

    invoke-direct {v0}, Lcom/twitter/android/media/camera/c$1;-><init>()V

    sput-object v0, Lcom/twitter/android/media/camera/c;->b:Landroid/hardware/Camera$ShutterCallback;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput v0, p0, Lcom/twitter/android/media/camera/c;->a:I

    .line 113
    iput v0, p0, Lcom/twitter/android/media/camera/c;->n:I

    .line 123
    new-instance v0, Lcom/twitter/android/media/camera/c$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/camera/c$2;-><init>(Lcom/twitter/android/media/camera/c;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->w:Landroid/hardware/Camera$PreviewCallback;

    .line 132
    new-instance v0, Lcom/twitter/android/media/camera/c$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/camera/c$3;-><init>(Lcom/twitter/android/media/camera/c;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->x:Landroid/hardware/Camera$PictureCallback;

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->d:Landroid/content/Context;

    .line 145
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/c;->e:I

    .line 146
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/c;I)I
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Lcom/twitter/android/media/camera/c;->n:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$a;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/c;Lcom/twitter/android/media/camera/c$c;)Lcom/twitter/android/media/camera/c$c;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/media/camera/c;
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/twitter/android/media/camera/c;->c:Lcom/twitter/android/media/camera/c;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lcom/twitter/android/media/camera/c;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/camera/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/media/camera/c;->c:Lcom/twitter/android/media/camera/c;

    .line 157
    const-class v0, Lcom/twitter/android/media/camera/c;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 159
    :cond_0
    sget-object v0, Lcom/twitter/android/media/camera/c;->c:Lcom/twitter/android/media/camera/c;

    return-object v0
.end method

.method private a(Landroid/hardware/Camera;Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 492
    :try_start_0
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501
    iget v0, p0, Lcom/twitter/android/media/camera/c;->t:I

    if-lez v0, :cond_0

    .line 502
    iget v0, p0, Lcom/twitter/android/media/camera/c;->t:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/c;->d(I)V

    .line 504
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/c;->m:Z

    if-eqz v0, :cond_1

    .line 505
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->g()V

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 508
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/c;->a(Ljava/lang/CharSequence;)V

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    if-eqz v0, :cond_3

    .line 511
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/media/camera/c$a;->a(Landroid/hardware/Camera;)V

    .line 512
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->l()V

    .line 514
    :cond_3
    :goto_0
    return-void

    .line 493
    :catch_0
    move-exception v0

    .line 494
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->f()V

    .line 495
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    if-eqz v1, :cond_4

    .line 496
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    invoke-interface {v1}, Lcom/twitter/android/media/camera/c$a;->a()V

    .line 498
    :cond_4
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Landroid/media/MediaRecorder;)V
    .locals 2

    .prologue
    .line 844
    if-nez p0, :cond_0

    .line 855
    :goto_0
    return-void

    .line 849
    :cond_0
    sget-object v0, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/twitter/android/media/camera/c$7;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/camera/c$7;-><init>(Landroid/media/MediaRecorder;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private a(Landroid/hardware/Camera$PictureCallback;)Z
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 519
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/android/media/camera/c;->n:I

    if-ne v0, v3, :cond_2

    .line 521
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/twitter/android/media/camera/c;->n:I

    .line 524
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    sget-object v3, Lcom/twitter/android/media/camera/c;->b:Landroid/hardware/Camera$ShutterCallback;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, p1}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    .line 525
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 526
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v5, "twitter_camera"

    aput-object v5, v4, v0

    const/4 v0, 0x1

    const-string/jumbo v5, ""

    aput-object v5, v4, v0

    const/4 v0, 0x2

    const-string/jumbo v5, "photo"

    aput-object v5, v4, v0

    const/4 v5, 0x3

    iget-boolean v0, p0, Lcom/twitter/android/media/camera/c;->q:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "front"

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x4

    const-string/jumbo v5, "captured"

    aput-object v5, v4, v0

    .line 527
    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 529
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v1

    .line 540
    :goto_1
    return v0

    .line 526
    :cond_0
    const-string/jumbo v0, "back"
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 532
    :catch_0
    move-exception v0

    .line 533
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    if-eqz v1, :cond_1

    .line 534
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    invoke-interface {v1}, Lcom/twitter/android/media/camera/c$a;->c()V

    .line 536
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->g()V

    .line 537
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    :cond_2
    move v0, v2

    .line 540
    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/media/camera/c;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/c;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    .line 712
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 713
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 714
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->x()Z

    move-result v0

    .line 716
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/media/camera/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Landroid/hardware/Camera;)V
    .locals 0

    .prologue
    .line 74
    invoke-static {p0}, Lcom/twitter/android/media/camera/c;->c(Landroid/hardware/Camera;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/media/camera/c;)Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    return-object v0
.end method

.method private static c(Landroid/hardware/Camera;)V
    .locals 2

    .prologue
    .line 835
    sget-object v0, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/twitter/android/media/camera/c$6;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/camera/c$6;-><init>(Landroid/hardware/Camera;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 841
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/media/camera/c;)I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/twitter/android/media/camera/c;->n:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/media/camera/c;)Landroid/hardware/Camera$Parameters;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/media/camera/c;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->u()V

    return-void
.end method

.method private g(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 190
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/twitter/android/media/camera/c;->e:I

    if-ge v0, v2, :cond_1

    .line 191
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/c;->a(I)Landroid/hardware/Camera$CameraInfo;

    move-result-object v2

    .line 192
    iget v2, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v2, p1, :cond_0

    .line 196
    :goto_1
    return v0

    .line 190
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 196
    goto :goto_1
.end method

.method static synthetic g(Lcom/twitter/android/media/camera/c;)Lcom/twitter/android/media/camera/c$c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    return-object v0
.end method

.method private t()V
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/c;->a(Ljava/lang/CharSequence;)V

    .line 456
    const-string/jumbo v0, "off"

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->g:Ljava/lang/String;

    .line 457
    return-void
.end method

.method private u()V
    .locals 3

    .prologue
    .line 460
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v1

    .line 461
    if-nez v1, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    const/4 v0, 0x0

    .line 465
    iget-boolean v2, p0, Lcom/twitter/android/media/camera/c;->q:Z

    if-eqz v2, :cond_3

    const-string/jumbo v2, "macro"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 466
    const-string/jumbo v0, "macro"

    .line 485
    :cond_2
    :goto_1
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 486
    invoke-direct {p0, v0}, Lcom/twitter/android/media/camera/c;->a(Ljava/lang/String;)Z

    goto :goto_0

    .line 468
    :cond_3
    iget-boolean v2, p0, Lcom/twitter/android/media/camera/c;->p:Z

    if-eqz v2, :cond_6

    .line 469
    const-string/jumbo v2, "continuous-video"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 470
    const-string/jumbo v0, "continuous-video"

    goto :goto_1

    .line 471
    :cond_4
    const-string/jumbo v2, "auto"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 472
    const-string/jumbo v0, "auto"

    goto :goto_1

    .line 473
    :cond_5
    const-string/jumbo v2, "fixed"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 474
    const-string/jumbo v0, "fixed"

    goto :goto_1

    .line 477
    :cond_6
    const-string/jumbo v2, "continuous-picture"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 478
    const-string/jumbo v0, "continuous-picture"

    goto :goto_1

    .line 479
    :cond_7
    const-string/jumbo v2, "auto"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 480
    const-string/jumbo v0, "auto"

    goto :goto_1
.end method

.method private v()Z
    .locals 2

    .prologue
    .line 652
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v0

    .line 653
    const-string/jumbo v1, "continuous-picture"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "continuous-video"

    .line 654
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "auto"

    .line 655
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "macro"

    .line 656
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 653
    :goto_0
    return v0

    .line 656
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 808
    invoke-static {}, Lcom/twitter/android/util/b;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/android/util/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 813
    :cond_0
    :goto_0
    return-void

    .line 811
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    iget-boolean v1, p0, Lcom/twitter/android/media/camera/c;->p:Z

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    .line 812
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->x()Z

    goto :goto_0
.end method

.method private x()Z
    .locals 3

    .prologue
    .line 822
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    monitor-enter v1

    .line 824
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    const/4 v0, 0x1

    :try_start_1
    monitor-exit v1

    .line 829
    :goto_0
    return v0

    .line 826
    :catch_0
    move-exception v0

    .line 827
    iget-object v2, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 828
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 829
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 831
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(I)Landroid/hardware/Camera$CameraInfo;
    .locals 1

    .prologue
    .line 201
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 202
    invoke-static {p1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 203
    return-object v0
.end method

.method public a()Landroid/hardware/Camera$Size;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 258
    iput-object p1, p0, Lcom/twitter/android/media/camera/c;->l:Landroid/graphics/SurfaceTexture;

    .line 259
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera;Landroid/graphics/SurfaceTexture;)V

    .line 262
    :cond_0
    return-void
.end method

.method public a(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 6

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    if-eqz p1, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->a()Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 439
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 440
    if-eqz v1, :cond_0

    .line 441
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v1

    invoke-static {v1}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v1

    int-to-double v2, v1

    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    div-double/2addr v2, v4

    .line 442
    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v1

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    new-array v0, v0, [B

    .line 443
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 448
    :cond_1
    return-void
.end method

.method a(Landroid/hardware/Camera;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 225
    if-nez p1, :cond_1

    .line 226
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/c$a;->a()V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iput-object p1, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    .line 233
    invoke-virtual {p1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 234
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->d()Landroid/hardware/Camera$CameraInfo;

    move-result-object v1

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v1, v0, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/media/camera/c;->q:Z

    .line 235
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->u()V

    .line 236
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->t()V

    .line 237
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->w()V

    .line 239
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->l:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->l:Landroid/graphics/SurfaceTexture;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera;Landroid/graphics/SurfaceTexture;)V

    goto :goto_0

    .line 234
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 291
    const-string/jumbo v0, "flash_mode"

    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string/jumbo v0, "camera_id"

    iget v1, p0, Lcom/twitter/android/media/camera/c;->j:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 293
    return-void
.end method

.method public a(Lcom/twitter/android/media/camera/c$a;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    .line 281
    return-void
.end method

.method public a(Ljava/io/File;ILcom/twitter/android/media/camera/i$d;)V
    .locals 7

    .prologue
    .line 759
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 760
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 772
    :cond_0
    :goto_0
    return-void

    .line 764
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->o:Landroid/media/MediaRecorder;

    if-nez v0, :cond_2

    .line 765
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->o:Landroid/media/MediaRecorder;

    .line 767
    :cond_2
    new-instance v0, Lcom/twitter/android/media/camera/i;

    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->o:Landroid/media/MediaRecorder;

    iget-object v2, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    iget v3, p0, Lcom/twitter/android/media/camera/c;->j:I

    iget-object v4, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 769
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->k()I

    move-result v5

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/media/camera/i;-><init>(Landroid/media/MediaRecorder;Landroid/hardware/Camera;ILandroid/hardware/Camera$Parameters;ILjava/io/File;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    .line 770
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    invoke-virtual {v0, p3}, Lcom/twitter/android/media/camera/i;->a(Lcom/twitter/android/media/camera/i$d;)V

    .line 771
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    invoke-virtual {v0, p2}, Lcom/twitter/android/media/camera/i;->a(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    if-nez v0, :cond_1

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v0

    .line 631
    if-eqz v0, :cond_0

    .line 634
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/media/camera/c;->h:Ljava/lang/String;

    .line 635
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 639
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->x()Z

    move-result v0

    if-nez v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    const-string/jumbo v1, "flash"

    invoke-interface {v0, v1, p1}, Lcom/twitter/android/media/camera/c$a;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 169
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/media/camera/c;->g(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/c;->c(I)V

    .line 170
    return-void

    .line 169
    :cond_0
    iget v0, p0, Lcom/twitter/android/media/camera/c;->i:I

    goto :goto_0
.end method

.method public a(Landroid/hardware/Camera$Area;)Z
    .locals 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v0

    if-lez v0, :cond_0

    .line 729
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-static {p1}, Lcom/twitter/util/collection/ImmutableList;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    .line 730
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->x()Z

    move-result v0

    .line 732
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/hardware/Camera$Area;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/twitter/android/media/camera/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-static {p1}, Lcom/twitter/util/collection/ImmutableList;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 722
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->x()Z

    move-result v0

    .line 724
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 174
    iget v0, p0, Lcom/twitter/android/media/camera/c;->e:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 178
    :goto_0
    return-void

    .line 177
    :cond_0
    iget v0, p0, Lcom/twitter/android/media/camera/c;->j:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/twitter/android/media/camera/c;->e:I

    rem-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/c;->c(I)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 207
    sget-object v0, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/twitter/android/media/camera/c$4;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/media/camera/c$4;-><init>(Lcom/twitter/android/media/camera/c;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 222
    return-void
.end method

.method public b(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 266
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->l:Landroid/graphics/SurfaceTexture;

    if-ne v0, p1, :cond_0

    .line 267
    iput-object v1, p0, Lcom/twitter/android/media/camera/c;->l:Landroid/graphics/SurfaceTexture;

    .line 268
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->h()V

    .line 269
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 271
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 272
    :catch_0
    move-exception v0

    .line 273
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 297
    if-eqz p1, :cond_0

    .line 298
    const-string/jumbo v0, "flash_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->g:Ljava/lang/String;

    .line 299
    const-string/jumbo v0, "camera_id"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/c;->i:I

    .line 304
    :goto_0
    return-void

    .line 301
    :cond_0
    const-string/jumbo v0, "off"

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->g:Ljava/lang/String;

    .line 302
    invoke-direct {p0, v1}, Lcom/twitter/android/media/camera/c;->g(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/camera/c;->i:I

    goto :goto_0
.end method

.method public b(Lcom/twitter/android/media/camera/c$a;)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    if-ne v0, p1, :cond_0

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    .line 287
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 594
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/c;->p:Z

    if-eq v0, p1, :cond_0

    .line 595
    iput-boolean p1, p0, Lcom/twitter/android/media/camera/c;->p:Z

    .line 596
    const-string/jumbo v0, "off"

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->g:Ljava/lang/String;

    .line 597
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->t()V

    .line 599
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->u()V

    .line 600
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->l()V

    .line 603
    :cond_0
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/twitter/android/media/camera/c;->j:I

    return v0
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 246
    iget v0, p0, Lcom/twitter/android/media/camera/c;->j:I

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    if-eqz v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->f()V

    .line 251
    iput p1, p0, Lcom/twitter/android/media/camera/c;->j:I

    .line 252
    new-instance v0, Lcom/twitter/android/media/camera/c$c;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/media/camera/c$c;-><init>(Lcom/twitter/android/media/camera/c;I)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    .line 253
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/camera/c$c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 817
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->enableShutterSound(Z)Z

    .line 818
    return-void
.end method

.method public d()Landroid/hardware/Camera$CameraInfo;
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/twitter/android/media/camera/c;->j:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/camera/c;->a(I)Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    return-object v0
.end method

.method public d(I)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 339
    iput p1, p0, Lcom/twitter/android/media/camera/c;->t:I

    .line 340
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    if-nez v0, :cond_1

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    invoke-static {}, Lcom/twitter/android/util/b;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 347
    iget v0, p0, Lcom/twitter/android/media/camera/c;->j:I

    invoke-static {v0}, Lcom/twitter/android/media/camera/i;->b(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    .line 348
    if-nez v0, :cond_8

    .line 349
    const/high16 v0, 0x3f100000    # 0.5625f

    move v1, p1

    .line 364
    :goto_1
    iget-object v4, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 365
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v4

    .line 364
    invoke-static {v4, v1, v0}, Lcom/twitter/android/util/b;->a(Ljava/util/List;IF)Landroid/hardware/Camera$Size;

    move-result-object v4

    .line 366
    if-eqz v4, :cond_b

    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 367
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    move v1, v2

    .line 369
    :goto_2
    iget-object v5, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 370
    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v5

    .line 369
    invoke-static {v5, p1, v0}, Lcom/twitter/android/util/b;->a(Ljava/util/List;IF)Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 371
    if-eqz v0, :cond_c

    iget-object v5, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 372
    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 374
    :goto_3
    if-nez v1, :cond_2

    if-eqz v2, :cond_0

    .line 379
    :cond_2
    if-eqz v1, :cond_3

    .line 380
    iget-boolean v3, p0, Lcom/twitter/android/media/camera/c;->m:Z

    .line 381
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->h()V

    .line 382
    iget-object v5, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    iget v6, v4, Landroid/hardware/Camera$Size;->width:I

    iget v7, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v5, v6, v7}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 385
    iget-object v5, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    const-string/jumbo v6, "video-size"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget v8, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_3
    if-eqz v2, :cond_4

    .line 389
    iget-object v5, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    iget v6, v0, Landroid/hardware/Camera$Size;->width:I

    iget v7, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v5, v6, v7}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 392
    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->x()Z

    move-result v5

    if-nez v5, :cond_7

    .line 395
    if-eqz v1, :cond_5

    .line 396
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    iget v5, v4, Landroid/hardware/Camera$Size;->width:I

    iget v4, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v5, v4}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 398
    :cond_5
    if-eqz v2, :cond_6

    .line 399
    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v2, v0}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 401
    :cond_6
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->x()Z

    .line 404
    :cond_7
    if-eqz v3, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->g()V

    goto/16 :goto_0

    .line 352
    :cond_8
    iget v1, v0, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    .line 353
    iget v4, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    .line 354
    if-ge v1, v4, :cond_9

    int-to-float v0, v1

    int-to-float v5, v4

    div-float/2addr v0, v5

    .line 357
    :goto_4
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto/16 :goto_1

    .line 354
    :cond_9
    int-to-float v0, v4

    int-to-float v5, v1

    div-float/2addr v0, v5

    goto :goto_4

    .line 360
    :cond_a
    const/high16 v0, 0x3f400000    # 0.75f

    move v1, p1

    .line 361
    goto/16 :goto_1

    :cond_b
    move v1, v3

    .line 367
    goto/16 :goto_2

    :cond_c
    move v2, v3

    .line 372
    goto/16 :goto_3
.end method

.method public e()I
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->r()V

    .line 309
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->o:Landroid/media/MediaRecorder;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->a(Landroid/media/MediaRecorder;)V

    .line 310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->o:Landroid/media/MediaRecorder;

    .line 311
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/c;->g:Ljava/lang/String;

    .line 314
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->f()V

    .line 315
    iget v0, p0, Lcom/twitter/android/media/camera/c;->j:I

    return v0
.end method

.method public e(I)I
    .locals 3

    .prologue
    .line 544
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->d()Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    .line 545
    iget v1, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 546
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    neg-int v0, v0

    sub-int/2addr v0, p1

    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(I)I

    move-result v0

    .line 548
    :goto_0
    return v0

    :cond_0
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/camera/c$c;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    iput-object v2, p0, Lcom/twitter/android/media/camera/c;->u:Lcom/twitter/android/media/camera/c$c;

    .line 327
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    .line 328
    if-eqz v0, :cond_0

    .line 329
    iput-object v2, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    .line 330
    iput-object v2, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    .line 331
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->s()V

    .line 332
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->h()V

    .line 333
    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->c(Landroid/hardware/Camera;)V

    goto :goto_0
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 589
    iput p1, p0, Lcom/twitter/android/media/camera/c;->s:I

    .line 590
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 411
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 412
    iput-boolean v1, p0, Lcom/twitter/android/media/camera/c;->m:Z

    .line 413
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/media/camera/c;->n:I

    if-nez v0, :cond_0

    .line 414
    iput v1, p0, Lcom/twitter/android/media/camera/c;->n:I

    .line 415
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/twitter/android/media/camera/c;->w:Landroid/hardware/Camera$PreviewCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 416
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 418
    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 422
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 423
    iput-boolean v1, p0, Lcom/twitter/android/media/camera/c;->m:Z

    .line 424
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/media/camera/c;->n:I

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 427
    :cond_0
    iput v1, p0, Lcom/twitter/android/media/camera/c;->n:I

    .line 428
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->x:Landroid/hardware/Camera$PictureCallback;

    invoke-direct {p0, v0}, Lcom/twitter/android/media/camera/c;->a(Landroid/hardware/Camera$PictureCallback;)Z

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 451
    iget-boolean v0, p0, Lcom/twitter/android/media/camera/c;->q:Z

    return v0
.end method

.method public k()I
    .locals 4

    .prologue
    .line 553
    invoke-static {}, Lcom/twitter/android/util/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->d:Landroid/content/Context;

    .line 557
    invoke-static {v0}, Lcom/twitter/util/n;->a(Landroid/content/Context;)I

    move-result v0

    neg-int v0, v0

    .line 556
    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(I)I

    move-result v0

    .line 562
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->d()Landroid/hardware/Camera$CameraInfo;

    move-result-object v2

    .line 567
    iget v1, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v1, v0

    .line 569
    iget v2, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/16 v2, 0x5a

    if-eq v0, v2, :cond_0

    const/16 v2, 0x10e

    if-ne v0, v2, :cond_2

    .line 576
    :cond_0
    add-int/lit16 v0, v1, 0xb4

    .line 579
    :goto_1
    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(I)I

    move-result v0

    return v0

    .line 559
    :cond_1
    iget v0, p0, Lcom/twitter/android/media/camera/c;->s:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public l()V
    .locals 3

    .prologue
    .line 606
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    if-nez v0, :cond_1

    .line 624
    :cond_0
    :goto_0
    return-void

    .line 610
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v0

    .line 611
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 612
    if-eqz v0, :cond_2

    .line 613
    iget-boolean v2, p0, Lcom/twitter/android/media/camera/c;->p:Z

    if-eqz v2, :cond_3

    .line 614
    const-string/jumbo v2, "torch"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615
    const-string/jumbo v0, "off"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 616
    const-string/jumbo v0, "torch"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 623
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->f:Lcom/twitter/android/media/camera/c$a;

    iget-object v2, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/media/camera/c$a;->a(Ljava/util/Set;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 618
    :cond_3
    iget-boolean v2, p0, Lcom/twitter/android/media/camera/c;->q:Z

    if-nez v2, :cond_2

    .line 619
    const-string/jumbo v2, "torch"

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 620
    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->r:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 661
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/twitter/android/media/camera/c;->n:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/twitter/android/media/camera/c;->a:I

    if-nez v2, :cond_0

    .line 663
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->v()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 664
    iput v0, p0, Lcom/twitter/android/media/camera/c;->a:I

    .line 666
    :try_start_0
    iget-object v2, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    new-instance v3, Lcom/twitter/android/media/camera/c$5;

    invoke-direct {v3, p0}, Lcom/twitter/android/media/camera/c$5;-><init>(Lcom/twitter/android/media/camera/c;)V

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 703
    :goto_0
    return v0

    .line 697
    :catch_0
    move-exception v0

    .line 698
    iput v1, p0, Lcom/twitter/android/media/camera/c;->a:I

    .line 699
    invoke-direct {p0}, Lcom/twitter/android/media/camera/c;->u()V

    .line 700
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    :cond_0
    move v0, v1

    .line 703
    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->k:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->l:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/i;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()V
    .locals 2

    .prologue
    .line 782
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 783
    invoke-virtual {p0}, Lcom/twitter/android/media/camera/c;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 793
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    .line 791
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    .line 792
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/i;->a()V

    goto :goto_0
.end method

.method public s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 797
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 798
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    .line 799
    if-eqz v0, :cond_0

    .line 800
    iput-object v1, p0, Lcom/twitter/android/media/camera/c;->v:Lcom/twitter/android/media/camera/i;

    .line 801
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/i;->b()V

    .line 803
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c;->o:Landroid/media/MediaRecorder;

    invoke-static {v0}, Lcom/twitter/android/media/camera/c;->a(Landroid/media/MediaRecorder;)V

    .line 804
    iput-object v1, p0, Lcom/twitter/android/media/camera/c;->o:Landroid/media/MediaRecorder;

    .line 805
    return-void
.end method
