.class Lcom/twitter/android/media/camera/c$b;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/media/model/ImageFile;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/media/camera/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:[B


# direct methods
.method constructor <init>(Landroid/content/Context;[BILcom/twitter/android/media/camera/c$a;)V
    .locals 1

    .prologue
    .line 927
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 928
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/camera/c$b;->a:Landroid/content/Context;

    .line 929
    iput-object p2, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    .line 930
    iput p3, p0, Lcom/twitter/android/media/camera/c$b;->b:I

    .line 931
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/media/camera/c$b;->c:Ljava/lang/ref/WeakReference;

    .line 932
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/media/model/ImageFile;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 938
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    if-nez v0, :cond_0

    move-object v0, v1

    .line 993
    :goto_0
    return-object v0

    .line 942
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 943
    iput-boolean v5, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 944
    iget-object v2, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    iget-object v3, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    array-length v3, v3

    invoke-static {v2, v4, v3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 945
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 946
    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 948
    mul-int/lit8 v0, v3, 0x3

    mul-int/lit8 v2, v4, 0x4

    if-eq v0, v2, :cond_1

    mul-int/lit8 v0, v3, 0x4

    mul-int/lit8 v2, v4, 0x3

    if-ne v0, v2, :cond_4

    .line 949
    :cond_1
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->c()Lcqr;

    move-result-object v0

    .line 950
    sget-object v2, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    iget-object v2, v2, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcqr;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 951
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    invoke-static {v2, v0}, Lcqc;->a([BLjava/io/File;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 952
    :cond_2
    iput-object v1, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    move-object v0, v1

    .line 953
    goto :goto_0

    .line 955
    :cond_3
    iput-object v1, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    .line 956
    iget v1, p0, Lcom/twitter/android/media/camera/c$b;->b:I

    invoke-static {v1}, Lcom/twitter/media/util/ImageOrientation;->a(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;Z)Z

    .line 958
    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {v0, v1}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    goto :goto_0

    .line 962
    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    array-length v5, v5

    invoke-static {v0, v2, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 963
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    .line 964
    if-nez v5, :cond_5

    move-object v0, v1

    .line 965
    goto :goto_0

    .line 970
    :cond_5
    if-gt v3, v4, :cond_6

    .line 971
    div-int/lit8 v0, v3, 0x3

    int-to-float v2, v0

    .line 972
    div-int/lit8 v0, v4, 0x4

    int-to-float v0, v0

    .line 977
    :goto_1
    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 978
    div-float v2, v6, v2

    sub-float v2, v8, v2

    int-to-float v7, v3

    mul-float/2addr v2, v7

    float-to-int v2, v2

    .line 979
    div-float v0, v6, v0

    sub-float v0, v8, v0

    int-to-float v6, v4

    mul-float/2addr v0, v6

    float-to-int v0, v0

    .line 980
    div-int/lit8 v6, v2, 0x2

    div-int/lit8 v7, v0, 0x2

    sub-int v2, v3, v2

    sub-int v0, v4, v0

    invoke-static {v5, v6, v7, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 981
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 983
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5f

    invoke-static {v0, v2, v3}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)Ljava/io/File;

    move-result-object v2

    .line 984
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 985
    if-nez v2, :cond_7

    move-object v0, v1

    .line 986
    goto/16 :goto_0

    .line 974
    :cond_6
    div-int/lit8 v0, v3, 0x4

    int-to-float v2, v0

    .line 975
    div-int/lit8 v0, v4, 0x3

    int-to-float v0, v0

    goto :goto_1

    .line 988
    :cond_7
    iget v0, p0, Lcom/twitter/android/media/camera/c$b;->b:I

    invoke-static {v0}, Lcom/twitter/media/util/ImageOrientation;->a(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;Z)Z

    .line 990
    sget-object v0, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {v2, v0}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/ImageFile;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 991
    :catch_0
    move-exception v0

    .line 992
    iput-object v1, p0, Lcom/twitter/android/media/camera/c$b;->d:[B

    move-object v0, v1

    .line 993
    goto/16 :goto_0
.end method

.method protected a(Lcom/twitter/media/model/ImageFile;)V
    .locals 1

    .prologue
    .line 1000
    if-eqz p1, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/twitter/android/media/camera/c$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/camera/c$a;

    .line 1002
    if-eqz v0, :cond_0

    .line 1003
    invoke-interface {v0, p1}, Lcom/twitter/android/media/camera/c$a;->a(Lcom/twitter/media/model/ImageFile;)V

    .line 1006
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 918
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/c$b;->a([Ljava/lang/Void;)Lcom/twitter/media/model/ImageFile;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 918
    check-cast p1, Lcom/twitter/media/model/ImageFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/camera/c$b;->a(Lcom/twitter/media/model/ImageFile;)V

    return-void
.end method
