.class Lcom/twitter/android/media/camera/h$a;
.super Lcom/twitter/android/media/widget/VideoSegmentEditView$l;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/camera/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/camera/h;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/twitter/android/media/camera/h;)V
    .locals 0

    .prologue
    .line 1382
    iput-object p1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-direct {p0}, Lcom/twitter/android/media/widget/VideoSegmentEditView$l;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/media/camera/h;Lcom/twitter/android/media/camera/h$1;)V
    .locals 0

    .prologue
    .line 1382
    invoke-direct {p0, p1}, Lcom/twitter/android/media/camera/h$a;-><init>(Lcom/twitter/android/media/camera/h;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1392
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->p()I

    move-result v0

    return v0
.end method

.method protected a(I)I
    .locals 1

    .prologue
    .line 1397
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1398
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->c(Lcom/twitter/android/media/camera/h;)I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x2

    .line 1408
    :goto_0
    return v0

    .line 1398
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1400
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/android/media/camera/h;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1408
    const/4 v0, 0x3

    goto :goto_0

    .line 1402
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 1405
    :pswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 1400
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(II)V
    .locals 2

    .prologue
    .line 1499
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p1, p2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1500
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;Z)Z

    .line 1501
    return-void
.end method

.method protected a(ILandroid/graphics/Rect;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1530
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    .line 1531
    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->m(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/HoverGarbageCanView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/HoverGarbageCanView;->getGarbageCanState()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 1532
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v2}, Lcom/twitter/android/media/camera/h;->m(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/HoverGarbageCanView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/media/widget/HoverGarbageCanView;->a()V

    .line 1533
    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v2}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1534
    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v2, v1}, Lcom/twitter/android/media/camera/h;->b(Z)V

    .line 1536
    :cond_0
    return v0

    .line 1531
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->m(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/HoverGarbageCanView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/HoverGarbageCanView;->getGarbageCanState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(I)Lcom/twitter/media/model/VideoFile;
    .locals 1

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->m(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/HoverGarbageCanView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    iget-object v1, v1, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v1}, Lcom/twitter/android/media/camera/b;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/android/media/widget/HoverGarbageCanView;->a(Landroid/view/View;Landroid/graphics/Rect;)Z

    .line 1526
    return-void
.end method

.method protected c(I)Z
    .locals 1

    .prologue
    .line 1421
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1431
    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v2}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1448
    :cond_0
    :goto_0
    return v0

    .line 1435
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v2}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1436
    if-le v2, v0, :cond_0

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_0

    .line 1439
    invoke-virtual {p0, p1, v1}, Lcom/twitter/android/media/camera/h$a;->b(II)V

    .line 1440
    add-int/lit8 v0, p1, -0x1

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/media/camera/h$a;->b(II)V

    move v0, v1

    .line 1445
    goto :goto_0
.end method

.method protected e(I)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1454
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1456
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0, v5}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/android/media/camera/h;I)I

    .line 1495
    :goto_0
    return-void

    .line 1460
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    .line 1461
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v2}, Lcom/twitter/android/media/camera/h;->n(Lcom/twitter/android/media/camera/h;)I

    move-result v2

    iget v3, v0, Lcom/twitter/media/model/VideoFile;->h:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/twitter/android/media/camera/h;->b(Lcom/twitter/android/media/camera/h;I)I

    .line 1462
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v1}, Lcom/twitter/android/media/camera/h;->o(Lcom/twitter/android/media/camera/h;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v2}, Lcom/twitter/android/media/camera/h;->n(Lcom/twitter/android/media/camera/h;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1463
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v1, v4}, Lcom/twitter/android/media/camera/h;->c(Lcom/twitter/android/media/camera/h;Z)Z

    .line 1464
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v1, v4}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;Z)Z

    .line 1466
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    const-string/jumbo v2, "twitter_camera::video:segment:delete"

    iget-object v3, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    iget v4, p0, Lcom/twitter/android/media/camera/h$a;->c:I

    iget v0, v0, Lcom/twitter/media/model/VideoFile;->h:I

    int-to-long v6, v0

    .line 1467
    invoke-static {v3, v4, v6, v7}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/android/media/camera/h;IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    .line 1466
    invoke-static {v1, v2, v0}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/android/media/camera/h;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    .line 1469
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1470
    if-nez v0, :cond_3

    .line 1471
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1472
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->p(Lcom/twitter/android/media/camera/h;)V

    .line 1474
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->q(Lcom/twitter/android/media/camera/h;)V

    .line 1475
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->r(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/CameraShutterBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraShutterBar;->setShutterButtonMode(I)V

    .line 1489
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->u(Lcom/twitter/android/media/camera/h;)V

    .line 1490
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1491
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    iget-object v0, v0, Lcom/twitter/android/media/camera/h;->d:Lcom/twitter/android/media/camera/b;

    invoke-interface {v0}, Lcom/twitter/android/media/camera/b;->d()Lcom/twitter/android/media/widget/CameraToolbar;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/CameraToolbar;->setFlashEnabled(Z)V

    .line 1494
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->v(Lcom/twitter/android/media/camera/h;)V

    goto/16 :goto_0

    .line 1476
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v1}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1477
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    iget-object v2, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    .line 1478
    invoke-static {v2}, Lcom/twitter/android/media/camera/h;->s(Lcom/twitter/android/media/camera/h;)I

    move-result v2

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v1, v0, v5}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    .line 1479
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v1}, Lcom/twitter/android/media/camera/h;->e(Lcom/twitter/android/media/camera/h;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1480
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->b()V

    .line 1481
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->r()V

    goto :goto_1

    .line 1483
    :cond_4
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    goto :goto_1

    .line 1486
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->t(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/VideoSegmentListView;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/android/media/widget/VideoSegmentListView;->a(Z)V

    goto :goto_1
.end method

.method protected f(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1505
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->s()V

    .line 1506
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1507
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0, p1}, Lcom/twitter/android/media/camera/h;->c(Lcom/twitter/android/media/camera/h;I)I

    .line 1508
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->c(Lcom/twitter/android/media/camera/h;)I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 1509
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->w(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->c()V

    .line 1517
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->c(Lcom/twitter/android/media/camera/h;)I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/camera/h$a;->b(II)V

    .line 1518
    iput p1, p0, Lcom/twitter/android/media/camera/h$a;->c:I

    .line 1519
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/twitter/android/media/camera/h;->e(Lcom/twitter/android/media/camera/h;I)I

    .line 1520
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/camera/h;->b(Z)V

    .line 1521
    return-void

    .line 1511
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0, v2}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;I)I

    .line 1512
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0, p1, v2}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->b()V

    goto :goto_0

    .line 1515
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->t(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/VideoSegmentListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/VideoSegmentListView;->a(Z)V

    goto :goto_0
.end method

.method protected g(I)V
    .locals 6

    .prologue
    .line 1542
    iget v0, p0, Lcom/twitter/android/media/camera/h$a;->c:I

    if-eq p1, v0, :cond_0

    .line 1545
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    const-string/jumbo v2, "twitter_camera::video:segment:change"

    iget-object v3, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    .line 1546
    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    iget v0, v0, Lcom/twitter/media/model/VideoFile;->h:I

    int-to-long v4, v0

    invoke-static {v3, p1, v4, v5}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/android/media/camera/h;IJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    iget v3, p0, Lcom/twitter/android/media/camera/h$a;->c:I

    .line 1547
    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->b(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    move-result-object v0

    .line 1545
    invoke-static {v1, v2, v0}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/android/media/camera/h;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;)V

    .line 1549
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1550
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    .line 1551
    invoke-static {v1}, Lcom/twitter/android/media/camera/h;->x(Lcom/twitter/android/media/camera/h;)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/camera/h;->a(II)Lcom/twitter/android/media/camera/VideoTextureView;

    move-result-object v0

    .line 1552
    iget-object v1, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v1}, Lcom/twitter/android/media/camera/h;->e(Lcom/twitter/android/media/camera/h;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1553
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-virtual {v0}, Lcom/twitter/android/media/camera/h;->r()V

    .line 1560
    :goto_0
    return-void

    .line 1555
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/media/camera/VideoTextureView;->a()V

    goto :goto_0

    .line 1558
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->t(Lcom/twitter/android/media/camera/h;)Lcom/twitter/android/media/widget/VideoSegmentListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/VideoSegmentListView;->a(Z)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 1387
    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->d(Lcom/twitter/android/media/camera/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/media/camera/h$a;->a:Lcom/twitter/android/media/camera/h;

    invoke-static {v0}, Lcom/twitter/android/media/camera/h;->a(Lcom/twitter/android/media/camera/h;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
