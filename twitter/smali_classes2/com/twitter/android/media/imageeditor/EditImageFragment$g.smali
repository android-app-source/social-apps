.class abstract Lcom/twitter/android/media/imageeditor/EditImageFragment$g;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/media/filters/Filters;",
        ">;"
    }
.end annotation


# instance fields
.field final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1135
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1136
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;->b:Ljava/lang/ref/WeakReference;

    .line 1137
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/twitter/android/media/imageeditor/EditImageFragment$1;)V
    .locals 0

    .prologue
    .line 1132
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/media/filters/Filters;
    .locals 3

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1142
    if-eqz v0, :cond_1

    .line 1143
    new-instance v1, Lcom/twitter/media/filters/Filters;

    invoke-direct {v1}, Lcom/twitter/media/filters/Filters;-><init>()V

    .line 1144
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Z)Z

    move-result v0

    .line 1145
    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1152
    :goto_0
    return-object v0

    .line 1148
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->b()V

    .line 1149
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "failed to load filters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 1152
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a(Lcom/twitter/media/filters/Filters;)V
.end method

.method protected b(Lcom/twitter/media/filters/Filters;)V
    .locals 0

    .prologue
    .line 1160
    if-eqz p1, :cond_0

    .line 1161
    invoke-virtual {p1}, Lcom/twitter/media/filters/Filters;->b()V

    .line 1163
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1132
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;->a([Ljava/lang/Void;)Lcom/twitter/media/filters/Filters;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1132
    check-cast p1, Lcom/twitter/media/filters/Filters;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;->b(Lcom/twitter/media/filters/Filters;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1132
    check-cast p1, Lcom/twitter/media/filters/Filters;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;->a(Lcom/twitter/media/filters/Filters;)V

    return-void
.end method
