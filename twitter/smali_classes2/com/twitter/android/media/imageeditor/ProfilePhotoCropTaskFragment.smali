.class public Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;
.super Lcom/twitter/app/common/base/BaseFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;,
        Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;

.field private b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

.field private c:Lcom/twitter/model/media/EditableImage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;)Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    return-object p1
.end method

.method public static a(Lcom/twitter/model/media/EditableImage;)Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-direct {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;-><init>()V

    .line 29
    new-instance v1, Lcom/twitter/app/common/base/b$a;

    invoke-direct {v1}, Lcom/twitter/app/common/base/b$a;-><init>()V

    const-string/jumbo v2, "editable_image"

    .line 30
    invoke-virtual {v1, v2, p0}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lcom/twitter/app/common/base/b$a;->c()Lcom/twitter/app/common/base/b;

    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 33
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;)Lcom/twitter/model/media/EditableImage;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->c:Lcom/twitter/model/media/EditableImage;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;)Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;

    return-object v0
.end method


# virtual methods
.method public d()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->cancel(Z)Z

    .line 55
    :cond_0
    new-instance v0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;-><init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$1;)V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    .line 56
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 57
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 39
    check-cast p1, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;

    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;

    .line 40
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->setRetainInstance(Z)V

    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    const-string/jumbo v1, "editable_image"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->c:Lcom/twitter/model/media/EditableImage;

    .line 48
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->d()V

    .line 49
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->onDestroy()V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->cancel(Z)Z

    .line 71
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->onDetach()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;

    .line 63
    return-void
.end method
