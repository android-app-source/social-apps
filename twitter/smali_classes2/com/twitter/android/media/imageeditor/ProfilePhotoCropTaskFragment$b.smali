.class Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/media/model/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$1;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;-><init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/media/model/MediaFile;
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->a(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;)Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    invoke-static {v0, v1}, Lbrt;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/model/MediaFile;)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;)Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->b(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;)Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;->a(Lcom/twitter/media/model/MediaFile;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->a(Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;)Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;

    .line 95
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a([Ljava/lang/Void;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 77
    check-cast p1, Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$b;->a(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method
