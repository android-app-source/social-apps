.class public Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;
.super Lcom/twitter/android/media/imageeditor/EditImageActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

.field private b:Lcom/twitter/model/media/EditableImage;

.field private c:Lcom/twitter/media/model/MediaFile;

.field private d:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)Lcom/twitter/media/model/MediaFile;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->c:Lcom/twitter/media/model/MediaFile;

    return-object v0
.end method

.method private a(I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-nez v0, :cond_0

    .line 137
    invoke-static {p1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 138
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 139
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 141
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;Lcom/twitter/media/model/MediaFile;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->i()V

    return-void
.end method

.method private b(Lcom/twitter/media/model/MediaFile;)V
    .locals 5

    .prologue
    .line 97
    const v0, 0x7f0a06f1

    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a(I)V

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 99
    invoke-static {}, Lcom/twitter/media/util/s;->a()Lcom/twitter/media/util/s;

    move-result-object v2

    invoke-virtual {v2, v0, v1, p1}, Lcom/twitter/media/util/s;->a(JLcom/twitter/media/model/MediaFile;)V

    .line 100
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/client/n;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, p1, v3, v4}, Lcom/twitter/library/client/n;-><init>(Lcom/twitter/media/model/MediaFile;Lcom/twitter/media/model/MediaFile;Z)V

    new-instance v3, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$a;

    invoke-direct {v3, p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$a;-><init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;Lcom/twitter/android/client/x$a;)Ljava/lang/String;

    .line 102
    return-void
.end method

.method public static c(Landroid/content/Intent;)Lcom/twitter/media/model/MediaFile;
    .locals 1

    .prologue
    .line 41
    const-string/jumbo v0, "media_file"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->j()V

    return-void
.end method

.method private c(Lcom/twitter/media/model/MediaFile;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 105
    invoke-static {}, Lbpr;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-static {p0, p1}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 117
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-static {}, Lbpr;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 112
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 113
    const-string/jumbo v1, "media_file"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 114
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->setResult(ILandroid/content/Intent;)V

    .line 115
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->l()V

    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 59
    if-eqz v0, :cond_1

    const v1, 0x3a83126f    # 0.001f

    invoke-virtual {v0, v1}, Lcom/twitter/util/math/c;->a(F)Z

    move-result v0

    if-nez v0, :cond_1

    .line 60
    const v0, 0x7f0a06f1

    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a(I)V

    .line 61
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 62
    const-string/jumbo v0, "crop_task_fragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->d:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    .line 64
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->d:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b:Lcom/twitter/model/media/EditableImage;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->a(Lcom/twitter/model/media/EditableImage;)Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->d:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    .line 66
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->d:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    const-string/jumbo v2, "crop_task_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 73
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->d:Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoCropTaskFragment;->d()V

    goto :goto_0

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b(Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->n()V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->c:Lcom/twitter/media/model/MediaFile;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->c(Lcom/twitter/media/model/MediaFile;)V

    .line 89
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->c:Lcom/twitter/media/model/MediaFile;

    goto :goto_0
.end method

.method private l()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->n()V

    .line 93
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->o()V

    .line 94
    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->e()V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->a:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 148
    :cond_0
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 151
    const v0, 0x7f1302e4

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a06e1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 154
    const v1, 0x7f0a0779

    new-instance v2, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$1;-><init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 161
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 162
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 165
    const v0, 0x7f1302e4

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a06e0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 168
    const v1, 0x7f0a06e2

    new-instance v2, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$2;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$2;-><init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 175
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 176
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/model/MediaFile;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->c:Lcom/twitter/media/model/MediaFile;

    .line 78
    if-eqz p1, :cond_0

    .line 79
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b(Lcom/twitter/media/model/MediaFile;)V

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->n()V

    .line 82
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->q()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->b:Lcom/twitter/model/media/EditableImage;

    .line 48
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->i()V

    .line 49
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->setResult(I)V

    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->finish()V

    .line 55
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 121
    packed-switch p1, :pswitch_data_0

    .line 128
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->setResult(I)V

    .line 129
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->finish()V

    .line 133
    :goto_0
    return-void

    .line 123
    :pswitch_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->setResult(I)V

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->finish()V

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
