.class Lcom/twitter/android/media/imageeditor/EditImageFragment$10;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/filters/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/imageeditor/EditImageFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 396
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setFilterRenderListener(Lcom/twitter/media/filters/b$a;)V

    .line 398
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 413
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 401
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v0, v2, v2}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    goto :goto_0

    .line 405
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f()V

    goto :goto_0

    .line 399
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
