.class Lcom/twitter/android/media/imageeditor/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laow;
.implements Lcom/twitter/android/media/widget/FilterFilmstripView$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laow",
        "<",
        "Lcdv;",
        ">;",
        "Lcom/twitter/android/media/widget/FilterFilmstripView$a;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field private final d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

.field private final e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

.field private final f:Lcom/twitter/model/media/EditableImage;

.field private final g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

.field private h:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

.field private i:Lcom/twitter/media/filters/Filters;

.field private j:Landroid/support/v4/view/ViewPager;

.field private k:I


# direct methods
.method constructor <init>(Lcom/twitter/android/media/stickers/StickerFilteredImageView;Lcom/twitter/android/media/imageeditor/CropMediaImageView;Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;Lcom/twitter/model/media/EditableImage;Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    .line 77
    iput-object p2, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    .line 78
    iput-object p3, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    .line 79
    iput-object p4, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    .line 80
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->a:Ljava/lang/ref/WeakReference;

    .line 81
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->b:Ljava/lang/ref/WeakReference;

    .line 83
    invoke-static {p5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 84
    const-string/jumbo v2, "filters_tooltip_times_shown"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    .line 85
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    const-string/jumbo v4, "filters_tooltip_last_time_shown"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/android/media/imageeditor/a;->c:Z

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/a;)Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    return-object v0
.end method

.method private c(Lcom/twitter/android/media/widget/FilterFilmstripView;)V
    .locals 4

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/a;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->getActivePreview()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    .line 347
    new-instance v1, Lcom/twitter/android/media/imageeditor/a$4;

    invoke-direct {v1, p0, p1, v0}, Lcom/twitter/android/media/imageeditor/a$4;-><init>(Lcom/twitter/android/media/imageeditor/a;Lcom/twitter/android/media/widget/FilterFilmstripView;Landroid/view/View;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {p1, v1, v2, v3}, Lcom/twitter/android/media/widget/FilterFilmstripView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method a()Lcom/twitter/model/media/EditableImage;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    return-object v0
.end method

.method a(F)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    .line 230
    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/CroppableImageView;->setCropAspectRatio(F)V

    .line 231
    return-void
.end method

.method a(IZ)V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    .line 273
    invoke-virtual {v0, p1, p2}, Lcom/twitter/ui/widget/CroppableImageView;->a(IZ)V

    .line 274
    return-void
.end method

.method public a(Lcdv;)V
    .locals 3

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    iget v1, p0, Lcom/twitter/android/media/imageeditor/a;->k:I

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->a(Lcdv;IZ)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->j:Landroid/support/v4/view/ViewPager;

    .line 199
    return-void
.end method

.method a(Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/a;->h:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    .line 264
    return-void
.end method

.method a(Lcom/twitter/android/media/stickers/data/a;Lcom/twitter/android/composer/ComposerType;I)V
    .locals 2

    .prologue
    .line 103
    iput p3, p0, Lcom/twitter/android/media/imageeditor/a;->k:I

    .line 104
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    .line 105
    :goto_0
    invoke-static {v0}, Lbpt;->a(Z)Z

    move-result v0

    .line 106
    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    new-instance v1, Lcom/twitter/android/media/imageeditor/a$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/media/imageeditor/a$1;-><init>(Lcom/twitter/android/media/imageeditor/a;Lcom/twitter/android/media/stickers/data/a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setRetryStickerCatalogListener(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$a;)V

    .line 113
    new-instance v0, Lcom/twitter/android/media/imageeditor/a$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/imageeditor/a$2;-><init>(Lcom/twitter/android/media/imageeditor/a;)V

    invoke-virtual {p1, p0, v0}, Lcom/twitter/android/media/stickers/data/a;->a(Laow;Lcom/twitter/android/media/stickers/data/a$b;)V

    .line 123
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->q()V

    .line 124
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setRetryStickerCatalogListener(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$a;)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/media/widget/FilterFilmstripView;)V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {p1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->getSelectedFilter()I

    move-result v1

    iput v1, v0, Lcom/twitter/model/media/EditableImage;->c:I

    .line 334
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {p1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->getIntensity()F

    move-result v1

    iput v1, v0, Lcom/twitter/model/media/EditableImage;->d:F

    .line 335
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->v()V

    .line 336
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/a;->c(Lcom/twitter/android/media/widget/FilterFilmstripView;)V

    .line 337
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->l()V

    .line 338
    return-void
.end method

.method a(Lcom/twitter/media/filters/Filters;)V
    .locals 1

    .prologue
    .line 325
    if-eqz p1, :cond_0

    .line 326
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setFilters(Lcom/twitter/media/filters/Filters;)V

    .line 328
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/a;->i:Lcom/twitter/media/filters/Filters;

    .line 329
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->setVisibility(I)V

    .line 212
    if-nez p1, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->j()V

    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->v()V

    .line 216
    :cond_0
    return-void
.end method

.method b()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setVisibility(I)V

    .line 131
    return-void
.end method

.method public b(Lcom/twitter/android/media/widget/FilterFilmstripView;)V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {p1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->getIntensity()F

    move-result v1

    iput v1, v0, Lcom/twitter/model/media/EditableImage;->d:F

    .line 384
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->l()V

    .line 385
    return-void
.end method

.method public synthetic b_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Lcdv;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/a;->a(Lcdv;)V

    return-void
.end method

.method c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 134
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setVisibility(I)V

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->l()V

    .line 137
    return-void
.end method

.method d()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->g:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->a()V

    .line 145
    return-void
.end method

.method f()I
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->j:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method g()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcdy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->getStickers()Ljava/util/ArrayList;

    move-result-object v1

    .line 154
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget v4, v0, Lcom/twitter/model/media/EditableImage;->e:I

    .line 159
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    iget-object v0, v0, Lcom/twitter/media/model/ImageFile;->f:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v2

    .line 160
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v3, v0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 164
    if-nez v3, :cond_2

    .line 165
    rem-int/lit16 v0, v4, 0xb4

    if-nez v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    div-float/2addr v0, v2

    :goto_1
    move v5, v0

    .line 174
    :goto_2
    invoke-static {v1}, Lcom/twitter/android/media/stickers/b;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 175
    new-instance v0, Lcom/twitter/android/media/imageeditor/a$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/media/imageeditor/a$3;-><init>(Lcom/twitter/android/media/imageeditor/a;FLcom/twitter/util/math/c;IF)V

    invoke-static {v6, v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 165
    goto :goto_1

    .line 169
    :cond_2
    rem-int/lit16 v0, v4, 0xb4

    if-nez v0, :cond_3

    .line 170
    invoke-virtual {v3}, Lcom/twitter/util/math/c;->d()F

    move-result v0

    invoke-virtual {v3}, Lcom/twitter/util/math/c;->c()F

    move-result v5

    mul-float/2addr v5, v2

    div-float/2addr v0, v5

    :goto_3
    move v5, v0

    .line 171
    goto :goto_2

    :cond_3
    invoke-virtual {v3}, Lcom/twitter/util/math/c;->d()F

    move-result v0

    mul-float/2addr v0, v2

    invoke-virtual {v3}, Lcom/twitter/util/math/c;->c()F

    move-result v5

    div-float/2addr v0, v5

    goto :goto_3
.end method

.method public h()I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->getStickers()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method i()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setVisibility(I)V

    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->l()V

    .line 206
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->setVisibility(I)V

    .line 207
    return-void
.end method

.method j()V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getCropState()Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->h:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    .line 220
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->h:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    iget v1, v1, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a:I

    iput v1, v0, Lcom/twitter/model/media/EditableImage;->e:I

    .line 221
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->h:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a()Lcom/twitter/util/math/c;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 222
    return-void
.end method

.method k()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    iget-object v0, v0, Lcom/twitter/media/model/ImageFile;->f:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/a;->a(F)V

    .line 226
    return-void
.end method

.method l()V
    .locals 6

    .prologue
    .line 234
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 235
    if-nez v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    invoke-virtual {v1}, Lcom/twitter/model/media/EditableImage;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    const/4 v2, 0x1

    .line 239
    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Z)Lcom/twitter/media/request/a$a;

    move-result-object v1

    new-instance v2, Lbzh;

    .line 241
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget v3, v3, Lcom/twitter/model/media/EditableImage;->c:I

    iget-object v4, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-boolean v4, v4, Lcom/twitter/model/media/EditableImage;->b:Z

    iget-object v5, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget v5, v5, Lcom/twitter/model/media/EditableImage;->d:F

    invoke-direct {v2, v0, v3, v4, v5}, Lbzh;-><init>(Landroid/content/Context;IZF)V

    .line 240
    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Lbzi;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 245
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v2, v0, Lcom/twitter/model/media/EditableImage;->h:Ljava/util/List;

    .line 246
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 247
    new-instance v3, Lbrf;

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    iget-object v0, v0, Lcom/twitter/media/model/ImageFile;->f:Lcom/twitter/util/math/Size;

    .line 248
    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4, v2}, Lbrf;-><init>(FILjava/util/List;)V

    .line 247
    invoke-virtual {v1, v3}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/process/a;)Lcom/twitter/media/request/a$a;

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->setScaleFactor(F)V

    .line 251
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/a;->h:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a(Lcom/twitter/media/request/a$a;Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;)Z

    goto :goto_0
.end method

.method m()Z
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method n()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->e:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getCropState()Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    move-result-object v0

    return-object v0
.end method

.method p()V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->c()V

    .line 280
    return-void
.end method

.method q()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->d()V

    .line 284
    return-void
.end method

.method r()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->g()V

    .line 288
    return-void
.end method

.method s()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->i:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->i:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0}, Lcom/twitter/media/filters/Filters;->b()V

    .line 294
    :cond_0
    return-void
.end method

.method t()Z
    .locals 2

    .prologue
    .line 297
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-boolean v0, v0, Lcom/twitter/model/media/EditableImage;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/twitter/model/media/EditableImage;->b:Z

    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->v()V

    .line 299
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->l()V

    .line 300
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    iget-boolean v0, v0, Lcom/twitter/model/media/EditableImage;->b:Z

    return v0

    .line 297
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method u()V
    .locals 0

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->v()V

    .line 305
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->l()V

    .line 306
    return-void
.end method

.method v()V
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 310
    if-nez v0, :cond_0

    .line 317
    :goto_0
    return-void

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->f:Lcom/twitter/model/media/EditableImage;

    .line 314
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->a(Lcom/twitter/model/media/EditableImage;)Z

    .line 315
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    iget v2, v0, Lcom/twitter/model/media/EditableImage;->d:F

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setFilterIntensity(F)V

    .line 316
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/a;->d:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    iget v2, v0, Lcom/twitter/model/media/EditableImage;->c:I

    iget-boolean v0, v0, Lcom/twitter/model/media/EditableImage;->b:Z

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->a(IZ)V

    goto :goto_0
.end method

.method w()Lcom/twitter/media/filters/Filters;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/a;->i:Lcom/twitter/media/filters/Filters;

    return-object v0
.end method
