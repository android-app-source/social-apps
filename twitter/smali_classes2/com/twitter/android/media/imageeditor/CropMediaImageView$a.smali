.class public Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/imageeditor/CropMediaImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:I

.field public final b:Lcom/twitter/util/math/c;


# direct methods
.method public constructor <init>(ILcom/twitter/util/math/c;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput p1, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a:I

    .line 92
    iput-object p2, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->b:Lcom/twitter/util/math/c;

    .line 93
    return-void
.end method

.method public static a(Lcom/twitter/model/media/EditableImage;)Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;
    .locals 3

    .prologue
    .line 102
    iget v2, p0, Lcom/twitter/model/media/EditableImage;->e:I

    .line 103
    iget-object v0, p0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    sget-object v1, Lcom/twitter/util/math/c;->c:Lcom/twitter/util/math/c;

    .line 104
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/c;

    .line 105
    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/util/math/c;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    :cond_0
    new-instance v1, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;-><init>(ILcom/twitter/util/math/c;)V

    move-object v0, v1

    .line 111
    :goto_0
    return-object v0

    .line 110
    :cond_1
    invoke-static {v2}, Lcom/twitter/media/util/ImageOrientation;->a(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/util/ImageOrientation;->b()Lcom/twitter/media/util/ImageOrientation;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/media/util/ImageOrientation;->a(Lcom/twitter/util/math/c;)Lcom/twitter/util/math/c;

    move-result-object v1

    .line 111
    new-instance v0, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    invoke-direct {v0, v2, v1}, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;-><init>(ILcom/twitter/util/math/c;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/util/math/c;
    .locals 2

    .prologue
    .line 97
    iget v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a:I

    invoke-static {v0}, Lcom/twitter/media/util/ImageOrientation;->a(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->b:Lcom/twitter/util/math/c;

    invoke-virtual {v0, v1}, Lcom/twitter/media/util/ImageOrientation;->a(Lcom/twitter/util/math/c;)Lcom/twitter/util/math/c;

    move-result-object v0

    return-object v0
.end method
