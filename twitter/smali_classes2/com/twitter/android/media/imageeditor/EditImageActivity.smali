.class public Lcom/twitter/android/media/imageeditor/EditImageActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/imageeditor/EditImageFragment$b;


# instance fields
.field private a:Lcom/twitter/android/media/imageeditor/EditImageFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/composer/ComposerType;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/media/imageeditor/EditImageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "editable_image"

    .line 38
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "composer_type"

    .line 39
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "editor_type"

    .line 40
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_section"

    .line 41
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;FIZ)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 49
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;FIZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;FIZLjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/media/imageeditor/EditImageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "editable_image"

    .line 59
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_section"

    .line 60
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "crop_aspect_ratio"

    .line 61
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "editor_type"

    .line 62
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "lock_editor"

    .line 63
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "done_button_text"

    .line 64
    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/model/media/EditableImage;
    .locals 1

    .prologue
    .line 69
    const-string/jumbo v0, "editable_image"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    return-object v0
.end method

.method public static b(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string/jumbo v0, "filter_effect"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 81
    const v0, 0x7f0400da

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 83
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 5

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 89
    const-string/jumbo v0, "editable_image"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 91
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v3, "image_edit"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/media/imageeditor/EditImageFragment;

    iput-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 93
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    if-nez v1, :cond_0

    .line 94
    const-string/jumbo v1, "editor_type"

    const/4 v3, 0x1

    .line 95
    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 96
    new-instance v3, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    invoke-direct {v3}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;-><init>()V

    const-string/jumbo v4, "scribe_section"

    .line 97
    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v3

    .line 98
    invoke-virtual {v3, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(I)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v3

    const-string/jumbo v1, "composer_type"

    .line 99
    invoke-virtual {v2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/composer/ComposerType;

    invoke-virtual {v3, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(Lcom/twitter/android/composer/ComposerType;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v1

    const-string/jumbo v3, "crop_aspect_ratio"

    const/4 v4, 0x0

    .line 100
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(F)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v1

    const-string/jumbo v3, "lock_editor"

    const/4 v4, 0x0

    .line 101
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(Z)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v1

    const-string/jumbo v3, "done_button_text"

    .line 102
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->b(Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v1

    .line 103
    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a()Lcom/twitter/android/media/imageeditor/EditImageFragment;

    move-result-object v1

    .line 104
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f1302e4

    const-string/jumbo v4, "image_edit"

    .line 105
    invoke-virtual {v2, v3, v1, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 106
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 107
    iput-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableImage;)V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment$b;)V

    .line 111
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 122
    const-string/jumbo v1, "editable_image"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 123
    if-eqz p2, :cond_0

    .line 124
    const-string/jumbo v1, "filter_effect"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->setResult(ILandroid/content/Intent;)V

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->finish()V

    .line 128
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->setResult(I)V

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->finish()V

    .line 134
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e()V

    .line 116
    return-void
.end method
